/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "core/cgfdefs.hpp"
#include "geo/ply_io.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include "datastructures/List.hpp"
#include "math/Math.hpp"
#include "datastructures/Tree.hpp"

namespace CGF{

  enum load_order{none = 0, ply_vertices, ply_list_vertex_indices};

  template<class T>
  class ply_data{
  public:
    int n_faces;
    int n_vertices;
    int n_vertex_elements;
    int current_element;
    load_order order[5];
    DCTetraMesh<T>* mesh;
    Tree<Vector4<T> > tree;
    Vector4<T>* vertices;
    int* faces;
  };

  double vmin[] = {10000, 10000, 10000};
  double vmax[] = {-10000, -10000, -10000};

  /*Loads a data line and skips a comment line*/
  std::string read_ply_line(std::ifstream& file){
    std::string line;
    while(!file.eof()){
      getline(file, line);

      std::string buffer;
      std::stringstream ss(line);

      ss >> buffer;
      if(buffer == "comment"){
        message("Comment:: %s", line.c_str());
        continue;
      }else if(buffer == "obj_info"){
        message("Obj info:: %s", line.c_str());
        continue;
      }else{
        return line;
      }
    }
    return line;
  }

  template<class T>
  void ply_load_list(std::ifstream& file, ply_data<T>* data){

    for(int i=0;i<data->n_faces;i++){
      List<int> list;
      std::string line = read_ply_line(file);
      std::string buffer;
      std::stringstream ss(line);

      /*Read length of list*/

      int length;
      ss >> length;
      //DCELHalfFace<> hf;

      //int hf_index = data->mesh->addHalfFace(hf);

      if(length != 3){
        error("face != triangle");
      }

      for(int j=0;j<length;j++){
        int index;
        ss >> index;

        //int tree_index = data->tree.findIndex(data->mesh.vertices[index]);
        //if(tree_index == -1){
        //tree_index = index;
        //}
        //std::cout << index << "\t";
        //DCELHalfEdge<> he(index, hf_index, Mesh::undefined,
        //Mesh::undefined, Mesh::undefined);
        //index = data->mesh->addHalfEdge(he);

        //list.append(index);
        data->faces[i*3 + j] = index;
      }

      /*
              Iterator<int> it = list.begin();

              while(it != list.end()){
              int index = *it;
              it++;
              int next = 0;
              if(it == list.end()){
              next = *(list.begin());
              }else{
              next = *it;
              }

              data->mesh->halfEdges[index].next = next;
              data->mesh->halfEdges[index].half_face = hf_index;
              data->mesh->vertices[data->mesh->halfEdges[index].origin].leaving =
              index;
              data->mesh->halfFaces[hf_index].half_edge = index;
              }
      */
    }
  }

  template<class T>
  void ply_load_vertices(std::ifstream& file,
                         ply_data<T>* data){
    for(int i=0;i<data->n_vertices;i++){
      std::string line = read_ply_line(file);
      std::string buffer;
      std::stringstream ss(line);

      for(int j=0;j<data->n_vertex_elements;j++){
        T val;
        ss >> val;

        if(j < 3){
          vmax[j] = (T)Max((T)vmax[j], val);
          vmin[j] = (T)Min((T)vmin[j], val);
          data->vertices[i][j] = val;
        }
      }

      //int index = data->mesh->addVertex(vertex);
      //data->tree.uniqueInsert(vertex.coord, index);
    }

    message("bbox max = %f, %f, %f", vmax[0], vmax[1], vmax[2]);
    message("bbox min = %f, %f, %f", vmin[0], vmin[1], vmin[2]);
  }


  bool read_property(std::ifstream& file){
    /*Store current position*/
    long cpos = file.tellg();

    std::string line = read_ply_line(file);

    std::string buffer;
    std::stringstream ss(line);

    ss >> buffer;

    if(buffer == "property"){
      return true;
    }

    /*Did not read a property, reset stream*/
    file.seekg(cpos);
    return false;
  }

  template<class T>
  bool read_element_type(std::ifstream& file, ply_data<T>* data){
    long cpos = file.tellg();

    std::string line = read_ply_line(file);

    std::string buffer;
    std::stringstream ss(line);

    ss >> buffer;

    if(buffer == "element"){
      message("element found");

      ss >> buffer;
      if(buffer == "vertex"){
        ss >> data->n_vertices;
        message("%d vertices", data->n_vertices);

        data->n_vertex_elements = 0;

        while(read_property(file)){
          data->n_vertex_elements++;
        }

        message("%d vertex elements", data->n_vertex_elements);
        data->order[data->current_element++] = ply_vertices;

        cpos = file.tellg();

      }else if(buffer == "face"){
        ss >> data->n_faces;
        message("%d faces", data->n_faces);

        while(read_property(file)){

        }

        data->order[data->current_element++] = ply_list_vertex_indices;

        cpos = file.tellg();
      }

      return true;
    }

    file.seekg(cpos);

    return false;
  }

  template<class T>
  DCTetraMesh<T>* load_ply(const char* filename, int operation){
    int format = 0;

    ply_data<T> data;

    data.n_faces = 0;
    data.n_vertices = 0;
    data.n_vertex_elements = 0;
    data.vertices = 0;
    data.faces = 0;
    data.current_element = 0;
    data.order[0] = data.order[1] = data.order[2] =
      data.order[3] = data.order[4] = none;

    data.mesh = new DCTetraMesh<T>();

    std::ifstream file(filename, std::ios::in);

    if(file.is_open()){
      std::string line;

      /*Read first line*/
      line = read_ply_line(file);

      /*Check header*/
      if(line != "ply"){
        message("Not a ply file");
        return data.mesh;
      }

      /*Check format*/
      line = read_ply_line(file);

      if(line == "format ascii 1.0"){
        format = 1;
      }else if(line == "format binary_big_endian 1.0"){
        format = 2;
      }else{
        message("Unsupported format, %s", line.c_str());
      }

      message("format = %d", format);

      if(format == 2){
        message("Binary format not supported yet.");
        return data.mesh;
      }

      /*Load element description*/
      while(read_element_type(file, &data)){

      }

      /*Should read now a end header line*/
      line = read_ply_line(file);
      if(line == "end_header"){
        message("load data");

        data.vertices = new Vector4<T>[data.n_vertices];
        data.faces    = new int [data.n_faces * 3];

        for(int i=0;i<data.current_element;i++){
          if(data.order[i] == ply_vertices){
            ply_load_vertices(file, &data);
          }else if(data.order[i] == ply_list_vertex_indices){
            ply_load_list(file, &data);
          }
        }
      }

      if(operation == 0){
        /*Do nothing special*/
        /*Construct DCEL mesh*/
        for(int i=0;i<data.n_vertices;i++){
          DCELVertex<T> vertex;
          vertex.coord = data.vertices[i];
          int index = data.mesh->addVertex(vertex);
          index = index;
          data.mesh->vertices[index].id = index;
          cgfassert(index == i);
        }

        /*Face*/
        for(int i=0;i<data.n_faces;i++){
          DCELHalfFace<T> hf;
          int hf_index = data.mesh->addHalfFace(hf);

          int edgeList[3];

          for(int j=0;j<3;j++){
            int index = data.faces[i*3+j];
            DCELHalfEdge<> he(index, hf_index, Mesh::UndefinedIndex,
                              Mesh::UndefinedIndex, Mesh::UndefinedIndex);
            index = data.mesh->addHalfEdge(he);
            edgeList[j] = index;
          }

          for(int j=0;j<3;j++){
            int index = edgeList[j];
            int next;
            if(j == 2){
              next = edgeList[0];
            }else{
              next = edgeList[j+1];
            }
            data.mesh->halfEdges[index].next = next;
            data.mesh->halfEdges[index].half_face = hf_index;
            data.mesh->vertices[data.mesh->halfEdges[index].origin].leaving =
              index;
            data.mesh->halfFaces[hf_index].half_edge = index;
          }
        }
        data.mesh->connectHalfEdges();
        delete [] data.faces;
        delete [] data.vertices;
        return data.mesh;
      }

      int* vertexDegree = new int[data.n_vertices];

      /*Remove duplicates and compact*/
      /*Store all unique vertices in a tree*/
      for(int i=0;i<data.n_vertices;i++){
        //std::cout << data.vertices[i] << std::endl;
        data.tree.uniqueInsert(data.vertices[i], i);
        //message("size = %d", data.tree.size());
        vertexDegree[i] = 0;
      }

      /*Update face indices*/
      for(int i=0;i<data.n_faces*3;i++){
        int orig_index = data.faces[i];
        int new_index = data.tree.findIndex(data.vertices[orig_index]);
        /*message("orig = %d, new = %d", orig_index, new_index);
          std::cout << data.vertices[orig_index] << std::endl;
          std::cout << data.vertices[new_index] << std::endl;

          message("less  = %d", less(data.vertices[orig_index],
          data.vertices[new_index]));

          message("lessr = %d", less(data.vertices[new_index],
          data.vertices[orig_index]));

          Vector4f diff = data.vertices[new_index] -  data.vertices[orig_index];

          message("%10.10e, %10.10e, %10.10e", sqrf(diff[0]), sqrf(diff[1]),
          sqrf(diff[2]));
        */
        if(orig_index != new_index){
          //getchar();
        }

        cgfassert(new_index != Mesh::UndefinedIndex);

        data.faces[i] = new_index;

        vertexDegree[new_index]++;
      }

      /*Remove singular faces*/
      int currentFaceIndex = 0;
      for(int i=0;i<data.n_faces;i++){
        if(data.faces[i*3+0] == data.faces[i*3+1] ||
           data.faces[i*3+1] == data.faces[i*3+2] ||
           data.faces[i*3+2] == data.faces[i*3+0]){
          /*Singular face, skip*/
        }else{
          data.faces[currentFaceIndex*3+0] = data.faces[i*3+0];
          data.faces[currentFaceIndex*3+1] = data.faces[i*3+1];
          data.faces[currentFaceIndex*3+2] = data.faces[i*3+2];
          currentFaceIndex++;
        }
      }

      data.n_faces = currentFaceIndex;

      int* vertex_map = new int[data.n_vertices];
      /*Remove unused vertices*/
      int currentVertexIndex = 0;
      for(int i=0;i<data.n_vertices;i++){
        if(vertexDegree[i] == 0){
          /*Skip*/
          vertex_map[i] = Mesh::UndefinedIndex;
        }else{
          data.vertices[currentVertexIndex] = data.vertices[i];
          vertex_map[i] = currentVertexIndex;
          currentVertexIndex++;
        }
      }

      data.n_vertices = currentVertexIndex;
      delete [] vertexDegree;

      /*Construct DCEL mesh*/
      for(int i=0;i<data.n_vertices;i++){
        DCELVertex<T> vertex;
        vertex.coord = data.vertices[i];
        int index = data.mesh->addVertex(vertex);
        index = index;
        data.mesh->vertices[index].id = index;
        cgfassert(index == i);
      }

      /*Face*/
      for(int i=0;i<data.n_faces;i++){
        DCELHalfFace<T> hf;
        int hf_index = data.mesh->addHalfFace(hf);

        int edgeList[3];

        for(int j=0;j<3;j++){
          int index = vertex_map[data.faces[i*3+j]];
          DCELHalfEdge<> he(index, hf_index, Mesh::UndefinedIndex,
                            Mesh::UndefinedIndex, Mesh::UndefinedIndex);
          index = data.mesh->addHalfEdge(he);
          edgeList[j] = index;
        }

        for(int j=0;j<3;j++){
          int index = edgeList[j];
          int next;
          if(j == 2){
            next = edgeList[0];
          }else{
            next = edgeList[j+1];
          }
          data.mesh->halfEdges[index].next = next;
          data.mesh->halfEdges[index].half_face = hf_index;
          data.mesh->vertices[data.mesh->halfEdges[index].origin].leaving =
            index;
          data.mesh->halfFaces[hf_index].half_edge = index;
        }
      }

      delete[] vertex_map;

      data.mesh->connectHalfEdges();

      if(operation & PLY_FILL_GAPS){
        data.mesh->DCELFillGaps();
      }
      if(operation & PLY_SIMPLIFY){
        data.mesh->simplify();
      }
      if(operation & PLY_TESSELATE){
        data.mesh->tesselate();
      }

      message("%d vertices, %d faces", data.n_vertices, data.n_faces);

    }else{
      message("Error opening file %s", filename);
    }

    delete[] data.vertices;
    delete[] data.faces;

    return data.mesh;
  }

  template<class T>
  void save_ply(const char* filename, DCTetraMesh<T>* mesh){
    std::ofstream file(filename, std::ios::out);

    if(file.is_open()){
      file << "ply" << std::endl;
      file << "format ascii 1.0" << std::endl;
      file << "comment CGF output" << std::endl;
      file << "element vertex " << mesh->n_vertices << std::endl;
      file << "property float x" << std::endl;
      file << "property float y" << std::endl;
      file << "property float z" << std::endl;

      file << "element face " << mesh->n_halfFaces << std::endl;
      file << "property list uchar int vertex_indices" << std::endl;
      file << "end_header" << std::endl;

      for(int i=0;i<mesh->n_vertices;i++){
        file << mesh->vertices[i].coord[0] << " "
             << mesh->vertices[i].coord[1] << " "
             << mesh->vertices[i].coord[2] << std::endl;
      }
      for(int i=0;i<mesh->n_halfFaces;i++){
        mesh->setCurrentEdge(mesh->halfFaces[i].half_edge);
        file << "3 ";
        file << mesh->getOriginVertex() << " ";
        mesh->nextEdge();
        file << mesh->getOriginVertex() << " ";
        mesh->nextEdge();
        file << mesh->getOriginVertex() << std::endl;
      }
    }
  }

  template
  DCTetraMesh<float>* load_ply<float>(const char* filename, int operation);

  template
  DCTetraMesh<double>* load_ply<double>(const char* filename, int operation);

  template
  void save_ply<float>(const char* filename, DCTetraMesh<float>* mesh);

  template
  void save_ply<double>(const char* filename, DCTetraMesh<double>* mesh);
}
