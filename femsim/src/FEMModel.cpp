/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "FEMModel.hpp"

namespace CGF{
  template<class T>
  FEMModel<T>::FEMModel(DCTetraMesh<T>* _mesh,
                        int _nElements):mesh(_mesh),
                                        nElements(_nElements){
  }

  template<class T>
  FEMModel<T>::~FEMModel(){
  }

  template<class T>
  void FEMModel<T>::setMu(T _mu){
    mu = _mu;
  }

  template<class T>
  void FEMModel<T>::setE(T _E){
    E = _E;
  }

  template class FEMModel<float>;
  template class FEMModel<double>;
}
