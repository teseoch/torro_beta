/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintSetEdge.hpp"
#include "math/ConstraintSet.hpp"
#include "math/ConstraintCandidateEdge.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

namespace CGF{

  template<class T>
  ConstraintSetEdge<T>::ConstraintSetEdge(){
    backFace = false;
  }

  template<class T>
  ConstraintSetEdge<T>::~ConstraintSetEdge(){
    CandidatePtrMapIterator it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = (*it++).getData();
      delete c;
    }
    candidates.clear();
  }

  template<class T>
  void ConstraintSetEdge<T>::setBackFace(){
    backFace = true;
  }

  template<class T>
  void ConstraintSetEdge<T>::deactivateAll(CollisionContext<T>* ctx){
    CandidatePtrMapIterator it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = (*it++).getData();
      if(c->status == Enabled){
        c->disableConstraint(this, ctx, true);
      }
    }
  }

  template<class T>
  bool ConstraintSetEdge<T>::evaluate(Edge<T>& p,
                                      Vector4<T>& com,
                                      Quaternion<T>& rot,
                                      CollisionContext<T>* ctx,
                                      const EvalType& etype,
                                      EvalStats& stats){
    CandidatePtrMapIterator it = candidates.begin();

    Tree<Candidate*> changedCandidates;
    Tree<Candidate*> changedIndices;

    bool evaluateGeometry    = etype.geometryCheck();
    bool evaluateConstraints = etype.existingCheck();

    /*if true, the solver is notified*/
    bool constraintsChanged  = false;

    bool debug = false;

    if(edgeId == 60465 || edgeId == 26385){
      debug = true;
    }

    debug = false;

    if(evaluateGeometry){
      /*Simple vertex-plane check for a fast collision detection*/

      /*Check if edge is collapsed*/
      bool collapsed = edgeCollapsed(startPosition, p);

      if(collapsed){
        DBG(warning("COLLAPSED EDGE %d, backface = %d", edgeId, backFace));
      }else if(debug){
        warning("EDGE NOT COLLAPSED %d, backface = %d", edgeId, backFace);
        message("s - p");
        std::cout << startPosition << std::endl;
        std::cout << p << std::endl;

        edgeCollapsed(startPosition, p, true);

      }

      if(backFace){
        ctx->collapsedBackEdges[edgeId] = collapsed;
      }else{
        ctx->collapsedEdges[edgeId] = collapsed;
      }

      if(collapsed){
        if(startPosition.isConvex() && p.isConcave()){
          if(!backFace){
            ctx->backSets[startPosition.faceVertexId(0)].forceConstraint(ctx, startPosition.faceId(1));
            ctx->backSets[startPosition.faceVertexId(1)].forceConstraint(ctx, startPosition.faceId(0));
          }else{
            ctx->sets[startPosition.faceVertexId(0)].forceConstraint(ctx, startPosition.faceId(1));
            ctx->sets[startPosition.faceVertexId(1)].forceConstraint(ctx, startPosition.faceId(0));
          }
        }
      }

      while(it != candidates.end()){
        Candidate* c = (*it++).getData();

#if 0 /*New edge class is always valid, however, it distinguishes
        between collided and non collided states*/
        /*Allow to re-evaluate validity*/
        //c->valid = true;

        if(!c->valid){
          /*The current edge-edge combination is marked as invalid,
            probably because its initial configuration was invalid or
            unlikely to collide. However, the edges may have moved
            into a configuration that is valid now.*/

#if 1
          c->updateGeometry(ctx);
          bool validc = false;
          /*recheck validity*/
          validc = (c->eu.validConfiguration(p, &c->referencedvEP) &&
                    p.validConfiguration(c->eu, &c->referencedvPE) );

          stats.n_geometry_check++;

          if(validc){
            c->valid = true;
          }else{
            c->valid = false;
            continue;
          }
#else
          continue;
#endif
        }

#endif

        if(c->status == Enabled){
          /*Check if the constraint is in a deactivated state, if so,
            remove constraint*/

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(c->enabledId);

          if(ccc->status == Inactive){
#ifdef ACTIVE_CONSTRAINTS
            //c->disableConstraint(this, ctx);
            //#error determine if a constraint can be removed
#else

#endif
          }
        }

        /*Update geometry information of associated edges*/
        try{
          c->updateGeometry(ctx);
#if 0
          if(!(p.validConfiguration(c->eu, &c->referencedvPE) &&
               c->eu.validConfiguration(p, &c->referencedvEP))){
            c->valid = false;
            continue;
          }
#endif
        }catch(Exception* e){
          warning("Degenerate edge found 1 %s", e->getError().c_str());
          delete e;

          if(c->status == Enabled){
            c->disableConstraint(this, ctx, true);
          }

          continue;
        }

        /*Evaluate distance of current configuration, if distance
          becomes negative there is a collision*/
        if(c->evaluate(this, ctx, p, com, rot)){
          /*Changed in sign, inspect further*/
          changedCandidates.insert(c, 1);
        }
      }

      /*Perform root-finding for finding collision-----------*/
      CandidatePtrTreeIterator it2 = changedCandidates.begin();

#if 1
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

#ifdef ACTIVE_CONSTRAINTS
        if(c->status == Enabled){
          //continue;
        }
#else
        if(c->status == Enabled){
          continue;
        }
#endif
        T dist = 10000;
        if(c->checkCollision(p, com, rot, &dist)){
          if(c->enableConstraint(this, ctx, startPosition, p)){
            constraintsChanged = true;

            changedIndices.insert(c, 1);
          }
        }
      }
#else
      Candidate* closestCandidate = 0;
      T closestRoot = 10000000.0;

      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

#ifdef ACTIVE_CONSTRAINTS
#else
        if(c->status == Enabled){
          continue;
        }
#endif
        T root = 0;
        if(c->checkCollision(p, com, rot, &root)){
          if(root < closestRoot){
            root = closestRoot;
            closestCandidate = c;
          }
        }

      }

      if(closestCandidate){
        if(closestCandidate->enableConstraint(this, ctx, startPosition, p)){
          constraintsChanged = true;

          changedIndices.insert(closestCandidate, 1);
        }
      }
#endif
      /*-----------------------------------------------------*/
    }

    /*Evaluate all enabled constraints---------------------*/

    /*Evaluate all enabled constraints (except for new constraints)
      and perform some sub-constraint operations (friction)*/

    it = candidates.begin();

    while(it != candidates.end() && evaluateConstraints){
      Candidate* cc = (*it++).getData();

      if(changedIndices.findIndex(cc) == -1){
        if(cc->status == Enabled){

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(cc->enabledId);

          bool active = false;

          bool changed = ccc->evaluate(*ctx->getSolver()->getx(),
                                       *ctx->getSolver()->getb(),
                                       0, etype, stats, &active,
                                       ctx->getSolver()->getb2());

          if(changed && active && etype.penetrationCheck()){
            /*If a constraint has been changed, but still active,
              update preconditioner*/

            ccc->computePreconditioner(*ccc->preconditioner);
            constraintsChanged = true;
          }

          /*Disable (remove) constraint if allowed*/
#ifdef ACTIVE_CONSTRAINTS
          /*Keep constraint*/
#else
          if(active == false && etype.penetrationCheck()){
            constraintsChanged = true;
            message("not active");
            cc->disableConstraint(this, ctx);
          }
#endif

          /*Notifies solver*/
          if(changed){
            constraintsChanged = true;
          }
        }
      }else{
        //Evaluate new constraint in order to update RHS
        if(cc->status == Enabled){

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(cc->enabledId);

          EvalType etype2;
          bool active = false;

          ccc->evaluate(*ctx->getSolver()->getx(),
                        *ctx->getSolver()->getb(),
                        0, etype2, stats, &active,
                        ctx->getSolver()->getb2());
        }
      }
    }

    /*-----------------------------------------------------*/

    if(evaluateGeometry){
      lastPosition = p;
      lastCenterOfMass = com;
      lastOrientation = rot;
    }

    return constraintsChanged;
  }

  template<class T>
  bool ConstraintSetEdge<T>::checkAndUpdate(Edge<T>& p,
                                            Vector4<T>& com,
                                            Quaternion<T>& rot,
                                            CollisionContext<T>* ctx,
                                            EvalStats& stats,
                                            bool friction,
                                            bool force, T bnorm){
    CandidatePtrMapIterator it = candidates.begin();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;
    while(it != candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->status != Enabled){
        continue;
      }

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(c->enabledId);

#ifndef ACTIVE_CONSTRAINTS
      if(cc->status != Active){
        //continue;
      }
#endif

      /*Check corresponding lambda*/
      if(!cc->valid(*ctx->getSolver()->getx())){
        DBG(message("CONSTRAINT NOT VALID"));
        //continue;
      }

      /*Update geometry information of associated edges*/
      try{
        c->updateGeometry(ctx);
      }catch(Exception* e){
        cc->status = Inactive;
        warning("Degenerate edge found 2 %s", e->getError().c_str());
        delete e;

        c->disableConstraint(this, ctx, true);

        continue;
      }

      /*Update value for distancec*/
      Vector4<T> b1, b2;
      bool distanceUpdated = false;

      c->evaluate(this, ctx, p, com, rot, &b1, &b2, &distanceUpdated);

      /*If distanceUpdated == false, the edge-edge pair is invalid*/

      if(distanceUpdated == false){
        constraintsChanged = true;
        message("distance not updated due to exception");
        getchar();
        //c->disableConstraint(this, ctx, true);
        continue;
      }

      /*Check geometric distance and state of the constraint*/
      if(true){
#if 0
      if(Abs(c->distancec - (T)DISTANCE_EPSILON) >
         (T)(DISTANCE_TOL*(T)0.0 /** bnorm + DISTANCE_TOL*/ )/*&&
         //if(c->distancec < 1e-6 &&
         c->eu.barycentricOnEdge(b1, (T)BARY_EPS) &&
         c->eu.barycentricOnEdge(b2, (T)BARY_EPS)*/){

        /*Check if current distances between the edges are ok.  At
          convergence, the geometry should be collision free and a
          certain safety distance must be respected.

         */
#endif
#if 1
        (warning("updating EE constraint, distance = %10.10e, %10.10e, %10.10e",
                 c->distancec, c->distancec - DISTANCE_EPSILON,
                 c->distancec - DISTANCE_TOL));
#ifdef ACTIVE_CONSTRAINTS
#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > SAFETY_DISTANCE){
            /*Constraint is not active, distance is positive*/
            continue;
          }
        }else{
          //if(c->distancec - (T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
          if(c->distancec < (T)(DISTANCE_EPSILON + DISTANCE_TOL) &&
             c->distancec > (T)DISTANCE_TOL){
            /*Constraint is active, distance is positive*/
            continue;
          }
        }
#else
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > DISTANCE_EPSILON * 2.0){
            DBG(message("skip: distancec > 0"));
            /*Constraint is not active, distance is positive.*/
            c->disableConstraint(this, ctx, true);
            continue;
          }
        }else{

        }
#endif
#else
        //if(false){
        if(cc->status != Active){
          DBG(message("!ACTIVE"));
          if(c->distancec > DISTANCE_EPSILON * 2.0){
            DBG(message("skip: distancec > 0"));
            /*Constraint is not active, distance is positive.*/
            c->disableConstraint(this, ctx, true);
            continue;
          }

          if(c->distancec > DISTANCE_EPSILON * 0.1){
            DBG(message("skip: distancec > 0"));
            /*Constraint is not active, distance is positive.*/
            continue;
          }
        }else{
          if(c->distancec - (T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
            /*Constraint is active, distance is positive.*/
            //continue;
          }
          if(c->distancec - (T)DISTANCE_EPSILON - (T)5*(T)DISTANCE_TOL > (T)0.0){
            /*Constraint is active, distance is positive.*/
            //continue;
          }
          if(Abs(c->distancec - (T)DISTANCE_EPSILON) < (T)DISTANCE_TOL){
            //continue;
          }
        }

#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          DBG(message("not valid"));
          if(c->distancec > SAFETY_DISTANCE){
            /*Constraint is not active or multiplier is very small,
              distance is positive*/
            DBG(message("skip: distancec > 0"));
            continue;
          }
        }else{
          //if(c->distancec - (T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
          //  continue;
          //}
          if(Abs(c->distancec - (T)DISTANCE_EPSILON) < (T)DISTANCE_TOL){
            continue;
          }
        }

#if 0
        {
          Vector4<T> weights1, weights2;
          c->eu.infiniteProjections(p, 0, 0, &weights2, &weights1, 0);
          if(!c->eu.barycentricOnEdge(weights1, (T)BARY_EPS) ||
             !p.barycentricOnEdge(weights2, (T)BARY_EPS)){
            /*Could be a source of potential errors!!*/
            c->disableConstraint(this, ctx, true);
            constraintsChanged = true;
            continue;
          }
        }
#endif
#endif
#endif
#endif
        /*Geometric distance and constraint distance are not in sync, update*/

        /*Update constraint does check validity!*/
        bool skipped = false;
        try{
          //c->updateConstraint(this, ctx, startPosition, p, com, rot, &skipped);

#ifdef ACTIVE_CONSTRAINTS
          if(!cc->valid(*ctx->getSolver()->getx())){
            if(c->distancec > DISTANCE_EPSILON * 1.0){
              continue;
            }
          }else{
            if(c->distancec - (T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
              /*Constraint is active, distance is positive.*/
              continue;
            }
            if(c->distancec - (T)DISTANCE_EPSILON - (T)5*(T)DISTANCE_TOL > (T)0.0){
              /*Constraint is active, distance is positive.*/
              continue;
            }
          }
#else
          if(cc->status != Active){
            if(c->distancec > DISTANCE_EPSILON * 1.0){
              continue;
            }
          }

          if(cc->status == Active){
            if(c->distancec - (T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
              /*Constraint is active, distance is positive.*/
              continue;
            }
            if(c->distancec - (T)DISTANCE_EPSILON - (T)5*(T)DISTANCE_TOL > (T)0.0){
              /*Constraint is active, distance is positive.*/
              continue;
            }
          }
#endif

          c->updateConstraint(this, ctx, startPosition, p, com, rot, &skipped,
                              stats);

          if(skipped){
            /*But trigger a new update*/
            //constraintsChanged = true;
            continue;
          }

          if(cc->status != Active){
            cc->status = Active;
            cc->frictionStatus[0] = Static;
            cc->frictionStatus[1] = Static;
          }

          message("Updated and enabled EE constraint");
        }catch(DegenerateCaseException* e){
          message("disabled due to exception in updateConstraint");
          c->disableConstraint(this, ctx, true);
          std::cerr << e->getError();
          //error("Exception occured while updating constraint");

          delete e;
        }

        constraintsChanged = true;

        DBG(message("EDGE_EDGE CONSTRAINT NOT CORRECT %d, %d\n\n\n", edgeId, c->edgeId));
        DBG(message("real distance = %10.10e", c->distancec + DISTANCE_EPS3));
        (warning("EDGE_EDGE CONSTRAINT NOT CORRECT %d, %d, backface = %d\n\n\n", edgeId, c->edgeId, backFace));
        (warning("real distance = %10.10e", c->distancec + DISTANCE_EPS3));
      }
    }

    it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->status == Enabled){
        continue;
      }

      //ContactConstraint<2, T>* cc =
      //(ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(c->enabledId);

      //c->updateInactiveConstraint(this, ctx, startPosition, p, com, rot);
    }

    return constraintsChanged;
  }

  template<class T>
  bool ConstraintSetEdge<T>::updateInitial(Edge<T>& p,
                                           Vector4<T>& com,
                                           Quaternion<T>& rot,
                                           CollisionContext<T>* ctx,
                                           EvalStats& stats,
                                           bool friction,
                                           bool force, T bnorm){
    CandidatePtrMapIterator it = candidates.begin();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;
    while(it != candidates.end()){
      Candidate* c = (*it++).getData();

#if 0
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(c->enabledId);

      /*Update geometry information of associated edges*/
      try{
        c->updateGeometry(ctx);
      }catch(Exception* e){
        cc->status = Inactive;
        warning("Degenerate edge found 2 %s", e->getError().c_str());
        delete e;

        c->disableConstraint(this, ctx, true);

        continue;
      }
#endif

      startPosition = p;
      startCenterOfMass = com;
      startOrientation = rot;

      bool skipped = false;
      c->updateInitial(this, ctx, startPosition, p, com, rot, &skipped, stats);

      if(skipped){
        constraintsChanged = true;
      }
    }

    return constraintsChanged;
  }

  template<class T>
  void ConstraintSetEdge<T>::forceConstraint(CollisionContext<T>* ctx,
                                             int _otherEdgeId){
    CandidatePtrMapIterator it = candidates.begin();

    warning("forcing edge %d, edge %d, backface %d",
            edgeId, _otherEdgeId, backFace);

    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(candidate->edgeId == _otherEdgeId){
        candidate->forceConstraint();
        return;
      }
    }

    warning("request for forcing a constraint for which no candidate exists!! Added to hotlist.");
    forcedPotentials.uniqueInsert(_otherEdgeId, _otherEdgeId);

    incrementalUpdate(ctx, true);

    it = candidates.begin();
    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(candidate->edgeId == _otherEdgeId){
        candidate->forceConstraint();
        return;
      }
    }
  }


  template<class T>
    void ConstraintSetEdge<T>::incrementalUpdate(CollisionContext<T>* ctx,
                                                 bool onlyForced){
    Edge<T> x;
    ctx->mesh->getEdge(&x, edgeId, backFace);

    Vector4<T> com(0,0,0,0);
    Quaternion<T> rot(0,0,0,1);

    if(Mesh::isRigid(type)){
      ctx->mesh->setCurrentEdge(edgeId);
      int objectId = ctx->mesh->vertexObjectMap[ctx->mesh->getOriginVertex()];
      com = ctx->mesh->centerOfMass[objectId];
      rot = ctx->mesh->orientations[objectId];
    }

    List<int> currentPotentials;  /*Stores face ids of found potentials*/
    if(!onlyForced){
      ctx->extractPotentialEdgeEdges(edgeId, currentPotentials, backFace);
    }

    /*Set visited flag*/
    List<int>::Iterator lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentEdge = *lit++;
      ctx->mesh->halfEdges[currentEdge].visited = true;
    }

    /*Add forced candidates*/
    Tree<int>::Iterator forcedIt = forcedPotentials.begin();
    while(forcedIt != forcedPotentials.end()){
      int forcedPotential = *forcedIt++;
      if(ctx->mesh->halfEdges[forcedPotential].visited == false){
        //currentPotentials.uniqueInsert(forcedPotential, forcedPotential);
        currentPotentials.append(forcedPotential);
        ctx->mesh->halfEdges[forcedPotential].visited = true;
      }
    }

    /*Reset visited flag*/
    lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentEdge = *lit++;
      ctx->mesh->halfEdges[currentEdge].visited = false;
    }

    forcedPotentials.clear();


    cgfassert(candidatesTree.size() == candidates.size());

    /*Add new candidates-----------------------------------*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;

      ctx->mesh->setCurrentEdge(candidate);
      int twinCandidate = ctx->mesh->getTwinEdge();

      if(twinCandidate < candidate){
        continue;
      }

      int index = candidatesTree.findIndex(candidate);

      if(index == -1){
        if(backFace){
          if(Mesh::isRigidOrStatic(ctx->mesh->halfEdges[candidate].type)){
            /*A rigid object can have internal collisions, hence Rigid*/
            continue;
          }

          int faceId = ctx->mesh->halfEdges[edgeId].half_face;
          int candidateFaceId = ctx->mesh->halfEdges[candidate].half_face;

          if(!ctx->stree->facesShareSameRoot(faceId, candidateFaceId)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        Candidate* cc = new Candidate(candidate, edgeId, backFace);

        ctx->mesh->setCurrentEdge(candidate);
        int edgeVertexId = ctx->mesh->getOriginVertex();

        cc->ownerEdgeType = type;
        cc->thisEdgeType  = ctx->mesh->vertices[edgeVertexId].type;

        candidates.insert(candidate, cc);
        candidatesTree.insert(candidate, 1);

        //ctx->mesh->getEdgeIndices(candidate, cc->indices);

        try{
          cc->recompute(this, ctx, x, com, rot);

#if 0
          validc = (cc->e0.validConfiguration(x, &cc->referencedvEP) &&
                    x.validConfiguration(cc->e0, &cc->referencedvPE) );
#endif
        }catch(DegenerateCaseException* e){
          std::cerr << e->getError();
          delete e;
        }
      }
    }
  }

  template<class T>
  bool ConstraintSetEdge<T>::checkForValidEdges(Edge<T>& p,
                                                Vector4<T>& com,
                                                Quaternion<T>& rot,
                                                CollisionContext<T>* ctx){
    /*Recheck for invalid pairs that became valid*/
    bool changed = false;

    return changed;
  }

  template<class T>
  void ConstraintSetEdge<T>::update(CollisionContext<T>* ctx){
    Edge<T> x;
    ctx->mesh->getEdge(&x, edgeId, backFace);

    Vector4<T> com(0,0,0,0);
    Quaternion<T> rot(0,0,0,1);

    if(Mesh::isRigid(type)){
      ctx->mesh->setCurrentEdge(edgeId);
      int objectId = ctx->mesh->vertexObjectMap[ctx->mesh->getOriginVertex()];
      com = ctx->mesh->centerOfMass[objectId];
      rot = ctx->mesh->orientations[objectId];
    }

    /*Clear forced potentials*/
    forcedPotentials.clear();

    //Tree<int> currentPotentials;  /*Stores face ids of found potentials*/
    List<int> currentPotentials;
    ctx->extractPotentialEdgeEdges(edgeId, currentPotentials, backFace);

    cgfassert(candidatesTree.size() == candidates.size());

    /*Add new candidates-----------------------------------*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;

      ctx->mesh->setCurrentEdge(candidate);
      int twinCandidate = ctx->mesh->getTwinEdge();

      if(twinCandidate < candidate){
        continue;
      }

      int index = candidatesTree.findIndex(candidate);

      if(index == -1){
        if(backFace){
          if(Mesh::isRigidOrStatic(ctx->mesh->halfEdges[candidate].type)){
            /*A rigid object can have internal collisions, hence Rigid*/
            continue;
          }

          int faceId = ctx->mesh->halfEdges[edgeId].half_face;
          int candidateFaceId = ctx->mesh->halfEdges[candidate].half_face;

          if(!ctx->stree->facesShareSameRoot(faceId, candidateFaceId)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        Candidate* cc = new Candidate(candidate, edgeId, backFace);

        ctx->mesh->setCurrentEdge(candidate);
        int edgeVertexId = ctx->mesh->getOriginVertex();

        cc->ownerEdgeType = type;
        cc->thisEdgeType  = ctx->mesh->vertices[edgeVertexId].type;

        candidates.insert(candidate, cc);
        candidatesTree.insert(candidate, 1);

        //ctx->mesh->getEdgeIndices(candidate, cc->indices);
      }
    }

    /*-----------------------------------------------------*/

    cgfassert(candidatesTree.size() == candidates.size());

    /*Check for disapeared potential collisions -> remove them from
      the set.*/

    CandidatePtrMapIterator cit2 = candidates.begin();

    while(cit2 != candidates.end()){
      Candidate* cc = (*cit2++).getData();
      cc->flag = false;
    }

    it = currentPotentials.begin();
    while(it != currentPotentials.end()){
      CandidatePtrMapIterator mit = candidates.find(*it++);
      if(mit != candidates.end()){
        (*mit).getData()->flag = true;
      }
    }

    cit2 = candidates.begin();
    while(cit2 != candidates.end()){
      Candidate* cc = (*cit2).getData();

      if(cc->flag == false){
        /*Edge disappeared, disable and remove*/

        /*This can only happen if the constraint is not active*/
        if(cc->status == Enabled){
          cit2++;
        }else{
          //message("disableConstraint due to disappeared combination");
          cc->disableConstraint(this, ctx, true);

          candidates.remove(cit2);
          candidatesTree.remove(cc->edgeId);

          delete cc;
        }
      }else{
        cit2++;
      }
    }

    /*-----------------------------------------------------*/

    cgfassert(candidatesTree.size() == candidates.size());

    /*Recompute normals and update existing constraints----*/
    CandidatePtrMapIterator it3 = candidates.begin();
    while(it3 != candidates.end()){
      Candidate* s = (*it3++).getData();

      try{
        /*Also sets the reference vectors such that the initial
          configuration yields a positive signed distance.*/
        s->recompute(this, ctx, x, com, rot);
#if 0
        validc = (s->e0.validConfiguration(x, &s->referencedvEP) &&
                  x.validConfiguration(s->e0, &s->referencedvPE) );
#endif
      }catch(DegenerateCaseException* e){
        std::cerr << e->getError();
        delete e;
      }
    }

    /*-----------------------------------------------------*/

    startPosition = x;
    startCenterOfMass = com;
    startOrientation = rot;
    lastPosition = x;
    lastCenterOfMass = com;
    lastOrientation = rot;
  }

  template<class T>
  void ConstraintSetEdge<T>::showEdgeCandidateState(int edge,
                                                    CollisionContext<T>* ctx){
    message("Show edge set %d", edgeId);
    message("vertex ids = %d, %d", vertexId[0], vertexId[1]);
    message("face ids = %d, %d", faceId[0], faceId[1]);

    message("Start position, com, orientation");

    std::cout << startPosition << std::endl;
    std::cout << startCenterOfMass << std::endl;
    std::cout << startOrientation << std::endl;

    message("Updated position, com, orientation");

    std::cout << lastPosition << std::endl;
    std::cout << lastCenterOfMass << std::endl;
    std::cout << lastOrientation << std::endl;

    message("set has %d candidates", candidates.size());

    CandidatePtrMapIterator it = candidates.begin();
    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(edge == -1){
        candidate->showCandidateState(lastPosition, ctx);
      }else if(candidate->edgeId == edge){
        candidate->showCandidateState(lastPosition, ctx);
      }
    }
  }

  template<class T>
  void ConstraintSetEdge<T>::updateStartPositions(const Edge<T>& edge,
                                                  const Vector4<T>& com,
                                                  const Quaternion<T>& rot,
                                                  Candidate* c){
    startPosition = edge;
    startCenterOfMass = com;
    startOrientation = rot;

    c->updateInitialState(edge, com, rot);
#if 1
    CandidatePtrMapIterator it = candidates.begin();
    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();
      candidate->updateInitialState(edge, com, rot);
    }
#endif
    warning("UPDATING INITIAL STATE EDGE !!!\n\n\n\n\n\n\n\n\n\n\n\n\n");
    //exit(0);
  }

  template class ConstraintSetEdge<float>;
  template class ConstraintSetEdge<double>;
}
