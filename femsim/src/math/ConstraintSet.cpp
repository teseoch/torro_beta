/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintSet.hpp"
#include "math/ConstraintCandidateFace.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

#define DISTANCE_EPS SAFETY_DISTANCE
//#define MU 0.5

namespace CGF{
  //#define EPS (7E-6*1)
  //#define EPS (1E-6*1)
#define EPS DISTANCE_EPSILON

  template<class T>
  ConstraintSet<T>::ConstraintSet(){
    backFace = false;
  }

  template<class T>
  ConstraintSet<T>::~ConstraintSet(){
    CandidateMapIterator it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = (*it++).getData();
      delete c;
    }
    candidates.clear();
  }

  template<class T>
  void ConstraintSet<T>::setBackFace(){
    backFace = true;
  }

  template<class T>
  void ConstraintSet<T>::deactivateAll(CollisionContext<T>* ctx){
    CandidateMapIterator it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = (*it++).getData();
      if(c->status == Enabled){
        c->disableConstraint(this, ctx, true);
      }
    }
  }

  /*Evaluate all candidates and constraints in this set given the
    provided EvalType value.*/
  template<class T>
  bool ConstraintSet<T>::evaluate(OrientedVertex<T>& p, Vector4<T>& com,
                                  Quaternion<T>& rot,
                                  CollisionContext<T>* ctx,
                                  const EvalType& etype, EvalStats& stats){
    CandidateMapIterator it = candidates.begin();

    Tree<Candidate*> changedCandidates;
    Tree<Candidate*> changedIndices;

    bool evaluateGeometry    = etype.geometryCheck();
    bool evaluateConstraints = etype.existingCheck();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    if(evaluateGeometry){
      /*Perform a simple plane vertex test for all candidates*/

      while(it != candidates.end()){
        Candidate* c = (*it++).getData();

        if(!c->valid){
          if(c->status == Enabled){
            /*An invalid candidate with an enabled constraint is not
              possible*/
            error("Invalid candidate is still enabled");
          }

          /*A vertex-face pair can only be invalid if:

            1) The rootfinding method had thrown an exception during
            the fine collision check. In this case the distance
            between the initial and current pair do not change, hence
            there is no intersection.

            2) The initial triangle is degenerate.

            If this function is called after a solve, case 1) can be
            resolved due to the update. If the initial triangle is
            valid, it is safe to mark the face as valid and to the
            collision check again.
          */

          try{
            ctx->mesh->getTriangle(&c->t0, c->faceId, backFace);
            /*No exception -> pair is valid and can be used in the
              collision test.*/
            c->valid = true;
          }catch(Exception* e){
            delete e;
            c->valid = false;
          }
          if(!c->valid){
            /*Still not valid, skip*/
            continue;
          }
        }

        if(IsNan(p.getVertex()[0])){
          warning("p is nan");
          std::cout << p.getVertex() << std::endl;
        }

        if(c->status == Enabled){
          /*Check if the constraint is in a deactivated state, if so,
            remove constraint*/

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(c->enabledId);

          if(ccc->status == Inactive){
#ifdef ACTIVE_CONSTRAINTS
            //c->disableConstraint(this, ctx);
            //#error determine if a constraint can be removed
#else

#endif
          }
        }

        /*Update geometry information of associated faces*/
        try{
          c->updateGeometry(ctx, this, p, startPosition);
          stats.n_geometry_check++;
        }catch(Exception* e){
          warning("Exception updateGeometry 2 %s", e->getError().c_str());
          delete e;

          /*Update of geometry failed -> invalid candidate. Disable
            associated constraint, if available.*/
          c->valid = false;
          if(c->status == Enabled){
            AbstractRowConstraint<2, T>* ccc =
              (AbstractRowConstraint<2, T>*)
              ctx->getMatrix()->getConstraint(c->enabledId);
            ccc->status = Inactive;
            c->disableConstraint(this, ctx, true);
          }
          continue;
        }

        /*Evaluate distance of current configuration, if distance
          becomes negative there is a collision*/

        /*Evaluate signed distance of pair*/
        if(c->evaluate(this, ctx, p, com, rot)){
          /*Changed in sign, inspect further*/
          changedCandidates.insert(c, 1);
        }
      }

      /*Perform root-finding for finding collision-----------*/
      CandidateTreeIterator it2 = changedCandidates.begin();

      /*Find closest colliding constraint*/

      //ConstraintCandidateFace<T>* closest = 0;
      //T closestDistance = (T)10000.0;

#if 1
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

#ifdef ACTIVE_CONSTRAINTS
        if(c->status == Enabled){
          //continue;
        }
#else
        if(c->status == Enabled){
          continue;
        }
#endif
        if(c->checkCollision(p, com, rot)){
          if(c->status != Enabled){
            if(c->enableConstraint(this, ctx, startPosition, p)){
              constraintsChanged = true;
              changedIndices.insert(c, 1);
            }
          }
        }
      }
#else
      /*Figure out the ´closest´ intersection point. This basically
        turns out to be the closest root.*/
      Candidate* closestCandidate = 0;
      T closestRoot = (T)100000.0;
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

#ifdef ACTIVE_CONSTRAINTS

#else
        if(c->status == Enabled){
          continue;
        }
#endif
        T root = (T)0.0;
        if(c->checkCollision(p, com, rot, &root)){
          if(c->status != Enabled){
            if(root < closestRoot){
              root = closestRoot;
              closestCandidate = c;
            }
          }
        }
      }
      if(closestCandidate){
        if(closestCandidate->enableConstraint(this, ctx, startPosition, p)){
          constraintsChanged = true;
          changedIndices.insert(closestCandidate, 1);
        }
      }
      /*Select and enable the candidate with the closest intersectionpoint*/
#endif
      /*-----------------------------------------------------*/
    }

    /*Evaluate all enabled constraints---------------------*/

    /*Evaluate all enabled constraints (except for new constraints)
      and perform some sub-constraint operations (friction)*/

    it = candidates.begin();

    while(it != candidates.end() && evaluateConstraints){
      Candidate* cc = (*it++).getData();

      if(changedIndices.findIndex(cc) == -1){
        if(cc->status == Enabled){

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(cc->enabledId);

          bool active = false;

          bool changed = ccc->evaluate(*ctx->getSolver()->getx(),
                                       *ctx->getSolver()->getb(),
                                       0, etype, stats, &active,
                                       ctx->getSolver()->getb2());

          if(changed && active && etype.penetrationCheck()){
            /*If a constraint has been changed, but still active,
              update preconditioner*/

            ccc->computePreconditioner(*ccc->preconditioner);
            constraintsChanged = true;
          }

          /*Disable (remove) constraint if allowed*/
#ifdef ACTIVE_CONSTRAINTS
          /*Keep constraint*/
#else
          if(active == false && etype.penetrationCheck()){
            constraintsChanged = true;
            cc->disableConstraint(this, ctx, true);
          }
#endif

          /*Notifies solver*/
          if(changed){
            constraintsChanged = true;
          }
        }
      }else{
        //Evaluate new constraint in order to update RHS
        if(cc->status == Enabled){

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(cc->enabledId);

          bool active = false;
          EvalType etype2;

          ccc->evaluate(*ctx->getSolver()->getx(),
                        *ctx->getSolver()->getb(),
                        0, etype2, stats, &active,
                        ctx->getSolver()->getb2());
        }
      }
    }

    if(evaluateGeometry){
      lastPosition     = p; /*Store last position*/
      lastCenterOfMass = com;
      lastOrientation  = rot;
    }
    return constraintsChanged;
  }

  template<class T>
  void ConstraintSet<T>::forceConstraint(CollisionContext<T>* ctx,
                                         int _faceId){
    CandidateMapIterator it = candidates.begin();

    (warning("forcing vertex %d, face %d, backface %d",
             vertexId, _faceId, backFace));

    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(candidate->faceId == _faceId){
        candidate->forceConstraint();
        return;
      }
    }

    warning("request for forcing a constraint for which no candidate exists!! Added to hotlist.");
    forcedPotentials.uniqueInsert(_faceId, _faceId);

    incrementalUpdate(ctx, true);

    it = candidates.begin();
    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(candidate->faceId == _faceId){
        candidate->forceConstraint();
        return;
      }
    }
  }


  /*Checks the current state of the constraint using the actual
    geometric information. Since the constraint in the solver is
    linearized at x0, there will be a deviation between the measured
    distance in the solver and the actual geometric distance. In the
    worst case, the collision will be resolved according the solver,
    but might still be present in the actual geometric
    representation.

    This check measures the actual geometric distance. If the
    correcponding constraint is active, the actual distance should
    should be close to EPS +/- DISTANCE_TOL. If not active, the
    distance must be positive.

    If the actual distance at convergence is not within this range,
    the constraint must be updated (linearized again) given the
    current positions. If the contact normal is correct, only the
    constraint distance is updated.*/

  template<class T>
  bool ConstraintSet<T>::checkAndUpdate(OrientedVertex<T>& p, Vector4<T>& com,
                                        Quaternion<T>& rot,
                                        CollisionContext<T>* ctx,
                                        EvalStats& stats,
                                        bool friction, bool force, T bnorm){
    CandidateMapIterator it = candidates.begin();

    //T b2Norm = ctx->getSolver()->getB2Norm()/(T)2.0;

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    /*Simple plane point check for a fast detection in change*/
    while(it != candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->status != Enabled){
        continue;
      }

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(c->enabledId);

#ifndef ACTIVE_CONSTRAINTS
      if(cc->status != Active){
        //continue;
      }
#endif

      /*Check corresponding lambda*/
      if(!cc->valid(*ctx->getSolver()->getx())){
        DBG(message("CONSTRAINT NOT VALID"));
        //continue;
      }

      /*Update geometry information of associated faces*/
      try{
        c->updateGeometry(ctx, this, p, startPosition);
      }catch(Exception* e){
        warning("Exception updateGeometry 1 %s", e->getError().c_str());
        error("exception!!");
        delete e;
        cc->status = Inactive;
        c->valid = false;
        if(c->status == Enabled){
          c->disableConstraint(this, ctx, true);
        }
        continue;
      }

      if(!c->valid){
        if(c->status == Enabled){
          error("Invalid candidate is enabled");
        }
        continue;
      }

      /*Update value for current distance*/
      c->evaluate(this, ctx, p, com, rot);

      /*Check geometric distance and state of the constraint*/

      /*For enabled constraints an addtional distance of EPS is added,
        so a current custance of EPS corresponds to a constraint
        distance of 0. This constraint distance should be smaller than
        half the epsilon.*/

      /*In case of a collision from inside, this will push the vertex
        on the wrong side!!*/

      if(true){
#if 0
      if(Abs(c->distancec - DISTANCE_TOL) > (T)0.0*DISTANCE_TOL /*&&
                                                                   c->tu.barycentricInTriangle(c->bary2, BARY_EPS)*/){
#endif
        warning("updating VF FF constraint, distance = %10.10e, %10.10e, %10.10e",
                c->distancec, c->distancec - EPS,
                c->distancec - DISTANCE_TOL);
#if 1
#ifdef ACTIVE_CONSTRAINTS
#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > SAFETY_DISTANCE){
            /*Constraint is not active, distance positive*/
            continue;
          }
        }else{
          //if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
          if(c->distancec < (T)(EPS + DISTANCE_TOL) &&
             c->distancec > (T)DISTANCE_TOL){

            /*Constraint is active, distance positive*/
            continue;
          }
        }
#else
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > EPS*2.0 ){
            /*Constraint is not active, distance positive*/
            /*Experimental, just get rid of it*/
            c->disableConstraint(this, ctx, true);
            continue;
          }
        }
#endif
#else
        if(cc->status != Active){
        //if(false){
          if(c->distancec > EPS*2.0 ){
            /*Constraint is not active, distance positive*/
            /*Experimental, just get rid of it*/
            c->disableConstraint(this, ctx, true);
            continue;
          }
          if(c->distancec > EPS*0.1 ){
            /*Constraint is not active, distance positive*/
            continue;
          }
        }else{
          //warning("Constraint active 1, dist = %10.10e",
          //      c->distancec - EPS + DISTANCE_TOL);
          //warning("EPS = %10.10e, DISTANCE_TOL = %10.10e",
          //      EPS, DISTANCE_TOL);

          if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
            //continue;
          }

          if(c->distancec - EPS - 5.0*DISTANCE_TOL > (T)0.0){
            //continue;
          }

          if(Abs(c->distancec - EPS) < DISTANCE_TOL){
            /*Constraint is active, distance positive*/
            //continue;
          }

          //if(c->forced && c->distancec > (T)1e-8){
          //  continue;
          //}

#if 0
          {
            Vector4<T> weights =
              c->tu.getBarycentricCoordinates(p.getVertex());
            if(!c->tu.barycentricInTriangle(weights, (T)BARY_EPS)){
              c->disableConstraint(this, ctx, true);
              constraintsChanged = true;
              continue;
            }
          }
#endif
          DBG(message("distance in testA = %10.10e",
                      c->distancec - EPS + DISTANCE_TOL));

          DBG(message("distance in testB = %10.10e",
                      c->distancec - EPS - DISTANCE_TOL));
        }

#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > SAFETY_DISTANCE){
            /*Constraint is not active or multiplier is very small,
              distance is positive*/
            continue;
          }
        }else{
          //warning("Constraint active 2, dist = %10.10e",
          //      c->distancec - EPS + DISTANCE_TOL);
          /*Valid*/
          //if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
          //continue;
          //}

          if(Abs(c->distancec - EPS) < DISTANCE_TOL){
            continue;
          }
        }
#endif
#endif
#endif
        /*Geometric distance and constraint distance are not in sync, update*/
        DBG(message("update VF constraint %d - %d", vertexId, c->faceId));
        bool skipped = false;

        //c->updateConstraint(this, ctx, startPosition, p, com, rot, &skipped);
#ifdef ACTIVE_CONSTRAINTS
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > EPS*1.0 ){
            continue;
          }
        }else{
          if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
            continue;
          }

          if(c->distancec - EPS - 1.0*DISTANCE_TOL > (T)0.0){
            continue;
          }
        }
#else
        if(cc->status != Active){
          if(c->distancec > DISTANCE_EPSILON * 5.0){
            /*Constraint is not active, distance is positive.*/
            c->disableConstraint(this, ctx, true);
            continue;
          }

          if(c->distancec > EPS*1.0 ){
            continue;
          }
        }

        if(cc->status == Active){
          if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
            continue;
          }

          if(c->distancec - EPS - 1.0*DISTANCE_TOL > (T)0.0){
            continue;
          }
        }
#endif

        c->updateConstraint(this, ctx, startPosition, p, com, rot, &skipped,
                            stats);

        if(skipped){
          //constraintsChanged = true;
          continue;
        }

        //cc->status = Active;
        if(cc->status != Active){
          cc->status = Active;
          cc->frictionStatus[0] = Static;
          cc->frictionStatus[1] = Static;
        }

        message("Updated and enabled VF constraint");

        constraintsChanged = true;

        (message("VF CONSTRAINT NOT CORRECT\n\n\n"));
        (message("real distance = %10.10e", c->distancec + DISTANCE_EPS));
        (warning("VF CONSTRAINT NOT CORRECT %d, %d, backFace = %d\n\n\n", vertexId, c->faceId, backFace));
        (warning("real distance = %10.10e", c->distancec + DISTANCE_EPS));
      }
    }
    return constraintsChanged;
  }

  template<class T>
  bool ConstraintSet<T>::updateInitial(OrientedVertex<T>& p, Vector4<T>& com,
                                       Quaternion<T>& rot,
                                       CollisionContext<T>* ctx,
                                       EvalStats& stats,
                                       bool friction, bool force, T bnorm){
    CandidateMapIterator it = candidates.begin();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    /*Simple plane point check for a fast detection in change*/
    while(it != candidates.end()){
      Candidate* c = (*it++).getData();

#if 0
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(c->enabledId);

      /*Update geometry information of associated faces*/
      try{
        c->updateGeometry(ctx, this, p, startPosition);
      }catch(Exception* e){
        warning("Exception updateGeometry 1 %s", e->getError().c_str());
        error("exception!!");
        delete e;
        cc->status = Inactive;
        c->valid = false;
        if(c->status == Enabled){
          c->disableConstraint(this, ctx, true);
        }
        continue;
      }

      if(!c->valid){
        if(c->status == Enabled){
          error("Invalid candidate is enabled");
        }
        continue;
      }
#endif

      startPosition = p;
      startCenterOfMass = com;
      startOrientation = rot;

      bool skipped = false;

      c->updateInitial(this, ctx, startPosition, p, com, rot, &skipped, stats);

      if(skipped){
        message("vf constraint updated");
        constraintsChanged = true;
      }
    }
    return constraintsChanged;
  }

  /*Updates the candidatelist due to some updated geometry. Each
    potential face that is not in the candidatelist, will be added.*/
  template<class T>
  void ConstraintSet<T>::incrementalUpdate(CollisionContext<T>* ctx,
                                           bool onlyForced){

    if(ctx->mesh->vertices[vertexId].leaving == Mesh::UndefinedIndex){
      /*Internal vertex, is not a part of the outher boundary, skip*/
      return;
    }

    OrientedVertex<T> x;
    ctx->mesh->getOrientedVertex(&x, vertexId, backFace);

    Vector4<T> com(0,0,0,0);
    Quaternion<T> rot(0,0,0,1);

    if(Mesh::isRigid(type)){
      int objectId = ctx->mesh->vertexObjectMap[vertexId];
      com = ctx->mesh->centerOfMass[objectId];
      rot = ctx->mesh->orientations[objectId];
    }

    List<int> currentPotentials;  /*Stores face ids of found potentials*/

    if(!onlyForced){
      ctx->extractPotentialVertexFace(vertexId, currentPotentials, backFace);
    }

    List<int>::Iterator lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentFace = *lit++;
      ctx->mesh->halfFaces[currentFace].visited = true;
    }

    /*Add forced candidates*/
    Tree<int>::Iterator forcedIt = forcedPotentials.begin();
    while(forcedIt != forcedPotentials.end()){
      int forcedPotential = *forcedIt++;

      if(ctx->mesh->halfFaces[forcedPotential].visited == false){
        currentPotentials.append(forcedPotential);
        ctx->mesh->halfFaces[forcedPotential].visited = true;
        //currentPotentials.uniqueInsert(forcedPotential, forcedPotential);
      }
    }

    lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentFace = *lit++;
      ctx->mesh->halfFaces[currentFace].visited = false;
    }

    forcedPotentials.clear();

    /*Add new candidates-----------------------------------*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;
      int index = candidatesTree.findIndex(candidate);

      if(index == -1){
        if(backFace){
          if(Mesh::isRigidOrStatic(ctx->mesh->halfFaces[candidate].type)){
            /*A rigid object can not have internal collisions, hence Rigid*/
            continue;
          }

          int faceId = ctx->mesh->halfEdges[ctx->mesh->vertices[vertexId].leaving].half_face;

          if(!ctx->stree->facesShareSameRoot(faceId, candidate)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        Candidate* cc = new Candidate(ctx, candidate, backFace);

        ctx->mesh->setCurrentEdge(ctx->mesh->halfFaces[candidate].half_edge);

        int faceVertexId = ctx->mesh->getOriginVertex();

        cc->vertexType = type;
        cc->faceType   = ctx->mesh->vertices[faceVertexId].type;

        candidates.insert(candidate, cc);
        candidatesTree.insert(candidate, 1);

        /*Update geometrical info*/
        ctx->mesh->getTriangleIndices(candidate, cc->indices, backFace);

        cc->recompute(this, ctx, x, com, rot);
      }
    }
  }

  /*Updates the geometry information*/
  template<class T>
  void ConstraintSet<T>::update(CollisionContext<T>* ctx){
    if(ctx->mesh->vertices[vertexId].leaving == Mesh::UndefinedIndex){
      /*Internal vertex, is not a part of the outher boundary, skip*/
      return;
    }

    if(backFace){
      if(Mesh::isRigidOrStatic(ctx->mesh->vertices[vertexId].type)){
        /*A rigid object can not have internal collisions*/
        return;
      }
    }

    OrientedVertex<T> x;
    ctx->mesh->getOrientedVertex(&x, vertexId, backFace);

    Vector4<T> com(0,0,0,0);
    Quaternion<T> rot(0,0,0,1);

    if(Mesh::isRigid(type)){
      int objectId = ctx->mesh->vertexObjectMap[vertexId];
      com = ctx->mesh->centerOfMass[objectId];
      rot = ctx->mesh->orientations[objectId];
    }

    /*Clear forced potentials*/
    forcedPotentials.clear();

    /*Find potential faces for vertex (vertexId)*/
    List<int> currentPotentials;
    ctx->extractPotentialVertexFace(vertexId, currentPotentials, backFace);

    /*Add new candidates which are note yet in the candidate list*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;
      int index = candidatesTree.findIndex(candidate);

      if(index == -1){
        /*Current candidate not found in set, add*/

        if(backFace){
          if(Mesh::isRigidOrStatic(ctx->mesh->halfFaces[candidate].type)){
            /*A rigid object can not have internal collisions, hence Rigid*/
            continue;
          }

          int faceId = ctx->mesh->halfEdges[ctx->mesh->vertices[vertexId].leaving].half_face;

          if(!ctx->stree->facesShareSameRoot(faceId, candidate)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        Candidate* cc = new Candidate(ctx, candidate, backFace);

        ctx->mesh->setCurrentEdge(ctx->mesh->halfFaces[candidate].half_edge);

        int faceVertexId = ctx->mesh->getOriginVertex();

        cc->vertexType = type;
        cc->faceType   = ctx->mesh->vertices[faceVertexId].type;

        candidates.insert(candidate, cc);
        candidatesTree.insert(candidate, 1);

        /*Update geometrical info*/
        ctx->mesh->getTriangleIndices(candidate, cc->indices, backFace);
      }
    }

    /*-----------------------------------------------------*/

    /*Check for disappeared candidates, remove them from the set.*/
    CandidateMapIterator tcit = candidates.begin();
    while(tcit != candidates.end()){
      Candidate* cc = (*tcit++).getData();
      cc->flag = false;
    }

    List<int>::Iterator lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      tcit = candidates.find(*lit++);
      if(tcit != candidates.end()){
        (*tcit).getData()->flag = true;
      }
    }

    tcit = candidates.begin();
    while(tcit != candidates.end()){
      Candidate* cc = (*tcit).getData();

      /*If not found in new potential set, remove.*/
      if(cc->flag == false){
        /*Disable and remove*/
        /*Check if constraint is enabled*/

        if(cc->status == Enabled){
          //error("Deleting enabled constraint!!");
          tcit++;
        }else{
          cc->disableConstraint(this, ctx, true);
          candidates.remove(tcit);
          candidatesTree.remove(cc->faceId);
          delete cc;
        }
      }else{
        tcit++;
      }
    }

    /*Recompute states at beginning of timestep*/
    CandidateMapIterator it3 = candidates.begin();

    while(it3 != candidates.end()){
      Candidate* s = (*it3++).getData();
      s->recompute(this, ctx, x, com, rot);
    }

    /*-----------------------------------------------------*/

    startPosition     = x;
    startCenterOfMass = com;
    startOrientation  = rot;
    lastPosition      = x;
    lastCenterOfMass  = com;
    lastOrientation   = rot;
  }

  template<class T>
  void ConstraintSet<T>::showFaceCandidateState(int face,
                                                CollisionContext<T>* ctx){
    message("Show vertex set %d", vertexId);

    message("Start position, com, orientation");

    std::cout << startPosition << std::endl;
    std::cout << startCenterOfMass << std::endl;
    std::cout << startOrientation << std::endl;

    message("Updated position, com, orientation");

    std::cout << lastPosition << std::endl;
    std::cout << lastCenterOfMass << std::endl;
    std::cout << lastOrientation << std::endl;

    message("set has %d candidates", candidates.size());

    CandidateMapIterator it = candidates.begin();
    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(face == -1){
        candidate->showCandidateState(lastPosition, ctx);
      }else if(candidate->faceId == face){
        candidate->showCandidateState(lastPosition, ctx);
      }
    }
  }

  template<class T>
  void ConstraintSet<T>::updateStartPositions(const OrientedVertex<T>& v,
                                              const Vector4<T>& com,
                                              const Quaternion<T>& rot,
                                              Candidate* c){
    startPosition = v;
    startCenterOfMass = com;
    startOrientation = rot;

    c->updateInitialState(v, com, rot);
#if 1
    CandidateMapIterator it = candidates.begin();
    while(it != candidates.end()){
      Candidate* candidate = (*it++).getData();
      candidate->updateInitialState(v, com, rot);
    }
#endif
    warning("UPDATING INITIAL STATE!!!\n\n\n\n\n\n\n\n\n\n\n\n\n");
    //exit(0);
  }


  template class ConstraintSet<float>;
  template class ConstraintSet<double>;
}
