/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/constraints/RowContactConstraintMultiplier.hpp"
#include "math/constraints/ContactConstraint.hpp"

namespace CGF{
  template<int N, class T>
  void RowContactConstraintMultiplier<N, T>::
  multiply(const AbstractRowConstraint<N, T>* aa,
           VectorC2<T>* D,
           const AbstractRowConstraint<N, T>* bb){
    ContactConstraint<N, T>* a = (ContactConstraint<N, T>*)aa;
    ContactConstraint<N, T>* b = (ContactConstraint<N, T>*)bb;

    //a->getMatrix()->constraints.print();

    //message("a->row_id = %d", a->row_id);
    //message("b->row_id = %d", b->row_id);

    this->row = a->getMatrix()->constraints.reverseIndex(a->row_id);
    this->col = b->getMatrix()->constraints.reverseIndex(b->row_id);
    this->offset = a->getOffset();

    PRINT(this->row);
    PRINT(this->col);

    clear();

    //error("used?");

    PRINT_FUNCTION;

    PRINT(a->getSize());
    PRINT(b->getSize());

    /*Multiply D * b'*/
    for(int i=0;i<b->getSize();i++){
      if(a->index[i] == Constraint::undefined){
        //continue;
      }

      //In principle we should use the transposed matrix. However, the
      //multiplication below just takes the dot product of the row
      //vectors of the non-transposed matrices, which has the same
      //result.
      tmp[i] = b->normals[i];
      PRINT(a->normals[i]);
      PRINT(b->normals[i]);

      if(b->index[i] == Constraint::undefined){
        continue;
      }


      //#undef FRICTION
#ifdef FRICTION
      if(b->frictionStatus[0] == Static){
        //tmp[i][1].set(0,0,0,0);
      }
      if(b->frictionStatus[1] == Static){
        //tmp[i][2].set(0,0,0,0);
      }
#else
      //tmp[i][1].set(0,0,0,0);
      //tmp[i][2].set(0,0,0,0);
#endif

      int col = b->index[i]*3;
      for(int j=0;j<3;j++){
        T val = (T)1.0/(*D)[col+j];
        PRINT(val);
        for(int k=0;k<3;k++){
          tmp[i][k][j] *= val;
        }
      }
    }

    /*D * b' is stored in temp*/

    /*multiply a * D * b'*/
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        T val = 0;

        for(int k=0;k<a->getSize();k++){
          int indexA = a->index[k];
          if(indexA == Constraint::undefined){
            continue;
          }
          for(int l=0;l<b->getSize();l++){
            int indexB = b->index[l];
            if(indexB == Constraint::undefined){
              continue;
            }
            if(indexA == indexB){
              if(j>0){
#ifdef FRICTION
                //if(a->frictionStatus[j-1] == Kinetic){
                val += dot(a->normals[k][j], tmp[l][i]);
                //}
#else
                //skip
#endif
              }else{
                val += dot(a->normals[k][j], tmp[l][i]);
              }
            }
          }
        }

        result[j][i] = val;
      }
    }
    PRINT(result);
  }


  template<int N, class T>
  void RowContactConstraintMultiplier<N, T>::store(SpMatrix<N, T>* mat){
    DBG(message("Store @ %d, %d", this->row, this->col));

    //    message("w, h = %d, %d", mat->getWidth(), mat->getHeight());

    //error("used?");
    for(int i=0;i<this->offset;i++){
      for(int j=0;j<this->offset;j++){
        //  message("Store %d, %d", this->row*this->offset + i,
        //      this->col*this->offset + j);
        (*mat)[this->row*this->offset + i][this->col*this->offset + j] =
          result[i][j];
      }
    }
  }

  template class RowContactConstraintMultiplier<1, float>;
  template class RowContactConstraintMultiplier<2, float>;
  template class RowContactConstraintMultiplier<4, float>;
  template class RowContactConstraintMultiplier<8, float>;
  template class RowContactConstraintMultiplier<1, double>;
  template class RowContactConstraintMultiplier<2, double>;
  template class RowContactConstraintMultiplier<4, double>;
  template class RowContactConstraintMultiplier<8, double>;
}
