/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/constraints/ContactBlockConstraint.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "math/VectorC.hpp"


namespace CGF{

  template<int N, class T>
  ContactBlockConstraint<N, T>::ContactBlockConstraint(){
    this->offset = 3;
    index[0] = index[1] = index[2] = index[3] = index[4] = Constraint::undefined;
  }

  template<int N, class T>
  void ContactBlockConstraint<N, T>::multiplyAHSHA(VectorC2<T>& r,
                                                   const VectorC2<T>& x,
                                                   const SpMatrixC2<N, T>* mat,
                                                   bool transposed,
                                                   const VectorC2<T>* mask)const{
#ifdef PREC_FULL
    ConstraintMatrixStatus status = mat->getStatus();

    if(status == Individual){
      if(this->rowConstraint->status == Inactive){
        return;//continue;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size   = this->getSize();

    const T*  maskx  = 0;

    if(mask){
      maskx = mask->getExtendedData();
    }

    //r.clear();
    //return;
    /*Multiply SHA*x = r*/
    //int size = x.getSize();
    const T* xdata  = x.getData();
    T*       rdatax = r.getExtendedData();
    T*       rdata  = r.getData();

    ContactConstraint<N, T>* constraint =
      (ContactConstraint<N, T>*)this->rowConstraint;

    //int cindex = this->row_id;
    //int offset = this->getOffset();

    Vector4<T> lx;
    Vector4<T> lr;

    for(int i=0;i<size;i++){
      if(this->getIndex(i) == Constraint::undefined){
        continue;
      }

      /*Load x for sub-constraint i*/
      int col = this->getIndex(i)*3;

      if(mask){
        for(int j=0;j<3;j++){
          lx[j] = xdata[col+j] * (*mask)[col+j];
        }
      }else{
        for(int j=0;j<3;j++){
          lx[j] = xdata[col+j];
        }
      }

      if(transposed){
        lr += this->AHST[i] * lx;
      }else{
        lr += this->SHA[i]  * lx;
      }
    }

    for(int i=0;i<3;i++){
      if(mask){
        lr[i] *= maskx[cindex * offset + i];
      }

      if(i > 0){
        if(status == Individual){
          if(constraint->frictionStatus[i-1] == Kinetic){
            ///Important
            //Skip active constraint
            lr[i] = 0;
          }
        }
      }

      /*Also store lr in r, since this is the result of the AHS
        preconditioner*/
      //r[size + cindex * offset + i] = lr[i];
      //r[size + cindex * offset + i] = lr[i];
      rdatax[cindex * offset + i] = lr[i];
    }

    /*Result of multiplication stored in lr*/

    /*Multiply AHT*lr  and store A - AHT*lr = r*/

    /*Multiply AHT*/
    for(int i=0;i<size;i++){
      if(this->getIndex(i) == Constraint::undefined){
        continue;
      }

      Vector4<T> res;
      if(transposed){
        res = this->HAT[i] * lr;
      }else{
        res = this->AHT[i] * lr;
      }

      int col = this->getIndex(i)*3;

      if(mask){
        for(int j=0;j<3;j++){
          rdata[col+j] += -res[j] * (*mask)[col+j];
        }
      }else{
        for(int j=0;j<3;j++){
          rdata[col+j] += -res[j];
        }
      }
    }

    /*Clear constraint part of r*/
    //r.clear(CONSTRAINTS);

    /*Continue with normal multiplication and add results to r*/
#else
#ifdef PREC_DIAG
    return;
#endif
#endif
  }

  template<int N, class T>
  void ContactBlockConstraint<N, T>::generalMultiply(VectorC2<T>& r,
                                                     const VectorC2<T>& x,
                                                     bool transposed)const{
    ConstraintMatrixStatus status = this->matrix->getStatus();
    if(status == Individual){
      if(this->rowConstraint->status == Inactive){
        return;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }


    //int size   = x.getSize();
    int cindex = this->row_id;
    int offset = this->getOffset();
#ifdef PREC_FULL
    int size   = this->getSize();
#endif
    const T* xdatax = x.getExtendedData();
#ifdef PREC_FULL
    T*       rdata  = r.getData();
#endif
    T*       rdatax = r.getExtendedData();

    ContactConstraint<N, T>* constraint =
      (ContactConstraint<N, T>*)this->rowConstraint;


    /*Multiply S*/

    Vector4<T> lx;
    for(int i=0;i<3;i++){
      lx[i] = xdatax[cindex * offset + i];

      if(i>0){
        if(status == Individual){
          if(constraint->frictionStatus[i-1] == Kinetic){
            //Skip active constraint
            lx[i] = (T)0.0;
          }
        }else if(status == AllActive){

        }
      }
    }

    Vector4<T> res;
#ifdef PREC_FULL
    res = -S * lx;
#else
#ifdef PREC_DIAG
    res =  S * lx;
#endif
#endif

#ifdef PREC_FULL
    /*Multiply AHS*/
    for(int i=0;i<size;i++){
      if(getIndex(i) == Constraint::undefined){
        continue;
      }

      int col = getIndex(i)*3;
      Vector4<T> res2;

      if(transposed){
        res2 = SHAT[i] * lx;
      }else{
        res2 = AHS[i]  * lx;
      }

      for(int j=0;j<3;j++){
        rdata[col+j] += res2[j];
      }
    }

#if 0
    /*Is already performed through pre-multiply and stored in r*/
    /*Multiply SHA*/
    for(int i=0;i<size;i++){
      if(getIndex(i) == Constraint::undefined){
        continue;
      }
      int col = getIndex(i)*3;

      for(int j=0;j<3;j++){
        lx[j] = x[col+j];
      }

      if(transposed){
        res += AHST[i] * lx;
      }else{
        res += SHA[i]  * lx;
      }
    }
#endif
#endif
    for(int k=0;k<3;k++){
      if(k>0){
        if(status == Individual){
          if(constraint->frictionStatus[k-1] == Kinetic){
            //Skip active constraint
            res[k] = 0;
          }
        }else if(status == AllActive){
          //do not stop and load the multiplier
        }
      }

      rdatax[cindex * offset + k] += res[k];
    }
  }


  /*Performs a multiplication r = Q x*/
  template<int N, class T>
  void ContactBlockConstraint<N, T>::multiply(VectorC2<T>& r,
                                              const VectorC2<T>& x)const{
    generalMultiply(r, x, false);
  }

  /*Performs a multiplication r = Q^T x*/
  template<int N, class T>
  void ContactBlockConstraint<N, T>::multiplyTransposed(VectorC2<T>& r,
                                                        const VectorC2<T>& x)const{
    generalMultiply(r, x, true);
  }

  template class ContactBlockConstraint<1, float>;
  template class ContactBlockConstraint<2, float>;
  template class ContactBlockConstraint<4, float>;
  template class ContactBlockConstraint<8, float>;

  template class ContactBlockConstraint<1, double>;
  template class ContactBlockConstraint<2, double>;
  template class ContactBlockConstraint<4, double>;
  template class ContactBlockConstraint<8, double>;
}
