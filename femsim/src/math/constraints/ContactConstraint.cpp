/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/constraints/ContactConstraint.hpp"
#include "math/constraints/ContactBlockConstraint.hpp"
#include "math/constraints/RowContactConstraintMultiplier.hpp"
#include "math/ConstraintTol.hpp"
#include "datastructures/AdjacencyList.hpp"

namespace CGF{

  extern double scf;

#define GAMMA_FACTOR 1.0
#define DIFF_EPS 1e-6*0

#define RERROR (1e-5)
#define RERRORKF (RERROR*100.75) /*Not used*/

  template<class T>
  T Pos2(T v){
    return Pos(v);
  }

#ifdef FRICTION_CONE
  extern double frictionNorm;
  //extern double forceNorm;
#else
  extern double frictionNorm;
#endif

  template<int N, class T>
  ContactConstraint<N, T>::ContactConstraint(){
#if defined (FRICTION_APPROX)
    //Active multiplier constraint constraints gamma
    //Inactive multiplier constaint does not constraint gamma,
    //hence, static friction
    frictionStatus[0] = Kinetic; //->kinetic friction
    frictionStatus[1] = Kinetic; //->kinetic friction

    frictionStatus[0] = Kinetic; //->kinetic friction
    frictionStatus[1] = Kinetic; //->kinetic friction
#else
#if defined(ACTIVE_CONSTRAINTS)
    //Active friction constraint tries to keep the tangental
    //velocities at zero
    //Inactive friction constraint allows tangental velocity to be
    //non-zero, but an additional friction force is introduced
    frictionStatus[0] = Kinetic;   //->static friction
    frictionStatus[1] = Kinetic;   //->static friction
#else
#error "No Multiplier mode specified"
#endif
#endif
    this->offset = 3;

    for(int i=0;i<5;i++){
      index[i] = Constraint::undefined;
    }

    c.set(0, 0, 0, 0);

    for(int i=0;i<3;i++){
      setMultipliers[i] = 0;
      acceptedMultipliers[i] = 0;
    }

    cache   = (T)0.0;
    cacheT1 = (T)0.0;
    cacheT2 = (T)0.0;

    preconditionerType = FULL_PRECONDITIONER;

#ifdef FRICTION_CONE
    lastHV = (T)0.0;
    HVDiff = (T)0.0;

    for(int i=0;i<HISTORY_SIZE;i++){
      magnitudeHistory[i] = (T)0.0;
    }
#endif
    usedScale = (T)1.0;

    this->subType = CONTACT_CONSTRAINT_SUBTYPE;
  }

  template<int N, class T>
  void ContactConstraint<N, T>::preMultiply(VectorC2<T>& r,
                                            const VectorC2<T>& x,
                                            const SpMatrixC2<N, T>* mat,
                                            bool transposed,
                                            const VectorC2<T>* mask)const{
    /*Do nothing, this type of constraint does not need any special
      preMultiplication stuff.*/
  }

  template<int N, class T>
  AbstractBlockConstraint<N, T>*
  ContactConstraint<N, T>::constructBlockConstraint()const{
    AbstractBlockConstraint<N, T>* cc = new ContactBlockConstraint<N, T>();
    cc->status = this->status;
    blockConstraint = cc;
    return cc;
  }

  template<int N, class T>
  RowConstraintMultiplier<N, T>*
  ContactConstraint<N, T>::constructConstraintMultiplier()const{
    PRINT_FUNCTION;
    return new RowContactConstraintMultiplier<N, T>();
  }

  template<int N, class T>
  void ContactConstraint<N, T>::setMu(T m){
    mu = m;
  }

  /*r = vector4, i = row in constraint, j = block, idx = first
    absolute index*/
  template<int N, class T>
  void ContactConstraint<N, T>::setRow(const Vector4<T>& r,
                                       T fc, int i, int j, int idx){
    cgfassert(i < 4);
    cgfassert(j < 5);

    if(IsNan(r[0]) || IsNan(r[1]) || IsNan(r[2]) || IsNan(fc)){
      std::cout << r << std::endl;
      std::cout << fc << std::endl;
      error("NAN constraint vector");
    }


    normals[j][i] = r;
    c[i] = fc;
    this->index[j] = idx;
    if((j+1) > this->cSize){
      this->cSize = j+1;
    }
  }

  template<int N, class T>
  Vector4<T> ContactConstraint<N, T>::getRow(int i, int j)const{
    PRINT_FUNCTION;
    DBG(message("i = %d, j = %d", i, j));
    cgfassert(i < 4);
    cgfassert(j < 5);
    return normals[j][i];
  }

  template<int N, class T>
  T& ContactConstraint<N, T>::getConstraintValue(int i){
    if(i == 0){
      return c[i];
    }else{
#ifdef ACTIVE_CONSTRAINTS
      return c[i];
#else
      if(frictionStatus[i-1] == Kinetic){
        zero = 0;
        return zero;
      }
#endif
    }
    return c[i];
  }

  template<int N, class T>
  const T& ContactConstraint<N, T>::getConstraintValue(int i)const{
    if(i == 0){
      return c[i];
    }else{
#ifdef ACTIVE_CONSTRAINTS
      return c[i];
#else
      if(frictionStatus[i-1] == Kinetic){
        zero = 0;
        return zero;
      }
#endif
    }
    return c[i];
  }

  template<int N, class T>
  ConstraintStatus ContactConstraint<N, T>::getStatus()const{
    return this->status;
  }

  template<int N, class T>
  void ContactConstraint<N, T>::resetMultipliers(VectorC2<T>& x,
                                                 Vector4<T>& old)const{
    int row = this->row_id;
    T* xdatax = x.getExtendedData();

    cache   = xdatax[row * this->offset + 0];
    cacheT1 = xdatax[row * this->offset + 1];
    cacheT2 = xdatax[row * this->offset + 2];

    for(int i=0;i<this->offset;i++){
      xdatax[row * this->offset + i] = old[i];
    }
  }

  template<int N, class T>
  void ContactConstraint<N, T>::cacheMultipliers(Vector4<T>& old,
                                                 VectorC2<T>& x)const{
    int row = this->row_id;
    T* xdatax = x.getExtendedData();

    //cache = xdatax[row * this->offset + 0];
    for(int i=0;i<this->offset;i++){
      old[i] = xdatax[row * this->offset + i];
    }
  }

  template<int N, class T>
  void ContactConstraint<N, T>::loadLocalValues(const VectorC2<T>& x,
                                                const VectorC2<T>& b,
                                                Vector4<T>& lm, Vector4<T>& lr,
                                                Vector4<T>& lc)const{
    int row  = this->row_id;
    int size = this->getSize();

    const T* xdatax = x.getExtendedData();
    const T* xdata  = x.getData();

    for(int i=0;i<3;i++){
      lm[i]  = xdatax[row * this->offset + i]; /*Get corresponding
                                                 multipliers*/
      lc[i] = c[i];
#ifndef FRICTION
      break; /*No need for looking up zeros in case of a
               friction-free simulation.*/
#endif
    }

    /*Evaluate distance*/
    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }
      Vector4<T> lx;

      for(int i=0;i<3;i++){
        lx[i] = xdata[this->index[j]*3 + i];  /*Get vector x*/
      }

      lr[0] += dot(normals[j][0], lx);
#ifdef FRICTION
      lr[1] += dot(normals[j][1], lx);
      lr[2] += dot(normals[j][2], lx);
#endif
    }

    for(int j=0;j<3;j++){
      lr[j] -= lc[j];
    }
  }

#ifdef FRICTION_CONE
  template<int N, class T>
  void ContactConstraint<N, T>::evaluateAndUpdateFriction(Vector4<T>& lm,
                                                          Vector4<T>& lr,
                                                          VectorC2<T>& x,
                                                          VectorC2<T>& b,
                                                          bool* changed,
                                                          bool* frictionChanged,
                                                          EvalStats& stats){
    int height = x.getSize();
    int row = this->row_id;

    cgfassert(frictionStatus[0] == frictionStatus[1]);

#if 1
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)blockConstraint;
#endif

    Vector4<T> rVec(0, lr[1], lr[2], 0);

    if(rVec.length() > 1e-14){
      rVec.normalize();
    }else{
      rVec.set(0,1,1,0);
      rVec.normalize();
    }

    Vector4<T> hvVec(0, lr[1], lr[2], 0);
    Vector4<T>  mVec(0, lm[1], lm[2], 0);

    /*
      mVec is ONLY defined when the constraint is in static friction
      mode. In case of kinetic friction, the multipliers are stored in
      acceptedMultipliers.
     */

    if(mVec.length() > 1e-14){
      mVec.normalize();
    }else{
      mVec = hvVec;
      if(mVec.length() > 1e-14){
        mVec.normalize();
      }
    }

    if( (Sqrt(Sqr(lm[1]) + Sqr(lm[2])) >= Pos2(mu * lm[0]) &&
         dot(hvVec, mVec) > -0*HV_EPS/*this->bb2norm/1.0*/)){
      /*Kinetic friction*/

      if(frictionStatus[0] == Static){
        /*Switch to kinetic*/
#if 1
        /*Compute difference*/
        T difference = Abs(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) - Pos2(mu * lm[0]));

        T relativeError = Max(difference / (usedScale * bc->S[1][1]),
                              difference / (usedScale * bc->S[2][2]));
#endif

        if(relativeError > 1*RERROR/*this->bb2norm/scf*/){
          //if(true){
#if 1
          kineticVector = mVec;

          /*AVERAGE_FRICTION**/
          averageFrictionDirection.clear();
          averageFrictionMagnitude.clear();

          //kineticVector = averageFrictionDirection.getAverage();


          if(kineticVector.length() > 1e-14){
            kineticVector.normalize();
            averageFrictionDirection.addSample(kineticVector);
          }
          else{
            if(hvVec.length() > 1e-14){
              hvVec.normalize();
              averageFrictionDirection.addSample(hvVec);
              //kineticVector += hvVec;
              kineticVector.normalize();
            }
          }

          //if(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) != (T)0.0){
            averageFrictionMagnitude.addSample(Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
            //}
#endif
            (message("\t[%d] friction update switch to kinetic, value = %10.10e, %10.10e, %10.10e, %10.10e",
                   this->row_id,
                   Sqrt(Sqr(lm[1]) + Sqr(lm[2])),
                   Pos2(mu * lm[0]),
                   dot(hvVec, mVec),
                   relativeError
                   ));
          DBG(printIndices(std::cout));

          START_DEBUG;
          message("mVec, hvVec, kineticVector");
          std::cout << mVec << std::endl;
          std::cout << hvVec << std::endl;
          std::cout << kineticVector << std::endl;
          END_DEBUG;

          frictionStatus[0] = Kinetic;
          frictionStatus[1] = Kinetic;

#if 1
          cumulativeVector = kineticVector;

          acceptedKineticVector = kineticVector;

          setMultipliers[1] = lm[1];
          setMultipliers[2] = lm[2];



#if 0
          T newValue = Pos2(lm[0]) * mu;
          //T oldValue = Sqrt(Sqr(acceptedMultipliers[1]) +
          //                Sqr(acceptedMultipliers[2]));
          //T value = (newValue + (T)8.0*oldValue)/(T)9.0;

          setMultipliers[1] = newValue * kineticVector[1];
          setMultipliers[2] = newValue * kineticVector[2];
#endif

          //////
          acceptedMultipliers[1] = setMultipliers[1];
          acceptedMultipliers[2] = setMultipliers[2];

          DBG(message("accepted multipliers = %10.10e, %10.10e",
                      acceptedMultipliers[1], acceptedMultipliers[2]));
#endif

          *changed = true;
          stats.n_friction_check++;

          /*Reset x and b*/
          x[height + row * this->offset + 1] = 0;
          x[height + row * this->offset + 2] = 0;

          b[height + row * this->offset + 1] = 0;
          b[height + row * this->offset + 2] = 0;
        }
      }
    }else{
      /*Static friction*/

      Vector4<T> acmVec(0, acceptedMultipliers[1], acceptedMultipliers[2], 0);

      if(frictionStatus[0] == Kinetic &&
         dot(hvVec, acmVec) < -0*HV_EPS/*this->bb2norm/1.0*/){
        /*Switch to static*/

        (message("\t[%d] friction update switch to static, value = %10.10e, %10.10e, %10.10e",
                 this->row_id,
                 kineticVector[1] * lr[1] + kineticVector[2] * lr[2],
                 Sqrt(Sqr(lm[1]) + Sqr(lm[2])), mu * lm[0]
                 )  );

        DBG(printIndices(std::cout));

        START_DEBUG;
        message("amVec, hvVec");
        std::cout << acmVec << std::endl;
        std::cout << hvVec << std::endl;
        END_DEBUG;

        //Deactivate constraint
        frictionStatus[0] = Static;
        frictionStatus[1] = Static;

        *changed = true;
        stats.n_friction_check++;

        /*Set x and b*/
        //x[height + row * this->offset + 1] = (T)1.0*mu*Pos2(lm[0])*kineticVector[1];
        x[height + row * this->offset + 1] = (T)acceptedMultipliers[1];
        b[height + row * this->offset + 1] = c[1];

        //x[height + row * this->offset + 2] = (T)1.0*mu*Pos2(lm[0])*kineticVector[2];
        x[height + row * this->offset + 2] = (T)acceptedMultipliers[2];
        b[height + row * this->offset + 2] = c[2];

        lm[1] = x[height + row * this->offset + 1];
        lm[2] = x[height + row * this->offset + 2];

        *frictionChanged = true;

        /*Reset kinetic vector*/
        kineticVector.set(0,0,0,0);

        setMultipliers[1] = (T)0.0;
        setMultipliers[2] = (T)0.0;

        acceptedMultipliers[1] = setMultipliers[1];
        acceptedMultipliers[2] = setMultipliers[2];
      }
    }
  }
#endif/*FRICTION_CONE*/

  /*Update state of friction at convergence
    according to gamma and lambda alone*/
#ifdef FRICTION_CONE
  template<int N, class T>
  void ContactConstraint<N, T>::forceFriction(Vector4<T>& lm,
                                              Vector4<T>& lr,
                                              VectorC2<T>& x,
                                              VectorC2<T>& b,
                                              bool* changed,
                                              bool* frictionChanged,
                                              EvalStats& stats){
    int height = x.getSize();
    int row = this->row_id;

    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)blockConstraint;

    DBG(message("FR force, %10.10e, %10.10e, %10.10e, %10.10e", lr[1], lr[2], lm[1], lm[2]));
    DBG(message("FR      , %10.10e, %10.10e, %10.10e, %10.10e", setMultipliers[1], setMultipliers[2], acceptedMultipliers[1], acceptedMultipliers[2]));

    //Only change friction if lambda is valid
    if(frictionStatus[0] == Kinetic){
      /*Kinetic friction case*/
      if(lm[0] <= 0){
#if 1
        /*Disabled for stability*/
        if(Abs(acceptedMultipliers[1]) > 0.0 ||
           Abs(acceptedMultipliers[2]) > 0.0){
          acceptedMultipliers[1] = (T)0.0;
          acceptedMultipliers[2] = (T)0.0;

          setMultipliers[1] = (T)0.0;
          setMultipliers[2] = (T)0.0;
          *frictionChanged = true;
          *changed = true;
          DBG(message("\t[%d] friction force reset kinetic to zero", this->row_id));
          DBG(printIndices(std::cout));
          stats.n_friction_force++;
        }
#endif
      }else{
        Vector4<T> rVec(0, lr[1], lr[2], 0);

        //lastHV = rVec.length();
        //HVDiff = (T)0.0;

        if(rVec.length() > 1e-14){
          rVec.normalize();
        }else{
          rVec.set(0,1,1,0);
          rVec.normalize();
        }

        if( (((kineticVector[1] * lr[1] +
               kineticVector[2] * lr[2]) < -0*HV_EPS/*this->bb2norm/1.0*/)
             /*&&
             (Sqrt(Sqr(acceptedMultipliers[1]) +
                   Sqr(acceptedMultipliers[2])) < Pos2(mu * lm[0]))
                   )*/)){
             /*(kineticVector[1] * acceptedMultipliers[1] +
               kineticVector[2] * acceptedMultipliers[2]) < Pos2(lm[0]))){*/

          //Deactivate constraint
          frictionStatus[0] = Static;
          frictionStatus[1] = Static;

          DBG(message("\t[%d] friction force switch to static, value = %10.10e",
                      this->row_id,
                      kineticVector[1] * lr[1] + kineticVector[2] * lr[2] ));
          DBG(printIndices(std::cout));

          *changed = true;
          stats.n_friction_force++;
          //x[height + row * this->offset + 1] = (T)1.0*mu*Pos2(lm[0])*kineticVector[1];
          x[height + row * this->offset + 1] = acceptedMultipliers[1];
          b[height + row * this->offset + 1] = c[1];

          x[height + row * this->offset + 2] = acceptedMultipliers[2];
          b[height + row * this->offset + 2] = c[2];

          *frictionChanged = true;
          kineticVector.set(0,0,0,0);
          //cumulativeVector = kineticVector;
          averageFrictionDirection.clear();
          averageFrictionMagnitude.clear();
#if 1
          setMultipliers[1] = (T)0.0;
          setMultipliers[2] = (T)0.0;

          acceptedMultipliers[1] = setMultipliers[1];
          acceptedMultipliers[2] = setMultipliers[2];
#endif
        }
      }
    }else{
      /*Static friction case*/
      Vector4<T> hvVec(0, lr[1], lr[2], 0);
      Vector4<T> mVec (0, lm[1], lm[2], 0);

      if(mVec.length() > 1e-14){
        mVec.normalize();
      }

      /*Measure realtive error to constraint switch*/
      T difference = 0;
#if 0
      T relativeError = 0;
      for(int i=0;i<2;i++){
        for(int j=0;j<5;j++){
          if(this->index[j] == Constraint::undefined){
            continue;
          }
        }
      }

      for(int i=0;i<2;i++){
        for(int j=0;j<5;j++){
          if(this->index[j] == Constraint::undefined){
              continue;
          }
          Vector4<T> resNew = -normals[j][i+1] * mVec[i+1] * Pos2(mu * lm[0]);
          Vector4<T> resOld = -normals[j][i+1] * lm[i+1];

          Vector4<T> diff = resNew - resOld;

          difference += dot(diff, diff);

          Vector4<T> diag;

          for(int k=0;k<3;k++){
            int idx = this->index[j]*3+k;
            diag[k] = (T)1.0 / (*this->matrix)[idx][idx];
          }

          Vector4<T> frictionNew = -normals[j][i+1] *
            mu * Pos(lm[0]);

          Vector4<T> frictionOld = -normals[j][i+1] *
            Sqrt(Sqr(lm[1])+Sqr(lm[2]));

          Vector4<T> frictionDiff = frictionNew - frictionOld;
          Vector4<T> frictionDiff2 = frictionDiff;

          for(int k=0;k<3;k++){
            //frictionOld[k]  *= diag[k];
            frictionDiff[k] *= diag[k];
          }

          T nom = dot(frictionDiff, frictionDiff2);

          if(Abs(frictionNorm) < 1e-9){
            relativeError = Max(nom, relativeError);
          }else{
            relativeError = Max(nom/(T)frictionNorm, relativeError);
          }
          DBG(message("FORCE Friction nom = %10.10e, denom = %10.10e", nom, frictionNorm));
          DBG(message("FORCE relative error of friction = %10.10e\n\n", relativeError));
        }
      }

#else

      T relativeError = (T)0.0;

      /*Compute difference*/
      difference = Abs(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) - Pos2(mu * lm[0]));

      relativeError = Max(difference / (usedScale * bc->S[1][1]),
                          difference / (usedScale * bc->S[2][2]));
#endif

      //relativeError = (T)1.0;

      difference = Sqrt(difference);

      T relativeDifference =
        Abs(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) - Pos2(mu*lm[0]))/Pos2(mu*lm[0]);

      if(relativeDifference > 1){
        relativeDifference = (T)1.0/relativeDifference;
      }

      //relativeError = 10000.0;

      /*It seems to be crucial te set the tolerance here to 1e-6
        instead of something larger*/
      //////MODIFIED!!!
      if( (T)GAMMA_FACTOR * Sqrt(Sqr(lm[1]) + Sqr(lm[2])) >=
          1.0*Pos2(mu * lm[0]) && relativeError > 1*RERROR/*this->bb2norm/scf*/ /* &&
                                                                                difference > 1e-6 && relativeError > RERROR*/
          //&&dot(mVec, hvVec) > -HV_EPS/1.0
          ){
        START_DEBUG;
        warning("Should be forced static to kinetic");
        warning("lambda mu %10.10e", Pos2(mu*lm[0]));
        warning("gamma     %10.10e", Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
        std::cerr << lm << std::endl;
        std::cerr << lr << std::endl;
        std::cerr << kineticVector << std::endl;
        std::cerr << hvVec << std::endl;
        std::cerr << mVec << std::endl;
        END_DEBUG;
      }

      //////MODIFIED!!!
      if( (T)GAMMA_FACTOR * Sqrt(Sqr(lm[1]) + Sqr(lm[2])) >=
          1.0*Pos2(mu * lm[0]) && relativeError > 1*RERROR/*this->bb2norm/scf*/
          /*&&
            difference > 1e-6 &&*/
          //&& dot(mVec, hvVec) > -0*HV_EPS/1.0
          ){

        DBG(message("\t[%d] friction force switch to kinetic, value = %10.10e, %10.10e, %10.10e, %10.10e",
                this->row_id,
                Sqrt(Sqr(lm[1]) + Sqr(lm[2])),
                Pos2(mu * lm[0]),
                dot(hvVec, mVec),
                relativeError
                    ));
        DBG(printIndices(std::cout));

        START_DEBUG;
        message("gamma   = %10.10e", Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
        message("mlambda = %10.10e", Pos2(mu*lm[0]));
        message("difference = %10.10e", difference);
        (message("FORCED static to kinetic friction %d", this->row_id));

        warning("Forced static to kinetic");
        warning("lambda mu %10.10e", Pos2(mu*lm[0]));
        warning("gamma     %10.10e", Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
        std::cerr << lm << std::endl;
        std::cerr << lr << std::endl;
        std::cerr << kineticVector << std::endl;
        std::cerr << hvVec << std::endl;
        std::cerr << mVec << std::endl;
        END_DEBUG;

        kineticVector = mVec;
        if(kineticVector.length() > 1e-14){
          kineticVector.normalize();
        }else{
          if(hvVec.length() > 1e-14){
            hvVec.normalize();
            //kineticVector += hvVec;
            kineticVector.normalize();
          }
        }

        cumulativeVector = kineticVector;

        acceptedKineticVector = kineticVector;
        //cumulativeVector += (T)(60) * kineticVector * (T)0.85;
        //cumulativeVector /= (T)1.1;

        frictionStatus[0] = Kinetic;
        frictionStatus[1] = Kinetic;

        setMultipliers[1] = lm[1];
        setMultipliers[2] = lm[2];

#if 0
        T newValue = Pos2(lm[0]) * mu;
        T oldValue = Sqrt(Sqr(acceptedMultipliers[1]) +
                          Sqr(acceptedMultipliers[2]));
        T value = (newValue + (T)0.0*oldValue)/(T)1.0;

        setMultipliers[1] = value * kineticVector[1];
        setMultipliers[2] = value * kineticVector[2];
#endif

        for(int j=0;j<HISTORY_SIZE;j++){
          //magnitudeHistory[j] = Pos2(lm[0] * mu);
        }

        acceptedMultipliers[1] = setMultipliers[1];
        acceptedMultipliers[2] = setMultipliers[2];

        *changed = true;
        *frictionChanged = true;
        stats.n_friction_force++;

        x[height + row * this->offset + 1] = 0;
        x[height + row * this->offset + 2] = 0;

        b[height + row * this->offset + 1] = 0;
        b[height + row * this->offset + 2] = 0;
      }
    }
  }
#endif

  template<int N, class T>
  void ContactConstraint<N, T>::update(VectorC2<T>& b,
                                       VectorC2<T>& x,
                                       VectorC2<T>& b2){
#ifdef FRICTION_APPROX
    if(mu != (T)0.0){
      Vector4<T> lm, lr, lc;

      loadLocalValues(x, b, lm, lr, lc);

      bool changed = false;
      EvalStats stats;
      evaluateAndUpdateFriction(lm, lr,	x, b, &changed, &changed, stats);
    }
#endif
  }

  template<int N, class T>
  bool ContactConstraint<N, T>::valid(const VectorC2<T>& v)const{
#ifdef ACTIVE_CONSTRAINTS
    return valid2(v);
#endif
    int row = this->row_id;//v.constraints.reverseIndex(this->row_id);
    int height = v.getSize();

    T lambda = v[height + row * this->offset];

    return lambda > 1e-4;
  }

  template<int N, class T>
  bool ContactConstraint<N, T>::valid2(const VectorC2<T>& v)const{
    int row = this->row_id;//v.constraints.reverseIndex(this->row_id);
    int height = v.getSize();

    T lambda = v[height + row * this->offset];

    DBG(message("lambda = %10.10e", lambda));

    return lambda > 0;
  }

  /*initialize constraint*/
  template<int N, class T>
  void ContactConstraint<N, T>::init(SpMatrixC2<N, T>* owner){
    AbstractMatrixConstraint<N, T>::init(owner);
    /*Copy matrices*/
#ifndef FRICTION
    normals[0][1].set(0,0,0,0);
    normals[1][1].set(0,0,0,0);
    normals[2][1].set(0,0,0,0);
    normals[3][1].set(0,0,0,0);
    normals[4][1].set(0,0,0,0);

    normals[0][2].set(0,0,0,0);
    normals[1][2].set(0,0,0,0);
    normals[2][2].set(0,0,0,0);
    normals[3][2].set(0,0,0,0);
    normals[4][2].set(0,0,0,0);
#endif

    normalsT[0] = normals[0].transpose();
    normalsT[1] = normals[1].transpose();
    normalsT[2] = normals[2].transpose();
    normalsT[3] = normals[3].transpose();
    normalsT[4] = normals[4].transpose();
  }

  template<int N, class T>
  void ContactConstraint<N, T>::evaluateDepth(VectorC2<T>& x,
                                              VectorC2<T>& b,
                                              bool evaluatePenetration,
                                              Vector4<T>& lm, Vector4<T>& lr,
                                              bool* penetrationViolated,
                                              bool* changed,
                                              bool* active, int height,
                                              int row,
                                              bool removeDuplicates,
                                              EvalStats& stats){
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)blockConstraint;

    if(evaluatePenetration){
      /*Evaluate distance with multiplier value*/
      /*Cache last values*/

#ifdef DEACTIVATE_CONSTRAINT
      if(this->status == Active &&
         removeDuplicates &&
         lr[0] < -DISTANCE_TOL){

        this->status = Inactive;
        *changed = true;
        stats.n_penetration_check++;

        /*Reset multipliers*/
        cache   = x[height + row * this->offset + 0];
        cacheT1 = x[height + row * this->offset + 1];
        cacheT2 = x[height + row * this->offset + 2];

        for(int i=0;i<this->offset;i++){
          x[height + row * this->offset + i] = 0;
        }
        if(active){
          *active = true;
        }
        if(penetrationViolated){
          *penetrationViolated = true;
        }
        error("called?");
        return;
      }
#endif

      DBG(message("\t[%d] NP evaluate, %10.10e, %10.10e",
                  this->row_id, lr[0], lm[0]));

      //if(this->status == Active){
        averageNPMagnitude.addSample(lm[0]);
        //}

      if(
#ifdef ACTIVE_CONSTRAINTS
         //GAUSS-SEIDEL method
         (lr[0] <=  /*-1E-5*/    1e-6*0) &&  /*Penetration depth*/
         (lm[0] <=  /*-1E-6*/    1e-6*0)     /*Multiplier value*/
#else
#ifdef METHOD_SP
         (lr[0] <=  /*-1E-5*/    0*JV_EPS) &&  /*Penetration depth*/
         (lm[0] <=  /*-1E-6*/    0*LAMBDA_EPS) /*Multiplier value*/
#else
         (lr[0] <=  /*-1E-5*/    0*JV_EPS/(T)1.0) &&  /*Penetration depth*/
         (lm[0] <=  /*-1E-6*/    0*LAMBDA_EPS/(T)1.0) /*Multiplier value*/
#endif
#endif//ACTIVE_CONSTRAINTS
         ){

        /*Negative penetration depth -> no penetration and a negative
          multiplier -> a non active constraint, for smooth
          convergence an epsilon is required. If not, the solver tries
          to reach 0 for the distance or the multiplier resulting in
          failure.*/

        *penetrationViolated = true;
#ifdef ACTIVE_CONSTRAINTS
        /*In order to remove a constraint in the GS method*/
        this->status = Inactive;
        return;
#endif//ACTIVE_CONSTRAINTS

#if 0
        T relativeError = 0;

        /*Measure difference due to constraint deactivation*/
        for(int j=0;j<5;j++){
          if(this->index[j] == Constraint::undefined){
            continue;
          }

          Vector4<T> diag;
          Vector4<T> diff = -normals[j][0] * lm[0];
          Vector4<T> diff2 = diff;

          for(int k=0;k<3;k++){
            int idx = this->index[j]*3+k;
            diag[k] = (T)1.0 / (*this->matrix)[idx][idx];
          }

          for(int k=0;k<3;k++){
            diff2  *= diag[k];
          }

          T nom = dot(diff, diff2);

          if(Abs(frictionNorm) < 1e-9){
            relativeError = Max(nom, relativeError);
          }else{
            relativeError = Max(nom/(T)frictionNorm, relativeError);
          }

          if(this->status == Active){
            START_DEBUG;
            message("multipliers");
            std::cout << lm;
            message("distances");
            std::cout << lr;
            message("NP nom = %10.10e, denom = %10.10e", nom, frictionNorm);
            message("NP relative error of kinetic friction = %10.10e\n\n", relativeError);
            END_DEBUG;
          }
        }
#else

        T diff = Abs(lm[0]);


        DBG(message("diff = %10.10e", diff));


        diff /= usedScale * bc->S[0][0];

        T relativeError = diff;
#endif
        /*End measure*/
        //relativeError = (T)1.0;

#ifdef DEACTIVATE_CONSTRAINT
        if( (this->status == Active && relativeError > 0*0.1*RERROR/*this->bb2norm/scf*/)
            /*|| (removeDuplicates && lr[0] < -DISTANCE_TOL)*/ ){
          this->status = Inactive;
          (warning("status -> inactive, %10.10e, %10.10e, %d", lr[0], lm[0], row));
          (message("\t[%d] NP switch to inactive, %10.10e, %10.10e, %10.10e, %10.10e",
                      this->row_id, lr[0], lm[0], relativeError, RERROR*this->bb2norm/scf));
          DBG(message("RERROR = %10.10e, bb2norm = %10.10e, scf = %10.10e", RERROR, this->bb2norm, scf));
          DBG(printIndices(std::cout));
          *changed = true;
          stats.n_penetration_check++;
          /*Reset multipliers*/
          cache   = x[height + row * this->offset + 0]/(T)1.0;
          cacheT1 = x[height + row * this->offset + 1]/(T)1.0;
          cacheT2 = x[height + row * this->offset + 2]/(T)1.0;

#ifdef FRICTION_CONE
          cumulativeVector = kineticVector;
#endif

          for(int i=0;i<this->offset;i++){
            x[height + row * this->offset + i] = 0;
          }

          if(removeDuplicates){
            error("called??");
          }
        }

#else
        *changed = true;
        stats.n_penetration_check++;
#endif//DEACTIVATE_CONSTRAINT
        if(active){
#ifdef DEACTIVATE_CONSTRAINT
          *active = true;
#else
          *active = false;
#endif//DEACTIVATE_CONSTRAINT
        }
      }else{
#ifdef ACTIVE_CONSTRAINTS
        this->status = Active;
        return;
#endif

#ifdef DEACTIVATE_CONSTRAINT
        if(this->status == Inactive
#ifdef METHOD_SP
           && lr[0] > 0*JV_EPS
#else
           && lr[0] > 0*0.25*JV_EPS/1.0
#endif
           ){
          this->status = Active;
          (warning("status -> active, %10.10e, %10.10e, %d", lr[0], lm[0], row));
          (message("\t[%d] NP switch to active, %10.10e, %10.10e",
                   this->row_id, lr[0], lm[0]));
          DBG(printIndices(std::cout));
          *changed = true;
          stats.n_penetration_check++;

#ifdef FRICTION_CONE
#if 0
          kineticVector = cumulativeVector;
          kineticVector *= 0.0;
          cumulativeVector *= 0.0;

          averageFrictionDirection.clear();
          averageFrictionMagnitude.clear();

          if(kineticVector.length() > 1e-14){
            kineticVector.normalize();
          }else{
            kineticVector[1] = lr[1];
            kineticVector[2] = lr[2];

            if(kineticVector.length() > 1e-14){
              kineticVector.normalize();
            }else{
              kineticVector.set(0,1,1,0);
              kineticVector.normalize();
            }
          }
#endif
#endif
          /*Reset all multipliers*/
#ifdef METHOD_SP
          lm[0] = 0*(cache)/(T)2.0;//Abs(lm[0])*0;
          lm[1] = 0*cacheT1;//Abs(lm[0])*0;
          lm[2] = 0*cacheT2;//Abs(lm[0])*0;

#else
          lm[0] = Abs(lm[0])*0;
          lm[1] = acceptedMultipliers[1]*0;
          lm[2] = acceptedMultipliers[2]*0;
#endif

#ifdef METHOD_CR
#ifndef FRICTION_CONE
          lm[0] = 0*cache*(T)0.95;//Abs(lm[0])*0;
          lm[1] = 0*cacheT1;//Abs(lm[0])*0;
          lm[2] = 0*cacheT2;//Abs(lm[0])*0;
#else
          lm[0] = averageNPMagnitude.getAverage();//(T)0.1*(T)0.95*cache/(T)1.0;//Abs(lm[0])*0;
          lm[1] = 0*cacheT1;//Abs(lm[0])*0;
          lm[2] = 0*cacheT2;//Abs(lm[0])*0;
#endif
#endif

          x[height + row * this->offset+0] = lm[0];
          x[height + row * this->offset+1] = lm[1];
          x[height + row * this->offset+2] = lm[2];

#ifdef METHOD_SP
          /*Do not touch friction states!!!*/
          //x[height + row * this->offset+1] = lm[1]*0;
          //x[height + row * this->offset+2] = lm[2]*0;

          //setMultipliers[1] = 0;
          //setMultipliers[2] = 0;

          //acceptedMultipliers[1] = setMultipliers[1];
          //acceptedMultipliers[2] = setMultipliers[2];
#else

          //x[height + row * this->offset + 0] = cache;
#if 0
#ifdef FRICTION_CONE
          T lastGamma = (T)0.0;

          for(int i = 0; i < HISTORY_SIZE; i++){
            lastGamma += magnitudeHistory[i];
          }
          lastGamma /= (T) HISTORY_SIZE;

          //lastGamma /= (T)3.0;

          //lastGamma = 0.0;

          setMultipliers[1] = kineticVector[1] * lastGamma;
          setMultipliers[2] = kineticVector[2] * lastGamma;

          acceptedMultipliers[1] = setMultipliers[1];
          acceptedMultipliers[2] = setMultipliers[2];
#else/*FRICTION_CONE*/
          setMultipliers[1] = Pos2(mu * lm[0]);
          setMultipliers[2] = Pos2(mu * lm[0]);

          acceptedMultipliers[1] = acceptedMultipliers[2] = setMultipliers[1];
#endif/*FRICTION_CONE*/
#endif
          /*Set initial state to kinetic friction*/
          frictionStatus[0] = Kinetic;
          frictionStatus[1] = Kinetic;

#ifndef FRICTION_CONE
          /*Correct tangent vectors*/
          for(int l=0;l<2;l++){
            if(lr[l+1] < 0){
              for(int j=0;j<5;j++){
                for(int k=0;k<3;k++){
                  normalsT[j][k][l+1] *= -1;
                }
                normals[j][l+1] *= -1;
              }
              lr[l+1] *= -1;   //Negate Hv
              //negate last gamma value
              lm[l+1] *= -1;
              if(l==0){
                cacheT1 *= -1;
              }else{
                cacheT2 *= -1;
              }
              x[height + row * this->offset + l+1] *= -1;
              c[l+1] *= -1;
              b[height + row * this->offset + l+1] *= -1;
            }
          }
#endif/*FRICTION_CONE*/
#endif/*METHOD_SP*/
        }
#endif//DEACTIVATE_CONSTRAINT
        if(active){
          *active = true;
        }
      }
    }else if(evaluatePenetration && false){
#ifdef DEACTIVATE_CONSTRAINT
      /*Keep constraint, although it can not change*/
      if(active){
        *active = true;
      }
#endif//DEACTIVATE_CONSTRAINT
    }else{
#ifdef ACTIVE_CONSTRAINTS
      this->status = Active;
#endif//ACTIVE_CONSTRAINTS
    }
  }

  template<int N, class T>
  void ContactConstraint<N, T>::forceEvaluateDepth(VectorC2<T>& x,
                                                   VectorC2<T>& b,
                                                   bool evaluatePenetration,
                                                   Vector4<T>& lm,
                                                   Vector4<T>& lr,
                                                   bool* penetrationViolated,
                                                   bool* changed,
                                                   bool* active, int height,
                                                   int row,
                                                   bool removeDuplicates,
                                                   EvalStats& stats){
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)blockConstraint;

    PRINT_FUNCTION;
    *active = true;
    if(evaluatePenetration){
      /*Evaluate distance with multiplier value*/
      /*Cache last values*/

#ifdef DEACTIVATE_CONSTRAINT
      if(this->status == Active &&
         removeDuplicates &&
         lr[0] < -DISTANCE_TOL*0){
        this->status = Inactive;
        message("status -> inactive");
        warning("Forced to inactive %d", row);
        std::cerr << lm << std::endl;
        std::cerr << lr << std::endl;
        error("called?");
        *changed = true;
        stats.n_penetration_force++;
        /*Reset multipliers*/
        cache   = x[height + row * this->offset + 0];
        cacheT1 = x[height + row * this->offset + 1];
        cacheT2 = x[height + row * this->offset + 2];

        for(int i=0;i<this->offset;i++){
          x[height + row * this->offset + i] = 0;
        }
        if(active){
          *active = true;
        }
        if(penetrationViolated){
          *penetrationViolated = true;
        }
        return;
      }
#endif//DEACTIVATE_CONSTRAINT

      DBG(message("\t[%d] NP forced evaluate, %10.10e, %10.10e",
                  this->row_id, lr[0], lm[0]));

      if(
#ifdef ACTIVE_CONSTRAINTS
         //GAUSS-SEIDEL method
         (lr[0] <=  /*-1E-5*/    1e-6*0) &&  /*Penetration depth*/
         (lm[0] <=  /*-1E-6*/    1e-6*0)     /*Multiplier value*/
         //error("called?");
#else
#ifdef METHOD_SP
         (lm[0] <= -1e-4*0)
         //&&
         //(lr[0] <=  /*-1E-5*/    0*JV_EPS)   /*Penetration depth*/
#else
         (lm[0] <=  0*LAMBDA_EPS)
         //&&
         //(lr[0] <=  /*-1E-5*/    JV_EPS)   /*Penetration depth*/
         //(lm[0] <=  /*-1E-6*/    LAMBDA_EPS) /*Multiplier value*/
#endif
#endif//ACTIVE_CONSTRAINTS
         ){//

        /*Negative penetration depth -> no penetration and a negative
          multiplier -> a non active constraint, for smooth
          convergence an epsilon is required. If not, the solver tries
          to reach 0 for the distance or the multiplier resulting in
          failure.
        */

        if(this->status == Active){
        *penetrationViolated = true;
        DBG(message("Active constraint forced, l = %10.10e", lm[0]));
#ifdef ACTIVE_CONSTRAINTS
        /*In order to remove a constraint in the GS method*/
        this->status = Inactive;
        return;
#endif//ACTIVE_CONSTRAINTS

#if 1
        T relativeError = 0;

        /*Measure difference due to constraint deactivation*/
        for(int j=0;j<5;j++){
          if(this->index[j] == Constraint::undefined){
            continue;
          }

          Vector4<T> diag;
          Vector4<T> diff = -normals[j][0] * lm[0];
          Vector4<T> diff2 = diff;

          for(int k=0;k<3;k++){
            int idx = this->index[j]*3+k;
            diag[k] = (T)1.0 / (*this->matrix)[idx][idx];
          }

          for(int k=0;k<3;k++){
            diff2  *= diag[k];
          }

          T nom = dot(diff, diff2);

          if(Abs(frictionNorm) < 1e-9){
            relativeError = Max(nom, relativeError);
          }else{
            relativeError = Max(nom/(T)frictionNorm, relativeError);
          }

          if(this->status == Active){
            START_DEBUG;
            message("multipliers");
            std::cout << lm;
            message("NP nom = %10.10e, denom = %10.10e", nom, frictionNorm);
            message("NP relative error of kinetic friction = %10.10e\n\n", relativeError);
            END_DEBUG;
          }
        }


        T diff = Abs(lm[0]);


        DBG(message("diff = %10.10e", diff));

        diff /= usedScale * bc->S[0][0];

        relativeError = diff;
#endif

        //relativeError = (T)1.0;

#ifdef DEACTIVATE_CONSTRAINT
        /////Modified!!!
        if((this->status == Active && relativeError > 0.1*RERROR /* this->bb2normCheck*/) /*|| lm[0] < (T)0.0*/){

          DBG(message("\t[%d] NP force to inactive, %10.10e, %10.10e, %10.10e",
                   this->row_id, lr[0], lm[0], relativeError));
          DBG(printIndices(std::cout));

          this->status = Inactive;
          DBG(message("FORCED status -> inactive %d", this->row_id));
          *changed = true;
          stats.n_penetration_force++;

          START_DEBUG;
          warning("Forced to inactive %d", this->row_id);
          std::cerr << lm << std::endl;
          std::cerr << lr << std::endl;
          END_DEBUG;

          /*Reset multipliers*/
          cache   = x[height + row * this->offset + 0];
          cacheT1 = x[height + row * this->offset + 1];
          cacheT2 = x[height + row * this->offset + 2];

          //cumulativeVector = kineticVector;

          for(int i=0;i<this->offset;i++){
            x[height + row * this->offset + i] = 0;
          }
        }
#else
        *changed = true;
        stats.n_penetration_force++;
#endif//DEACTIVATE_CONSTRAINT
        if(active){
#ifdef DEACTIVATE_CONSTRAINT
          *active = true;
#else
          *active = false;
#endif//DEACTIVATE_CONSTRAINT
        }
        }
      }else if(lr[0] >  0*JV_EPS/1.0 && this->status !=  Active){
#ifdef ACTIVE_CONSTRAINTS
        this->status = Active;
        return;
#endif//ACTIVE_CONSTRAINTS

#ifdef DEACTIVATE_CONSTRAINT

#if 1
        if(this->status == Inactive){
          this->status = Active;
          DBG(warning("FORCED status -> active %d", this->row_id));

          (message("\t[%d] NP force to active, %10.10e, %10.10e",
                      this->row_id, lr[0], lm[0]));
          (printIndices(std::cout));

          START_DEBUG;
          std::cerr << lm << std::endl;
          std::cerr << lr << std::endl;
          END_DEBUG;

          *changed = true;
          stats.n_penetration_force++;
#ifdef METHOD_SP
          /*Do not touch friction states!!*/
#else

#ifdef FRICTION_CONE
#if 0
          kineticVector = cumulativeVector;
          kineticVector *= 0.0;
          cumulativeVector *= 0.0;

          averageFrictionDirection.clear();
          averageFrictionMagnitude.clear();

          if(kineticVector.length() > 1e-6){
            kineticVector.normalize();
          }else{
            kineticVector[1] = lr[1];
            kineticVector[2] = lr[2];
            if(kineticVector.length() > 1e-9){
              kineticVector.normalize();
            }else{
              kineticVector.set(0,1,1,0);
              kineticVector.normalize();
            }
          }
#endif
          PRINT(kineticVector);
#endif//FRICTION_CONE
#endif//METHOD_SP

          /*Reset all multipliers*/
#ifdef METHOD_SP
          lm[0] = 0*(cache)/(T)2.0;
          lm[1] = 0*cacheT1;
          lm[2] = 0*cacheT2;
#else
          lm[0] = Abs(lm[0])*0;
          lm[1] = acceptedMultipliers[1]*0;
          lm[2] = acceptedMultipliers[2]*0;
#endif

#ifdef METHOD_CR
#ifndef FRICTION_CONE
          lm[0] = cache*(T)0.95;
          lm[1] = 0*cacheT1;
          lm[2] = 0*cacheT2;
#else
          lm[0] = -cache*(T)0.5;
          lm[1] = -cacheT1;
          lm[2] = -cacheT2;
          //lm[0] = lm[0];
          //lm[1] = acceptedMultipliers[1];
          //lm[2] = acceptedMultipliers[2];
#endif
#endif

          x[height + row * this->offset+0] = lm[0];
          x[height + row * this->offset+1] = lm[1];
          x[height + row * this->offset+2] = lm[2];

          //x[height + row * this->offset + 0] = cache;
#ifdef METHOD_SP
          /*Do not touch friction states!!*/
#else/*METHOD_SP*/

#ifdef FRICTION_CONE
#if 0
          T lastGamma = (T)0.0;

          for(int i = 0; i < HISTORY_SIZE; i++){
            lastGamma += magnitudeHistory[i];
          }

          lastGamma /= (T) HISTORY_SIZE;

          //lastGamma /= (T)3.0;

          //lastGamma = 0.0;

          setMultipliers[1] = kineticVector[1] * lastGamma;
          setMultipliers[2] = kineticVector[2] * lastGamma;

          acceptedMultipliers[1] = setMultipliers[1];
          acceptedMultipliers[2] = setMultipliers[2];
#endif
#else/*FRICTION_CONE*/
          setMultipliers[1] = Pos2(mu * lm[0]);
          setMultipliers[2] = Pos2(mu * lm[0]);

          acceptedMultipliers[1] = acceptedMultipliers[2] = setMultipliers[1];
#endif/*FRICTION_CONE*/

          /*Set initial state to kinetic friction*/
          frictionStatus[0] = Kinetic;
          frictionStatus[1] = Kinetic;

#ifndef FRICTION_CONE
          /*Correct tangent vectors*/
          for(int l=0;l<2;l++){
            if(lr[l+1] < 0){
              for(int j=0;j<5;j++){
                for(int k=0;k<3;k++){
                  normalsT[j][k][l+1] *= -1;
                }
                normals[j][l+1] *= -1;
              }
              lr[l+1] *= -1;   //Negate Hv
              //negate last gamma value
              lm[l+1] *= -1;
              x[height + row * this->offset + l+1] *= -1;
              c[l+1] *= -1;
              b[height + row * this->offset + l+1] *= -1;
            }
          }
#endif/*FRICTION_CONE*/
#endif/*METHOD_SP*/
        }
#endif//DEACTIVATE_CONSTRAINT
        if(active){
          *active = true;
        }
      }
#endif
    }else if(evaluatePenetration && false){
#ifdef DEACTIVATE_CONSTRAINT
      /*Keep constraint, although it can not change*/
      if(active){
        *active = true;
      }
#endif//DEACTIVATE_CONSTRAINT
    }else{
#ifdef ACTIVE_CONSTRAINTS
      this->status = Active;
#endif//ACTIVE_CONSTRAINTS
    }
  }

  template<int N, class T>
  bool ContactConstraint<N, T>::project(VectorC2<T>& x,
                                        VectorC2<T>& b, T meps,
                                        const EvalType& etype,
                                        bool* active,
                                        VectorC2<T>* kb){
    int row = this->row_id;

    T* xdatax = x.getExtendedData();
    //T* xdata  = x.getData();

    /*NP*/
    if(xdatax[row * this->offset + 0] < (T)0.0){
      xdatax[row * this->offset + 0] = (T)0.0;
    }

    /*Friction*/
    if(frictionStatus[0] == Kinetic){
      T length = Sqrt(Sqr(xdatax[row * this->offset + 1]) +
                      Sqr(xdatax[row * this->offset + 2]));

      if(length > xdatax[row * this->offset + 0] * this->mu){
        Vector4<T> dir(xdatax[row * this->offset + 1],
                       xdatax[row * this->offset + 2],
                       0,0);

        dir.normalize();
        dir *= mu * xdatax[row * this->offset + 0];

        xdatax[row * this->offset + 1] = dir[0];
        xdatax[row * this->offset + 2] = dir[1];
      }
    }

    return false;
  }

  /*Evaluates Hx - b <=> lambda*/
  template<int N, class T>
  bool ContactConstraint<N, T>::evaluate(VectorC2<T>& x,
                                         VectorC2<T>& b, T meps,
                                         const EvalType& etype,
                                         EvalStats& stats,
                                         bool* active,
                                         VectorC2<T>* kb){
    bool changed = false;
#ifdef FRICTION_APPROX
    bool frictionChanged = false;
#endif
    bool evaluatePenetration = true;
    bool evaluateFriction = true;
    bool updateKineticFriction = false;
    bool acceptFriction = false;
#ifdef FRICTION_APPROX
    bool evForceFriction = false;
#endif
    bool updateKineticVector = false;
    bool evForcePenetration = false;
    bool removeDuplicates = false;

    evaluatePenetration   = etype.penetrationCheck();
    evaluateFriction      = etype.frictionCheck();
    updateKineticFriction = etype.updateKineticFrictionCheck();
    acceptFriction        = etype.acceptFrictionCheck();

#ifdef FRICTION_APPROX
    evForceFriction       = etype.forceFrictionCheck();
#endif
    updateKineticVector   = etype.updateKineticVectorCheck();
    evForcePenetration    = etype.forcePenetrationCheck();
    removeDuplicates      = etype.forceDuplicateRemovalCheck();

#ifdef FRICTION_APPROX
    if(acceptFriction && (frictionStatus[0] == Kinetic ||
                          frictionStatus[1] == Kinetic) &&
       this->status == Active){
      /*Update acceptFriction with latest values, and then return*/

      if(frictionStatus[0] == Kinetic){
        acceptedMultipliers[1] = setMultipliers[1];
      }
      if(frictionStatus[1] == Kinetic){
        acceptedMultipliers[2] = setMultipliers[2];
      }

#ifdef FRICTION_CONE
      if(frictionStatus[0] == Kinetic){// && updateKineticVector){
        acceptedKineticVector = kineticVector;

        DBG(message("acepted values = %10.10e, %10.10e", acceptedMultipliers[1], acceptedMultipliers[2]));
        DBG(warning("accept friction, update history"));



        Vector4<T> lr, lc, lm;

        loadLocalValues(x, b, lm, lr, lc);

        Vector4<T> HV(0,lr[1], lr[2], 0);
        T length = HV.length();

        HVDiff = Abs(lastHV - length);
        DBG(message("lastHV = %10.10e, curr = %10.10e, diff = %10.10e",
                    lastHV, length, HVDiff));

        lastHV = length;

        for(int i = HISTORY_SIZE-2; i >= 0;i--){
          magnitudeHistory[i+1] = magnitudeHistory[i];
          DBG(message("shift history [%d] = %10.10e", i+1, magnitudeHistory[i+1]));
        }

        magnitudeHistory[0] = Sqrt(Sqr(setMultipliers[1]) +
                                   Sqr(setMultipliers[2]) );
        magnitudeHistory[0] = Pos2(lm[0] * mu);
        DBG(message("new history [%d] = %10.10e", 0, magnitudeHistory[0]));
      }
#endif//FRICTION_CONE

      if(active){
        *active = true;
      }
      return false;
    }

    if(acceptFriction){
      if(active){
        *active = true;
      }
      /*Required to return after */
      return false;
    }
#else
    if(acceptFriction){
      return false;
    }
#endif//FRICTION_APPROX


    START_DEBUG;
    message("status            = %d", this->status == Active);
    message("check penetration = %d", evaluatePenetration);
    message("check friction    = %d", evaluateFriction);
    message("update kinetic    = %d", updateKineticFriction);
    message("update kinetic vec= %d", updateKineticVector);
    message("remove duplicates = %d", removeDuplicates);
    message("force penetration = %d", evForcePenetration);
#ifdef FRICTION_APPROX
    message("force friction    = %d", evForceFriction);
#endif
    message("accept friction   = %d", acceptFriction);
    END_DEBUG;




    Vector4<T> lr, lc, lm;

    loadLocalValues(x, b, lm, lr, lc);

    int height = this->matrix->getHeight();
    int row = this->row_id;//b.constraints.reverseIndex(this->row_id);

    START_DEBUG;
    message("multiplier value = %10.10e", lm[0]);
    message("depth      value = %10.10e", lr[0]);
    message("distance   value = %10.10e", lr[0] + lc[0]);
    message("if m < 0 && d < 0 -> not active");
    message("multiplier epsilon = %10.10e", meps);
    END_DEBUG;

    bool penetrationViolated = false;

    if(evForcePenetration){
      forceEvaluateDepth(x, b, evaluatePenetration, lm, lr,
                         &penetrationViolated,
                         &changed, active, height, row, removeDuplicates,
                         stats);
      //message("changed = %d", changed);
    }else{
      evaluateDepth(x, b, evaluatePenetration, lm, lr, &penetrationViolated,
                    &changed, active, height, row, removeDuplicates, stats);
    }

#ifdef FRICTION_APPROX
    if(evaluateFriction && this->status == Active && mu != (T)0.0){
      if(evForceFriction){
        forceFriction(lm, lr, x, b, &changed, &frictionChanged, stats);
      }else{
        evaluateAndUpdateFriction(lm, lr, x, b, &changed, &frictionChanged,
                                  stats);
      }

      if(updateKineticFriction){
        evaluateKineticFriction(lm, lr, x, b, updateKineticFriction,
                                updateKineticVector,
                                &changed, &frictionChanged, stats);

        frictionChanged = false;
      }
    }

    updateRHS(kb);
#endif

    return changed;
  }

  template<int N, class T>
  void ContactConstraint<N, T>::updateRHS(VectorC2<T>* kb)const{
    if(kb == 0){
      return;
    }

#ifdef FRICTION_APPROX
    if(this->status == Inactive){
      return;
    }

    for(int i=0;i<2;i++){
      if(frictionStatus[i] == Kinetic){
        //Move to RHS
        for(int j=0;j<5;j++){
          Vector4<T> res = -normals[j][i+1] * setMultipliers[i+1];
          //Update kb;

          if(this->index[j] == Constraint::undefined){
            continue;
          }
          int row = this->index[j]*3;

          cgfassert(kb != 0);

          for(int k=0;k<3;k++){
            (*kb)[row+k] += res[k];
          }
        }
      }
    }
#endif
  }

  template<int N, class T>
  void ContactConstraint<N, T>::multiply(VectorC2<T>& r,
                                         const VectorC2<T>& x)const{
#ifndef ACTIVE_CONSTRAINTS
    ConstraintMatrixStatus status = this->matrix->getStatus();

    if(status == Individual){
      if(this->status == Inactive){
        return;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }
#endif

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size   = this->getSize();

    const T* xdata  = x.getData();
    T*       rdatax = r.getExtendedData();

    Vector4<T> lr;
    Vector4<T> lcm;

    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }

      Vector4<T> lx;
      int column = this->index[j]*3;

      /*Multiply if this constraint is active, or its owner has
        enabled all constraints.*/
      if(true){
        for(int i=0;i<3;i++){
          lx[i] = xdata[column+i];
        }

        lr += normals[j] * lx;

#ifdef FRICTION
        //Compute Hv
#if defined(ACTIVE_CONSTRAINTS)

#else //Perform multiplication regarding state of constraint

#ifdef FRICTION_APPROX
        //Ignore active constraints
        if(status == AllActive){
          //Keep values
        }else if(status == Individual){
          if(frictionStatus[0] == Kinetic){
            lr[1] = 0;
          }
          if(frictionStatus[1] == Kinetic){
            lr[2] = 0;
          }
        }
#endif //FRICTION_APPROX
#endif //ACTIVE_CONSTRAINTS
#endif /*FRICTION*/
      }

      for(int i=0;i<3;i++){
        rdatax[cindex * offset + i] = lr[i];
      }
    }
  }

  template<int N, class T>
  void ContactConstraint<N, T>::multiplyTransposed(VectorC2<T>& r,
                                                   const VectorC2<T>& x)const{
    Vector4<T> lx;
    Vector4<T> lcm;

#ifndef ACTIVE_CONSTRAINTS
    ConstraintMatrixStatus status = this->matrix->getStatus();

    if(status == Individual){
      if(this->status == Inactive){
        return;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }
#endif

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size   = this->getSize();

    const T* xdatax = x.getExtendedData();
    T*       rdata  = r.getData();

    for(int i=0;i<3;i++){
#ifdef FRICTION_APPROX
      //Ignore active constraints
      if(i>0){
        if(status == Individual){
          if(frictionStatus[i-1] == Kinetic){
            //Skip active constraint
            continue;
          }
        }else if(status == AllActive){
          //do not stop and load the multiplier
        }
      }
#endif
      lx[i] = xdatax[cindex * offset + i];

#ifndef FRICTION
      break; /*Quit after one constraint*/
#endif/*FRICTION*/
    }

    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }

      if(true){
        int row    = this->index[j]*3;
        Vector4<T> res = normalsT[j] * lx;

        for(int i=0;i<3;i++){
          rdata[row+i] += res[i];
        }
      }
    }
  }

  template<int N, class T>
  void ContactConstraint<N, T>::computePreconditioner(VectorC2<T>& C)const{
    this->preconditioner = &C;

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size = C.getSize();

    for(int i=0;i<3;i++){
      /*Clear constraint part in diagonal preconditioner, these values
        are stored in the precondtioner matrix.*/
      C[size + cindex * offset + i] = 0.0;
    }
  }

#define FULL_BLOCK
#undef  FULL_BLOCK

  template<int N, class T>
  void ContactConstraint<N, T>::computePreconditionerMatrix(VectorC2<T>& DD,
                                                            SpMatrixC2<N, T>& C,
                                                            const SpMatrixC2<N, T>& B,
                                                            AdjacencyList* l,
                                                            VectorC2<T>* scaling)const{
    ContactBlockConstraint<N, T>* c =
      (ContactBlockConstraint<N, T>*)C.getConstraint(this->row_id);


    if(this->status == Inactive){
      //return;
    }

    c->cSize = this->getSize();
    int size = this->getSize();

    /*Copy all indices*/
    for(int i=0;i<size;i++){
      c->setIndex(i, getIndex(i));
    }

    /*Compute S*/
#if 1 //S

    c->S.clear();
    c->OS.clear();
    /*Since we only use the diagonal terms of S =
      (H*A^{-1}*H^T)^{-1}, we only compute these terms.*/

    /*If H = a b c | d e f
             g h i | j k l
             m n o | p q r  (having size = 2) the diagonal terms of S
             are computed as:

                                   1
                ---------------------------------------
          s1 =  a^2    b^2    c^2    d^2    e^2    f^2 ,
                ---- + ---- + ---- + ---- + ---- + ----
                z1     z2     z3     z4     z5     z6

                                   1
                ---------------------------------------
          s2 =  g^2    h^2    i^2    j^2    k^2    l^2 ,
                ---- + ---- + ---- + ---- + ---- + ----
                z1     z2     z3     z4     z5     z6

                                   1
                ---------------------------------------
          s3 =  m^2    n^2    o^2    p^2    q^2    r^2 ,
                ---- + ---- + ---- + ---- + ---- + ----
                z1     z2     z3     z4     z5     z6

          with z1-6 the corresponding diagonal values in A.
        */

    Matrix44<T> tmp;

    T maxScale = 1000.0;
    T avgScale = 0.0;
    int avgCount = 0;

    for(int j=0;j<size;j++){
      if(getIndex(j) == Constraint::undefined){
        continue;
      }

      Matrix44<T> diag;

      //int degree = l->getDegree(getIndex(j));

      int col = getIndex(j)*3;

      for(int k=0;k<3;k++){
#ifdef FULL_BLOCK
        for(int l=0;l<3;l++){
          diag[k][l] = B[col+k][col+l];
        }

        DD[col+k] = 0;
#else
        diag[k][k] = DD[col+k];
#endif
      }

#ifdef FULL_BLOCK
      diag[3][3] = 1;
      diag = diag.inverse();
      diag[3][3] = 0;

      PRINT(diag);

      for(int k=0;k<3;k++){
        for(int l=0;l<3;l++){
          C[col+k][col+l] = diag[k][l];
        }
      }
#endif


      tmp += normals[j] * diag * normalsT[j];// * (*scaling)[col];
      DBG(message("scaling for %d = %10.10e", col, (*scaling)[col]));
      maxScale = Max(maxScale, (*scaling)[col]);
      avgScale += (*scaling)[col];
      avgCount++;
    }

#if 1
    if(tmp[0][0] == (T)0.0){
      tmp[0][0] = 1.0;
    }
    if(tmp[1][1] == (T)0.0){
      tmp[1][1] = 1.0;
    }
    if(tmp[2][2] == (T)0.0){
      tmp[2][2] = 1.0;
    }
#endif

    PRINT(tmp);

#ifdef FULL_BLOCK
    tmp[3][3] = 1;
    tmp = tmp.inverse();
    PRINT(tmp);
    tmp[3][3] = 0;
    c->S = tmp;
    c->OS = tmp;
#else

#ifdef PREC_FULL
    T* xdata = scaling->getExtendedData();
    T factor = xdata[this->row_id * this->getOffset()];
#else
    T factor = (T)1.0;
#endif

    if(factor != (T)1.0){
      //(warning("Scale factor = %10.10e", factor));
    }

    DBG(message("maxScale = %10.10e", maxScale));

    if(factor < (T)1.0){
      factor = (T)1.0;
    }

    //usedScale = Sqrt(factor * (T)2.0);
    usedScale = Max(factor, (T)1.5);// * (T)1.5;
    //usedScale = Sqr(factor*(T)2.0);
    bool singular = false;
    for(int j=0;j<3;j++){
      c->S[j][j] = ((T)1.0/(tmp[j][j]))/(usedScale);
      c->OS[j][j] = ((T)1.0/(tmp[j][j]))/(usedScale);
      if(IsNan(c->S[j][j]) || IsNan(c->OS[j][j])){
        singular = true;
      }
    }

    if(singular){
      std::cout << tmp << std::endl;
      std::cout << c->S << std::endl;
      std::cout << c->OS << std::endl;
      print(std::cout);

      for(int j=0;j<size;j++){
        if(getIndex(j) == Constraint::undefined){
          continue;
        }

        Matrix44<T> diag;

        int col = getIndex(j)*3;

        for(int k=0;k<3;k++){
#ifdef FULL_BLOCK
          for(int l=0;l<3;l++){
            diag[k][l] = B[col+k][col+l];
          }

          DD[col+k] = 0;
#else
          diag[k][k] = DD[col+k];
#endif
        }
        std::cout << diag << std::endl;
      }

      error("singular contact preconditioner");
    }
#endif

#endif //S

#if 1 //SHA
      /*Compute SHA*/

      /*If H = a b c | d e f
               g h i | j k l
               m n o | p q r  (having size = 2) S*H*A^{-1}
        is computed as:

        s1*a, s1*b, s1*c, s1*d, s1*e, s1*f
        ----  ----  ----  ----  ----  ----
        z1    z2    z3    z4    z5    z6

        s2*g, s2*h, s2*i, s2*j, s2*k, s2*l
        ----  ----  ----  ----  ----  ----
        z1    z2    z3    z4    z5    z6

        s3*m, s3*n, s3*o, s3*p, s3*q, s3*r
        ----  ----  ----  ----  ----  ----
        z1    z2    z3    z4    z5    z6

        with s1-3 the corresponding values of matrix S and
        z1-6 the corresponding diagonal elements in A

      */

    for(int i=0;i<size;i++){
      if(getIndex(i) == Constraint::undefined){
        continue;
      }

      int col = getIndex(i)*3;

      Matrix44<T> diag;

      for(int j=0;j<3;j++){
#ifdef FULL_BLOCK
        for(int k=0;k<3;k++){
          diag[j][k] = B[col+j][col+k];
        }
#else
        diag[j][j] = (DD[col+j]);
#endif
      }

#if 1
#ifdef FULL_BLOCK
      diag[3][3] = 1;
      diag = diag.inverse();
      diag[3][3] = 0;
#endif
#endif
      c->HA[i]  = normals[i] * diag;
      c->AHT[i] = diag * normalsT[i];

      c->SHA[i] = (c->OS * normals[i]) * diag;
      c->AHS[i] = diag * (normalsT[i] * c->OS);

      c->SHAT[i] = c->SHA[i].transpose();
      c->AHST[i] = c->AHS[i].transpose();
      c->HAT[i]  = c->HA[i].transpose();
    }
#endif //SHA
  }

  template<int N, class T>
  void ContactConstraint<N, T>::print(std::ostream& os)const{
    bool st = false;
    if(this->status == Active){
      st = true;
    }
    os << "Row = " << this->row_id << ", column[0] = " << index[0]*3 <<
      ", column[1] = " << index[1]*3 << ", column[2] = " << index[2]*3 <<
      ", column[3] = " << index[3]*3 << ", column[4] = " << index[4]*3 <<
      ", status = " << st << std::endl;

    os << "Source Type = ";
    if(this->sourceType == 1){
      os << "Edge - Edge ";
    }else if(this->sourceType == 2){
      os << "Face - Vertex ";
    }else if(this->sourceType == 3){
      os << "BackFace - Vertex ";
    }else if(this->sourceType == 4){
      os << "BackEdge - BackEdge ";
    }

    os << this->sourceType << ", A = " << this->sourceA <<
      ", B = " << this->sourceB << std::endl;

    bool f1, f2;
    f1 = f2 = false;
    if(frictionStatus[0] == Kinetic){
      f1 =true;
    }

    if(frictionStatus[1] == Kinetic){
      f2 =true;
    }

    message("this = %p", this);

    os << "f[0] = " << f1 << ", f[1] = " << f2 << ", mu = "
       << mu << ", c[0] = " << c[0]
       << ", c[1] = " << c[1]
       << ", c[2] = " << c[2] << std::endl;

    for(int i=0;i<5;i++){
      message("normals");
      os << normals[i] << std::endl;

      message("normalsT");
      os << normalsT[i] << std::endl;
    }

    if(Abs(c[0]) > 1E-2){
      warning("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      message("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      //getchar();
      //abort();
    }
    message("this = %p", this);
  }

  template<int N, class T>
  void ContactConstraint<N, T>::printIndices(std::ostream& os)const{
    bool st = false;
    if(this->status == Active){
      st = true;
    }
    os << "Row = " << this->row_id << ", column[0] = " << index[0]*3 <<
      ", column[1] = " << index[1]*3 << ", column[2] = " << index[2]*3 <<
      ", column[3] = " << index[3]*3 << ", column[4] = " << index[4]*3 <<
      ", status = " << st << std::endl;

    os << "Source Type = ";
    if(this->sourceType == 1){
      os << "Edge - Edge ";
    }else if(this->sourceType == 2){
      os << "Face - Vertex ";
    }else if(this->sourceType == 3){
      os << "BackFace - BackVertex ";
    }else if(this->sourceType == 4){
      os << "BackEdge - BackEdge ";
    }else if(this->sourceType == 8){
      os << "Edge - Vertex";
    }

    os << this->sourceType << ", A = " << this->sourceA <<
      ", B = " << this->sourceB << std::endl;

    bool f1, f2;
    f1 = f2 = false;
    if(frictionStatus[0] == Kinetic){
      f1 = true;
    }

    if(frictionStatus[1] == Kinetic){
      f2 = true;
    }

    message("this = %p", this);

    os << "f[0] = " << f1 << ", f[1] = " << f2 << ", mu = "
       << mu << ", c[0] = " << c[0]
       << ", c[1] = " << c[1]
       << ", c[2] = " << c[2] << std::endl;

    if(Abs(c[0]) > 1E-2){
      warning("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      message("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      //getchar();
      //abort();
    }
    message("used scale = %10.10e", usedScale);
    message("this = %p", this);
  }

  template<int N, class T>
  void ContactConstraint<N, T>::collectElements(List<ConstraintElement<T> >&
                                                elements)const{
    int row = this->row_id;
    int offset = 3;
    int size = this->getSize();

    for(int i=0;i<size;i++){
      if(this->getIndex(i) == Constraint::undefined){
        continue;
      }

      int column = this->getIndex(i)*3;
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          if(k>0){
            if(frictionStatus[k-1] == Kinetic){
              continue;
            }
          }
          ConstraintElement<T> element;
          element.row   = this->matrix->getHeight() + row * offset + j;
          element.col   = column + k;
          element.value = normals[i][j][k];
          elements.append(element);
        }
      }
    }
  }


  template<int N, class T>
  int ContactConstraint<N, T>::getIndex(int i)const{
    cgfassert(i>=0);
    cgfassert(i<5);
    return index[i];
  }

  template<int N, class T>
  void ContactConstraint<N, T>::setIndex(int i, int idx){
    cgfassert(i>=0);
    cgfassert(i<5);
    index[i] = idx;
  }


  template<int N, class T>
  void ContactConstraint<N, T>::computeFBFunction(VectorC2<T>& np,
                                                  VectorC2<T>& sf,
                                                  VectorC2<T>& kf,
                                                  SpMatrixC2<N, T>& C,
                                                  const VectorC2<T>& x,
                                                  const VectorC2<T>& b,
                                                  const VectorC2<T>& b2){
    ContactBlockConstraint<N, T>* c =
      (ContactBlockConstraint<N, T>*)blockConstraint;

    message("FB value comp, %d", this->row_id);
    Vector4<T> lm;
    Vector4<T> lr;
    Vector4<T> lc;

    loadLocalValues(x, b, lm, lr, lc);

    std::cout << lm;
    std::cout << lr;

    Vector4<T> fx; /*Multipliers*/
    Vector4<T> fy; /*Distances*/

    int cindex = this->row_id;
    int offset = this->getOffset();

    T* npdatax = np.getExtendedData();
    T* sfdatax = sf.getExtendedData();
    T* kfdatax = kf.getExtendedData();

    /*Non penetration*/

    if(this->status == Active){
      fx[0] = lm[0];

#ifdef FRICTION_CONE
      if(frictionStatus[0] == Kinetic){
        T diff = mu * lm[0] - Sqrt(Sqr(acceptedMultipliers[1]) + Sqr(acceptedMultipliers[2]));
        Vector4<T> dir(0, acceptedMultipliers[1], acceptedMultipliers[2], 0);
        if(dir.length() > (T)1e-14){
          dir.normalize();
          dir *= diff;// * (T)0.001;
        }
        fx[1] = Sqrt(Sqr(dir[1]) + Sqr(dir[2]));
        fx[2] = Sqrt(Sqr(dir[1]) + Sqr(dir[2]));
        message("Kinetic");
      }else{
        fx[1] = lm[1];
        fx[2] = lm[2];
      }

#else
      for(int i=0;i<2;i++){
        if(frictionStatus[i] == Kinetic){
          T diff = mu * lm[0] - Sqrt(Sqr(acceptedMultipliers[i+1]));
          fx[i+1] = diff;
        }else{
          fx[i+1] = lm[i+1];
        }
      }
#endif
      std::cout << c->S;

      fx[0] /= usedScale*c->S[0][0];
      fx[1] /= usedScale*c->S[1][1];
      fx[2] /= usedScale*c->S[2][2];


      fy[0] = lr[0];
      fy[1] = lr[1];
      fy[2] = lr[2];
    }

    message("x, y");
    std::cout << fx;
    std::cout << fy;

    message("Accepted %10.10e, %10.10e", acceptedMultipliers[1], acceptedMultipliers[2]);

    for(int i=0;i<3;i++){
      T value = Abs(fx[i]) + Abs(fy[i]) - Sqrt(Sqr(fx[i]) + Sqr(fy[i]));

      if(i==0){
        npdatax[cindex * offset + i] = value;
        message("value[%d] = %10.10e", i, npdatax[cindex * offset + i]);
      }else{
        if(frictionStatus[0] == Kinetic){
          kfdatax[cindex * offset + i] = value;
          message("value[%d] = %10.10e", i, kfdatax[cindex * offset + i]);
        }else{
          sfdatax[cindex * offset + i] = value;
          message("value[%d] = %10.10e", i, sfdatax[cindex * offset + i]);
        }
      }
    }
  }

  /*Store values in SparseMatrix*/
  template<int N, class T>
  void ContactConstraint<N, T>::extractValues(SpMatrix<N, T>* L,
                                              SpMatrix<N, T>* LT,
                                              Tree<int>* columns,
                                              int row,
                                              Vector<T>* rhs)const{
    //int row = this->row_id;
    int offset = 3;
    int size = this->getSize();

    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }

      Vector4<T> lx;
      int column = this->index[j]*3;

      for(int k=0;k<3;k++){
        for(int l=0;l<3;l++){
          int column2 = column+l;
          columns->uniqueInsert(column2, column2);

          (*L )[row * offset + k][column2] = normals[j][k][l];
          (*LT)[column2][row * offset + k] = normals[j][k][l];
        }
      }
    }
    if(rhs){
      for(int j=0;j<offset;j++){
        (*rhs)[row * offset + j] = c[j];
      }
    }
  }

#ifdef FRICTION_CONE
  template<int N, class T>
  void ContactConstraint<N, T>::evaluateKineticFriction(Vector4<T>& lm,
                                                        Vector4<T>& lr,
                                                        VectorC2<T>& x,
                                                        VectorC2<T>& b,
                                                        bool updateKinetic,
                                                        bool updateVector,
                                                        bool* changed,
                                                        bool* frictionChanged,
                                                        EvalStats& stats){
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)blockConstraint;

    if(updateKinetic &&
       //this->status == Active &&
       //Abs(lr[0]) < RERROR &&
       frictionStatus[0] == Kinetic && mu != (T)0.0){

#ifdef METHOD_SP
      error("Staggered Projections may not update kinetic friction directly");
#endif

      T newValue = (T)0.0;
      for(int i=0;i<HISTORY_SIZE;i++){
        newValue += magnitudeHistory[i];
        //message("history value[%d] = %10.10e", i, magnitudeHistory[i]);
      }

      newValue += Pos2(mu * lm[0]);
      newValue /= (T)(HISTORY_SIZE+1);

      T oldValue = Sqrt(Sqr(acceptedMultipliers[1]) +
                        Sqr(acceptedMultipliers[2]));

      newValue = ((T)1.0*Pos2(lm[0] * mu) + (T)1.0*oldValue)/(T)2.0;

      //if(Pos2(lm[0]*mu)!= (T)0.0){
      averageFrictionMagnitude.addSample(Pos2(lm[0] * mu));
        //}
      //newValue = averageFrictionMagnitude.getWeightedAverage();
      newValue =
        //((T)0*averageFrictionMagnitude.getReverseWeightedAverage() +
        (T)1*averageFrictionMagnitude.getWeightedAverage();//)/(T)1.0;
      //(message("eval kinetic friction %d, new = %10.10e, old = %10.10e", this->row_id, newValue, oldValue));
      {
        //T diff = Pos2(mu * lm[0]) - oldValue;
#if 0
        T diff = newValue - oldValue;

        Vector4<T> dir(0, acceptedMultipliers[1], acceptedMultipliers[2], 0);

        if(dir.length() > (T)1e-14){
          dir.normalize();
          dir *= diff;// * (T)0.001;

          dir[1] /= usedScale*bc->S[1][1];
          dir[2] /= usedScale*bc->S[2][2];

          T relativeError = Max(Abs(dir[1]), Abs(dir[2]));

          //message("relative error = %10.10e, diff = %10.10e", relativeError, diff);
          bool updated = false;

          T oldNewValue = newValue;

          if(relativeError > (RERRORKF)){
            //T factor = relativeError / (T)(RERRORKF);
            T errorDiff = relativeError - (T)RERRORKF;
            T dydx = relativeError / diff;

            //message("errorDiff = %10.10e", errorDiff);

            //message("dydx = %10.10e", dydx);

            if(newValue > oldValue){
              T dx = errorDiff / dydx;
              //message("dx1 = %10.10e", dx);
              newValue = newValue - dx;
            }else{
              T dx = errorDiff / dydx;
              //message("dx2 = %10.10e", dx);
              newValue = newValue - dx;
            }

            updated = true;
            stats.n_kinetic_friction_update++;
          }

          diff = newValue - oldValue;
          dir.set(0, acceptedMultipliers[1], acceptedMultipliers[2], 0);
          dir.normalize();
          dir *= diff;

          dir[1] /= usedScale*bc->S[1][1];
          dir[2] /= usedScale*bc->S[2][2];

          T relativeError2 = Max(Abs(dir[1]), Abs(dir[2]));

          //message("relative error2 = %10.10e, diff = %10.10e", relativeError2, diff);

          //message("newValue = %10.10e", newValue);
          if(Abs(relativeError2 - RERRORKF) > 1e-9 && updated){
            error("stop");
          }

          if(newValue > oldNewValue){
            //error("what");
          }

          if(oldNewValue > oldValue){
            if(newValue > oldValue){

            }else{
              //error("what");
            }
          }
        }else{
          /*Accepted multipliers is zero, keep newValue as is*/
          newValue = ((T)1.0*Pos2(lm[0] * mu) + (T)1.0*oldValue)/(T)2.0;
        }
#endif
      }

      if(lm[0] < (T)0.0){
        //newValue /= (T)2.0;
        newValue = (T)0.0;
        //newValue = oldValue/(T)2.0;
      }

      //newValue = Abs(aitkenD2(magnitudeHistory[1], magnitudeHistory[0],
      //                      Pos2(lm[0]*mu)));

      DBG(message("oldValue = %10.10e", oldValue));
      DBG(message("newValue = %10.10e", newValue));
      DBG(message("curValue = %10.10e", Pos2(mu*lm[0])));



      bool kineticFrictionUpdated = false;

      T factor = newValue/oldValue;

      if(factor > 1.0){
        factor = (T)1.0/factor;
      }

      if(Abs(oldValue) < 1e-9){
        factor = (T)0.0;
      }

#if 0
      if(factor < 0.85 && oldValue != 0.0){
        if(newValue > oldValue){
          factor = (T)1.0/(T)0.85;
          newValue = factor * oldValue;
        }else{
          factor = (T)0.85;
          newValue = factor * oldValue;
        }

        factor = (T)0.85;
      }
#endif
      DBG(message("factor = %10.10e", factor));
      /*
        Since the constraints contain normalized vectors times dt = 1e-3,
        we allow an absolute difference in the approximated gamma to be 1e-2.
        That allowed error is then multiplied by dt, resulting in O(1e-5) error,
        which is fine.
       */
      if(Abs(Abs(oldValue) - newValue)</*DIFF_EPS*/1e-3){
        //warning("Error in multipliers = %10.10e",
        //      Abs(Abs(oldValue) - Pos2(lm[0]*mu))
        //      );
        //factor = 1;
      }else{
        //warning("Error in multipliers = %10.10e, factor = %10.10e",
        //      Abs(Abs(oldValue) - Pos2(lm[0]*mu)), factor);
      }

      /*Check for violation with current kinetic vector*/

      bool vectorUpdated = false;

      Vector4<T> hvVec(0, lr[1], lr[2], 0);
      Vector4<T> curVector(0, lr[1], lr[2], 0);
      Vector4<T> oldVector = kineticVector;

      DBG(message("curVector = %10.10e", curVector.length()));
      DBG(std::cout << curVector << std::endl);

      if(curVector.length() > 1e-14){
        curVector.normalize();
      }else{
        curVector.set(0,1,1,0);
        curVector.normalize();
      }

      T weight = (T)1.0;
      T length = hvVec.length();

      /*Compute weight*/
      if(length > Abs(HV_EPS*15.0)){
        /*Sliding distance significant*/
        weight = 1;
      }else if(length < Abs(HV_EPS*1.0)){
        /*Sliding distance not significant*/
        weight = 0;
      }else{
        /*Length = between 10 * HV_EPS and HV_eps compute weight*/
        weight = (length - (T)(Abs(HV_EPS)*1.0))/(T)(14.0*Abs(HV_EPS));
        //weight = (length - (T)(HV_EPS*1.0))/(T)(1.0*HV_EPS);
        //weight = 0;
      }

      DBG(message("weigth = %10.10e", weight));

      cgfassert(weight <= (T)1.0);
      cgfassert(weight >= (T)0.0);

      //Vector4<T> weightedHalfVector = kineticVector + curVector * weight;
      Vector4<T> weightedHalfVector = (T)1.0*kineticVector + (T)1.0*curVector * weight;

      DBG(message("weighted halfVector = %10.10e", weightedHalfVector.length()));
      DBG(std::cout << weightedHalfVector << std::endl);

      if(weightedHalfVector.length() < 1e-14){
        /*If zero, kinetic vector is undefined, and hvVec
          might be very small. However, hvVec is the only
          vector we currently have.*/
        weightedHalfVector = curVector;
      }

      if(weightedHalfVector.length() > 1e-14){
        weightedHalfVector.normalize();
      }

      //averageFrictionDirection.addSample(weightedHalfVector);
      //averageFrictionDirection.addSample(curVector*length);

      DBG(message("multipliers = %10.10e, %10.10e, %10.10e",
                  lm[0], lm[1], lm[2]));

      if( (updateVector || kineticVector.length() < 1e-14) &&
          lm[0] > (T)0.0){
        DBG(message("update vector = %d", updateVector));
        if(kineticVector.length() > 1e-14 &&
           (kineticVector[1] * lr[1] + kineticVector[2] * lr[2]) < Abs(HV_EPS/(T)10.0)){
          /*Kinetic vector can not be updated since it is too small*/
          DBG(message("not updated"));
        }else{
          Vector4<T> lnormal(1,0,0,0);
          oldVector = oldVector;

          DBG(message("old vector"));
          DBG(std::cout << oldVector << std::endl);

          if(frictionStatus[0] == Kinetic){
            /*Kinetic friction*/

            DBG(message("dot(kv, cv) = %10.10e", dot(kineticVector, curVector)));

            if(dot(kineticVector, curVector) > 0 ||
               kineticVector.length() < 1e-14){

              T val = (T)1.0-dot(acceptedKineticVector, weightedHalfVector);
              //T val = (T)1.0-dot(acceptedKineticVector, curVector);
              //T val2 = dot(acceptedKineticVector, weightedHalfVector);
              T val2 = dot(acceptedKineticVector, curVector);

              START_DEBUG;
              message("val = %10.10e", val);
              message("accepted * weighted = %10.10e",
                      dot(acceptedKineticVector, weightedHalfVector));
              message("hv * kinetic = %10.10e",
                      dot(curVector, kineticVector));
              END_DEBUG;

              bool force = false;
              if(kineticVector.length() < 1e-14){
                force = true;
                val = 1;
                val2 = 1;
              }

              DBG(message("hv * kinetic = %10.10e", dot(hvVec, kineticVector)));

              /*If HV is small, it makes no sense to update the
                kinetic vector, since it will point in any direction.*/
              Vector4<T> lastVector = oldVector;//kineticVector;
              if( (
                   Abs(val2) < 0.99984 &&
                   //Abs(val) > 0.000152305 && //1 degree
                   //Abs(val) > 1e-5 &&
                   dot(hvVec, kineticVector) > HV_EPS &&
                   hvVec.length() > Abs(HV_EPS)) || force){

                DBG(message("all ok, update cumulative vector"));

                Vector4<T> diffVector = (kineticVector - curVector)/(T)50.0;

                kineticVector += weightedHalfVector;

                if(kineticVector.length() > 1e-14){
                  kineticVector.normalize();
                }

                cumulativeVector +=
                  kineticVector * (T)0.125;

                cumulativeVector +=
                  lastVector * (T)0.25;

                cumulativeVector /= (T)1.1;

                cumulativeVector = lastVector - diffVector;
                cumulativeVector.normalize();

                averageFrictionDirection.addSample(cumulativeVector);
                cumulativeVector =
                  ((T)1*averageFrictionDirection.getWeightedAverage() +
                   (T)0*averageFrictionDirection.getCumulativeAverage())/(T)1.0;

                vectorUpdated = true;

                START_DEBUG;
                message("lastVector * cumulativeVector = %10.10e",
                        dot(lastVector,
                            cumulativeVector/cumulativeVector.length()));
                message("lastVec.length() = %10.10e", lastVector.length());

                message("hvVec * cumulativeVector = %10.10e",
                        dot(curVector,
                            cumulativeVector/cumulativeVector.length()));
                END_DEBUG;

                //kineticVector = cumulativeVector;

                //kineticVector = weightedHalfVector;

                //cumulativeVector = kineticVector;

                if(dot(lastVector,
                       cumulativeVector/cumulativeVector.length()) > 0.999 &&
                   lastVector.length() > 1e-6 && !force){
                  vectorUpdated = false;
                  kineticVector = lastVector;
                  DBG(message("kinetic vector not updated"));
                }
              }

              if(kineticVector.length() > 1e-14){
                kineticVector.normalize();
              }else{
                kineticVector.set(0,1,1,0);
                kineticVector = weightedHalfVector;
                kineticVector.normalize();
                //vectorUpdated = false;
              }

              START_DEBUG;
              message("weighted half vector");
              std::cout << weightedHalfVector << std::endl;
              message("lastVector");
              std::cout << lastVector << std::endl;
              message("kineticVector");
              std::cout << kineticVector << std::endl;

              message("proj = %10.10e", dot(lastVector, kineticVector));
              END_DEBUG;
            }else{
              START_DEBUG;
              message("cumulative not updated");
              message("hvec,length = %10.10e", hvVec.length());
              END_DEBUG;
            }

            if(vectorUpdated){
              DBG(message("Vector updated"));
              /*1 - acceptedVector * weightedHalfVector > 1e-5  and all vectors
               are well defined

               and

               1 - lastKineticVector * cumulativeVector > 1e-4

               then update kinetic vector
               */
              ////cumulativeVector = averageFrictionDirection.getAverage();
              //// cumulativeVector =
              ////  ((T)1*averageFrictionDirection.getWeightedAverage() +
              ////   (T)1*averageFrictionDirection.getReverseWeightedAverage())/(T)2.0;

              kineticVector = cumulativeVector;

              DBG(message("cumulativeVector"));
              DBG(std::cout << cumulativeVector << std::endl);

              if(kineticVector.length() > 1e-14){
                kineticVector.normalize();
              }

              message("Kinetic vector and cumulative vector");
              std::cout << kineticVector << std::endl;
              std::cout << cumulativeVector << std::endl;
            }else if(kineticVector.length() > 1e-6){
#if 1
              /*Update vector without trigger iff*/
              DBG(message("Vector not updated"));

              kineticVector = oldVector;
#if 1
              Vector4<T> lastVector = kineticVector;

              kineticVector += weightedHalfVector;
              kineticVector.normalize();

              cumulativeVector +=
                kineticVector * (T)0.01;
              cumulativeVector +=
                lastVector * (T)0.99;

              cumulativeVector /= (T)1.1;

              //cumulativeVector = averageFrictionDirection.getAverage();
              ////cumulativeVector =
              ////  ((T)1*averageFrictionDirection.getWeightedAverage() +
              ////   (T)1*averageFrictionDirection.getReverseWeightedAverage())/(T)2.0;

              kineticVector = cumulativeVector;

              if(kineticVector.length() > 1e-14){
                kineticVector.normalize();
              }

              START_DEBUG;
              message("weighted half vector");
              std::cout << weightedHalfVector << std::endl;
              message("lastVector");
              std::cout << lastVector << std::endl;
              message("kineticVector");
              std::cout << kineticVector << std::endl;

              message("proj = %10.10e", dot(lastVector, kineticVector));
              END_DEBUG;
#endif
#endif
            }
          }
        }
      }else if(lm[0] < 0){
        /*kineticVector = curVector;
        kineticVector.normalize();

        cumulativeVector = kineticVector;*/
      }



      START_DEBUG;
      message("Cumulative vector = %10.10e", cumulativeVector.length());
      std::cout << cumulativeVector << std::endl;

      message("setting multipliers");
      END_DEBUG;

      setMultipliers[1] = kineticVector[1] * newValue;
      setMultipliers[2] = kineticVector[2] * newValue;


      T difference = 0;
      T magnitude  = 0;
#if 0
      T relativeError = 0;
      for(int i=0;i<2;i++){
        for(int j=0;j<5;j++){
          if(this->index[j] == Constraint::undefined){
            continue;
          }

          Vector4<T> diag;

          for(int k=0;k<3;k++){
            int idx = this->index[j]*3+k;
            diag[k] = (T)1.0 / (*this->matrix)[idx][idx];
          }

          Vector4<T> resNew = -normals[j][i+1] * setMultipliers[i+1];
          Vector4<T> resOld = -normals[j][i+1] * acceptedMultipliers[i+1];

          T ov = Sqrt(Sqr(acceptedMultipliers[1]) + Sqr(acceptedMultipliers[2]));         T nv = Sqrt(Sqr(setMultipliers[1]) + Sqr(setMultipliers[2]));

          Vector4<T> frictionNew = -normals[j][i+1] * nv;
          Vector4<T> frictionOld = -normals[j][i+1] * ov;

          Vector4<T> diff = resNew - resOld;

          Vector4<T> frictionDiff = frictionNew - frictionOld;
          Vector4<T> frictionDiff2 = frictionDiff;

          for(int k=0;k<3;k++){
            //frictionOld[k]  *= diag[k];
            frictionDiff[k] *= diag[k];
          }


          T nom = dot(frictionDiff, frictionDiff2);

          if(Abs(frictionNorm) < 1e-9){
            relativeError = Max(nom, relativeError);
          }else{
            relativeError = Max(nom/(T)frictionNorm, relativeError);
          }

          DBG(message("nom = %10.10e, denom = %10.10e", nom, frictionNorm));
          DBG(message("relative error of kinetic friction = %10.10e\n\n", relativeError));

          difference += dot(diff, diff);
          magnitude  += dot(resNew, resNew);
        }
      }

      difference = Sqrt(difference);
      magnitude  = Sqrt(magnitude);

      START_DEBUG;
      message("diff    = %10.10e", difference);
      message("magn    = %10.10e", magnitude);
      message("fact    = %10.10e", factor);
      message("lastHV  = %10.10e", lastHV);
      message("HVDiff  = %10.10e", HVDiff);
      END_DEBUG;
#endif

#ifdef _DEBUG
      T tmpHVDiff = Abs(length - lastHV);
#endif

      DBG(message("THVDiff = %10.10e", tmpHVDiff));

      DBG(message("HVDiff = %10.10e", HVDiff));


      /*DIFF_EPS = 1e-4*/
      if(difference < DIFF_EPS){
        magnitude = 1;
      }

      T diff =
        Sqrt(Sqr(setMultipliers[1]) +
             Sqr(setMultipliers[2]))
        -
        Sqrt(Sqr(acceptedMultipliers[1]) +
             Sqr(acceptedMultipliers[2])
             );

      DBG(message("diff = %10.10e", diff));

      Vector4<T> dir(0, setMultipliers[1], setMultipliers[2], 0);

      DBG(message("dir"));
      DBG(std::cout << dir);

      if(dir.length() > (T)1e-14){
        dir.normalize();
        dir *= diff;// * (T)0.001;
      }

      DBG(message("dir"));
      DBG(std::cout << dir);

      dir[1] /= usedScale*bc->S[1][1];
      dir[2] /= usedScale*bc->S[2][2];

      DBG(std::cout << dir);

      T relativeError = Max(Abs(dir[1]), Abs(dir[2]));

      DBG(message("relative error = %10.10e", relativeError));

      if(vectorUpdated){
        stats.n_kinetic_friction_vector_update++;
      }


      //relativeError = (T)1.0;
      //(message("factor = %10.10e, %10.10e, %10.10e, %10.10e",
      //       factor, oldValue, newValue, relativeError));
      if(//factor < GAMMA_FACTOR && difference > DIFF_EPS){
         (vectorUpdated || true) && //factor < GAMMA_FACTOR &&
         //factor < 0.99 &&
         /*(tmpHVDiff > DISTANCE_EPSILON || HVDiff > DISTANCE_EPSILON)
           &&*/ relativeError*(T)1.0 > RERROR/(T)1.0

         //&& HVDiff > DISTANCE_EPSILON

         //&& difference > DIFF_EPS
         ){

         /*(factor < GAMMA_FACTOR &&
          difference > DIFF_EPS &&
          difference/magnitude > DIFF_EPS) &&*/
         /////tmpHVDiff > DISTANCE_EPSILON ||
         //// HVDiff > DISTANCE_EPSILON){
        //if(difference/magnitude > DIFF_EPS){
      //T diff2 = Abs(Abs(oldValue) - Abs(newValue));

      //if(newValue/diff2 < 1000.0){
        //if(factor < GAMMA_FACTOR &&
        // difference/magnitude > DIFF_EPS){
        //if(diff2 / newValue > 1e-3){


        if(frictionStatus[0] == Kinetic){

          START_DEBUG;
          DBG(message("\t[%d] friction update kinetic 1, value = %10.10e, %10.10e, %10.10e, %10.10e, factor = %10.10e",
                  this->row_id,
                  relativeError,
                  newValue,
                  oldValue,
                   dot(oldVector, kineticVector), factor
                   ));
          DBG(std::cout << oldVector << std::endl);
          DBG(std::cout << kineticVector << std::endl);
          DBG(std::cout << hvVec << std::endl);
          DBG(std::cout << curVector << std::endl);
          DBG(message("dot hvvec kinetic vector = %10.10e", dot(hvVec, kineticVector)));
          Vector4<T> acc(0, acceptedMultipliers[1], acceptedMultipliers[2], 0);
          DBG(message("dot hvvec multipliers = %10.10e", dot(hvVec, acc)));

          DBG(printIndices(std::cout));
          END_DEBUG;

#ifdef _DEBUG
          ContactBlockConstraint<N, T>* ccc =
            (ContactBlockConstraint<N, T>*)blockConstraint;
#endif
          DBG(std::cout << ccc->S << std::endl);

          DBG(std::cout << kineticVector << std::endl);

          START_DEBUG;
          message("Updating %d", this->row_id);
          message("difference = %10.10e", difference);
          message("factor     = %10.10e", factor);
          message("diff/magn  = %10.10e", difference/magnitude);
          message("gamma      = %10.10e", lm[0]*mu);
          message("gamma'     = %10.10e", oldValue);
          std::cout << hvVec << std::endl;
          std::cout << lr << std::endl;
          std::cout << oldVector << std::endl;
          std::cout << kineticVector << std::endl;
          std::cout << curVector << std::endl;
          std::cout << weightedHalfVector << std::endl;
          std::cout << cumulativeVector / cumulativeVector.length() << std::endl;
          std::cout << acceptedKineticVector << std::endl;
          message("hv * kin = %10.10e", dot(hvVec, kineticVector));
          END_DEBUG;

          /*Update RHS*/
          *changed = true;
          kineticFrictionUpdated = true;
          stats.n_kinetic_friction_update++;

          *frictionChanged = true;
        }
      }
#if 1
      else if(vectorUpdated && difference > DIFF_EPS
              && factor < GAMMA_FACTOR
              /*&&
              difference > 1e-5 &&
              HVDiff > DISTANCE_EPSILON*/
              /*&& factor < GAMMA_FACTOR*//*DIFF_EPS*/ && relativeError*(T)1.0 > RERROR/(T)1.000){

        (message("\t[%d] friction update kinetic 2, value = %10.10e, %10.10e, %10.10e, %10.10e, factor = %10.10e",
                this->row_id,
                relativeError,
                newValue,
                oldValue,
                 dot(oldVector, kineticVector), factor
                 ));
        (printIndices(std::cout));

        DBG(std::cout << kineticVector << std::endl);

#if 1
        ContactBlockConstraint<N, T>* ccc =
          (ContactBlockConstraint<N, T>*)blockConstraint;
#endif

        (std::cout << ccc->S << std::endl);

        START_DEBUG;
        message("Updating2 %d", this->row_id);
        message("difference = %10.10e", difference);
        message("factor     = %10.10e", factor);
        message("diff/magn  = %10.10e", difference/magnitude);
        message("gamma      = %10.10e", lm[0]*mu);
        message("gamma'     = %10.10e", oldValue);
        std::cout << hvVec << std::endl;
        std::cout << lr << std::endl;
        std::cout << oldVector << std::endl;
        std::cout << kineticVector << std::endl;
        std::cout << curVector << std::endl;
        std::cout << weightedHalfVector << std::endl;
        std::cout << cumulativeVector / cumulativeVector.length() << std::endl;
        std::cout << acceptedKineticVector << std::endl;

        message("hv * kin = %10.10e", dot(hvVec, kineticVector));
        END_DEBUG;

        /*Update RHS*/
        *changed = true;
        kineticFrictionUpdated = true;
        *frictionChanged = true;
        stats.n_kinetic_friction_vector_update++;
      }
#endif

      if(kineticFrictionUpdated){
        acceptedMultipliers[1] = setMultipliers[1];
        acceptedMultipliers[2] = setMultipliers[2];

        acceptedKineticVector = kineticVector;
      }else{
        //setMultipliers[1] = acceptedMultipliers[1];
        //setMultipliers[2] = acceptedMultipliers[2];
        //setMultipliers[1] = old1;
        //setMultipliers[2] = old2;
      }

      if(*frictionChanged == false){
        /*Not active or no update requested, update shadow values,
          in case some other update is triggered*/

        /*This works well when a diagonal preconditioner is used*/
        //setMultipliers[1] = old1;
        //setMultipliers[2] = old2;
#if 1

#if 1
        T oldValue = Sqrt(Sqr(acceptedMultipliers[1]) +
                          Sqr(acceptedMultipliers[2]));

        T newValue = ((T)1.0*Pos2(lm[0] * mu) + (T)5.0*oldValue)/(T)6.0;
        //T newValue = ((T)1.0*Pos2(lm[0] * mu) + (T)1.0*oldValue)/(T)2.0;
        //newValue = oldValue;

        //newValue = Abs(aitkenD2(magnitudeHistory[1], magnitudeHistory[0],
        //                      Pos2(lm[0]*mu)));

        //averageFrictionMagnitude.addSample(newValue);
        if(Pos(lm[0]*mu) != (T)0.0){
          //averageFrictionMagnitude.addSample(Pos2(lm[0]*mu));
        }
        //newValue = averageFrictionMagnitude.getWeightedAverage();
        //newValue = averageFrictionMagnitude.getCumulativeAverage();

        //newValue =
        //  (averageFrictionMagnitude.getWeightedAverage() +
        //   averageFrictionMagnitude.getAverage())/(T)2.0;

#if 0
        {
          //T diff = Pos2(mu * lm[0]) - oldValue;
          T diff = newValue - oldValue;

          Vector4<T> dir(0, acceptedMultipliers[1], acceptedMultipliers[2], 0);

          if(dir.length() > (T)1e-14){
            dir.normalize();
            dir *= diff;// * (T)0.001;
            dir[1] /= usedScale*bc->S[1][1];
            dir[2] /= usedScale*bc->S[2][2];

            T relativeError = Max(Abs(dir[1]), Abs(dir[2]));

            if(relativeError > (RERRORKF)){
              //T factor = relativeError / (T)(RERRORKF);
              T errorDiff = relativeError - (T)RERRORKF;
              T dydx = relativeError / diff;

              if(newValue > oldValue){
                T dx = errorDiff / dydx;
                newValue = newValue - dx;
              }else{
                T dx = errorDiff / dydx;
                newValue = newValue - dx;
              }
            }
          }else{
            newValue = ((T)1.0*Pos2(lm[0] * mu) + (T)9.0*oldValue)/(T)10.0;
          }
        }
#endif
        if(lm[0] < 0.0){
          //newValue /= (T)2.0;
          newValue = (T)0.0;
        }//else{
          setMultipliers[1] = kineticVector[1] * newValue;
          setMultipliers[2] = kineticVector[2] * newValue;
          DBG(message("Shadow update"));
          //}
#else
        T newValue = (T)0.0;
        for(int i=0;i<HISTORY_SIZE;i++){
          newValue += magnitudeHistory[i];
          //message("history value[%d] = %10.10e", i, magnitudeHistory[i]);
        }

        newValue += Pos2(mu * lm[0]);
        newValue /= (T)(HISTORY_SIZE+1);

        setMultipliers[1] = kineticVector[1] * newValue;
        setMultipliers[2] = kineticVector[2] * newValue;
#endif
#endif
      }
    }
  }
#endif



  template class ContactConstraint<1, float>;
  template class ContactConstraint<2, float>;
  template class ContactConstraint<4, float>;
  template class ContactConstraint<8, float>;

  template class ContactConstraint<1, double>;
  template class ContactConstraint<2, double>;
  template class ContactConstraint<4, double>;
  template class ContactConstraint<8, double>;
}
