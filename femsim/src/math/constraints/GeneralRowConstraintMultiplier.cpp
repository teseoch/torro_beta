/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/constraints/GeneralRowConstraintMultiplier.hpp"
#include "math/SpMatrixC.hpp"

namespace CGF{
  template<int N, class T>
  void GeneralRowConstraintMultiplier<N, T>::
  multiply(const AbstractRowConstraint<N, T>* a,
           VectorC2<T>* D,
           const AbstractRowConstraint<N, T>* b){
    clear();

    this->row = a->getMatrix()->constraints.reverseIndex(a->row_id);
    this->col = b->getMatrix()->constraints.reverseIndex(b->row_id);
    this->offset = a->getOffset();

    for(int i=0;i<a->getIndividualSize();i++){
      for(int j=0;j<a->getOffset();j++){
        ConstraintElement<T> elementA = a->getValue(j, i);

        //message("a %d, %d = %d, %10.10e", i, j, elementA.col, elementA.value);
        if(elementA.col != -1){
          for(int k=0;k<b->getIndividualSize();k++){
            for(int l=0;l<a->getOffset();l++){
              ConstraintElement<T> elementB = b->getValue(l, k);

              if(elementB.col != -1){
              //message("b %d, %d = %d, %10.10e", k, l, elementB.col, elementB.value);
                if(elementA.col == elementB.col){
                  T val = (T)1.0/(*D)[elementB.col];

                  localMatrix[j][l] += elementA.value * val * elementB.value;
                }
              }
            }
          }
        }
      }
    }
  }

  template<int N, class T>
  void GeneralRowConstraintMultiplier<N, T>::
  store(SpMatrix<N, T>* mat){

    for(int i=0;i<this->offset;i++){
      for(int j=0;j<this->offset;j++){
        (*mat)[this->row*this->offset + i][this->col*this->offset + j] =
          localMatrix[i][j];
      }
    }

    if(this->row == this->col){
      /*Diagonal entry*/
      return;
    }

    //localMatrix.transpose();

    for(int i=0;i<this->offset;i++){
      for(int j=0;j<this->offset;j++){
        (*mat)[this->col*this->offset + i][this->row*this->offset + j] =
          localMatrix[j][i];
      }
    }
  }

  template<int N, class T>
  void GeneralRowConstraintMultiplier<N, T>::
  clear(){
    localMatrix.clear();
  }

  template class GeneralRowConstraintMultiplier<1, float>;
  template class GeneralRowConstraintMultiplier<2, float>;
  template class GeneralRowConstraintMultiplier<4, float>;
  template class GeneralRowConstraintMultiplier<8, float>;
  template class GeneralRowConstraintMultiplier<1, double>;
  template class GeneralRowConstraintMultiplier<2, double>;
  template class GeneralRowConstraintMultiplier<4, double>;
  template class GeneralRowConstraintMultiplier<8, double>;

}
