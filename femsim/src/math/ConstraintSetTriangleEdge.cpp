/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintSetTriangleEdge.hpp"
#include "math/ConstraintCandidateTriangleEdge.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

#define DISTANCE_EPS SAFETY_DISTANCE

namespace CGF{
#define EPS DISTANCE_EPSILON

  template<class T>
  ConstraintSetTriangleEdge<T>::ConstraintSetTriangleEdge(){
  }

  template<class T>
  ConstraintSetTriangleEdge<T>::~ConstraintSetTriangleEdge(){
    CandidateTreeIterator it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = *it++;
      delete c;
    }
    candidates.clear();
  }

  template<class T>
  void ConstraintSetTriangleEdge<T>::deactivateAll(CollisionContext<T>* ctx){
    CandidateTreeIterator it = candidates.begin();

    while(it != candidates.end()){
      Candidate* c = *it++;
      if(c->status == Enabled){
        c->disableConstraint(this, ctx, true);
      }
    }
  }

  template<class T>
  bool ConstraintSetTriangleEdge<T>::evaluate(Edge<T>& p, Vector4<T>& com,
                                              Quaternion<T>& rot,
                                              CollisionContext<T>* ctx,
                                              const EvalType& etype,
                                              EvalStats& stats){
    CandidateTreeIterator it = candidates.begin();

    Tree<Candidate*> changedCandidates;
    Tree<Candidate*> changedIndices;

    bool evaluateGeometry    = etype.geometryCheck();
    bool evaluateConstraints = etype.existingCheck();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    if(evaluateGeometry){
      /*Perform a simple plane vertex test for all candidates*/

      while(it != candidates.end()){
        Candidate* c = *it++;

        if(false){
          if(c->status == Enabled){
            /*An invalid candidate with an enabled constraint is not
              possible*/
            error("Invalid candidate is still enabled");
          }

          c->v0 = ctx->mesh->vertices[c->vertexId].coord;
          //ctx->mesh->getVertex(&c->v0, c->vertexId, false);
          //ctx->mesh->getTriangle(&c->t0, c->faceId, backFace);

        }

        if(c->status == Enabled){
          /*Check if the constraint is in a deactivated state, if so,
            remove constraint*/

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(c->enabledId);

          if(ccc->status == Inactive){
#ifdef ACTIVE_CONSTRAINTS
            //c->disableConstraint(this, ctx);
            //#error determine if a constraint can be removed
#else

#endif
          }
        }

        /*Update geometry information of associated faces*/
        c->updateGeometry(ctx, this, p, startPosition);

        /*Evaluate distance of current configuration, if distance
          becomes negative there is a collision*/

        /*Evaluate signed distance of pair*/
        if(c->evaluate(p, com, rot)){
          /*Changed in sign, inspect further*/
          changedCandidates.insert(c, 1);
        }
      }

      /*Perform root-finding for finding collision-----------*/
      CandidateTreeIterator it2 = changedCandidates.begin();

      /*Find closest colliding constraint*/

      //ConstraintCandidateFace<T>* closest = 0;
      //T closestDistance = (T)10000.0;

#if 1
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

#ifdef ACTIVE_CONSTRAINTS
        if(c->status == Enabled){
          //continue;
        }
#else
        if(c->status == Enabled){
          continue;
        }
#endif
        if(c->checkCollision(p, com, rot)){
          DBG(message("collision"));
          if(c->status != Enabled){
            DBG(message("enabling constraint"));
            if(c->enableConstraint(this, ctx, startPosition, p)){
              constraintsChanged = true;
              changedIndices.insert(c, 1);
            }
          }
        }
      }
#else
      /*Figure out the ´closest´ intersection point. This basically
        turns out to be the closest root.*/
      Candidate* closestCandidate = 0;
      T closestRoot = (T)100000.0;
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

#ifdef ACTIVE_CONSTRAINTS

#else
        if(c->status == Enabled){
          continue;
        }
#endif
        T root = (T)0.0;
        if(c->checkCollision(p, com, rot, &root)){
          if(c->status != Enabled){
            if(root < closestRoot){
              root = closestRoot;
              closestCandidate = c;
            }
          }
        }
      }
      if(closestCandidate){
        if(closestCandidate->enableConstraint(this, ctx, startPosition, p)){
          constraintsChanged = true;
          changedIndices.insert(closestCandidate, 1);
        }
      }
      /*Select and enable the candidate with the closest intersectionpoint*/
#endif
      /*-----------------------------------------------------*/
    }

    /*Evaluate all enabled constraints---------------------*/

    /*Evaluate all enabled constraints (except for new constraints)
      and perform some sub-constraint operations (friction)*/

    it = candidates.begin();

    while(it != candidates.end() && evaluateConstraints){
      Candidate* cc = *it++;

      if(changedIndices.findIndex(cc) == -1){
        if(cc->status == Enabled){

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(cc->enabledId);

          bool active = false;

          bool changed = ccc->evaluate(*ctx->getSolver()->getx(),
                                       *ctx->getSolver()->getb(),
                                       0, etype, stats, &active,
                                       ctx->getSolver()->getb2());

          if(changed && active && etype.penetrationCheck()){
            /*If a constraint has been changed, but still active,
              update preconditioner*/

            ccc->computePreconditioner(*ccc->preconditioner);
            constraintsChanged = true;
          }

          /*Disable (remove) constraint if allowed*/
#ifdef ACTIVE_CONSTRAINTS
          /*Keep constraint*/
#else
          if(active == false && etype.penetrationCheck()){
            constraintsChanged = true;
            cc->disableConstraint(this, ctx, true);
          }
#endif

          /*Notifies solver*/
          if(changed){
            constraintsChanged = true;
          }
        }
      }else{
        //Evaluate new constraint in order to update RHS
        if(cc->status == Enabled){

          AbstractRowConstraint<2, T>* ccc =
            (AbstractRowConstraint<2, T>*)
            ctx->getMatrix()->getConstraint(cc->enabledId);

          bool active = false;
          EvalType etype2;

          ccc->evaluate(*ctx->getSolver()->getx(),
                        *ctx->getSolver()->getb(),
                        0, etype2, stats, &active,
                        ctx->getSolver()->getb2());
        }
      }
    }

    if(evaluateGeometry){
      lastPosition     = p; /*Store last position*/
      lastCenterOfMass = com;
      lastOrientation  = rot;
    }
    return constraintsChanged;
  }

  template<class T>
  bool ConstraintSetTriangleEdge<T>::checkAndUpdate(Edge<T>& p,
                                                    Vector4<T>& com,
                                                    Quaternion<T>& rot,
                                                    CollisionContext<T>* ctx,
                                                    EvalStats& stats,
                                                    bool friction,
                                                    bool force, T bnorm){
    CandidateTreeIterator it = candidates.begin();

    //T b2Norm = ctx->getSolver()->getB2Norm()/(T)2.0;

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    /*Simple plane point check for a fast detection in change*/
    while(it != candidates.end()){
      Candidate* c = *it++;

      if(c->status != Enabled){
        continue;
      }

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(c->enabledId);

#ifndef ACTIVE_CONSTRAINTS
      if(cc->status != Active){
        //continue;
      }
#endif

      /*Check corresponding lambda*/
      if(!cc->valid(*ctx->getSolver()->getx())){
        DBG(message("CONSTRAINT NOT VALID"));
        //continue;
      }

      DBG(message("update geometry"));
      /*Update geometry information of associated faces*/
      c->updateGeometry(ctx, this, p, startPosition);

      if(false){
        if(c->status == Enabled){
          error("Invalid candidate is enabled");
        }
      }

      /*Update value for current distance*/
      c->evaluate(p, com, rot);

      /*Check geometric distance and state of the constraint*/

      if(true){
#if 0
      if(Abs(c->distancec - DISTANCE_TOL) > (T)0.0*DISTANCE_TOL /*&&
                                                                   c->tu.barycentricInTriangle(c->bary2, BARY_EPS)*/){
#endif
        warning("updating EV constraint, distance = %10.10e, %10.10e, %10.10e",
                c->distancec, c->distancec - EPS,
                c->distancec - DISTANCE_TOL);
#if 1
#ifdef ACTIVE_CONSTRAINTS
#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > SAFETY_DISTANCE){
            /*Constraint is not active, distance positive*/
            if(cc->getStatus() != Active){
              c->disableConstraint(this, ctx, true);
            }
            continue;
          }
        }else{
          //if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
          if(c->distancec < (T)(EPS + DISTANCE_TOL) &&
             c->distancec > (T)DISTANCE_TOL){

            /*Constraint is active, distance positive*/
            if(cc->getStatus() != Active){
              c->disableConstraint(this, ctx, true);
            }
            continue;
          }
        }
#else
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > EPS*2.0 ){
            /*Constraint is not active, distance positive*/
            /*Experimantal, just get rid of it*/
            message("not valid, distance = %10.10e, disableConstraint",
                    c->distancec);
            c->disableConstraint(this, ctx, true);
            continue;
          }
        }
#endif
#else
        if(cc->status != Active){
        //if(false){
          if(c->distancec > EPS*2.0 ){
            /*Constraint is not active, distance positive*/
            /*Experimantal, just get rid of it*/
            c->disableConstraint(this, ctx, true);
            continue;
          }
          if(c->distancec > EPS*0.1 ){
            /*Constraint is not active, distance positive*/
            //continue;
          }
        }else{
          //warning("Constraint active 1, dist = %10.10e",
          //      c->distancec - EPS + DISTANCE_TOL);
          //warning("EPS = %10.10e, DISTANCE_TOL = %10.10e",
          //      EPS, DISTANCE_TOL);

          if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
            //continue;
          }

          if(c->distancec - EPS - 1.0*DISTANCE_TOL > (T)0.0){
            //continue;
          }

          if(Abs(c->distancec - EPS) < DISTANCE_TOL){
            /*Constraint is active, distance positive*/
            //continue;
          }

#if 0
          {
            Vector4<T> weights =
              c->tu.getBarycentricCoordinates(p.getVertex());
            if(!c->tu.barycentricInTriangle(weights, (T)BARY_EPS)){
              c->disableConstraint(this, ctx, true);
              constraintsChanged = true;
              continue;
            }
          }
#endif
          DBG(message("distance in testA = %10.10e",
                      c->distancec - EPS + DISTANCE_TOL));

          DBG(message("distance in testB = %10.10e",
                      c->distancec - EPS - DISTANCE_TOL));
        }

#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->distancec > SAFETY_DISTANCE){
            /*Constraint is not active or multiplier is very small,
              distance is positive*/
            continue;
          }
        }else{
          //warning("Constraint active 2, dist = %10.10e",
          //      c->distancec - EPS + DISTANCE_TOL);
          /*Valid*/
          //if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
          //continue;
          //}

          if(Abs(c->distancec - EPS) < DISTANCE_TOL){
            continue;
          }
        }
#endif
#endif
#endif
        /*Geometric distance and constraint distance are not in sync, update*/
        (message("update EV constraint %d - %d", edgeId, c->vertexId));
        bool skipped = false;

        //c->updateConstraint(this, ctx, startPosition, p, com, rot, &skipped);

#ifdef ACTIVE_CONSTRAINTS
        if(!cc->valid(*ctx->getSolver()->getx())){
          c->disableConstraint(this, ctx, true);
          message("not valid, disable");
          continue;
        }else{
          if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
            message("skip, distance = %10.10e", c->distancec);
            continue;
          }

          if(c->distancec - EPS - DISTANCE_TOL > (T)0.0){
            message("skip, distance = %10.10e", c->distancec);
            continue;
          }
        }
#else
        if(cc->status != Active){

          c->disableConstraint(this, ctx, true);
          continue;
          if(c->distancec > EPS*1.0 ){
            continue;
          }
        }

        if(cc->status == Active){
          if(c->distancec - EPS + DISTANCE_TOL > (T)0.0){
            continue;
          }

          if(c->distancec - EPS - DISTANCE_TOL > (T)0.0){
            continue;
          }
        }
#endif

        c->updateConstraint(this, ctx, startPosition, p, com, rot, &skipped,
                            stats);

        if(skipped){
          //constraintsChanged = true;
          message("skipped");
          continue;
        }

        //cc->status = Active;

        if(cc->status != Active){
          cc->status = Active;
          cc->frictionStatus[0] = Static;
          cc->frictionStatus[1] = Static;
        }

        message("Updated and enabled EV constraint");

        constraintsChanged = true;

        (message("EV CONSTRAINT NOT CORRECT\n\n\n"));
        (message("real distance = %10.10e", c->distancec + DISTANCE_EPS));
        (warning("EV CONSTRAINT NOT CORRECT %d, %d, backFace = %d\n\n\n", edgeId, c->vertexId));
        (warning("real distance = %10.10e", c->distancec + DISTANCE_EPS));
      }
    }
    return constraintsChanged;
  }

  template<class T>
  bool ConstraintSetTriangleEdge<T>::updateInitial(Edge<T>& p,
                                                   Vector4<T>& com,
                                                   Quaternion<T>& rot,
                                                   CollisionContext<T>* ctx,
                                                   EvalStats& stats,
                                                   bool friction,
                                                   bool force, T bnorm){
    CandidateTreeIterator it = candidates.begin();

    bool constraintsChanged = false;

    while(it != candidates.end()){
      Candidate* c = *it++;

      /*Update geometry information of associated faces*/
      c->updateGeometry(ctx, this, p, startPosition);

      startPosition     = p;
      startCenterOfMass = com;
      startOrientation  = rot;

      bool skipped = false;
      c->updateInitial(this, ctx, startPosition, p, com, rot, &skipped, stats);

      if(skipped){
        constraintsChanged = true;
      }
    }
    return constraintsChanged;
  }

  /*Updates the geometry information*/
  template<class T>
  void ConstraintSetTriangleEdge<T>::update(CollisionContext<T>* ctx){
    Edge<T> x;
    if(Mesh::isRigid(type)){
      return;
    }
    ctx->mesh->getEdge(&x, edgeId, false);

    Vector4<T> com(0,0,0,0);
    Quaternion<T> rot(0,0,0,1);

    if(Mesh::isRigid(type)){
      ctx->mesh->setCurrentEdge(edgeId);
      int objectId = ctx->mesh->vertexObjectMap[ctx->mesh->getOriginVertex()];
      com = ctx->mesh->centerOfMass[objectId];
      rot = ctx->mesh->orientations[objectId];
      return;
    }

    /*Find potential faces for vertex (vertexId)*/
    Tree<int> currentPotentials;
    ctx->extractPotentialEdgeVertices(edgeId, currentPotentials);

    /*Add new candidates which are note yet in the candidate list*/
    Tree<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;

      int index = candidatesTree.findIndex(candidate);

      if(index == -1){
        /*Current candidate not found in set, add*/

        if(Mesh::isRigidOrStatic(ctx->mesh->vertices[candidate].type)){
          continue;
        }

        Candidate* cc = new Candidate(edgeId, candidate);

        cc->vertexType = ctx->mesh->vertices[candidate].type;
        cc->edgeType   = type;

        candidates.insert(cc, 1);
        candidatesTree.insert(candidate, 1);
      }
    }

    /*-----------------------------------------------------*/

    /*Check for disappeared candidates, remove them from the set.*/
    CandidateTreeIterator tcit = candidates.begin();

    while(tcit != candidates.end()){
      Candidate* cc = *tcit;

      /*If not found in new potential set, remove.*/
      if(currentPotentials.findIndex(cc->vertexId) == -1){
        /*Disable and remove*/
        /*Check if constraint is enabled*/

        if(cc->status == Enabled){
          //error("Deleting enabled constraint!!");
          tcit++;
        }else{
          cc->disableConstraint(this, ctx, true);
          candidates.remove(tcit);
          candidatesTree.remove(cc->edgeId);
          delete cc;
        }
      }else{
        tcit++;
      }
    }

    /*Recompute states at beginning of timestep*/
    CandidateTreeIterator it3 = candidates.begin();

    while(it3 != candidates.end()){
      Candidate* s = *it3++;
      s->recompute(this, ctx, x, com, rot);
    }

    /*-----------------------------------------------------*/

    startPosition     = x;
    startCenterOfMass = com;
    startOrientation  = rot;
    lastPosition      = x;
    lastCenterOfMass  = com;
    lastOrientation   = rot;
  }

  template<class T>
  void ConstraintSetTriangleEdge<T>::showVertexCandidateState(int vertex,
                                                              CollisionContext<T>* ctx){

  }

  template class ConstraintSetTriangleEdge<float>;
  template class ConstraintSetTriangleEdge<double>;
}
