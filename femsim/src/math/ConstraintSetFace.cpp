/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintSetFace.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

namespace CGF{

  template<class T>
  ConstraintSetFaces<T>::ConstraintSetFaces(){

  }

  template<class T>
  ConstraintSetFaces<T>::~ConstraintSetFaces(){

  }

  template<class T>
  void ConstraintSetFaces<T>::update(CollisionContext<T>* n){
    candidates.clear();

    /*Performs a raw-collision check for the face associated with this
      set and the rest of the scene stored in stree*/
    /*Two faces belonging to the same rigid object are ignored, also
      two faces from static objects are neglected.*/
    n->stree->findPotentialFaceCollisions(faceId, candidates);
  }

  template class ConstraintSetFaces<float>;
  template class ConstraintSetFaces<double>;
}
