/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintCandidateEdge.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"
#include "math/ConstraintSetEdge.hpp"
#include "math/ConstraintSet.hpp"

//#define MU 0.5
#define EPS DISTANCE_EPSILON
#define DISTANCE_EPS SAFETY_DISTANCE
#define TOTAL_EPS (EPS + DISTANCE_EPS)

#define DEBUG_UPDATE
#undef DEBUG_UPDATE

namespace CGF{

  /*Only update distances and reference vectors only if intersection
    test did not thrown an exception. In that case the edges are
    parallel so we can't determine a crossing properly.

    The valid check will be dropped, since this results in missed collisions
  */

  template<class T>
  ConstraintCandidateEdge<T>::ConstraintCandidateEdge(int eid,
                                                      int oeid,
                                                      bool b){
    edgeId = eid;
    otherEdgeId = oeid;

    status = Disabled;

    distance0 = distancec = 0;

    intersection0 = intersectionc = false;

    tangentialDefined = false;
    anyReferenceSet = false;

    referencedvEP.set(1,1,1,0);
    referencedvEP.normalize();
    referencedvPE = -referencedvEP;

    backFace = b;

    collapsed = false;

    forced = false;
  }

  template<class T>
  void ConstraintCandidateEdge<T>::forceConstraint(){
    forced = true;
    warning("forcing constraint e %d - e %d - backface = %d",
            edgeId, otherEdgeId, backFace);
    //forced = false;
  }

  /*p = current, x = start*/
  template<class T>
  bool ConstraintCandidateEdge<T>::checkCollision(const Edge<T>& p,
                                                  const Vector4<T>& com,
                                                  const Quaternion<T>& rot,
                                                  T* dist){
    int colCode = -1;

    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

    bool debug = false;
    T root = (T)0.0;
#if 1
    if(
       //(edgeId == 64451 && otherEdgeId == 14866) ||
       //(edgeId == 14866 && otherEdgeId == 64451) ||
       //(edgeId == 64451 && otherEdgeId == 35300) ||
       //(edgeId == 35300 && otherEdgeId == 64451)
       //(edgeId == 21398 && otherEdgeId == 24275) ||
       //(edgeId == 24275 && otherEdgeId == 21398)

       //edgeId == 976 || otherEdgeId == 976

       //       (edgeId == 17749 && otherEdgeId == 65333) ||
       //(edgeId == 65333 && otherEdgeId == 17749)

       //(edgeId == 10206 && otherEdgeId == 23895) ||
       //(edgeId == 23895 && otherEdgeId == 10206) ||

       //(edgeId == 62092 && otherEdgeId == 23895) ||
       //(edgeId == 23895 && otherEdgeId == 62092)

       //(edgeId == 16121 && otherEdgeId == 21556) ||
       //(edgeId == 21556 && otherEdgeId == 16121) ||


       //(edgeId == 46728 && otherEdgeId == 61909) ||
       //(edgeId == 61909 && otherEdgeId == 46728)

       //(edgeId == 173381 && otherEdgeId == 33419) ||
       //(edgeId == 33419 && otherEdgeId == 173381) ||

       //(edgeId == 173381 && otherEdgeId == 33485) ||
       //(edgeId == 33485 && otherEdgeId == 173381)
       //false

       //(edgeId == 447 && otherEdgeId == 52408) ||
       //(edgeId == 52408 && otherEdgeId == 447)

       //(edgeId == 142031 && otherEdgeId == 9812) ||
       //(edgeId == 9812   && otherEdgeId == 142031)
       //||
       //(edgeId == 142028 && otherEdgeId == 9812) ||
       //(edgeId == 9812   && otherEdgeId == 142028)
       //(edgeId == 157541 && otherEdgeId == 107330) ||
       //(edgeId == 107330 && otherEdgeId == 157541)
       (edgeId == 144166 && otherEdgeId == 56468) ||
       (edgeId == 56468  && otherEdgeId == 144166) ||
       (edgeId == 144164 && otherEdgeId == 56468) ||
       (edgeId == 56468  && otherEdgeId == 144164)
       ){

      debug = true;

      warning("check collision %d, %d, backFace = %d",
              edgeId, otherEdgeId, backFace);
    }
#endif

    try{
      if(!rigidEdge1 && !rigidEdge2){
        if(debug){
          warning("deformable intersection");
        }
        colCode = intersection(p, p0, eu, e0, colPoint, (T)SAFETY_DISTANCE,
                               &baryi1, &baryi2, &pi, &ei,
                               (T)DISTANCE_EPS, 1,
                               &referencedvPE, &root);
        //*dist = p0.getSignedDistance(e0, 0, 0, 0, 0, &referencedvPE);
        if(dist) *dist = root;
      }else{
        if(debug){
          warning("rigid intersection, com, comp0, comeu, come0, rots");
          std::cerr << com << std::endl;
          std::cerr << comp0 << std::endl;
          std::cerr << comeu << std::endl;
          std::cerr << come0 << std::endl;
          std::cerr << rot << std::endl;
          std::cerr << rotp0 << std::endl;
          std::cerr << roteu << std::endl;
          std::cerr << rote0 << std::endl;
        }

        colCode = rigidIntersection(p,  com,   rot*rotp0.inverse(),
                                    p0, comp0, rotp0*rotp0.inverse(),
                                    eu, comeu, roteu*rote0.inverse(),
                                    e0, come0, rote0*rote0.inverse(),
                                    colPoint, (T)SAFETY_DISTANCE,
                                    &baryi1, &baryi2,
                                    &pi, &ei, (T)DISTANCE_EPS, 1,
                                    rigidEdge1, rigidEdge2, &root,
                                    &referencedvPE);

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
          compi = comp0 * ((T)1.0 - root) + com   * root;
          comei = come0 * ((T)1.0 - root) + comeu * root;

          rotpi = slerp(rotp0*rotp0.inverse(), rot*rotp0.inverse(),   root);
          rotei = slerp(rote0*rote0.inverse(), roteu*rote0.inverse(), root);
        }
        if(dist) *dist = root;//p0.getSignedDistance(e0, 0, 0, 0, 0, &referencedvPE);
      }
    }catch(Exception* e){
      delete e;
      return false;
    }

    if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
      if(ei.getTangentDistance(0) < 5e-4 ||
         ei.getTangentDistance(1) < 5e-4 ||
         pi.getTangentDistance(0) < 5e-4 ||
         pi.getTangentDistance(1) < 5e-4){
        warning("tangent distances too small, wait for other constraints");
        return false;
      }
    }

    /*If the method has converged, then the distance between ei and pi
      is positive, hence, ei.getOutwardNormal will give a properly
      defined normal*/

#if 1
    referencedvEP = ei.getOutwardNormal(pi, &referencedvEP);
    referencedvPE = -referencedvEP;

    /*Check angle of normal vector*/
    if(dot(referencedvEP, ei.faceTangent(0)) > 0.5 ||
       dot(referencedvEP, ei.faceTangent(1)) > 0.5){
      std::cout << referencedvEP << std::endl;
      message("1 normals vector for edge %d - %d too close to plane",
              edgeId, otherEdgeId);
      return false;
    }

    if(dot(referencedvPE, pi.faceTangent(0)) > 0.5 ||
       dot(referencedvPE, pi.faceTangent(1)) > 0.5){
      std::cout << referencedvEP << std::endl;
      message("2 normals vector for edge %d - %d too close to plane",
              edgeId, otherEdgeId);
      return false;
    }



    bool deg = false;

    Vector4<T> pp1, pp2, bb1, bb2;

    T sd = ei.getSignedDistance(pi, &pp1, &pp2, &bb1, &bb2, &deg, &referencedvEP);

    if(deg){
      return false;
    }

    /*Signed distance must be positive for ei pi pair*/
    if(sd < 0.0 && colCode == 1){
      referencedvEP = -referencedvEP;
      referencedvPE = -referencedvPE;
      message("intersection0 = %d", intersection0);
      message("intersectionc = %d", intersectionc);
      message("d0 = %10.10e", distance0);
      message("dc = %10.10e", distancec);
      error("interpolated signed distance is negative, should be positive, sd = %10.10e", sd);
      return false;
    }
#endif
    if(debug){

      warning("colcode = %d", colCode);

      warning("refEP");

      message("intersection0 = %d", intersection0);
      message("intersectionc = %d", intersectionc);
      message("d0 = %10.10e", distance0);
      message("dc = %10.10e", distancec);
      message("di = %10.10e", sd);

      std::cerr << referencedvEP << std::endl;
      std::cerr << ei << std::endl;
      std::cerr << pi << std::endl;

      message("input e0 p0, eu, pu");
      std::cerr << e0 << std::endl;
      std::cerr << p0 << std::endl;
      std::cerr << eu << std::endl;
      std::cerr << p  << std::endl;

      getchar();
      bool deg = false;

      T sdc = eu.getSignedDistance(p, &pp1, &pp2, &bb1, &bb2, &deg, &referencedvEP);

      std::cerr << pp1 << std::endl;
      std::cerr << pp2 << std::endl;
      std::cerr << bb1 << std::endl;
      std::cerr << bb2 << std::endl;

      T sd0 = e0.getSignedDistance(p0, &pp1, &pp2, &bb1, &bb2, &deg, &referencedvEP);

      std::cerr << pp1 << std::endl;
      std::cerr << pp2 << std::endl;
      std::cerr << bb1 << std::endl;
      std::cerr << bb2 << std::endl;

      T sd = ei.getSignedDistance(pi, &pp1, &pp2, &bb1, &bb2, &deg, &referencedvEP);

      std::cerr << pp1 << std::endl;
      std::cerr << pp2 << std::endl;
      std::cerr << bb1 << std::endl;
      std::cerr << bb2 << std::endl;

      message("sdc = %10.10e", sdc);
      message("sd0 = %10.10e", sd0);
      message("sd  = %10.10e", sd);

      warning("distance = %10.10e", sd);
      warning("root = %10.10e", *dist);

      getchar();
      //error("stop");
    }

    //ei = e0;
    //pi = p0;

    //pi.infiniteProjections(ei, 0, 0, &baryi1, &baryi2);

    if(colCode == -1 || colCode == 0){
      return false;
    }

    if(pi.isConcave() && ei.isConcave()){
      /*If both edges are concave, then the negated edges are both
        convex, which could potentially trigger and intersection.*/
      //return false;
    }

    if(pi.isIndeterminate() || ei.isIndeterminate()){
      //return false;
    }

    /*Distance between ei and pi is most likely zero, provide
      referencevector for correction*/

    dv = ei.getOutwardNormal(pi, &referencedvEP);

    if(IsNan(dv)){
      std::cout << ei << std::endl;
      std::cout << pi << std::endl;

      std::cout << referencedvEP << std::endl;

      std::cout << ei.getOutwardNormal(pi) << std::endl;

      std::cout << dv << std::endl;

      error("NaN");
    }

    if(dot(dv, referencedvEP) < 0){
      error("new dv not in sync");
    }

    if(colCode == 1){
      /*An intersection can be invalid due to flipped normals*/

      /*Check if edges are in a valid configuration*/

      //Vector4<T> referencedvEPTemp =  dv;
      //Vector4<T> referencedvPETemp = -dv;

      /*Check distances*/
      Vector4<T> x1, x2;
      bool parallel = false;
      ei.infiniteProjections(pi, &x1, &x2, 0, 0, &parallel);

      if((x1-x2).length() > 2e-8){
        return false;
      }

      if(parallel){
        return false;
      }

      return true; /*Valid*/
    }
    return false; /*No coliision*/
  }

  /*Evaluate signed distance*/
  template<class T>
  bool ConstraintCandidateEdge<T>::evaluate(ConstraintSetEdge<T>* c,
                                            CollisionContext<T>* ctx,
                                            const Edge<T>& edge,
                                            const Vector4<T>& com,
                                            const Quaternion<T>& rot,
                                            Vector4<T>* bb1,
                                            Vector4<T>* bb2,
                                            bool* updated){
    bool debug = false;

    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

#if 1
    if(//(edgeId == 1549 && otherEdgeId == 53802) ||
       //(otherEdgeId == 1549 && edgeId == 53802)

       //(edgeId == 64451 && otherEdgeId == 14866) ||
       //(edgeId == 14866 && otherEdgeId == 64451) ||
       //(edgeId == 64451 && otherEdgeId == 35300) ||
       //(edgeId == 35300 && otherEdgeId == 64451)
       //edgeId == 976 || otherEdgeId == 976
       //(edgeId == 21398 && otherEdgeId == 24275) ||
       //(edgeId == 24275 && otherEdgeId == 21398)
       //(edgeId == 61120 && otherEdgeId == 22997) ||
       //(edgeId == 22997 && otherEdgeId == 61120) ||
       //(edgeId == 61121 && otherEdgeId == 22997) ||
       //(edgeId == 22997 && otherEdgeId == 61121)


       //       (edgeId == 17749 && otherEdgeId == 65333) ||
       //  (edgeId == 65333 && otherEdgeId == 17749)

       //(edgeId == 10206 && otherEdgeId == 23895) ||
       //(edgeId == 23895 && otherEdgeId == 10206) ||
       //(edgeId == 62092 && otherEdgeId == 23895) ||
       //(edgeId == 23895 && otherEdgeId == 62092)

       //(edgeId == 173381 && otherEdgeId == 33419) ||
       //(edgeId == 33419 && otherEdgeId == 173381)

       //(edgeId == 173381 && otherEdgeId == 33485) ||
       //(edgeId == 33485 && otherEdgeId == 173381)

       //false

       //(edgeId == 447 && otherEdgeId == 52408) ||
       //(edgeId == 52408 && otherEdgeId == 447)

       //||
       //(edgeId == 15362 && otherEdgeId == 3154) ||
       //(otherEdgeId == 15362 && edgeId == 3154)
       //false
       //(edgeId == 142031 && otherEdgeId == 9812) ||
       //(edgeId == 9812   && otherEdgeId == 142031)
       //||
       //(edgeId == 142028 && otherEdgeId == 9812) ||
       //(edgeId == 9812   && otherEdgeId == 142028)
       (edgeId == 144166 && otherEdgeId == 56468) ||
       (edgeId == 56468  && otherEdgeId == 144166) ||
       (edgeId == 144164 && otherEdgeId == 56468) ||
       (edgeId == 56468  && otherEdgeId == 144164)
       ){
      warning("%d - %d, backFace = %d", edgeId, otherEdgeId, backFace);

      debug = true;
    }
#endif

    bool collapsedE = edgeCollapsed(e0, eu);
    bool collapsedP = edgeCollapsed(p0, edge);

    if(backFace){
      ctx->collapsedBackEdges[edgeId] = collapsedE;
      ctx->collapsedBackEdges[otherEdgeId] = collapsedP;
    }else{
      ctx->collapsedEdges[edgeId] = collapsedE;
      ctx->collapsedEdges[otherEdgeId] = collapsedP;
    }

    if(eu.getTangentDistance(0) < 5e-4 ||
       eu.getTangentDistance(1) < 5e-4 ||
       edge.getTangentDistance(0) < 5e-4 ||
       edge.getTangentDistance(1) < 5e-4){
      warning("tangent distances too small, wait for other constraints");
      if(debug){
        getchar();
      }
      return false;
    }

    if(collapsedE || collapsedP){
      collapsed = true;


      //START_DEBUG;
      warning("COLLAPSED %d - %d, backface = %d", edgeId, otherEdgeId, backFace);

      message("collapsed E = %d, P = %d", collapsedE, collapsedP);

      std::cerr << e0 << std::endl;
      std::cerr << eu << std::endl;
      std::cerr << p0 << std::endl;
      std::cerr << edge << std::endl;
      //END_DEBUG;

      if(collapsedE){
        //If there is a transistion from concave to convex, force the
        //corresponding vertex/face pair to be active

        /*If a back or frontface must be activated, depends on the
          onfiguration of the edge and the type of the edge. Not only
          on the type of the edge. If a frontface edge collapses, a
          backface constraint must be enabled if the initial
          configuration is convex and the current one is concave (and
          collapsed)*/

        if(e0.isConvex() && eu.isConcave()){
          if(!backFace){
            ctx->backSets[e0.faceVertexId(0)].forceConstraint(ctx, e0.faceId(1));
            ctx->backSets[e0.faceVertexId(1)].forceConstraint(ctx, e0.faceId(0));
          }else{
            ctx->sets[e0.faceVertexId(0)].forceConstraint(ctx, e0.faceId(1));
            ctx->sets[e0.faceVertexId(1)].forceConstraint(ctx, e0.faceId(0));
          }
        }
      }

      if(collapsedP){
        //If there is a transistion from concave to convex, force the
        //corresponding vertex/face pair to be active

        if(p0.isConvex() && edge.isConcave()){
          if(!backFace){
            ctx->backSets[p0.faceVertexId(0)].forceConstraint(ctx, p0.faceId(1));
            ctx->backSets[p0.faceVertexId(1)].forceConstraint(ctx, p0.faceId(0));
          }else{
            ctx->sets[p0.faceVertexId(0)].forceConstraint(ctx, p0.faceId(1));
            ctx->sets[p0.faceVertexId(1)].forceConstraint(ctx, p0.faceId(0));
          }
        }
      }

      if(debug){
        getchar();
      }

      return false;
    }else{
      collapsed = false;
    }

    intersectionc = eu.edgesIntersect(edge, (T)SAFETY_DISTANCE,
                                      rigidEdge1 || rigidEdge2, debug);

    /*If there is no intersection, check if they could have intersected*/

    bool exceptionalIntersection = false;

    if(updated){
      *updated = true;
    }

    if(debug){
      message("prev referencedvEP");
      std::cout << referencedvEP << std::endl;
    }

    /*Compute current distance, used for final check*/
    referencedvEP = eu.getOutwardNormal(edge, &referencedvEP);
    referencedvPE = -referencedvEP;

    if(debug){
      message("new referencedvEP");
      std::cout << referencedvEP << std::endl;
    }

    bool degenerate = false;
    distancec = eu.getSignedDistance(edge, 0, 0, 0, 0, &degenerate, &referencedvEP);// - (T)DISTANCE_EPS3;


    if(debug){
      message("intersection0 = %d", intersection0);
      message("intersectionc = %d", intersectionc);
      message("distance0 = %10.10e", distance0);
      message("distancec = %10.10e", distancec);
      message("dc degenerate = %d", degenerate);
      message("e0.isConvex() = %d", e0.isConvex());
      message("p0.isConvex() = %d", p0.isConvex());
      message("eu.isConvex() = %d", eu.isConvex());
      message("pu.isConvex() = %d", edge.isConvex());
    }



    distance0 = e0.getSignedDistance(p0, 0, 0, 0, 0, &degenerate, &referencedvEP);



    if(debug){
      message("d0 degenerate = %d", degenerate);
      message("distance0 = %10.10e", distance0);
    }

    if(status == Enabled){
      //distancec -= (T)DISTANCE_EPS3;
      if(debug){
        message("constraint already enabled");

        ContactConstraint<2, T>* cc =
          (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

        cc->print(std::cout);
      }
      return false;
    }

#if 1
    if( ((intersection0 == false && intersectionc)
         ) && !exceptionalIntersection
       //&&
       //(Sign(distance0) != Sign(distancec))
        ){


      if(Abs(distancec) < (T)SAFETY_DISTANCE){
        /*Edge crosses through cylinder area, figure out outward normal*/
      //if(false){
      //if(Abs(distancec - (T)SAFETY_DISTANCE) < 1e-8){
      //if(Abs(distancec) < (T)SAFETY_DISTANCE){
        warning("distance too small %10.10e", distancec);

        /*Separate edges by displacing them in the negative normal
          direction, edges are convex by definition*/

        Vector4<T> normalFromFacesAndTangentsE =
          (eu.faceNormal(0)  + eu.faceNormal(1) -
           eu.faceTangent(0) - eu.faceTangent(1) ).normalize();

        Vector4<T> normalFromFacesAndTangentsP =
          (edge.faceNormal(0)  + edge.faceNormal(1) -
           edge.faceTangent(0) - edge.faceTangent(1) ).normalize();


        Vector4<T> veu0 = eu.vertex(0) - (T)1e-7 * normalFromFacesAndTangentsE;
        Vector4<T> veu1 = eu.vertex(1) - (T)1e-7 * normalFromFacesAndTangentsE;
        Vector4<T> fveu0 = eu.faceVertex(0) - (T)1e-7 * normalFromFacesAndTangentsE;
        Vector4<T> fveu1 = eu.faceVertex(1) - (T)1e-7 * normalFromFacesAndTangentsE;

        Edge<T> eu2(veu0, veu1, fveu0, fveu1);

        Vector4<T> vpu0 = edge.vertex(0) - (T)1e-7 * normalFromFacesAndTangentsP;
        Vector4<T> vpu1 = edge.vertex(1) - (T)1e-7 * normalFromFacesAndTangentsP;
        Vector4<T> fvpu0 = edge.faceVertex(0) - (T)1e-7 * normalFromFacesAndTangentsP;
        Vector4<T> fvpu1 = edge.faceVertex(1) - (T)1e-7 * normalFromFacesAndTangentsP;

        Edge<T> pu2(vpu0, vpu1, fvpu0, fvpu1);

        intersectionc =
          eu.edgesIntersect(edge, (T)SAFETY_DISTANCE,
                            rigidEdge1 || rigidEdge2, true);

        message("referencedvEPx2");
        std::cout << referencedvEP << std::endl;

        /*Compute outward normal without reference, this forces the
          edges to be separated before computing the normal.*/
        message("get outward normal, %d - %d", edgeId, otherEdgeId);

        referencedvEP = eu2.getOutwardNormal(pu2);
        referencedvPE = -referencedvEP;

        std::cout << referencedvEP << std::endl;

        T dist = eu2.getSignedDistance(pu2, 0, 0, 0, 0, 0, &referencedvEP);

        message("dist after recomputing normal = %10.10e", dist);

        if(Abs(dist) < (T)SAFETY_DISTANCE){
          message("not improved");
          /*not improved*/
          return false;
        }

        /*Distance must be positive since the edges are displaced in
          the negative normal direction*/

        /*To prevent that we hit zero again*/
        if(dist - (T)0.5*SAFETY_DISTANCE < 0.0){
          /*Distance must be positive*/
          referencedvEP = -referencedvEP;
          referencedvPE = -referencedvPE;
          warning("flipped due to some error");
          distancec = eu.getSignedDistance(edge, 0, 0, 0, 0, &degenerate, &referencedvEP);// - (T)DISTANCE_EPS3;
          distance0 = e0.getSignedDistance(p0, 0, 0, 0, 0, &degenerate, &referencedvEP);// - (T)DISTANCE_EPS3;
        }
      }else{
        /*Distance must be negative since edges are intersecting*/

        if(distancec - (T)SAFETY_DISTANCE > 0.0){
          DBG(message("flipping reference"));
          DBG(std::cout << referencedvEP << std::endl);
          referencedvEP = -referencedvEP;
          referencedvPE = -referencedvPE;
          distancec = eu.getSignedDistance(edge, 0, 0, 0, 0, &degenerate, &referencedvEP);// - (T)DISTANCE_EPS3;
          distance0 = e0.getSignedDistance(p0, 0, 0, 0, 0, &degenerate, &referencedvEP);// - (T)DISTANCE_EPS3;
          DBG(std::cout << referencedvEP << std::endl);
          DBG(message("new distancec = %10.10e", distancec));
        }
      }
    }
#endif

    //distancec -= (T)DISTANCE_EPS3;

    if(debug){
      warning("distance0     = %10.10e", distance0);
      warning("distancec     = %10.10e", distancec);
      warning("intersection0 = %d", intersection0);
      warning("intersectionc = %d", intersectionc);
      warning("backface      = %d", backFace);
      warning("enabled       = %d", status == Enabled);
      std::cerr << e0 << std::endl;
      std::cerr << p0 << std::endl;
      std::cerr << eu << std::endl;
      std::cerr << edge << std::endl;
      warning("reference dv");
      std::cerr << referencedvEP << std::endl;


      getchar();

    }

    if(debug && ( (e0.isConvex() && eu.isConcave()) ||
                  (e0.isConcave() && eu.isConvex()) ||
                  (p0.isConvex() && edge.isConcave()) ||
                  (p0.isConcave() && edge.isConvex())) && backFace){
      warning("edge changed from concave to convex or the other way around");
      warning("distance0     = %10.10e", distance0);
      warning("distancec     = %10.10e", distancec);
      warning("intersection0 = %d", intersection0);
      warning("intersectionc = %d", intersectionc);
      warning("backface      = %d", backFace);
      warning("enabled       = %d", status == Enabled);


      warning("X reference");
      std::cerr << referencedvEP << std::endl;

      T sd1 = eu.getSignedDistance(edge, 0,0,0,0,0,&referencedvEP);
      warning("sd1 = %10.10e", sd1);

      T sd2 = edge.getSignedDistance(eu, 0,0,0,0,0,&referencedvPE);
      warning("sd2 = %10.10e", sd2);

      Vector4<T> normalFromFacesAndTangents =
        (eu.faceNormal(0)  + eu.faceNormal(1) -
         eu.faceTangent(0) - eu.faceTangent(1) ).normalize();

      warning("normal from tangent and normal");
      std::cerr << normalFromFacesAndTangents << std::endl;

      std::cerr << eu << std::endl;
      std::cerr << edge << std::endl;
      std::cerr << e0 << std::endl;
      std::cerr << p0 << std::endl;
      //error("stop");

    }

    if( ( (e0.isConvex() && eu.isConcave()) ||
          (e0.isConcave() && eu.isConvex()) ||
          (p0.isConvex() && edge.isConcave()) ||
          (p0.isConcave() && edge.isConvex())) ){
      //return false;
    }

    Vector4<T> bary01, bary02, baryc1, baryc2;
    e0.infiniteProjections(p0,   0, 0, &bary01, &bary02, 0);
    eu.infiniteProjections(edge, 0, 0, &baryc1, &baryc2, 0);

    /*
    bool projection0 =
      (e0.barycentricOnEdge(bary01, (T)0.0) &&
       p0.barycentricOnEdge(bary02, (T)0.0));

    bool projectionc =
      (  eu.barycentricOnEdge(baryc1, (T)0.0) &&
       edge.barycentricOnEdge(baryc2, (T)0.0));
    */

    bool projection =
      barycentricEdgeMatch(bary01, baryc1) &&
      barycentricEdgeMatch(bary02, baryc2);

    projection = false;

    if(status != Enabled){
      bool test = false;
      if(projection){
        /*Collisionpoint stays 'inside' the edges. A collision can
          only be due to a change from positive to negative.*/
        if(
           (!intersection0 && intersectionc) &&
           (distance0 > SAFETY_DISTANCE && distancec < SAFETY_DISTANCE)
           ){
          test = true;
        }
      }else{
        if(
           ((!intersection0 && intersectionc &&
             distance0 > SAFETY_DISTANCE && distancec < SAFETY_DISTANCE)
            ||
            (intersection0 && intersectionc && distance0 < SAFETY_DISTANCE && distancec < SAFETY_DISTANCE)
            )
           ){
          test = true;
        }
        //test = true;
      }
      if(
#if 0
         ((intersection0 == false && intersectionc)
          ||
          (!intersection0 && !intersectionc))
         //)
         //|| /*Normal case*/
         /*Transistion to concave edges, there could be an
           intersection in between*/
         //(e0.isConvex() && eu.isConcave())
         //||
         //(p0.isConvex() && edge.isConcave())

         &&
         //distance0 > SAFETY_DISTANCE*0 && distancec < SAFETY_DISTANCE*0
         ((distance0 > SAFETY_DISTANCE && distancec < SAFETY_DISTANCE)
          ||
          (distance0 > SAFETY_DISTANCE && distancec > SAFETY_DISTANCE))
#endif
         test
         ){
        /*Get normal for non-intersected case*/
        //referencedvEP = e0.getOutwardNormal(p0, 0);
        //referencedvPE = -referencedvEP;


        if(exceptionalIntersection){
          //reference already computed
        }else{
#if 1
          /*Get normal after getting reference*/
          Vector4<T> oldref = referencedvEP;

          referencedvEP = eu.getOutwardNormal(edge, &referencedvEP);
          referencedvPE = -referencedvEP;
          /*From non intersecting case to intersecting case
            -> trigger root finder*/

          if(debug){
            message("old ref");
            std::cout << oldref << std::endl;
            message("new ref");
            std::cout << referencedvEP << std::endl;
          }

          /*eu must be convex*/
          /*in case of a valid intersection, the contact normal is
            always between the two facenormals. However, if the edge
            is collapsed, i.e., eu is concave, this test is
            failing. Therefore, eu must be convex.*/
          Vector4<T> normalFromFacesAndTangents;

          if(eu.isConvex()){
            normalFromFacesAndTangents =
              (eu.faceNormal(0)  + eu.faceNormal(1) -
               eu.faceTangent(0) - eu.faceTangent(1) ).normalize();
          }else{
            normalFromFacesAndTangents =
              (eu.faceNormal(0)  + eu.faceNormal(1) +
               eu.faceTangent(0) + eu.faceTangent(1) ).normalize();
          }

          if(dot(referencedvEP, normalFromFacesAndTangents) < 0.0){
            referencedvEP = -referencedvEP;
            referencedvPE = -referencedvEP;
            (warning("corrected reference vector using tangents and normals\n\n"));
            if(debug){
              message("corrected reference vector using tangents, tangent = ");
              std::cout << normalFromFacesAndTangents << std::endl;
            }
          }


          if(dot(eu.faceNormal(0), referencedvEP) < 0.0 &&
             dot(eu.faceNormal(1), referencedvEP) < 0.0 ){
            warning("%d - %d", edgeId, otherEdgeId);
            warning("distance0     = %10.10e", distance0);
            warning("distancec     = %10.10e", distancec);
            warning("intersection0 = %d", intersection0);
            warning("intersectionc = %d", intersectionc);
            warning("backface      = %d", backFace);
            warning("enabled       = %d", status == Enabled);

            std::cerr << referencedvEP << std::endl;
            std::cerr << eu << std::endl;
            std::cerr << edge << std::endl;
            std::cerr << e0 << std::endl;
            std::cerr << p0 << std::endl;

            error("reference does not match facenormals");
          }
#endif

          if(IsNan(referencedvEP)){
            //std::cout << oldref << std::endl;
            std::cout << referencedvEP << std::endl;
            error("dv = NAN");
          }

        }

        if(debug){
          warning("distance0     = %10.10e", distance0);
          warning("distancec     = %10.10e", distancec);
          warning("intersection0 = %d", intersection0);
          warning("intersectionc = %d", intersectionc);
          warning("backface      = %d", backFace);
          warning("enabled       = %d", status == Enabled);


          warning("X reference");
          std::cerr << referencedvEP << std::endl;

          T sd1 = eu.getSignedDistance(edge, 0,0,0,0,0,&referencedvEP);
          warning("sd1 = %10.10e", sd1);

          T sd2 = edge.getSignedDistance(eu, 0,0,0,0,0,&referencedvPE);
          warning("sd2 = %10.10e", sd2);

          /*eu should be convex*/
          Vector4<T> normalFromFacesAndTangents =
            (eu.faceNormal(0)  + eu.faceNormal(1) -
             eu.faceTangent(0) - eu.faceTangent(1) ).normalize();

          warning("normal from tangent and normal");
          std::cerr << normalFromFacesAndTangents << std::endl;

          std::cerr << eu << std::endl;
          std::cerr << edge << std::endl;
          std::cerr << e0 << std::endl;
          std::cerr << p0 << std::endl;


        }
        return true;
      }
    }else{
      /*Not enabled*/
      //referencedvEP = eu.getOutwardNormal(edge, &referencedvEP);
      //referencedvPE = -referencedvEP;
    }
    if(debug){
      getchar();
    }
    return false;
  }

  /*Update position of edge*/
  template<class T>
  void ConstraintCandidateEdge<T>::updateGeometry(CollisionContext<T>* ctx){
    ctx->mesh->getDisplacedEdge(&eu, edgeId, backFace);

    ctx->mesh->setCurrentEdge(edgeId);
    int vertex = ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(ctx->mesh->vertices[vertex].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertex];

      comeu = ctx->mesh->centerOfMassDispl[objectId];
      roteu = ctx->mesh->orientationsDispl[objectId];
    }
  }

  template<class T>
  void ConstraintCandidateEdge<T>
  ::updateInactiveConstraint(ConstraintSetEdge<T>* c,
                             CollisionContext<T>* ctx,
                             const Edge<T>& x,
                             const Edge<T>& p,
                             const Vector4<T>& com,
                             const Quaternion<T>& rot){

    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

    e0 = x;
    ei = e0;
    eu = e0;

    come0 = comeu = comei = Vector4<T>(0,0,0,0);
    rote0 = roteu = rotei = Quaternion<T>(0,0,0,1);

    ctx->mesh->setCurrentEdge(edgeId);
    int vertex = ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(ctx->mesh->vertices[vertex].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertex];

      come0 = comeu = comei = ctx->mesh->centerOfMass[objectId];
      rote0 = roteu = rotei = ctx->mesh->orientations[objectId];

      comeu = ctx->mesh->centerOfMassDispl[objectId];
      roteu = ctx->mesh->orientationsDispl[objectId];
    }

    p0 = p;
    pi = p;

    comp0 = compi = com;
    rotp0 = rotpi = rot;

    intersection0 = intersectionc =
      e0.edgesIntersect(p0, (T)SAFETY_DISTANCE, rigidEdge1 || rigidEdge2);

    if(intersection0 == false){
      message("2::edgeIds = %d, %d, backface = %d", edgeId, otherEdgeId, backFace);
      referencedvEP = e0.getOutwardNormal(p0, 0);
      referencedvPE = -referencedvEP;

      if(IsNan(referencedvEP)){
        error("dv = NAN");
      }
    }

    //contactNormal = referencedvEP;

    /*Try to invalidate combination p-e as soon as possible*/
    if(status != Enabled){
      Vector4<T> ldv;

      if(e0.isIndeterminate() || p0.isIndeterminate()){
        //return;
      }
    }

    if(e0.isIndeterminate() || p0.isIndeterminate()){
      if(status == Enabled){
        //disableConstraint(c, ctx, true);
      }
      //return;
    }
  }

  template<class T>
  bool ConstraintCandidateEdge<T>::disableConstraint(ConstraintSetEdge<T>* c,
                                                     CollisionContext<T>* ctx,
                                                     bool force){
    if(status == Enabled){
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);

#ifdef ACTIVE_CONSTRAINTS
      cc->cacheMultipliers(cachedMultipliers, *ctx->getSolver()->getx());
#endif

      bool active = false;

      status = Disabled;

      cgfassert(enabledId == cc->id);

      /*Cache corresponding tangent vectors for future use*/
      tangentReference[0] = tangentReference[1].set(0,0,0,0);

      int indices[4];
      ctx->mesh->getEdgeIndices(c->edgeId, indices    );
      ctx->mesh->getEdgeIndices(   edgeId, indices + 2);

      bool rigidEdge1 = false;//=edge set edge
      bool rigidEdge2 = false;//=this edge

      for(int i=0;i<2;i++){
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge1 = true;
        }
      }

      for(int i=2;i<4;i++){
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge2 = true;
        }
      }

      if(rigidEdge1){
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[1] += cc->getRow(2,0);
      }else{
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[0] += cc->getRow(1,1);
        tangentReference[1] += cc->getRow(2,0);
        tangentReference[1] += cc->getRow(2,1);
      }

      if(rigidEdge2){
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[1] -= cc->getRow(2,2);
      }else{
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[0] -= cc->getRow(1,3);
        tangentReference[1] -= cc->getRow(2,2);
        tangentReference[1] -= cc->getRow(2,3);
      }

      tangentReference[0].normalize();
      tangentReference[1].normalize();

      tangentialDefined = true;

      if(!force){
        active = false;
      }else{
        /*Force removal*/
        active = false;
      }

      if(active == false){
        ctx->getSolver()->removeConstraint(enabledId);
        status = Disabled;

        delete cc;

        return true;
      }
    }else{
      /*Constraint was already disabled*/
    }
    return false;
  }


  template<class T>
  void ConstraintCandidateEdge<T>::updateConstraint(ConstraintSetEdge<T>*c,
                                                    CollisionContext<T>* ctx,
                                                    const Edge<T>& x,
                                                    const Edge<T>& p,
                                                    const Vector4<T>& com,
                                                    const Quaternion<T>& rot,
                                                    bool* skipped,
                                                    EvalStats& stats){
    ContactConstraint<2, T>* cc =
      (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

    bool debug = false;
    //Vector4<T> baryi1Old = baryi1;
    //Vector4<T> baryi2Old = baryi2;

    if(

       //(edgeId == 173381 && otherEdgeId == 33419) ||
       //(edgeId == 33419 && otherEdgeId == 173381) ||

       //(edgeId == 173381 && otherEdgeId == 33485) ||
       //(edgeId == 33485 && otherEdgeId == 173381)

       false

       //||

       //(edgeId == 25100 && otherEdgeId == 19644) ||
       //(edgeId == 19644 && otherEdgeId == 25100)

       ){
      debug = true;
    }

#ifdef DEBUG_UPDATE
    debug = true;
#endif

    if(debug){
      warning("updateConstraint");
      std::cerr << e0 << std::endl;
      std::cerr << p0 << std::endl;
      std::cerr << eu << std::endl;
      std::cerr << p << std::endl;
      std::cerr << x << std::endl;
      std::cerr << ei << std::endl;
      std::cerr << pi << std::endl;
      std::cerr << enabledEU << std::endl;
      std::cerr << enabledPU << std::endl;
      std::cerr << enabledE0 << std::endl;
      std::cerr << enabledP0 << std::endl;

      warning("referencedvEP0");
      std::cerr << referencedvEP0 << std::endl;
      std::cerr << referencedvEP << std::endl;
      std::cerr << dv << std::endl;
      std::cerr << contactNormal << std::endl;
    }

    Vector4<T> contactNormalOld = ei.getOutwardNormal(pi, &dv);

    if(debug){
      message("old contact normal");
      std::cerr << contactNormalOld << std::endl;
    }

    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

    T initialWeight = (T)3.0;

    /*Find a proper configuration for the new interpolated edges in
      which the change in the contact normal is not extremely
      large. If the dotproduct between the new and current
      contactnormal is more than 0.25, the current edges are
      accepted. If not, the initial weight is reduced such that the
      new configuration changes less compared to the used
      configuration. This continues until a proper set of edges are
      found for which their contact normal does not change much
      compared to the previous case.

      When edges are short, a tiny change in a vertex position can
      result in a large change in the contactnormal. Using this
      procedure the method is prevented to make too large steps toward
      the unknown solution.*/

    Edge<T> eii;
    Edge<T> pii;

    Vector4<T> weights1;
    Vector4<T> weights2;

    T weight1 = (T)1.0;
    T weight2 = (T)0.0;

    if(debug){
      cc->print(std::cerr);
    }

    while(true){
      weight1 = (T)1.0 - (T)1.0/(T)initialWeight;
      weight2 = (T)1.0 - weight1;

      Edge<T> eii2;
      Edge<T> pii2;

      if(rigidEdge1){
        Vector4<T> newComPi = weight1 * compi + weight2 * com;
        Quaternion<T> newRotPi = slerp(rotpi, //is already corrected
                                       rot*rotp0.inverse(), weight2);

        Vector4<T> v11 = newComPi + newRotPi.rotate(p0.vertex(0)  - comp0);
        Vector4<T> v12 = newComPi + newRotPi.rotate(p0.vertex(1)  - comp0);
        Vector4<T> v13 = newComPi + newRotPi.rotate(p0.faceVertex(0) - comp0);
        Vector4<T> v14 = newComPi + newRotPi.rotate(p0.faceVertex(1) - comp0);

        pii2.set(v11, v12, v13, v14);
      }else{
        pii2 = interpolateEdges(pi,  p, (T)1, (T)0, weight1);
      }

      if(rigidEdge2){
        Vector4<T> newComEi = weight1 * comei + weight2 * comeu;
        Quaternion<T> newRotEi = slerp(rotei, //is already corrected
                                       roteu*rote0.inverse(), weight2);

        Vector4<T> v11 = newComEi + newRotEi.rotate(e0.vertex(0)  - come0);
        Vector4<T> v12 = newComEi + newRotEi.rotate(e0.vertex(1)  - come0);
        Vector4<T> v13 = newComEi + newRotEi.rotate(e0.faceVertex(0) - come0);
        Vector4<T> v14 = newComEi + newRotEi.rotate(e0.faceVertex(1) - come0);

        eii2.set(v11, v12, v13, v14);
      }else{
        eii2 = interpolateEdges(ei, eu, (T)1, (T)0, weight1);
      }

      /*This can and will go wrong if the change in the contactnormal
        changes significantly.*/
      contactNormal = eii2.getOutwardNormal(pii2, &dv);

      if(debug){
        message("contactNormal = ");
        std::cout << contactNormal << std::endl;
        message("contactNormalOld = ");
        std::cout << contactNormalOld << std::endl;
        message("dot = %10.10e", dot(contactNormal, contactNormalOld));
      }

      bool parallel = false;
      eii2.infiniteProjections(pii2, 0, 0, &weights2, &weights1, &parallel);

      if(debug){
        message("weights");
        std::cout << weights1 << std::endl;
        std::cout << weights2 << std::endl;

        warning("weight = %10.10e, %10.10e", weight1, weight2);
      }

      if(parallel){
        throw new DegenerateCaseException(__LINE__, __FILE__, "parallel edges after update");
      }

      if(dot(contactNormal, contactNormalOld) > 0.85){
        /*The new configuration is reasonable, accept this one.*/
        eii = eii2;
        pii = pii2;
        baryi1 = weights1;
        baryi2 = weights2;
        break;
      }

      /*The current configuration is has a large change compared to
        the previos configuration, choose one closer to the previous
        approximation.*/
      initialWeight *= (T)2.0;
    }

    bool convex1 = eii.isConvex();
    bool convex2 = pii.isConvex();

    message("convex1 = %d", convex1);
    message("convex2 = %d", convex2);

#if 0
    //Vector4<T> diff1 =
    //  (eii.getCoordinates(weights2) - eii.getCoordinates(baryi2Old));
    //Vector4<T> diff2 =
    //  (pii.getCoordinates(weights1) - pii.getCoordinates(baryi1Old));

    message("points");
    std::cout << eii.getCoordinates(weights2) << std::endl;
    std::cout << eii.getCoordinates(baryi2Old) << std::endl;
    std::cout << pii.getCoordinates(weights1) << std::endl;
    std::cout << pii.getCoordinates(baryi1Old) << std::endl;
    message("bary");
    std::cout << weights2 << std::endl;
    std::cout << baryi2Old << std::endl;
    std::cout << weights1 << std::endl;
    std::cout << baryi1Old << std::endl;
#endif
    if(dot(contactNormalOld, contactNormal) < (T)0.999
       ||
       (baryi1 - weights1).length() > 1e-4 ||
       (baryi2 - weights2).length() > 1e-4 ){
      //diff1.length() > 1e-4 ||
      //diff2.length() > 1e-4){
      //if(true){
      /*The contact normal has changed significantly, we need to
        update the constraint and so we need to update the
        preconditioner and residual vector.*/
      referencedvEP = contactNormal;
      referencedvPE = -contactNormal;
      dv = contactNormal;

#if 0
      bool degenerate = false;
      distancec = eii.getSignedDistance(pii, &colPoint, 0,
                                        &baryi2, &baryi1,
                                        &degenerate, &referencedvEP);// - (T)DISTANCE_EPS;
      if(degenerate){
        disableConstraint(c, ctx, true);
        return;
      }
#endif

      warning("contact normal changed");
      if(debug){
        warning("reference PE");
        std::cerr << referencedvPE << std::endl;
        warning("reference EP");
        std::cerr << referencedvEP << std::endl;

        warning("p0e0");
        std::cerr << p0 << std::endl;
        std::cerr << e0 << std::endl;

        warning("piei");
        std::cerr << pi << std::endl;
        std::cerr << ei << std::endl;

        warning("piieii");
        std::cerr << pii << std::endl;
        std::cerr << eii << std::endl;
      }

#if 0//was 0
      if(pii.isConcave() && eii.isConcave()){
        /*One of the two edges is concave, so there must be a vertex
          face collision elsewhere that prevents penetration.*/
        disableConstraint(c, ctx, true);

        *skipped = true;
#if 1
        e0 = eu;
        rote0 = roteu;
        come0 = comeu;

        //updateInitialState(p, com, rot);
        c->updateStartPositions(p, com, rot, this);
#endif
        warning("Edge-Edge disabled due to double concave edge, %d - %d", edgeId, otherEdgeId);

        return;
      }
#endif

      if(pii.isIndeterminate() || eii.isIndeterminate()){
        //disableConstraint(c, ctx, true);
        //return;
      }

      ei = eii;
      pi = pii;

      if(debug){
        warning("weights of point");
        std::cerr << weights1 << std::endl;
        std::cerr << weights2 << std::endl;
      }

      /*Do not update collision point*/
      baryi1 = weights1;
      baryi2 = weights2;

      if(rigidEdge1){
        compi = compi * weight1 + com   * weight2;
        rotpi = slerp(rotpi, rot*rotp0.inverse(),   weight2);
      }

      if(rigidEdge2){
        comei = comei * weight1 + comeu * weight2;
        rotei = slerp(rotei, roteu*rote0.inverse(), weight2);
      }

      int indices[4];
      ctx->mesh->getEdgeIndices(c->edgeId, indices    );
      ctx->mesh->getEdgeIndices(   edgeId, indices + 2);

      T dt = ctx->getDT();

      //bool offEdge = false;
      //T offEdgeEps = (T)1e-8;

#if 1
      if((!pi.barycentricOnEdgeDist(weights1, (T)DISTANCE_EPS) ||
          !ei.barycentricOnEdgeDist(weights2, (T)DISTANCE_EPS))){
        message("contact off edge");
        std::cout << weights1 << std::endl;
        std::cout << weights2 << std::endl;
      }

      /*Check if new edges project on each other*/
      if((!pi.barycentricOnEdgeDist(weights1, (T)DISTANCE_EPSILON) ||
          !ei.barycentricOnEdgeDist(weights2, (T)DISTANCE_EPSILON))
         &&
         distancec > DISTANCE_EPSILON - DISTANCE_TOL
         ){
        /*Could be a source of potential errors!!*/
        *skipped = true;
        //updateInactiveConstraint(c, ctx, x, p, com, rot);
        message("don't care");
#if 1
        disableConstraint(c, ctx, true);
        e0 = eu;
        rote0 = roteu;
        come0 = comeu;

        e0 = eu;
        rote0 = roteu;
        come0 = comeu;

        p0 = p;
        rotp0 = rot;
        comp0 = com;

        distance0 = distancec;
        intersection0 = intersectionc;

        //updateInitialState(p, com, rot);

        c->updateStartPositions(p, com, rot, this);
#endif
        return;

        //offEdge = true;
        //if(distancec > offEdgeEps){
          //*skipped = true;
          //return;
        //}

      }
#endif

      if(debug){
        warning("Edges1 = %10.10e", (eu.vertex(0) - eu.vertex(1)).length());
        warning("Edges2 = %10.10e", (p.vertex(0)  -  p.vertex(1)).length());
        warning("Normal error = %10.10e", dot(contactNormal, contactNormalOld));
        getchar();
      }

      if(dot(contactNormal, contactNormalOld) < (T)0.0){
        /*Should be always positive by definition*/
        warning("Normal flip eii pii ei pi");
        std::cerr << eii << std::endl;
        std::cerr << pii << std::endl;
        getchar();
        error("should not be possible");
      }

      /*Update reference vectors*/
      referencedvEP =  contactNormal;
      referencedvPE = -contactNormal;
      dv = referencedvEP;

      /*Extract tangent vectors*/
      Vector4<T> t1, t2;
      /*
      t1 = -( cc->getRow(1, 0) - (cc->getRow(1, 1) +
                                  cc->getRow(1, 2) +
                                  cc->getRow(1, 3)));

      t2 = -( cc->getRow(2, 0) -
             (cc->getRow(2, 1) +
              cc->getRow(2, 2) +
              cc->getRow(2, 3)));
      */
      bool bodyEdge1  = true; //=edge-set edge
      bool bodyEdge2  = true; //=this edge
      bool rigidEdge1 = false;//=edge-set edge
      bool rigidEdge2 = false;//=this edge

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyEdge1 = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge1 = true;
        }
      }

      for(int i=2;i<4;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyEdge2 = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge2 = true;
        }
      }

      if(debug){
        warning("coordinates");
        std::cerr << pii.getCoordinates(weights1) << std::endl;
        std::cerr << eii.getCoordinates(weights2) << std::endl;
      }

#ifdef WEIGHT_CLAMPING
      //weights1 = p0.clampWeights(weights1, WEIGHT_MIN, WEIGHT_MAX);
      //weights2 = e0.clampWeights(weights2, WEIGHT_MIN, WEIGHT_MAX);
#else
      weights1 =  weights1;
      weights2 =  weights2;
#endif

      baryi1 = weights1;
      baryi2 = weights2;

      if(debug){
        warning("weights of point");
        std::cerr << weights1 << std::endl;
        std::cerr << weights2 << std::endl;


        warning("coordinates");
        std::cerr << pii.getCoordinates(weights1) << std::endl;
        std::cerr << eii.getCoordinates(weights2) << std::endl;
      }

      Vector4<T> re1, re2;

      t1 = t2 *= 0.0;

      if(bodyEdge1){
        if(rigidEdge1){
          re1  = pii.getCoordinates(weights1) - compi;
          t1 = -cc->getRow(1, 0);
          t2 = -cc->getRow(2, 0);
        }else{
          t1 = -cc->getRow(1, 0) - cc->getRow(1,1);
          t2 = -cc->getRow(2, 0) - cc->getRow(2,1);
        }
      }

      if(bodyEdge2){
        t1 = t2 *= 0.0;
        if(rigidEdge2){
          re2  = eii.getCoordinates(weights2) - comei;
          t1 = cc->getRow(1, 2);
          t2 = cc->getRow(2, 2);
        }else{
          t1 = cc->getRow(1, 2) + cc->getRow(1,3);
          t2 = cc->getRow(2, 2) + cc->getRow(2,3);
        }
      }

      if(debug){
        warning("t1");
        std::cerr << t1 << std::endl;
        warning("t2");
        std::cerr << t2 << std::endl;
      }

      t1.normalize();
      t2.normalize();

      /*Re-align tangent vectors with contact normal*/
      Vector4<T> tt1, tt2;
      tt2 = cross(t1, contactNormal).normalize();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      tt1 = cross(t2, contactNormal).normalize();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

      if(debug){
        warning("t1");
        std::cerr << t1 << std::endl;
        warning("t2");
        std::cerr << t2 << std::endl;
      }

      //cc->status = Inactive;
      //cc->frictionStatus[0] = Kinetic;
      //cc->frictionStatus[1] = Kinetic;

      /*Compute normal and tangential distances*/

      T d5 = ei.getPlaneDistance(p0.vertex(0), dv)*baryi1[0];
      T d6 = ei.getPlaneDistance(p0.vertex(1), dv)*baryi1[1];
      T d7 = ei.getPlaneDistance(e0.vertex(0), dv)*baryi2[0];
      T d8 = ei.getPlaneDistance(e0.vertex(1), dv)*baryi2[1];

      T totalDistance = (d5+d6-d7-d8) - (T)DISTANCE_EPS - (T)EPS;

#if 1
      /*Measure the error between the desired and current normal
        distance of the constraint. Use this error distance to update
        the constraint. This errordistance is then weighted/averaged
        in order to move the current configuration closer to the
        desired solution*/
      T errorDist = (T)(DISTANCE_EPS+EPS)*(T)1.0 - distancec;

      //if(offEdge){
      //  totalDistance = (d5+d6-d7-d8) - offEdgeEps;
      //  errorDist = offEdgeEps - distancec;
      // }

      //errorDist = Sign(errorDist)*Max(Abs(errorDist), (T)DISTANCE_EPS + (T)EPS);

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-4);

      //totalDistance = cc->getConstraintValue(0) - errorDist*(T)0.45;
      totalDistance -= errorDist*(T)0.45;
#endif

      //totalDistance = -(distance0 + (T)DISTANCE_EPS + (T)EPS - distancec);

      /*T1*/
      d5 = dot(t1, pii.vertex(0) - p0.vertex(0)) * baryi1[0];
      d6 = dot(t1, pii.vertex(1) - p0.vertex(1)) * baryi1[1];
      d7 = dot(t1, eii.vertex(0) - e0.vertex(0)) * baryi2[0];
      d8 = dot(t1, eii.vertex(1) - e0.vertex(1)) * baryi2[1];

      T totalDistanceT1 = -(d5+d6-d7-d8);

      /*T2*/
      d5 = dot(t2, pii.vertex(0) - p0.vertex(0)) * baryi1[0];
      d6 = dot(t2, pii.vertex(1) - p0.vertex(1)) * baryi1[1];
      d7 = dot(t2, eii.vertex(0) - e0.vertex(0)) * baryi2[0];
      d8 = dot(t2, eii.vertex(1) - e0.vertex(1)) * baryi2[1];

      T totalDistanceT2 = -(d5+d6-d7-d8);

      //totalDistanceT1 = totalDistanceT2 = (T)0.0;

      //totalDistanceT1 = cc->getConstraintValue(1);
      //totalDistanceT2 = cc->getConstraintValue(2);

      if(debug){
        warning("distance0       = %10.10e", distance0);
        warning("distancec       = %10.10e", distancec);
        warning("totalDistance   = %10.10e", totalDistance);
        warning("totalDistanceT1 = %10.10e", totalDistanceT1);
        warning("totalDistanceT2 = %10.10e", totalDistanceT2);
      }

      //totalDistanceT1 += cc->c[1];
      //totalDistanceT2 += cc->c[2];

      //totalDistanceT1 /= (T)2.0;
      //totalDistanceT2 /= (T)2.0;

      //totalDistanceT1 = totalDistanceT2 = (T)0.0;

      //totalDistanceT1 = cc->getConstraintValue(1);
      //totalDistanceT2 = cc->getConstraintValue(2);

      /*Update constraint with new normals and tangential vectors and
        updated distances.*/

      if(bodyEdge1){
        if(rigidEdge1){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-contactNormal*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(           -t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(           -t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(-cross(re1, contactNormal)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(           -cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(           -cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-contactNormal*baryi1[0]*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(           -t1*baryi1[0]*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(           -t2*baryi1[0]*dt, totalDistanceT2, 2, 0, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(-contactNormal*baryi1[1]*dt, totalDistance,   0, 1, velIndex);
          cc->setRow(           -t1*baryi1[1]*dt, totalDistanceT1, 1, 1, velIndex);
          cc->setRow(           -t2*baryi1[1]*dt, totalDistanceT2, 2, 1, velIndex);
        }
      }
      if(bodyEdge2){
        if(rigidEdge2){
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(contactNormal*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(           t1*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(           t2*dt, totalDistanceT2, 2, 2, velIndex);

          cc->setRow(cross(re2, contactNormal)*dt, totalDistance,   0, 3, velIndex+1);
          cc->setRow(           cross(re2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
          cc->setRow(           cross(re2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(contactNormal*baryi2[0]*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(           t1*baryi2[0]*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(           t2*baryi2[0]*dt, totalDistanceT2, 2, 2, velIndex);

          velIndex = ctx->mesh->vertices[indices[3]].vel_index/3;

          cc->setRow(contactNormal*baryi2[1]*dt, totalDistance,   0, 3, velIndex);
          cc->setRow(           t1*baryi2[1]*dt, totalDistanceT1, 1, 3, velIndex);
          cc->setRow(           t2*baryi2[1]*dt, totalDistanceT2, 2, 3, velIndex);
        }
      }
    }else{
      contactNormal =
        /*ei.getOutwardNormal(pi, &referencedvEP, 0, 0,
          &weights2, &weights1);*/
        ei.getOutwardNormal(pi, &referencedvEP);

      warning("contact normal not changed");

      bool parallel = false;
      ei.infiniteProjections(pi, 0, 0, &weights2, &weights1, &parallel);

      warning("check order of weights");

      if(parallel){
        throw new DegenerateCaseException(__LINE__, __FILE__, "parallel edges after update");
      }

      warning("weights of point");
      std::cerr << weights1 << std::endl;
      std::cerr << weights2 << std::endl;

      //bool offEdge = false;
      //T offEdgeEps = (T)1e-8;

#if 1
      if((!pi.barycentricOnEdgeDist(weights1, (T)DISTANCE_EPSILON) ||
          !ei.barycentricOnEdgeDist(weights2, (T)DISTANCE_EPSILON))){
        message("contact off edge");
        std::cout << weights1 << std::endl;
        std::cout << weights2 << std::endl;
      }
      /*Check if new edges project on each other*/
      if((!pi.barycentricOnEdgeDist(weights1, (T)DISTANCE_EPS) ||
          !ei.barycentricOnEdgeDist(weights2, (T)DISTANCE_EPS))
         &&
         distancec > DISTANCE_EPS
         ){
        /*Could be a source of potential errors!!*/

        *skipped = true;
        //updateInactiveConstraint(c, ctx, x, p, com, rot);
        //return;

        //offEdge = true;
        //if(distancec > offEdgeEps){
          //*skipped = true;
        message("don't care");
#if 1
        disableConstraint(c, ctx, true);
        e0 = eu;
        rote0 = roteu;
        come0 = comeu;

        e0 = eu;
        rote0 = roteu;
        come0 = comeu;

        p0 = p;
        rotp0 = rot;
        comp0 = com;

        distance0 = distancec;
        intersection0 = intersectionc;

        //updateInitialState(p, com, rot);

        c->updateStartPositions(p, com, rot, this);
#endif
        return;
        //}

      }
#endif
      warning("Edges1 = %10.10e", (eu.vertex(0) - eu.vertex(1)).length());
      warning("Edges2 = %10.10e", (p.vertex(0)  -  p.vertex(1)).length());
      warning("Normal error = %10.10e", dot(contactNormal, contactNormalOld));


      warning("bary");
      std::cerr << baryi1 << std::endl;
      std::cerr << baryi2 << std::endl;

      /*The normal has not changed significantly, just update the
        constraint normal distance*/

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

      /*Error wrt desired distance*/
      T errorDist = (T)(DISTANCE_EPS+EPS) - distancec;

      T totalDistance = cc->getConstraintValue(0) - (T)(errorDist*0.45);

      if(distancec > (DISTANCE_EPSILON - DISTANCE_TOL) && distancec < DISTANCE_TOL + (T)DISTANCE_EPSILON){
        //return false;
        *skipped = true;
        message("in range");
        return;
        totalDistance = cc->getConstraintValue(0) - (T)(errorDist*0.05);
      }

      //errorDist = Sign(errorDist)*Max(Abs(errorDist*(T)0.45), (T)DISTANCE_EPS + (T)EPS);
      //errorDist = Sign(errorDist)*Max(Abs(errorDist), (T)DISTANCE_EPS + (T)EPS);

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-4);


      warning(" distancec = %10.10e", distancec);
      warning(" errorDist = %10.10e", errorDist);
      warning(" totalDist = %10.10e", totalDistance);

      if(cc->status != Active){
        /*Do not update inactive constraint this way*/
        // return;
      }

      cc->getConstraintValue(0) = totalDistance;
    }

    if(debug){
      getchar();
    }

    /*Update constraint*/
    cc->init(ctx->getMatrix());
    cc->update(*ctx->getSolver()->getb(),
               *ctx->getSolver()->getx(),
               *ctx->getSolver()->getb2());

    stats.n_geometry_check++;

    //cc->status = Active;
  }

  template<class T>
  void ConstraintCandidateEdge<T>::updateInitial(ConstraintSetEdge<T>*c,
                                                 CollisionContext<T>* ctx,
                                                 const Edge<T>& x,
                                                 const Edge<T>& p,
                                                 const Vector4<T>& com,
                                                 const Quaternion<T>& rot,
                                                 bool* skipped,
                                                 EvalStats& stats){
    Vector4<T> weights1, weights2;
    bool parallel = false;
    return;
    e0 = eu;
    rote0 = roteu;
    come0 = comeu;

    p0 = p;
    rotp0 = rot;
    comp0 = com;

    distance0 = distancec;
    intersection0 = intersectionc;

    ei.infiniteProjections(pi, 0, 0, &weights2, &weights1, &parallel);

    if(status == Enabled){
      /*Check if new edges project on each other*/
      if((!pi.barycentricOnEdgeDist(weights1, (T)DISTANCE_EPSILON) ||
          !ei.barycentricOnEdgeDist(weights2, (T)DISTANCE_EPSILON))
         &&
         distancec > 5*DISTANCE_EPSILON+DISTANCE_TOL
         ){

        if(       (edgeId == 144166 && otherEdgeId == 56468) ||
                  (edgeId == 56468  && otherEdgeId == 144166) ||
                  (edgeId == 144164 && otherEdgeId == 56468) ||
                  (edgeId == 56468  && otherEdgeId == 144164)){
          message("update initial disable %d - %d", edgeId, otherEdgeId);
          message("distancec = %10.10e", distancec);
          std::cout << weights1 << std::endl;
          std::cout << weights2 << std::endl;
          getchar();
        }

        disableConstraint(c, ctx, true);
        *skipped = true;
      }
    }
  }


  template<class T>
  bool ConstraintCandidateEdge<T>::enableConstraint(ConstraintSetEdge<T>* c,
                                                    CollisionContext<T>* ctx,
                                                    Edge<T>& s,
                                                    Edge<T>& p){
    if(status == Enabled){
      return false;
    }else{
      bool fault = false;

      enabledPU = p;
      enabledEU = eu;
      enabledP0 = p0;
      enabledE0 = e0;

      referencedvEP0 = referencedvEP;

      ContactConstraint<2, T>* cc = new ContactConstraint<2, T>();
      cc->sourceType = 1;
      cc->sourceA = edgeId;
      cc->sourceB = c->edgeId;

      if(backFace){
        cc->sourceType = 4;
      }

      try{
        /*Compute barycentric coordinates on both edges*/
#ifdef WEIGHT_CLAMPING
        Vector4<T> weights1 =  baryi1;
        Vector4<T> weights2 =  baryi2;
        //Vector4<T> weights1 = p0.clampWeights(baryi1, WEIGHT_MIN, WEIGHT_MAX);
        //Vector4<T> weights2 = e0.clampWeights(baryi2, WEIGHT_MIN, WEIGHT_MAX);
#else
        Vector4<T> weights1 =  baryi1;
        Vector4<T> weights2 =  baryi2;
#endif
        cc->setMu((T)MU);

        if(backFace){
          cc->setMu((T)MU*(T)0.0);
        }

        int indices[4];
        ctx->mesh->getEdgeIndices(c->edgeId, indices    );
        ctx->mesh->getEdgeIndices(   edgeId, indices + 2);

        T dt = ctx->getDT();

        bool bodyEdge1  = true; //=edge-set edge
        bool bodyEdge2  = true; //=this edge
        bool rigidEdge1 = false;//=edge-set edge
        bool rigidEdge2 = false;//=this edge

        for(int i=0;i<2;i++){
          if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
            bodyEdge1 = false;
          }

          if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
            rigidEdge1 = true;
          }
        }

        for(int i=2;i<4;i++){
          if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
            bodyEdge2 = false;
          }

          if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
            rigidEdge2 = true;
          }
        }

        T d5 = ei.getPlaneDistance(p0.vertex(0), dv)*weights1[0];
        T d6 = ei.getPlaneDistance(p0.vertex(1), dv)*weights1[1];
        T d7 = ei.getPlaneDistance(e0.vertex(0), dv)*weights2[0];
        T d8 = ei.getPlaneDistance(e0.vertex(1), dv)*weights2[1];

        T totalDistance = (d5+d6-d7-d8) - (T)DISTANCE_EPS - (T)EPS;

        if(Abs(totalDistance) > 1E-2){
          /*When a constraint is enabled and its corresponding
            distance is too large, there can be a problem.*/
          warning("dump edges");
          std::cerr << s << std::endl;
          std::cerr << p << std::endl;
          std::cerr << e0 << std::endl;
          std::cerr << eu << std::endl;
          std::cerr << pi << std::endl;
          std::cerr << ei << std::endl;

          //warning("d0 = %10.10e", s.getSignedDistance(e0, 0, 0, 0, 0, &referencedvPE));
          //warning("dc = %10.10e", p.getSignedDistance(eu, 0, 0, 0, 0, &referencedvPE));

          PRINT(s);
          PRINT(p);
          PRINT(e0);
          PRINT(eu);
          std::cerr << weights1 << std::endl;
          std::cerr << weights2 << std::endl;
          std::cerr << dv << std::endl;
          std::cerr << referencedvEP << std::endl;
          std::cerr << ei.getOutwardNormal(pi, &referencedvEP) << std::endl;
          std::cerr << ei.getOutwardNormal(pi, &referencedvEP) << std::endl;

          warning("too large distance");


          warning("total distance = %10.10e", totalDistance);


          fault = true;
        }

        Vector4<T> t1, t2;
        Vector4<T> re1, re2;

        contactNormal = ei.getOutwardNormal(pi, &referencedvEP);

#ifdef FRICTION_CONE
        if(tangentialDefined){
          t1 = -tangentReference[0];
          //t2 = -tangentReference[1];

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, dv).normalize();
          //if(tt2 * t2 < 0){
          //tt2 *= -1;
          //}

          tt1 = cross(tt2, dv).normalize();
          //if(tt1 * t1 < 0){
          //tt1 *= -1;
          //}

          t1 = tt1;
          t2 = tt2;

          if(bodyEdge1){
            if(rigidEdge1){
              re1  = pi.getCoordinates(baryi1) - compi;
            }
          }

          if(bodyEdge2){
            if(rigidEdge2){
              re2  = ei.getCoordinates(baryi2) - comei;
            }
          }
          cc->tangent1 = t1;
          cc->tangent2 = t2;
          cc->normal   = dv;
        }else{
          Vector4<T> rnd(genRand<T>(), genRand<T>(), genRand<T>(), 0);

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;

          tt2 = cross(rnd, dv).normalize();

          tt1 = cross(tt2, dv).normalize();

          t1 = tt1;
          t2 = tt2;

          if(bodyEdge1){
            if(rigidEdge1){
              re1  = pi.getCoordinates(baryi1) - compi;
            }
          }

          if(bodyEdge2){
            if(rigidEdge2){
              re2  = ei.getCoordinates(baryi2) - comei;
            }
          }

          cc->tangent1 = t1;
          cc->tangent2 = t2;
          cc->normal   = dv;
        }
#else
#ifdef ACTIVE_CONSTRAINTS
        /*In case of ICA, all constraints are removed and newly
          added. Using old tangent vectors wihout re-aligning them is
          not correct.*/
        //tangentialDefined = false;
#endif
        if(tangentialDefined){
          t1 = -tangentReference[0];
          t2 = -tangentReference[1];

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, dv).normalize();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(t2, dv).normalize();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }

          t1 = tt1;
          t2 = tt2;

          if(rigidEdge1){
            re1  = pi.getCoordinates(baryi1) - compi;
          }

          if(rigidEdge2){
            re2  = ei.getCoordinates(baryi2) - comei;
          }
        }else{
          Vector4<T> vel(0,0,0,0);
          Vector<T>* vx = ctx->getSolver()->getx();

          if(bodyEdge1){
            if(rigidEdge1){
              int vel_index = ctx->mesh->vertices[indices[0]].vel_index;
              cgfassert(vel_index != -1);

              vel[0] -= (*vx)[vel_index+0];
              vel[1] -= (*vx)[vel_index+1];
              vel[2] -= (*vx)[vel_index+2];

              Vector4<T> ang_vel;

              ang_vel[0] = (*vx)[vel_index+3];
              ang_vel[1] = (*vx)[vel_index+4];
              ang_vel[2] = (*vx)[vel_index+5];

              /*Compute angular displacement*/
              re1  = pi.getCoordinates(baryi1) - compi;
              ang_vel = cross(ang_vel, re1);

              vel -= ang_vel;
            }else{
              for(int l=0;l<2;l++){
                int vel_index = ctx->mesh->vertices[indices[l+0]].vel_index;

                cgfassert(vel_index != -1);

                vel[0] -= (*vx)[vel_index+0] * weights1[l];
                vel[1] -= (*vx)[vel_index+1] * weights1[l];
                vel[2] -= (*vx)[vel_index+2] * weights1[l];
              }
            }
          }

          if(bodyEdge2){
            if(rigidEdge2){
              int vel_index = ctx->mesh->vertices[indices[2]].vel_index;

              cgfassert(vel_index != -1);

              vel[0] += (*vx)[vel_index+0];
              vel[1] += (*vx)[vel_index+1];
              vel[2] += (*vx)[vel_index+2];

              Vector4<T> ang_vel;

              ang_vel[0] = (*vx)[vel_index+3];
              ang_vel[1] = (*vx)[vel_index+4];
              ang_vel[2] = (*vx)[vel_index+5];

              /*Compute angular displacement*/

              re2  = ei.getCoordinates(baryi2) - comei;
              ang_vel = cross(ang_vel, re2);

              vel += ang_vel;
            }else{
              for(int l=0;l<2;l++){
                int vel_index = ctx->mesh->vertices[indices[l+2]].vel_index;

                cgfassert(vel_index != -1);

                vel[0] += (*vx)[vel_index+0] * weights2[l];
                vel[1] += (*vx)[vel_index+1] * weights2[l];
                vel[2] += (*vx)[vel_index+2] * weights2[l];
              }
            }
          }

          Vector4<T> vn = cross(vel, dv);

          /*Use velocity to align tangent vectors*/
          if(vn.length() < 1e-6){
            /*Choose some arbitrary direction*/
            Vector4<T> vec(1, 1, 1, 0);
            Vector4<T> vn2 = cross(vec, dv);

            if(vn2.length() < 1e-6){
              vec.set(-1, 1, 1, 0);
              vn2 = cross(vec, dv);
            }

            t2 = vn2.normalize();
            t1 = cross(t2, dv).normalize();
          }else{
            t2 = vn.normalize();
            t1 = cross(t2, dv).normalize();
          }
        }
#endif

        d5 = dot(t1, pi.vertex(0) - p0.vertex(0)) * weights1[0];
        d6 = dot(t1, pi.vertex(1) - p0.vertex(1)) * weights1[1];
        d7 = dot(t1, ei.vertex(0) - e0.vertex(0)) * weights2[0];
        d8 = dot(t1, ei.vertex(1) - e0.vertex(1)) * weights2[1];

        PRINT(d5);
        PRINT(d6);
        PRINT(d7);
        PRINT(d8);

        T totalDistanceT1 = -(d5+d6-d7-d8);

        PRINT(totalDistanceT1);

        d5 = dot(t2, pi.vertex(0) - p0.vertex(0)) * weights1[0];
        d6 = dot(t2, pi.vertex(1) - p0.vertex(1)) * weights1[1];
        d7 = dot(t2, ei.vertex(0) - e0.vertex(0)) * weights2[0];
        d8 = dot(t2, ei.vertex(1) - e0.vertex(1)) * weights2[1];

        PRINT(d5);
        PRINT(d6);
        PRINT(d7);
        PRINT(d8);

        T totalDistanceT2 = -(d5+d6-d7-d8);

        //totalDistanceT1 = totalDistanceT2 = 0;

        PRINT(totalDistanceT2);

#ifdef FRICTION_CONE
        //cc->kineticVector.set(0, -totalDistanceT1, -totalDistanceT2, 0);
        //cc->kineticVector.normalize();
#endif

        //totalDistance = totalDistanceT1 = totalDistanceT2 = (T)0.0;
        //totalDistanceT1 = totalDistanceT2 = (T)0.0;

        if(fault){
          warning("tangent vectors");
          std::cerr << t1 << std::endl;
          std::cerr << t2 << std::endl;
          std::cerr << re1 << std::endl;
          std::cerr << re2 << std::endl;

          warning("td1 = %10.10e", totalDistanceT1);
          warning("td2 = %10.10e", totalDistanceT2);
        }

        //totalDistance = totalDistanceT1 = totalDistanceT2 = (T)0.0;

        //totalDistance *= (T)2.0;

        if(bodyEdge1){
          if(rigidEdge1){
            int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(-dv*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

            cc->setRow(-cross(re1, dv)*dt, totalDistance,   0, 1, velIndex+1);
            cc->setRow(-cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
            cc->setRow(-cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
          }else{
            int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(-dv*weights1[0]*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(-t1*weights1[0]*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(-t2*weights1[0]*dt, totalDistanceT2, 2, 0, velIndex);

            velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

            cc->setRow(-dv*weights1[1]*dt, totalDistance,   0, 1, velIndex);
            cc->setRow(-t1*weights1[1]*dt, totalDistanceT1, 1, 1, velIndex);
            cc->setRow(-t2*weights1[1]*dt, totalDistanceT2, 2, 1, velIndex);
          }
        }
        if(bodyEdge2){
          if(rigidEdge2){
            int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(dv*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(t1*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(t2*dt, totalDistanceT2, 2, 2, velIndex);

            cc->setRow(cross(re2, dv)*dt, totalDistance,   0, 3, velIndex+1);
            cc->setRow(cross(re2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
            cc->setRow(cross(re2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
          }else{
            int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(dv*weights2[0]*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(t1*weights2[0]*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(t2*weights2[0]*dt, totalDistanceT2, 2, 2, velIndex);

            velIndex = ctx->mesh->vertices[indices[3]].vel_index/3;

            cc->setRow(dv*weights2[1]*dt, totalDistance,   0, 3, velIndex);
            cc->setRow(t1*weights2[1]*dt, totalDistanceT1, 1, 3, velIndex);
            cc->setRow(t2*weights2[1]*dt, totalDistanceT2, 2, 3, velIndex);
          }
        }
      }catch(DegenerateCaseException* e){
        std::cout << e->getError();
        delete cc;
        return false;
      }

      cc->init(ctx->getMatrix());

      cc->status = Active;
      //cc->frictionStatus[0] = Static;
      //cc->frictionStatus[1] = Static;

      enabledId = ctx->getSolver()->addConstraint(cc);
      cc->id = enabledId;

      if(fault){
        cc->print(std::cerr);
      }

#ifdef ACTIVE_CONSTRAINTS
      cc->resetMultipliers(*ctx->getSolver()->getx(), cachedMultipliers);
#else
      Vector4<T> zero;
      cc->resetMultipliers(*ctx->getSolver()->getx(), zero);
#endif
      status = Enabled;
    }
    return true;
  }

  template<class T>
  void ConstraintCandidateEdge<T>::recompute(ConstraintSetEdge<T>* c,
                                             CollisionContext<T>* ctx,
                                             Edge<T>& p,
                                             Vector4<T>& com,
                                             Quaternion<T>& rot){
    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

    ctx->mesh->getEdge(&e0, edgeId, backFace);
    ei = e0;
    eu = e0;

#ifndef DELASSUS_TEST
    if(collapsed){
      error("edge in collapsed state %d - %d", edgeId, otherEdgeId);
    }
#endif

    come0 = comeu = comei = Vector4<T>(0,0,0,0);
    rote0 = roteu = rotei = Quaternion<T>(0,0,0,1);

    ctx->mesh->setCurrentEdge(edgeId);
    int vertex = ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(ctx->mesh->vertices[vertex].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertex];

      come0 = comeu = comei = ctx->mesh->centerOfMass[objectId];
      rote0 = roteu = rotei = ctx->mesh->orientations[objectId];
    }

    p0 = p;
    pi = p;

    comp0 = compi = com;
    rotp0 = rotpi = rot;

    intersection0 = intersectionc =
      e0.edgesIntersect(p0, (T)SAFETY_DISTANCE, rigidEdge1 || rigidEdge2);

    if(intersection0 == false && status != Enabled){
      /*If there is no intersection and there is no enabled
        constraint, then the actual reference is not of interest. When
        there is no intersection0, a new intersection will reset the
        reference correctly.  If there is a constraint enabled, then
        there should be no intersection, so in that case we need to
        compute the new reference using the old one.*/
      bool potentialError = false;
      referencedvEP = e0.getOutwardNormal(p0, 0, 0, &potentialError);
      referencedvPE = -referencedvEP;

      referencedvEP0 = referencedvEP;

      if(potentialError){
        message("get outward normal took too long, %d - %d", edgeId, otherEdgeId);
      }
      if(IsNan(referencedvEP)){
        error("dv = NAN");
      }
    }

    contactNormal = referencedvEP;

    /*Try to invalidate combination p-e as soon as possible*/
    if(status != Enabled){
      Vector4<T> ldv;

      if(e0.isIndeterminate() || p0.isIndeterminate()){
        //return;
      }
    }else{
      referencedvEP = e0.getOutwardNormal(p0, &referencedvEP);
      referencedvPE = -referencedvEP;

      referencedvEP0 = referencedvEP;

      if(IsNan(referencedvEP)){
        error("dv = NAN");
      }
    }

    Vector4<T> dv1 = referencedvEP;



    //bool degenerate = false;

#if 0
    /*We have performed a validity check before*/
    if(degenerate){

      if(status == Enabled){
        disableConstraint(c, ctx, true);
      }
      return;
    }
#endif

    if(e0.isIndeterminate() || p0.isIndeterminate()){
      if(status == Enabled){
        //disableConstraint(c, ctx, true);
      }
      //return;
    }

    cgfassert(Abs(dv1.length() -1.0) < 1E-5);

    if(status == Enabled){
      /*Distance here IS positive by definition*/
      bool degenerate = false;

      referencedvEP = e0.getOutwardNormal(p0, &referencedvEP);
      referencedvPE = -referencedvEP;
      dv1 = referencedvEP;

      if(IsNan(referencedvEP)){
        error("dv = NAN");
      }


      distance0 = distancec =
        e0.getSignedDistance(p0, &colPoint, 0,
                             &baryi2, &baryi1, &degenerate, &referencedvEP);// - (T)DISTANCE_EPS;

      if(degenerate){
        disableConstraint(c, ctx, true);
        return;
      }

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);

      //cc->averageFrictionMagnitude.clear();
      //cc->averageFrictionDirection.clear();

      if(cc->status == Inactive){
        if(distancec > DISTANCE_EPSILON+DISTANCE_TOL){
          /*Constraint is inactive and there is no collision -> disable.*/
          disableConstraint(c, ctx, true);
          return;
        }else{
          /*Constraint was disabled, but there is a collision -> reenable*/
          //cc->status = Active;
        }
      }

      if(!p0.barycentricOnEdgeDist(baryi1, (T)DISTANCE_EPS) ||
         !e0.barycentricOnEdgeDist(baryi2, (T)DISTANCE_EPS)){
        DBG(message("disable non-projecting edge constraint"));
        DBG(cc->print(std::cout));
        disableConstraint(c, ctx, true);
        warning("check barycentric coordinates differently");
        return;
      }


#ifdef WEIGHT_CLAMPING
      Vector4<T> weights1 =  baryi1;
      Vector4<T> weights2 =  baryi2;
      //Vector4<T> weights1 = p0.clampWeights(baryi1, WEIGHT_MIN, WEIGHT_MAX);
      //Vector4<T> weights2 = e0.clampWeights(baryi2, WEIGHT_MIN, WEIGHT_MAX);
#else
      Vector4<T> weights1 =  baryi1;
      Vector4<T> weights2 =  baryi2;
#endif

      dv = dv1;

      cc->setMu((T)MU);

      if(backFace){
        cc->setMu((T)MU*(T)0.0);
      }

#ifdef FRICTION_CONE
      cc->cumulativeVector = cc->kineticVector * (T)60;
      //cc->cumulativeVector /= (T)1.1;

      if(cc->cumulativeVector.length() > 1e-6){
        cc->cumulativeVector.normalize();
      }

      if(cc->frictionStatus[0] != Kinetic){
        cc->cumulativeVector.set(0,0,0,0);
        cc->kineticVector.set(0,0,0,0);
        cc->acceptedKineticVector.set(0,0,0,0);
      }

#endif

      int indices[4];
      ctx->mesh->getEdgeIndices(c->edgeId, indices  );
      ctx->mesh->getEdgeIndices(   edgeId, indices+2);

      T dt = ctx->getDT();

      bool bodyEdge1 = true;
      bool bodyEdge2 = true;
      bool rigidEdge1 = false;
      bool rigidEdge2 = false;

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyEdge1 = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge1 = true;
        }
      }

      for(int i=2;i<4;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyEdge2 = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge2 = true;
        }
      }

      T d1 =  ei.getPlaneDistance(p0.vertex(0), dv1) * weights1[0];
      T d2 =  ei.getPlaneDistance(p0.vertex(1), dv1) * weights1[1];
      T d3 =  ei.getPlaneDistance(e0.vertex(0), dv1) * weights2[0];
      T d4 =  ei.getPlaneDistance(e0.vertex(1), dv1) * weights2[1];


      T totalDistance = (d1 + d2 - d3 - d4) -
        (T)DISTANCE_EPS - (T)EPS;

      if(Abs(totalDistance) > 1E-2){
        warning("dump edges");
        std::cerr << p0 << std::endl;
        std::cerr << e0 << std::endl;

        bool degenerate = false;
        warning("d0 = %10.10e", p0.getSignedDistance(e0, 0, 0, 0, 0,
                                                     &degenerate, &referencedvPE));
        warning("degenerate = %d", degenerate);

        //PRINT(s);
        PRINT(p);
        PRINT(e0);
        PRINT(eu);
        warning("too large distance");
      }

      Vector4<T> t1, t2;
      Vector4<T> re1, re2;

#ifdef FRICTION_CONE
      if(bodyEdge1){
        if(rigidEdge1){
          t1 += cc->getRow(1,0);
          t2 += cc->getRow(2,0);
          re1  = pi.getCoordinates(baryi1) - compi;
        }else{
          t1 += cc->getRow(1,0);
          t1 += cc->getRow(1,1);
          t2 += cc->getRow(2,0);
          t2 += cc->getRow(2,1);
        }
      }

      if(bodyEdge2){
        if(rigidEdge2){
          t1 -= cc->getRow(1,2);
          t2 -= cc->getRow(2,2);
          re2  = ei.getCoordinates(baryi2) - comei;
        }else{
          t1 -= cc->getRow(1,2);
          t1 -= cc->getRow(1,3);
          t2 -= cc->getRow(2,2);
          t2 -= cc->getRow(2,3);
        }
      }

      t1 *= -1;
      t2 *= -1;

      t1.normalize();
      t2.normalize();

      Vector4<T> tt1, tt2;
      tt2 = cross(t1, dv).normalize();
      tt1 = cross(tt2, dv).normalize();

      t1 = -tt1;
      t2 = -tt2;

      cc->tangent1 = t1;
      cc->tangent2 = t2;
      cc->normal   = dv;


#else
      Vector4<T> vel(0,0,0,0);
      Vector<T>* vx = ctx->getSolver()->getx();

      if(bodyEdge1){
        if(rigidEdge1){
          int vel_index = ctx->mesh->vertices[indices[0]].vel_index;

          vel[0] -= (*vx)[vel_index+0];
          vel[1] -= (*vx)[vel_index+1];
          vel[2] -= (*vx)[vel_index+2];

          Vector4<T> ang_vel;

          ang_vel[0] = (*vx)[vel_index+3];
          ang_vel[1] = (*vx)[vel_index+4];
          ang_vel[2] = (*vx)[vel_index+5];

          /*Compute angular displacement*/
          re1  = pi.getCoordinates(baryi1) - compi;
          ang_vel = cross(ang_vel, re1);

          vel -= ang_vel;
        }else{
          for(int l=0;l<2;l++){
            int vel_index = ctx->mesh->vertices[indices[l+0]].vel_index;

            vel[0] -= (*vx)[vel_index+0] * weights1[l];
            vel[1] -= (*vx)[vel_index+1] * weights1[l];
            vel[2] -= (*vx)[vel_index+2] * weights1[l];
          }
        }
      }

      if(bodyEdge2){
        if(rigidEdge2){
          int vel_index = ctx->mesh->vertices[indices[2]].vel_index;

          vel[0] += (*vx)[vel_index+0];
          vel[1] += (*vx)[vel_index+1];
          vel[2] += (*vx)[vel_index+2];

          Vector4<T> ang_vel;

          ang_vel[0] = (*vx)[vel_index+3];
          ang_vel[1] = (*vx)[vel_index+4];
          ang_vel[2] = (*vx)[vel_index+5];

          /*Compute angular displacement*/
          re2  = ei.getCoordinates(baryi2) - comei;
          ang_vel = cross(ang_vel, re2);

          vel += ang_vel;
        }else{
          for(int l=0;l<2;l++){
            int vel_index = ctx->mesh->vertices[indices[l+2]].vel_index;
            vel[0] += (*vx)[vel_index+0] * weights2[l];
            vel[1] += (*vx)[vel_index+1] * weights2[l];
            vel[2] += (*vx)[vel_index+2] * weights2[l];
          }
        }
      }

      ////////////////
      t1 = t2.set(0,0,0,0);

      if(rigidEdge1){
        t1 += cc->getRow(1,0);
        t2 += cc->getRow(2,0);
      }else{
        t1 += cc->getRow(1,0);
        t1 += cc->getRow(1,1);
        t2 += cc->getRow(2,0);
        t2 += cc->getRow(2,1);
      }

      if(rigidEdge2){
        t1 -= cc->getRow(1,2);
        t2 -= cc->getRow(2,2);
      }else{
        t1 -= cc->getRow(1,2);
        t1 -= cc->getRow(1,3);
        t2 -= cc->getRow(2,2);
        t2 -= cc->getRow(2,3);
      }

      /*
      t1 = -((cc->getRow(1, 0) + cc->getRow(1, 1)) -
             (cc->getRow(1, 2) + cc->getRow(1, 3)));
      t2 = -((cc->getRow(2, 0) + cc->getRow(2, 1)) -
             (cc->getRow(2, 2) + cc->getRow(2, 3)));
      */
      t1 *= -1;
      t2 *= -1;

      t1.normalize();
      t2.normalize();

      Vector4<T> vn = cross(vel, dv);

      START_DEBUG;
      message("edge: t1, t2");
      std::cout << t1;
      std::cout << t2;

      std::cout << vel;

      std::cout << vn;
      END_DEBUG;

      /*Re-align with velocity and/or normal*/
      if(vn.length() < 1e-6){
        DBG(message("realign without velocity"));
        /*Fixed point, only align with normal*/
        Vector4<T> tt1, tt2;
        tt2 = cross(t1, dv).normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
          DBG(message("flip tt2"));
        }

        tt1 = cross(t2, dv).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
          DBG(message("flip tt1"));
        }

        t1 = tt1;
        t2 = tt2;
      }else{
        DBG(message("realign with velocity"));
        Vector4<T> tt1, tt2;
        tt2 = vn.normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
          DBG(message("flip tt2"));
        }

        tt1 = cross(tt2, dv).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
          DBG(message("flip tt1"));
        }
        t1 = tt1;
        t2 = tt2;
      }
#endif

#ifndef FRICTION_CONE
      START_DEBUG;
      message("edge: after realign, %10.10e, %10.10e", dot(t1, vn), dot(t2,vn));
      std::cout << t1;
      std::cout << t2;
      END_DEBUG;
#endif

      if(bodyEdge1){
        if(rigidEdge1){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-dv*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(-t1*dt,             0, 1, 0, velIndex);
          cc->setRow(-t2*dt,             0, 2, 0, velIndex);

          cc->setRow(-cross(re1, dv)*dt, totalDistance, 0, 1, velIndex+1);
          cc->setRow(-cross(re1, t1)*dt,             0, 1, 1, velIndex+1);
          cc->setRow(-cross(re1, t2)*dt,             0, 2, 1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-dv*weights1[0]*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(-t1*weights1[0]*dt,             0, 1, 0, velIndex);
          cc->setRow(-t2*weights1[0]*dt,             0, 2, 0, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(-dv*weights1[1]*dt, totalDistance, 0, 1, velIndex);
          cc->setRow(-t1*weights1[1]*dt,             0, 1, 1, velIndex);
          cc->setRow(-t2*weights1[1]*dt,             0, 2, 1, velIndex);
        }
      }
      if(bodyEdge2){
        if(rigidEdge2){
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(dv*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(t1*dt,             0, 1, 2, velIndex);
          cc->setRow(t2*dt,             0, 2, 2, velIndex);

          cc->setRow(cross(re2, dv)*dt, totalDistance, 0, 3, velIndex+1);
          cc->setRow(cross(re2, t1)*dt,             0, 1, 3, velIndex+1);
          cc->setRow(cross(re2, t2)*dt,             0, 2, 3, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(dv*weights2[0]*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(t1*weights2[0]*dt,             0, 1, 2, velIndex);
          cc->setRow(t2*weights2[0]*dt,             0, 2, 2, velIndex);

          velIndex = ctx->mesh->vertices[indices[3]].vel_index/3;

          cc->setRow(dv*weights2[1]*dt, totalDistance, 0, 3, velIndex);
          cc->setRow(t1*weights2[1]*dt,             0, 1, 3, velIndex);
          cc->setRow(t2*weights2[1]*dt,             0, 2, 3, velIndex);
        }
      }

      cc->init(ctx->getMatrix());


      cc->update(*ctx->getSolver()->getb(),
                 *ctx->getSolver()->getx(),
                 *ctx->getSolver()->getb2());


    }else{
      bool degenerate = false;
      distance0 = distancec =
        e0.getSignedDistance(p0, &colPoint, 0,
                             &baryi2, &baryi1,
                             &degenerate, &referencedvEP);// - (T)DISTANCE_EPS;
    }
  }

  template<class T>
  void ConstraintCandidateEdge<T>::showCandidateState(Edge<T>& pu,
                                                      CollisionContext<T>* ctx){
    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

    message("\n\nShow candidate state %d - %d", edgeId, otherEdgeId);

    message("p0 e0");
    std::cout << p0 << std::endl;
    std::cout << e0 << std::endl;

    message("pu eu");
    std::cout << pu << std::endl;
    std::cout << eu << std::endl;

    message("pi ei");
    std::cout << pi << std::endl;
    std::cout << ei << std::endl;

    message("distance0 = %10.10e", distance0);
    message("distancec = %10.10e", distancec);

    intersection0 =
      e0.edgesIntersect(p0, (T)SAFETY_DISTANCE,
                        rigidEdge1 || rigidEdge2, true);

    message("intersection0 = %d", intersection0);

    intersectionc =
      eu.edgesIntersect(pu, (T)SAFETY_DISTANCE,
                        rigidEdge1 || rigidEdge2, true);

    message("intersectionc = %d", intersectionc);

    message("collapsed = %d", collapsed);

    message("referencedvEP");
    std::cout << referencedvEP << std::endl;
    message("referencedvPE");
    std::cout << referencedvPE << std::endl;

    message("recompute distances");

    message("d0 = %10.10e", e0.getSignedDistance(p0, 0,0,0,0,0, &referencedvEP));
    message("dc = %10.10e", eu.getSignedDistance(pu, 0,0,0,0,0, &referencedvEP));
    message("di = %10.10e", ei.getSignedDistance(pi, 0,0,0,0,0, &referencedvEP));

    message("barycentric weights");
    std::cout << baryi1 << std::endl;
    std::cout << baryi2 << std::endl;

    message("enabledId = %d", enabledId);

    if(status == Enabled){
      message("Constraint enabled");
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);
      cc->print(std::cout);
    }

    int indices[4];
    ctx->mesh->getEdgeIndices(otherEdgeId, indices    );
    ctx->mesh->getEdgeIndices(     edgeId, indices + 2);

    message("indices p: %d, %d", indices[0], indices[1]);
    message("indices e: %d, %d", indices[2], indices[3]);
  }

  template<class T>
  void ConstraintCandidateEdge<T>::updateInitialState(const Edge<T>& edge,
                                                      const Vector4<T>& com,
                                                      const Quaternion<T>& rot){


    bool rigidEdge1 = Mesh::isRigidOrStatic(ownerEdgeType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(thisEdgeType);

    if(

       (edgeId == 25100 && otherEdgeId == 27087) ||
       (edgeId == 27087 && otherEdgeId == 25100)

       ||

       (edgeId == 25100 && otherEdgeId == 19644) ||
       (edgeId == 19644 && otherEdgeId == 25100)

       ){

      message("update initial state %d - %d", edgeId, otherEdgeId);

    }

    p0 = edge;

    comp0 = com;
    rotp0 = rot;

    message("intersection was %d", intersection0);

    intersection0 =
      e0.edgesIntersect(p0, (T)SAFETY_DISTANCE, rigidEdge1 || rigidEdge2);

    message("intersection becomes %d", intersection0);

    contactNormal = referencedvEP;
  }

  template class ConstraintCandidateEdge<float>;
  template class ConstraintCandidateEdge<double>;
}
