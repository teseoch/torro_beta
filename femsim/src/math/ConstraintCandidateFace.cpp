/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintCandidateFace.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"
#include "math/ConstraintSet.hpp"
#include "core/Exception.hpp"

//#define MU 0.5
#define EPS DISTANCE_EPSILON
#define DISTANCE_EPS SAFETY_DISTANCE
/*EPS = additional gap*/
#define TOTAL_EPS (EPS + DISTANCE_EPS)

#define DEBUG_UPDATE
#undef DEBUG_UPDATE

namespace CGF{

  template<class T>
  ConstraintCandidateFace<T>::ConstraintCandidateFace(const CollisionContext<T>* ctx,
                                                      int f, bool b){
    faceId = f;
    status = Disabled;
    distance0 = distancec = 0;

    intersection0 = intersectionc = false;

    tangentialDefined = false;

    valid = true;

    backFace = b;

    vertexId = -1;

    forced = false;

    vertexCollapsed = false;

    ctx->mesh->getEdgeIndicesOfTriangle(f, edgeIndices);
  }

  /*Check if there is an intersection */
  template<class T>
  bool ConstraintCandidateFace<T>::checkCollision(const OrientedVertex<T>& p,
                                                  const Vector4<T>& com,
                                                  const Quaternion<T>& rot,
                                                  T* dist,
                                                  bool* plane){
    int colCode = -1;

    bool rigidFace   = Mesh::isRigidOrStatic(faceType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    T eps = (T)DISTANCE_EPS2;

    bool debug = false;

    //START_DEBUG;
    if(//vertexId == 4378 && faceId == 20156 /*&& backFace*/
       //(vertexId == 7077 && faceId == 21330)
              //||
              //(vertexId == 1497 && faceId == 15466)
       //vertexId == 4451
       //false
       //(vertexId == 7373 && faceId == 57794)||
       //(vertexId == 31840 && faceId == 3270)||
       //(vertexId == 31840 && faceId == 3271)
       //(vertexId == 31849 && faceId == 2070)
       (vertexId == 32186 && faceId == 18822) ||
       (vertexId == 32186 && faceId == 18823)
              ){
      debug = true;
      warning("checkCollision vf %d, %d", vertexId, faceId);



      message("state + rigid transformations");


      std::cout << tu << std::endl;
      std::cout << comtu << std::endl;
      std::cout << rottu << std::endl;
      std::cout << t0 << std::endl;
      std::cout << comt0 << std::endl;
      std::cout << rott0 << std::endl;
      std::cout << p  << std::endl;
      std::cout << com << std::endl;
      std::cout << rot << std::endl;
      std::cout << x0 << std::endl;
      std::cout << comx0 << std::endl;
      std::cout << rotx0 << std::endl;
      getchar();
    }
    //END_DEBUG;

    try{
      if(!rigidFace && !rigidVertex){
        T root = 0;
        Vector4<T> intersectedVertex;

        colCode = intersection(tu, t0, p.getVertex(), x0.getVertex(),
                               intersectedVertex, eps*0,
                               &baryi, &ti, (T)DISTANCE_EPS,
                               (T)1.0, 1, &root,
                               forced);

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
          Vector4<T> intersectedVertex2 = x0.getVertex() * ((T)1.0 - root) + p.getVertex() * root;

          START_DEBUG;
          message("deformable root = %10.10e, result = ", root);
          std::cout << intersectedVertex2 << std::endl;
          std::cout << intersectedVertex << std::endl;
          std::cout << baryi << std::endl;
          END_DEBUG;

          xi.setVertex(intersectedVertex);
        }else{
          START_DEBUG;
          message("no intersection, root = %10.10e", root);
          END_DEBUG;
        }
        if(dist)*dist = root;
      }else{
        T root = 0;

        Vector4<T> intersectedVertex;

        colCode = rigidIntersection(tu, comtu, rottu*rott0.inverse(),
                                    t0, comt0, rott0*rott0.inverse(),
                                    p.getVertex(),  com,   rot*rotx0.inverse(),
                                    x0.getVertex(), comx0,  rotx0*rotx0.inverse(),
                                    intersectedVertex, eps*0,
                                    &baryi, &ti, (T)DISTANCE_EPS,
                                    (T)1.0, 1,
                                    rigidFace, rigidVertex, &root, forced);

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
          comxi = comx0 * ((T)1.0 - root) + com   * root;
          comti = comt0 * ((T)1.0 - root) + comtu * root;

          rotxi = slerp(rotx0*rotx0.inverse(), rot*rotx0.inverse(),   root);
          rotti = slerp(rott0*rott0.inverse(), rottu*rott0.inverse(), root);

          Vector4<T> intersectedNormal;

          Vector4<T> intersectedVertex2 = x0.getVertex() * ((T)1.0 - root) + p.getVertex() * root;

          START_DEBUG;
          message("rigid root = %10.10e, result = ", root);
          std::cout << intersectedVertex2 << std::endl;
          std::cout << intersectedVertex << std::endl;
          std::cout << baryi << std::endl;
          END_DEBUG;

          xi.setVertex(intersectedVertex);
        }
        if(dist) *dist = root;//t0.getSignedDistance(x0);
      }
    }catch(Exception* e){
      message("Exception in root finding:: %s", e->getError().c_str());
#if 0
      message("rigidFace = %d", rigidFace);
      message("rigidVertex = %d", rigidVertex);
      message("face = %d", faceId);
      message("vertex = %d", vertexId);
      message("t0, tu, x0, p");
      std::cout << t0 << std::endl;
      std::cout << tu << std::endl;
      std::cout << x0 << std::endl;
      std::cout << p << std::endl;

      if(debug){
        warning("rootfinder failed");
        warning("Exception in root finding:: %s", e->getError().c_str());
        warning("rigidFace = %d", rigidFace);
        warning("rigidVertex = %d", rigidVertex);
        warning("face = %d", faceId);
        warning("vertex = %d", vertexId);
        warning("t0, tu, x0, p");
        std::cerr << t0 << std::endl;
        std::cerr << tu << std::endl;
        std::cerr << x0 << std::endl;
        std::cerr << p << std::endl;
      }

      getchar();
#endif

      delete e;
      valid = false;
      return false;
    }

    if(debug){
      warning("colcode = %d", colCode);
      warning("barycentric = ");
      std::cerr << baryi << std::endl;

      warning("rigidFace = %d", rigidFace);
      warning("rigidVertex = %d", rigidVertex);
      warning("face = %d", faceId);
      warning("vertex = %d", vertexId);
      warning("t0, tu, x0, p");
      std::cerr << t0 << std::endl;
      std::cerr << tu << std::endl;
      std::cerr << x0 << std::endl;
      std::cerr << p << std::endl;

      getchar();
    }

    if(debug){
      //exit(0);
    }

    if(colCode == 1){
      int vertexEdges[3][3] = {{0, 1, 2},
                               {1, 2, 0},
                               {2, 1, 0}};
      for(int i=0;i<3;i++){
        /*Check distances of vertices and edges*/
        Vector4<T> edge = ti.v[vertexEdges[i][1]] - ti.v[vertexEdges[i][2]];
        edge.normalize();

        Vector4<T> pp = ti.v[vertexEdges[i][0]] - ti.v[vertexEdges[i][2]];

        T projection = dot(edge, pp);

        Vector4<T> projectedpp = ti.v[vertexEdges[i][2]] + projection * edge;

        if((projectedpp - pp).length() < 5e-4){
          /*Too close*/
          warning("vertex too close to edge 1, triangle too small");
          return false;
        }

        /*These tangent distances are checked in the edge candidate
          before the vertex-face candidates are forced.*/
        /*
        if(forced){
          edge = xi.getVertex() - ti.v[i];
          if(edge.length() < 5e-4){
            warning("vertex too close to edge 2, triangle too small");
            return false;
          }
        }
        */
      }
    }

    if(colCode == 1){
      T dist = ti.getSignedDistance(xi.getVertex());

      if(Abs(dist) > 2e-8){
        return false;
      }
      distancei = dist - eps;

      //ti = t0;
      //xi = x0;

      //baryi = ti.getBarycentricCoordinates(xi.getVertex());

      return true;
    }
    return false;
  }

  template<class T>
  void ConstraintCandidateFace<T>::forceConstraint(){
    forced = true;
    //warning("forcing constraint v %d - f %d - backface = %d",
    //        vertexId, faceId, backFace);
    //forced = false;
  }

  template<class T>
  bool ConstraintCandidateFace<T>::evaluate(ConstraintSet<T>* c,
                                            CollisionContext<T>* ctx,
                                            const OrientedVertex<T>& p,
                                            const Vector4<T>& com,
                                            const Quaternion<T>& rot){
    T distance = 0.0;

    bool rigidFace   = Mesh::isRigidOrStatic(faceType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    /*Check if one of the edges associated with p is collapsed, if so,
      skip evaluation until the collapsed state is resolved. If this
      pair is forced to resolve a collapse, then we should perform the
      test.*/

    if(!forced){
      List<int> edgeIds;
      p.getEdgeIndices(edgeIds);

      List<int>::Iterator eIdIt = edgeIds.begin();

      while(eIdIt != edgeIds.end()){
        int edgeId = *eIdIt++;

        if(backFace){
          if(ctx->collapsedBackEdges[edgeId]){
            //message("edge connected to vertex is collapsed");
            return false;
          }
        }else{
          if(ctx->collapsedEdges[edgeId]){
            //message("edge connected to vertex is collapsed");
            return false;
          }
        }
      }

      bool collapsed = false;

      if(rigidFace || rigidVertex){
        collapsed = false;
      }else{
        collapsed = orientedVertexCollapsed(x0, p);
      }

      if(collapsed){
        START_DEBUG;
        std::cout << x0 << std::endl;
        std::cout << p  << std::endl;
        warning("collapsed vertex v %d - f %d - backface = %d",
                vertexId, faceId, backFace);

        END_DEBUG;
        vertexCollapsed = true;
        return false;
      }else{
        vertexCollapsed = false;
      }
    }else{
      /*Forced*/
      vertexCollapsed = false;
    }

    intersectionc = p.planeIntersection(tu.v[0], tu.n, indices,
                                        forced || rigidFace || rigidVertex,
                                        false);

    //if(distance0 >= 0.0){
      /*Initial distance is positive, so there can only be a frontface
        collision*/
    distance = tu.getSignedDistance(p.getVertex(), &bary2) - (T)(DISTANCE_EPS2*0.0);
    distancec = distance;
      //}else{
      /*Initial distance is negative, so there can only be a backface
        collision*/
      //distance = tu.getSignedDistance(p, &bary2) + (T)DISTANCE_EPS2;
      //distancec = distance;
      //}

    if(!forced){
      /*Not a forced pair, check for collapsed edges forming this triangle*/
      for(int i=0;i<3;i++){
        //if(backFace){
          if(ctx->collapsedBackEdges[edgeIndices[i]]){
            DBG(warning("Ignored vertex-face case due to collapsed edge"));
            return false;
          }
          //}else{
          if(ctx->collapsedEdges[edgeIndices[i]]){
            DBG(warning("Ignored vertex-face case due to collapsed edge"));
            return false;
          }
          //}
      }
    }

    int vertexEdges[3][3] = {{0, 1, 2},
                             {1, 2, 0},
                             {2, 1, 0}};
    for(int i=0;i<3;i++){
      /*Check distances of vertices and edges*/
      Vector4<T> edge = tu.v[vertexEdges[i][1]] - tu.v[vertexEdges[i][2]];
      edge.normalize();

      Vector4<T> pp = tu.v[vertexEdges[i][0]] - tu.v[vertexEdges[i][2]];

      T projection = dot(edge, pp);

      Vector4<T> projectedpp = tu.v[vertexEdges[i][2]] + projection * edge;

      if((projectedpp - pp).length() < 5e-4){
        /*Too close*/
        //warning("vertex too close to edge, triangle too small");
        //return false;
      }
    }

    /*Check if oriented vertex does not have a too small edge*/
    if(p.getSmallestEdge() < 5e-4){
      warning("vertex connected to too small edge");
      return false;
    }

    bool debug = false;

    if(
       //vertexId == 2457 && faceId == 17124 && !backFace
       //(vertexId == 7077 && faceId == 21330)
       //||
       //(vertexId == 14083 && faceId == 15466)
       //(faceId == 9896 && vertexId == 4097)
       //||
       //(faceId == 5120 && vertexId == 4214)
       //vertexId == 4451
       //(vertexId == 7373 && faceId == 57794)||
       //(vertexId == 31840 && faceId == 3270)||
       //(vertexId == 31840 && faceId == 3271)
       (vertexId == 32186 && faceId == 18822) ||
       (vertexId == 32186 && faceId == 18823)
       //false
       ){
      warning("face     = %d", faceId);
      warning("vertex   = %d", vertexId);
      warning("backface = %d", backFace);
      warning("enabled  = %d", status == Enabled);

      std::cerr << p << std::endl;
      std::cerr << tu << std::endl;
      warning("distance0     = %10.10e", distance0);
      warning("distancec     = %10.10e", distancec);
      warning("intersection0 = %d", intersection0);
      warning("intersectionc = %d", intersectionc);
      std::cerr << t0 << std::endl;
      std::cerr << tu << std::endl;
      std::cerr << x0 << std::endl;
      std::cerr << p << std::endl;

      message("intersection0");
      intersection0 = x0.planeIntersection(t0.v[0], t0.n, indices, false, true);

      message("intersectionc");
      intersectionc = p.planeIntersection(tu.v[0], tu.n, indices, false, true);
      debug = true;
      getchar();
    }



    if(IsNan(distance)){
      std::cout << tu << std::endl;
      std::cout << p << std::endl;
      error("NAN detected");
    }

    if(status != Enabled){
      /*In the old approach we used the signed distance to determine
        if a collision had occured. This works fine as long the motion
        is approximately linear. However, in case of extreme
        deformations, triangles can become small and the non-linearity
        of the motion increases. In general, when there is a
        transistion between a non-intersecting to an intersecting
        state, one can assume that in between there should be a
        configuration in which the triangles are just
        touching. However, it is possible that the initial
        configuration is also 'intersecting'. Since we only test
        infinite planes and edges, they could have a collision, while
        the collision points are outside the triangle/edge. Normally
        such a case is resolved by surrounding faces/edges such that
        at the beginning of the next time-step, that collision is
        'resolved'.  There was however no real collision, but now the
        entities are on the proper side of each other. If they not
        start to collide, we have a clear transistion between a
        non-intersecting to an intersecting case.

        If the motion is highly non-linear, it is possible that a
        particular vertex is behind a face at the beginning of the
        time-step (but outside the triangle and touching neighbouring
        faces). When the vertex moves in the direction of the face,
        eventually it goes of the current face and crosses the
        neighboring face from behind, resulting in a non-intersecting
        state. If the neighboring face does not change much, then at
        the next time-step, it will probably intersect the face again
        from the front side. However, if in the previous time-step,
        where it crosses the backface, the face changes orientation,
        it is possible that it crosses through the front-face first. A
        clear intersection/collision. However, if we only look at the
        signed distances, they are both negative at the beginning and
        end of the time-step. So testing for a sign-change on the
        signed distance won't work. They are in both cases
        negative. Also the face and vertex are intersecting at both
        the beginning and end of the timestep. So there is no
        transistion between a non-intersecting to an intersecting
        state. Hence, the crossing is not detected. This eventually
        result in a failure of the simulation since collisions are
        missed and the system relies on a collision free state.

        In the new approach we don't look for a crossing, but start
        the rootfinder immediately after an intersecting state is
        found. Using the knowledge that multiple zero crossings exist
        (for the signed distance) we are able to find them using a
        different root finding technique. This guarantees that all
        collisions are found.

        The described problem is especially notable for vertices close
        to the edge of a triangle and is about to cross the edge to a
        neighboring triangle. */

      Vector4<T> bary0 = t0.getBarycentricCoordinates(x0.getVertex());
      Vector4<T> baryc = tu.getBarycentricCoordinates(p.getVertex());

      //bool projection0 = t0.barycentricInTriangle(bary0, (T)0.0);
      //bool projectionc = tu.barycentricInTriangle(baryc, (T)0.0);

      bool projection = barycentricFaceMatch(bary0, baryc);
      projection = false;

      if(rigidVertex || rigidFace){
        if(debug){message("rigid vertex or face");getchar();}

        if(projection){
          if(distancec <= 0.0 && distance0 > 0.0){
            if(debug){message("intersection based on distances %10.10e, %10.10e", distancec, distance0);getchar();}
            return true;
          }
        }else{
          if((distancec <= 0.0 && distance0 >= 0.0)
             ||
             (distancec <= 0.0 && distance0 <= 0.0)){
            if(debug){message("intersection based on distances %10.10e, %10.10e", distancec, distance0);getchar();}
            return true;
          }
        }
#if 0
        if( (distancec <= 0.0 && distance0 > 0)
            ||
            (distancec >= 0.0 && distance0 > 0.0)
            ){
          if(debug){message("intersection based on distances %10.10e, %10.10e", distancec, distance0);getchar();}
          return true;
        }
#endif
      }else{
        if(projection){
          if(distancec <= 0.0 && distance0 > 0.0 &&
             intersectionc && !intersection0){
            if(debug){message("intersection based on distances %10.10e, %10.10e", distancec, distance0);getchar();}
            return true;
          }
        }else{
          if((distancec <= 0.0 && distance0 >= 0.0 &&
              intersectionc && !intersection0)
             ||
             (distancec <= 0.0 && distance0 <= 0.0 &&
              intersectionc && intersection0
              )){
            if(debug){message("intersection based on distances %10.10e, %10.10e", distancec, distance0);getchar();}
            if(debug){message("intersection based on status %d, %d", intersection0, intersectionc);getchar();}
            return true;
          }
        }
        //
      }
#if 0
      }else if((
                (distancec <= 0 && distance0 >= 0)
                ||
                (distancec >= 0 && distance0 >= 0)
                )
               &&
               ((intersection0 == false && intersectionc == true)
                ||
               //(intersection0 && !intersectionc)||
                (!intersection0 && !intersectionc))
               //)
               //(intersection0 == false && distancec < 0.0 && intersectionc == true
               //&& distancec <= 0.0 && distance0 > 0.0
         //||
         //(forced && distancec <= 1e-9 && distance0 >= 0.0)
         ){
        if(debug){message("intersection based on stated %d, %d", intersection0, intersectionc);getchar();}
        DBG(message("intersection"));
        if(forced){
          DBG(message("forced intersection"));
        }
        return true;
      }
#endif
    }else{
      /*Constraint is enabled check projection*/
    }
    return false;
  }

  template<class T>
  void ConstraintCandidateFace<T>::updateGeometry(CollisionContext<T>* ctx,
                                                  ConstraintSet<T>* s,
                                                  const OrientedVertex<T>& p,
                                                  const OrientedVertex<T>& x){
    vertexId = s->vertexId;
    try{
      ctx->mesh->getDisplacedTriangle(&tu, faceId, backFace);
      valid = true;
    }catch(Exception* e){
      warning("Singular displaced triangle, %s", e->getError().c_str());
      warning("face id = %d", faceId);
      std::cout << tu << std::endl;
      getchar();
      valid = false;
      throw e;
    }

    /*If normal is ok, compute signed distance of the face with its
      own vertex*/

    T det = (T)0.0;
    T sd = tu.getSignedDistance(tu.v[0], 0, 0, &det);

    if(IsNan(sd) || Abs(det) < 1e-19) {
      warning("sd = %10.10e, det = %10.10e", sd, det);
      throw new DegenerateCaseException(__LINE__, __FILE__, "Degenerate case");
    }


    ctx->mesh->setCurrentEdge(ctx->mesh->halfFaces[faceId].half_edge);
    int vertex = ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(ctx->mesh->vertices[vertex].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertex];

      comtu = ctx->mesh->centerOfMassDispl[objectId];
      rottu = ctx->mesh->orientationsDispl[objectId];
    }
  }

  template<class T>
  void ConstraintCandidateFace<T>::updateConstraint(ConstraintSet<T>* c,
                                                    CollisionContext<T>* ctx,
                                                    OrientedVertex<T>& x,
                                                    OrientedVertex<T>& p,
                                                    const Vector4<T>& com,
                                                    const Quaternion<T>& rot,
                                                    bool* skipped,
                                                    EvalStats& stats){
    ContactConstraint<2, T>* cc =
      (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);

#ifdef DEBUG_UPDATE
    warning("updateConstraint");
    std::cerr << t0 << std::endl;
    std::cerr << tu << std::endl;
    std::cerr << x0 << std::endl;
    std::cerr << p << std::endl;
    std::cerr << x << std::endl;
    std::cerr << xi << std::endl;
    std::cerr << ti << std::endl;
    std::cerr << enabledX0 << std::endl;
    std::cerr << enabledXU << std::endl;
    std::cerr << enabledT0 << std::endl;
    std::cerr << enabledTU << std::endl;
#endif

    T initialWeight = (T)3.0;

    bool rigidFace   = Mesh::isRigidOrStatic(faceType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    Triangle<T> newFace;
    Vector4<T> newVertex;
    Vector4<T> newNormal;
    //Vector4<T> baryOld = baryi;

    T weight1 = (T)1.0;
    T weight2 = (T)0.0;

#ifdef DEBUG_UPDATE
    cc->print(std::cerr);
#endif

    /*Search for a new interpolated instance of the vertex and face in
      which the contactnormal does not change too much.*/
    while(true){
      weight1 = (T)1.0 - (T)1.0/initialWeight;
      weight2 = (T)1.0 - weight1;

      Triangle<T> newFace2;
      Vector4<T> newVertex2;

      if(rigidFace){
        Vector4<T> newCom = weight1 * comti + weight2 * comtu;
        Quaternion<T> newRot = slerp(rotti,//is already corrected
                                     rottu*rott0.inverse(), weight2);

        Vector4<T> v11 = newCom + newRot.rotate(t0.v[0] - comt0);
        Vector4<T> v12 = newCom + newRot.rotate(t0.v[1] - comt0);
        Vector4<T> v13 = newCom + newRot.rotate(t0.v[2] - comt0);

        newFace2.set(v11, v12, v13);
      }else{
        newFace2.set(weight1 * ti.v[0] + weight2 * tu.v[0],
                     weight1 * ti.v[1] + weight2 * tu.v[1],
                     weight1 * ti.v[2] + weight2 * tu.v[2]);
      }

      if(rigidVertex){
        Vector4<T> newCom = weight1 * comxi + weight2 * com;
        Quaternion<T> newRot = slerp(rotxi,//is already corrected
                                     rot*rotx0.inverse(), weight2);

        Vector4<T> v1 = newCom + newRot.rotate(x0.getVertex() - comx0);
        newVertex2 = v1;
      }else{
        newVertex2 = weight1 * xi.getVertex() + weight2 * p.getVertex();
      }

      if(dot(ti.n, newFace2.n) > 0.85){
        /*Accept current face and vertex*/
        newFace   = newFace2;
        newVertex = newVertex2;
        break;
      }
      initialWeight *= (T)2.0;
    }

#ifdef DEBUG_UPDATE
    warning("Face edges1 = %10.10e", (tu.v[0] - tu.v[1]).length());
    warning("Face edges2 = %10.10e", (tu.v[1] - tu.v[2]).length());
    warning("Face edges3 = %10.10e", (tu.v[2] - tu.v[0]).length());
    warning("Normal error = %10.10e", dot(ti.n, newFace.n));
#endif

    Vector4<T> bary = ti.getBarycentricCoordinates(xi.getVertex());
    Vector4<T> weights = bary;

    if(forced){
      ////weights = ti.edgeCrossedBarycentricCoordinates(xi.getVertex());
      //weights = ti.getBarycentricCoordinates(xi.getVertex());
    }

    //Vector4<T> diff =
    //  (ti.getCoordinates(baryOld) - ti.getCoordinates(weights));

    if(dot(ti.n, newFace.n) < (T)0.999 ||
       //diff.length() > 1e-4){
       (baryi-weights).length() > 1e-4){
      //if(true){
      warning("contact normal changed");
#ifdef DEBUG_UPDATE
      warning("t0 x0");
      std::cerr << t0 << std::endl;
      std::cerr << x0 << std::endl;

      warning("sd = %10.10e", t0.getSignedDistance(x0.getVertex()));

      warning("ti xi");
      std::cerr << ti << std::endl;
      std::cerr << xi << std::endl;

      warning("sd = %10.10e", ti.getSignedDistance(xi.getVertex()));
#endif

      /*Normal has changed, update constraint*/
      ti = newFace;

      if(rigidFace){
        comti = weight1 * comti + weight2 * comtu;
        rotti = slerp(rotti, rottu*rott0.inverse(), weight2);
      }

      if(rigidVertex){
        comxi = weight1 * comxi + weight2 * com;
        rotxi = slerp(rotxi, rot*rotx0.inverse(), weight2);
      }

      xi.setVertex(newVertex);

#ifdef DEBUG_UPDATE
      warning("tii xii");
      std::cerr << ti << std::endl;
      std::cerr << xi << std::endl;

      warning("sd = %10.10e", ti.getSignedDistance(xi.getVertex()));

      warning("coordinates");
      std::cerr << ti.getCoordinates(weights) << std::endl;
      std::cerr << xi << std::endl;
#endif

#ifdef WEIGHT_CLAMPING

      if(forced){
        weights =
          ti.edgeCrossedBarycentricCoordinates(ti.getCoordinates(weights));
      }else{
        weights = ti.clampWeights(weights, WEIGHT_MIN, WEIGHT_MAX);
      }
#else
      weights = weights;
      if(forced){
        // weights =
        //ti.edgeCrossedBarycentricCoordinates(ti.getCoordinates(weights));
      }
#endif

#ifdef DEBUG_UPDATE
      warning("weights of point");
      std::cerr << weights << std::endl;

      warning("coordinates");
      std::cerr << ti.getCoordinates(weights) << std::endl;
      std::cerr << xi << std::endl;
#endif
      if(!ti.barycentricInTriangleDist(bary, (T)DISTANCE_EPS)){
        message("contact off triangle");
        std::cout << weights << std::endl;
      }
#if 1
      //exit(0);
      //bool offTriangle = false;
      //T offTriangleEps = (T)1e-8;
      if(!forced
         &&
         !ti.barycentricInTriangleDist(bary, (T)DISTANCE_EPS)
         && distancec > DISTANCE_EPS
          ){
        /*Could be a source of potential errors!!!*/

        /*Check for all edges connected to this vertex, which other
          edge is crossed and recompute the intersection0 state. This
          coud trigger new edge-edge collisions*/

        //#error blblbl
        message("don't care");
        *skipped = true;
#if 1
        disableConstraint(c, ctx, true);
        /*Save current configuration for root finding in future*/
        t0 = tu;
        comt0 = comtu;
        rott0 = rottu;

        //x0 = pu;
        //updateInitialState(p, com, rot);

        t0 = tu;
        comt0 = comtu;
        rott0 = rottu;

        x0 = p;
        comx0 = com;
        rotx0 = rot;

        distance0 = distancec;
        intersection0 = intersectionc;


        c->updateStartPositions(p, com, rot, this);
#endif
        return;
        //offTriangle = true;
        //if(distancec > offTriangleEps){
        //  *skipped = true;
        //  return;
        // }
      }
#endif
      //DO NOT UPDATE collisionpoint
      baryi = weights;

      Vector4<T> t1, t2;

      int indices[3];
      ctx->mesh->getTriangleIndices(faceId, indices, backFace);

      T dt = ctx->getDT();

      t1 = -( cc->getRow(1, 0) -
             (cc->getRow(1, 1) +
              cc->getRow(1, 2) +
              cc->getRow(1, 3)));

      t2 = -( cc->getRow(2, 0) -
             (cc->getRow(2, 1) +
              cc->getRow(2, 2) +
              cc->getRow(2, 3)));

      t1.normalize();
      t2.normalize();

      /*Re-align with contact normal*/
      Vector4<T> tt1, tt2;
      tt2 = cross(t1, ti.n).normalize();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      tt1 = cross(t2, ti.n).normalize();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

      /*Check if face id is from the body or the constraining geometry*/
      bool bodyVertex  = true;
      bool bodyFace    = true;
      bool rigidVertex = false;
      bool rigidFace   = false;

      if(Mesh::isStatic(ctx->mesh->vertices[c->vertexId].type)){
        bodyVertex = false;
      }
      if(Mesh::isRigid(ctx->mesh->vertices[c->vertexId].type)){
        rigidVertex = true;
      }

      for(int i=0;i<3;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyFace = false;
        }
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
      }

      int cidx2 = 0;

      if(bodyVertex){
        if(rigidVertex){
          cidx2 = 2;
        }else{
          cidx2 = 1;
        }
      }

      if(bodyVertex){
        if(rigidVertex){
          t1 = -cc->getRow(1, 0);
          t2 = -cc->getRow(2, 0);
        }else{
          t1 = -cc->getRow(1, 0);
          t2 = -cc->getRow(2, 0);
        }
      }

      if(bodyFace){
        if(rigidFace){
          t1 = cc->getRow(1, cidx2);
          t2 = cc->getRow(2, cidx2);
        }else{
          t1 = cc->getRow(1, cidx2) + cc->getRow(1, cidx2+1) + cc->getRow(1, cidx2+2);
          t2 = cc->getRow(2, cidx2) + cc->getRow(2, cidx2+1) + cc->getRow(2, cidx2+2);
        }
      }

      t1.normalize();
      t2.normalize();

      /*Re-align with contact normal*/
      tt2 = cross(t1, ti.n).normalize();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      tt1 = cross(t2, ti.n).normalize();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

      Vector4<T> rv, rf;

      if(rigidVertex){
        rv    = xi.getVertex() - comxi;
      }

      if(rigidFace){
        Vector4<T> txi = ti.getCoordinates(weights);
        rf = txi - comti;
      }

      //cc->status = Inactive;
      //cc->frictionStatus[0] = Kinetic;
      //cc->frictionStatus[1] = Kinetic;

      /*Non-penetration constraints go in row 0*/
      T d1 = ti.getSignedDistance(x0.getVertex());
      T d2 = ti.getSignedDistance(t0.v[0]);
      T d3 = ti.getSignedDistance(t0.v[1]);
      T d4 = ti.getSignedDistance(t0.v[2]);

      d2 *= baryi[0];
      d3 *= baryi[1];
      d4 *= baryi[2];

      PRINT(d1 - d2 - d3 - d4);

      T totalDistance =
        (d1 - d2 - d3 - d4) - (T)DISTANCE_EPS - (T)EPS;

      //if(offTriangle){
      //  totalDistance = (d1 - d2 - d3 - d4) - offTriangleEps;
      //}

#ifdef DEBUG_UPDATE
      if(true){
        warning("d1 = %10.10e, d2 = %10.10e, d3 = %10.10e, d4 = %10.10e, td = %10.10e", d1, d2, d3, d4, totalDistance);
      }
#endif


      /*Set values for normal distance*/

      int cidx = 0;

      /*Compute tangential gap*/
      d1 = dot(t1, xi.getVertex() - x0.getVertex());
      d2 = dot(t1, ti.v[0] - t0.v[0]);
      d3 = dot(t1, ti.v[1] - t0.v[1]);
      d4 = dot(t1, ti.v[2] - t0.v[2]);

      d2 *= baryi[0];
      d3 *= baryi[1];
      d4 *= baryi[2];

      T totalDistanceT1 = -(d1 - d2 - d3 - d4);

      d1 = dot(t2, xi.getVertex() - x0.getVertex());
      d2 = dot(t2, ti.v[0] - t0.v[0]);
      d3 = dot(t2, ti.v[1] - t0.v[1]);
      d4 = dot(t2, ti.v[2] - t0.v[2]);

      d2 *= baryi[0];
      d3 *= baryi[1];
      d4 *= baryi[2];

      T totalDistanceT2 = -(d1 - d2 - d3 - d4);

#if 1
      /*Check this for backface collisions*/
      T errorDist = (T)(DISTANCE_EPS+EPS)*(T)1.0 - distancec;

      //if(offTriangle){
        //  errorDist = offTriangleEps - distancec;
      //}

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-4);

#ifdef DEBUG_UPDATE
      if(true){
        message("distancec = %10.10e", distancec);
        message("errordist = %10.10e", errorDist);
      }

      //errorDist = Sign(errorDist)*Max(Abs(errorDist), (T)DISTANCE_EPS + (T)EPS);

      if(true){
        message("errordist = %10.10e", errorDist);
      }
#endif

      //totalDistance = (T)cc->getConstraintValue(0) - errorDist*(T)0.45;
      totalDistance -= errorDist*(T)0.45;

#ifdef DEBUG_UPDATE
      if(true){
        message("totalDistance = %10.10e", totalDistance);
      }
#endif

#endif
      Vector4<T> normal = ti.n;

      //totalDistance = totalDistanceT1 = totalDistanceT2 = (T)0.0;

      //totalDistanceT1 += cc->c[1];
      //totalDistanceT2 += cc->c[2];

      //totalDistanceT1 /= (T)2.0;
      //totalDistanceT2 /= (T)2.0;

      //totalDistanceT1 = totalDistanceT2 = (T)0.0;

      //totalDistanceT1 = cc->getConstraintValue(1);
      //totalDistanceT2 = cc->getConstraintValue(2);

      if(bodyVertex){
        int velIndex = ctx->mesh->vertices[c->vertexId].vel_index/3;

        if(rigidVertex){
          cc->setRow(-normal*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(    -t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(    -t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(-cross(rv, normal)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(    -cross(rv, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(    -cross(rv, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);

          cidx = 2;
        }else{
          cc->setRow(-normal*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(    -t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(    -t2*dt, totalDistanceT2, 2, 0, velIndex);

          cidx = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(normal*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(    t1*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(    t2*dt, totalDistanceT2, 2, cidx, velIndex);

          cc->setRow(cross(rf,normal)*dt, totalDistance,   0, cidx+1, velIndex+1);
          cc->setRow(    cross(rf,t1)*dt, totalDistanceT1, 1, cidx+1, velIndex+1);
          cc->setRow(    cross(rf,t2)*dt, totalDistanceT2, 2, cidx+1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(normal*baryi[0]*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(    t1*baryi[0]*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(    t2*baryi[0]*dt, totalDistanceT2, 2, cidx, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(normal*baryi[1]*dt, totalDistance,   0, cidx+1, velIndex);
          cc->setRow(    t1*baryi[1]*dt, totalDistanceT1, 1, cidx+1, velIndex);
          cc->setRow(    t2*baryi[1]*dt, totalDistanceT2, 2, cidx+1, velIndex);

          velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(normal*baryi[2]*dt, totalDistance,   0, cidx+2, velIndex);
          cc->setRow(    t1*baryi[2]*dt, totalDistanceT1, 1, cidx+2, velIndex);
          cc->setRow(    t2*baryi[2]*dt, totalDistanceT2, 2, cidx+2, velIndex);
        }
      }
    }else{
      /*Current face aligned with interpolated face, only update the
        distance*/

      warning("no change in normal detected");

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);

      cc->setMu((T)MU);

      if(backFace || forced){
        cc->setMu((T)MU*(T)0.0);
      }

      if(cc->status != Active){
        /*Do not update inactive constraint this way*/
        //return;
      }

      /*Check this for backface collisions*/
      T errorDist = (T)DISTANCE_EPS+(T)EPS - distancec;

      //errorDist = Sign(errorDist)*Max(Abs(errorDist*(T)0.45), (T)DISTANCE_EPS + (T)EPS);
      //errorDist = Sign(errorDist)*Max(Abs(errorDist), (T)DISTANCE_EPS + (T)EPS);
#if 1
      Vector4<T> weights = ti.getBarycentricCoordinates(xi.getVertex());

      if(forced){
        //weights = ti.edgeCrossedBarycentricCoordinates(xi.getVertex());
        //weights = ti.getBarycentricCoordinates(xi.getVertex());
      }

      if(!ti.barycentricInTriangleDist(bary, (T)DISTANCE_EPSILON)){
        message("contact off triangle");
        std::cout << weights << std::endl;
      }

      if(!forced
         &&
         !ti.barycentricInTriangleDist(weights, (T)DISTANCE_EPS)
         &&
         distancec > DISTANCE_EPS
         ){
        /*Could be a source of potential errors!!!*/
        *skipped = true;

        ////t0 = tu;
        ////comt0 = comtu;
        ////rott0 = rottu;
        message("don't care");

#if 1
        disableConstraint(c, ctx, true);
        //updateInitialState(p, com, rot);
        t0 = tu;
        comt0 = comtu;
        rott0 = rottu;

        t0 = tu;
        comt0 = comtu;
        rott0 = rottu;

        x0 = p;
        comx0 = com;
        rotx0 = rot;

        distance0 = distancec;
        intersection0 = intersectionc;

        c->updateStartPositions(p, com, rot, this);
        /*Check for all edges connected to this vertex, which other
          edge is crossed and recompute the intersection0 state. This
          coud trigger new edge-edge collisions*/

        //#error blblbl
#endif
        return;
        //offTriangle = true;
        //if(distancec > offTriangleEps){
        //  *skipped = true;
        //  return;
        // }
      }
#endif

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-4);

      T totalDistance = cc->getConstraintValue(0) - (T)(errorDist*0.45);

      if(distancec > (DISTANCE_EPSILON - DISTANCE_TOL) && distancec < DISTANCE_TOL + (T)DISTANCE_EPSILON){
        *skipped = true;
        message("in range");
        totalDistance = cc->getConstraintValue(0) - (T)(errorDist*0.05);
        return;
      }
      T oldC = cc->getConstraintValue(0);


#if 1
      warning("old c     = %10.10e", oldC);
      warning("distancec = %10.10e", distancec);
      warning("errorDist = %10.10e", errorDist);
      warning("totalDist = %10.10e", totalDistance);

      int cindex = cc->row_id;
      int offset = cc->getOffset();
      int size   = ctx->solver->getx()->getSize();

      warning("distance = %10.10e, multiplier = %10.10e", distancec,
              (*ctx->solver->getx())[size + cindex * offset]);

      warning("bary centric = ");
      std::cerr << baryi << std::endl;
#endif

      cc->getConstraintValue(0) = totalDistance;
    }

    updated = true;

    stats.n_geometry_check++;

    /*Set transposed values*/
    cc->init(ctx->getMatrix());
    cc->update(*ctx->getSolver()->getb(),
               *ctx->getSolver()->getx(),
               *ctx->getSolver()->getb2());
  }

  template<class T>
  void ConstraintCandidateFace<T>::updateInitial(ConstraintSet<T>* c,
                                                 CollisionContext<T>* ctx,
                                                 OrientedVertex<T>& x,
                                                 OrientedVertex<T>& p,
                                                 const Vector4<T>& com,
                                                 const Quaternion<T>& rot,
                                                 bool* skipped,
                                                 EvalStats& stats){
    Vector4<T> weights = ti.getBarycentricCoordinates(xi.getVertex());
    return;
    t0 = tu;
    comt0 = comtu;
    rott0 = rottu;

    x0 = p;
    comx0 = com;
    rotx0 = rot;

    distance0 = distancec;
    intersection0 = intersectionc;

    if(status == Enabled){
      if(!forced
         &&
         !ti.barycentricInTriangleDist(weights, (T)DISTANCE_EPSILON)
         &&
         distancec > 5*DISTANCE_EPSILON+DISTANCE_TOL){

        *skipped = true;

        if((vertexId == 32186 && faceId == 18822) ||
           (vertexId == 32186 && faceId == 18823)){
          message("disabling constraint %d - %d, upate initial",
                  vertexId, faceId);
          message("distance = %10.10e", distancec);
          std::cout << weights << std::endl;
          getchar();
        }

        disableConstraint(c, ctx, true);
      }
    }
  }

  template<class T>
  bool ConstraintCandidateFace<T>::enableConstraint(ConstraintSet<T>* c,
                                                    CollisionContext<T>* ctx,
                                                    OrientedVertex<T>& s,
                                                    OrientedVertex<T>& p){
    /*In case of a backfacing case, we need to use an inverted face
      normal in order to make the constraint working properly*/

    if(status == Enabled){
      /*Constraint was already active, do nothing*/
      return false;
    }else{
      ContactConstraint<2, T>* cc = new ContactConstraint<2, T>();

      cc->sourceType = 2;
      cc->sourceA = faceId;
      cc->sourceB = c->vertexId;

      if(backFace){
        cc->sourceType = 3;
      }

      enabledX0 = x0;
      enabledXU = p;
      enabledT0 = t0;
      enabledTU = tu;

      /*Bary1 -> barycentric coordinate of startPos on t0*/
      /*Bary2 -> barycentric coordinate of p        on tu*/
      /*Baryi -> barycentric coordinate of xi       on ti*/

      /*Distance0 -> distance between x0       and t0*/

      Vector4<T> weights = baryi;
#ifdef WEIGHT_CLAMPING
      //Vector4<T> weights = baryi;
      if(forced){
        weights = ti.edgeCrossedBarycentricCoordinates(ti.getCoordinates(baryi));//, WEIGHT_MIN, WEIGHT_MAX);
      }
#else
      weights = baryi;
      if(forced){
        //weights = ti.edgeCrossedBarycentricCoordinates(ti.getCoordinates(baryi));//, WEIGHT_MIN, WEIGHT_MAX);
      }
#endif

      cc->setMu((T)MU);
      if(backFace || forced){
        cc->setMu((T)MU*(T)0.0);
      }

      int indices[3];
      ctx->mesh->getTriangleIndices(faceId, indices, backFace);

      T dt = ctx->getDT();

      /*Check if face id is from the body or the constraining geometry*/
      bool bodyVertex  = true;
      bool bodyFace    = true;
      bool rigidVertex = false;
      bool rigidFace   = false;

      if(Mesh::isStatic(ctx->mesh->vertices[c->vertexId].type)){
        bodyVertex = false;
      }
      if(Mesh::isRigid(ctx->mesh->vertices[c->vertexId].type)){
        rigidVertex = true;
      }

      for(int i=0;i<3;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyFace = false;
        }
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
      }

      /*Non-penetration constraints go in row 0*/
      T d1 = ti.getSignedDistance(x0.getVertex());
      T d2 = ti.getSignedDistance(t0.v[0]);
      T d3 = ti.getSignedDistance(t0.v[1]);
      T d4 = ti.getSignedDistance(t0.v[2]);

      d2 *= weights[0];
      d3 *= weights[1];
      d4 *= weights[2];

      T totalDistance =
        (d1 - d2 - d3 - d4) - (T)DISTANCE_EPS - (T)EPS;

      Vector4<T> t1, t2;
      Vector4<T> rv, rf;

      Vector4<T> newDispl;
      Vector4<T> oldDispl;

      if(rigidVertex){
        Vector4<T> displVec = xi.getVertex() - x0.getVertex();
        Vector4<T> arm = x0.getVertex() - comxi;
        oldDispl = displVec;

        arm.normalize();

        Vector4<T> perp = cross(displVec, arm);
        perp.normalize();
        perp = cross(perp, arm);
        perp.normalize();

        if(dot(perp, displVec) < 0){
          perp *= -1;
        }

        Triangle<T> tip = ti; /*Displaced collision plane*/

        Vector4<T> offset = ti.n * (T)EPS;
        tip.v[0] += offset;
        tip.v[1] += offset;
        tip.v[2] += offset;

        /*Find intersection of displVec with ti*/
        Vector4<T> newxi;
        T tt;
        Vector4<T> bxi;
        tip.rayIntersection(x0.getVertex(), perp, newxi, &tt, &bxi);

        newDispl = newxi - x0.getVertex();
      }

#ifdef FRICTION_CONE
      if(tangentialDefined == true){
        /*If there is an old tangential vector, use that one*/
        t1 = -tangentReference[0];
        //t2 = -tangentReference[1];

        /*Align updated tangential vectors with old tangential vectors*/
        Vector4<T> tt1, tt2;
        tt2 = cross(t1,  ti.n).normalize();
        tt1 = cross(tt2, ti.n).normalize();

        t1 = tt1;
        t2 = tt2;

        if(rigidVertex){
          rv  = xi.getVertex() - comxi;
        }

        if(rigidFace){
          Vector4<T> txi = ti.getCoordinates(baryi);
          rf = txi - comti;
        }
        cc->tangent1 = t1;
        cc->tangent2 = t2;
        cc->normal   = ti.n;
      }else{
        Vector4<T> rnd(genRand<T>(), genRand<T>(), genRand<T>(), 0);

        Vector4<T> tt1, tt2;
        tt2 = cross(rnd, ti.n).normalize();
        tt1 = cross(tt2, ti.n).normalize();

        t1 = tt1;
        t2 = tt2;

        if(rigidVertex){
          rv  = xi.getVertex() - comxi;
        }

        if(rigidFace){
          Vector4<T> txi = ti.getCoordinates(baryi);
          rf = txi - comti;
        }
        cc->tangent1 = t1;
        cc->tangent2 = t2;
        cc->normal   = ti.n;
      }

#else
#ifdef ACTIVE_CONSTRAINTS
      /*In case of ICA, all constraints are removed and newly
        added. Using old tangent vectors wihout re-aligning them is
        not correct.*/
      //tangentialDefined = false;
#endif
      DBG(message("tangential defined = %d", tangentialDefined));

      if(tangentialDefined == true){
        /*If there is an old tangential vector, use that one*/
        t1 = -tangentReference[0];
        t2 = -tangentReference[1];

        /*Align updated tangential vectors with old tangential vectors*/
        Vector4<T> tt1, tt2;
        tt2 = cross(t1, ti.n).normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
        }

        tt1 = cross(t2, ti.n).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
        }

        t1 = tt1;
        t2 = tt2;

        if(rigidVertex){
          rv  = xi.getVertex() - comxi;
        }

        if(rigidFace){
          Vector4<T> txi = ti.getCoordinates(baryi);
          rf = txi - comti;
        }
      }else{
        Vector4<T> vel(0,0,0,0);
        Vector<T>* vx = ctx->getSolver()->getx();

        /*Compute current relative velocity*/
        if(bodyVertex){
          int vel_index = ctx->mesh->vertices[c->vertexId].vel_index;

          vel[0] = -(*vx)[vel_index+0];
          vel[1] = -(*vx)[vel_index+1];
          vel[2] = -(*vx)[vel_index+2];

          Vector4<T> ang_vel;

          if(rigidVertex){
            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            /*Compute angular displacement*/
            rv  = xi.getVertex() - comxi;
            ang_vel = cross(ang_vel, rv);

            vel -= ang_vel;
          }
        }

        if(bodyFace){
          if(rigidFace){
            int vel_index = ctx->mesh->vertices[indices[0]].vel_index;
            vel[0] += (*vx)[vel_index+0];
            vel[1] += (*vx)[vel_index+1];
            vel[2] += (*vx)[vel_index+2];

            Vector4<T> ang_vel;

            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            Vector4<T> txi = ti.getCoordinates(baryi);
            rf = txi - comti;

            ang_vel = cross(ang_vel, rf);

            vel += ang_vel;
          }else{
            for(int l=0;l<3;l++){
              int vel_index = ctx->mesh->vertices[indices[l]].vel_index;

              vel[0] += (*vx)[vel_index+0]*weights[l];
              vel[1] += (*vx)[vel_index+1]*weights[l];
              vel[2] += (*vx)[vel_index+2]*weights[l];
            }
          }
        }

        Vector4<T> vn = cross(vel, ti.n);

        /*Use velocity to align tangent vectors*/
        if(vn.length() < 1e-6){
          /*Choose some arbitrary direction*/
          Vector4<T> vec(1, 1, 1, 0);
          Vector4<T> vn2 = cross(vec, ti.n);
          if(vn2.length() < 1e-6){
            vec.set(-1, 1, 1, 0);
            vn2 = cross(vec, ti.n);
          }

          t2 = vn2.normalize();
          t1 = cross(t2, ti.n).normalize();
        }else{
          t2 = vn.normalize();
          t1 = cross(t2, ti.n).normalize();
        }
      }
#endif

      int cidx = 0;

      /*Compute tangential gap*/
      d1 = dot(t1, xi.getVertex() - x0.getVertex());
      d2 = dot(t1, ti.v[0] - t0.v[0]);
      d3 = dot(t1, ti.v[1] - t0.v[1]);
      d4 = dot(t1, ti.v[2] - t0.v[2]);

      d2 *= weights[0];
      d3 *= weights[1];
      d4 *= weights[2];

      T totalDistanceT1 = -(d1 - d2 - d3 - d4);

      d1 = dot(t2, xi.getVertex() - x0.getVertex());
      d2 = dot(t2, ti.v[0] - t0.v[0]);
      d3 = dot(t2, ti.v[1] - t0.v[1]);
      d4 = dot(t2, ti.v[2] - t0.v[2]);

      d2 *= weights[0];
      d3 *= weights[1];
      d4 *= weights[2];

      T totalDistanceT2 = -(d1 - d2 - d3 - d4);

#ifdef FRICTION_CONE
      //cc->kineticVector.set(0, -totalDistanceT1, -totalDistanceT2, 0);
      //cc->kineticVector.normalize();
#endif

      //totalDistanceT1 = totalDistanceT2 = 0;

      Vector4<T> normal = ti.n;

      //totalDistance = totalDistanceT1 = totalDistanceT2 = (T)0.0;
      //totalDistance *= (T)2.0;
      //totalDistanceT1 = totalDistanceT2 = (T)0.0;

      if(bodyVertex){
        int velIndex = ctx->mesh->vertices[c->vertexId].vel_index/3;

        if(rigidVertex){
          cc->setRow(-normal*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(    -t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(    -t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(-cross(rv, normal)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(    -cross(rv, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(    -cross(rv, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);

          cidx = 2;
        }else{
          cc->setRow(-normal*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(    -t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(    -t2*dt, totalDistanceT2, 2, 0, velIndex);

          cidx = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(normal*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(    t1*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(    t2*dt, totalDistanceT2, 2, cidx, velIndex);

          cc->setRow(cross(rf,normal)*dt, totalDistance,   0, cidx+1, velIndex+1);
          cc->setRow(    cross(rf,t1)*dt, totalDistanceT1, 1, cidx+1, velIndex+1);
          cc->setRow(    cross(rf,t2)*dt, totalDistanceT2, 2, cidx+1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(normal*weights[0]*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(    t1*weights[0]*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(    t2*weights[0]*dt, totalDistanceT2, 2, cidx, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(normal*weights[1]*dt, totalDistance,   0, cidx+1, velIndex);
          cc->setRow(    t1*weights[1]*dt, totalDistanceT1, 1, cidx+1, velIndex);
          cc->setRow(    t2*weights[1]*dt, totalDistanceT2, 2, cidx+1, velIndex);

          velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(normal*weights[2]*dt, totalDistance,   0, cidx+2, velIndex);
          cc->setRow(    t1*weights[2]*dt, totalDistanceT1, 1, cidx+2, velIndex);
          cc->setRow(    t2*weights[2]*dt, totalDistanceT2, 2, cidx+2, velIndex);
        }
      }

      /*Set transposed values*/
      cc->init(ctx->mat);

      cc->status = Active;
      //cc->frictionStatus[0] = Static;
      //cc->frictionStatus[1] = Static;

      enabledId = ctx->getSolver()->addConstraint(cc);

#ifdef ACTIVE_CONSTRAINTS
      cc->resetMultipliers(*ctx->getSolver()->getx(), cachedMultipliers);
#else
      Vector4<T> zero;
      cc->resetMultipliers(*ctx->getSolver()->getx(), zero);
#endif

      status = Enabled;
    }
    return true;
  }

  /*Recompute geometric properties of constraint*/
  template<class T>
  void ConstraintCandidateFace<T>::recompute(ConstraintSet<T>* c,
                                             CollisionContext<T>* ctx,
                                             OrientedVertex<T>& p,
                                             Vector4<T>& com,
                                             Quaternion<T>& rot){
    bool rigidFace   = Mesh::isRigidOrStatic(faceType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    vertexId = c->vertexId;

    if(vertexCollapsed){
      message("vertex %d collapsed", vertexId);
      std::cout << x0 << std::endl;
      std::cout << p << std::endl;
    }

    try{
      ctx->mesh->getTriangle(&t0, faceId, backFace);
      valid = true;
    }catch(Exception* e){
#ifndef DELASSUS_TEST
      warning("Singularity %s", e->getError().c_str());
      message("face id = %d", faceId);
      std::cout << t0 << std::endl;
      error("Singular face");
      getchar();
      delete e;
      valid = false;

      if(status == Enabled){
        disableConstraint(c, ctx, true);
      }
#endif
      return;
    }
    valid = true;

    ti = t0;
    tu = t0;

    comt0 = comtu = comti = Vector4<T>(0,0,0,0);
    rott0 = rottu = rotti = Quaternion<T>(0,0,0,1);

    ctx->mesh->setCurrentEdge(ctx->mesh->halfFaces[faceId].half_edge);
    int vertex = ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(ctx->mesh->vertices[vertex].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertex];

      comt0 = comtu = comti = ctx->mesh->centerOfMass[objectId];
      rott0 = rottu = rotti = ctx->mesh->orientations[objectId];
    }

    x0 = p;
    xi = p;

    comx0 = comxi = com;
    rotx0 = rotxi = rot;

    baryi = bary2 = t0.getBarycentricCoordinates(p.getVertex());

    intersection0 = intersectionc =
      x0.planeIntersection(t0.v[0], t0.n, indices, rigidFace || rigidVertex, false);

    //orientation = FrontFaceCollision;


    //if(orientation == FrontFaceCollision){
      distance0 = distancei = distancec =
        t0.getPlaneDistance(p.getVertex()) - (T)(DISTANCE_EPS*0.0);

      //}else if(orientation == BackFaceCollision){
      //distance0 = distancei = distancec =
      //  t0.getPlaneDistance(p) + (T)DISTANCE_EPS;

      //}


#ifndef DELASSUS_TEST
    if(vertexCollapsed){
      error("vertex is in a collapsed state, i.e., the cone has self intersections");
    }
#endif

    updated = false;

    forced = false;

    if(status == Enabled){
      /*Check if p projects on the triangle, if not, disable the
        constraint*/

      if(!forced && !t0.barycentricInTriangleDist(baryi, (T)DISTANCE_EPS)
         /*&& distancec > (T)DISTANCE_EPS/(T)2.0*/){
        disableConstraint(c, ctx, true);
        return;
      }

      if(distancec > (T)DISTANCE_EPS*(T)2){
        //disableConstraint(c, ctx, true);
        //return;
      }

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

      //cc->averageFrictionMagnitude.clear();
      //cc->averageFrictionDirection.clear();

      if(cc->status == Inactive){
        if(distancec >  (T)DISTANCE_EPSILON+DISTANCE_TOL){
          /*Constraint is inactive and there is no collision -> disable.*/

          disableConstraint(c, ctx, true);
          return;
        }else{
          /*Constraint was disabled, but there is a collision -> reenable*/
          //cc->status = Active;
        }
      }

      Vector4<T> weights = baryi;

#ifdef WEIGHT_CLAMPING
      //Vector4<T> weights = baryi;
      if(forced){
        weights = t0.edgeCrossedBarycentricCoordinates(t0.getCoordinates(baryi));//, WEIGHT_MIN, WEIGHT_MAX);
        //weights = t0.getBarycentricCoordinates(t0.getCoordinates(baryi));
      }else{
        weights = t0.getBarycentricCoordinates(t0.getCoordinates(baryi));
      }
#else
      weights = baryi;

      if(forced){
        //weights = t0.edgeCrossedBarycentricCoordinates(t0.getCoordinates(baryi));//, WEIGHT_MIN, WEIGHT_MAX);
        //weights = t0.getBarycentricCoordinates(t0.getCoordinates(baryi));
      }
#endif

      cc->setMu((T)MU);
      if(backFace || forced){
        cc->setMu((T)MU*(T)0.0);
      }

#ifdef FRICTION_CONE

      cc->cumulativeVector = cc->kineticVector * (T)60;
      //cc->cumulativeVector /= (T)1.1;

      if(cc->cumulativeVector.length() > 1e-6){
        cc->cumulativeVector.normalize();
      }

      if(cc->frictionStatus[0] != Kinetic){
        cc->cumulativeVector.set(0,0,0,0);
        cc->kineticVector.set(0,0,0,0);
        cc->acceptedKineticVector.set(0,0,0,0);
      }


#endif

      int indices[3];

      ctx->mesh->getTriangleIndices(faceId, indices, backFace);

      T dt = ctx->getDT();

      bool bodyFace    = true;
      bool bodyVertex  = true;
      bool rigidFace   = false;
      bool rigidVertex = false;

      for(int i=0;i<3;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyFace = false;
        }
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
      }

      if(Mesh::isStatic(ctx->mesh->vertices[c->vertexId].type)){
        bodyVertex = false;
      }
      if(Mesh::isRigid(ctx->mesh->vertices[c->vertexId].type)){
        rigidVertex = true;
      }

      T totalDistance =
        (t0.getSignedDistance(p.getVertex()) -
         t0.getSignedDistance(t0.v[0]) * weights[0] -
         t0.getSignedDistance(t0.v[1]) * weights[1] -
         t0.getSignedDistance(t0.v[2]) * weights[2]) +

        -(T)DISTANCE_EPS - (T)EPS;

      /*Compute tangent vectors*/
      Vector4<T> t1, t2;
      Vector4<T> rv, rf;

#ifdef FRICTION_CONE
      int cindex = 0;
      if(bodyVertex){
        if(rigidVertex){
          t1 += cc->getRow(1, 0);
          t2 += cc->getRow(2, 0);
          cindex = 2;

          rv  = xi.getVertex() - comxi;
        }else{
          t1 += cc->getRow(1, 0);
          t2 += cc->getRow(2, 0);
          cindex = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          t1 -= cc->getRow(1, cindex + 0);
          t2 -= cc->getRow(2, cindex + 0);

          Vector4<T> txi = ti.getCoordinates(baryi);
          rf = txi - comti;
        }else{
          t1 -= cc->getRow(1, cindex + 0);
          t1 -= cc->getRow(1, cindex + 1);
          t1 -= cc->getRow(1, cindex + 2);

          t2 -= cc->getRow(2, cindex + 0);
          t2 -= cc->getRow(2, cindex + 1);
          t2 -= cc->getRow(2, cindex + 2);
        }
      }

      t1 *= -1;
      t2 *= -1;

      Vector4<T> tt1, tt2;
      tt2 = cross(t1,  ti.n).normalize();
      tt1 = cross(tt2, ti.n).normalize();

      t1 = -tt1;
      t2 = -tt2;

      cc->tangent1 = t1;
      cc->tangent2 = t2;
      cc->normal   = ti.n;
#else

      Vector4<T> vel(0,0,0,0);
      Vector<T>* vx = ctx->getSolver()->getx();

      /*Compute current relative velocity*/
      if(bodyVertex){
        int vel_index = ctx->mesh->vertices[c->vertexId].vel_index;

        vel[0] = -(*vx)[vel_index+0];
        vel[1] = -(*vx)[vel_index+1];
        vel[2] = -(*vx)[vel_index+2];

        Vector4<T> ang_vel;

        if(rigidVertex){
          ang_vel[0] = (*vx)[vel_index+3];
          ang_vel[1] = (*vx)[vel_index+4];
          ang_vel[2] = (*vx)[vel_index+5];

          /*Compute angular displacement*/
          rv  = xi.getVertex() - comxi;
          ang_vel = cross(ang_vel, rv);

          vel -= ang_vel;
        }
      }

      if(bodyFace){
        if(rigidFace){
          int vel_index = ctx->mesh->vertices[indices[0]].vel_index;
          vel[0] += (*vx)[vel_index+0];
          vel[1] += (*vx)[vel_index+1];
          vel[2] += (*vx)[vel_index+2];

          Vector4<T> ang_vel;

          ang_vel[0] = (*vx)[vel_index+3];
          ang_vel[1] = (*vx)[vel_index+4];
          ang_vel[2] = (*vx)[vel_index+5];

          Vector4<T> txi = ti.getCoordinates(baryi);
          rf = txi - comti;

          ang_vel = cross(ang_vel, rf);

          vel += ang_vel;
        }else{
          for(int l=0;l<3;l++){
            int vel_index = ctx->mesh->vertices[indices[l]].vel_index;

            vel[0] += (*vx)[vel_index+0]*weights[l];
            vel[1] += (*vx)[vel_index+1]*weights[l];
            vel[2] += (*vx)[vel_index+2]*weights[l];
          }
        }
      }

      t1 = t2.set(0,0,0,0);

      int cindex = 0;
      if(bodyVertex){
        if(rigidVertex){
          t1 += cc->getRow(1, 0);
          t2 += cc->getRow(2, 0);
          cindex = 2;
        }else{
          t1 += cc->getRow(1, 0);
          t2 += cc->getRow(2, 0);
          cindex = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          t1 -= cc->getRow(1, cindex + 0);
          t2 -= cc->getRow(2, cindex + 0);
        }else{
          t1 -= cc->getRow(1, cindex + 0);
          t1 -= cc->getRow(1, cindex + 1);
          t1 -= cc->getRow(1, cindex + 2);

          t2 -= cc->getRow(2, cindex + 0);
          t2 -= cc->getRow(2, cindex + 1);
          t2 -= cc->getRow(2, cindex + 2);
        }
      }
      t1 *= -1;
      t2 *= -1;

      t1.normalize();
      t2.normalize();

      Vector4<T> vn = cross(vel, ti.n);

      /*Re-align with velocity and/or normal*/
      if(vn.length() < 1e-6){
        /*Fixed point, only align with normal*/
        Vector4<T> tt1, tt2;
        tt2 = cross(t1, ti.n).normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
        }

        tt1 = cross(t2, ti.n).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
        }

        t1 = tt1;
        t2 = tt2;
      }else{
        Vector4<T> tt1, tt2;
        tt2 = vn.normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
        }

        tt1 = cross(tt2, ti.n).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
        }
        t1 = tt1;
        t2 = tt2;
      }
#endif

#ifndef FRICTION_CONE
      START_DEBUG;
      message("after realign, %10.10e, %10.10e", dot(t1, vn), dot(t2,vn));
      std::cout << t1;
      std::cout << t2;
      END_DEBUG;
#endif

      int cidx = 0;

      Vector4<T> normal = ti.n;

      if(bodyVertex){
        int velIndex = ctx->mesh->vertices[c->vertexId].vel_index/3;

        if(rigidVertex){
          cc->setRow(-normal*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(    -t1*dt,             0, 1, 0, velIndex);
          cc->setRow(    -t2*dt,             0, 2, 0, velIndex);

          cc->setRow(-cross(rv, normal)*dt, totalDistance, 0, 1, velIndex+1);
          cc->setRow(    -cross(rv, t1)*dt,             0, 1, 1, velIndex+1);
          cc->setRow(    -cross(rv, t2)*dt,             0, 2, 1, velIndex+1);

          cidx = 2;
        }else{
          cc->setRow(-normal*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(    -t1*dt,             0, 1, 0, velIndex);
          cc->setRow(    -t2*dt,             0, 2, 0, velIndex);

          cidx = 1;
        }
      }
      if(bodyFace){
        if(rigidFace){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(normal*dt, totalDistance, 0, cidx, velIndex);
          cc->setRow(    t1*dt,             0, 1, cidx, velIndex);
          cc->setRow(    t2*dt,             0, 2, cidx, velIndex);

          cc->setRow(cross(rf, normal)*dt, totalDistance, 0, cidx+1, velIndex+1);
          cc->setRow(    cross(rf, t1)*dt,             0, 1, cidx+1, velIndex+1);
          cc->setRow(    cross(rf, t2)*dt,             0, 2, cidx+1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(normal*weights[0]*dt, totalDistance, 0, cidx, velIndex);
          cc->setRow(    t1*weights[0]*dt,             0, 1, cidx, velIndex);
          cc->setRow(    t2*weights[0]*dt,             0, 2, cidx, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(normal*weights[1]*dt, totalDistance, 0, cidx+1, velIndex);
          cc->setRow(    t1*weights[1]*dt,             0, 1, cidx+1, velIndex);
          cc->setRow(    t2*weights[1]*dt,             0, 2, cidx+1, velIndex);

          velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(normal*weights[2]*dt, totalDistance, 0, cidx+2, velIndex);
          cc->setRow(    t1*weights[2]*dt,             0, 1, cidx+2, velIndex);
          cc->setRow(    t2*weights[2]*dt,             0, 2, cidx+2, velIndex);
        }
      }

      /*Set transposed values*/
      cc->init(ctx->getMatrix());
      cc->update(*ctx->getSolver()->getb(),
                 *ctx->getSolver()->getx(),
                 *ctx->getSolver()->getb2());

    }
  }

  template<class T>
  bool ConstraintCandidateFace<T>::disableConstraint(ConstraintSet<T>* c,
                                                     CollisionContext<T>* ctx,
                                                     bool force,
                                                     bool keepNeighbor){
    forced = false;
    if(status == Enabled){
      /*Check if we can disable this constraint, i.e., its
        corresponding multiplier must then also be negative*/
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

#ifdef ACTIVE_CONSTRAINTS
      cc->cacheMultipliers(cachedMultipliers, *ctx->getSolver()->getx());
#endif

      bool active = false;

      /*Cache corresponding tangent vectors*/
      bool rigidFace   = false;
      bool rigidVertex = false;
      bool bodyFace    = false;
      bool bodyVertex  = false;

      for(int i=0;i<3;i++){
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
        if(Mesh::isNotStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyFace = true;
        }
      }

      if(Mesh::isRigid(ctx->mesh->vertices[c->vertexId].type)){
        rigidVertex = true;
      }

      if(Mesh::isNotStatic(ctx->mesh->vertices[c->vertexId].type)){
        bodyVertex = true;
      }

      /*Save tangentvectors*/
      tangentReference[0] = tangentReference[1].set(0,0,0,0);

      int cindex = 0;
      if(bodyVertex){
        if(rigidFace){
          tangentReference[0] += cc->getRow(1, 0);
          tangentReference[1] += cc->getRow(2, 0);
          cindex = 2;
        }else{
          tangentReference[0] += cc->getRow(1, 0);
          tangentReference[1] += cc->getRow(2, 0);
          cindex = 1;
        }
      }

      if(bodyFace){
        if(rigidVertex){
          tangentReference[0] -= cc->getRow(1, cindex + 0);
          tangentReference[1] -= cc->getRow(2, cindex + 0);
        }else{
          tangentReference[0] -= cc->getRow(1, cindex + 0);
          tangentReference[0] -= cc->getRow(1, cindex + 1);
          tangentReference[0] -= cc->getRow(1, cindex + 2);

          tangentReference[1] -= cc->getRow(2, cindex + 0);
          tangentReference[1] -= cc->getRow(2, cindex + 1);
          tangentReference[1] -= cc->getRow(2, cindex + 2);
        }
      }

      tangentReference[0].normalize();
      tangentReference[1].normalize();

      tangentialDefined = true;

      if(!force){
        active = false;
      }else{
        /*Force removal*/
        active = false;
      }

      if(active == false){
        /*Direct evaluation of the constraint gives the deactive
          status, so deactivate and disable*/
        ctx->getSolver()->removeConstraint(enabledId);
        status = Disabled;
        delete cc;
        cc = 0;

        return true;
      }
    }else{
      /*Constraint was already disabled*/
    }
    return false;
  }

  template<class T>
  void ConstraintCandidateFace<T>::showCandidateState(OrientedVertex<T>& xu,
                                                      CollisionContext<T>* ctx){
    message("\n\nShow triangle candidate state %d - %d", faceId, vertexId);

    message("x0 t0");
    std::cout << x0 << std::endl;
    std::cout << t0 << std::endl;

    message("xu tu");
    std::cout << xu << std::endl;
    std::cout << tu << std::endl;

    message("xi ti");
    std::cout << xi << std::endl;
    std::cout << ti << std::endl;

    message("distance0 = %10.10e", distance0);
    message("distancec = %10.10e", distancec);

    message("intersection0 = %d", intersection0);
    message("intersectionc = %d", intersectionc);

    message("forced = %d", forced);

    message("indiced      = %d, %d, %d", indices[0], indices[1], indices[2]);
    message("edge indiced = %d, %d, %d", edgeIndices[0], edgeIndices[1], edgeIndices[2]);

    message("enabledId = %d", enabledId);

    if(status == Enabled){
      message("Constraint enabled");
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

      cc->print(std::cout);
    }
  }

  template<class T>
  void ConstraintCandidateFace<T>::updateInitialState(const OrientedVertex<T>& v,
                                                      const Vector4<T>& com,
                                                      const Quaternion<T>& rot){

    bool rigidFace   = Mesh::isRigidOrStatic(faceType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    x0 = v;
    comx0 = com;
    rotx0 = rot;

    intersection0 =
      x0.planeIntersection(t0.v[0], t0.n, indices, rigidFace || rigidVertex, false);

    //orientation = FrontFaceCollision;


    //if(orientation == FrontFaceCollision){
    distance0 =
      t0.getPlaneDistance(x0.getVertex()) - (T)(DISTANCE_EPS*0.0);
  }

  template class ConstraintCandidateFace<float>;
  template class ConstraintCandidateFace<double>;
}
