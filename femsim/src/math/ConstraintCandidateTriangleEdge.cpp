/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/ConstraintCandidateTriangleEdge.hpp"
#include "math/constraints/ContactConstraint.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"
#include "math/ConstraintSetTriangleEdge.hpp"

#define EPS DISTANCE_EPSILON*(T)2.0
#define DISTANCE_EPS SAFETY_DISTANCE

#define MIN_DISTANCE 1e-3

#define DEBUG_UPDATE
//#undef DEBUG_UPDATE

namespace CGF{

  template<class T>
  ConstraintCandidateTriangleEdge<T>::ConstraintCandidateTriangleEdge(int eId,
                                                                      int vId){
    edgeId = eId;
    vertexId = vId;

    status = Disabled;

    distance0 = distancec = (T)0.0;

    tangentialDefined = false;
  }

  /*p = current, x = start*/
  template<class T>
  bool ConstraintCandidateTriangleEdge<T>::checkCollision(const Edge<T>& p,
                                                          const Vector4<T>& com,
                                                          const Quaternion<T>& rot,
                                                          T* dist){
    int colCode = -1;

    bool rigidEdge   = Mesh::isRigidOrStatic(edgeType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    T eps = (T)DISTANCE_EPS3 + (T)MIN_DISTANCE;

    //warning("check collision edge = %d, vertex = %d", edgeId, vertexId);

    try{
      if(!rigidEdge && !rigidVertex){
        colCode = intersection(p, p0, vu, v0, colPoint, eps,
                               &baryi, &pi, &vi, (T)BARY_EPS, 1,
                               dist);
        //*dist = p0.getSignedDistance(e0, 0, 0, 0, 0, &referencedvPE);
      }else{
        T root = 0;

        colCode = rigidIntersection(p,  com,   rot,
                                    p0, comp0, rotp0,
                                    vu, comvu, rotvu,
                                    v0, comv0, rotv0,
                                    colPoint, eps,
                                    &baryi,
                                    &pi, &vi, (T)BARY_EPS, 1,
                                    rigidEdge, rigidVertex, &root);

        if(colCode == 1){
          compi = comp0 * ((T)1.0 - root) + com   * root;
          comvi = comv0 * ((T)1.0 - root) + comvu * root;

          rotpi = slerp(rotp0, rot,   root);
          rotvi = slerp(rotv0, rotvu, root);
        }
        if(dist) *dist = root;//p0.getSignedDistance(e0, 0, 0, 0, 0, &referencedvPE);
      }
    }catch(Exception* e){
      delete e;
      return false;
    }

    if(colCode == -1){
      return false;
    }


    /*Distance between ei and pi is most likely zero, provide
      referencevector for correction*/


    if(colCode == 1){
      /*An intersection can be invalid due to flipped normals*/

      /*Check if edges are in a valid configuration*/

      //Vector4<T> referencedvEPTemp =  dv;
      //Vector4<T> referencedvPETemp = -dv;

      /*Check distances*/
      ////ei.infiniteProjections(pi, &x1, &x2, 0, 0, &parallel);
      contactNormal = pi.getNormalToVertex(vi);

      T dist = (pi.getCoordinates(baryi) - vi).length();

      if(dist - (T)MIN_DISTANCE > 2e-8){
        //return false;
      }

      //pi = p0;
      //vi = v0;

      return true; /*Valid*/
    }
    return false; /*No coliision*/
  }

  /*Evaluate signed distance*/
  template<class T>
  bool ConstraintCandidateTriangleEdge<T>::evaluate(const Edge<T>& edge,
                                                    const Vector4<T>& com,
                                                    const Quaternion<T>& rot,
                                                    Vector4<T>* bb,
                                                    bool* updated){

    //contactNormal = edge.getNormalToVertex(vu);
    distancec = edge.getPlaneDistance(vu, contactNormal) - (T)MIN_DISTANCE;
    //return false;
    /*If there is no intersection, check if they could have intersected*/

    //warning("edge %d vertex distance = %10.10e", edgeId, distancec);

    if(updated){
      *updated = true;
    }

    /*Compute current distance, used for final check*/
    //referencedvEP = eu.getOutwardNormal(edge, &referencedvEP);

    //bool degenerate = false;
    //distancec = eu.getSignedDistance(edge, 0, 0, 0, 0, &degenerate, &referencedvEP);//

    distancec -= (T)DISTANCE_EPS3;

    if(status != Enabled){
      if(distancec < (T)0.0){

        return true;
      }
    }else{

    }
    return false;
  }

  /*Update position of edge*/
  template<class T>
  void ConstraintCandidateTriangleEdge<T>::updateGeometry(CollisionContext<T>* ctx, ConstraintSetTriangleEdge<T>* c, const Edge<T>& p, const Edge<T>& s){
    vu = ctx->mesh->vertices[vertexId].displCoord;

    if(Mesh::isRigid(ctx->mesh->vertices[vertexId].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertexId];

      comvu = ctx->mesh->centerOfMassDispl[objectId];
      rotvu = ctx->mesh->orientationsDispl[objectId];
    }
  }

  template<class T>
  bool ConstraintCandidateTriangleEdge<T>::disableConstraint(ConstraintSetTriangleEdge<T>* c,
                                                             CollisionContext<T>* ctx,
                                                             bool force){
    if(status == Enabled){
      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);

#ifdef ACTIVE_CONSTRAINTS
      cc->cacheMultipliers(cachedMultipliers, *ctx->getSolver()->getx());
#endif

      bool active = false;

      status = Disabled;

      cgfassert(enabledId == cc->id);

      /*Cache corresponding tangent vectors for future use*/
      tangentReference[0] = tangentReference[1].set(0,0,0,0);

      int indices[3];
      ctx->mesh->getEdgeIndices(c->edgeId, indices);
      indices[2] = vertexId;

      bool rigidEdge   = false;
      bool rigidVertex = false;

      for(int i=0;i<2;i++){
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge = true;
        }
      }

      for(int i=2;i<3;i++){
        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidVertex = true;
        }
      }

      if(rigidEdge){
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[1] += cc->getRow(2,0);
      }else{
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[0] += cc->getRow(1,1);
        tangentReference[1] += cc->getRow(2,0);
        tangentReference[1] += cc->getRow(2,1);
      }

      if(rigidVertex){
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[1] -= cc->getRow(2,2);
      }else{
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[0] -= cc->getRow(1,3);
        tangentReference[1] -= cc->getRow(2,2);
        tangentReference[1] -= cc->getRow(2,3);
      }

      tangentReference[0].normalize();
      tangentReference[1].normalize();

      tangentialDefined = true;

      if(!force){
        active = false;
      }else{
        /*Force removal*/
        active = false;
      }

      if(active == false){
        ctx->getSolver()->removeConstraint(enabledId);
        status = Disabled;

        delete cc;

        return true;
      }
    }else{
      /*Constraint was already disabled*/
    }
    return false;
  }


  template<class T>
  void ConstraintCandidateTriangleEdge<T>::updateConstraint(ConstraintSetTriangleEdge<T>*c,
                                                            CollisionContext<T>* ctx,
                                                            const Edge<T>& x,
                                                            const Edge<T>& p,
                                                            const Vector4<T>& com,
                                                            const Quaternion<T>& rot,
                                                            bool* skipped,
                                                            EvalStats& stats){
    ContactConstraint<2, T>* cc =
      (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

#ifdef DEBUG_UPDATE
    warning("updateConstraint");
    std::cerr << v0 << std::endl;

    std::cerr << vu << std::endl;
    std::cerr << vi << std::endl;

    std::cerr << p0 << std::endl;

    std::cerr << p << std::endl;
    std::cerr << pi << std::endl;

    std::cerr << x << std::endl;
#endif


    //error("Update constraints");

    Vector4<T> contactNormalOld = pi.getNormalToVertex(vi);

#ifdef DEBUG_UPDATE
    std::cerr << contactNormalOld << std::endl;
#endif

    bool rigidEdge   = Mesh::isRigidOrStatic(edgeType);
    bool rigidVertex = Mesh::isRigidOrStatic(vertexType);

    T initialWeight = (T)3.0;

    /*Find a proper configuration for the new interpolated edges in
      which the change in the contact normal is not extremely
      large. If the dotproduct between the new and current
      contactnormal is more than 0.25, the current edges are
      accepted. If not, the initial weight is reduced such that the
      new configuration changes less compared to the used
      configuration. This continues until a proper set of edges are
      found for which their contact normal does not change much
      compared to the previous case.

      When edges are short, a tiny change in a vertex position can
      result in a large change in the contactnormal. Using this
      procedure the method is prevented to make too large steps toward
      the unknown solution.*/

    Vector4<T> vii;
    Edge<T> pii;

    Vector4<T> weights;

    T weight1 = (T)1.0;
    T weight2 = (T)0.0;

#ifdef DEBUG_UPDATE
    cc->print(std::cerr);
#endif

    while(true){
      weight1 = (T)1.0 - (T)1.0/(T)initialWeight;
      weight2 = (T)1.0 - weight1;

      Vector4<T> vii2;
      Edge<T>    pii2;

      if(rigidEdge){
        Vector4<T>    newComPi = weight1 * compi + weight2 * com;
        Quaternion<T> newRotPi = slerp(rotpi, rot, weight2);

        Vector4<T> v11 = newComPi + newRotPi.rotate(p0.vertex(0)     - comp0);
        Vector4<T> v12 = newComPi + newRotPi.rotate(p0.vertex(1)     - comp0);
        Vector4<T> v13 = newComPi + newRotPi.rotate(p0.faceVertex(0) - comp0);
        Vector4<T> v14 = newComPi + newRotPi.rotate(p0.faceVertex(1) - comp0);

        pii2.set(v11, v12, v13, v14);
      }else{
        pii2 = interpolateEdges(pi,  p, (T)1, (T)0, weight1);
      }

      if(rigidVertex){
        Vector4<T>    newComVi = weight1 * comvi + weight2 * comvu;
        Quaternion<T> newRotVi = slerp(rotvi, rotvu, weight2);

        Vector4<T> v11 = newComVi + newRotVi.rotate(v0  - comv0);

        vii2 = v11;

      }else{
        //eii2 = interpolateEdges(ei, eu, (T)1, (T)0, weight1);
        //error("update me");
        vii2 = linterp(weight1, &vi, &vu, (T)1.0, (T)0.0);
      }

      /*This can and will go wrong if the change in the contactnormal
        changes significantly.*/
      contactNormal = pii2.getNormalToVertex(vii2);
      weights = pii2.getBarycentricCoordinates(vii2);

      if(dot(contactNormal, contactNormalOld) > 0.95){
        /*The new configuration is reasonable, accept this one.*/
        vii = vii2;
        pii = pii2;
        break;
      }

      /*The current configuration is has a large change compared to
        the previos configuration, choose one closer to the previous
        approximation.*/
      initialWeight *= (T)2.0;
    }

    if(dot(contactNormalOld, contactNormal) < (T)0.9 ||
       (baryi - weights).length() > 1e-4){
      //if(true){
      /*The contact normal has changed significantly, we need to
        update the constraint and so we need to update the
        preconditioner and residual vector.*/
      warning("Change in contact normal detected");
#ifdef DEBUG_UPDATE
      warning("p0v0");
      std::cerr << p0 << std::endl;
      std::cerr << v0 << std::endl;

      warning("pivi");
      std::cerr << pi << std::endl;
      std::cerr << vi << std::endl;

      warning("piivii");
      std::cerr << pii << std::endl;
      std::cerr << vii << std::endl;
#endif

      vi = vii;
      pi = pii;

      //DO NOT UPDATE Collisionpoint
      baryi = weights;

      if(rigidEdge){
        compi = compi * weight1 + com   * weight2;
        rotpi = slerp(rotpi, rot,   weight2);
      }

      if(rigidVertex){
        comvi = comvi * weight1 + comvu * weight2;
        rotvi = slerp(rotvi, rotvu, weight2);
      }

      int indices[3];
      ctx->mesh->getEdgeIndices(edgeId, indices);
      indices[2] = vertexId;

      T dt = ctx->getDT();

#ifdef DEBUG_UPDATE
      warning("weights of point");
      std::cerr << weights << std::endl;
#endif

#if 1
      /*Check if new edges project on each other*/
      if(!pi.barycentricOnEdgeBand(weights, (T)EPS * (T)0.0)){
        /*Could be a source of potential errors!!*/
        disableConstraint(c, ctx, true);
        *skipped = true;
        return;
      }
#endif

      if(cc->getStatus() != Active){
        disableConstraint(c, ctx, true);
        *skipped = true;
        return;
      }

      /*Extract tangent vectors*/
      Vector4<T> t1, t2;

      bool bodyEdge    = true;
      bool bodyVertex  = true;
      bool rigidEdge   = false;
      bool rigidVertex = false;

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyEdge = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge = true;
        }
      }

      for(int i=2;i<3;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyVertex = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidVertex = true;
        }
      }

#ifdef DEBUG_UPDATE
      warning("coordinates");
      std::cerr << pii.getCoordinates(weights) << std::endl;
#endif

#ifdef WEIGHT_CLAMPING
      //weights = p0.clampWeights(weights, WEIGHT_MIN, WEIGHT_MAX);
#else
      weights =  weights;
#endif

#ifdef DEBUG_UPDATE
      warning("weights of point");
      std::cerr << weights << std::endl;

      warning("coordinates");
      std::cerr << pii.getCoordinates(weights) << std::endl;
#endif

      Vector4<T> re1, rv2;

      t1 = t2 *= 0.0;

      if(bodyEdge){
        if(rigidEdge){
          re1  = pii.getCoordinates(weights) - compi;
          t1 = cc->getRow(1, 0);
          t2 = cc->getRow(2, 0);
        }else{
          t1 = cc->getRow(1, 0) + cc->getRow(1,1);
          t2 = cc->getRow(2, 0) + cc->getRow(2,1);
        }
      }

      if(bodyVertex){
        t1 = t2 *= 0.0;
        if(rigidVertex){
          rv2  = vii - comvi;
          t1 = -cc->getRow(1, 2);
          t2 = -cc->getRow(2, 2);
        }else{
          t1 = -cc->getRow(1, 2) - cc->getRow(1,3);
          t2 = -cc->getRow(2, 2) - cc->getRow(2,3);
        }
      }

#ifdef DEBUG_UPDATE
      warning("t1");
      std::cerr << t1 << std::endl;
      warning("t2");
      std::cerr << t2 << std::endl;
#endif

      t1.normalize();
      t2.normalize();

      /*Re-align tangent vectors with contact normal*/
      Vector4<T> tt1, tt2;
      tt2 = cross(t1, contactNormal).normalize();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      tt1 = cross(t2, contactNormal).normalize();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

#ifdef DEBUG_UPDATE
      warning("t1");
      std::cerr<< t1 << std::endl;
      warning("t2");
      std::cerr << t2 << std::endl;
#endif

      //cc->status = Inactive;
      //cc->frictionStatus[0] = Kinetic;
      //cc->frictionStatus[1] = Kinetic;

      /*Compute normal and tangential distances*/

      T d5 = pi.getPlaneDistance(p0.vertex(0), contactNormal)*baryi[0];
      T d6 = pi.getPlaneDistance(p0.vertex(1), contactNormal)*baryi[1];
      T d7 = pi.getPlaneDistance(v0,           contactNormal);
      //T d8 = ei.getPlaneDistance(e0.vertex(1), dv)*weights2[1];

      //d5 = dot(contactNormal, pi.vertex(0) - p0.vertex(0)) * weights[0];
      //d6 = dot(contactNormal, pi.vertex(1) - p0.vertex(1)) * weights[1];
      //d7 = dot(contactNormal, vi           - v0          );

      T totalDistance = (-d5-d6+d7) - (T)DISTANCE_EPS - (T)EPS - (T)MIN_DISTANCE;

#ifdef DEBUG_UPDATE
      warning("update constraint");
      warning("totaldistance = %10.10e", totalDistance);
      warning("distancec = %10.10e", distancec);
      warning("distance0 = %10.10e", distance0);
#endif

#if 0
      /*Measure the error between the desired and current normal
        distance of the constraint. Use this error distance to update
        the constraint. This errordistance is then weighted/averaged
        in order to move the current configuration closer to the
        desired solution*/
      T errorDist = (T)(DISTANCE_EPS+EPS + MIN_DISTANCE*0.0)*(T)1.0 - distancec;

      //if(offEdge){
      // totalDistance = (d5+d6-d7-d8) - offEdgeEps;
      //errorDist = offEdgeEps - distancec;
      //}

      //errorDist = Sign(errorDist)*Max(Abs(errorDist), (T)DISTANCE_EPS + (T)EPS);

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-3);

      totalDistance = (cc->getConstraintValue(0) + errorDist*(T)0.25);

      warning("errordist = %10.10e", errorDist);
      warning("totaldistance = %10.10e", totalDistance);
#endif

      /*T1*/
      d5 = dot(t1, pii.vertex(0) - p0.vertex(0)) * baryi[0];
      d6 = dot(t1, pii.vertex(1) - p0.vertex(1)) * baryi[1];
      d7 = dot(t1, vii           - v0          );
      //d8 = dot(t1, eii.vertex(1) - e0.vertex(1)) * weights2[1];

      T totalDistanceT1 = (d5+d6-d7);

      /*T2*/
      d5 = dot(t2, pii.vertex(0) - p0.vertex(0)) * baryi[0];
      d6 = dot(t2, pii.vertex(1) - p0.vertex(1)) * baryi[1];
      d7 = dot(t2, vii           - v0          );
      //d8 = dot(t2, eii.vertex(1) - e0.vertex(1)) * weights2[1];

      T totalDistanceT2 = (d5+d6-d7);

      //totalDistanceT1 = totalDistanceT2 = (T)0.0;

      //totalDistanceT1 += cc->c[1];
      //totalDistanceT2 += cc->c[2];

      //totalDistanceT1 /= (T)2.0;
      //totalDistanceT2 /= (T)2.0;

      //totalDistanceT1 = totalDistanceT2 = (T)0.0;

      //totalDistanceT1 = cc->getConstraintValue(1);
      //totalDistanceT2 = cc->getConstraintValue(2);

      /*Update constraint with new normals and tangential vectors and
        updated distances.*/
      if(bodyEdge){
        if(rigidEdge){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(contactNormal*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(           t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(           t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(cross(re1, contactNormal)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(           cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(           cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(contactNormal*baryi[0]*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(           t1*baryi[0]*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(           t2*baryi[0]*dt, totalDistanceT2, 2, 0, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(contactNormal*baryi[1]*dt, totalDistance,   0, 1, velIndex);
          cc->setRow(           t1*baryi[1]*dt, totalDistanceT1, 1, 1, velIndex);
          cc->setRow(           t2*baryi[1]*dt, totalDistanceT2, 2, 1, velIndex);
        }
      }
      if(bodyVertex){
        if(rigidVertex){
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-contactNormal*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(           -t1*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(           -t2*dt, totalDistanceT2, 2, 2, velIndex);

          cc->setRow(-cross(rv2, contactNormal)*dt, totalDistance,   0, 3, velIndex+1);
          cc->setRow(           -cross(rv2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
          cc->setRow(           -cross(rv2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-contactNormal*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(           -t1*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(           -t2*dt, totalDistanceT2, 2, 2, velIndex);
        }
      }
    }else{
      //contactNormal =
        /*ei.getOutwardNormal(pi, &referencedvEP, 0, 0,
          &weights2, &weights1);*/
        //ei.getOutwardNormal(pi, &referencedvEP);

      //bool parallel = false;
      //ei.infiniteProjections(pi, 0, 0, &weights2, &weights1, &parallel);

      //warning("check order of weights");

      //if(parallel){
      //  throw new DegenerateCaseException(__LINE__, __FILE__, "parallel edges after update");
      //}

      warning("no change in contact normal");
      warning("weights of point");
      std::cerr << weights << std::endl;

      //bool offEdge = false;


      //T offEdgeEps = (T)1e-8;

#if 1
      /*Check if new edges project on each other*/
      if(!pi.barycentricOnEdgeBand(weights, (T)EPS*0.0) ){
        /*Could be a source of potential errors!!*/
        disableConstraint(c, ctx, true);
        *skipped = true;
        //updateInactiveConstraint(c, ctx, x, p, com, rot);
        //return;

        //offEdge = true;
        //if(distancec > offEdgeEps){
          //*skipped = true;
        return;
        //}

      }
#endif
      //message("Edges1 = %10.10e", (eu.vertex(0) - eu.vertex(1)).length());
      //message("Edges2 = %10.10e", (p.vertex(0)  -  p.vertex(1)).length());
      //message("Normal error = %10.10e", dot(contactNormal, contactNormalOld));


      warning("bary");
      std::cerr << baryi << std::endl;

      /*The normal has not changed significantly, just update the
        constraint normal distance*/

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->mat->getConstraint(enabledId);

      if(cc->status != Active){
        /*Do not update inactive constraint this way*/
        //return;
      }

      /*Error wrt desired distance*/
      T errorDist = (T)(DISTANCE_EPS+EPS) - distancec;

      //errorDist = Sign(errorDist)*Max(Abs(errorDist*(T)0.45), (T)DISTANCE_EPS + (T)EPS);
      //errorDist = Sign(errorDist)*Max(Abs(errorDist), (T)DISTANCE_EPS + (T)EPS);

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-4);

      T totalDistance = cc->getConstraintValue(0) - (T)(errorDist*0.25);

      warning("update distance");
      warning("totaldistance = %10.10e", totalDistance);
      warning("distancec = %10.10e", distancec);
      warning("distance0 = %10.10e", distance0);
      warning("errdist = %10.10e", errorDist);


      cc->getConstraintValue(0) = totalDistance;
    }

    stats.n_geometry_check++;

    /*Update constraint*/
    cc->init(ctx->getMatrix());
    cc->update(*ctx->getSolver()->getb(),
               *ctx->getSolver()->getx(),
               *ctx->getSolver()->getb2());

    //cc->status = Active;
  }

  template<class T>
  void ConstraintCandidateTriangleEdge<T>::updateInitial(ConstraintSetTriangleEdge<T>*c,
                                                         CollisionContext<T>* ctx,
                                                         const Edge<T>& x,
                                                         const Edge<T>& p,
                                                         const Vector4<T>& com,
                                                         const Quaternion<T>& rot,
                                                         bool* skipped,
                                                         EvalStats& stats){
    Vector4<T> weights = pi.getBarycentricCoordinates(vi);

    v0 = vu;

    comv0 = comvu;
    rotv0 = rotvu;

    p0 = p;

    comp0 = com;
    rotp0 = rot;

    distance0 = distancec;
    //intersection0 = intersectionc;

    if(status == Enabled){
      /*Check if new edges project on each other*/
      if(!pi.barycentricOnEdgeBand(weights, (T)EPS*0.0) ){
        /*Could be a source of potential errors!!*/
        disableConstraint(c, ctx, true);
        *skipped = true;
      }
    }
  }

  template<class T>
  bool ConstraintCandidateTriangleEdge<T>::enableConstraint(ConstraintSetTriangleEdge<T>* c,
                                                            CollisionContext<T>* ctx,
                                                            Edge<T>& s,
                                                            Edge<T>& p){
    if(status == Enabled){
      return false;
    }else{
      ContactConstraint<2, T>* cc = new ContactConstraint<2, T>();
      cc->sourceType = 8;
      cc->sourceA = edgeId;
      cc->sourceB = vertexId;

      try{
        /*Compute barycentric coordinates on both edges*/
#ifdef WEIGHT_CLAMPING
        Vector4<T> weights =  baryi;
        //Vector4<T> weights1 = p0.clampWeights(baryi1, WEIGHT_MIN, WEIGHT_MAX);
        //Vector4<T> weights2 = e0.clampWeights(baryi2, WEIGHT_MIN, WEIGHT_MAX);
#else
        Vector4<T> weights =  baryi;
#endif
        cc->setMu((T)MU * 0.0);

        int indices[3];
        ctx->mesh->getEdgeIndices(edgeId, indices);
        //ctx->mesh->getEdgeIndices(   edgeId, indices + 2);
        indices[2] = vertexId;

        T dt = ctx->getDT();

        bool bodyEdge    = true;
        bool bodyVertex  = true;
        bool rigidEdge   = false;
        bool rigidVertex = false;

        for(int i=0;i<2;i++){
          if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
            bodyEdge = false;
          }

          if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
            rigidEdge = true;
          }
        }

        for(int i=2;i<3;i++){
          if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
            bodyVertex = false;
          }

          if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
            rigidVertex = true;
          }
        }

        T d5 = pi.getPlaneDistance(p0.vertex(0), contactNormal)*weights[0];
        T d6 = pi.getPlaneDistance(p0.vertex(1), contactNormal)*weights[1];
        T d7 = pi.getPlaneDistance(v0,           contactNormal);

        //d5 = dot(contactNormal, pi.vertex(0) - p0.vertex(0)) * weights[0];
        //d6 = dot(contactNormal, pi.vertex(1) - p0.vertex(1)) * weights[1];
        //d7 = dot(contactNormal, vi           - v0          );

        //T d8 = ei.getPlaneDistance(e0.vertex(1), dv)*weights2[1];

        T totalDistance = (-d5-d6+d7) - (T)DISTANCE_EPS - (T)EPS - (T)MIN_DISTANCE;


        if(Abs(totalDistance) > 1E-2){
          /*When a constraint is enabled and its corresponding
            distance is too large, there can be a problem.*/
          warning("dump data");
          std::cerr << s << std::endl;
          std::cerr << p << std::endl;
          std::cerr << v0 << std::endl;
          std::cerr << vu << std::endl;
          std::cerr << pi << std::endl;
          std::cerr << vi << std::endl;

          //warning("d0 = %10.10e", s.getSignedDistance(e0, 0, 0, 0, 0, &referencedvPE));
          //warning("dc = %10.10e", p.getSignedDistance(eu, 0, 0, 0, 0, &referencedvPE));

          PRINT(s);
          PRINT(p);
          //PRINT(e0);
          //PRINT(eu);
          std::cerr << weights << std::endl;

          warning("too large distance");


          warning("total distance = %10.10e", totalDistance);

        }

        Vector4<T> t1, t2;
        Vector4<T> re1, rv2;

#ifdef FRICTION_CONE
        if(tangentialDefined){
          t1 = -tangentReference[0];
          //t2 = -tangentReference[1];

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, contactNormal).normalize();
          //if(tt2 * t2 < 0){
          //tt2 *= -1;
          //}

          tt1 = cross(tt2, contactNormal).normalize();
          //if(tt1 * t1 < 0){
          //tt1 *= -1;
          //}

          t1 = tt1;
          t2 = tt2;

          if(bodyEdge){
            if(rigidEdge){
              re1  = pi.getCoordinates(baryi) - compi;
            }
          }

          if(bodyVertex){
            if(rigidVertex){
              rv2  = vi - comvi;
            }
          }
          cc->tangent1 = t1;
          cc->tangent2 = t2;
          cc->normal   = contactNormal;
        }else{
          Vector4<T> rnd(genRand<T>(), genRand<T>(), genRand<T>(), 0);

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;

          tt2 = cross(rnd, contactNormal).normalize();

          tt1 = cross(tt2, contactNormal).normalize();

          t1 = tt1;
          t2 = tt2;

          if(bodyEdge){
            if(rigidEdge){
              re1  = pi.getCoordinates(baryi) - compi;
            }
          }

          if(bodyVertex){
            if(rigidVertex){
              rv2  = vi - comvi;
            }
          }

          cc->tangent1 = t1;
          cc->tangent2 = t2;
          cc->normal   = contactNormal;
        }
#else
#ifdef ACTIVE_CONSTRAINTS
        /*In case of ICA, all constraints are removed and newly
          added. Using old tangent vectors wihout re-aligning them is
          not correct.*/
        //tangentialDefined = false;
#endif
        if(tangentialDefined){
          t1 = -tangentReference[0];
          t2 = -tangentReference[1];

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, contactNormal).normalize();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(t2, contactNormal).normalize();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }

          t1 = tt1;
          t2 = tt2;

          if(rigidEdge){
            re1  = pi.getCoordinates(baryi) - compi;
          }

          if(rigidVertex){
            rv2  = vi - comvi;
          }
        }else{
          Vector4<T> vel(0,0,0,0);
          Vector<T>* vx = ctx->getSolver()->getx();

          if(bodyEdge){
            if(rigidEdge){
              int vel_index = ctx->mesh->vertices[indices[0]].vel_index;
              cgfassert(vel_index != -1);

              vel[0] += (*vx)[vel_index+0];
              vel[1] += (*vx)[vel_index+1];
              vel[2] += (*vx)[vel_index+2];

              Vector4<T> ang_vel;

              ang_vel[0] = (*vx)[vel_index+3];
              ang_vel[1] = (*vx)[vel_index+4];
              ang_vel[2] = (*vx)[vel_index+5];

              /*Compute angular displacement*/
              re1  = pi.getCoordinates(baryi) - compi;
              ang_vel = cross(ang_vel, re1);

              vel += ang_vel;
            }else{
              for(int l=0;l<2;l++){
                int vel_index = ctx->mesh->vertices[indices[l+0]].vel_index;

                cgfassert(vel_index != -1);

                vel[0] += (*vx)[vel_index+0] * weights[l];
                vel[1] += (*vx)[vel_index+1] * weights[l];
                vel[2] += (*vx)[vel_index+2] * weights[l];
              }
            }
          }

          if(bodyVertex){
            if(rigidVertex){
              int vel_index = ctx->mesh->vertices[indices[2]].vel_index;

              cgfassert(vel_index != -1);

              vel[0] -= (*vx)[vel_index+0];
              vel[1] -= (*vx)[vel_index+1];
              vel[2] -= (*vx)[vel_index+2];

              Vector4<T> ang_vel;

              ang_vel[0] = (*vx)[vel_index+3];
              ang_vel[1] = (*vx)[vel_index+4];
              ang_vel[2] = (*vx)[vel_index+5];

              /*Compute angular displacement*/

              rv2  = vi - comvi;
              ang_vel = cross(ang_vel, rv2);

              vel -= ang_vel;
            }else{
              for(int l=0;l<1;l++){
                int vel_index = ctx->mesh->vertices[indices[l+2]].vel_index;

                cgfassert(vel_index != -1);

                vel[0] -= (*vx)[vel_index+0];
                vel[1] -= (*vx)[vel_index+1];
                vel[2] -= (*vx)[vel_index+2];
              }
            }
          }

          Vector4<T> vn = cross(vel, contactNormal);

          /*Use velocity to align tangent vectors*/
          if(vn.length() < 1e-6){
            /*Choose some arbitrary direction*/
            Vector4<T> vec(1, 1, 1, 0);
            Vector4<T> vn2 = cross(vec, contactNormal);

            if(vn2.length() < 1e-6){
              vec.set(-1, 1, 1, 0);
              vn2 = cross(vec, contactNormal);
            }

            t2 = vn2.normalize();
            t1 = cross(t2, contactNormal).normalize();
          }else{
            t2 = vn.normalize();
            t1 = cross(t2, contactNormal).normalize();
          }
        }
#endif

        d5 = dot(t1, pi.vertex(0) - p0.vertex(0)) * weights[0];
        d6 = dot(t1, pi.vertex(1) - p0.vertex(1)) * weights[1];
        d7 = dot(t1, vi           - v0          );
        //d8 = dot(t1, ei.vertex(1) - e0.vertex(1)) * weights2[1];

        PRINT(d5);
        PRINT(d6);
        PRINT(d7);
        //PRINT(d8);

        T totalDistanceT1 = (d5+d6-d7);

        PRINT(totalDistanceT1);

        d5 = dot(t2, pi.vertex(0) - p0.vertex(0)) * weights[0];
        d6 = dot(t2, pi.vertex(1) - p0.vertex(1)) * weights[1];
        d7 = dot(t2, vi           - v0          );
        //d8 = dot(t2, ei.vertex(1) - e0.vertex(1)) * weights2[1];

        PRINT(d5);
        PRINT(d6);
        PRINT(d7);
        //PRINT(d8);

        T totalDistanceT2 = (d5+d6-d7);


        //totalDistanceT1 = totalDistanceT2 = 0;

        PRINT(totalDistanceT2);

#ifdef FRICTION_CONE
        //cc->kineticVector.set(0, -totalDistanceT1, -totalDistanceT2, 0);
        //cc->kineticVector.normalize();
#endif

        //totalDistance = totalDistanceT1 = totalDistanceT2 = (T)0.0;


        //totalDistance = totalDistanceT1 = totalDistanceT2 = (T)0.0;
        //totalDistanceT1 = totalDistanceT2 = (T)0.0;

        //totalDistance *= (T)2.0;

        if(bodyEdge){
          if(rigidEdge){
            int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(contactNormal*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(t1*dt,            totalDistanceT1, 1, 0, velIndex);
            cc->setRow(t2*dt,            totalDistanceT2, 2, 0, velIndex);

            cc->setRow(cross(re1, contactNormal)*dt, totalDistance,   0, 1, velIndex+1);
            cc->setRow(cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
            cc->setRow(cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
          }else{
            int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(contactNormal*weights[0]*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(t1*weights[0]*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(t2*weights[0]*dt, totalDistanceT2, 2, 0, velIndex);

            velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

            cc->setRow(contactNormal*weights[1]*dt, totalDistance,   0, 1, velIndex);
            cc->setRow(t1*weights[1]*dt, totalDistanceT1, 1, 1, velIndex);
            cc->setRow(t2*weights[1]*dt, totalDistanceT2, 2, 1, velIndex);
          }
        }
        if(bodyVertex){
          if(rigidVertex){
            int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(-contactNormal*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(-t1*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(-t2*dt, totalDistanceT2, 2, 2, velIndex);

            cc->setRow(-cross(rv2, contactNormal)*dt, totalDistance,   0, 3, velIndex+1);
            cc->setRow(-cross(rv2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
            cc->setRow(-cross(rv2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
          }else{
            int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(-contactNormal*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(-t1*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(-t2*dt, totalDistanceT2, 2, 2, velIndex);
          }
        }
      }catch(DegenerateCaseException* e){
        std::cout << e->getError();
        delete cc;
        return false;
      }

      cc->init(ctx->getMatrix());

      cc->status = Active;
      //cc->frictionStatus[0] = Static;
      //cc->frictionStatus[1] = Static;

      enabledId = ctx->getSolver()->addConstraint(cc);
      cc->id = enabledId;

#ifdef ACTIVE_CONSTRAINTS
      cc->resetMultipliers(*ctx->getSolver()->getx(), cachedMultipliers);
#else
      Vector4<T> zero;
      cc->resetMultipliers(*ctx->getSolver()->getx(), zero);
#endif
      status = Enabled;
    }
    return true;
  }

  template<class T>
  void ConstraintCandidateTriangleEdge<T>::recompute(ConstraintSetTriangleEdge<T>* c,
                                                     CollisionContext<T>* ctx,
                                                     Edge<T>& p,
                                                     Vector4<T>& com,
                                                     Quaternion<T>& rot){
    //ctx->mesh->getEdge(&e0, edgeId, backFace);
    v0 = ctx->mesh->vertices[vertexId].coord;
    vi = v0;
    vu = v0;

    comv0 = comvu = comvi = Vector4<T>(0,0,0,0);
    rotv0 = rotvu = rotvi = Quaternion<T>(0,0,0,1);

    //ctx->mesh->setCurrentEdge(edgeId);
    //int vertex = ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(ctx->mesh->vertices[vertexId].type)){
      int objectId = ctx->mesh->vertexObjectMap[vertexId];

      comv0 = comvu = comvi = ctx->mesh->centerOfMass[objectId];
      rotv0 = rotvu = rotvi = ctx->mesh->orientations[objectId];
    }

    p0 = p;
    pi = p;

    comp0 = compi = com;
    rotp0 = rotpi = rot;

    contactNormal = p0.getNormalToVertex(v0);

    baryi = p0.getBarycentricCoordinates(v0);

    if(status == Enabled){
      /*Distance here IS positive by definition*/

      distance0 = distancec = p0.getPlaneDistance(v0, contactNormal) -
        (T)DISTANCE_EPS - (T)MIN_DISTANCE;

      if(distance0 < 0.0){
        //error("Initial distance negative %10.10e", distance0);
      }

      ContactConstraint<2, T>* cc =
        (ContactConstraint<2, T>*)ctx->getMatrix()->getConstraint(enabledId);

      //cc->averageFrictionMagnitude.clear();
      //cc->averageFrictionDirection.clear();

      if(cc->status == Inactive){
        if(distancec > DISTANCE_EPS*1.1){
          /*Constraint is inactive and there is no collision -> disable.*/
          disableConstraint(c, ctx, true);
          return;
        }else{
          /*Constraint was disabled, but there is a collision -> reenable*/
          //cc->status = Active;
        }
      }

      if(!p0.barycentricOnEdge(baryi, (T)BARY_EPS * 0.0) ){
        disableConstraint(c, ctx, true);
        return;
      }


#ifdef WEIGHT_CLAMPING
      Vector4<T> weights =  baryi;
      //Vector4<T> weights1 = p0.clampWeights(baryi1, WEIGHT_MIN, WEIGHT_MAX);
      //Vector4<T> weights2 = e0.clampWeights(baryi2, WEIGHT_MIN, WEIGHT_MAX);
#else
      Vector4<T> weights =  baryi;
#endif

      cc->setMu((T)MU * 0.0);

#ifdef FRICTION_CONE
      cc->cumulativeVector = cc->kineticVector * (T)60;
      //cc->cumulativeVector /= (T)1.1;

      if(cc->cumulativeVector.length() > 1e-6){
        cc->cumulativeVector.normalize();
      }

      if(cc->frictionStatus[0] != Kinetic){
        cc->cumulativeVector.set(0,0,0,0);
        cc->kineticVector.set(0,0,0,0);
        cc->acceptedKineticVector.set(0,0,0,0);
      }

#endif

      int indices[3];
      ctx->mesh->getEdgeIndices(edgeId, indices);
      indices[2] = vertexId;

      T dt = ctx->getDT();

      bool bodyEdge    = true;
      bool bodyVertex  = true;
      bool rigidEdge   = false;
      bool rigidVertex = false;

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyEdge = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidEdge = true;
        }
      }

      for(int i=2;i<3;i++){
        if(Mesh::isStatic(ctx->mesh->vertices[indices[i]].type)){
          bodyVertex = false;
        }

        if(Mesh::isRigid(ctx->mesh->vertices[indices[i]].type)){
          rigidVertex = true;
        }
      }

      T d1 =  pi.getPlaneDistance(p0.vertex(0), contactNormal) * weights[0];
      T d2 =  pi.getPlaneDistance(p0.vertex(1), contactNormal) * weights[1];
      T d3 =  pi.getPlaneDistance(v0,           contactNormal);
      //T d3 =  pi.getPlaneDistance(v0          , contactNormal);
      //T d4 =  ei.getPlaneDistance(e0.vertex(1), dv1) * weights2[1];

      //d1 = dot(contactNormal, pi.vertex(0) - p0.vertex(0)) * weights[0];
      //d2 = dot(contactNormal, pi.vertex(1) - p0.vertex(1)) * weights[1];
      //d3 = dot(contactNormal, vi           - v0          );


      T totalDistance = (-d1 - d2 + d3) -
        (T)DISTANCE_EPS - (T)EPS - (T)MIN_DISTANCE;

      Vector4<T> t1, t2;
      Vector4<T> re1, rv2;

#ifdef FRICTION_CONE
      if(bodyEdge){
        if(rigidEdge){
          t1 -= cc->getRow(1,0);
          t2 -= cc->getRow(2,0);
          re1  = pi.getCoordinates(baryi) - compi;
        }else{
          t1 -= cc->getRow(1,0);
          t1 -= cc->getRow(1,1);
          t2 -= cc->getRow(2,0);
          t2 -= cc->getRow(2,1);
        }
      }

      if(bodyVertex){
        if(rigidVertex){
          t1 += cc->getRow(1,2);
          t2 += cc->getRow(2,2);
          rv2  = vi - comvi;
        }else{
          t1 += cc->getRow(1,2);
          t1 += cc->getRow(1,3);
          t2 += cc->getRow(2,2);
          t2 += cc->getRow(2,3);
        }
      }

      t1 *= -1;
      t2 *= -1;

      t1.normalize();
      t2.normalize();

      Vector4<T> tt1, tt2;
      tt2 = cross(t1, contactNormal).normalize();
      tt1 = cross(tt2, contactNormal).normalize();

      t1 = -tt1;
      t2 = -tt2;

      cc->tangent1 = t1;
      cc->tangent2 = t2;
      cc->normal   = contactNormal;


#else
      Vector4<T> vel(0,0,0,0);
      Vector<T>* vx = ctx->getSolver()->getx();

      if(bodyEdge){
        if(rigidEdge){
          int vel_index = ctx->mesh->vertices[indices[0]].vel_index;

          vel[0] += (*vx)[vel_index+0];
          vel[1] += (*vx)[vel_index+1];
          vel[2] += (*vx)[vel_index+2];

          Vector4<T> ang_vel;

          ang_vel[0] = (*vx)[vel_index+3];
          ang_vel[1] = (*vx)[vel_index+4];
          ang_vel[2] = (*vx)[vel_index+5];

          /*Compute angular displacement*/
          re1  = pi.getCoordinates(baryi) - compi;
          ang_vel = cross(ang_vel, re1);

          vel += ang_vel;
        }else{
          for(int l=0;l<2;l++){
            int vel_index = ctx->mesh->vertices[indices[l+0]].vel_index;

            vel[0] += (*vx)[vel_index+0] * weights[l];
            vel[1] += (*vx)[vel_index+1] * weights[l];
            vel[2] += (*vx)[vel_index+2] * weights[l];
          }
        }
      }

      if(bodyVertex){
        if(rigidVertex){
          int vel_index = ctx->mesh->vertices[indices[2]].vel_index;

          vel[0] -= (*vx)[vel_index+0];
          vel[1] -= (*vx)[vel_index+1];
          vel[2] -= (*vx)[vel_index+2];

          Vector4<T> ang_vel;

          ang_vel[0] = (*vx)[vel_index+3];
          ang_vel[1] = (*vx)[vel_index+4];
          ang_vel[2] = (*vx)[vel_index+5];

          /*Compute angular displacement*/
          rv2  = vi - comvi;
          ang_vel = cross(ang_vel, rv2);

          vel -= ang_vel;
        }else{
          for(int l=0;l<1;l++){
            int vel_index = ctx->mesh->vertices[indices[l+2]].vel_index;
            vel[0] -= (*vx)[vel_index+0];
            vel[1] -= (*vx)[vel_index+1];
            vel[2] -= (*vx)[vel_index+2];
          }
        }
      }

      ////////////////
      t1 = t2.set(0,0,0,0);

      if(rigidEdge){
        t1 -= cc->getRow(1,0);
        t2 -= cc->getRow(2,0);
      }else{
        t1 -= cc->getRow(1,0);
        t1 -= cc->getRow(1,1);
        t2 -= cc->getRow(2,0);
        t2 -= cc->getRow(2,1);
      }

      if(rigidVertex){
        t1 += cc->getRow(1,2);
        t2 += cc->getRow(2,2);
      }else{
        t1 += cc->getRow(1,2);
        t1 += cc->getRow(1,3);
        t2 += cc->getRow(2,2);
        t2 += cc->getRow(2,3);
      }

      /*
      t1 = -((cc->getRow(1, 0) + cc->getRow(1, 1)) -
             (cc->getRow(1, 2) + cc->getRow(1, 3)));
      t2 = -((cc->getRow(2, 0) + cc->getRow(2, 1)) -
             (cc->getRow(2, 2) + cc->getRow(2, 3)));
      */
      t1 *= -1;
      t2 *= -1;

      t1.normalize();
      t2.normalize();

      Vector4<T> vn = cross(vel, contactNormal);

      START_DEBUG;
      message("edge: t1, t2");
      std::cout << t1;
      std::cout << t2;

      std::cout << vel;

      std::cout << vn;
      END_DEBUG;

      /*Re-align with velocity and/or normal*/
      if(vn.length() < 1e-6){
        DBG(message("realign without velocity"));
        /*Fixed point, only align with normal*/
        Vector4<T> tt1, tt2;
        tt2 = cross(t1, contactNormal).normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
          DBG(message("flip tt2"));
        }

        tt1 = cross(t2, contactNormal).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
          DBG(message("flip tt1"));
        }

        t1 = tt1;
        t2 = tt2;
      }else{
        DBG(message("realign with velocity"));
        Vector4<T> tt1, tt2;
        tt2 = vn.normalize();
        if(dot(tt2, t2) < 0){
          tt2 *= -1;
          DBG(message("flip tt2"));
        }

        tt1 = cross(tt2, contactNormal).normalize();
        if(dot(tt1, t1) < 0){
          tt1 *= -1;
          DBG(message("flip tt1"));
        }
        t1 = tt1;
        t2 = tt2;
      }
#endif

#ifndef FRICTION_CONE
      START_DEBUG;
      message("edge: after realign, %10.10e, %10.10e", dot(t1, vn), dot(t2,vn));
      std::cout << t1;
      std::cout << t2;
      END_DEBUG;
#endif

      if(bodyEdge){
        if(rigidEdge){
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(contactNormal*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(t1*dt,             0, 1, 0, velIndex);
          cc->setRow(t2*dt,             0, 2, 0, velIndex);

          cc->setRow(cross(re1, contactNormal)*dt, totalDistance, 0, 1, velIndex+1);
          cc->setRow(cross(re1, t1)*dt,             0, 1, 1, velIndex+1);
          cc->setRow(cross(re1, t2)*dt,             0, 2, 1, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(contactNormal*weights[0]*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(t1*weights[0]*dt,             0, 1, 0, velIndex);
          cc->setRow(t2*weights[0]*dt,             0, 2, 0, velIndex);

          velIndex = ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(contactNormal*weights[1]*dt, totalDistance, 0, 1, velIndex);
          cc->setRow(t1*weights[1]*dt,             0, 1, 1, velIndex);
          cc->setRow(t2*weights[1]*dt,             0, 2, 1, velIndex);
        }
      }
      if(bodyVertex){
        if(rigidVertex){
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-contactNormal*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(-t1*dt,             0, 1, 2, velIndex);
          cc->setRow(-t2*dt,             0, 2, 2, velIndex);

          cc->setRow(-cross(rv2, contactNormal)*dt, totalDistance, 0, 3, velIndex+1);
          cc->setRow(-cross(rv2, t1)*dt,             0, 1, 3, velIndex+1);
          cc->setRow(-cross(rv2, t2)*dt,             0, 2, 3, velIndex+1);
        }else{
          int velIndex = ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-contactNormal*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(-t1*dt,             0, 1, 2, velIndex);
          cc->setRow(-t2*dt,             0, 2, 2, velIndex);
        }
      }

      cc->init(ctx->getMatrix());


      cc->update(*ctx->getSolver()->getb(),
                 *ctx->getSolver()->getx(),
                 *ctx->getSolver()->getb2());


    }else{
      //bool degenerate = false;
      distance0 = distancec = p0.getPlaneDistance(v0, contactNormal) - (T)DISTANCE_EPS - (T)MIN_DISTANCE;

      if(distance0 < 0.0){
        //error("Initial distance negative %10.10e", distance0);
      }
    }
  }

  template class ConstraintCandidateTriangleEdge<float>;
  template class ConstraintCandidateTriangleEdge<double>;
}
