/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/BBox.hpp"
#include "math/ConstraintSetFace.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

#include "math/ConstraintSet.hpp"
#include "math/ConstraintSetEdge.hpp"
#include "math/ConstraintSetTriangleEdge.hpp"

//#define BBEPS 5e-3
namespace CGF{

  template<class T>
  void CollisionContext<T>::initializeBBoxes(){
    int nEdges = mesh->getNHalfEdges();
    bboxEdge = new Vector4<T>[nEdges * 2];

    int nVertices = mesh->getNVertices();
    bboxVertex = new Vector4<T>[nVertices * 2];

    /*Extended bboxes of faces are stored in stree*/
  }

  template<class T>
  void CollisionContext<T>::deleteBBoxes(){
    delete[] bboxEdge;
    delete[] bboxVertex;
  }

  template<class T>
  void CollisionContext<T>::updateBBoxes(){
    //return;
    /*Compute boxes for each vertex*/
    message("Updating bboxes");

    for(int i=0;i<mesh->getNVertices();i++){
      if(mesh->vertices[i].leaving == Mesh::UndefinedIndex){
        /*Internal vertex, is not a part of the outher boundary, skip*/
        continue;
      }

      BBox<T> box(mesh->vertices[i].coord,
                  mesh->vertices[i].coord);

      box.addPoint(mesh->vertices[i].displCoordExt);
      box.addEpsilon((T)BBOXEPS);

      bboxVertex[i*2+0] = box.getMin();
      bboxVertex[i*2+1] = box.getMax();
    }

    /*Compute boxes for each edge*/
    for(int i=0;i<mesh->getNHalfEdges();i++){
      int vertices[2];

      mesh->getEdgeIndices(i, vertices);

      BBox<T> box(mesh->vertices[vertices[0]].coord,
                  mesh->vertices[vertices[0]].coord);

      box.addPoint(mesh->vertices[vertices[1]].coord);

      box.addPoint(mesh->vertices[vertices[0]].displCoordExt);
      box.addPoint(mesh->vertices[vertices[1]].displCoordExt);

      box.addEpsilon((T)BBOXEPS);

      bboxEdge[i*2+0] = box.getMin();
      bboxEdge[i*2+1] = box.getMax();
    }
  }

  template<class T>
  void CollisionContext<T>::checkBBoxesVertex(List<int>* invalidVertices){
    int n_vertices = mesh->getNVertices();

    for(int i=0;i<n_vertices;i++){
      if(mesh->vertices[i].leaving == Mesh::UndefinedIndex){
        /*Internal vertex, is not a part of the outher boundary, skip*/
        continue;
      }

      BBox<T> box(bboxVertex[i*2+0], bboxVertex[i*2+1]);

      if(box.inside(mesh->vertices[i].displCoord)){
        /*vertex still in original box, ok*/
      }else{
        /*vertex has moved out of box, possible collisions could be missed*/
        invalidVertices->append(i);
      }
    }
  }

  template<class T>
  void CollisionContext<T>::checkBBoxesEdge(List<int>* invalidEdges){
    int n_edges = mesh->getNHalfEdges();

    for(int i=0;i<n_edges;i++){
      mesh->setCurrentEdge(i);

      int twin = mesh->getTwinEdge();

      if(i < twin){
        int vertices[2];
        mesh->getEdgeIndices(i, vertices);

        BBox<T> box(bboxEdge[i*2+0], bboxEdge[i*2+1]);

        if(!box.inside(mesh->vertices[vertices[0]].displCoord)){
          /*edge moved out of box, possible collisions could be missed*/
          invalidEdges->append(i);
        }else if(!box.inside(mesh->vertices[vertices[1]].displCoord)){
          /*edge moved out of box, possible collisions could be missed*/
          invalidEdges->append(i);
        }else{
          /*edge still in original box, ok*/
        }
      }
    }
  }

  /*Finds all vertex-face potential collisions given the already found
    face-face potentials.*/
  template<class T>
  void CollisionContext<T>::extractPotentialVertexFace(int vertex,
                                                       List<int>& faces,
                                                       bool internal){
    /*Get all connected faces of this vertex*/
    List<int> connectedFaces;
    List<int> foundFaces;

    mesh->getConnectedFacesOfVertex(vertex, connectedFaces);

    /*Find all proximate faces of the faces connected to this vertex*/
    List<int>::Iterator it = connectedFaces.begin();
    while(it != connectedFaces.end()){
      int currentFace = *it++;

      List<int>::Iterator it2 = faceFaceSets[currentFace].candidates.begin();

      while(it2 != faceFaceSets[currentFace].candidates.end()){
        int proximateFace = *it2++;

        if(internal){
          /*When searching for internal collisions, the neighboring
            face of the requested edge must have the same root as the
            found faces.*/

          if(stree->facesShareSameRoot(currentFace,
                                       proximateFace) == false){
            /*Faces belong to different objects, hence there can't be
              an internal collision.*/
            continue;
          }
        }

        /*If the proximate face contains the queried vertex,
          discard, since they can never intersect*/
        int faceVertices[3];
        mesh->getTriangleIndices(proximateFace, faceVertices, false);

        bool connectedFace = false;
        for(int i=0;i<3;i++){
          if(faceVertices[i] == vertex){
            connectedFace = true;
          }
        }

        if(mesh->halfFaces[proximateFace].visited == false){
          if(!connectedFace){
            foundFaces.append(proximateFace);
            //foundFaces.insert(proximateFace, proximateFace);
            mesh->halfFaces[proximateFace].visited = true;
          }
        }
      }
    }

    //foundFaces.removeDuplicates();

    BBox<T> box(bboxVertex[vertex*2+0], bboxVertex[vertex*2+1]);

    List<int>::Iterator it2 = foundFaces.begin();

    while(it2 != foundFaces.end()){
      int currentFace = *it2++;

      mesh->halfFaces[currentFace].visited = false;

      if(box.intersection(stree->leafNodes[currentFace].box)){
        //faces.insert(currentFace, currentFace);
        faces.append(currentFace);
      }
    }
  }

  /*Finds all edge-edge potential collisions given the already found
    face-face potentials.*/
  template<class T>
  void CollisionContext<T>::extractPotentialEdgeEdges(int edge,
                                                      List<int>& edges,
                                                      bool internal){
    List<int> foundEdges;
    //Tree<int> foundEdges;

    int vertices[2];
    int faces[2];

    /*Obtain edge indices*/
    mesh->getEdgeIndices(edge, vertices);

    /*Obtain face indices of edge*/
    mesh->setCurrentEdge(edge);
    faces[0] = mesh->getFace();
    faces[1] = mesh->getTwinFace();

    Tree<int> foundFaces;
    for(int i=0;i<2;i++){
      int currentNeighboringFace = faces[i];

      List<int>::Iterator it2 = faceFaceSets[currentNeighboringFace].candidates.begin();

      while(it2 != faceFaceSets[currentNeighboringFace].candidates.end()){
        int currentFace = *it2++;
        //if(currentFace == lastFace){
        //  /*Duplicate*/
        //  continue;
        //}

        //lastFace = currentFace;

        if(internal){
          /*When searching for internal collisions, the neighboring
            face of the requested edge must have the same root as the
            found faces.*/

          if(stree->facesShareSameRoot(currentNeighboringFace,
                                       currentFace) == false){
            /*Faces belong to different objects, hence there can't be
              an internal collision.*/
            continue;
          }
        }


        mesh->setCurrentEdge(mesh->halfFaces[currentFace].half_edge);
        for(int j=0;j<3;j++){
          int cedge = (int)mesh->getCurrentEdge();
          int tedge = (int)mesh->getTwinEdge();

          int selectedEdge = cedge<tedge?cedge:tedge;

          if(mesh->halfEdges[selectedEdge].visited == false){
            if(edge < selectedEdge){
              int edgeVertices[2];
              mesh->getEdgeIndices(selectedEdge, edgeVertices);

              /*Check if the current edge and proximate edge do not
                have a vertex in common. If so, they are connected and
                should be discarded. Two connected edges do have a
                zero distance and may produce false positives in the
                fine collision check.*/
              bool connected = false;
              for(int k=0;k<2;k++){
                for(int l=0;l<2;l++){
                  if(vertices[k] == edgeVertices[l]){
                    connected = true;
                  }
                }
              }
              if(!connected){
                /*Edge is not connected with this edge.*/
                mesh->halfEdges[selectedEdge].visited = true;
                foundEdges.append(selectedEdge);
                //foundEdges.insert(selectedEdge, selectedEdge);
              }
            }
          }
          mesh->nextEdge();
        }
      }
    }

    /*Remove duplicate edges*/
    //foundEdges.removeDuplicates();

    /*Check for box-box intersections of the edge boxes*/

    BBox<T> box(bboxEdge[edge*2+0], bboxEdge[edge*2+1]);
    List<int>::Iterator it = foundEdges.begin();
    while(it != foundEdges.end()){
      int currentEdge = *it++;
      mesh->halfEdges[currentEdge].visited = false;

      BBox<T> edgeBox(bboxEdge[currentEdge*2+0], bboxEdge[currentEdge*2+1]);

      if(box.intersection(edgeBox)){
        //edges.insert(currentEdge, currentEdge);
        edges.append(currentEdge);
      }
    }
  }

  template<class T>
  void CollisionContext<T>::extractPotentialEdgeVertices(int edge,
                                                         Tree<int>& vertices){

    //int vertices[2];
    int faceVertices[2];

    /*Obtain edge indices*/
    //mesh->getEdgeIndices(edge, vertices);

    /*Obtain face indices of edge*/
    mesh->setCurrentEdge(edge);
    mesh->nextEdge();
    mesh->nextEdge();
    faceVertices[0] = mesh->getOriginVertex();
    mesh->setCurrentEdge(edge);
    mesh->twinEdge();
    mesh->nextEdge();
    mesh->nextEdge();
    faceVertices[1] = mesh->getOriginVertex();

    for(int i=0;i<2;i++){
      int currentVertex = faceVertices[i];

      vertices.insert(currentVertex, currentVertex);
    }
  }

  template<class T>
  void CollisionContext<T>::showStateVertexFace(int vertex, int face,
                                                bool backFace){
    message("showStateVertexFace %d, %d, %d", vertex, face, backFace);
    message("nVertices = %d", mesh->getNVertices());
    message("nFaces    = %d", mesh->getNHalfFaces());

    message("this pointer = %p", this);

    if(vertex >= mesh->getNVertices()){
      return;
    }

    if(face >= mesh->getNHalfFaces()){
      return;
    }

    if(backFace){
      backSets[vertex].showFaceCandidateState(face, this);
    }else{
      sets[vertex].showFaceCandidateState(face, this);
    }
  }

  template<class T>
  void CollisionContext<T>::showStateEdgeEdge(int edgeA, int edgeB,
                                              bool backFace){
    message("showStateEdgeEdge %d, %d, %d", edgeA, edgeB, backFace);
    message("nEdges = %d", mesh->getNHalfEdges());
    message("this pointer = %p", this);

    if(edgeA >= mesh->getNHalfEdges()){
      return;
    }

    if(edgeB >= mesh->getNHalfEdges()){
      return;
    }

    if(backFace){
      backEdgeSets[edgeA].showEdgeCandidateState(edgeB, this);
    }else{
      edgeSets[edgeA].showEdgeCandidateState(edgeB, this);
    }
  }

  template<class T>
  void CollisionContext<T>::showStateEdgeVertex(int edge, int vertex){
    if(edge >= mesh->getNHalfEdges()){
      return;
    }

    triangleEdgeSets[edge].showVertexCandidateState(vertex, this);
  }

  template class CollisionContext<float>;
  template class CollisionContext<double>;
}
