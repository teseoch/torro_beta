/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef OCTREE_HPP
#define OCTREE_HPP

#include "core/cgfdefs.hpp"
#include "datastructures/List.hpp"
#include "datastructures/Tree.hpp"
#include "math/Vector4.hpp"
#include "math/BBox.hpp"

namespace CGF{
#define MAX_ITEMS 10

  static int node_id = 0;

#if 0
  template<class T, class Y>
  struct OctreeGetPosition{
    static const Vector4<Y> getPosition(T& a){
      warning("Default %s used", __PRETTY_FUNCTION__);
      return Vector4<Y>((Y)0.0, (Y)0.0, (Y)0.0, (Y)0.0);
    }
  };
#endif

#if 1
  template<class T, class Y>
  const Vector4<Y> getPosition(T& a){
    warning("Default %s used", __PRETTY_FUNCTION__);
    return Vector4<Y>((Y)0.0, (Y)0.0, (Y)0.0, (Y)0.0);
  }

  template<class T, class Y>
  const Vector4<Y> getPosition(T* a){
    warning("Default %s used", __PRETTY_FUNCTION__);
    return Vector4<Y>((Y)0.0, (Y)0.0, (Y)0.0, (Y)0.0);
  }
#endif

  template<class T>
  int getId(T& a){
    return 0;
  }

  template<class T>
  class NBody;

  namespace OctreeInternals{
    template<class T, class Y>
    class OctreeNode;
  }

  template<class T, class Y>
  class GlOctree;

  template<class T, class Y>
  class Octree{
  public:

  private:
    typedef OctreeInternals::OctreeNode<T, Y> Node;
    typedef Octree<T, Y>                      Self;
    typedef typename List<Node*>::Iterator    NodeListIterator;
    typedef typename List<T>::Iterator        ValueListIterator;
  public:

    Octree(){
      PRINT_FUNCTION;
      root = new Node();
      root->clear();
      root->id = node_id;
      node_id++;
      BBox<Y> initial_box;
      initial_box.setMin(Vector4<Y>(-1,-1,-1,0));
      initial_box.setMax(Vector4<Y>( 1, 1, 1,0));
      root->bbox = initial_box;
      max_items = MAX_ITEMS;
    }

    Octree(const BBox<Y>& box){
      PRINT_FUNCTION;
      root = new Node();
      root->clear();
      root->id = node_id;
      node_id++;
      root->bbox = box;
      max_items = MAX_ITEMS;
    }

    Octree(const Self&){
    }

    Self& operator=(const Self&){
    }

    virtual ~Octree(){
      delete root;
    }

    void setMaxItems(int m){
      max_items = m;
    }

    void insert(T& a){
      if(root->insert(a) == false){
        //message("root = %p", root);
        //std::cout << root->bbox << std::endl;
        //std::cout << getPosition<T, Y>(a) << " " << getId(a) << std::endl;
        //message("Inside %d", root->bbox.inside(getPosition<T, Y>(a)));
        //message("Outside %d", root->bbox.outside(getPosition<T, Y>(a)));
        int quadrant = root->quadrant(getPosition<T, Y>(a));
        int this_new_quadrant = 7-quadrant;
        Node* new_root = new Node();
        new_root->bbox = root->bbox;

        /*Extend bbox of root*/
        Vector4<Y> boxmin = root->bbox.getMin();
        Vector4<Y> boxmax = root->bbox.getMax();
        Vector4<Y> boxsize = boxmax - boxmin;

        Vector4<Y> newmax, newmin;

        switch(quadrant){
        case 0:
          newmax = boxmax;
          break;
        case 1:
          newmax = boxmax;
          newmax[0] += boxsize[0];
          break;
        case 2:
          newmax = boxmax;
          newmax[1] += boxsize[1];
          break;
        case 3:
          newmax = boxmax;
          newmax[0] += boxsize[0];
          newmax[1] += boxsize[1];
          break;
        case 4:
          newmax = boxmax;
          newmax[2] += boxsize[2];
          break;
        case 5:
          newmax = boxmax;
          newmax[0] += boxsize[0];
          newmax[2] += boxsize[2];
          break;
        case 6:
          newmax = boxmax;
          newmax[1] += boxsize[1];
          newmax[2] += boxsize[2];
          break;
        case 7:
          newmax = boxmax + boxsize;
        }
        newmin = newmax - boxsize*(Y)2.0;

        new_root->bbox.setMax(newmax);
        new_root->bbox.setMin(newmin);
        new_root->center = new_root->bbox.center();

        /*Split new root, which creates the childs and bounding boxes*/
        new_root->split(1,max_items,0,true);

        /*Replace one child with old root*/
        delete new_root->childs[this_new_quadrant];
        new_root->childs[this_new_quadrant] = root;
        root->parent = new_root;
        root = new_root;

        /*insert into new root recursively, since the new point might
          still be outside the newly created cells*/
        insert(a);
      }
    }

#if 0
    void remove(T& a){
      root->remove(a);
    }
#endif

    void remove(T& a){
      root->remove(a);
    }

    void split(int depth, bool empty_split = false){
      root->split(depth, max_items, 0, empty_split);
    }

    void merge(){
      root->merge();
    }

    void computeNodeAverage(){
      root->computeNodeAverage();
    }

    void balance(){
      List<Node*> leafNodes;
      root->getLeafNodes(leafNodes);

      NodeListIterator it = leafNodes.begin();
      NodeListIterator end = leafNodes.end();

      while(it != end){
        Node* curNode = *it;

        message("%p, end = %p, listSize = %d", curNode, end, leafNodes.size());

        if(curNode->hasToSplit()){
          if(curNode->childs){
            for(int i = 0;i<8;i++){
              Node* child = curNode->childs[i];
              message("append %p of node %p is root %d", child, curNode, curNode->isRoot());
              leafNodes.append(child);
            }
          }

          /*Inspect neighbors*/
          Node* nb;
          for(int i=0;i<6;i++){
            nb = curNode->neighbor(i);
            if(nb){
              if(nb->hasToSplit()){
                leafNodes.append(nb);
              }
            }
          }
        }
        it++;
        leafNodes.removeAt(0);
      }
    }

    void getLeafNodes(List<Node*>& list){
      root->getLeafNodes(list);
    }

    void getBranchNodes(List<Node*>& list){
      root->getBranchNodes(list);
    }

    int size(){
      return root->size();
    }

    BBox<Y> getBox(){
      return root->getBox();
    }

#if 1
    int getNeighboringElements(BBox<Y>& range, List<T>& el){
      List<Node*> nodes = getNeighborhood(range);

      NodeListIterator it = nodes.begin();

      List<T> elements;
      List<T> closeElements;
      int n_found = 0;

      while(it != nodes.end()){
        Node* node = *it++;
        node->getAllElements(elements);
      }

      ValueListIterator eit = elements.begin();

      while(eit != elements.end()){
        T object = *eit++;
        if(range.inside(getPosition<T, Y>(object))){
          el.append(object);
          n_found++;
        }
      }
      return n_found;
    }
#endif

    int getNeighboringElements(Vector4<Y>& center, Y radius, List<T>& el){
      List<Node*> nodes = getNeighborhood(center, radius);

      NodeListIterator it = nodes.begin();

      List<T> elements;
      List<T> closeElements;
      int n_found = 0;

      while(it != nodes.end()){
        Node* node = *it++;
        node->getAllElements(elements);
      }

      //message("       Elements.size = %d", elements.size());
      //elements.unique();
      //message("Unique Elements.size = %d", elements.size());

      ValueListIterator eit = elements.begin();

      while(eit != elements.end()){
        T object = *eit++;
        if((getPosition<T, Y>(object) - center).length() < radius){
          el.append(object);
          n_found++;
        }
      }
      return n_found;
    }

    List<Node*> getNeighborhood(Vector4<Y>& center, Y radius){
      return root->getNeighborhood(center, radius);
    }

#if 1
    List<Node*> getNeighborhood(BBox<Y>& range){
      return root->getNeighborhood(range);
    }
#endif

    void trace(const Vector4<Y>& a, const Vector4<Y>& b, List<T>& list){

    }

    void computeBounds(){
      root->computeBounds();
    }
    template<class YY>
    friend class NBody;
  protected:
    Node* root;
    int max_items;
  };

  namespace OctreeInternals{
    template<class T, class Y>
    class OctreeNode{
    public:
      int id;

      typedef OctreeNode<T, Y> Self;

    protected:
      typedef typename Tree<T>::Iterator ValueTreeIterator;
    public:

      OctreeNode():parent(0), childs(0), bbox(){
        id = 0;
        n_items = 0;
      }

      OctreeNode(const Self&);

      ~OctreeNode(){
        if(childs != 0){
          for(int i=0;i<8;i++){
            delete childs[i];
          }
          delete[] childs;
        }
      }

      Self& operator=(const Self&);

      Self& getNeighbor(int i);

      void clear(){
        items.clear();
      }

      BBox<Y> getBox(){
        return bbox;
      }

      bool insert(T& a){
        Vector4<Y> pos = getPosition<T, Y>(a);
        if(bbox.inside(pos)){
          if(childs == 0){
            items.insert(a, 1);
            return true;
          }else{
            int q = quadrant(pos);
            return childs[q]->insert(a);
          }
        }
        return false;
      }

      void computeBounds(){
        ValueTreeIterator it = items.begin();
        ValueTreeIterator end = items.end();

        //bbox = BBox();
        while(it != end){
          Vector4<Y> pos = getPosition<T, Y>(*it);
          if(bbox.inside(pos)){
            /*Point in bounding box, no need to adapt bbox*/
          }else{
            /*Point is stored in this node but lies outside bbox of node*/
            bbox.extend(pos);
          }
          it++;
        }
      }

      int getId(){
        return id;
      }

      bool remove(T& a){
        Vector4<Y> pos = getPosition<T, Y>(a);
        if(bbox.inside(pos)){
          if(childs == 0){
            items.remove(a);
            return true;
          }else{
            int q = quadrant(pos);
            return childs[q]->remove(a);
          }
        }
        return false;
      }

      void getAllElements(List<T>& l){
        ValueTreeIterator it = items.begin();

        while(it != items.end()){
          l.append(*it);
          it++;
        }
        if(childs){
          for(int i=0;i<8;i++){
            childs[i]->getAllElements(l);
          }
        }
      }

      List<Self*> getNeighborhood(){
        List<Self*> list;

        /*Add direct neighbors*/
        for(int i=0;i<6;i++){
          Self* ptr = neighbor(i);
          if(ptr != 0){
            list.append(ptr);
          }
        }

        /*Add indirect neighbors*/
        return list;
      }

      /*Returns nodes which contains or intersect the sphere defined by
        center and radius*/
      List<Self*> getNeighborhood(Vector4<Y>& center, Y radius){
        List<Self*> list;

        if(isLeaf()){
          list.append(this);
        }else{
          cgfassert(childs != 0);

          for(int i=0;i<8;i++){
            if(childs[i]->bbox.intersects(center, radius) ||
               childs[i]->bbox.inside(center, radius)||
               childs[i]->bbox.contains(center, radius)){
              List<Self*> tmp =
                childs[i]->getNeighborhood(center, radius);
              list.append(tmp);
            }
          }
        }

        return list;
      }

#if 1
      /*Returns nodes which contains or intersect the bounding box*/
      List<Self*> getNeighborhood(BBox<Y>& range){
        List<Self*> list;

        if(isLeaf()){
          list.append(this);
        }else{
          cgfassert(childs != 0);

          for(int i=0;i<8;i++){
            if(childs[i]->bbox.intersection(range)){
              List<Self*> tmp =
                childs[i]->getNeighborhood(range);
              list.append(tmp);
            }
          }
        }

        return list;
      }
#endif

      void computeNodeAverage(){
        T childSum;
        int n = 0;
        if(childs){
          for(int i=0;i<8;i++){
            childs[i]->computeNodeAverage();
            childSum += childs[i]->sum * getWeight(childs[i]->sum);
            n += childs[i]->n_items;
          }
          cgfassert(items.size() == 0);
        }else{
          ValueTreeIterator it = items.begin();
          ValueTreeIterator end = items.end();

          while(it != end){
            childSum += (*it) * getWeight(*it);
            it++;
          }
          n += items.size();
        }
        if(n != 0){
          sum = childSum/getWeight(childSum);
        }else{
          sum = childSum;
        }
        n_items = n;
      }

      Self* neighbor27(int q){
        /*If no parent, then no neighbors can be found*/
        if(parent == 0)
          return 0;

        /*Direct neighbors*/
        int direct_neighbors[27][17] =
          { /*n_cases , case, returns, case, returns*/
            /*0*/{1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*1*/{2, 6, 0, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*2*/{1, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*3*/{2, 5, 0, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*4*/{4, 4, 0, 5, 1, 6, 2, 7, 3, 0, 0, 0, 0, 0, 0, 0, 0},
            /*5*/{2, 4, 1, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*6*/{1, 5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*7*/{2, 4, 2, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*8*/{1, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*9*/{2, 3, 0, 7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*10*/{4, 2, 0, 3, 1, 6, 4, 7, 5, 0, 0, 0, 0, 0, 0, 0, 0},
            /*11*/{2, 2, 1, 6, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*12*/{4, 1, 0, 5, 4, 3, 2, 7, 6, 0, 0, 0, 0, 0, 0, 0, 0},
            /*Center cell*/
            /*13*/{8, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7},
            /*14*/{4, 0, 1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            /*15*/{2, 1, 2, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*16*/{4, 0, 2, 1, 3, 4, 6, 5, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            /*17*/{2, 0, 3, 4, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*18*/{1, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*19*/{2, 2, 4, 3, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*20*/{1, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*21*/{2, 1, 4, 3, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*22*/{4, 0, 4, 1, 5, 2, 6, 3, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            /*23*/{2, 0, 5, 2, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*24*/{1, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*25*/{2, 0, 6, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            /*26*/{1, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          };

        cgfassert(parent->childs);

        Self* r = 0;

        /*Find direct neighbors*/
        int n_cases = direct_neighbors[q][0];
        for(int i=0;i<n_cases;i++){
          if(this == parent->childs[direct_neighbors[q][1+i*2]]){
            return parent->childs[direct_neighbors[q][2+i*2]];
          }
        }

        /*No direct neighbor found, check parent recursively*/
        /*Find proper neighbor of parent*/

        int parent_neighbors[27][8] =
          { /*Current child, return parent*/
            /*dir 0, 1, 2, 3, 4,  5,  6,  7*/
            /*0*/{0, 1, 3, 4, 9,  10, 12, -1},
            /*1*/{1, 1, 4, 4, 10, 10, -1, -1},
            /*2*/{1, 2, 4, 5, 10, 11, -1, 14},
            /*3*/{3, 4, 3, 4, 12, -1, 12, -1},
            /*4*/{4, 4, 4, 4, -1, -1, -1, -1},
            /*5*/{4, 5, 4, 5, -1, 14, -1, 14},
            /*6*/{3, 4, 6, 7, 12, -1, 15, 16},
            /*7*/{4, 4, 7, 7, -1, -1, 16, 16},
            /*8*/{4, 5, 7, 8, -1, 14, 16, 17},
            /*9*/{9, 10, 12, -1, 9, 10, 12, -1},
            /*10*/{10, 10, -1, -1, 10, 10, -1, -1},
            /*11*/{10, 11, -1, 14, 10, 11, -1, 14},
            /*12*/{12, -1, 12, -1, 12, -1, 12, -1},
            /*13*/{-1, -1, -1, -1, -1, -1, -1, -1},
            /*14*/{-1, 14, -1, 14, -1, 14, -1, 14},
            /*15*/{12, -1, 15, 16, 12, -1, 15, 16},
            /*16*/{-1, -1, 16, 16, -1, -1, 16, 16},
            /*17*/{-1, 14, 16, 17, -1, 14, 16, 17},
            /*18*/{9, 10, 12, -1, 18, 19, 21, 22},
            /*19*/{10, 10, -1, -1, 19, 19, 22, 22},
            /*20*/{10, 11, -1, 14, 19, 20, 22, 23},
            /*21*/{12, -1, 12, -1, 21, 22, 21, 22},
            /*22*/{-1, -1, -1, -1, 22, 22, 22, 22},
            /*23*/{-1, 14, -1, 14, 22, 23, 22, 23},
            /*24*/{12, -1, 15, 16, 21, 22, 24, 25},
            /*25*/{-1, -1, 16, 16, 22, 22, 25, 25},
            /*26*/{-1, 14, 16, 17, 22, 23, 25, 26}
          };

        int p_neighbor = -1;
        for(int i=0;i<8;i++){
          if(this == parent->childs[i]){
            p_neighbor = parent_neighbors[q][i];
          }
        }

        cgfassert(p_neighbor != -1);

        r = parent->neighbor27(p_neighbor);

        if(r == 0 || r->childs == 0){
          return r;
        }

        int indirect_neighbors[27][8] =
          { /*     0, 1, 2, 3, 4, 5, 6, 7*/
            /*0*/ {7, 6, 5, 4, 3, 2, 1, 0},
            /*1*/ {6, 7, 4, 5, 2, 3, 0, 1},
            /*2*/ {7, 6, 5, 4, 3, 2, 1, 0},
            /*3*/ {5, 4, 7, 6, 1, 0, 3, 2},
            /*4*/ {4, 5, 6, 7, 0, 1, 2, 3},
            /*5*/ {5, 4, 7, 6, 1, 0, 3, 2},
            /*6*/ {7, 6, 5, 4, 3, 2, 1, 0},
            /*7*/ {6, 7, 4, 5, 2, 3, 0, 1},
            /*8*/ {7, 6, 5, 4, 3, 2, 1, 0},
            /*9*/ {3, 2, 1, 0, 7, 6, 5, 4},
            /*10*/{2, 3, 0, 1, 6, 7, 4, 5},
            /*11*/{3, 2, 1, 0, 7, 6, 5, 4},
            /*12*/{1, 0, 3, 2, 5, 4, 7, 6},
            /*13*/{0, 1, 2, 3, 4, 5, 6, 7},
            /*14*/{1, 0, 3, 2, 5, 4, 7, 6},
            /*15*/{3, 2, 1, 0, 7, 6, 5, 4},
            /*16*/{2, 3, 0, 1, 6, 7, 4, 5},
            /*17*/{3, 2, 1, 0, 7, 6, 5, 4},
            /*18*/{7, 6, 5, 4, 3, 2, 1, 0},
            /*19*/{6, 7, 4, 5, 2, 3, 0, 1},
            /*20*/{7, 6, 5, 4, 3, 2, 1, 0},
            /*21*/{5, 4, 7, 6, 1, 0, 3, 2},
            /*22*/{4, 5, 6, 7, 0, 1, 2, 3},
            /*23*/{5, 4, 7, 6, 1, 0, 3, 2},
            /*24*/{7, 6, 5, 4, 3, 2, 1, 0},
            /*25*/{6, 7, 4, 5, 2, 3, 0, 1},
            /*26*/{7, 6, 5, 4, 3, 2, 1, 0},
          };

        for(int i=0;i<8;i++){
          if(this == parent->childs[i]){
            return r->childs[indirect_neighbors[q][i]];
          }
        }
        error("Unknown octree error while finding neighbor");
        return 0;
      }

      Self* neighbor(int q){
        if(parent == 0)
          return 0;

        /*Our parent must have childs since this object is a child*/
        cgfassert(parent->childs);

        Self* r = 0;

        switch(q){
        case 0:
          if(this == parent->childs[1])
            return parent->childs[0];
          if(this == parent->childs[3])
            return parent->childs[2];
          if(this == parent->childs[7])
            return parent->childs[6];
          if(this == parent->childs[5])
            return parent->childs[4];
          break;
        case 1:
          if(this == parent->childs[2])
            return parent->childs[3];
          if(this == parent->childs[0])
            return parent->childs[1];
          if(this == parent->childs[6])
            return parent->childs[7];
          if(this == parent->childs[4])
            return parent->childs[5];
          break;
        case 3:
          if(this == parent->childs[0])
            return parent->childs[2];
          if(this == parent->childs[1])
            return parent->childs[3];
          if(this == parent->childs[4])
            return parent->childs[6];
          if(this == parent->childs[5])
            return parent->childs[7];
          break;
        case 2:
          if(this == parent->childs[2])
            return parent->childs[0];
          if(this == parent->childs[3])
            return parent->childs[1];
          if(this == parent->childs[6])
            return parent->childs[4];
          if(this == parent->childs[7])
            return parent->childs[5];
          break;
        case 4:
          if(this == parent->childs[6])
            return parent->childs[2];
          if(this == parent->childs[7])
            return parent->childs[3];
          if(this == parent->childs[4])
            return parent->childs[0];
          if(this == parent->childs[5])
            return parent->childs[1];
          break;
        case 5:
          if(this == parent->childs[0])
            return parent->childs[4];
          if(this == parent->childs[1])
            return parent->childs[5];
          if(this == parent->childs[2])
            return parent->childs[6];
          if(this == parent->childs[3])
            return parent->childs[7];
          break;
        default:
          error("Wrong direction");
        }

        r = parent->neighbor(q);

        if(r == 0 || r->childs == 0)
          return r;

        switch(q){
        case 0:
          if(this == parent->childs[0])
            return r->childs[1];
          if(this == parent->childs[2])
            return r->childs[3];
          if(this == parent->childs[6])
            return r->childs[7];
          if(this == parent->childs[4])
            return r->childs[5];
          break;
        case 1:
          if(this == parent->childs[3])
            return r->childs[2];
          if(this == parent->childs[1])
            return r->childs[0];
          if(this == parent->childs[7])
            return r->childs[6];
          if(this == parent->childs[5])
            return r->childs[4];
          break;
        case 2:
          if(this == parent->childs[0])
            return r->childs[2];
          if(this == parent->childs[1])
            return r->childs[3];
          if(this == parent->childs[4])
            return r->childs[6];
          if(this == parent->childs[5])
            return r->childs[7];
          break;
        case 3:
          if(this == parent->childs[2])
            return r->childs[0];
          if(this == parent->childs[3])
            return r->childs[1];
          if(this == parent->childs[6])
            return r->childs[4];
          if(this == parent->childs[7])
            return r->childs[5];
          break;
        case 4:
          if(this == parent->childs[2])
            return r->childs[6];
          if(this == parent->childs[3])
            return r->childs[7];
          if(this == parent->childs[0])
            return r->childs[4];
          if(this == parent->childs[1])
            return r->childs[5];
          break;
        case 5:
          if(this == parent->childs[6])
            return r->childs[2];
          if(this == parent->childs[7])
            return r->childs[3];
          if(this == parent->childs[4])
            return r->childs[0];
          if(this == parent->childs[5])
            return r->childs[1];
          break;
        default:
          error("Wrong direction");
        }
        return 0;
      }


      void getSurroundingNodes(List<Self*>& list){
        Self* currentNode = this;

        while(currentNode->parent != 0){
          cgfassert(currentNode->parent->childs);

          for(int i=0;i<8;i++){
            if(currentNode->parent->childs[i] != currentNode){
              list.append(currentNode->parent->childs[i]);
            }
          }

          currentNode = currentNode->parent;
        }
      }

      void getLeafNodes(List<Self*>& list){
        if(childs){
          for(int i=0;i<8;i++){
            childs[i]->getLeafNodes(list);
          }
        }else{
          Self* ptr = this;
          list.append(ptr);
        }
      }

      void getBranchNodes(List<Self*>& list){
        if(childs){
          for(int i=0;i<8;i++){
            OctreeNode<T, Y>* ptr = this;
            list.append(ptr);
            childs[i]->getBranchNodes(list);
          }
        }
      }

      void trace(Vector4<Y>& a, Vector4<Y>& dir, List<T>& list){
        bool intersects[8];
        Vector4<Y> centers[8];
        Y    distances[8];
        for(int i=0;i<8;i++){
          centers[i] = childs[i].box.center();
          Vector4<Y> dist = centers[i]-a;
          distances[i] = dir*dist;

          intersects[i] = false;
        }
      }

      inline bool isLeaf(){
        return (childs == 0)?true:false;
      }

      inline bool isRoot(){
        return (parent == 0)?true:false;
      }

      inline int quadrant(const Vector4<Y>& p){
        int q = 0;
        Vector4<Y> b = (p >= center);

        if(b[0] == (Y)1.0)
          q |= 1;

        if(b[1] == (Y)1.0)
          q |= 2;

        if(b[2] == (Y)1.0)
          q |= 4;

        return q;
      }

      int size(){
        int sz = 0;
        if(childs != NULL){

          cgfassert(items.size() == 0);

          for(int i=0;i<8;i++){
            sz += childs[i]->size();
          }
        }
        return sz + items.size();
      }

      void merge(){
        if(childs){
          /*First merge child*/
          for(int i=0;i<8;i++){
            childs[i]->merge();
          }

          for(int i=0;i<8;i++){
            ValueTreeIterator cit = childs[i]->items.begin();
            ValueTreeIterator cend = childs[i]->items.end();

            while(cit != cend){
              items.insert(*cit);
              cit++;
            }
            childs[i]->items.clear();

            delete childs[i];
          }
          delete[] childs;
          childs = 0;
        }
      }

      void split(int depth, int max, int curDepth = 0, bool empty_split = false){
        if(depth == curDepth)
          return;

        if(empty_split == false){
          if(items.size() < max){
            /*Recursive split, maybe the childs needs to be splitted*/
            if(childs){
              for(int i=0;i<8;i++){
                childs[i]->split(depth, max, curDepth+1, empty_split);
              }
            }
            return;
          }
        }

        if(childs != 0)
          return;

        if(parent != NULL){
          if(items.size() == parent->items.size()){
            ValueTreeIterator it = items.begin();

            while(it != items.end()){
              std::cout << *it << std::endl;
              it++;
            }
            //error("Error");
          }
        }

        center = bbox.center();

        cgfassert(childs == 0);

        childs = new Self*[8];

        for(int i=0;i<8;i++){
          childs[i] = new Self;
          childs[i]->parent = this;
          childs[i]->id = node_id;
          node_id++;
        }

        Vector4<Y> box[3];
        box[0][0] = bbox.getMin()[0];
        box[1][0] = center[0];
        box[2][0] = bbox.getMax()[0];

        box[0][1] = bbox.getMin()[1];
        box[1][1] = center[1];
        box[2][1] = bbox.getMax()[1];

        box[0][2] = bbox.getMin()[2];
        box[1][2] = center[2];
        box[2][2] = bbox.getMax()[2];

        childs[0]->bbox.setMin(Vector4<Y>(box[0][0], box[0][1], box[0][2], 0));
        childs[0]->bbox.setMax(Vector4<Y>(box[1][0], box[1][1], box[1][2], 0));
        childs[0]->bbox.computeCorners();

        childs[1]->bbox.setMin(Vector4<Y>(box[1][0], box[0][1], box[0][2], 0));
        childs[1]->bbox.setMax(Vector4<Y>(box[2][0], box[1][1], box[1][2], 0));
        childs[1]->bbox.computeCorners();

        childs[2]->bbox.setMin(Vector4<Y>(box[0][0], box[1][1], box[0][2], 0));
        childs[2]->bbox.setMax(Vector4<Y>(box[1][0], box[2][1], box[1][2], 0));
        childs[2]->bbox.computeCorners();

        childs[3]->bbox.setMin(Vector4<Y>(box[1][0], box[1][1], box[0][2], 0));
        childs[3]->bbox.setMax(Vector4<Y>(box[2][0], box[2][1], box[1][2], 0));
        childs[3]->bbox.computeCorners();

        childs[4]->bbox.setMin(Vector4<Y>(box[0][0], box[0][1], box[1][2], 0));
        childs[4]->bbox.setMax(Vector4<Y>(box[1][0], box[1][1], box[2][2], 0));
        childs[4]->bbox.computeCorners();

        childs[5]->bbox.setMin(Vector4<Y>(box[1][0], box[0][1], box[1][2], 0));
        childs[5]->bbox.setMax(Vector4<Y>(box[2][0], box[1][1], box[2][2], 0));
        childs[5]->bbox.computeCorners();

        childs[6]->bbox.setMin(Vector4<Y>(box[0][0], box[1][1], box[1][2], 0));
        childs[6]->bbox.setMax(Vector4<Y>(box[1][0], box[2][1], box[2][2], 0));
        childs[6]->bbox.computeCorners();

        childs[7]->bbox.setMin(Vector4<Y>(box[1][0], box[1][1], box[1][2], 0));
        childs[7]->bbox.setMax(Vector4<Y>(box[2][0], box[2][1], box[2][2], 0));
        childs[7]->bbox.computeCorners();

        ValueTreeIterator it = items.begin();
        ValueTreeIterator end = items.end();

        while(it != end){
          childs[quadrant(getPosition<T, Y>(*it))]->insert(*it);
          it++;
        }

        items.clear();

        for(int i=0;i<8;i++){
          childs[i]->split(depth, max, curDepth+1, empty_split);
        }
      }

      bool hasToSplit(){
        Self* node;
        //bool split = false;
        /*0*/
        node = neighbor(0);
        if(node){
          if(node->childs != 0){
            if(node->childs[1]->isLeaf() ||
               node->childs[3]->isLeaf() ||
               node->childs[7]->isLeaf() ||
               node->childs[5]->isLeaf()){
              return true;
            }
          }
        }
        /*1*/
        node = neighbor(1);
        if(node){
          if(node->childs != 0){
            if(node->childs[0]->isLeaf() ||
               node->childs[2]->isLeaf() ||
               node->childs[4]->isLeaf() ||
               node->childs[6]->isLeaf()){
              return true;
            }
          }
        }
        /*2*/
        node = neighbor(2);
        if(node){
          if(node->childs != 0){
            if(node->childs[2]->isLeaf() ||
               node->childs[3]->isLeaf() ||
               node->childs[6]->isLeaf() ||
               node->childs[7]->isLeaf()){
              return true;
            }
          }
        }
        /*3*/
        node = neighbor(3);
        if(node){
          if(node->childs != 0){
            if(node->childs[0]->isLeaf() ||
               node->childs[1]->isLeaf() ||
               node->childs[4]->isLeaf() ||
               node->childs[5]->isLeaf()){
              return true;
            }
          }
        }
        /*4*/
        node = neighbor(4);
        if(node){
          if(node->childs != 0){
            if(node->childs[4]->isLeaf() ||
               node->childs[5]->isLeaf() ||
               node->childs[6]->isLeaf() ||
               node->childs[7]->isLeaf()){
              return true;
            }
          }
        }
        /*5*/
        node = neighbor(5);
        if(node){
          if(node->childs != 0){
            if(node->childs[0]->isLeaf() ||
               node->childs[1]->isLeaf() ||
               node->childs[2]->isLeaf() ||
               node->childs[3]->isLeaf()){
              return true;
            }
          }
        }
        return false;
      }

      Tree<T> items;
      T sum;
      int n_items;

      template<class TT, class YY>
      friend class CGF::Octree;

      template<class TT, class YY>
      friend class GlOctree;

      template<class YY>
      friend class NBody;
    protected:
      Self* parent;
      Self** childs;
      BBox<Y> bbox;
      Vector4<Y> center;
    };
  }
}

#endif/*OCTREE_HPP*/
