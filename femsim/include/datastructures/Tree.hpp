/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef TREE_HPP
#define TREE_HPP
#include <iostream>
#include "core/cgfdefs.hpp"
#include <stdlib.h>
#include "math/Compare.hpp"
#include <string.h>
#include <datastructures/List.hpp>
#include <datastructures/Pair.hpp>
#include "core/types.hpp"

/*AVL tree*/

namespace CGF{
  template<class T>
  class Tree;

  template<class T>
  class TreeIterator;

  template<class T>
  class TreeReduce;



  namespace TreeInternals{
    template<typename T>
    struct TreeNode{
    public:
      typedef T           value_type;
      typedef TreeNode<T> Self;

      TreeNode(value_type& t, int idx):parent(0), l_child(0),
                                       r_child(0), data(t),
                                       index(idx), balance(0), depth(0){
      }

      ~TreeNode(){
        if(l_child){
          delete l_child;
          l_child = 0;
        }

        if(r_child){
          delete r_child;
          r_child = 0;
        }

        parent = 0;
      }

      void computeDepth(){
        int left_depth = 0;
        int right_depth = 0;

        if(l_child){
          left_depth = l_child->depth;
        }
        if(r_child){
          right_depth = r_child->depth;
        }

        depth = 1+MAX(left_depth, right_depth);
        balance = left_depth - right_depth;
      }

      Self* parent;
      Self* l_child;
      Self* r_child;
      value_type data;

      int index;
      int balance;
      int depth;
    };

    template<typename T>
    struct TreeIterator{
    public:
      typedef T               value_type;
      typedef TreeNode<T>     Node;
      typedef TreeIterator<T> Self;

      TreeIterator(Node* p):ptr(p){
      }

      int getIndex(){
        return ptr->index;
      }

      value_type& operator*(){
        return ptr->data;
      }

      value_type* operator->(){
        return &(ptr->data);
      }

      /*Post increment*/
      Self operator++(int i){
        Self it(ptr);
        next();
        return it;
      }

      /*Post decrement*/
      Self operator--(int i){
        Self it(ptr);
        prev();
        return it;
      }

      /*Pre increment*/
      Self& operator++(){
        next();
        return *this;
      }

      /*Pre decrement*/
      Self& operator--(){
        prev();
        return *this;
      }

      /*Equality*/
      bool operator==(const Self& i)const{
        return ptr == i.ptr;
      }

      /*Equality*/
      bool operator!=(const Self& i)const{
        return ptr != i.ptr;
      }

      //protected:
      Node* ptr;

      void next(){
        if(ptr->r_child){
          /*Find left most child*/
          ptr = ptr->r_child;
          while(ptr->l_child){
            ptr = ptr->l_child;
          }
          /*Arrived at left most child*/
        }else{
          /*Go to parent node if this node is a left child of the parent*/
          if(ptr->parent){
            if(ptr->parent->l_child == ptr){
              ptr = ptr->parent;
            }else if(ptr->parent->r_child == ptr){
              /*Find first parent node for which
                the current node is a left child*/
              while(ptr->parent){
                if(ptr->parent->r_child == ptr){
                  ptr = ptr->parent;
                }else if(ptr->parent->l_child == ptr){
                  ptr = ptr->parent;
                  return;
                }
              }
              ptr = 0;
            }
          }else{
            ptr = 0;
          }
        }
      }

      void prev(){
        //error();
        //message("pointer = %p", ptr);
        if(ptr->l_child){
          /*Find right most child*/
          ptr = ptr->l_child;
          while(ptr->r_child){
            ptr = ptr->r_child;
          }
          /*Arrived at left most child*/
        }else{
          /*Go to parent node if this node is a right child of the parent*/
          if(ptr->parent){
            if(ptr->parent->r_child == ptr){
              ptr = ptr->parent;
            }else if(ptr->parent->l_child == ptr){
              /*Find first parent node for which
                the current lode is a left child*/
              while(ptr->parent){
                if(ptr->parent->l_child == ptr){
                  ptr = ptr->parent;
                }else if(ptr->parent->r_child == ptr){
                  ptr = ptr->parent;
                  return;
                }
              }
              ptr = 0;
            }
          }else{
            ptr = 0;
          }
        }
      }

      friend class Tree<T>;
    };
  }

  template<class T>
  class Tree{
  public:
    typedef T                              value_type;
    typedef TreeInternals::TreeNode<T>     Node;
    typedef TreeInternals::TreeIterator<T> Iterator;
    typedef Tree<T>                        Self;

    Tree(bool(*le)(const T&, const T&) = 0,
         bool(*eq)(const T&, const T&) = 0):root(0), beginNode(0),
                                            updateBegin(true),
                                            lastNode(0),
                                            updateLast(true),
                                            order(0), sz(0){
      l_less = le;
      l_equal = eq;
    }

    Tree(const List<value_type>& list):root(0), beginNode(0),
                                       updateBegin(true),
                                       lastNode(0),
                                       updateLast(true),
                                       order(0), sz(0){
      l_less = 0;
      l_equal = 0;

      typename List<T>::Iterator lit = list.begin();
      while(lit != list.end()){
        insert(*lit++);
      }
    }

    ~Tree(){
      if(root){
        delete root;
      }
    }

    static const int undefinedIndex = -1;

    /*Implicitly advances the iterator to the next node*/
    void remove(Iterator& it){
      Node* node = it.ptr;

      if(node->l_child != 0 &&
         node->r_child != 0){
        /*Node will be reused and will contain the next logical value,
          no need to update the iterator*/
      }else{
        /*Node will be deleted, advance iterator to next*/
        it.next();
      }

      deleteNode2(node);
    }

    /*Removes the node having value t.*/
    void remove(value_type& t){
      Node* node = findNode(t);
      if(node == 0){
        return;
      }
      deleteNode2(node);

      if(node == beginNode){
        updateBegin = true;
      }

      if(node == lastNode){
        updateLast = true;
      }
    }

    int size()const{
      return sz;
    }

    int getDepth(){
      return getDepth(root);
    }

    void getBalance(Node* node)const{
      if(node){
        message("node = %p, depth = %d, balance = %d",
                node, node->depth, node->balance);

        getBalance(node->l_child);
        getBalance(node->r_child);
      }
    }

    int getBalance()const{
      if(root){
        getBalance(root);
        return root->balance;
      }
      return 0;
    }

    void uniqueInsert(value_type& t, int index = 0){
      int findex = findIndex(t);

      if(findex == undefinedIndex){
        insert(t, index);
      }
    }

    void insertc(value_type t, int index = 0){
      T tt = t;
      insert(tt, index);
    }

    /*AVL insert*/
    void insert(value_type& t, int index = 0){
      sz++;

      if(root == 0){
        root = new Node(t, index);
        updateDepth(root, false);
        return;
      }
#if 1
      if(beginNode && (updateBegin == false)){
        /*beginNode is valid*/
        if(l_less){
          if(l_less(t, beginNode->data)){
            /*New value is smaller than begin, update beginnode*/
            updateBegin = true;
          }
        }else{
          if(Compare<T>::less(t, beginNode->data)){
            /*New value is smaller than begin, update beginnode*/
            updateBegin = true;
          }
        }
      }
      //message("lastNode = %p, updateLast = %d", lastNode, updateLast);
      if(lastNode && (updateLast == false)){
        //message("last node = valid");
        /*lastNode is valid*/
        if(l_less){
          if(!l_less(t, lastNode->data)){
            /*New value is larger than last, update lastnode*/
            updateLast = true;
            //message("last node must be updated");
          }
        }else{
          if(!Compare<T>::less(t, lastNode->data)){
            /*New value is larger than last, update lastnode*/
            updateLast = true;
            //message("last node must be updated");
          }
        }
      }
#endif
      Node* curr = root;

      if(l_less){
        while(curr){
          if(l_less(t, curr->data)){
            /*Traverse left*/
            if(curr->l_child){
              curr = curr->l_child;
            }else{
              /*Left child does not exist, create*/
              curr->l_child = new Node(t, index);
              curr->l_child->parent = curr;
              updateDepth(curr->l_child, false);
              return;
            }
          }else{
            /*Traverse right*/
            if(curr->r_child){
              curr = curr->r_child;
            }else{
              curr->r_child = new Node(t, index);
              curr->r_child->parent = curr;
              updateDepth(curr->r_child, false);
              return;
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::less(t, curr->data)){
            /*Traverse left*/
            if(curr->l_child){
              curr = curr->l_child;
            }else{
              /*Left child does not exist, create*/
              curr->l_child = new Node(t, index);
              curr->l_child->parent = curr;
              updateDepth(curr->l_child, false);
              return;
            }
          }else{
            /*Traverse right*/
            if(curr->r_child){
              curr = curr->r_child;
            }else{
              curr->r_child = new Node(t, index);
              curr->r_child->parent = curr;
              updateDepth(curr->r_child, false);
              return;
            }
          }
        }
      }
    }

    Iterator begin(){
      if(updateBegin){
        /*Find left most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->l_child){
          curr = curr->l_child;
        }
        /*Current node is left most*/
        Iterator it(curr);

        updateBegin = false;

        beginNode = curr;
        return it;
      }else{
        return Iterator(beginNode);
      }
    }

    Iterator last(){
      if(updateLast){
        /*Find right most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->r_child){
          curr = curr->r_child;
        }
        /*Current node is left most*/
        Iterator it(curr);

        updateLast = false;
        lastNode = curr;
        return it;
      }else{
        return Iterator(lastNode);
      }
    }

    const Iterator begin()const{
      if(updateBegin){
        /*Find left most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->l_child){
          curr = curr->l_child;
        }
        /*Current node is left most*/
        beginNode = curr;
        updateBegin = false;
        const Iterator it(curr);
        return it;
      }else{
        const Iterator it(beginNode);
        return it;
      }
    }

    const Iterator last()const{
      if(updateLast){
        /*Find left most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->r_child){
          curr = curr->r_child;
        }
        /*Current node is left most*/
        const Iterator it(curr);

        updateLast = false;
        lastNode = curr;
        return it;
      }else{
        const Iterator it(lastNode);
        return it;
      }
    }

    Iterator end(){
      Iterator it(0);
      return it;
    }

    const Iterator end()const{
      const Iterator it(0);
      return it;
    }

    Iterator find(const value_type& t)const{
      Node* f = findNode(t);
      if(f==0){
        return end();
      }

      return Iterator(f);
    }

    Iterator findClosestBefore(const value_type& t)const{
      Node* f = findClosestNodeBefore(t);
      if(f==0){
        return end();
      }

      return Iterator(f);
    }

    int findClosestIndexBefore(const value_type& t)const{
      Node* f = findClosestNodeBefore(t);
      if(f == 0)
        return undefinedIndex;
      else
        return f->index;
    }

    int findClosestIndexAfter(const value_type& t)const{
      Node* f = findClosestNodeAfter(t);
      if(f == 0)
        return undefinedIndex;
      else
        return f->index;
    }


    int findIndex(const value_type& t)const{
      Node* f = findNode(t);
      if(f == 0)
        return undefinedIndex;
      else
        return f->index;
    }

    void traverse(){
      ltraverse(root);
    }

    void clear(){
      if(root){
        delete root;
        root = 0;

        sz = 0;
        beginNode = 0;
        lastNode = 0;
        updateBegin = true;
        updateLast = true;
      }
    }

    void consistent(){
      message("Consistency check");
      consistent(root, 0);
      message("End consistency check");
    }

    template<class U> friend inline std::ostream& operator<<(std::ostream& os,
                                                             const Tree<U>& t);

  public:
    Tree(const Self& t){
      *this = t;
    }

    Self& operator=(const Self& t){
      if(this != &t){
        clear();
        Iterator it = t.begin();
        while(it != t.end()){
          T val = it.ptr->data;
          int index = it.ptr->index;
          insert(val, index);
          it++;
        }
      }
      return *this;
    }

  protected:
    /*User defined compare functions*/
    bool(*l_less )(const value_type&, const value_type&);
    bool(*l_equal)(const value_type&, const value_type&);

    int getDepth(Node* node){
      return node->depth;
    }

    void consistent(Node* node, Node* parent){
      if(node != 0){
        cgfassert(node->parent == parent);
        if(node->parent != parent){
          error("inconsistent tree");
        }
        consistent(node->l_child, node);
        consistent(node->r_child, node);
      }
    }


#if 0
    void computeDepth(Node* node, int depth = 0){
      if(node){
        computeDepth(node->l_child, depth+1);
        computeDepth(node->r_child, depth+1);
      }

      if(node){
        node->computeDepth();
        message("node level = %d, depth = %d, balance = %d",
                depth, node->depth, node->balance);
        if(node->l_child){
          message("left depth = %d", node->l_child->depth);
        }
        if(node->r_child){
          message("right depth = %d", node->r_child->depth);
        }
      }
    }
#endif

    /*If cont == false, we can skip the traversal to the
      root. Otherwise, we must continue the traversal.*/
    void updateDepth(Node* node, bool deleting, bool cont = false){
      node->computeDepth();

      if(deleting && node->balance == 0){
        cont = true;
      }

      cgfassert(node->balance >= -2 && node->balance <= 2);

      if(!cont){
        /*We can stop the traversal*/
        if(!deleting){
          if(node->l_child != 0 || node->r_child != 0){
            if(node->balance == 0){
              return;
            }
          }
        }else{
          if(node->l_child != 0 && node->r_child != 0){
            if(node->balance == 1 || node->balance == -1){
              return;
            }
          }
        }
      }

      if(node->balance == 2){
        /*Left subtree is deeper*/
        Node* P = node;
        Node* L = node->l_child;

        if(L->balance == -1){
          /*Left-right case*/
          rotL(L);
          rotR(P);
        }else if(L->balance == 1){
          /*Left-left case*/
          rotR(P);
        }else{
          if(deleting){
            rotR(P);
          }else{
            error();
          }
        }
        /*Update parent*/
        updateDepth(P, deleting, cont);
      }else if(node->balance == -2){
        /*Right subtree is deeper*/
        Node* P = node;
        Node* R = node->r_child;

        if(R->balance == -1){
          /*Right-tight case*/
          rotL(P);
        }else if(R->balance == 1){
          /*Left-right case*/
          rotR(R);
          rotL(P);
        }else{
          if(deleting){
            rotL(P);
          }else{
            error();
          }
        }
        /*Update parent*/
        updateDepth(P, deleting, cont);
      }else if(node->parent){
        /*Update parent*/
        updateDepth(node->parent, deleting, cont);
      }
    }

    Node* rotL(Node* node){
      Node* right  = node->r_child;
      Node* parent = node->parent;
      Node* tmp;
      int dir = 0;
      if(parent){
        if(parent->l_child == node){
          dir = -1;
        }else{
          dir = 1;
        }
      }

      tmp = node->r_child->l_child;
      right->l_child = node;
      node->r_child = tmp;
      if(tmp){
        tmp->parent = node;
      }

      node->parent = right;
      right->parent = parent;

      /*Update parent*/
      if(dir == 0){
        /*Root*/
        root = right;
      }else if(dir == -1){
        parent->l_child = right;
      }else{
        parent->r_child = right;
      }

      /*Update depth/balance info*/
      if(node)node->computeDepth();
      if(right)right->computeDepth();
      if(parent)parent->computeDepth();

      return right;
    }

    Node* rotR(Node* node){
      Node* left  = node->l_child;
      Node* parent = node->parent;
      Node* tmp;
      int dir = 0;
      if(parent){
        if(parent->l_child == node){
          dir = -1;
        }else{
          dir = 1;
        }
      }

      tmp = node->l_child->r_child;
      left->r_child = node;
      node->l_child = tmp;
      if(tmp){
        tmp->parent = node;
      }

      node->parent = left;
      left->parent = parent;

      /*Update parent*/
      if(dir == 0){
        /*Root*/
        root = left;
      }else if(dir == -1){
        parent->l_child = left;
      }else{
        parent->r_child = left;
      }

      /*Update depth/balance info*/
      if(node)node->computeDepth();
      if(left)left->computeDepth();
      if(parent)parent->computeDepth();

      return left;
    }

    void deleteNode(Node* node){
      if(node == beginNode){
        updateBegin = true;
      }

      if(node->l_child == 0 && node->r_child == 0){
        if(node == root){
          root = 0;
        }else{
          /*Remove this node*/
          Node* parent = node->parent;
          if(parent->l_child == node){
            parent->l_child = 0;
          }
          if(parent->r_child == node){
            parent->r_child = 0;
          }
        }
        delete node;
        return;
      }else if(node->l_child == 0 && node->r_child != 0){
        /*Left child is NULL. Replace current node with right child.*/
        Node* parent = node->parent;
        if(parent != 0){
          if(parent->l_child == node){
            parent->l_child = node->r_child;
            node->r_child->parent = parent;
          }else if(parent->r_child == node){
            parent->r_child = node->r_child;
            node->r_child->parent = parent;
          }
        }else{
          /*Node == Root*/
          root = node->r_child;
          root->parent = 0;
        }
        node->r_child = 0;

        delete node;

        return;
      }else if(node->l_child != 0 && node->r_child == 0){
        /*Right child is NULL. Replace current node with left child.*/
        Node* parent = node->parent;
        if(parent != 0){
          if(parent->l_child == node){
            parent->l_child = node->l_child;
            node->l_child->parent = parent;
          }else if(parent->r_child == node){
            parent->r_child = node->l_child;
            node->l_child->parent = parent;
          }
        }else{
          /*Node == Root*/
          root = node->l_child;
          root->parent = 0;
        }
        node->l_child = 0;

        order = 1 - order;

        delete node;

        return;
      }
    }

    void deleteNode2(Node* node){
      Node* parent = node->parent;

      sz--;

      if(node->l_child == 0 && node->r_child == 0){
        deleteNode(node);
        if(parent)updateDepth(parent, true);
      }else if(node->l_child == 0 && node->r_child != 0){
        deleteNode(node);
        if(parent)updateDepth(parent, true);
      }else if(node->l_child != 0 && node->r_child == 0){
        deleteNode(node);
        if(parent)updateDepth(parent, true);
      }else{
        cgfassert(node->l_child != 0 && node->r_child != 0);
        /*Both childs are not NULL*/
        Node* currentNode=0;

        if(order == 0){
          /*Find current node in-order successor*/
          currentNode = node->r_child;
          while(currentNode->l_child){
            currentNode = currentNode->l_child;
          }

          cgfassert(currentNode->l_child == 0);
        }else{
          /*Find current node in-order predecessor*/
          currentNode = node->l_child;
          while(currentNode->r_child){
            currentNode = currentNode->r_child;
          }

          cgfassert(currentNode->r_child == 0);
        }

        node->data = currentNode->data;
        node->index = currentNode->index;

        parent = currentNode->parent;

        deleteNode(currentNode);

        if(parent){
          updateDepth(parent, true);
        }
        //return;
      }
    }

    Node* findClosestNodeBefore(const value_type& t)const{
      Node* curr = root;

      if(l_equal && l_less){
        while(curr){
          if(l_equal(t, curr->data)){
            Iterator it(curr);
            it.prev();
            return it.ptr;
          }else{
            if(l_less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::equal(t, curr->data)){
            Iterator it(curr);
            it.prev();
            return it.ptr;
          }else{
            if(Compare<T>::less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }
          }
        }
      }
      return 0;
    }

    Node* findClosestNodeAfter(const value_type& t)const{
      Node* curr = root;

      if(l_equal && l_less){
        while(curr){
          if(l_equal(t, curr->data)){
            Iterator it(curr);
            it.next();
            return it.ptr;
          }else{
            if(l_less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::equal(t, curr->data)){
            Iterator it(curr);
            it.next();
            return it.ptr;
          }else{
            if(Compare<T>::less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }
          }
        }
      }
      return 0;
    }

    Node* findNode(const value_type& t)const{
      Node* curr = root;

      if(root == 0){
        return 0;
      }

      if(l_equal && l_less){
        while(curr){
          if(l_equal(t, curr->data)){
            return curr;
          }else{
            if(l_less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                return 0;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                return 0;
              }
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::less(t, curr->data)){
            if(curr->l_child){
              curr = curr->l_child;
            }else{
              return 0;
            }
          }else{
            if(Compare<T>::less(curr->data, t)){
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                return 0;
              }
            }else{
              return curr;
            }
          }
        }
      }
      return 0;
    }

    void ltraverse(Node* subtree){
      if(subtree == 0){
        return;
      }

      ltraverse(subtree->l_child);
      message("[%d], %d || t = %p, p = %p, l = %p, r = %p, depth = %d, bal = %d",
              subtree->index, subtree->data, subtree,
              subtree->parent, subtree->l_child, subtree->r_child,
              subtree->depth, subtree->balance);
      ltraverse(subtree->r_child);
    }

    Node* root;
    mutable Node* beginNode;
    mutable bool updateBegin;

    mutable Node* lastNode;
    mutable bool updateLast;

#if 0
    Node* index;
    int last_direction;
#endif
    int order;
    int sz;

    friend class TreeReduce<T>;
  };

  template<class T, class U>
  class MapPair{
  public:
    MapPair(){
    }

    MapPair(T _key, U _data):key(_key), data(_data){
    }

    MapPair(const MapPair<T, U>& p){
      key = p.key;
      data = p.data;
    }

    void operator=(const MapPair<T, U>& p){
      key = p.key;
      data = p.data;
    }

    inline const T getKey()const{
      return key;
    }

    inline T getKey(){
      return key;
    }

    inline const U& getData()const{
      return data;
    }

    inline U& getData(){
      return data;
    }

    template<class V, class W> friend inline std::ostream& operator<<(std::ostream& os, const MapPair<V, W>& m);
  protected:
    T key;
    U data;
  };

  template<class T, class U>
  class Compare<MapPair<T, U> >{
  public:
    static bool less(const MapPair<T, U>& a, const MapPair<T, U>& b){
      return Compare<T>::less(a.getKey(), b.getKey());
    }

    static bool equal(const MapPair<T, U>& a, const MapPair<T, U>& b){
      return Compare<T>::equal(a.getKey(), b.getKey());
    }
  };

  template<class T, class U>
  class Map{
  public:
    typedef T key_type;
    typedef U data_type;
    typedef typename Tree<MapPair<key_type, data_type> >::Iterator Iterator;
    Map(){
    }

    Iterator begin(){
      return tree.begin();
    }

    Iterator end(){
      return tree.end();
    }

    void insert(key_type& key, const data_type& val){
      MapPair<key_type, data_type> pair(key, val);
      tree.insert(pair);
    }

    void insert(key_type& key, data_type& val){
      MapPair<key_type, data_type> pair(key, val);
      tree.insert(pair);
    }

    Iterator find(const key_type& key)const{
      MapPair<key_type, data_type> pair(key, U());
      return tree.find(pair);
    }

    void clear(){
      tree.clear();
    }

    int size()const{
      return tree.size();
    }

    void remove(Iterator& it){
      tree.remove(it);
    }


    template<class V, class W> friend inline std::ostream& operator<<(std::ostream& os, const Map<V, W>& m);

  protected:
    Tree<MapPair<key_type, data_type> > tree;
  };

#if 0
  template<class U>
  inline void getSearchTree(Tree<U>& t, List<U>& list){
    Iterator<U> it = list.begin();
    while(it != list.end()){
      t.insert(*it++,1);
    }
  }
#endif

  template<class U>
  inline std::ostream& operator<<(std::ostream& os, const Tree<U>& t){
    typename Tree<U>::Iterator it = t.begin();
    U last = *it;
    bool first = true;
    while(it != t.end()){
      int index = it.getIndex();
      if(!first){
        if(!Compare<U>::less(last, *it)){
          std::cout << *it << "(" << index << ")" << std::endl;
          error("not ordered");
        }
      }
      int balance = it.ptr->balance;
      last = *it;
      first = false;
      os << *it++ << "(" << index << ", " << balance << ")" << ',';
    }
    return os;
  }

  template<class T, class U>
  inline std::ostream& operator<<(std::ostream& os, const MapPair<T, U>& t){
    os << "[" << t.key << "] -> " << t.data << ", ";
    return os;
  }

  template<class T, class U>
  inline std::ostream& operator<<(std::ostream& os, const Map<T, U>& t){
    os << t.tree;
    return os;
  }

  template<class T>
  class SumReduce{
  public:
    static void reduction(T* r, T* a, T* b, T* c){
      if(b!=0 && c!=0){
        *r = (*a + *b) + *c;
      }else if(b==0 && c!=0){
        *r = *a + *c;
      }else if(c==0 && b!=0){
        *r = *a + *b;
      }else{
        *r = *a;
      }
    }
  };

  template<class T>
  class TreeReduce{
  public:
    TreeReduce(const Tree<T>* _tree,
               void (*_function)(T*, T*, T*, T*)):tree(_tree),
                                                  function(_function){
    }

    T reduce(){
      T result;
      memset(&result, 0, sizeof(T));
      reduce(&result, tree->root);

      return result;
    }
  protected:
    void reduce(T* a, typename Tree<T>::Node* node){
      T b;
      T c;

      memset(&b, 0, sizeof(T));
      memset(&c, 0, sizeof(T));

      if(node == 0){
        *a = b;
      }else{
        reduce(&b, node->l_child);
        reduce(&c, node->r_child);
        T* pb = &b;
        T* pc = &c;
        if(node->l_child == 0){
          pb = 0;
        }
        if(node->r_child == 0){
          pc = 0;
        }
        (*function)(a, &node->data, pb, pc);
      }
    }

    const Tree<T>* tree;
    void (*function)(T*, T*, T*, T*);
  };
}

#endif/*TREE_HPP*/
