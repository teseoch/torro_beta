/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef ADJACENCYLIST_HPP
#define ADJACENCYLIST_HPP
#include "datastructures/Tree.hpp"
#include<ostream>

namespace CGF{

#define ADJV2
  //#undef ADJV2

  class AdjacencyList{
  public:
    AdjacencyList(long _size, long _max_degree);

    ~AdjacencyList();

    void connect(int a, int b);
    void disconnect(int a, int b);

    int getDegree(int a)const;

    int getConnectedItem(int a, int idx)const;

    void clear(int a);

    void readFromFile(std::ifstream& file);
    void saveToFile(std::ofstream& file)const;

    long getMaxDegree()const{
      return max_degree;
    }

    long getSize()const{
      return size;
    }

    void remap(Tree<int>& mapping);
    void compress(Tree<int>& mapping, int new_size);

    static const int undefinedIndex = -1;

    void print();

  protected:
    void extendSize();
    void extendDegree();


#ifdef ADJV2
    int** adj;
    int*  sizes; /*sizes of arrays stored in adj*/
    int*  degrees; /*degrees of nodes*/
#else
    int* adj;
#endif
    long  size;
    long  max_degree;
  };
}


#endif/*ADJACENCYLIST_HPP*/
