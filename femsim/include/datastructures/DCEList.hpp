/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef DCELIST_HPP
#define DCELIST_HPP

#include <iostream>
#include <fstream>

#include "datastructures/List.hpp"
#include "datastructures/Tree.hpp"
#include "math/Quaternion.hpp"
#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "math/Vector.hpp"
#include "string.h"
#include <limits.h>
#include <map>
#include <list>
#include <stdio.h>
#include "math/BBox.hpp"
#include "geo/Triangle.hpp"
#include "geo/Edge.hpp"
#include "geo/OrientedVertex.hpp"

namespace CGF{
#define DEFAULT_SIZE 100

  //#define DIST_C (T)3.0
#define DIST_C (T)1.2

  //#define NI -1

  /*Tetrahedron -> 4 faces*/
  /*Face        -> 1 half-edge*/
  /*Half-edge   -> 1 origin vertices & 1 faces & 1 twin & 1 next half-edge*/
  /*Vertex      -> 1 halfedge*/

  typedef Vector4f vertex;

  namespace Mesh{
    static const int UndefinedIndex = -1;
    enum GeometryType{
      Deformable,  /*Velocity is defined by translations only*/
      Rigid,       /*Velocity is defined by translation and angular velocity,
                     Geometry should have a center of mass defined*/
      Static,      /*Velocity is always zero*/
      Cloth,       /*Special case of deformable geometry*/
      Undefined    /*Undefined type of geometry*/
    };

    inline bool isRigidOrStatic(GeometryType& type){
      if(type == Rigid || type == Static){
        return true;
      }
      return false;
    }

    inline bool isRigid(GeometryType& type){
      if(type == Rigid){
        return true;
      }
      return false;
    }

    inline bool isStatic(GeometryType& type){
      if(type == Static){
        return true;
      }
      return false;
    }

    inline bool isNotStatic(GeometryType& type){
      return !isStatic(type);
    }
  }

  template<class T, class VD=char>
  class DCELVertex{
  public:
    DCELVertex():coord(0,0,0,0),
                 displCoord(0,0,0,0),
                 displCoordExt(0,0,0,0),
                 leaving(Mesh::UndefinedIndex),
                 normal(0,0,0,0),
                 displNormal(0,0,0,0),
                 data(),
                 id(0),
                 vel_index(0),
                 type(Mesh::Undefined){
    }

    DCELVertex(const Vector4<T>& c, int l):coord(c),
                                           displCoord(c),
                                           displCoordExt(c),
                                           leaving(l),
                                           normal(0,0,0,0),
                                           displNormal(0,0,0,0),
                                           data(),
                                           id(0),
                                           vel_index(0),
                                           type(Mesh::Undefined){
    }

    Vector4<T> coord;         /*Position of this vertex*/
    Vector4<T> displCoord;    /*Displaced position of this vertex*/
    Vector4<T> displCoordExt; /*Extended displaced position of this vertex*/
    int        leaving;       /*A half edge that leaves from this vertex*/
    Vector4<T> normal;        /*Vertex normal, used for smoothening*/
    Vector4<T> displNormal;   /*Displaced vertex normal, used for smoothening*/
    VD         data;          /*User defined data*/
    int        id;
    int        vel_index;     /*Index in velocity vector*/

    Mesh::GeometryType type;

    template <class U, class V>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const DCELVertex<U, V>& v);

    void print()const{
      message("Vertex:: id = %d, leaving = %d, vel_index = %d",
              id, leaving, vel_index);
      std::cout << coord  << displCoord << displCoordExt;
      std::cout << normal << displNormal;
    }

    template<class VD2>
    DCELVertex<T, VD>& operator=(const DCELVertex<T, VD2>& v){
      if((void*)this != (void*)&v){
        coord         = v.coord;
        displCoord    = v.displCoord;
        displCoordExt = v.displCoordExt;
        leaving       = v.leaving;
        normal        = v.normal;
        displNormal   = v.displNormal;
        id            = v.id;
        vel_index     = v.vel_index;
        type          = v.type;

        data          = v.data;
      }
      return *this;
    }

    DCELVertex<T, VD>& operator=(const DCELVertex<T, VD>& v){
      if((void*)this != (void*)&v){
        coord         = v.coord;
        displCoord    = v.displCoord;
        displCoordExt = v.displCoordExt;
        leaving       = v.leaving;
        normal        = v.normal;
        displNormal   = v.displNormal;
        id            = v.id;
        vel_index     = v.vel_index;
        type          = v.type;
        /*Copy vertex specific data*/
        data          = v.data;
      }
      return *this;
    }

    template<class VD2>
    DCELVertex(const DCELVertex<T, VD2>& v){
      *this = v;
    }

    bool operator<(const DCELVertex<T, VD>& b)const{
      return coord < b.coord;
    }
  };

  template<class U, class V>
  inline std::ostream& operator<<(std::ostream& os,
                                  const DCELVertex<U, V>& v){
    v.print();
    return os;
  }


  template<class ED=char>
  class DCELHalfEdge{
  public:
    DCELHalfEdge():origin(Mesh::UndefinedIndex),
                   half_face(Mesh::UndefinedIndex),
                   twin(Mesh::UndefinedIndex),
                   next(Mesh::UndefinedIndex),
                   next_twin(Mesh::UndefinedIndex),
                   visited(false),
                   data(),
                   type(Mesh::Undefined){
    }

    DCELHalfEdge(int o, int hf, int t, int n, int nt):origin(o),
                                                      half_face(hf),
                                                      twin(t),
                                                      next(n),
                                                      next_twin(nt),
                                                      visited(false),
                                                      data(),
                                                      type(Mesh::Undefined){
    }

    int origin;     /*A pointer to the origin vertex of this half edge*/
    int half_face;  /*A pointer to the half face left of this half edge*/
    int twin;       /*A pointer to the half edge right of this edge*/
    int next;       /*A pointer to the next half edge*/
    int next_twin;  /**/
    bool visited;
    ED data;        /*User defined data*/

    Mesh::GeometryType type;

    template<class ED2>
    DCELHalfEdge& operator=(const DCELHalfEdge<ED2>& e){
      if((void*)this != (void*)&e){
        origin    = e.origin;
        half_face = e.half_face;
        twin      = e.twin;
        next      = e.next;
        next_twin = e.next_twin;
        type      = e.type;
        visited   = e.visited;

        data      = e.data;
      }
      return *this;
    }

    DCELHalfEdge& operator=(const DCELHalfEdge& e){
      if((void*)this != (void*)&e){
        origin    = e.origin;
        half_face = e.half_face;
        twin      = e.twin;
        next      = e.next;
        next_twin = e.next_twin;
        type      = e.type;
        visited   = e.visited;

        data      = e.data;
      }
      return *this;
    }

    template<class ED2>
    DCELHalfEdge(const DCELHalfEdge<ED2>& e){
      *this = e;
    }

    void print()const{
      message("HalfEdge:: origin = %d, half_face = %d, twin = %d, next = %d",
              origin, half_face, twin, next);
    }
  };

  template<class T, class FD=char>
  class DCELHalfFace{
  public:
    DCELHalfFace():half_edge(Mesh::UndefinedIndex),
                   twin(Mesh::UndefinedIndex),
                   next(Mesh::UndefinedIndex),
                   normal(0,0,0,0),
                   displNormal(0,0,0,0),
                   visited(false),
                   data(),
                   type(Mesh::Undefined){
    }

    DCELHalfFace(int he, int t, int n):half_edge(he),
                                       twin(t),
                                       next(n),
                                       normal(0,0,0,0),
                                       displNormal(0,0,0,0),
                                       visited(false),
                                       data(),
                                       type(Mesh::Undefined){
    }

    int half_edge;          /*A pointer to a half edge object that has
                              this face as a face object */
    int twin;               /*A pointer to the twin face of this face*/
    int next;               /*A pointer to the next face of a tetrahedron*/
    Vector4<T> normal;      /*Normal of the face*/
    Vector4<T> displNormal; /*Displaced normal of the face*/
    bool visited;
    FD data;                /*User data*/

    Mesh::GeometryType type;

    template<class FD2>
    DCELHalfFace<T, FD>& operator=(const DCELHalfFace<T, FD2>& f){
      if((void*)this != (void*)&f){
        half_edge   = f.half_edge;
        twin        = f.twin;
        next        = f.next;
        normal      = f.normal;
        displNormal = f.displNormal;
        type        = f.type;
        visited     = f.visited;

        data        = FD();//f.data;
      }
      return *this;
    }

    DCELHalfFace<T, FD>& operator=(const DCELHalfFace<T, FD>& f){
      if((void*)this != (void*)&f){
        half_edge   = f.half_edge;
        twin        = f.twin;
        next        = f.next;
        normal      = f.normal;
        displNormal = f.displNormal;
        type        = f.type;
        visited     = f.visited;
        data        = f.data;
      }
      return *this;
    }

    template<class FD2>
    DCELHalfFace(const DCELHalfFace<T, FD2>& f){
      *this = f;
    }

    void print()const{
      message("HalfFace %p:: half_edge = %d, twin = %d", this, half_edge, twin);
    }
  };

  template<class TD=char>
  class DCELTet{
  public:
    DCELTet():half_face(Mesh::UndefinedIndex),data(),
              v1(Mesh::Undefined),
              v2(Mesh::Undefined),
              v3(Mesh::Undefined),
              v4(Mesh::Undefined),
              type(Mesh::Undefined){
    }

    DCELTet(int hf):half_face(hf), data(),
                    v1(Mesh::Undefined),
                    v2(Mesh::Undefined),
                    v3(Mesh::Undefined),
                    v4(Mesh::Undefined),
                    type(Mesh::Undefined){
    }

    int half_face;  /*A pointer to a half face object*/
    TD data;        /*Userdefined data*/
    int v1;         /*Indices to vertices*/
    int v2;
    int v3;
    int v4;

    Mesh::GeometryType type;

    template<class TD2>
    DCELTet& operator=(const DCELTet<TD2>& t){
      if((void*)this != (void*)&t){
        half_face = t.half_face;
        v1        = t.v1;
        v2        = t.v2;
        v3        = t.v3;
        v4        = t.v4;
        type      = t.type;

        data      = t.data;
      }
      return *this;
    }

    DCELTet& operator=(const DCELTet& t){
      if(this != &t){
        half_face = t.half_face;
        v1        = t.v1;
        v2        = t.v2;
        v3        = t.v3;
        v4        = t.v4;
        type      = t.type;

        data      = t.data;
      }
      return *this;
    }

    template<class TD2>
    DCELTet(const DCELTet<TD2>& t){
      *this = t;
    }

    void getIndices(int indices[4])const{
      indices[0] = v1;
      indices[1] = v2;
      indices[2] = v3;
      indices[3] = v4;
    }
  };

  template<class T, class VD=char, class ED=char, class FD=char, class TD=char>
  class DCTetraMesh{
  public:
    DCTetraMesh(){
      init(DEFAULT_SIZE);
    }

    DCTetraMesh(int s){
      init(s);
    }

    void init(int size){
      PRINT_FUNCTION;
      PRINT(this);

      tetrahedra      = 0;
      halfFaces       = 0;
      halfEdges       = 0;
      vertices        = 0;
      n_tetrahedra    = 0;
      n_halfFaces     = 0;
      n_halfEdges     = 0;
      n_vertices      = 0;
      n_rigidObjects  = 0;
      size_tetrahedra = 0;
      size_halfFaces  = 0;
      size_halfEdges  = 0;
      size_vertices   = 0;
      currentEdge     = Mesh::UndefinedIndex;
      currentFace     = Mesh::UndefinedIndex;
      currentTet      = Mesh::UndefinedIndex;


      size_tetrahedra = size;
      size_halfFaces  = 4 * size;
      size_halfEdges  = 4 * 3 * size;
      size_vertices   = 4 * 3 * 3 * size;
      size_map        = size_vertices;

      tetrahedra = new DCELTet<TD>[size_tetrahedra];
      halfFaces  = new DCELHalfFace<T, FD>[size_halfFaces];
      halfEdges  = new DCELHalfEdge<ED>[size_halfEdges];
      vertices   = new DCELVertex<T, VD>[size_vertices];

      vertexObjectMap   = new int[size_vertices];
      //faceObjectMap   = new int[size_halfFaces];
      //edgeObjectMap   = new int[size_halfEdges];
      //tetObjectMap    = new int[size_tetrahedra];
      centerOfMass      = new Vector4<T>[size_map];
      centerOfMassDispl = new Vector4<T>[size_map];
      orientations      = new Quaternion<T>[size_map];
      orientationsDispl = new Quaternion<T>[size_map];

      for(int i=0;i<size_vertices;i++){
        vertexObjectMap[i] = Mesh::UndefinedIndex;
      }

#ifdef USE_THREADS
      pthread_mutex_init(&accessMutex, 0);
#endif
      message("END %s", __FUNCTION__);
    }

    ~DCTetraMesh(){
      PRINT_FUNCTION;
      PRINT(this);

      PRINT(n_vertices);
      PRINT(n_halfEdges);
      PRINT(n_halfFaces);
      PRINT(n_tetrahedra);

      delete[] tetrahedra;
      delete[] halfFaces;
      delete[] halfEdges;
      delete[] vertices;

      delete[] vertexObjectMap;
      //delete[] faceObjectMap;
      //delete[] edgeObjectMap;
      //delete[] tetObjectMap;
      delete[] centerOfMass;
      delete[] centerOfMassDispl;
      delete[] orientations;
      delete[] orientationsDispl;

#ifdef USE_THREADS
      pthread_mutex_destroy(&accessMutex);
#endif
      message("END %s", __FUNCTION__);
    }

    template<class VD2, class ED2, class FD2, class TD2>
    DCTetraMesh(const DCTetraMesh<VD2, ED2, FD2, TD2>& m){
      PRINT_FUNCTION;
      PRINT(this);
      init(DEFAULT_SIZE);
      PRINT(this);
      *this = m;
      message("END %s", __FUNCTION__);
    }

    template<class VD2, class ED2, class FD2, class TD2>
    DCTetraMesh(const DCTetraMesh<VD2, ED2, FD2, TD2>* m){
      PRINT_FUNCTION;
      PRINT(this);
      init(DEFAULT_SIZE);
      PRINT(this);
      merge(m);
      message("END %s", __FUNCTION__);
    }

    template<class VD2, class ED2, class FD2, class TD2>
    DCTetraMesh& operator=(const DCTetraMesh<VD2, ED2, FD2, TD2>& m){
      if((void*)&m != (void*)this){
        PRINT_FUNCTION;
        PRINT(this);

        PRINT(size_vertices);
        PRINT(size_halfFaces);
        PRINT(size_halfEdges);
        PRINT(size_tetrahedra);

        message("operator= this = %p, other = %p", this, &m);

        //getchar();

        n_vertices   = 0;
        n_halfEdges  = 0;
        n_halfFaces  = 0;
        n_tetrahedra = 0;

        merge(&m);

        message("END %s", __FUNCTION__);
      }
      return *this;
    }

    void transform(Matrix44<T> mat){
      for(int i=0;i<n_vertices;i++){
        /*Make homogene coordinates*/
        vertices[i].coord[3] = (T)1.0;

        Vector4<T> res = mat * vertices[i].coord;

        vertices[i].coord = res;
        vertices[i].coord[3] = (T)0.0;
      }

      for(int i=0;i<n_rigidObjects;i++){
        /*Make homogene coordinates*/
        centerOfMass[i][3] = (T)1.0;

        Vector4<T> res = mat * centerOfMass[i];

        centerOfMass[i] = res;
        centerOfMass[i][3] = (T)0.0;
      }
    }

    template<class VD2, class ED2, class FD2, class TD2>
    void merge(const DCTetraMesh<T, VD2, ED2, FD2, TD2>* m){
      int old_n_vertices  = n_vertices;
      int old_n_halfEdges = n_halfEdges;
      int old_n_halfFaces = n_halfFaces;
      //int old_n_tetrahedra = n_tetrahedra;

      message("merge this = %p, other = %p", this, &m);

      int old_n_rigid_objects = n_rigidObjects;

      PRINT(n_vertices);
      PRINT(n_halfEdges);
      PRINT(n_halfFaces);
      PRINT(n_tetrahedra);

      //getchar();

      for(int i=0;i<m->n_vertices;i++){
        DCELVertex<T, VD> v = m->vertices[i];
        if(v.leaving   != Mesh::UndefinedIndex) v.leaving += old_n_halfEdges;
        v.id      += old_n_vertices;
        addVertex(v);

        if(m->vertexObjectMap[i] != Mesh::UndefinedIndex){
          vertexObjectMap[n_vertices-1] =
            m->vertexObjectMap[i] + old_n_rigid_objects;
        }
      }

      for(int i=0;i<m->n_halfEdges;i++){
        DCELHalfEdge<ED> e = m->halfEdges[i];
        if(e.origin    != Mesh::UndefinedIndex) e.origin    += old_n_vertices;
        if(e.half_face != Mesh::UndefinedIndex) e.half_face += old_n_halfFaces;
        if(e.twin      != Mesh::UndefinedIndex) e.twin      += old_n_halfEdges;
        if(e.next      != Mesh::UndefinedIndex) e.next      += old_n_halfEdges;
        if(e.next_twin != Mesh::UndefinedIndex) e.next_twin += old_n_halfEdges;
        addHalfEdge(e);
      }

      for(int i=0;i<m->n_halfFaces;i++){
        DCELHalfFace<T, FD> f = m->halfFaces[i];
        if(f.half_edge != Mesh::UndefinedIndex) f.half_edge += old_n_halfEdges;
        if(f.twin      != Mesh::UndefinedIndex) f.twin      += old_n_halfFaces;
        if(f.next      != Mesh::UndefinedIndex) f.next      += old_n_halfFaces;
        addHalfFace(f);
      }

      for(int i=0;i<m->n_tetrahedra;i++){
        DCELTet<TD> t = m->tetrahedra[i];
        if(t.half_face != Mesh::UndefinedIndex) t.half_face += old_n_halfFaces;
        addTetrahedron(t);
      }

      /*Merge rigid data*/
      for(int i=0;i<m->n_rigidObjects;i++){
        int objectId = n_rigidObjects;
        addCenterOfMass(m->centerOfMass[i], 0);

        centerOfMass[objectId]      = m->centerOfMass[i];
        centerOfMassDispl[objectId] = m->centerOfMassDispl[i];
        orientations[objectId]      = m->orientations[i];
        orientationsDispl[objectId] = m->orientationsDispl[i];
      }
    }

    void dumpVertices(){
      for(int i=0;i<n_vertices;i++){
        vertices[i].print();
      }
    }

  private:
    void findAllConnectedFaces(int face,
                               Tree<int>* vertexFace,
                               Tree<int>* faceVertex,
                               List<int>* currentFaces,
                               Tree<int>* faces,
                               bool* visitedFaces,
                               bool* visitedVertices)const{
      if(visitedFaces[face] == true){
        return;
      }

      int lastCurrentEdge = currentEdge;

      visitedFaces[face] = true;
      faces->remove(face);

      currentFaces->append(face);

      //Visit all connected vertices
      setCurrentEdge(halfFaces[face].half_edge);

      for(int i=0;i<3;i++){
        int vertex = getOriginVertex();
        if(visitedVertices[vertex] == false){
          visitedVertices[vertex] = true;

          Tree<int> connectedFaces;
          getConnectedFacesOfVertex(vertex, connectedFaces);

          /*Iterate over all connected faces*/
          Tree<int>::Iterator fit = connectedFaces.begin();
          while(fit != connectedFaces.end()){
            int cface = *fit++;
            findAllConnectedFaces(cface, vertexFace, faceVertex, currentFaces,
                                  faces, visitedFaces, visitedVertices);
          }
        }
        nextEdge();
      }

      currentEdge = lastCurrentEdge;
    }

  public:
    List<List<int> > getAllConnectedFaces()const{
      Tree<int>* vertexFaceTrees = new Tree<int>[n_vertices];
      Tree<int>* faceVertexTrees = new Tree<int>[n_halfFaces];
      Tree<int>  faces;

      bool* visitedVertices = new bool[n_vertices];
      bool* visitedFaces    = new bool[n_halfFaces];

      PRINT_FUNCTION;

      for(int i=0;i<n_vertices;i++){
        visitedVertices[i] = false;
      }

      message("visited reset");

      for(int i=0;i<n_halfFaces;i++){
        setCurrentEdge(halfFaces[i].half_edge);

        for(int j=0;j<3;j++){
          int originVertex = getOriginVertex();
          vertexFaceTrees[originVertex].uniqueInsert(i);
          faceVertexTrees[i].uniqueInsert(originVertex);
          nextEdge();
        }
        faces.uniqueInsert(i);
        visitedFaces[i] = false;
      }

       int n_surfaces = 0;
      List<List<int> > surfaces;

      while(faces.size() != 0){
        Tree<int>::Iterator fit = faces.begin();
        int face = *fit++;

        PRINT(face);

        List<int> currentFaces;

        findAllConnectedFaces(face, vertexFaceTrees, faceVertexTrees,
                              &currentFaces, &faces,
                              visitedFaces, visitedVertices);

        surfaces.append(currentFaces);
        n_surfaces++;
      }

      delete[] vertexFaceTrees;
      delete[] faceVertexTrees;

      delete[] visitedVertices;
      delete[] visitedFaces;

      return surfaces;
    }

    void connectHalfEdges2(){
      PRINT_FUNCTION;
      std::map<int, std::list<int> > edgeMap;
      //dumpVertices();
      message("connectHalfEdges2");

      /*Store all half edges in a map, according to its origin.*/
      for(int i=0;i<n_halfEdges;i++){
        edgeMap[halfEdges[i].origin].push_back(i);
      }

      message("%d halfEdges", n_halfEdges);

      for(int i=0;i<n_halfEdges;i++){
        //message("%d ----------------------", i);
        //halfEdges[i].print();

        currentEdge = i;

        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          /*Get next vertex of this edge. That vertex is the origin
            vertex of the edge we are searching fo.*/
          int origin = getOriginVertex();
          int next_origin = getNextOriginVertex();

          //message("edge %d-%d", origin, next_origin);
          std::list<int>::iterator it = edgeMap[next_origin].begin();
          //message("vertex %d has %d edges", next_origin, edgeMap[next_origin].size());

          /*Loop over all edges with 'next_origin' as origin vertex*/
          while(it != edgeMap[next_origin].end()){
            currentEdge = *it++;
            //halfEdges[currentEdge].print();
            //message("currentEdge = %d", currentEdge);
            //message("nextOrigin = %d", getNextOriginVertex());
            if(getNextOriginVertex() == origin){
              //message("twin found");
              halfEdges[i].twin = currentEdge;
              //edgeObjectMap[currentEdge] = edgeObjectMap[i];
              halfEdges[currentEdge].twin = i;
              it = edgeMap[next_origin].end();
            }
          }
        }
      }

      bool inconsistent = false;

      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          message("Unconnected edge");
          halfEdges[i].print();
          inconsistent = true;
        }
      }

      if(inconsistent){
        error("Input mesh not consistent. Not able to connect half edges. Perhaps one triangle has an improper winding.");
      }
    }

    /*If a polygon model is loaded, the half edges are mostly not
      connected. (the twins). This operation finds and connects
      them.*/
    void connectHalfEdges(){
      std::map<int, std::list<int> > edgeMap;
      dumpVertices();
      message("connectHalfEdges");
      /*Store all half edges in a map, according to its origin.*/
      for(int i=0;i<n_halfEdges;i++){
        edgeMap[halfEdges[i].origin].push_back(i);
      }

      /*Connect half-edges by searching for half-edges with the same
        origin as its next half-edge. This collection should contain
        one or none half-edge with a next origin which is the same as
        the origin of the initial half-edge.*/

      for(int i=0;i<n_halfEdges;i++){
        //message("-----------");
        //halfEdges[i].print();
        currentEdge = i;
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          int origin = getOriginVertex();
          int next_origin = getNextOriginVertex();
          //message("edge %d-%d", origin, next_origin);
          std::list<int>::iterator it = edgeMap[next_origin].begin();

          while(it != edgeMap[next_origin].end()){
            currentEdge = *it++;
            if(getNextOriginVertex() == origin){
              /*Found twin*/
              //message("twin found");
              halfEdges[i].twin = currentEdge;
              //edgeObjectMap[currentEdge] = edgeObjectMap[i];
              halfEdges[currentEdge].twin = i;
              it = edgeMap[next_origin].end();
            }
          }
        }
        currentEdge = i;
        //halfEdges[i].print();
      }

      /*Find half edges without a twin*/
      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          /*Create a new half-edge*/
          DCELHalfEdge<ED> hedge;
          int hedge_index = addHalfEdge(hedge);

          currentEdge = i;
          halfEdges[hedge_index].origin    = getNextOriginVertex();
          halfEdges[hedge_index].half_face = Mesh::UndefinedIndex;
          halfEdges[hedge_index].twin      = i;
          halfEdges[hedge_index].next_twin = Mesh::UndefinedIndex;

          edgeMap[halfEdges[hedge_index].origin].push_back(hedge_index);

          halfEdges[i].twin = hedge_index;
        }
      }

      /*Check next pointers for new half-edges*/
      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].next == Mesh::UndefinedIndex){
          currentEdge = i;
          int origin = getTwinOriginVertex();
          cgfassert(getTwinEdge() != Mesh::UndefinedIndex);

          std::list<int>::iterator it = edgeMap[origin].begin();

          int count = 0;
          while(it != edgeMap[origin].end()){
            int edge = *it++;

            if(halfEdges[edge].half_face == Mesh::UndefinedIndex){
              count++;

              halfEdges[i].next = edge;
            }
          }
          message("count = %d", count);
          cgfassert(count == 1);
        }
      }

      /*Check previous pointers*/
      for(int i=0;i<n_halfEdges;i++){
        int next = halfEdges[i].next;
        currentEdge = next;
        cgfassert(prevEdge() == i);
      }

      /*Check faces*/
      for(int i=0;i<n_halfEdges;i++){
        currentEdge = i;
        //message("%d, %d, %d", getFace(), getTwinFace(), Mesh::undefined);
        cgfassert(getFace() != getTwinFace());
      }

      message("Done connecting half faces");
      dumpVertices();
    }

#if 1
    void computeFaceNormal(int f){
      int lastEdge = currentEdge;

      int edge = halfFaces[f].half_edge;
      currentEdge = edge;

      int vert[3];
      vert[0] = getOriginVertex(); nextEdge();
      vert[1] = getOriginVertex(); nextEdge();
      vert[2] = getOriginVertex();

      Vector4<T> coords[3];
      coords[0] = vertices[vert[0]].coord;
      coords[1] = vertices[vert[1]].coord;
      coords[2] = vertices[vert[2]].coord;

      coords[1] -= coords[0];
      coords[2] -= coords[0];

      Vector4<T> normal = cross(coords[1], coords[2]);
      normal.normalize();

      halfFaces[f].normal = norm(normal);

      currentEdge = lastEdge;
    }

    void computeFaceNormalDispl(int f){
      int lastEdge = currentEdge;

      int edge = halfFaces[f].half_edge;
      currentEdge = edge;

      int vert[3];
      vert[0] = getOriginVertex(); nextEdge();
      vert[1] = getOriginVertex(); nextEdge();
      vert[2] = getOriginVertex();

      Vector4<T> coords[3];
      coords[0] = vertices[vert[0]].displCoord;
      coords[1] = vertices[vert[1]].displCoord;
      coords[2] = vertices[vert[2]].displCoord;

      coords[1] -= coords[0];
      coords[2] -= coords[0];

      Vector4<T> normal = cross(coords[1], coords[2]);
      normal.normalize();

      halfFaces[f].displNormal = norm(normal);

      currentEdge = lastEdge;
    }

    void computeVertexNormal(int v){
      int lastEdge = currentEdge;

      int edge = vertices[v].leaving;
      currentEdge = edge;
      int faceCount = 0;

      Vector4<T> normal(0,0,0,0);

      while(true){
        int face = halfEdges[edge].half_face;

        if(face != Mesh::UndefinedIndex){
          normal += halfFaces[face].normal;
          faceCount++;
        }

        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }

      normal.normalize();

      vertices[v].normal = normal;

      currentEdge = lastEdge;
    }

    void computeVertexNormalDispl(int v){
      int lastEdge = currentEdge;

      int edge = vertices[v].leaving;
      currentEdge = edge;
      int faceCount = 0;

      Vector4<T> normal(0,0,0,0);

      while(true){
        int face = halfEdges[edge].half_face;

        if(face != Mesh::UndefinedIndex){
          normal += halfFaces[face].displNormal;
          faceCount++;
        }

        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }

      normal.normalize();

      vertices[v].displNormal = normal;

      currentEdge = lastEdge;
    }
#endif

    void tesselate(){
      std::map<int, std::list<int> >edgeMap;

      T* edgeLengths = new T[n_halfEdges];

      T avgEdgeLength = 0;

      /*Compute edge lengths*/
      for(int i=0;i<n_halfEdges;i++){
        currentEdge = i;

        int v1 = getOriginVertex();
        twinEdge();
        int v2 = getOriginVertex();

        edgeLengths[i] = (vertices[v1].coord - vertices[v2].coord).length();
        avgEdgeLength += edgeLengths[i];
      }

      avgEdgeLength /= (T)n_halfEdges;

      /*Compute variance*/
      T variance = 0;
      for(int i=0;i<n_halfEdges;i++){
        variance += Sqr(edgeLengths[i] - avgEdgeLength);
      }
      variance /= (T)n_halfEdges;

      message("average length = %10.10e, s^2 = %10.10e, s = %10.10e, tol = %10.10e",
              avgEdgeLength, variance, Sqrt(variance), avgEdgeLength - (T)2.0*Sqrt(variance));

      T tol = avgEdgeLength + (T)4.0*Sqrt(variance);

      delete[] edgeLengths;

      while(true){
        List<int> divideCandidates;

        for(int i=0;i<n_halfEdges;i++){
          currentEdge = i;

          int v1 = getOriginVertex();
          int v2 = getTwinOriginVertex();

          int f1 = getFace();
          int f2 = getTwinFace();

          edgeMap[v1].push_back(i);

          Vector4<T> diff = vertices[v1].coord - vertices[v2].coord;

          if(diff.length() > tol &&
             //if(diff.length() > avgEdgeLength &&
             f1 != Mesh::UndefinedIndex &&
             f2 != Mesh::UndefinedIndex){
            divideCandidates.append(i);
          }
        }

        if(divideCandidates.size()  == 0){
          return;
        }

        List<int>::Iterator it2 = divideCandidates.begin();

        while(it2 != divideCandidates.end()){
          int edge = *it2++;

          if(DCELCanSplitEdge(edge)){
            DCELDivideEdge(edge);
          }
        }

        DCELCompact();
      }
    }

    void simplify(){
      std::map<int, std::list<int> >edgeMap;

      T* edgeLengths = new T[n_halfEdges];

      T avgEdgeLength = 0;
      //float avgFaceArea = 0;
      /*Compute edge lengths*/
      for(int i=0;i<n_halfEdges;i++){
        currentEdge = i;

        int v1 = getOriginVertex();
        twinEdge();
        int v2 = getOriginVertex();

        edgeLengths[i] = (vertices[v1].coord - vertices[v2].coord).length();
        avgEdgeLength += edgeLengths[i];
      }

      avgEdgeLength /= (T)n_halfEdges;

      /*Compute variance*/
      T variance = 0;
      for(int i=0;i<n_halfEdges;i++){
        variance += Sqr(edgeLengths[i] - avgEdgeLength);
      }
      variance /= (T)n_halfEdges;

      message("average length = %10.10e, s^2 = %10.10e, s = %10.10e, tol = %10.10e",
              avgEdgeLength, variance, Sqrt(variance), avgEdgeLength - (T)2.0*Sqrt(variance));

      T tol = avgEdgeLength - (T)4.0*Sqrt(variance);

      delete[] edgeLengths;

#if 1
      /*Compute face normals*/
      for(int i=0;i<n_halfFaces;i++){
        computeFaceNormal(i);
      }

      for(int i=0;i<n_vertices;i++){
        computeVertexNormal(i);
      }

#endif

      while(true){
        List<int> collapseCandidates;
        for(int i=0;i<n_halfEdges;i++){
          currentEdge = i;

          int v1 = getOriginVertex();
          int v2 = getTwinOriginVertex();

          edgeMap[v1].push_back(i);

          Vector4<T> diff = vertices[v1].coord - vertices[v2].coord;
          if(diff.length()< tol){
            collapseCandidates.append(i);
          }
        }

#if 0
        //List<int> collapseFaces;
        for(int i=0;i<n_halfFaces;i++){
          currentEdge = halfFaces[i].half_edge;

          int v1 = getOriginVertex();
          nextEdge();
          int v2 = getOriginVertex();
          nextEdge();
          int v3 = getOriginVertex();

          //float e1 = (vertices[v1].coord - vertices[v2].coord).length();
          //float e2 = (vertices[v2].coord - vertices[v3].coord).length();
          //float e3 = (vertices[v3].coord - vertices[v1].coord).length();

          //float smallest = minf(e1, minf(e2, e3));

          Vector4<T> va = vertices[v1].coord - vertices[v3].coord;
          Vector4<T> vb = vertices[v2].coord - vertices[v3].coord;

          T area = cross(va, vb).length();

          currentEdge = halfFaces[i].half_edge;

          T faceTol = avgFaceArea - Sqrt(faceVariance);

          if(area < faceTol){
            collapseCandidates.append(currentEdge);
            collapseCandidates.append(getNextEdge());
            collapseCandidates.append(getPrevEdge());
#if 0
            if(e1 == smallest){
              collapseCandidates.append(currentEdge);
            }else if(e2 == smallest){
              nextEdge();
              collapseCandidates.append(currentEdge);
            }else{
              prevEdge();
              collapseCandidates.append(currentEdge);
            }
#endif
          }
        }
#endif

#if 0
        List<int>::Iterator it2 = divideCandidates.begin();

        while(it2 != divideCandidates.end()){
          int edge = *it2++;

          /*Should have a face on both sides*/

          int f1 = getFace();
          int f2 = getTwinFace();

          if((f1 != Mesh::UndefinedIndex) &&
             (f2 != Mesh::UndefinedIndex)){
            DCELDivideEdge(edge);
          }
        }
#endif
        //return;

        if(collapseCandidates.size() == 0){
          return;
        }

        List<int>::Iterator it = collapseCandidates.begin();

        while(it != collapseCandidates.end()){
          int edge = *it++;

          if(halfEdges[edge].twin == Mesh::UndefinedIndex){
            continue;
          }

          currentEdge = edge;
          int v1 = getOriginVertex();
          int v2 = getTwinOriginVertex();

          nextEdge();

          int v3 = getTwinOriginVertex();

          currentEdge = edge;

          twinEdge();
          nextEdge();

          int v4 = getTwinOriginVertex();

          if((v3 != v4) && (v1 != v2)){
            DCELCollapseEdge(edge,
                             edgeMap[v1],
                             edgeMap[v2],
                             edgeMap[v3],
                             edgeMap[v4], tol);
          }
        }
        DCELCompact();
      }
    }

    void removeDuplicates(){
      message("Removing duplicates, %d vertices, %d faces", n_vertices, n_halfFaces);
      while(true){
      /*Construct edge map*/
        bool collapsed = false;
        std::map<int, std::list<int> > edgeMap;
        for(int i=0;i<n_halfEdges;i++){
          edgeMap[halfEdges[i].origin].push_back(i);
        }

        for(int i=0;i<n_halfFaces;i++){
          int cedge = halfFaces[i].half_edge;

          if(cedge == Mesh::UndefinedIndex){
            message("current face(%d) already removed due to a previous collapse",
                    i);
            /*Face removed due to previous collapse*/
            continue;
          }

          currentEdge = cedge;

          std::list<int> edge_list;
          std::list<int> vertex_list;

          int idx = 0;
          Vector4<T> coordinates[10];

          while(getNextEdge() != cedge){
            edge_list.push_back(currentEdge);
            vertex_list.push_back(halfEdges[currentEdge].origin);
            coordinates[idx++] = vertices[halfEdges[currentEdge].origin].coord;
            nextEdge();
          }
          edge_list.push_back(currentEdge);
          vertex_list.push_back(halfEdges[currentEdge].origin);
          coordinates[idx++] = vertices[halfEdges[currentEdge].origin].coord;

          std::list<int>::iterator it = vertex_list.begin();

          it = vertex_list.begin();

          int cvertex = *it++;
          bool valid = true;

          while(it != vertex_list.end()){
            if(cvertex == *it++){
              valid = false;
            }
          }

          if(valid == false){
            message("invalid face found");
          }

          Vector4<T> u, v;
          u = coordinates[1] - coordinates[0];
          v = coordinates[2] - coordinates[0];

          Vector4<T> norm = cross(u, v);
          T area = norm.length();
          norm.normalize();

          T nrmlength = norm.length();
          bool collapse = false;
          if(nrmlength!= nrmlength || area < 1E-6){
            //message("Singular face");
            collapse = true;
          }

          if((coordinates[0] - coordinates[1]).length() < 1E-6 ||
             (coordinates[1] - coordinates[2]).length() < 1E-6 ||
             (coordinates[2] - coordinates[0]).length() < 1E-6){
            //message("duplicate vertices");
            collapse = true;
          }

          //collapsed = collapse;

          if(collapse){
            /*std::cout << std::scientific;
              std::cout.precision(10);

              std::cout << coordinates[0] << std::endl;
              std::cout << coordinates[1] << std::endl;
              std::cout << coordinates[2] << std::endl;
            */

            int vertex1 = halfEdges[halfFaces[i].half_edge].origin;
            int vertex2 = halfEdges[halfEdges[halfFaces[i].half_edge].twin].origin;
            currentEdge = halfFaces[i].half_edge;
            int vertex3 = halfEdges[getPrevEdge()].origin;
            twinEdge();
            int vertex4 = halfEdges[getPrevEdge()].origin;


            //message("v1 = %d, v2 = %d, v3 = %d, v4 = %d", vertex1, vertex2, vertex3, vertex4);

            if((vertex3 != vertex4) && (vertex1 != vertex2)){
              if(!DCELCollapseEdge(halfFaces[i].half_edge,
                                   edgeMap[vertex1],
                                   edgeMap[vertex2],
                                   edgeMap[vertex3],
                                   edgeMap[vertex4])){

                /*Try a different edge of this face*/
                currentEdge = halfFaces[i].half_edge;
                nextEdge();
                vertex1 = halfEdges[currentEdge].origin;
                vertex2 = halfEdges[getTwinEdge()].origin;

                currentEdge = halfFaces[i].half_edge;
                nextEdge();
                vertex3 = halfEdges[getPrevEdge()].origin;
                twinEdge();
                vertex4 = halfEdges[getPrevEdge()].origin;


                //message("v1 = %d, v2 = %d, v3 = %d, v4 = %d", vertex1, vertex2, vertex3, vertex4);
                if(DCELCollapseEdge(halfFaces[i].half_edge,
                                    edgeMap[vertex1],
                                    edgeMap[vertex2],
                                    edgeMap[vertex3],
                                    edgeMap[vertex4])){
                  collapsed = true;
                }
              }else{
                collapsed = true;
              }
            }
          }
        }

        DCELCompact();
        if(collapsed == false){
          message("Done removing duplicates, %d vertices, %d faces", n_vertices, n_halfFaces);
          return;
        }
      }
    }

    void getEdgeIndices(int i, int* indices){
      int lastEdge = currentEdge;
      int edge = i;
      currentEdge = edge;
      indices[0] = getOriginVertex();
      indices[1] = getTwinOriginVertex();
      currentEdge = lastEdge;
    }

    void computeDisplacement(Vector<T>* v, T dt){
      if(v == 0){
        for(int i=0;i<n_vertices;i++){
          vertices[i].displCoord    = vertices[i].coord;
          vertices[i].displCoordExt = vertices[i].coord;
        }
      }else{
        int n_static = 0;
        int n_deformable = 0;
        int n_cloth = 0;
        int n_rigid = 0;

        for(int i=0;i<n_vertices;i++){
          int index = vertices[i].vel_index;

          if(vertices[i].type == Mesh::Deformable){
            n_deformable++;
          }

          if(vertices[i].type == Mesh::Cloth){
            n_cloth++;
          }

          if(vertices[i].type == Mesh::Rigid){
            n_rigid++;
          }

          if(vertices[i].type == Mesh::Static){
            n_static++;
          }


          if(vertices[i].type == Mesh::Deformable ||
             vertices[i].type == Mesh::Cloth){
            /*Velocity defined*/
            Vector4<T> vel;
            for(int j=0;j<3;j++){
              vel[j] = (T)(*v)[index+j];
            }

            if(IsNan(vel[0]) || IsNan(vel[1]) || IsNan(vel[2])){
              warning("NAN velocity");
              std::cout << *v ;
              std::cout << vel << std::endl;
              error("NAN");
            }

            vertices[i].displCoord    = vertices[i].coord + vel * dt;
            vertices[i].displCoordExt = vertices[i].coord + vel * dt*DIST_C;
          }else if(vertices[i].type == Mesh::Static){
            /*Static vertex -> no displacement*/
            vertices[i].displCoord    = vertices[i].coord;
            vertices[i].displCoordExt = vertices[i].coord;
          }else if(vertices[i].type == Mesh::Rigid){
            //error("displacing rigid vertex");
            int rigidObject = vertexObjectMap[i];

            Vector4<T> com  = centerOfMass[rigidObject];
            Vector4<T> axis = vertices[i].coord - com;

            /*Load linear velocity*/
            Vector4<T> vel_lin;
            for(int j=0;j<3;j++){
              vel_lin[j] = (*v)[index + j];
            }

            if(IsNan(vel_lin[0]) || IsNan(vel_lin[1]) || IsNan(vel_lin[2])){
              warning("NAN velocity");
              std::cout << vel_lin << std::endl;
              error("NAN");
            }

            centerOfMassDispl[rigidObject] = com + dt * vel_lin;

            /*Load angular velocity*/
            Vector4<T> vel_ang;
            for(int j=0;j<3;j++){
              vel_ang[j] = (*v)[index + j+3];
            }

            Quaternion<T> ang_vel_q(vel_ang[0], vel_ang[1], vel_ang[2], 0);
            Quaternion<T> qz(0,0,0,1);

            qz = qz + (dt/(T)2.0) * (ang_vel_q * qz);

            orientationsDispl[rigidObject] = qz;

            axis = qz.rotate(axis);

            Vector4<T> comExt = com + dt * vel_lin * DIST_C;

            Quaternion<T> ang_vel_q_ext(vel_ang[0]*DIST_C, vel_ang[1]*DIST_C,
                                        vel_ang[2]*DIST_C, 0);
            qz.set(0,0,0,1);

            qz = qz + (dt/(T)2.0) * (ang_vel_q_ext * qz);

            Vector4<T> axis_ext = vertices[i].coord - com;
            axis_ext = qz.rotate(axis_ext);


            vertices[i].displCoord    = axis + centerOfMassDispl[rigidObject];
            vertices[i].displCoordExt = axis_ext + comExt;
          }else if(vertices[i].type == Mesh::Undefined){
            error("Displacing undefined vertex type");
          }else{
            error("Displacing undefined vertex type");
          }
        }
      }
    }

    /*Displace the mesh using the displaced vertices*/
    void acceptDisplacement(){
      for(int i=0;i<n_vertices;i++){
        vertices[i].coord = vertices[i].displCoord;
      }

      for(int i=0;i<n_rigidObjects;i++){
        centerOfMass[i] = centerOfMassDispl[i];
        orientationsDispl[i].set(0,0,0,1);
        orientations[i].set(0,0,0,1);
      }
    }

    void computeFaceNormals(){
      for(int i=0;i<n_halfFaces;i++){
        computeFaceNormal(i);
      }
    }

    void computeFaceNormalsDispl(){
      for(int i=0;i<n_halfFaces;i++){
        computeFaceNormalDispl(i);
      }
    }

    void getDisplacedEdge(Edge<T>* e, int edge, bool backFace){
      int lastEdge = currentEdge;
      currentEdge = edge;

      e->setEdgeId(edge);

      if(backFace){
        e->setFaceId(0, getTwinFace());
        e->setFaceId(1, getFace());
      }else{
        e->setFaceId(0, getFace());
        e->setFaceId(1, getTwinFace());
      }

      Vector4<T> pos[4];

      int indices[4];
      indices[0] = getOriginVertex();
      indices[1] = getTwinOriginVertex();

      /*Find tangent indices*/
      nextEdge();
      nextEdge();
      indices[2] = getOriginVertex();

      currentEdge = edge;
      twinEdge();
      nextEdge();
      nextEdge();
      indices[3] = getOriginVertex();

      pos[0] = vertices[indices[0]].displCoord;
      pos[1] = vertices[indices[1]].displCoord;
      pos[2] = vertices[indices[2]].displCoord;
      pos[3] = vertices[indices[3]].displCoord;

      if(backFace){
        Vector4<T> tmp = pos[2];
        pos[2] = pos[3];
        pos[3] = tmp;

        int itmp = indices[2];
        indices[2] = indices[3];
        indices[3] = itmp;
      }

      e->setFaceVertexId(0, indices[2]);
      e->setFaceVertexId(1, indices[3]);

      e->set(pos[0], pos[1], pos[2], pos[3]);
      currentEdge = lastEdge;
    }

    /*@TODO: optimize this. */
    void getEdge(Edge<T>* e, int edge, bool backFace){
      int lastEdge = currentEdge;
      currentEdge = edge;

      e->setEdgeId(edge);

      if(backFace){
        e->setFaceId(0, getTwinFace());
        e->setFaceId(1, getFace());
      }else{
        e->setFaceId(0, getFace());
        e->setFaceId(1, getTwinFace());
      }

      Vector4<T> pos[4];

      pos[0] = vertices[getOriginVertex()].coord;
      pos[1] = vertices[getTwinOriginVertex()].coord;
      nextEdge();
      nextEdge();
      pos[2] = vertices[getOriginVertex()].coord;
      int v1 = getOriginVertex();
      setCurrentEdge(edge);
      twinEdge();
      nextEdge();
      nextEdge();
      pos[3] = vertices[getOriginVertex()].coord;
      int v2 = getOriginVertex();

      if(backFace){
        Vector4<T> tmp = pos[2];
        pos[2] = pos[3];
        pos[3] = tmp;

        int itmp = v2;
        v2 = v1;
        v1 = itmp;
      }

      e->setFaceVertexId(0, v1);
      e->setFaceVertexId(1, v2);

      e->set(pos[0], pos[1], pos[2], pos[3]);
      currentEdge = lastEdge;
    }

    void getTriangleIndices(int i, int* indices, bool backFace){
      int lastEdge = currentEdge;
      int edge = halfFaces[i].half_edge;
      currentEdge = edge;

      if(backFace){
        for(int i=0;i<3;i++){
          indices[i] = getOriginVertex();
          prevEdge();
        }
      }else{
        for(int i=0;i<3;i++){
          indices[i] = getOriginVertex();
          nextEdge();
        }
      }
      currentEdge = lastEdge;
    }

    void getDisplacedTriangle(Triangle<T>* t, int id, bool backFace){
      int lastEdge = currentEdge;
      int edge = halfFaces[id].half_edge;
      currentEdge = edge;

      int indices[3];
      getTriangleIndices(id, indices, backFace);

      for(int i=0;i<3;i++){
        t->v[i] = vertices[indices[i]].displCoord;
      }

      currentEdge = lastEdge;
      t->computeNormal();
    }

    void getDisplacedTriangleExt(Triangle<T>* t, int id, bool backFace){
      int lastEdge = currentEdge;
      int edge = halfFaces[id].half_edge;
      currentEdge = edge;

      int indices[3];
      getTriangleIndices(id, indices, backFace);

      for(int i=0;i<3;i++){
        t->v[i] = vertices[indices[i]].displCoordExt;
      }

      currentEdge = lastEdge;
      t->computeNormal();
    }

    /*Converts a halfFace to a triangle with vertices and a
      normal. Usefull for intersection computations*/
    void getTriangle(Triangle<T>* t, int id, bool backFace)const{
#ifdef USE_THREADS
      pthread_mutex_lock(&accessMutex);
#endif
      int lastEdge = currentEdge;
      int edge = halfFaces[id].half_edge;
      currentEdge = edge;

      if(backFace){
        for(int i=0;i<3;i++){
          t->v[i] = vertices[getOriginVertex()].coord;
          prevEdge();
        }
      }else{
        for(int i=0;i<3;i++){
          t->v[i] = vertices[getOriginVertex()].coord;
          nextEdge();
        }
      }

      currentEdge = lastEdge;

#ifdef USE_THREADS
      pthread_mutex_unlock(&accessMutex);
#endif

      /*Compute Normal can throw an exception, so do it after the lock
        in order to prevent a deadlock.*/
      t->computeNormal();
    }

    void getEdgeIndicesOfTriangle(int f, int* indices)const{
      int lastEdge = currentEdge;

      currentEdge = halfFaces[f].half_edge;

      for(int i=0;i<3;i++){
        int edge = getCurrentEdge();
        int twin = getTwinEdge();

        if(edge < twin){
          indices[i] = edge;
        }else{
          indices[i] = twin;
        }

        nextEdge();
      }

      currentEdge = lastEdge;
    }

    void getOrientedVertex(OrientedVertex<T>* ov, int id, bool backFace){
      if(vertices[id].leaving == Mesh::UndefinedIndex){
        /*Internal vertex -> skip*/
        return;
      }

      int lastEdge = currentEdge;

      currentEdge = vertices[id].leaving;
      int edge = currentEdge;

      List<Vector4<T> > surroundingVertices;
      List<Vector4<T> > edgeNormals;
      List<int>         edgeIds;
      List<int>         vIds;

      if(backFace){
        while(true){
          computeFaceNormal(getFace());
          computeFaceNormal(getTwinFace());

          Vector4<T> edgeNormal =
            -(halfFaces[getFace()].normal +
              halfFaces[getTwinFace()].normal).normalize();

          twinEdge();

          int vertexId = getOriginVertex();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          surroundingVertices.append(vertices[getOriginVertex()].coord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }else{
        while(true){
          computeFaceNormal(getFace());
          computeFaceNormal(getTwinFace());

          Vector4<T> edgeNormal =
            (halfFaces[getFace()].normal +
             halfFaces[getTwinFace()].normal).normalize();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          nextEdge();

          int vertexId = getOriginVertex();

          surroundingVertices.append(vertices[getOriginVertex()].coord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();
          twinEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }
      currentEdge = lastEdge;

      computeVertexNormal(id);

      ov->set(vertices[id].coord, surroundingVertices, edgeNormals,
              edgeIds, vIds);
    }

    void getDisplacedOrientedVertex(OrientedVertex<T>* ov, int id,
                                    bool backFace){
      if(vertices[id].leaving == Mesh::UndefinedIndex){
        /*Internal vertex -> skip*/
        return;
      }

      int lastEdge = currentEdge;

      currentEdge = vertices[id].leaving;
      int edge = currentEdge;

      List<Vector4<T> > surroundingVertices;
      List<Vector4<T> > edgeNormals;
      List<int>         edgeIds;
      List<int>         vIds;

      if(backFace){
        while(true){
          computeFaceNormalDispl(getFace());
          computeFaceNormalDispl(getTwinFace());

          Vector4<T> edgeNormal =
            -(halfFaces[getFace()].normal +
              halfFaces[getTwinFace()].normal).normalize();

          twinEdge();

          int vertexId = getOriginVertex();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          surroundingVertices.append(vertices[getOriginVertex()].displCoord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }else{
        while(true){
          computeFaceNormalDispl(getFace());
          computeFaceNormalDispl(getTwinFace());

          Vector4<T> edgeNormal =
            (halfFaces[getFace()].normal +
             halfFaces[getTwinFace()].normal).normalize();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          nextEdge();

          int vertexId = getOriginVertex();

          surroundingVertices.append(vertices[getOriginVertex()].displCoord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();
          twinEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }

      ov->set(vertices[id].displCoord, surroundingVertices,
              edgeNormals, edgeIds, vIds);

      currentEdge = lastEdge;
    }

    int addCenterOfMass(const Vector4<T>& com, int id){
      int r = n_rigidObjects;

      if(n_rigidObjects == size_map){
        extendRigidObjects();
      }

      centerOfMass[n_rigidObjects] = com;
      orientations[n_rigidObjects].set(0,0,0,1);
      n_rigidObjects++;

      return r;
    }

    int addVertex(DCELVertex<T, VD>& v){
      int r = n_vertices;

      if(n_vertices == size_vertices){
        extendVertices();
      }

      vertices[n_vertices] = v;
      n_vertices++;

      return r;
    }

    int addHalfEdge(DCELHalfEdge<ED>& f){
      int r = n_halfEdges;

      if(n_halfEdges == size_halfEdges){
        extendHalfEdges();
      }

      halfEdges[n_halfEdges] = f;
      n_halfEdges++;

      return r;
    }

    int addHalfFace(DCELHalfFace<T, FD>& f){
      int r = n_halfFaces;

      if(n_halfFaces == size_halfFaces){
        extendHalfFaces();
      }

      halfFaces[n_halfFaces] = f;
      n_halfFaces++;

      return r;
    }

    int addTetrahedron(DCELTet<TD>& t){
      int r = n_tetrahedra;

      if(n_tetrahedra == size_tetrahedra){
        extendTetrahedra();
      }

      tetrahedra[n_tetrahedra] = t;
      n_tetrahedra++;

      return r;
    }

    void removeObjectFromFace(int faceId){
      Tree<int> faceIds;
      Tree<int> vertexIds;
      Tree<int> edgeIds;

      List<int> faceStack;

      faceStack.append(faceId);

      while(faceStack.size() != 0){
        int cFace = *faceStack.begin();
        faceIds.uniqueInsert(cFace);

        setCurrentEdge(halfFaces[cFace].half_edge);

        for(int i=0;i<3;i++){
          int vertex = getOriginVertex();
          int edge = getCurrentEdge();
          edgeIds.uniqueInsert(edge);
          vertexIds.uniqueInsert(vertex);

          Tree<int>::Iterator it = faceIds.find(getTwinFace());

          if(it == faceIds.end()){
            /*Face not encountered before, add to stack*/
            faceStack.append(getTwinFace());
          }else{
            /*Face encountered before, is on the stack and will be
              removed later*/
          }
          nextEdge();
        }
        faceStack.removeAt(0);
      }

      /*Invalidate removed geometry*/
      Tree<int>::Iterator it = faceIds.begin();
      while(it != faceIds.end()){
        int f = *it++;

        halfFaces[f] = DCELHalfFace<T, FD>();
      }

      it = vertexIds.begin();
      while(it != vertexIds.end()){
        int v = *it++;

        vertices[v] = DCELVertex<T, VD>();

        vertexObjectMap[v] = Mesh::UndefinedIndex;
      }

      it = edgeIds.begin();
      while(it != edgeIds.end()){
        int e = *it++;

        halfEdges[e] = DCELHalfEdge<ED>();
      }

      DCELCompact();
    }

    void divideHalfEdge(int edge){
      /*Dividing an edge results in an devided tetrahedron and
        introduces one vertex.*/
    }

    void subDivideTetrahedron(int tet){
      int local_vertices[4];
      int local_edges[12];
      int local_faces[4];

      /*Locate faces*/
      for(int i=0;i<4;i++){
        if(i==0){
          local_faces[i] = tetrahedra[tet].half_face;
        }else{
          local_faces[i] = halfFaces[local_faces[i-1]].next;
        }

        /*Locate half edges*/
        for(int j=0;j<12;j++){
          if(j==0){
            local_edges[j] = halfFaces[local_faces[i]].half_edge;
          }else{
            local_edges[j] = halfEdges[local_edges[j-1]].next;
          }
        }
      }
#if 0
      DCELVertex<T, VD> v[6] = {
        DCELVertex<T, VD>(vertices[i].coord - vertices[1].coord, 2)

      };

      DCELHalfEdge<ED> e[12] = {};

      DCELHalfFace<T, FD> f[]
#endif
    }

    void setInitial(){
      DCELVertex<T, VD> v[4] = {
#if 0
        DCELVertex<T, VD>(Vector4<T>(0,0,0,0), 0),
        DCELVertex<T, VD>(Vector4<T>(2,0,0,0), 2),
        DCELVertex<T, VD>(Vector4<T>(1,2,0,0), 6),
        DCELVertex<T, VD>(Vector4<T>(1,1,2,0), 1)
#else
#if 1
        DCELVertex<T, VD>(Vector4<T>(-20,-2,0,0), 0),
        DCELVertex<T, VD>(Vector4<T>(20,-2,0,0), 2),
        DCELVertex<T, VD>(Vector4<T>(0,40,0,0), 6),
        DCELVertex<T, VD>(Vector4<T>(-20,-2,-2,0), 1)
#else

        DCELVertex<T, VD>(Vector4<T>(-0.25,-0.5,0,0), 0),
        DCELVertex<T, VD>(Vector4<T>(0.25,-0.5,0,0), 2),
        DCELVertex<T, VD>(Vector4<T>(0,0.5,0,0), 6),
        DCELVertex<T, VD>(Vector4<T>(-0.25,-0.25,-2,0), 1)
#endif
#endif
      };

      addVertex(v[0]);
      addVertex(v[1]);
      addVertex(v[2]);
      addVertex(v[3]);

      /*HalfFaces, clockwise orientation.*/
      /*face 0: 0, 3, 1 n: 1 he: 0*/
      /*face 1: 1, 3, 2 n: 2 he: 3*/
      /*face 2: 2, 3, 0 n: 3 he: 6*/
      /*face 3: 1, 2, 0 n: 0 he: 9*/
      /*face twin: -1*/
      DCELHalfFace<T, FD> f[4] = {
        DCELHalfFace<T, FD>(0, Mesh::UndefinedIndex, 1),
        DCELHalfFace<T, FD>(3, Mesh::UndefinedIndex, 2),
        DCELHalfFace<T, FD>(6, Mesh::UndefinedIndex, 3),
        DCELHalfFace<T, FD>(9, Mesh::UndefinedIndex, 0)
      };

      addHalfFace(f[0]);
      addHalfFace(f[1]);
      addHalfFace(f[2]);
      addHalfFace(f[3]);

      /*edge 0:  0, 3 or: 0, hf 0, n: 1, nt:*/
      /*edge 1:  3, 1 or: 3, hf 0, n: 2*/
      /*edge 2:  1, 0 or: 1, hf 0, n: 0*/
      /*edge 3:  1, 3 or: 1, hf 1, n: 4*/
      /*edge 4:  3, 2 or: 3, hf 1, n: 5*/
      /*edge 5:  2, 1 or: 2, hf 1, n: 3*/
      /*edge 6:  2, 3 or: 2, hf 2, n: 7*/
      /*edge 7:  3, 0 or: 3, hf 2, n: 8*/
      /*edge 8:  0, 2 or: 0, hf 2, n: 6*/
      /*edge 9:  1, 2 or: 1, hf 3, n: 10*/
      /*edge 10: 2, 0 or: 2, hf 3, n: 11*/
      /*edge 11: 0, 1 or: 0, hf 3, n: 9*/
      DCELHalfEdge<ED> e[12] = {
        DCELHalfEdge<ED>(0, 0, Mesh::UndefinedIndex, 1,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(3, 0, Mesh::UndefinedIndex, 2,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(1, 0, Mesh::UndefinedIndex, 0,  Mesh::UndefinedIndex),

        DCELHalfEdge<ED>(1, 1, Mesh::UndefinedIndex, 4,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(3, 1, Mesh::UndefinedIndex, 5,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(2, 1, Mesh::UndefinedIndex, 3,  Mesh::UndefinedIndex),

        DCELHalfEdge<ED>(2, 2, Mesh::UndefinedIndex, 7,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(3, 2, Mesh::UndefinedIndex, 8,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(0, 2, Mesh::UndefinedIndex, 6,  Mesh::UndefinedIndex),

        DCELHalfEdge<ED>(1, 3, Mesh::UndefinedIndex, 10, Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(2, 3, Mesh::UndefinedIndex, 11, Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(0, 3, Mesh::UndefinedIndex, 9,  Mesh::UndefinedIndex)
      };

      addHalfEdge(e[0]);
      addHalfEdge(e[1]);
      addHalfEdge(e[2]);
      addHalfEdge(e[3]);
      addHalfEdge(e[4]);
      addHalfEdge(e[5]);
      addHalfEdge(e[6]);
      addHalfEdge(e[7]);
      addHalfEdge(e[8]);
      addHalfEdge(e[9]);
      addHalfEdge(e[10]);
      addHalfEdge(e[11]);

      DCELTet<TD> t[1] = {
        DCELTet<TD>(0)
      };

      addTetrahedron(t[0]);

      connectHalfEdges();

    }

  public:
    //DCTetraMesh(const DCTetraMesh&);
    //DCTetraMesh& operator=(const DCTetraMesh&);
    DCELTet<TD>*         tetrahedra;
    DCELHalfFace<T, FD>* halfFaces;
    DCELHalfEdge<ED>*    halfEdges;
    DCELVertex<T, VD>*   vertices;
    Vector4<T>*          centerOfMass;
    Vector4<T>*          centerOfMassDispl;
    Quaternion<T>*       orientations;
    Quaternion<T>*       orientationsDispl;
    int*                 vertexObjectMap;
    //int*              faceObjectMap;
    //int*              edgeObjectMap;
    //int*              tetObjectMap;

    int n_tetrahedra;
    int n_halfFaces;
    int n_halfEdges;
    int n_vertices;
    int n_rigidObjects;

    int size_tetrahedra;
    int size_halfFaces;
    int size_halfEdges;
    int size_vertices;
    int size_map;

    mutable int currentEdge;
    mutable int currentFace;
    mutable int currentTet;

#ifdef USE_THREADS
    mutable pthread_mutex_t accessMutex;
#endif

    int getNVertices()const{
      return n_vertices;
    }

    int getNHalfEdges()const{
      return n_halfEdges;
    }

    int getNHalfFaces()const{
      return n_halfFaces;
    }

    int getNTetrahedra()const{
      return n_tetrahedra;
    }

    void getTetrahedronCoords(const int tet, Matrix44<T>& coords)const{
      int indices[4];
      tetrahedra[tet].getIndices(indices);
      for(int i=0;i<4;i++){
        coords[i] = vertices[indices[i]].coord;
        coords[i][3] = (T)1.0;
      }
    }

    void stats(){
      message("%d vertices", n_vertices);
      message("%d halfEdges", n_halfEdges);
      message("%d halfFaces", n_halfFaces);
      message("%d tetrahedra", n_tetrahedra);
    }

    void extendRigidObjects(){
      Vector4<T>* tmp = new Vector4<T>[size_map*2];
      Vector4<T>* tmp3 = new Vector4<T>[size_map*2];
      Quaternion<T>* tmp2 = new Quaternion<T>[size_map*2];
      Quaternion<T>* tmp4 = new Quaternion<T>[size_map*2];

      for(int i=0;i<n_rigidObjects;i++){
        tmp[i]  = centerOfMass[i];
        tmp3[i] = centerOfMassDispl[i];
        tmp2[i] = orientations[i];
        tmp4[i] = orientationsDispl[i];
      }

      //memcpy(tmp, tetrahedra, sizeof(DCELTet<TD>)*size_tetrahedra);
      size_map *= 2;

      delete[] centerOfMass;
      delete[] centerOfMassDispl;
      delete[] orientations;
      delete[] orientationsDispl;

      centerOfMass      = tmp;
      centerOfMassDispl = tmp3;
      orientations      = tmp2;
      orientationsDispl = tmp4;

    }

    void extendTetrahedra(){
      PRINT(size_tetrahedra);
      DCELTet<TD>* tmp  = new DCELTet<TD>[size_tetrahedra*2];
      //int*         tmp2 = new int[size_tetrahedra*2];
      for(int i=0;i<n_tetrahedra;i++){
        tmp[i]  = tetrahedra[i];
        //tmp2[i] = tetObjectMap[i];
      }
      //memcpy(tmp, tetrahedra, sizeof(DCELTet<TD>)*size_tetrahedra);
      size_tetrahedra *= 2;
      delete[] tetrahedra;
      //delete[] tetObjectMap;
      tetrahedra   = tmp;
      //tetObjectMap = tmp2;
    }

    void extendHalfFaces(){
      DCELHalfFace<T, FD>* tmp  = new DCELHalfFace<T, FD>[size_halfFaces*2];
      //int*              tmp2 = new int[size_halfFaces*2];
      for(int i=0;i<n_halfFaces;i++){
        tmp[i]  = halfFaces[i];
        //tmp2[i] = faceObjectMap[i];
      }
      //memcpy(tmp, halfFaces, sizeof(DCELHalfFace<FD>)*size_halfFaces);
      size_halfFaces *= 2;
      delete[] halfFaces;
      //delete[] faceObjectMap;
      halfFaces     = tmp;
      //faceObjectMap = tmp2;
    }

    void extendHalfEdges(){
      DCELHalfEdge<ED>* tmp  = new DCELHalfEdge<ED>[size_halfEdges*2];
      for(int i=0;i<n_halfEdges;i++){
        tmp[i] = halfEdges[i];
      }

      size_halfEdges *= 2;

      delete[] halfEdges;

      halfEdges     = tmp;
    }

    void extendVertices(){
      PRINT(size_vertices);
      DCELVertex<T, VD>* tmp  = new DCELVertex<T, VD>[size_vertices*2];
      int*               tmp2 = new int[size_vertices*2];

      for(int i=0;i<size_vertices*2;i++){
        tmp[i]  = DCELVertex<T, VD>();
        tmp2[i] = -1;
      }

      for(int i=0;i<n_vertices;i++){
        tmp[i]  = vertices[i];
        tmp2[i] = vertexObjectMap[i];
      }

      size_vertices *= 2;

      delete[] vertices;
      delete[] vertexObjectMap;

      vertices        = tmp;
      vertexObjectMap = tmp2;
    }

    inline void setCurrentEdge(int e)const{
      currentEdge = e;
    }

    inline int getCurrentEdge()const{
      return currentEdge;
    }

    inline int nextEdge()const{
      cgfassert(currentEdge != Mesh::UndefinedIndex);
      currentEdge =  halfEdges[currentEdge].next;
      cgfassert(currentEdge != Mesh::UndefinedIndex);
      return currentEdge;
    }

    inline int twinEdge()const{
      cgfassert(currentEdge != Mesh::UndefinedIndex);
      currentEdge = halfEdges[currentEdge].twin;
      cgfassert(currentEdge != Mesh::UndefinedIndex);
      return currentEdge;
    }

    inline int prevEdge()const{
      int cedge = currentEdge;
      nextEdge();
      int lastEdge = currentEdge;
      while(currentEdge != cedge){
        lastEdge = currentEdge;
        nextEdge();
      }
      currentEdge = lastEdge;
      return lastEdge;
    }

    inline int getOriginVertex()const{
      return halfEdges[currentEdge].origin;
    }

    inline int getNextEdge()const{
      return halfEdges[currentEdge].next;
    }

    inline int getTwinEdge()const{
      return halfEdges[currentEdge].twin;
    }

    inline int getTwinNextEdge()const{
      return halfEdges[getNextEdge()].twin;
    }

    inline int getPrevEdge()const{
      int prev;
      int last = currentEdge;
      prev = prevEdge();
      currentEdge = last;
      return prev;
    }

    inline int getPrevTwinEdge()const{
      return halfEdges[getPrevEdge()].twin;
    }

    inline int getNextTwinEdge()const{
      return halfEdges[getNextEdge()].twin;
    }

    inline int getFace()const{
      return halfEdges[currentEdge].half_face;
    }

    inline int getTwinFace()const{
      if(getTwinEdge() == Mesh::UndefinedIndex){
        return Mesh::UndefinedIndex;
      }
      return halfEdges[getTwinEdge()].half_face;
    }

    inline int getTwinOriginVertex()const{
      if(halfEdges[currentEdge].twin == Mesh::UndefinedIndex){
        return Mesh::UndefinedIndex;
      }
      return halfEdges[halfEdges[currentEdge].twin].origin;
    }

    inline int getTwinTwinOriginVertex()const{
      return halfEdges[halfEdges[getTwinEdge()].twin].origin;
    }

    inline int getNextOriginVertex()const{
      return halfEdges[getNextEdge()].origin;
    }

    inline int getPrevOriginVertex()const{
      return halfEdges[getPrevEdge()].origin;
    }

    inline Vector4<T>& getOriginCoord()const{
      return vertices[getOriginVertex()].coord;
    }

    bool DCELCollapseEdge(int e, std::list<int>& edgeList1,
                          std::list<int>& edgeList2,
                          std::list<int>& edgeList3,
                          std::list<int>& edgeList4, T tol = (T)1E-3){
      if(!DCELCanCollapseEdge(e)){
        return false;
      }
      currentEdge = e;

      /*This vertex will be removed*/
      int removed_vertex = getOriginVertex();

      /*All edges and faces connected to the removed vertex, are
        reconnected to subst_vertex.*/
      int subst_vertex = getTwinOriginVertex();

      Vector4<T> diff = (vertices[removed_vertex].coord -
                         vertices[subst_vertex].coord);
      if(diff.length() > tol){
        return false;
      }

      /*message("removed vertex = %d, subst_vertex = %d", removed_vertex,
        subst_vertex);*/

      int twin_edge = getTwinEdge();

      if(halfEdges[e].half_face == Mesh::UndefinedIndex){
        return false;
      }

      cgfassert(twin_edge != e);
      cgfassert(e         != Mesh::UndefinedIndex);
      cgfassert(twin_edge != Mesh::UndefinedIndex);

      cgfassert(halfEdges[e].half_face != Mesh::UndefinedIndex);

      cgfassert(removed_vertex != Mesh::UndefinedIndex);
      cgfassert(subst_vertex   != Mesh::UndefinedIndex);
      cgfassert(removed_vertex != subst_vertex);

      /*message("edge = %d", e);
        message("twin = %d", twin_edge);*/

      /*These faces will be removed*/
      int f1 = getFace();
      int f2 = getTwinFace();

      cgfassert(f1 != Mesh::UndefinedIndex);
      //cgfassert(f2 != Mesh::UndefinedIndex);

      //message("Removing face %d, %d", f1, f2);

      int edges1[3];
      int edges2[3];

      edges1[0] = currentEdge;
      edges1[1] = getNextEdge();
      edges1[2] = getPrevEdge();

      //message("e1 %d, %d, %d", edges1[0], edges1[1], edges1[2]);

      currentEdge = twin_edge;
      edges2[0]   = currentEdge;
      edges2[1]   = getNextEdge();
      edges2[2]   = getPrevEdge();

      //message("e2 %d, %d, %d", edges2[0], edges2[1], edges2[2]);

      currentEdge = e;

      /*Reset edges*/
      halfEdges[currentEdge].origin    = Mesh::UndefinedIndex;
      halfEdges[currentEdge].half_face = Mesh::UndefinedIndex;
      halfEdges[currentEdge].twin      = Mesh::UndefinedIndex;
      halfEdges[currentEdge].next      = Mesh::UndefinedIndex;
      halfEdges[currentEdge].next_twin = Mesh::UndefinedIndex;

      halfEdges[twin_edge].origin      = Mesh::UndefinedIndex;
      halfEdges[twin_edge].half_face   = Mesh::UndefinedIndex;
      halfEdges[twin_edge].twin        = Mesh::UndefinedIndex;
      halfEdges[twin_edge].next        = Mesh::UndefinedIndex;
      halfEdges[twin_edge].next_twin   = Mesh::UndefinedIndex;

      /*Reset faces*/
      halfFaces[f1].half_edge = Mesh::UndefinedIndex;
      halfFaces[f1].twin      = Mesh::UndefinedIndex;
      halfFaces[f1].next      = Mesh::UndefinedIndex;

      if(f2 != Mesh::UndefinedIndex){
        halfFaces[f2].half_edge = Mesh::UndefinedIndex;
        halfFaces[f2].twin      = Mesh::UndefinedIndex;
        halfFaces[f2].next      = Mesh::UndefinedIndex;
      }

      /*Connect remaining twin edges*/
      currentEdge = edges1[1];
      int twin1 = getTwinEdge();
      currentEdge = edges1[2];
      int twin2 = getTwinEdge();

      cgfassert(twin1 != Mesh::UndefinedIndex);
      cgfassert(twin2 != Mesh::UndefinedIndex);

      //message("twin 1 %d origin = %d", twin1, halfEdges[twin1].origin);
      //message("twin 2 %d origin = %d", twin2, halfEdges[twin2].origin);

      halfEdges[twin1].twin = twin2;
      halfEdges[twin2].twin = twin1;

      /*Do this later*/
      //halfEdges[twin2].origin = subst_vertex;

      /*Connect remaining twin edges*/
      if(f2 != Mesh::UndefinedIndex){
        currentEdge = edges2[1];
        twin1 = getTwinEdge();
        currentEdge = edges2[2];
        twin2 = getTwinEdge();

        cgfassert(twin1 != Mesh::UndefinedIndex);
        cgfassert(twin2 != Mesh::UndefinedIndex);

        //message("twin 1 %d origin = %d", twin1, halfEdges[twin1].origin);
        //message("twin 2 %d origin = %d", twin2, halfEdges[twin2].origin);

        halfEdges[twin1].twin = twin2;
        halfEdges[twin2].twin = twin1;

        cgfassert(halfEdges[twin2].origin == subst_vertex);
        halfEdges[twin2].origin = subst_vertex;
      }

      if(false){
        std::list<int>::iterator it2 = edgeList1.begin();
        while(it2 != edgeList1.end()){
          message("1 | %d", *it2++);
        }

        it2 = edgeList2.begin();

        while(it2 != edgeList2.end()){
          message("2 | %d", *it2++);
        }

        it2 = edgeList3.begin();

        while(it2 != edgeList3.end()){
          message("3 | %d", *it2++);
        }

        it2 = edgeList4.begin();

        while(it2 != edgeList4.end()){
          message("4 | %d", *it2++);
        }
      }


      //message("1 %d", edgeList1.size());
      edgeList1.remove(edges1[0]);
      //message("1 %d", edgeList1.size());
      //message("1 %d", edgeList1.size());
      edgeList1.remove(edges2[1]);
      //message("1 %d", edgeList1.size());
      //message("2 %d", edgeList2.size());
      edgeList2.remove(edges2[0]);
      //message("2 %d", edgeList2.size());
      //message("2 %d", edgeList2.size());
      edgeList2.remove(edges1[1]);
      //message("2 %d", edgeList2.size());
      //message("3 %d", edgeList3.size());
      edgeList3.remove(edges1[2]);
      //message("3 %d", edgeList3.size());
      //message("4 %d", edgeList4.size());
      edgeList4.remove(edges2[2]);
      //message("4 %d", edgeList4.size());

      /*Update lists*/
      std::list<int>::iterator it = edgeList1.begin();

      while(it != edgeList1.end()){
        int edge = *it++;
        if((edge != e) && (edge != twin_edge)){
          edgeList2.push_back(edge);
          //message("elist1[%d].origin = %d", edge, halfEdges[edge].origin);
          cgfassert(halfEdges[edge].origin == removed_vertex);
          halfEdges[edge].origin = subst_vertex;
          vertices[subst_vertex].leaving = edge;
          //message("moving edge %d to list 2 with origin %d", edge, subst_vertex);
        }else{
          error();
        }
      }
      edgeList1.clear();

      vertices[removed_vertex].leaving = Mesh::UndefinedIndex;

      for(int jj=0;jj<3;jj++){
        halfEdges[edges1[jj]].origin    = Mesh::UndefinedIndex;
        halfEdges[edges1[jj]].half_face = Mesh::UndefinedIndex;
        halfEdges[edges1[jj]].twin      = Mesh::UndefinedIndex;
        halfEdges[edges1[jj]].next      = Mesh::UndefinedIndex;
        halfEdges[edges1[jj]].next_twin = Mesh::UndefinedIndex;

        if(f2 != Mesh::UndefinedIndex){
          halfEdges[edges2[jj]].origin    = Mesh::UndefinedIndex;
          halfEdges[edges2[jj]].half_face = Mesh::UndefinedIndex;
          halfEdges[edges2[jj]].twin      = Mesh::UndefinedIndex;
          halfEdges[edges2[jj]].next      = Mesh::UndefinedIndex;
          halfEdges[edges2[jj]].next_twin = Mesh::UndefinedIndex;
        }
      }
      if(f2 == Mesh::UndefinedIndex){
        halfEdges[edges2[2]].next = edges2[1];
      }

#if 1
      /*compute new position*/
      Vector4<T> avg = (vertices[removed_vertex].coord +
                        vertices[subst_vertex].coord)/(T)2.0;

      vertices[subst_vertex].coord = avg;// + (1.0-avgNormalLength)*avg;
#endif
      return true;
    }


    void getConnectedEdgesOfVertex(int vertex, Tree<int>& edges){
      int lastEdge = currentEdge;
      currentEdge  = vertices[vertex].leaving;
      int edge     = currentEdge;

      while(true){
        edges.uniqueInsert(currentEdge, currentEdge);
        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    void getSurroundingEdgesOfVertex(DCELVertex<T, VD>& v, List<int>& list){
      int lastEdge = currentEdge;
      currentEdge  = v.leaving;
      int edge     = currentEdge;

      while(true){
        list.append(currentEdge);
        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    void getConnectedFacesOfVertex(int vertex, Tree<int>& faces)const{
      int lastEdge = currentEdge;
      currentEdge  = vertices[vertex].leaving;
      int edge     = currentEdge;

      while(true){
        int face = halfEdges[currentEdge].half_face;
        if(face != Mesh::UndefinedIndex){
          int uface = face;
          faces.insert(uface, uface);
        }
        twinEdge();
        nextEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    void getConnectedFacesOfVertex(int vertex, List<int>& faces)const{
      int lastEdge = currentEdge;
      currentEdge  = vertices[vertex].leaving;
      int edge     = currentEdge;

      while(true){
        int face = halfEdges[currentEdge].half_face;
        if(face != Mesh::UndefinedIndex){
          int uface = face;
          faces.append(uface);
        }
        twinEdge();
        nextEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    void getSurroundingFacesOfVertex(DCELVertex<T, VD>& v, List<int>& list){
      int lastEdge = currentEdge;
      currentEdge  = v.leaving;
      int edge     = currentEdge;

      while(true){
        int face = halfEdges[currentEdge].half_face;
        if(face != Mesh::UndefinedIndex){
          list.append(face);
        }
        twinEdge();
        nextEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    void getSurroundingVerticesOfVertex(DCELVertex<T, VD>& v,
                                        List<int>& list){
      int lastEdge = currentEdge;
      currentEdge  = v.leaving;
      int edge     = currentEdge;

      while(true){
        nextEdge();

        list.append(getOriginVertex());

        nextEdge();
        twinEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    void DCELComputeFaceArea(int f);
    void DCELGetShortestEdgeOfFace(int f, int* e);
    void DCELCheckFaceConsistency(int f);
    void DCELCheckEdgeVertexConnectivity(int e, int v);
    bool DCELCheckVertexVertexConnectivity(int v1,int v2);
    void DCELDisplaySpanningVerticesOfEdge(int e);
    void DCELGetConnectedVerticesOfVertex(int v, int* verts);


    /*v3-v2 = edge for which all connected vertices must be in front*/
    /*v1-v2 = edge to collapse*/
    bool allVerticesInFront(int v3, int v2, int v1,
                            List<int>& connected){

      /*Check projections*/

      /*Compute tangent vector*/
      Vector4<T> ea = (vertices[v1].coord - vertices[v2].coord);
      Vector4<T> eb = (vertices[v3].coord - vertices[v2].coord);

      /*Compute face normal*/
      Vector4<T> normal = cross(ea, eb);
      normal.normalize();

      Vector4<T> edgeNormal = cross(eb, normal);
      /*Tangent vector, normal to edge*/
      edgeNormal.normalize();

      bool allInFront = true;
      /*Check v3 with list1*/
      List<int>::Iterator it = connected.begin();
      while(it != connected.end()){
        int eee = *it++;

        if(eee == v2 || eee == v3){
          continue;
        }

        /*An edge between v1 and v1 is not allowed.*/
        cgfassert(eee != v1);

        Vector4<T> vec = vertices[v1].coord - vertices[v3].coord;
        vec.normalize();

        cgfassert(dot(vec, edgeNormal) >= 0);

        vec = vertices[eee].coord - vertices[v3].coord;

        if(dot(vec, edgeNormal) <= 1E-4){
          allInFront = false;
        }
      }
      return allInFront;
    }

    void DCELDivideEdge(int e){
      currentEdge = e;

      DCELHalfFace<T, FD> face;

      int f1 = halfEdges[e].half_face;
      int f2 = halfEdges[getTwinEdge()].half_face;
      int f3 = addHalfFace(face);
      int f4 = addHalfFace(face);

      int e1 = currentEdge;
      int v1 = getOriginVertex(); nextEdge();
      int e2 = currentEdge;
      int v2 = getOriginVertex(); nextEdge();
      int e3 = currentEdge;
      int v3 = getOriginVertex(); nextEdge();

      cgfassert(currentEdge == e);
      twinEdge();

      int e4 = currentEdge;
      cgfassert(getOriginVertex() == v2);
      nextEdge();
      int e5 = currentEdge;
      cgfassert(getOriginVertex() == v1);
      nextEdge();
      int e6 = currentEdge;
      int v4 = getOriginVertex();

      currentEdge = e;

      DCELVertex<T, VD> vertex;
      int v7 = addVertex(vertex);
      vertices[v7].coord = (vertices[v1].coord + vertices[v2].coord)/(T)2.0;

      DCELHalfEdge<ED> edge;
      int se1 = addHalfEdge(edge);
      int se2 = addHalfEdge(edge);
      int se3 = addHalfEdge(edge);
      int se4 = addHalfEdge(edge);
      int se5 = addHalfEdge(edge);
      int se6 = addHalfEdge(edge);
      /*Split face 1*/

      halfEdges[e1].next    = se1;
      halfEdges[se1].next   = e3;
      halfEdges[se1].origin = v7;
      halfEdges[e3].next    = e1;

      halfEdges[e1].half_face  = f1;
      halfEdges[se1].half_face = f1;
      halfEdges[e3].half_face  = f1;

      halfFaces[f1].half_edge  = e1;

      ////

      halfEdges[e2].next    = se2;
      halfEdges[se2].next   = se3;
      halfEdges[se2].origin = v3;
      halfEdges[se3].next   = e2;
      halfEdges[se3].origin = v7;

      halfEdges[e2].half_face  = f2;
      halfEdges[se2].half_face = f2;
      halfEdges[se3].half_face = f2;

      halfFaces[f2].half_edge  = e2;

      ////

      halfEdges[e4].next    = se4;
      halfEdges[se4].next   = e6;
      halfEdges[se4].origin = v7;
      halfEdges[e6].next    = e4;

      halfEdges[e4].half_face  = f3;
      halfEdges[se4].half_face = f3;
      halfEdges[e6].half_face  = f3;

      halfFaces[f3].half_edge  = e4;

      ////

      halfEdges[e5].next = se5;
      halfEdges[se5].next = se6;
      halfEdges[se5].origin = v4;
      halfEdges[se6].next = e5;
      halfEdges[se6].origin = v7;

      halfEdges[e5].half_face  = f4;
      halfEdges[se5].half_face = f4;
      halfEdges[se6].half_face = f4;

      halfFaces[f4].half_edge = e5;

      /*Connect twins*/
      halfEdges[se1].twin = se2;
      halfEdges[se2].twin = se1;
      halfEdges[se5].twin = se4;
      halfEdges[se4].twin = se5;

      halfEdges[se3].twin = e4;
      halfEdges[e4].twin = se3;
      halfEdges[se6].twin = e1;
      halfEdges[e1].twin = se6;

      vertices[v7].leaving  = se1;

#if 0
      message("vertices:: %d, %d, %d, %d, %d", v1, v2, v3, v4, v7);

      vertices[v1].print();
      vertices[v2].print();
      vertices[v3].print();
      vertices[v4].print();
      vertices[v7].print();

      message("edges:: %d, %d, %d, %d, %d, %d", e1, e2, e3, e4, e5, e6);

      halfEdges[e1].print();
      halfEdges[e2].print();
      halfEdges[e3].print();
      halfEdges[e4].print();
      halfEdges[e5].print();
      halfEdges[e6].print();

      message("nedges:: %d, %d, %d, %d, %d, %d", se1, se2, se3, se4, se5, se6);

      halfEdges[se1].print();
      halfEdges[se2].print();
      halfEdges[se3].print();
      halfEdges[se4].print();
      halfEdges[se5].print();
      halfEdges[se6].print();

      message("faces:: %d, %d, %d, %d", f1, f2, f3, f4);

      halfFaces[f1].print();
      halfFaces[f2].print();
      halfFaces[f3].print();
      halfFaces[f4].print();

      //getchar();
#endif
    }

    void normalize(Vector4<T>& center, T& scale){
      BBox<T> box = getBBox();
      Vector4<T> diff = box.getMax() - box.getMin();
      scale = Max(diff[0], Max(diff[1], diff[2]));

      center = diff/(T)2.0 + box.getMin();

      scale += (T)1E-4;

      for(int i=0;i<n_vertices;i++){
        vertices[i].coord = (T)2.0*(vertices[i].coord - center)/scale;
      }
    }

    BBox<T> getBBox(){
      BBox<T> box;

      for(int i=0;i<n_vertices;i++){
        Vector4<T> v = vertices[i].coord;

        box.addPoint(v);
      }

      return box;
    }

    bool DCELCanSplitEdge(int e){
      currentEdge = e;

      /*Can only split an edge if it is the largest of both faces*/

      T edgeLengths[6] = {0,0,0,0,0,0};

      if(halfEdges[currentEdge].half_face == Mesh::UndefinedIndex){
        return false;
      }

      for(int i=0;i<3;i++){
        int v1 = getOriginVertex();
        int v2 = getTwinOriginVertex();

        edgeLengths[i] = (vertices[v1].coord - vertices[v2].coord).length();
        nextEdge();
      }

      cgfassert(currentEdge == e);

      twinEdge();

      if(halfEdges[currentEdge].half_face == Mesh::UndefinedIndex){
        return false;
      }

      for(int i=0;i<3;i++){
        int v1 = getOriginVertex();
        int v2 = getTwinOriginVertex();

        edgeLengths[i+3] = (vertices[v1].coord - vertices[v2].coord).length();
        nextEdge();
      }

      cgfassert(edgeLengths[0] == edgeLengths[3]);

      if(edgeLengths[0] == Max(edgeLengths[0], Max(edgeLengths[1], edgeLengths[2])) &&
         edgeLengths[3] == Max(edgeLengths[3], Max(edgeLengths[4], edgeLengths[5]))){
        return true;
      }

      return false;
    }

    bool DCELCanCollapseEdge(int e){
      currentEdge = e;

      int v1 = getOriginVertex();
      int v2 = getTwinOriginVertex();

      nextEdge();

      int v3 = getTwinOriginVertex();

      currentEdge = e;
      twinEdge();
      nextEdge();
      int v4 = getTwinOriginVertex();

      if(v2 == v4 || v1 == v2){
        return false;
      }

      //message("v1, v2, v3, v4 = %d, %d, %d, %d", v1, v2, v3, v4);

      /*Check connected vertices connected to v1 and v2*/
      List<int> list1;
      List<int> list2;

      currentEdge = e;
      twinEdge();

      while(true){
        int vertex = getOriginVertex();
        if(vertex != v1 || vertex != v2 || vertex != v3 || vertex != v4){
          list1.append(vertex);
        }

        nextEdge();
        if(currentEdge == e){
          break;
        }
        twinEdge();
      }

      currentEdge = e;

      while(true){
        int vertex = getOriginVertex();
        if(vertex != v1 || vertex != v2 || vertex != v3 || vertex != v4){
          list2.append(vertex);
        }

        nextEdge();
        twinEdge();
        if(currentEdge == e){
          break;
        }
      }

      /*v3-v2 around v1->list1*/
      if(!allVerticesInFront(v3, v2, v1, list1)){
        return false;
      }

      /*v2-v4 around v1->list1*/
      if(!allVerticesInFront(v2, v4, v1, list1)){
        return false;
      }

      /*v4-v1 around v2->list2*/
      if(!allVerticesInFront(v4, v1, v2, list2)){
        return false;
      }

      /*v1-v3 around v2->list2*/
      if(!allVerticesInFront(v1, v3, v2, list2)){
        return false;
      }

      return true;
    }

    /*Polygonize v1-edge-v2-----v3, returns new created edge*/
    int polygonizeEdge(int edge){
      DCELHalfEdge<ED> ed;
      currentEdge = edge;

      int v1 = getOriginVertex();
      nextEdge();
      //      int v2 = getOriginVertex();
      nextEdge();
      int v3 = getOriginVertex();
      currentEdge=edge;

      int e0 = edge;
      int eprev = getPrevEdge();
      int e1 = getNextEdge();
      nextEdge();
      int enext = getNextEdge();

      int e2 = addHalfEdge(ed);
      int e3 = addHalfEdge(ed);
      DCELHalfFace<T, FD> face;
      int f1 = addHalfFace(face);

      halfEdges[e1].next = e2;
      halfEdges[e2].next = e0;

      halfEdges[eprev].next = e3;
      halfEdges[e3].next = enext;

      halfEdges[e2].twin = e3;
      halfEdges[e3].twin = e2;

      halfEdges[e2].origin = v3;
      halfEdges[e3].origin = v1;

      halfEdges[e0].half_face = f1;
      halfEdges[e1].half_face = f1;
      halfEdges[e2].half_face = f1;

      halfFaces[f1].half_edge = e0;
      return e3;
    }

    bool DCELPolygonize(int e){
      currentEdge = e;
      List<int> polygon;

      if(halfEdges[e].half_face != Mesh::UndefinedIndex){
        return false;
      }

      polygon.append(currentEdge);

      nextEdge();
      while(currentEdge != e){
        polygon.append(currentEdge);
        nextEdge();
      }

      while(true){
        List<int>::Iterator it = polygon.begin();

        if(polygon.size() == 3){
          int e0 = *it++;
          int e1 = *it++;
          int e2 = *it++;

          DCELHalfFace<T, FD> face;
          int f1 = addHalfFace(face);

          halfEdges[e0].half_face = f1;
          halfEdges[e1].half_face = f1;
          halfEdges[e2].half_face = f1;

          halfFaces[f1].half_edge = e0;

          return true;
        }

        /*Find smallest angle*/
        int smallestIndex = Mesh::UndefinedIndex;
        T smallestAngle = 1000;

        for(int i=0;i<polygon.size();i++){
          it = polygon.begin();
          const int edge = *it;

          currentEdge=edge;
          int v1 = getOriginVertex();
          int f1 = getTwinFace();
          nextEdge();
          int v2 = getOriginVertex();
          int f2 = getTwinFace();
          nextEdge();
          int v3 = getOriginVertex();
          currentEdge=edge;

          Vector4<T> e0 = vertices[v1].coord - vertices[v2].coord;
          Vector4<T> e1 = vertices[v3].coord - vertices[v2].coord;
          e0.normalize();
          e1.normalize();

          computeFaceNormal(f1);
          computeFaceNormal(f2);

          Vector4<T> edgeNormal1 = cross(halfFaces[f1].normal, -e0);
          Vector4<T> edgeNormal2 = cross(halfFaces[f2].normal,  e1);
          edgeNormal1.normalize();
          edgeNormal2.normalize();


          if(dot(edgeNormal1, e1) > 0 &&
             dot(edgeNormal2, e0) > 0){
            T dotp = dot(e0, e1);
            if(dotp > (T)1.0){
              dotp = (T)1.0;
            }else if(dotp < -(T)1.0){
              dotp = -(T)1.0;
            }

            T angle = ArcCos(dotp);

            if(angle<smallestAngle){
              smallestAngle = angle;
              smallestIndex = i;
            }
          }
          /*Move front edge to back*/
          List<int>::Iterator rit = polygon.begin();
          polygon.remove2(rit);
          polygon.append(edge);
        }

        /*Jump to smallestIndex*/
        for(int i=0;i<smallestIndex;i++){
          it = polygon.begin();
          int edge = *it;
          List<int>::Iterator rit = polygon.begin();

          polygon.remove2(rit);
          polygon.append(edge);
        }

        it = polygon.begin();
        int edge = *it;

        int newEdge = polygonizeEdge(edge);
        List<int>::Iterator rit = polygon.begin();
        polygon.remove2(rit);
        rit = polygon.begin();
        polygon.remove2(rit);
        polygon.append(newEdge);
      }
    }

    void DCELFillGaps(){
      /*Find edges with no half face*/
      T proj = 0.2f;
      while(true){
        for(int i=0;i<n_halfEdges;i++){
          if(halfEdges[i].half_face == Mesh::UndefinedIndex){
            /*Current edge is a part of a gap, fill up*/
            //DCELFillGap(i, proj);
            DCELPolygonize(i);
          }else{
            /*Current edge is a part of a polygon, skip*/
          }
        }

        DCELCompact();

        /*Check*/
        bool gaps = false;
        for(int i=0;i<n_halfEdges;i++){
          if(halfEdges[i].half_face == Mesh::UndefinedIndex){
            //error("Found a gap");
            gaps = true;
          }
        }
        if(gaps == false){
          return;
        }
        proj /= (T)2.0;
      }
    }

    void DCELCompact(){
      /*Compact faces*/
      int currentFaceIndex = 0;
      for(int i=0;i<n_halfFaces;i++){
        if(halfFaces[i].half_edge == Mesh::UndefinedIndex){
          continue;
        }

        halfFaces[currentFaceIndex] = halfFaces[i];
        currentEdge = halfFaces[currentFaceIndex].half_edge;

        int edge = currentEdge;
        halfEdges[currentEdge].half_face = currentFaceIndex;
        nextEdge();
        while(currentEdge != edge){
          halfEdges[currentEdge].half_face = currentFaceIndex;
          nextEdge();
        }
        currentFaceIndex++;
      }

      n_halfFaces = currentFaceIndex;

      int currentEdgeIndex = 0;
      /*Compact edges*/
      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          continue;
        }
        currentEdge = i;
        int prevEdge = getPrevEdge();

        /*Copy*/
        halfEdges[currentEdgeIndex] = halfEdges[i];

        /*half_edge of face*/
        if(halfEdges[currentEdgeIndex].half_face != Mesh::UndefinedIndex){
          halfFaces[halfEdges[currentEdgeIndex].half_face].half_edge =
            currentEdgeIndex;
        }

        /*Twin*/
        halfEdges[halfEdges[currentEdgeIndex].twin].twin = currentEdgeIndex;
        /*Next of previous edge*/
        halfEdges[prevEdge].next = currentEdgeIndex;

        /*Leaving of origin vertex*/
        vertices[halfEdges[currentEdgeIndex].origin].leaving = currentEdgeIndex;
        currentEdgeIndex++;
      }

      n_halfEdges = currentEdgeIndex;

      /*Compact vertices*/
      std::map<int, std::list<int> > edgeMap;

      for(int i=0;i<n_halfEdges;i++){
        edgeMap[halfEdges[i].origin].push_back(i);
      }

      int currentVertexIndex = 0;
      for(int i=0;i<n_vertices;i++){
        if(edgeMap[i].size() == 0){
          continue;
        }

        vertices[currentVertexIndex] = vertices[i];
        vertices[currentVertexIndex].id = currentVertexIndex;

        vertexObjectMap[currentVertexIndex] = vertexObjectMap[i];

        std::list<int>::iterator it = edgeMap[i].begin();

        while(it != edgeMap[i].end()){
          int edge = *it++;

          halfEdges[edge].origin = currentVertexIndex;
        }
        currentVertexIndex++;
      }

      n_vertices = currentVertexIndex;
    }

    void dumpFace(int f){
      setCurrentEdge(halfFaces[f].half_edge);
      int startEdge = currentEdge;

      std::cout << "face[" << f << "] = ";

      while(true){
        std::cout << getOriginVertex() << ", ";
        nextEdge();
        if(currentEdge == startEdge){
          std::cout << std::endl;
          return;
        }
      }
    }

    void computeCentersOfMass(){
      for(int i=0;i<n_rigidObjects;i++){
        centerOfMass[i].set(0,0,0,0);

        double totalVolume = 0;

        for(int j=0;j<n_halfFaces;j++){
          setCurrentEdge(halfFaces[j].half_edge);
          int v = getOriginVertex();

          if(vertexObjectMap[v] == i){
            /*Vertex of face j belongs to object i*/

            Vector4<T> va = vertices[v].coord;
            nextEdge();
            Vector4<T> vb = vertices[getOriginVertex()].coord;
            nextEdge();
            Vector4<T> vc = vertices[getOriginVertex()].coord;

            Vector4<T> vd(0,0,0,0);

            nextEdge();

            if(v != getOriginVertex()){
              error("vertex not equal to begin vertex");
            }

            Matrix44d tet;
            tet[0] = va;
            tet[1] = vb;
            tet[2] = vc;
            tet[3] = vd;

            tet[0][3] = 1;
            tet[1][3] = 1;
            tet[2][3] = 1;
            tet[3][3] = 1;

            double volume = tet.det()/6.0;
            totalVolume += volume;

            centerOfMass[i] += (T)volume*(va + vb + vc + vd)*(T)0.25;
          }
        }
        centerOfMass[i] /= (T)totalVolume;
        message("center of mass %d", i);
        std::cout << centerOfMass[i];
        message("total volume = %10.10e", totalVolume);
      }

    }

    void saveMesh(std::ofstream& out){
      if(out.is_open()){
        out.precision(10);
        out << std::scientific;

        /*Write n vertices*/
        out << getNVertices() << std::endl;

        /*Write n edges*/
        out << getNHalfEdges() << std::endl;

        /*Write n faces*/
        out << getNHalfFaces() << std::endl;

        for(int i=0;i<getNVertices();i++){
          out << vertices[i].coord[0] << "\t" <<
            vertices[i].coord[1] << "\t" <<
            vertices[i].coord[2] << std::endl;
        }

        for(int i=0;i<getNHalfEdges();i++){
          int twin = halfEdges[i].twin;
          out<< halfEdges[i].origin << "\t" << halfEdges[twin].origin <<
            std::endl;
        }

        for(int i=0;i<getNHalfFaces();i++){
          setCurrentEdge(halfFaces[i].half_edge);

          out << getOriginVertex();
          nextEdge();
          out << "\t" << getOriginVertex();
          nextEdge();
          out << "\t" << getOriginVertex();

          setCurrentEdge(halfFaces[i].half_edge);

          int cedge = getCurrentEdge();
          int tedge = getTwinEdge();

          out << "\t" << (cedge<tedge?cedge:tedge);

          nextEdge();

          cedge = getCurrentEdge();
          tedge = getTwinEdge();

          out << "\t" << (cedge<tedge?cedge:tedge);

          nextEdge();

          cedge = getCurrentEdge();
          tedge = getTwinEdge();

          out << "\t" << (cedge<tedge?cedge:tedge) << std::endl;
        }
      }

    }

    template<class U, class V, class W, class X>
    friend void convertMesh(DCTetraMesh<U, V, W, X>, DCTetraMesh<U>* s);
  };

#if 0
  class GlMesh{
  public:
    DCTetraMesh<> mesh;
    void compile();     /*Generates VBOs*/
    void updateVBOs();
    void draw();
  };
#endif

  template<class T, class U, class V, class W, class TT, class UU, class VV, class WW>
  void convertMesh(DCTetraMesh<T, U, V, W>* r,
                   DCTetraMesh<TT, UU, VV, WW>* s){
#if 0
    for(int i=0;i<s->getNVertices();i++){
      DCELVertex<T> v;
      v.coord   = s->vertices[i].coord;
      v.leaving = s->vertices[i].leaving;
      v.normal  = s->vertices[i].normal;
      v.id      = s->vertices[i].id;
      r->addVertex(v);
    }

    for(int i=0;i<s->getNHalfEdges();i++){
      DCELHalfEdge<U> e;
      e.origin    = s->halfEdges[i].origin;
      e.half_face = s->halfEdges[i].half_face;
      e.twin      = s->halfEdges[i].twin;
      e.next      = s->halfEdges[i].next;
      e.next_twin = s->halfEdges[i].next_twin;
      r->addHalfEdge(e);
    }

    for(int i=0;i<s->getNHalfFaces();i++){
      DCELHalfFace<T, V> f;
      f.half_edge = s->halfFaces[i].half_edge;
      f.twin      = s->halfFaces[i].twin;
      f.next      = s->halfFaces[i].next;
      f.normal    = s->halfFaces[i].normal;
      r->addHalfFace(f);
    }

    for(int i=0;i<s->getNTetrahedra();i++){
      DCELTet<W> t;
      t.half_face = s->tetrahedra[i].half_face;
      r->addTetrahedron(t);
    }
    r->currentEdge = 0;
    r->currentFace = 0;
    r->currentTet  = 0;
#else
    *r = *s;
#endif
  }
}

#endif/*DCELIST_HPP*/
