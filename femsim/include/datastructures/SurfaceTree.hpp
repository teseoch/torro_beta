/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef SURFACETREE_HPP
#define SURFACETREE_HPP

#include "datastructures/DCEList.hpp"
#include "math/Random.hpp"
#include "math/Vector.hpp"
#include "datastructures/Octree.hpp"

namespace CGF{

  template<class T, class A, class B, class C, class D>
  class SurfaceTree;

  template<class T, class A, class B, class C, class D>
  class GlSurfaceTree;

  namespace SurfaceTreeInternals{
    template<class T>
    class SurfaceNode;
  }

  enum SurfaceType{VERTEX, FACE, EDGE};

  enum RayTraceMode{BackFace, FrontFace, FrontAndBackFace};

#define BBOXEPS 5e-4

#if 1
  template<class T>
  class SurfaceNodeCompare{
  public:
    typedef SurfaceTreeInternals::SurfaceNode<T>* SNP;
    static bool snless(const SNP& a, const SNP& b);
    static bool snequal(const SNP& a, const SNP& b);
    static bool snless2(const SNP& a, const SNP& b);
    static bool snequal2(const SNP& a, const SNP& b);
  };
#endif

  //typedef SurfaceNode* SNP;

  //inline bool snless2(const SNP& a, const SNP& b);
  //template<class T>
  //inline bool snless2(const SurfaceNode<T>* & a, const SurfaceNode<T>* & b);

  //inline bool snequal2(const SNP& a, const SNP& b);
  //template<class T>
  //inline bool snequal2(const SurfaceNode<T>* & a, const SurfaceNode<T>* & b);

  inline int binarySearch(int* A, int key, int imin, int imax){
    while(imax >= imin){
      int imid = imin + ((imax - imin) / 2);

      if(A[imid] < key){
        imin = imid + 1;
      }else if(A[imid] > key){
        imax = imid - 1;
      }else{
        return imid;
      }
    }

    return -1;
  }

  namespace SurfaceTreeInternals{
    template<class T>
    class SurfaceNode{
    public:
      typedef SurfaceNode<T> Self;

      SurfaceNode():neighboringFaces(&SurfaceNodeCompare<T>::snless2,
                                     &SurfaceNodeCompare<T>::snequal2){
        parent = childs[0] = childs[1] = 0;
        leaf = false;
        face = 0;
        id = 0;
        color[0] = genRand<float>();
        color[1] = genRand<float>();
        color[2] = genRand<float>();
        perimeter = 0;
        level = 0;
        visible = false;
        visibility = 0;
        visibilityExt = 0;
        //adjacentFaceIndices = 0;
        root = 0;

        /*Range of faces*/
        minFace = 0;
        maxFace = 0;
      }

      virtual ~SurfaceNode(){
        if(parent){
          if(parent->childs[0] == this){
            parent->childs[0] = 0;
          }

          if(parent->childs[1] == this){
            parent->childs[1] = 0;
          }
        }

        if(!leaf){
          if(childs[0] == childs[1]){
            if(childs[0]){
              delete childs[0];
            }
          }else{
            if(childs[0]){
              delete childs[0];
            }
            if(childs[1]){
              delete childs[1];
            }
          }
        }
      }

      template<class A, class B, class C, class D>
      void addEdges(Tree<int>& tree,
                    DCTetraMesh<T, A, B, C, D>* mesh){
        Tree<int>::Iterator it = tree.begin();

        while(it != tree.end()){
          int edge = *it;
          /*Check if its twinedge exists*/
          int twin = mesh->halfEdges[edge].twin;
          int index = interiorEdges.findIndex(twin);

          if(index != Tree<int>::undefinedIndex){
            /*Twin edge exists, so both sides of the edge are present,
              which implies that both surfaces are glued on this
              edge. Hence this edge will be an interior edge and can be
              removed.*/
            interiorEdges.remove(twin);
          }else{
            interiorEdges.uniqueInsert(edge, edge);
          }
          it++;
        }
      }

      template<class A, class B, class C, class D>
      void computeVisibility(DCTetraMesh<T, A, B, C, D>* mesh,
                             bool recursive = true){
        //bool hasVector = false;
        visibility = 0;
        if(leaf){
          /*Perform the actual test using the geometry*/
          //Triangle t = mesh->getTriangle(id);
          for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
              for(int k=0;k<3;k++){
                if((i == 1) && (j == 1) && (k == 1)){
                  /*Skip, center*/
                }else{
                  Vector4<T> testVector(1-i, 1-j, 1-k, 0);
                  if(dot(testVector, tri.n) >= 0){
                    visibility |= 1 << (i*9+j*3+k);
                  }
                  if(dot(testVector, triExt.n) >= 0){
                    visibilityExt |= 1 << (i*9+j*3+k);
                  }
                }
              }
            }
          }
        }else{
          if(recursive){
            if(childs[0] == childs[1]){
              childs[0]->computeVisibility(mesh);
            }else{
              childs[0]->computeVisibility(mesh);
              childs[1]->computeVisibility(mesh);
            }
          }

          /*And-ing the visibility values*/
          visibility = childs[0]->visibility & childs[1]->visibility;
          visibilityExt = childs[0]->visibilityExt & childs[1]->visibilityExt;
        }

        if(visibility){
          visible = true;
        }
      }

      template<class A, class B, class C, class D>
      void computePerimeter(DCTetraMesh<T, A, B, C, D>* mesh){
        Tree<int>::Iterator it = interiorEdges.begin();
        perimeter = 0;

        while(it != interiorEdges.end()){
          int edge = *it++;

          mesh->setCurrentEdge(edge);

          Vector4<T> ea = mesh->vertices[mesh->getOriginVertex()].coord;
          Vector4<T> eb = mesh->vertices[mesh->getTwinOriginVertex()].coord;

          perimeter += (ea-eb).length();
        }
      }

      template<class A, class B, class C, class D>
      T computeOverlap(Self* node, DCTetraMesh<T, A, B, C, D>* mesh){
        /*Overlapping edges in this surface, have twin edges in the other*/
        Tree<int> overlappingEdges;
        //Tree<int> twinEdges;
        Tree<int>::Iterator it = interiorEdges.begin();

        while(it != interiorEdges.end()){
          int edge = *it++;
          mesh->setCurrentEdge(edge);

          int twinEdge = mesh->getTwinEdge();
          int index = node->interiorEdges.findIndex(twinEdge);

          if(index != Tree<int>::undefinedIndex){
            overlappingEdges.uniqueInsert(twinEdge, twinEdge);
          }
        }
        T overlap = 0;

        it = overlappingEdges.begin();

        while(it != overlappingEdges.end()){
          int edge = *it++;
          mesh->setCurrentEdge(edge);
          Vector4<T> ea = mesh->vertices[mesh->getOriginVertex()].coord;
          Vector4<T> eb = mesh->vertices[mesh->getTwinOriginVertex()].coord;

          overlap += (ea - eb).length();
        }
        return overlap;
      }

      /*Check*/
      template<class A, class B, class C, class D>
      bool projectionsValid(DCTetraMesh<T, A, B, C, D>* mesh,
                            List<int>* invalidFaces){
        if(leaf == true){
          /*Update extended face*/
          mesh->getDisplacedTriangle(&triExt, face);

          for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
              for(int k=0;k<3;k++){
                if((i == 1) && (j == 1) && (k == 1)){
                  /*Skip, center*/
                }else{
                  Vector4f testVector(1-i, 1-j, 1-k, 0);

                  if(dot(testVector, triExt.n) >= 0){
                    visibilityExt |= 1 << (i*9+j*3+k);
                  }
                }
              }
            }
          }
        }else{
          if(childs[0] == childs[1]){
            /*Nothing to check*/
            bool valid = childs[0]->projectionsValid(mesh, invalidFaces);
            visibilityExt = childs[0]->visibilityExt;
          }else{
            int originalVisibilityExt = visibilityExt;
            bool valid1 = childs[0]->projectionsValid(mesh, invalidFaces);
            bool valid2 = childs[1]->projectionsValid(mesh, invalidFaces);

            visibilityExt = childs[0]->visibilityExt & childs[1]->visibilityExt;

            if(!visibility){
              /*Current node could have self collision, no need for
                further inspection*/
            }else{

            }
          }
        }
        return false;
      }

      /*This function is called at convergence. If an invalid face is
        detected here, there is no need to report it.*/
      template<class A, class B, class C, class D>
      bool volumesValid(DCTetraMesh<T, A, B, C, D>* mesh,
                        List<int>* invalidFaces){
        if(leaf == true){
          /*Check if displaced triangle is contained by this bounding box*/
          try{
            mesh->getDisplacedTriangle(&triExt, face, false);
          }catch(Exception* e){
            warning("Singular face %d %s", face, e->getError().c_str());
            delete e;

            /*Since this face is singular we don't care.*/
            return true;
          }

          if(box.inside(triExt.v[0]) &&
             box.inside(triExt.v[1]) &&
             box.inside(triExt.v[2])){
            /*All vertices of triangle are bound by the volume*/
            return true;
          }else{
            message("Face %d not in box", face);
            std::cout << triExt << std::endl;
            std::cout << box << std::endl;
            invalidFaces->append(face);
            return false;
          }
        }else{
          if(childs[0] == childs[1]){
            return childs[0]->volumesValid(mesh, invalidFaces);
          }else{
            return (childs[0]->volumesValid(mesh, invalidFaces) &&
                    childs[1]->volumesValid(mesh, invalidFaces));
          }
        }
      }

      template<class A, class B, class C, class D>
      void updateBBox(DCTetraMesh<T, A, B, C, D>* mesh){
        if(leaf == true){
          box.reset();

          /*Update triangle*/
          try{
            mesh->getTriangle(&tri, face, false);
            mesh->getDisplacedTriangleExt(&triExt, face, false);
          }catch(Exception* e){
            warning("Singular face found %s", e->getError().c_str());
            delete e;
          }

          /*However, the vertices are still correct, so we can create a
            correct bounding box.*/
          box.addPoint(tri.v[0]);
          box.addPoint(tri.v[1]);
          box.addPoint(tri.v[2]);

          box.addPoint(triExt.v[0]);
          box.addPoint(triExt.v[1]);
          box.addPoint(triExt.v[2]);

          box.addEpsilon((T)BBOXEPS);
        }else{
          if(childs[0] == childs[1]){
            childs[0]->updateBBox(mesh);
            box = childs[0]->box;
          }else{
            childs[0]->updateBBox(mesh);
            childs[1]->updateBBox(mesh);
            box = childs[0]->box;
            box.addBox(childs[1]->box);
          }
        }
      }

      /*Bottom up*/
      void computeLevelsBU(){
        if(leaf){
          level = 0;
        }else{
          if(childs[0] == childs[1]){
            childs[0]->computeLevelsBU();
            level = childs[0]->level + 1;
          }else{
            childs[0]->computeLevelsBU();
            childs[1]->computeLevelsBU();
            level = Max(childs[0]->level, childs[1]->level) + 1;
          }
        }
      }

      /*Top down*/
      void computeLevelsTD(int parentLevel){
        if(leaf){
          level = parentLevel;
        }else{
          if(childs[0] == childs[1]){
            childs[0]->computeLevelsTD(parentLevel-1);
            level = parentLevel;
          }else{
            childs[0]->computeLevelsTD(parentLevel-1);
            childs[1]->computeLevelsTD(parentLevel-1);
            level = parentLevel;
          }
        }
      }

      void setRoot(Self* node){
        root = node;
        if(leaf){
          return;
        }else{
          if(childs[0] == childs[1]){
            childs[0]->setRoot(node);
          }else{
            childs[0]->setRoot(node);
            childs[1]->setRoot(node);
          }
        }
      }

      int getDepth()const{
        if(leaf){
          return 1;
        }else{
          return 1+MAX(childs[0]->getDepth(), childs[1]->getDepth());
        }
      }

      int maxAdjacency()const{
        int adjSize = adjacentFaces.size();
        if(!leaf){
          if(childs[0]){
            if(childs[0] == childs[1]){
              int mx0 = childs[0]->maxAdjacency();
              return adjSize>mx0?adjSize:mx0;
            }else{
              int mx0 = childs[0]->maxAdjacency();
              int mx1 = childs[1]->maxAdjacency();
              int mx3 = mx0>mx1?mx0:mx1;
              return adjSize>mx3?adjSize:mx3;
            }
          }
        }else{
          return adjSize;
        }
        return 0;
      }

      void printSpaces(int dp){
        for(int i=0;i<dp*4;i++){
          std::cout << ' ';
        }
      }

      void printVisibility(int dp){
        printSpaces(dp);
        for(int i=0;i<27;i++){
          int mask = 1<<i;
          int val  = (visibility & mask)>>i;
          std::cout << val;
        }
        std::cout << std::endl;
      }

      bool checkAdjacency(Self* query,
                          int* adjacentNodes,
                          int* nAdjacent, int max_adj){
        int id = nodeId;
        int qid = query->nodeId;
        int n = nAdjacent[nodeId];

        int res = binarySearch(adjacentNodes + id * max_adj, qid, 0, n);

        if(res == -1){
          return false;
        }
        return true;
      }

      bool findSelfCollisions(Self* query,
                              List<int>* list,
                              int* adjacentNodes,
                              int* nAdjacent, int max_adj){
        Self* stack[1000];
        int top = 0;
        bool collision = false;

        stack[top++] = this;

        while(top != 0){
          Self* stackTop = stack[top-1];

          if(stackTop == query){
            /*Skip*/
            top--;
            continue;
          }

          bool boxIntersection = false;
          bool leafAdjCheck = false;

          if(stackTop->singleNode){
            boxIntersection = true;
          }else{
            boxIntersection = stackTop->box.intersection(query->box);
          }

          if(boxIntersection){
            /*Possible collision*/

            /*Check if this node is a parent of query node*/
            Self* child = query;

            /*Check if query and current node share the same root. If
              so, check for self collisions. Otherwise, there are no
              self collisions, skip self collision check.*/
            if(!stackTop->singleNode){
              /*Traverse the tree to the root until the current node has
                no parent*/
              bool skip = false;
              while(!skip){
                if(child == stackTop){
                  /*The query node is a part of the sub-surface
                    represented by this node. If visibility has one bit
                    enabled, the subsurface is free of self
                    collisions. Hence this node does not collide with
                    the query node. */
                  if(stackTop->visibility){
                    /*There exists a vector defining a plane on which
                      this node and it children can project without
                      self-intersection*/

                    /*If there exists such a vector AND the query node
                      is a deep child of this node, there is no (self)
                      intersection*/
                    skip = true;
                  }
                  break;
                }
                if(child == query->root){
                  /*We have reached the root of the object associated
                    with the query node*/
                  break;
                }

                if(stackTop->level == child->level){
                  /*We have reached a node in the chain to the root
                    which is at the same depth as this node. Check if
                    that node is an adjacent node of the current node.*/

                  if(stackTop->visibility & child->visibility){
                    /*If there exists a vector defining a plane on which
                      both adjacent subtrees can project, there is no
                      (self) collision.*/

                    leafAdjCheck = true;

                    if(stackTop->checkAdjacency(child, adjacentNodes,
                                                nAdjacent, max_adj)){
                      skip = true;
                    }
                  }else{
                    /*Current node and this node are not adjacent.*/
                  }
                  /*Current node and child node are not adjacent or
                    adjacent but not collision free projection. Continue
                    with recursive collision check*/
                  break;
                }
                child = child->parent;
              }
              if(skip){
                top--;
                continue;
              }
            }

            if(stackTop->leaf){
              bool adjacentFaces = false;

              /*Query node and this node share the same root. Check if the
                nodes are adjacent. If so, there is no collision
                possible.*/
              if(leafAdjCheck){
                adjacentFaces =
                  stackTop->checkAdjacency(child, adjacentNodes,
                                           nAdjacent, max_adj);
              }

              if(adjacentFaces){
                top--;
              }else{
                list->append(stackTop->id);
                collision = true;
                top--;
              }
            }else{
              if(stackTop->childs[0] == stackTop->childs[1]){
                Self* c1 = stackTop->childs[0];
                top--;
                stack[top++] = c1;
              }else{
                Self* c1 = stackTop->childs[0];
                Self* c2 = stackTop->childs[1];
                top--;
                stack[top++] = c1;
                stack[top++] = c2;
              }
            }
          }else{
            top--;
          }
        }
        return collision;
      }

      bool findCollisions(Self* query,
                          List<int>* list){
        Self* stack[1000];
        int top = 0;
        bool collision = false;

        stack[top++] = this;

        while(top != 0){
          Self* stackTop = stack[top-1];

          if(stackTop == query){
            /*Skip*/
            top--;
            continue;
          }

          if(stackTop->root == query->root && query->root != 0){
            top--;
            continue;
          }

          bool boxIntersection = false;

          if(stackTop->singleNode){
            boxIntersection = true;
          }else{
            boxIntersection = stackTop->box.intersection(query->box);
          }

          if(boxIntersection){
            if(stackTop->leaf){
              list->append(stackTop->id);
              collision = true;
              top--;
            }else{
              if(stackTop->childs[0] == stackTop->childs[1]){
                Self* c1 = stackTop->childs[0];
                top--;
                stack[top++] = c1;
              }else{
                Self* c1 = stackTop->childs[0];
                Self* c2 = stackTop->childs[1];
                top--;
                stack[top++] = c1;
                stack[top++] = c2;
              }
            }
          }else{
            top--;
          }
        }
        return collision;
      }

      bool checkAdjacencyR(Self* query){
        int index = adjacentFaces.findIndex(query);
        if(index == Tree<int>::undefinedIndex){
          return false;
        }
        return true;
      }

      template<class A, class B, class C, class D>
      bool findCollisionsR(Self* query,
                           const DCTetraMesh<T, A, B, C, D>* mesh,
                           List<int>* list = 0, bool skipCheck = false){
        bool dump = false;

        if(root == query->root){
          /*Current node has same root as query node. If query node is
            of type static or rigid, we can stop here since rigid and
            static objects do not have self collisions.*/
          if(Mesh::isRigidOrStatic(mesh->halfFaces[query->face].type)){
            return false;
          }
        }

        if(query->face == 2910 ||
           query->face == 2906){
          //dump = true;
        }

        //findcall++;
        if(query == this){
          return false;
        }

        bool boxIntersection = false;
        if(skipCheck){
          boxIntersection = true;
        }else{
          boxIntersection = box.intersection(query->box);
        }

        if(boxIntersection){
          /*Possible collision*/


          /*Check if this node is a parent of query node*/
          Self* child = query;

          /*Check if query and current node share the same root. If so,
            check for self collisions. Otherwise, there are no self
            collisions, skip self collision check.*/
          if(!skipCheck && query->root == root){
            /*Traverse the tree to the root until the current node has
              no parent*/
            while(true){
              if(child == this){
                /*The query node is a part of the sub-surface
                  represented by this node. If visibility has one bit
                  enabled, the subsurface is free of self
                  collisions. Hence this node does not collide with the
                  query node. */
                if(visibility && visibilityExt){
                  /*There exists a vector defining a plane on which this
                    node and its children can project without
                    self-intersection*/

                  /*If there exists such a vector AND the query node is
                    a deep child of this node, there is no (self)
                    intersection*/
                  ///return false;
                }
                break;
              }
              if(child == 0){
                /*We have reached the root of the object associated with
                  the query node*/
                break;
              }

              if(level == child->level){
                /*We have reached a node in the chain to the root which
                  is at the same depth as this node. Check if that node
                  is an adjacent node of the current node.*/
                /*TODO: figure out what to do here*/

                if(/*visibility & child->visibility &
                     visibilityExt & child->visibilityExt*/false){
                  /*If there exists a vector defining a plane on which
                    both adjacent subtrees can project, there is no
                    (self) collision.*/

                  if(checkAdjacencyR(child)){
                    return false;
                  }
                }else{
                  /*Current node and this node are not adjacent.*/
                }
                /*Current node and child node are not adjacent or
                  adjacent but not collision free projection. Continue
                  with recursive collision check*/
                break;
              }
              child = child->parent;
            }
          }

          if(leaf){
            bool valid = true;
            bool adjacentFaces = false;

            if(query->root == root){
              if(dump){
                warning("face %d and face %d share same root",
                        face, query->face);
              }
              /*Query node and this node share the same root. Check if
                the nodes are adjacent. If so, there is no collision
                possible.*/
              adjacentFaces = false;//checkAdjacencyR(query);
              if(adjacentFaces){
                if(dump){
                  warning("adjacent, skip");
                }
                return false;
              }else{
                if(dump){
                  warning("not adjacent");
                }
              }
            }

            if(valid && !adjacentFaces){
              //collision = true;
              //query->collision = true;

              if(dump){
                warning("adding %d, %d", face, query->face);
              }

              if(list){
                //message("collision %d - %d", query->id, id);
                list->append(id);
              }
              return true;
            }else{
              return false;
            }
          }else{
            if(childs[0] == childs[1]){
              return childs[0]->findCollisionsR(query, mesh, list, singleNode);
            }else{
              bool c1 = childs[0]->findCollisionsR(query, mesh, list);
              bool c2 = childs[1]->findCollisionsR(query, mesh, list);
              return c1 || c2;
            }
          }
        }
        return false;
      }

      bool findDebugCollisions(Self* query,
                               List<int>* list = 0, bool skipCheck = false){
        warning("findDebugCollisions, level = %d, ids %d, %d", level, id, query->id);
        if(query == this){
          warning("query == this");
          return false;
        }

        bool boxIntersection = false;
        if(skipCheck){
          boxIntersection = true;
        }else{
          boxIntersection = box.intersection(query->box);
        }

        if(boxIntersection){
          /*Possible collision*/

          warning("bounding box intersection");

          /*Check if this node is a parent of query node*/
          Self* child = query;

          /*Check if query and current node share the same root. If so,
            check for self collisions. Otherwise, there are no self
            collisions, skip self collision check.*/
          if(!skipCheck && query->root == root){
            warning("Both query and this node have the same root");
            /*Traverse the tree to the root until the current node has
              no parent*/
            while(true){
              warning("traverse up levels :: %d, %d", this->level, child->level);
              if(child == this){
                warning("query and subject node share this node");
                /*The query node is a part of the sub-surface
                  represented by this node. If visibility has one bit
                  enabled, the subsurface is free of self
                  collisions. Hence this node does not collide with the
                  query node. */

                warning("vis %d, %d", visibility, visibilityExt);
                if(visibility && visibilityExt){
                  /*There exists a vector defining a plane on which this
                    node and its children can project without
                    self-intersection*/

                  /*If there exists such a vector AND the query node is
                    a deep child of this node, there is no (self)
                    intersection*/
                  warning("Shared node is completely visible, no collisions");
                  return false;
                }
                break;
              }
              if(child == 0){
                /*We have reached the root of the object associated with
                  the query node*/
                warning("root reached, break");
                break;
              }

              if(level == child->level){
                /*We have reached a node in the chain to the root which
                  is at the same depth as this node. Check if that node
                  is an adjacent node of the current node.*/

                warning("parent nodes found on the same level, %d", level);
                warning("vis bits this  %d %d", visibility, visibilityExt);
                warning("vis bits child %d %d", child->visibility,
                        child->visibilityExt);

                warning("%d", visibility & child->visibility);
                warning("%d", visibilityExt & child->visibilityExt);

                if(/*visibility & child->visibility &
                     visibilityExt & child->visibilityExt*/false){
                  /*If there exists a direction from which we can see
                    all faces in the normal and extended adjacent
                    subtrees, there cant be a (self) collision*/

                  if(checkAdjacencyR(child)){
                    warning("Adjacent nodes");
                    return false;
                  }
                }else{
                  /*Current node and this node are not adjacent.*/
                }
                /*Current node and child node are not adjacent or
                  adjacent but not collision free projection. Continue
                  with recursive collision check*/
                break;
              }
              child = child->parent;
            }
          }

          warning("Root reached");

          if(leaf){
            warning("this node is leaf");
            bool valid = true;
            bool adjacentFaces = false;

            if(query->root == root){
              warning("Nodes share same root");
              /*Query node and this node share the same root. Check if
                the nodes are adjacent. If so, there is no collision
                possible.*/
              adjacentFaces = checkAdjacencyR(query);
              if(adjacentFaces){
                warning("adjacent, skip");
                return false;
              }else{
                warning("not adjacent");
              }
            }

            if(valid && !adjacentFaces){
              //collision = true;
              //query->collision = true;

              warning("valid and not adjacent");

              if(list){
                //message("collision %d - %d", query->id, id);
                list->append(id);
              }
              return true;
            }else{
              return false;
            }
          }else{
            if(childs[0] == childs[1]){
              return childs[0]->findDebugCollisions(query, list,
                                                    singleNode);
            }else{
              bool c1 = childs[0]->findDebugCollisions(query, list);
              bool c2 = childs[1]->findDebugCollisions(query, list);
              return c1 || c2;
            }
          }
        }else{
          warning("No bounding box intersections");
        }
        return false;
      }

      void collectNeighboringFaces(const BBox<T>& query, List<int>& faces){
        if(box.intersection(query)){
          if(leaf){
            faces.append(id);
          }else{
            if(childs[0] == childs[1]){
              childs[0]->collectNeighboringFaces(query, faces);
            }else{
              childs[0]->collectNeighboringFaces(query, faces);
              childs[1]->collectNeighboringFaces(query, faces);
            }
          }
        }
      }

      void traceLine(const Vector4<T>& a, const Vector4<T>& b, Tree<int>* faces){
        Vector4<T> col;
        Vector4<T> bary;

        Ray<T> ray;

        Vector4<T> dir = b-a;
        ray.setOrigin(a);
        ray.setDirection(dir);

        if(box.intersects(ray)){
          if(leaf){
            T t = -1000;

            if(tri.lineIntersection(a, dir, col, &t, &bary)){
              faces->uniqueInsert(face);
              if(t == -1000){
                error("T not set while intersecting");
              }
            }
          }else{
            /*Trace childs*/
            if(childs[0] == childs[1]){
              childs[0]->traceLine(a, b, faces);
            }else{
              childs[0]->traceLine(a, b, faces);
              childs[1]->traceLine(a, b, faces);
            }
          }
        }else{
          /*No box intersection*/
        }
      }

      /*When both back and front faces are searched, it is possible that
        a ray exactly hits a shared edge of two faces. When used in a
        raytracing setting, the next ray starts from the intersection
        point of the previous ray. When both back and front faces are
        searched, this method will return the naighboring face, since
        that is the closest, but is in principle not correct.*/
      int traceRay(const Ray<T>& ray, Vector4<T>& col, int self,
                   RayTraceMode mode,
                   T* distance = 0, Vector4<T>* bary = 0, int* depth = 0)const{
        T epsilon = (T)0.0;
        if(mode != FrontAndBackFace){
          /*When only front or backfaces are traced, we can move the
            startingpoint of a ray slightly backwards. This ensures a
            proper ray-face intersection test in case of touching
            objects.*/
          epsilon = (T)1e-5;
        }

        if(box.intersects(ray)){
          if(leaf){
            if(face == self){
              return -1;
            }

            //Check ray triangle intersection
            T t;
            //float nd = -1;//tri.n * ray.dir;
            T nd = dot(tri.n, ray.getDirection());

            bool check = false;
            if(mode == FrontAndBackFace && Abs(nd) > 0.0f){
              check = true;
            }else if(mode == FrontFace && nd < 0.0f){
              check = true;
            }else if(mode == BackFace && nd > 0.0f){
              check = true;
            }

            if(check){
              //          if(nd < 0){
              if(tri.intersects(ray, col, &t, bary)){
                if(t <= -epsilon){
                  return -1;
                }
                if(distance){
                  //Vector4f dir = ray.getDirection();
                  //dir.normalize();
                  *distance = t;//-(ray.getOrigin() - col)*dir;
                }

                return face;
              }else{
                return -1;
              }
            }else{
              return -1;
            }
          }else{
            T d1, d2;
            Vector4<T> b1, b2;
            Vector4<T> c1, c2;
            int depth1 = *depth+1;
            int depth2 = *depth+1;
            d1 = d2 = 1e+10;
            int t1, t2;
            t1 = t2 = -1;

            if(childs[0] == childs[1]){
              t1 = childs[0]->traceRay(ray, c1, self, mode, &d1, &b1, &depth1);
            }else{
              t1 = childs[0]->traceRay(ray, c1, self, mode, &d1, &b1, &depth1);
              t2 = childs[1]->traceRay(ray, c2, self, mode, &d2, &b2, &depth2);
            }

            if(t1 == -1 && t2 == -1){
              //ok
            }else{
              cgfassert(t1 != t2);
            }

            if(childs[0] == childs[1]){
              if(distance){
                *distance = d1;
              }
              if(bary){
                *bary = b1;
              }
              col = c1;
              return t1;
            }else{
              if(Abs(d1) < Abs(d2)){
                if(distance){
                  *distance = d1;
                }
                if(bary){
                  *bary = b1;
                }
                col = c1;
                return t1;
              }else{
                if(distance){
                  *distance = d2;
                }
                if(bary){
                  *bary = b2;
                }
                col = c2;
                return t2;
              }
            }
          }
        }

        if(distance){
          *distance = 1e+10;
        }
        return -1;
      }

      //protected:
      Self* root; /*Root of the object != root of tree*/
      Self* parent;
      Self* childs[2];

      Vector4<T> averageNormal;
      T area;
      int face;
      int minFace;
      int maxFace;
      BBox<T> box;
      bool leaf;
      //List<SurfaceNode*> neighboringFaces;
      Tree<Self*> neighboringFaces;
      Tree<Self*> adjacentFaces;
      //SurfaceNode*       adjacentFaceIndices;
      Tree<int> interiorEdges;
      int id;
      int level;
      Vector4f color;
      SurfaceType type;

      int visibility;
      int visibilityExt;
      bool visible;

      T perimeter;

      Triangle<T> tri;
      Triangle<T> triExt;

      int nodeId;  /*Used to compute a unique identifier*/
      bool singleNode;

      template<class A, class B, class C, class D, class E>
      friend class SurfaceTree;

      template<class A, class B, class C, class D, class E>
      friend class GlSurfaceTree;
    };
  }


  template<class T>
  class Compare<SurfaceTreeInternals::SurfaceNode<T>*>{
  public:
    typedef SurfaceTreeInternals::SurfaceNode<T>* SNP;
    static bool less(const SNP& a, const SNP& b){
      return a < b;
    }

    static bool equal(const SNP& a, const SNP& b){
      return a == b;
    }
  };

  template<class T>
  inline bool SurfaceNodeCompare<T>::snless(const SNP& a,
                                            const SNP& b){
    return Compare<Vector4<T> >::less(a->box.getMin(), b->box.getMin());
  }

  template<class T>
  inline bool SurfaceNodeCompare<T>::snequal(const SNP& a,
                                             const SNP& b){
    //return a->box.center() == b->box.center();
    return a == b;
    Vector4<T> va = a->box.center();
    Vector4<T> vb = b->box.center();
    if((va - vb).length() < (T)1E-6){
      return true;
    }
    return false;
  }

  template<class T>
  inline bool SurfaceNodeCompare<T>::snless2(const SNP& a,
                                             const SNP& b){
    return a->area > b->area;
    //return a < b;
  }

  template<class T>
  inline bool SurfaceNodeCompare<T>::snequal2(const SNP& a,
                                              const SNP& b){
    //return true;
    //return a->box.center() == b->box.center();
    return a == b;
    Vector4<T> va = a->box.center();
    Vector4<T> vb = b->box.center();
    if((va - vb).length() < (T)1E-6){
      return true;
    }
    return false;
  }

  template<>
  inline const Vector4<float> getPosition<SurfaceTreeInternals::SurfaceNode<float>*, float>(SurfaceTreeInternals::SurfaceNode<float>*& a){
    return a->box.center();
  }

  template<>
  inline const Vector4<double> getPosition<SurfaceTreeInternals::SurfaceNode<double>*, double>(SurfaceTreeInternals::SurfaceNode<double>*& a){
    return a->box.center();
  }

  template<class T, class A=char, class B=char, class C=char, class D=char>
  class SurfaceTree{
  public:
    typedef SurfaceTreeInternals::SurfaceNode<T> Node;

  private:
    typedef typename List<Node*>::Iterator NodePtrListIterator;
    typedef typename Tree<Node*>::Iterator NodePtrTreeIterator;
    typedef Octree<Node*, T>               NodeOctree;
  public:

    SurfaceTree(DCTetraMesh<T, A, B, C, D>* m){
      mesh = m;
      leafNodes = root = 0;
      adjacentNodes = 0;
      nAdjacent = 0;
    }

    virtual ~SurfaceTree(){
      delete [] leafNodes;
      delete root;

      if(adjacentNodes){
        delete[] adjacentNodes;
      }
      if(nAdjacent){
        delete[] nAdjacent;
      }
    }

    BBox<T> getBBox()const{
      if(mesh->getNHalfFaces() != 0){
        return root->box;
      }else{
        return BBox<T>();
      }
    }

    /*@TODO: Update the structure of the tree above the level where
      complete objects are grouped. Due to the movement of the
      objects, the current structure can be inefficient.*/
    void update(){
      if(mesh->getNHalfFaces() != 0){
        mesh->computeDisplacement((Vector<T>*)0x0, (T)0.0);
        root->updateBBox(mesh);
        root->computeVisibility(mesh);
      }
    }

    void update(Vector<T>* velocity, T dt){
      if(mesh->getNHalfFaces() != 0){
        mesh->computeDisplacement(velocity, dt);
        root->updateBBox(mesh);
        root->computeVisibility(mesh);
      }
    }

    bool projectionsValid(List<int>* invalidFaces){
      if(mesh->getNHalfFaces() != 0){
        return root->projectionsValid(mesh, invalidFaces);
      }else{
        return true;
      }
    }

    bool volumesValid(List<int>* invalidFaces){
      if(mesh->getNHalfFaces() != 0){
        return root->volumesValid(mesh, invalidFaces);
      }else{
        return true;
      }
    }

    int getDepth()const{
      if(root){
        return root->getDepth();
      }else{
        return 0;
      }
    }

    int maxAdjacency()const{
      if(root){
        return root->maxAdjacency();
      }else{
        return 0;
      }
    }

    /*Breadth-first collection of nodes*/
    void collectNodesBFS(List<Node*>* list){
      List<Node*> queue;
      if(root == 0){
        return;
      }
      queue.append(root);
      list->append(root);

      while(queue.size() != 0){
        NodePtrListIterator it = queue.begin();
        Node* node = *it;

        if(node->childs[0]){
          if(node->childs[0] == node->childs[1]){
            list->append(node->childs[0]);
            queue.append(node->childs[0]);
          }else{
            list->append(node->childs[0]);
            queue.append(node->childs[0]);
            list->append(node->childs[1]);
            queue.append(node->childs[1]);
          }
        }
        queue.remove2(it);
      }
    }

    void computeNodeIds(){
      List<Node*> list;
      collectNodesBFS(&list);

      NodePtrListIterator it = list.begin();
      int currentId = 0;

      while(it != list.end()){
        Node* node = *it++;
        node->nodeId = currentId++;
      }
    }

    void saveTree(std::ofstream& out){
      if(out.is_open() && mesh->getNHalfFaces() != 0){
        /*Compute node ids*/
        computeNodeIds();

        /*Write root id of this tree*/
        if(root){
          out << root->nodeId << std::endl;
        }else{
          out << -1 << std::endl;
        }

        List<Node*> list;
        collectNodesBFS(&list);

        /*Write number of nodes*/
        out << list.size() << std::endl;

        /*Write max adjacency*/
        out << maxAdjacency() << std::endl;

        /*Write nodes*/

        NodePtrListIterator it = list.begin();

        while(it != list.end()){
          Node* currentNode = *it++;

          /*Write face id, if non-leaf node id = -1*/
          if(currentNode->leaf == true){
            out << currentNode->face << "\t";
          }else{
            out << -1 << "\t";
          }

          /*Write parent, in case of no parent, -1*/
          if(currentNode->parent){
            out << currentNode->parent->nodeId << "\t";
          }else{
            out << -1 << "\t";
          }

          /*Write childs, if none-> -1*/
          if(currentNode->childs[0]){
            if(currentNode->childs[0] == currentNode->childs[1]){
              out << currentNode->childs[0]->nodeId << "\t" << -1 << "\t";
            }else{
              out << currentNode->childs[0]->nodeId << "\t"
                  << currentNode->childs[1]->nodeId << "\t";
            }
          }else{
            out << -1 << "\t" << -1 << "\t";
          }

          /*Write visibility*/
          //out << currentNode->visibility << "\t";

          /*Write level*/
          out << currentNode->level << std::endl;

          /*Write bbox min and max*/
          //Vector4f mn = currentNode->box.getMin();
          //Vector4f mx = currentNode->box.getMax();
          //out << mn[0] << "\t" << mn[1] << "\t" << mn[2] << "\t";
          //out << mx[0] << "\t" << mx[1] << "\t" << mx[2] << std::endl;
        }

        it = list.begin();

        /*Write adjacent faces*/
        while(it != list.end()){
          Node* currentNode = *it++;
          out << currentNode->adjacentFaces.size();

          NodePtrTreeIterator adit = currentNode->adjacentFaces.begin();
          Tree<int> sortedAdjacent;

          while(adit != currentNode->adjacentFaces.end()){
            Node* adjNode = *adit++;
            sortedAdjacent.insert(adjNode->nodeId);
          }

          Tree<int>::Iterator sadit = sortedAdjacent.begin();
          while(sadit != sortedAdjacent.end()){
            int nodeId = *sadit++;
            out << "\t" << nodeId;
          }
          out << std::endl;
        }
      }
    }

    int traceRay(const Ray<T>& ray, Vector4<T>& col, int self,
                 RayTraceMode mode,
                 T* distance = 0,
                 Vector4<T>* bary = 0)const{
      if(mesh->getNHalfFaces() != 0){
        int depth = 0;
        return root->traceRay(ray, col, self, mode, distance, bary, &depth);
      }
      return -1;
    }

    /*Finds all intersections of line a-b and the triangles stored
      inside this tree. Line-triangle intersections that are outside
      the segment a-b are not detected.*/
    void traceLine(const Vector4<T>& a, const Vector4<T>& b, Tree<int>* tree){
      if(mesh->getNHalfFaces() != 0){
        root->traceLine(a, b, tree);
      }
    }

    bool facesShareSameRoot(int faceId1, int faceId2){
      Node* faceNode1 = &leafNodes[faceId1];
      Node* faceNode2 = &leafNodes[faceId2];

      return faceNode1->root == faceNode2->root;
    }

    void findPotentialFaceCollisions(int faceId, List<int>& faces){
      if(mesh->getNHalfFaces() == 0){
        return;
      }

      Node* faceNode = &leafNodes[faceId];
      List<int> potentialFaces;

      /*Find collisions between query face and faces of root*/
      //root->findCollisions(faceNode, &potentialFaces);
      //root->findCollisionsR(faceNode, &potentialFaces, adjacentNodes, nAdjacent,
      //                    max_adj);

      root->findCollisionsR(faceNode, mesh, &potentialFaces);

      /*Find self-collisions between query face and its root*/
      //faceNode->root->findSelfCollisions(faceNode, &potentialFaces,
      //                                 adjacentNodes, nAdjacent, max_adj);

      //faceNode->root->findCollisionsR(faceNode, &potentialFaces,
      //                              adjacentNodes, nAdjacent, max_adj);

      if(potentialFaces.size() == 0){
        return;
      }

      int origin = mesh->halfEdges[mesh->halfFaces[faceId].half_edge].origin;

      Vector4<T> f1_origin = mesh->vertices[origin].coord;
      Vector4<T> f1_n = mesh->halfFaces[faceId].normal;

      Mesh::GeometryType type1 = mesh->vertices[origin].type;

      List<int>::Iterator it = potentialFaces.begin();

      while(it != potentialFaces.end()){
        /*Check normals of both faces. If both faces are facing each
          other, keep this combination, otherwise, skip.*/
        int face = *it++;

        Mesh::GeometryType type2;

        if(dot(f1_n, mesh->halfFaces[face].normal) >= 0){
          //continue;
        }

        /*If a vertex of face2 is behind face1, ignore face1*/
        int verticesBehindFace = 0;

        int currentEdge = mesh->halfFaces[face].half_edge;
        mesh->setCurrentEdge(currentEdge);

        for(int i=0;i<3;i++){
          //float sd = f1.getSignedDistance(f2.v[i]);
          //Using a rough approximation is fine here
          int vertex = mesh->getOriginVertex();

          type2 = mesh->vertices[vertex].type;

          T sd = dot(f1_n, mesh->vertices[vertex].coord - f1_origin);

          if(sd <= 0){
            verticesBehindFace++;
          }
          mesh->nextEdge();
        }

        if(verticesBehindFace == 3){
          //continue;
        }

        if(type1 == Mesh::Static && type2 == Mesh::Static){
          /*Do not check two static faces, they can never collide*/
          continue;
        }

        if(type1 == Mesh::Rigid && type2 == Mesh::Rigid){
          /*If they belong to the same rigid object, skip. No
            collision possible.*/
          int object1 = mesh->vertexObjectMap[origin];
          int object2 = mesh->vertexObjectMap[mesh->getOriginVertex()];
          if(object1 == object2){
            continue;
          }
        }

        faces.append(face);
      }
    }

    void collectNeighboringFaces(const BBox<T>& query, List<int>& faces){
      if(mesh->getNHalfFaces() != 0){
        root->collectNeighboringFaces(query, faces);
      }
    }

    void computeLevels(){
      if(mesh->getNHalfFaces() != 0){
        root->computeLevelsBU();
        root->computeLevelsTD(root->level);
      }
    }

    /*An orphan node represents a single closed object not connected
      to other objects in this tree. In order to construct a full
      tree, these nodes should be merged together.*/
    void mergeOrphans(List<Node*>* nodes){
      /*Merge nodes with the largest overlap*/
      message("Merging individual objects");
      message("%d objects", nodes->size());

      NodePtrListIterator it = nodes->begin();

      if(nodes->size() == 1){
        root = *it;
        root->setRoot(root);
        return;
      }else{
        /*Set root information*/
        it = nodes->begin();

        while(it != nodes->end()){
          Node* objectRoot = *it++;
          objectRoot->setRoot(objectRoot);
        }

        it = nodes->begin();

        Tree<Node*>* t1 =
          new Tree<Node*>(&SurfaceNodeCompare<T>::snless,
                          &SurfaceNodeCompare<T>::snequal);
        Tree<Node*>* t2 =
          new Tree<Node*>(&SurfaceNodeCompare<T>::snless,
                          &SurfaceNodeCompare<T>::snequal);

        NodeOctree* octree = new NodeOctree();

        /*Fill octree*/
        while(it != nodes->end()){
          Node* n = *it++;
          octree->insert(n);
          t1->insert(n, 1);
        }
        octree->split(9999);

        int pass = 0;
        while(true){
          message("pass = %d\n\n\n", pass++);

          message("t1 size = %d", t1->size());

          if(t1->size() == 1){
            break;
          }
          NodePtrTreeIterator it2 = t1->begin();
          while(it2 != t1->end()){
            Node* node = *it2;

            List<Node*> neighborhood;

            Vector4<T> pos = node->box.center();
            Vector4<T> mn  = node->box.getMin();
            Vector4<T> mx  = node->box.getMax();

            T radius = (T)0.5*(mn-mx).length();

            std::cout << pos;
            std::cout << mn;
            std::cout << mx;

            PRINT(node->box);

            PRINT(radius);
            //message("node = %p, radius = %10.10e", radius);
            //std::cout << pos;

            while(neighborhood.size() == 0){
              octree->getNeighboringElements(pos, (T)radius, neighborhood);
              message("neighborhood.size = %d, radius = %10.10e",
                      neighborhood.size(), radius);
              if(neighborhood.size() == 1){
                neighborhood.clear();
                /*Only result is the query node*/
              }
              radius *= 1.5f;
            }

            NodePtrListIterator it3 = neighborhood.begin();
            Node* bestCandidate = 0;
            T volume = 100000000;
            message("n_neighbors = %d", neighborhood.size());

#if 1

            //float dist = (box.center() - node2->box.center()).length();

            Vector4<T> mn3 = node->box.getMin();
            Vector4<T> mx3 = node->box.getMax();
            Vector4<T> dd3 = mn3 - mx3;

            T origVol = Abs(dd3[0] * dd3[1] * dd3[2]);
#endif

            /*Minimize volume of new node*/
            while(it3 != neighborhood.end()){
              Node* node2 = *it3++;
              if(node2 == node){
                message("skip, node = node2");
                continue;
              }

              BBox<T> box = node->box;

              box.addPoint(node2->box.getMin());
              box.addPoint(node2->box.getMax());

              Vector4<T> mn2 = box.getMin();
              Vector4<T> mx2 = box.getMax();
              Vector4<T> dd = mn2 - mx2;
              T newVol = Abs(dd[0] * dd[1] * dd[2]);

              T factor = newVol/origVol;

              message("origVol = %10.10e, newVol = %10.10e", origVol, newVol);
              message("factor = %10.10e, oldFactor = %10.10e",
                      factor, volume);

              //factor*=dist;

              //factor = newVol;

              if(newVol <= volume){
                volume = newVol;
                bestCandidate = node2;
              }
            }

            cgfassert(bestCandidate != 0);
            cgfassert(bestCandidate != node);

            Node* parentNode = new Node();
            parentNode->parent = 0;
            parentNode->childs[0] = node;
            parentNode->childs[1] = bestCandidate;
            parentNode->area = 0;
            parentNode->box = node->box;
            parentNode->box.addPoint(bestCandidate->box.getMin());
            parentNode->box.addPoint(bestCandidate->box.getMax());
            parentNode->face = 0;
            parentNode->leaf = false;
            parentNode->level = Max(node->level,
                                    bestCandidate->level)+1;

            if(node == bestCandidate){
              parentNode->singleNode = false;
            }else{
              parentNode->singleNode = true;
            }

            parentNode->minFace =
              Min(node->minFace, bestCandidate->minFace);
            parentNode->maxFace =
              Max(node->maxFace, bestCandidate->maxFace);

            octree->remove(node);
            octree->remove(bestCandidate);

            t2->insert(parentNode, 1);
            t1->remove(it2);

            t1->remove(bestCandidate);
            it2 = t1->begin();
            message("size t1 after removal = %d", t1->size());
            message("size t2 after removal = %d", t2->size());
            message("oct size = %d", octree->size());

            if(t1->size() == 1){
              t2->insert(*it2);

              octree->remove(*it2);
              t1->remove(it2);
            }
          }
          swap(&t1, &t2);
          delete octree;
          octree = new NodeOctree();

          NodePtrTreeIterator it4 = t1->begin();
          int count = 0;
          while(it4 != t1->end()){
            Node* node = *it4++;
            octree->insert(node);
            count++;
          }

          octree->split(999);
        }

        root = *(t1->begin());
        delete octree;
        delete t1;
        delete t2;
        return;
      }
    }

    int getCommonVisibility(Node* a, Node* b){
      int result = a->visibility & b->visibility;
      /*Not efficient, use popcount.*/
      int count = 0;
      for(int i=0;i<27;i++){
        int mask = 1<<i;
        if(result&mask){
          count++;
        }
      }
      return count;
    }

    void debugPair(int face1, int face2){
      Node* node1 = &leafNodes[face1];
      Node* node2 = &leafNodes[face2];
      Node* commonRoot;

      List<Node*> path1;
      List<Node*> path2;

      /*Find common node*/
      {
        Node* root1 = node1;
        Node* root2 = node2;

        while(root1 != root2){
          if(root1->level == root2->level){
            path1.append(root1);
            path2.append(root2);

            root1 = root1->parent;
            root2 = root2->parent;

          }else if(root1->level > root2->level){
            root2 = root2->parent;
            path2.append(root2);
          }else{
            root1 = root1->parent;
            path1.append(root1);
          }
        }

        commonRoot = root1;
      }


      List<int> faces1;
      List<int> faces2;

      warning("test %d, %d", face1, face2);
      commonRoot->findDebugCollisions(node1, &faces1);
      warning("test %d, %d", face2, face1);
      commonRoot->findDebugCollisions(node2, &faces2);

      List<int>::Iterator it1 = faces1.begin();
      while(it1 != faces1.end()){
        warning("faces %d, %d", face1, *it1++);
      }

      List<int>::Iterator it2 = faces2.begin();
      while(it2 != faces2.end()){
        warning("faces %d, %d", face2, *it2++);
      }

    }

    void constructTree(){
      PRINT_FUNCTION;
      if(mesh->getNHalfFaces() == 0){
        /*No mesh, nothing to construct*/
        return;
      }

      Tree<Node*>* faceTree1 = new Tree<Node*>();
      Tree<Node*>* faceTree2 = new Tree<Node*>();

      Vector4<T> eps((T)1e-6, (T)1e-6, (T)1e-6, 0);

      /*List for orphaned nodes, which are individual objects*/
      List<Node*> orphanNodes;

      /*Create leaf nodes. Each leafnode represents one face element*/
      leafNodes = new Node[mesh->getNHalfFaces()];

      message("V = %d, E = %d, F = %d", mesh->getNVertices(),
              mesh->getNHalfEdges(),
              mesh->getNHalfFaces());

      for(int i=0;i<mesh->getNHalfFaces();i++){
        /*Add current face node to search tree*/
        faceTree1->insertc(&(leafNodes[i]), i);

        leafNodes[i].face = i;
        Triangle<T> t;
        mesh->getTriangle(&t, i, false);
        leafNodes[i].averageNormal = t.n;
        leafNodes[i].area = t.getArea();
        leafNodes[i].leaf = true;
        leafNodes[i].id   = i;
        leafNodes[i].level = 0;
        leafNodes[i].type = FACE;
        leafNodes[i].tri = t;
        leafNodes[i].minFace = i;
        leafNodes[i].maxFace = i;
        leafNodes[i].singleNode = false;

        cgfassert(leafNodes[i].childs[0] == 0);
        cgfassert(leafNodes[i].childs[1] == 0);
      }

      message("Leafnodes created, construct tree");

      for(int i=0;i<mesh->getNHalfFaces();i++){
        /*Fill neighbor information*/
        int edge = mesh->halfFaces[i].half_edge;
        mesh->setCurrentEdge(edge);

        while(true){
          int twinFace = mesh->getTwinFace();
          int currentEdge = mesh->getCurrentEdge();

          /*Twin-face is a direct neighbor of face i*/
          Node* nd = &(leafNodes[twinFace]);
          leafNodes[i].neighboringFaces.uniqueInsert(nd, twinFace);

          /*Add current vertex to bounding box*/
          leafNodes[i].box.addPoint(mesh->vertices[mesh->getOriginVertex()].coord);
          leafNodes[i].interiorEdges.uniqueInsert(currentEdge, currentEdge);

          /*Collect all faces adjacent to originVertex of current
            edge*/

          List<int> adjacent;
          mesh->getSurroundingFacesOfVertex(mesh->vertices[mesh->getOriginVertex()], adjacent);

          List<int>::Iterator adjit = adjacent.begin();
          while(adjit != adjacent.end()){
            int adjFace = *adjit++;
            if(adjFace != i){
              Node* node = &(leafNodes[adjFace]);
              leafNodes[i].adjacentFaces.uniqueInsert(node, adjFace);
            }
          }
          mesh->setCurrentEdge(currentEdge);
          mesh->nextEdge();
          if(mesh->getCurrentEdge() == edge){
            break;
          }
        }
        leafNodes[i].computePerimeter(mesh);
        leafNodes[i].computeVisibility(mesh, false);
      }

      /*In the next level we have to merge sub-surfaces by searching
        all adjacent sub-surface. These are found by collecting all
        unique parents of surrounding nodes.

        Select a node from the temporary tree, find a surrounding
        sub-surface which has the smallest normal deviation. Create a
        new parent node, store in temporary tree, and remove the
        merged nodes from the temporary tree.*/
      int depth = 0;

      message("Construct tree");

      while(faceTree1->size() != 0){
        /*Remove non-mergable nodes*/
        NodePtrTreeIterator it = faceTree1->begin();

        it = faceTree1->begin();
        Node* first = *it++;

        /*Inspect neighboring sub-surfaces*/
        if(first->neighboringFaces.size() == 0){
          error("No neighbours left");
        }

        /*Select the best node to merge with this node*/
        NodePtrTreeIterator sit = first->neighboringFaces.begin();
        T dotp = (T)-1000;
        T smallest = (T)100000;
        //int minNeighbors = 10000;
        T smallestRatio = (T)10000;
        Node* bestCandidate = 0;
        Node* smallestCandidate = 0;
        Node* bestCommonCandidate = 0;
        T bestFactor = (T)-10000;

        Vector4<T> fmn = first->box.getMin();
        Vector4<T> fmx = first->box.getMax();
        fmx -= fmn;
        fmx += eps;

        T fvolume = (T)(fmx[0] * fmx[1] * fmx[2]);

        int bestInCommon = 0;
        int smallestInCommon = 0;
        T bestCommonFactor = -10000;

        while(sit != first->neighboringFaces.end()){
          Node* current = *sit++;
          T current_dot = dot(first->averageNormal,
                              current->averageNormal);
          T current_area = current->area + first->area;
          Vector4<T> mn = current->box.getMin();
          Vector4<T> mx = current->box.getMax();

          mx -= mn;
          mx += eps;

          BBox<T> result = current->box;
          result.addPoint(mn);
          result.addPoint(mx);

          Vector4<T> rmn = result.getMin();
          Vector4<T> rmx = result.getMax();
          rmx -= rmn;
          rmx += eps;


          T rvolume = (T)(rmx[0] * rmx[1] * rmx[2]);
          T  volume = (T)( mx[0] *  mx[1] *  mx[2]);

          T ratio = volume*(rvolume*current->area-fvolume*first->area)/current_dot;

          bool inSet = false;
          /*Neighboring must exist in faceTree1*/
          if(faceTree1->findIndex(current) == Tree<int>::undefinedIndex){
            inSet = false;
          }else{
            inSet = true;
          }

          if((current_dot > dotp) && inSet){
            dotp = current_dot;
            bestCandidate = current;
          }

          if((current_area < smallest) && inSet){
            smallest = current_area;
          }
          if((ratio < smallestRatio) && inSet){
            smallestRatio = ratio;
          }

          if(inSet){
            T currentFactor = Sqrt(first->area+current->area);
            T overlap = first->computeOverlap(current, mesh);
            T resPerimeter =
              (first->perimeter + current->perimeter) - (T)2.0*overlap;
            currentFactor/=resPerimeter;

            /*Check for vectors which are in common*/
            int common = getCommonVisibility(first, current);
            if(common != 0){
              if(common == bestInCommon){
                if(currentFactor > bestCommonFactor){
                  bestCommonFactor = currentFactor;
                  bestCommonCandidate = current;
                }
              }else if(common > bestInCommon){
                bestInCommon = common;
                bestCommonCandidate = current;
              }
            }

            if(currentFactor > bestFactor){
              bestFactor = currentFactor;
              smallestCandidate = current;
              smallestInCommon = common;
            }
          }
        }

        if(smallestCandidate == 0){

        }else{
          bestCandidate = smallestCandidate;
        }

        /*If there is a candidate with common visibility vectors, use
          that candidate*/
        if(bestCommonCandidate){
          if(smallestInCommon <= bestInCommon){
            bestCandidate = smallestCandidate;
          }else{
            bestCandidate = bestCommonCandidate;
          }
        }

        if(bestCandidate == 0){
          if(smallestCandidate){
            bestCandidate = smallestCandidate;
          }else if(bestCommonCandidate){
            bestCandidate = bestCommonCandidate;
          }
        }

        if(bestCandidate == 0){
          /*Move up one level*/
          /*Create dummy surface node*/
          Node* parentNode = new Node();
          parentNode->parent = 0;
          parentNode->childs[0] = first;
          parentNode->childs[1] = first;
          parentNode->area = first->area;
          parentNode->averageNormal = first->averageNormal;
          parentNode->box = first->box;
          parentNode->face = 0;
          parentNode->leaf = false;
          parentNode->level = depth+1;
          parentNode->id = 100;
          parentNode->addEdges(first->interiorEdges, mesh);
          parentNode->computePerimeter(mesh);
          parentNode->computeVisibility(mesh, false);
          parentNode->minFace = first->minFace;
          parentNode->maxFace = first->maxFace;
          parentNode->singleNode = true;

          first->parent = parentNode;
          faceTree1->remove(first);
          faceTree2->insert(parentNode);
        }else{
          Node* parentNode = new Node();
          parentNode->parent = 0;
          parentNode->childs[0] = first;
          parentNode->childs[1] = bestCandidate;
          parentNode->area = first->area + bestCandidate->area;

          parentNode->averageNormal =
            (first->averageNormal * first->area +
             bestCandidate->averageNormal * bestCandidate->area);

          if(parentNode->averageNormal.length() < 1e-6){
            /*If we can not normalize this vector, just put some dummy
              values*/
            parentNode->averageNormal.set(1,0,0,0);
          }else{
            parentNode->averageNormal.normalize();
          }

          parentNode->box = first->box;
          parentNode->box.addPoint(bestCandidate->box.getMin());
          parentNode->box.addPoint(bestCandidate->box.getMax());
          parentNode->face = 0;
          parentNode->leaf = false;
          parentNode->level = depth+1;
          parentNode->singleNode = false;

          parentNode->addEdges(first->interiorEdges, mesh);
          parentNode->addEdges(bestCandidate->interiorEdges, mesh);
          parentNode->computePerimeter(mesh);
          parentNode->computeVisibility(mesh, false);
          parentNode->minFace =
            Min(first->minFace, bestCandidate->minFace);
          parentNode->maxFace =
            Max(first->maxFace, bestCandidate->maxFace);

          first->parent = parentNode;
          bestCandidate->parent = parentNode;

          faceTree1->remove(first);
          faceTree1->remove(bestCandidate);
          faceTree2->insert(parentNode);
        }

        /*All nodes are merged in higher-level nodes. Update
          neighboring and adjacency information*/

        if(faceTree1->size() == 0){
          faceTree1->clear();

          /*FaceTree1 is empty now, compute neighboring sub-surfaces */
          NodePtrTreeIterator it = faceTree2->begin();

          /*Iterate over all parent nodes*/
          while(it!=faceTree2->end()){
            Node* current = *it;

            /*Collect from both childs the parents of the
              neighboring faces*/
            Tree<Node*> parentTree;
            Tree<Node*> parentAdjTree;

            for(int l=0;l<2;l++){
              Node* child = current->childs[l];
              NodePtrTreeIterator nit = child->neighboringFaces.begin();

              while(nit != child->neighboringFaces.end()){
                Node* currNeighbor = *nit++;

                if(currNeighbor->parent == current){
                  /*In order to prevent that in the next iteration
                    this node will be merged with itself.*/
                }else{
                  if(currNeighbor->parent == 0){
                    error("orphan node");
                  }

                  parentTree.uniqueInsert(currNeighbor->parent);
                }
              }

              nit = child->adjacentFaces.begin();
              while(nit != child->adjacentFaces.end()){
                Node* currNeighbor = *nit++;

                if(currNeighbor->parent == current){

                }else{
                  if(currNeighbor->parent == 0){
                    error("orphan node");
                  }

                  parentAdjTree.uniqueInsert(currNeighbor->parent);
                }
              }
            }

            NodePtrTreeIterator pit = parentTree.begin();

            while(pit != parentTree.end()){
              Node* neigh = *pit++;
              current->neighboringFaces.uniqueInsert(neigh, neigh->face);
            }

            pit = parentAdjTree.begin();
            while(pit != parentAdjTree.end()){
              Node* adj = *pit++;
              current->adjacentFaces.uniqueInsert(adj, adj->face);
            }

            if(current->neighboringFaces.size() == 0){
              orphanNodes.append(current);
              faceTree2->remove(it);
            }else{
              it++;
            }
          }

          /*Swap trees*/
          swap(&faceTree1, &faceTree2);

          depth++;
          if(faceTree1->size() == 1){
            root = *(faceTree1->begin());
            break;
          }
        }
      }
      delete faceTree1;
      delete faceTree2;

      if(orphanNodes.size() != 0){
        /*Merge orphan nodes. Each orphan node is one single object,
          not connected to others.*/
        mergeOrphans(&orphanNodes);
      }

      root->updateBBox(mesh);
      root->computeVisibility(mesh);
      computeLevels();
      compileAdjacency();
    }

    /*Store adjacency info in a 2d array*/
    void compileAdjacency(){
      List<Node*> nodes;

      computeNodeIds();

      collectNodesBFS(&nodes);

      max_adj = maxAdjacency();

      if(adjacentNodes){
        delete[] adjacentNodes;
      }
      if(nAdjacent){
        delete[] nAdjacent;
      }

      adjacentNodes = new int[nodes.size() * max_adj];
      nAdjacent     = new int[nodes.size() * max_adj];

      NodePtrListIterator it = nodes.begin();
      while(it != nodes.end()){
        Node* node = *it++;

        int n_nodes = 0;
        NodePtrTreeIterator surface = node->adjacentFaces.begin();

        Tree<int> nodeIds;

        while(surface != node->adjacentFaces.end()){
          Node* neighbor = *surface++;
          nodeIds.insert(neighbor->nodeId);
          n_nodes++;
        }
        nAdjacent[node->nodeId] = n_nodes;

        Tree<int>::Iterator nodeIt = nodeIds.begin();

        int idx = 0;
        while(nodeIt != nodeIds.end()){
          int id = *nodeIt++;
          adjacentNodes[node->nodeId * max_adj + idx] = id;
          idx++;
        }
      }
    }

    DCTetraMesh<T, A, B, C, D>* mesh;
    Node* root;
    Node* leafNodes;
    int* adjacentNodes;
    int* nAdjacent;
    int max_adj;
  };
}


#endif/*SURFACETREE_HPP*/
