/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef LIST_HPP
#define LIST_HPP

#include "core/cgfdefs.hpp"
#include <ostream>
#include "math/Compare.hpp"
namespace CGF{

  /*Double linked list*/
  template<class T>
  class ListNode;

  template<class T>
  class List;

  template<class T>
  class Allocator{
  public:
    static Allocator* instance(){
      if(inst == 0){
        inst = new Allocator;
      }
      return inst;
    }

    T* allocate(){
      if(used == size){
        extendStorage();
      }
      int idx = index[used++];
      return &(data[idx]);
    }
  protected:
    ~Allocator(){
      PRINT_FUNCTION;
      delete [] data;
      delete [] index;
    }

  private:
    void extendStorage(){
    }

    Allocator(){
      PRINT_FUNCTION;
      message("singleton constructor");
      size = 1000;
      used = 0;

      data = new T[size];
      index = new int[size];
      for(int i=0;i<size;i++){
        index[i] = i;
      }
    }

    T* data;
    int* index;
    int size;
    int used;
    static Allocator<T>* inst;
  };

  namespace ListInternals{

    template<typename T>
    struct ListNode{
      typedef T           value_type;
      typedef ListNode<T> self;

      ListNode():next(0),prev(0){
      }

      self* next;
      self* prev;
      value_type  data;
    protected:
      ListNode(const self& l);
      ListNode& operator=(const self& l);
    };


    template<typename T>
    struct ListIterator{
    public:
      typedef T                        value_type;
      typedef ListNode<value_type>*    nodePtr;
      typedef ListIterator<value_type> self;

      nodePtr ptr;

      /*Dereference operator*/
      value_type& operator*(){
        return ptr->data;
      }

      /*Pointer operator*/
      value_type* operator->(){
        return &(ptr->data);
      }

      /*Post increment*/
      self operator++(int i){
        self it;
        it.ptr = ptr;
        ptr = ptr->next;
        return it;
      }

      /*Post decrement*/
      self operator--(int i){
        self it;
        it.ptr = ptr;
        ptr = ptr->prev;
        return it;
      }

      /*Pre increment*/
      self& operator++(){
        ptr = ptr->next;
        return *this;
      }

      /*Pre decrement*/
      self& operator--(){
        ptr = ptr->prev;
        return *this;
      }

      /*Equality*/
      bool operator==(const self& i)const{
        return ptr == i.ptr;
      }

      /*Inequality*/
      bool operator!=(const self& i)const{
        return ptr != i.ptr;
      }

      /*Next item*/
      self next(){
        self i;
        i.ptr = ptr->next;
        return i;
      }

      /*Previous item*/
      self prev(){
        self i;
        i.ptr = ptr->prev;
        return i;
      }
    };
  }

  template<typename T>
  class List{
  public:
    typedef T                              value_type;
    typedef List<T>                        Self;
    typedef ListInternals::ListNode<T>     Node;
    typedef ListInternals::ListIterator<T> Iterator;

    List(){
      first = new Node;
      last  = new Node;
      first->next = last;
      last->prev  = first;
      sz = 0;
    }

    ~List(){
      clear();
      delete first;
      delete last;
    }

    List(const Self& c){
      first = new Node;
      last  = new Node;
      first->next = last;
      last->prev  = first;
      sz = 0;

      Iterator it = c.begin();
      Iterator e  = c.end();
      while(it != e){
        append(*it++);
      }
    }

    Self& operator=(const Self& c){
      if(this == &c){
        message("Self assignment");
      }else{
        clear();
        Iterator it = c.begin();
        Iterator e  = c.end();
        while(it != e){
          append(*it++);
        }
      }
      return *this;
    }

    Iterator begin() const{
      Iterator i;
      i.ptr = first->next;
      return i;
    }

    Iterator end() const{
      Iterator i;
      i.ptr = last;
      return i;
    }

    void clear(){
      while(first->next != last){
        Node* next = first->next->next;
        delete first->next;
        first->next = next;
        first->next->prev = first;
      }
      sz = 0;
    }

    void append(const value_type& t){
      Node* n = new Node;
      Node* current = last->prev;
      current->next = n;
      n->prev = current;
      n->data = t;
      last->prev = n;
      n->next = last;
      sz++;
    }

    void append(Self& l){
      Iterator iter = l.begin();
      while(l.size() != 0){
        append(*iter);
        l.remove2(iter);
      }
    }

    void appendAndKeep(const Self& l){
      Iterator iter = l.begin();
      while(iter != l.end()){
        append(*iter);
        iter++;
      }
    }

    void appendByValue(value_type t){
      Node* n = new Node;
      Node* current = last->prev;
      current->next = n;
      n->prev = current;
      n->data = t;
      last->prev = n;
      n->next = last;
      sz++;
    }

    /*Removes the current node and advances to the next node*/
    /*If used in an iteration, do not increase the iterator after removal*/
    void remove2(Iterator& it){
      Node* curr = it.ptr;
      Node* prev = it.ptr->prev;
      Node* next = it.ptr->next;

      next->prev = prev;
      prev->next = next;

      delete curr;
      sz--;
      it.ptr = next;
    }

    void remove(value_type val){
      Iterator it = begin();
      while(it != end()){
        if(*it == val){
          remove2(it);
          return;
        }
        it++;
      }
    }

    /*Removes the current node and advances to the previous node*/
    /*If used in an iteration, always increase the iterator after removal*/
    void remove(Iterator& it){
      Node* curr = it.ptr;
      Node* prev = it.ptr->prev;
      Node* next = it.ptr->next;

      error("deprecated, will be removed");

      next->prev = prev;
      prev->next = next;

      delete curr;
      sz--;
      it.ptr = prev;
    }

    void removeAt(int i){
      Node* current = first->next;
      int index = 0;

      if(i>=sz)
        return;

      while(index != i){
        current = current->next;
        index++;
      }
      Node* prev = current->prev;
      Node* next = current->next;

      prev->next = next;
      next->prev = prev;

      delete current;
      sz--;
    }

    int size()const{
      return sz;
    }

    value_type& operator[](int idx) const{
      cgfassert(idx >=0 && idx < sz);

      Node* current = first->next;
      int index = 0;
      while(index != idx){
        current = current->next;
        index++;
      }
      return current->data;
    }

    void removeDuplicates(){
      unique();
    }

    void unique(){
      if(sz == 0 || sz == 1){
        return;
      }

      sort();

      /*List has at least 2 elements*/

      Node* curr = first->next;
      Node* next = curr->next;

      while(curr != last && next != last){
        if(curr->data == next->data){
          /*duplicate found, remove current*/
          Node* prev = curr->prev;
          prev->next = next;
          next->prev = prev;
          delete curr;
          curr = next;
          next = curr->next;
          sz--;
        }else{
          curr = next;
          next = next->next;
        }
      }
    }

    void sort(){
      /*Mergesort*/
      if(sz == 0 || sz == 1){
        return;
      }

      int insize = 1;
      int nmerges, psize, qsize, i;

      Node* list = first->next;

      list->prev = 0;
      first->next = 0;
      cgfassert(list!=0);

      /*Remove link to end*/
      last->prev->next = 0;
      last->prev = 0;

      Node* p;
      Node* q;

      Node* e;
      Node* tail;

      while(1){
        p = list;
        list = 0;
        tail = 0;

        nmerges = 0;

        while(p){
          nmerges++;
          q = p;
          psize = 0;
          for(i=0;i<insize;i++){
            psize++;
            q = q->next;
            if(!q)
              break;
          }

          qsize = insize;

          while(psize > 0 || (qsize > 0 && q)){
            if(psize == 0){
              e = q; q = q->next; qsize--;
            }else if(qsize == 0 || !q){
              e = p; p = p->next; psize--;
              //}else if(p->data < q->data){
            }else if(Compare<T>::less(p->data, q->data)){
              e = p; p = p->next; psize--;
            }else{
              e = q; q = q->next; qsize--;
            }

            if(tail){
              tail->next = e;
            }else{
              list = e;
            }
            if(true){
              e->prev = tail;
            }
            tail = e;
          }
          p = q;
        }
        tail->next = 0;

        if(nmerges <= 1){
          first->next = list;
          list->prev = first;

          while(list->next){
            list = list->next;
          }
          list->next = last;
          last->prev = list;
          return;
        }
        insize *= 2;
      }
    }
    template<class U> friend inline std::ostream& operator<<(std::ostream& os,
                                                             const List<U>& l);

  protected:
    Node* first;
    Node* last;
    int sz;
  };

  template<typename U>
  inline std::ostream& operator<<(std::ostream& os, const List<U>& l){
    typename List<U>::Iterator it = l.begin();
    while(it != l.end()){
      os << *it++ << ',';
    }
    return os;
  }
}

#endif/*LIST_HPP*/
