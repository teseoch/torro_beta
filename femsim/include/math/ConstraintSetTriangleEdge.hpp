/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTSETTRIANGLEEDGE_HPP
#define CONSTRAINTSETTRIANGLEEDGE_HPP

#include "datastructures/DCEList.hpp"
#include "geo/Edge.hpp"
#include "datastructures/Tree.hpp"
#include "math/EvalType.hpp"

namespace CGF{

  template<class T>
  class CollisionContext;

  template<class T>
  class ConstraintCandidateTriangleEdge;

  template<class T>
  class ConstraintSetTriangleEdge{
  public:
    typedef ConstraintCandidateTriangleEdge<T> Candidate;
    typedef typename Tree<Candidate*>::Iterator CandidateTreeIterator;

    ConstraintSetTriangleEdge();

    virtual ~ConstraintSetTriangleEdge();

    void deactivateAll(CollisionContext<T>* n);

    bool evaluate(Edge<T>& p, Vector4<T>& com, Quaternion<T>& rot,
                  CollisionContext<T>* n, const EvalType& etype,
                  EvalStats& stats);

    bool checkAndUpdate(Edge<T>& p,
                        Vector4<T>& com, Quaternion<T>& rot,
                        CollisionContext<T>* n, EvalStats& stats,
                        bool friction,
                        bool force, T bnorm);

    bool updateInitial(Edge<T>& p,
                       Vector4<T>& com, Quaternion<T>& rot,
                       CollisionContext<T>* n, EvalStats& stats,
                       bool friction,
                       bool force, T bnorm);

    void incrementatlUpdate(CollisionContext<T>* n);
    void update(CollisionContext<T>* n);

    void showVertexCandidateState(int vertex, CollisionContext<T>* ctx);

    Edge<T> startPosition;
    Vector4<T> startCenterOfMass;
    Quaternion<T> startOrientation;
    Edge<T> lastPosition;
    Vector4<T> lastCenterOfMass;
    Quaternion<T> lastOrientation;

    Tree<Candidate*> candidates;
    Tree<int> candidatesTree;

    int edgeId;

    Mesh::GeometryType type;
  };

}

#endif/*CONSTRAINTSETTRIANGLEEDGE_HPP*/
