/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTCANDIDATEEDGE_HPP
#define CONSTRAINTCANDIDATEEDGE_HPP

#include "math/constraints/Constraint.hpp"
#include "datastructures/DCEList.hpp"
#include "math/ConstraintCandidateStatus.hpp"
#include "math/Compare.hpp"
#include "geo/Edge.hpp"
#include "math/ConstraintTol.hpp"
#include "math/EvalType.hpp"

#define DISTANCE_EPS3 SAFETY_DISTANCE

namespace CGF{

  template<class T>
  class CollisionContext;

  template<class T>
  class ConstraintSetEdge;

  template<class T>
  class ConstraintCandidateEdge{
  public:

    ConstraintCandidateEdge(int eid, int oeid, bool backFace);

    void recompute(ConstraintSetEdge<T>* c, CollisionContext<T>* n,
                   Edge<T>& p, Vector4<T>& com, Quaternion<T>& rot);

    /*p = current, x = start*/
    bool checkCollision(const Edge<T>& p, const Vector4<T>& com,
                        const Quaternion<T>& rot,
                        T* dist);


    /*Evaluate signed distance*/
    bool evaluate(ConstraintSetEdge<T>* c,
                  CollisionContext<T>* n,
                  const Edge<T>& edge,
                  const Vector4<T>& com,
                  const Quaternion<T>& rot,
                  Vector4<T>* bb1 = 0, Vector4<T>* bb2 = 0, bool* updated = 0);

    /*Update position of edge*/
    void updateGeometry(CollisionContext<T>* n);

    bool enableConstraint(ConstraintSetEdge<T>* c, CollisionContext<T>* n,
                          Edge<T>& s, Edge<T>& p);

    void updateConstraint(ConstraintSetEdge<T>* c, CollisionContext<T>* n,
                          const Edge<T>& x, const Edge<T>& p,
                          const Vector4<T>& com, const Quaternion<T>& rot,
                          bool* skipped, EvalStats& stats);

    void updateInitial(ConstraintSetEdge<T>* c, CollisionContext<T>* n,
                       const Edge<T>& x, const Edge<T>& p,
                       const Vector4<T>& com, const Quaternion<T>& rot,
                       bool* skipped, EvalStats& stats);

    void updateInactiveConstraint(ConstraintSetEdge<T>* c,
                                  CollisionContext<T>* n,
                                  const Edge<T>& x,
                                  const Edge<T>& p,
                                  const Vector4<T>& com,
                                  const Quaternion<T>& rot);


    bool disableConstraint(ConstraintSetEdge<T>* c, CollisionContext<T>* n,
                           bool force = false);

    void showCandidateState(Edge<T>& pu, CollisionContext<T>* ctx);

    void forceConstraint();

    void updateInitialState(const Edge<T>& edge,
                            const Vector4<T>& com,
                            const Quaternion<T>& rot);

    int edgeId;
    int otherEdgeId;

    bool forced;

    Edge<T> p0; /*Initial edge p*/
    Edge<T> pi; /**/
    Edge<T> e0; /*Initial edge e*/
    Edge<T> eu; /*Edge at current iteration*/
    Edge<T> ei; /*Linear interpolated edge between e and eu with
                  distance 0*/

    Edge<T> enabledEU;
    Edge<T> enabledPU;
    Edge<T> enabledE0;
    Edge<T> enabledP0;

    Vector4<T>    comp0;
    Quaternion<T> rotp0;
    Vector4<T>    come0;
    Quaternion<T> rote0;
    Vector4<T>    comeu;
    Quaternion<T> roteu;

    Vector4<T>    compi;
    Quaternion<T> rotpi;
    Vector4<T>    comei;
    Quaternion<T> rotei;

    T distance0;   /*Distance at begin of timestep*/
    T distancec;   /*Distance at current iteration*/

    bool intersection0; /*Intersection state at begin of timestep*/
    bool intersectionc; /*Intersection state at current iteration*/

    Vector4<T> colPoint; /*Projected collision point on this edge*/

    //int indices[2];

    Vector4<T> baryi1;   /*Barycentric coordinates of colPoint*/
    Vector4<T> baryi2;   /*Barycentric coordinates of colPoint on subject edge*/

    CandidateStatus status; /*Enabled or disabled*/
    int enabledId; /*Id of constraint in solver*/

    Vector4<T> dv;
    Vector4<T> referencedvPE; /*Reference distance vector used for
                              correcting signs p-e*/
    Vector4<T> referencedvEP;

    Vector4<T> referencedvEP0; /*Reference at enabling*/

    bool anyReferenceSet; /*True if referencedv's are set in previous steps.*/

    bool tangentialDefined;
    Vector4<T> tangentReference[2];

    bool fixed;

    bool backFace;

    Vector4<T> contactNormal;/*Used when updating the constraint, E->P*/

    Mesh::GeometryType thisEdgeType;
    Mesh::GeometryType ownerEdgeType;

    bool collapsed;

    bool flag;
#ifdef ACTIVE_CONSTRAINTS
    Vector4<T> cachedMultipliers;
#endif
  };

  /*Required to make the simulation deterministic. It compares two
    candidates given its id instead of comparing their pointer values
    (default)*/
  typedef ConstraintCandidateEdge<float>* CCEPf;
  typedef ConstraintCandidateEdge<double>* CCEPd;

  template<>
  class Compare<CCEPf>{
  public:
    static bool less(const CCEPf& a,
                     const CCEPf& b){
      //return a < b;
      cgfassert(a!=0);
      cgfassert(b!=0);
      return a->edgeId < b->edgeId;
    }

    static bool equal(const CCEPf& a,
                      const CCEPf& b){
      cgfassert(a!=0);
      cgfassert(b!=0);
      //return a == b;
      return a->edgeId == b->edgeId;
    }
  };

  template<>
  class Compare<CCEPd>{
  public:
    static bool less(const CCEPd& a,
                     const CCEPd& b){
      cgfassert(a!=0);
      cgfassert(b!=0);

      return a->edgeId < b->edgeId;
    }

    static bool equal(const CCEPd& a,
                      const CCEPd& b){
      cgfassert(a!=0);
      cgfassert(b!=0);

      return a->edgeId == b->edgeId;
    }
  };
}

#endif/*CONSTRAINTCANDIDATEEDGE_HPP*/
