/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTCANDIDATETRIANGLEEDGE_HPP
#define CONSTRAINTCANDIDATETRIANGLEEDGE_HPP

#include "math/constraints/Constraint.hpp"
#include "datastructures/DCEList.hpp"
#include "math/ConstraintCandidateStatus.hpp"
#include "math/Compare.hpp"
#include "geo/Edge.hpp"
#include "math/ConstraintTol.hpp"
#include "math/EvalType.hpp"

#define DISTANCE_EPS3 SAFETY_DISTANCE

namespace CGF{
  template<class T>
  class CollisionContext;

  template<class T>
  class ConstraintSetTriangleEdge;

  template<class T>
  class ConstraintCandidateTriangleEdge{
  public:
    ConstraintCandidateTriangleEdge(int edgeId, int vertexId);

    void recompute(ConstraintSetTriangleEdge<T>* c,
                   CollisionContext<T>* n,
                   Edge<T>& p, Vector4<T>& com, Quaternion<T>& rot);

    bool checkCollision(const Edge<T>& p, const Vector4<T>& com,
                        const Quaternion<T>& rot, T* dist = 0);

    bool evaluate(const Edge<T>& p,
                  const Vector4<T>& com,
                  const Quaternion<T>& rot,
                  Vector4<T>* bb = 0, bool* updated = 0);

    void updateGeometry(CollisionContext<T>* n, ConstraintSetTriangleEdge<T>* c,
                        const Edge<T>& p, const Edge<T>& s);

    void updateConstraint(ConstraintSetTriangleEdge<T>* c,
                          CollisionContext<T>* n,
                          const Edge<T>& x, const Edge<T>& p,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          bool* skipped,
                          EvalStats& stats);

    void updateInitial(ConstraintSetTriangleEdge<T>* c,
                       CollisionContext<T>* n,
                       const Edge<T>& x, const Edge<T>& p,
                       const Vector4<T>& com,
                       const Quaternion<T>& rot,
                       bool* skipped,
                       EvalStats& stats);

    bool enableConstraint(ConstraintSetTriangleEdge<T>* c,
                          CollisionContext<T>* n,
                          Edge<T>& s, Edge<T>& p);

    bool disableConstraint(ConstraintSetTriangleEdge<T>* c,
                           CollisionContext<T>* n,
                           bool force = false);



    int edgeId;
    int vertexId;

    Edge<T>    p0;
    Edge<T>    pi;
    Vector4<T> v0;
    Vector4<T> vu;
    Vector4<T> vi;

    Vector4<T>    comp0;
    Quaternion<T> rotp0;
    Vector4<T>    comv0;
    Quaternion<T> rotv0;
    Vector4<T>    comvu;
    Quaternion<T> rotvu;

    Vector4<T>    compi;
    Quaternion<T> rotpi;
    Vector4<T>    comvi;
    Quaternion<T> rotvi;

    T distance0;
    T distancec;

    Vector4<T> colPoint;
    Vector4<T> baryi;

    CandidateStatus status;
    int enabledId;

    Vector4<T> contactNormal;

    bool tangentialDefined;
    Vector4<T> tangentReference[2];

    Mesh::GeometryType edgeType;
    Mesh::GeometryType vertexType;

#ifdef ACTIVE_CONSTRAINTS
    Vector4<T> cachedMultipliers;
#endif

  };
}

#endif/*CONSTRAINTCANDIDATETRIANGLEEDGE_HPP*/
