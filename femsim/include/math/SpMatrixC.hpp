/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef SPMATRIXC_HPP
#define SPMATRIXC_HPP

#include "math/constraints/AbstractMatrixConstraint.hpp"
#include "math/constraints/Constraint.hpp"
#include "math/SpMatrix.hpp"
#include "math/Vector.hpp"
#include "datastructures/Tree.hpp"
#include "datastructures/DynamicArray.hpp"
#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "math/VectorC.hpp"
#include "math/Compare.hpp"

#include "core/BenchmarkTimer.hpp"
#include "core/Timer.hpp"
#include "math/matrix_loaders.hpp"

#define FRICTION
//#undef  FRICTION

/*An extension of the sparse matrix in which constraints can be
  updated dynamically*/

namespace CGF{

  template<int N, class T=float>
  class CGFAPI SpMatrixC2 : public SpMatrix<N,T>{
  public:
    template<int M, class TT>
      friend class IECLinSolveT2;

    template<int M, class TT>
      friend class IECLinSolveT2CR;

    template<int M, class TT>
      friend class IECLinSolveT2GS;

    template<class TT>
      friend class VectorC2;

    template<class TT>
      friend class ConstraintCandidate;
    template<class TT>
      friend class ConstraintCandidateEdge;
    template<class TT>
      friend class ConstraintSet;
    template<class TT>
      friend class ConstraintSetEdge;

    /*Constructors*/
    SpMatrixC2():SpMatrix<N, T>(), status(Individual), constraints(10){
      activeConstraints = 0;
    }

    SpMatrixC2(int width, int height):SpMatrix<N, T>(width, height),
      status(Individual), constraints(10){
      activeConstraints = 0;
    }

    SpMatrixC2(const SpMatrix<N, T>& m):SpMatrix<N,T>(m),
      status(Individual), constraints(10){
      activeConstraints = 0;
    }

    /*Copy constructor*/
    SpMatrixC2(const SpMatrixC2<N, T>& m):SpMatrix<N,T>(m),
      status(Individual), constraints(m.constraints){
      if(&m != this){
        activeConstraints = m.activeConstraints;
      }
    }

    /*Destructor*/
    virtual ~SpMatrixC2(){
      /*Delete constraints*/
      for(int i=0; i<constraints.getNActive();i++){
        int idx = constraints.activeIndex(i);
        delete constraints[idx];
        constraints[idx] = 0;
      }
    }

    /*Assignment operators*/
    SpMatrixC2<N, T> operator=(const SpMatrixC2<N, T>& m){
      if(this == &m){
        /*Self assignment*/
      }else{
        SpMatrix<N, T>::operator=(m);
        constraints = m.constraints;
      }
      return *this;
    }

    SpMatrixC2<N, T> operator=(const SpMatrix<N, T>& m){
      if(this == &m){
        /*Self assignment*/
        return *this;
      }else{
        SpMatrix<N, T>::operator=(m);
        constraints.reset();
      }
      return *this;
    }

    int addConstraint(AbstractMatrixConstraint<N, T>* c/*, int column*/){
      //c->index = column;
      PRINT_FUNCTION;

      START_DEBUG;
#if 0
      message("dump constraints");

      for(int i=0;i<constraints.getNActive();i++){
        int index = constraints.activeIndex(i);

        message("[%d] = %p", i, constraints[index]);
      }
#endif
      constraints.print();

      message("this = %p, constraint = %p, array = %p", this, c, &constraints);
      message("pre activate size = %d", constraints.getNActive());
      END_DEBUG;

      c->row_id = constraints.getFirstFreeItem(0);

      c->status = Active;
      constraints[c->row_id] = c;
      constraints.activate(c->row_id);

      c->row_id_given = constraints.reverseIndex(c->row_id);

      activateConstraint(c->row_id);

      START_DEBUG;
      constraints.print();

      message("post activate size = %d", constraints.getNActive());

      message("row_id = %d", c->row_id);
      END_DEBUG;

      c->init(this);

      //int r = constraints.activeIndex(c->row_id);

#if 0
      message("%d constraints", constraints.getNActive());
      message("r = %d", c->row_id);


      getchar();
#endif
      START_DEBUG;
      c->print(std::cout);
      message("end matrix add constraint, row_id = %d -- %d", c->row_id,
              constraints.reverseIndex(c->row_id));
      END_DEBUG;

      return c->row_id;
    }

    void updateConstraint(int id, T vc, int column){
      //int idx = constraints.activeIndex(id);
      error("used?");
      //constraints[id].c = vc;
      //constraints[id].index = column;
    }

    bool isActiveConstraint(int i){
      //int idx = constraints.activeIndex(i);
      if(constraints[i]->status == Active){
        return true;
      }
      return false;
    }

    T getConstraintValue(int i){
      error("used??");
      //int index = constraints.activeIndex(i);
      //return constraints[index]->c;
      return 0.0;
    }

    int getConstraintIndex(int i, int j){
      error("used???");
      return -1;
      //int index = constraints.activeIndex(i);
      //return constraints[index]->index[j];
    }

    void removeConstraint(int id){
      DBG(message("removing constraint %d", id));
      DBG(constraints.print());
      deactivateConstraint(id);
      //id = constraints.reverseIndex(id);
      constraints[id] = 0;
      constraints.deactivate(id);
      DBG(constraints.print());
    }

    void activateConstraint(int id){
      //int index = constraints.reverseIndex(id);
      constraints[id]->status = Active;
      activeConstraints->uniqueInsert(id, id);
    }

    void deactivateConstraint(int id){
      //int index = constraints.reverseIndex(id);
      //constraints[index]->status = Inactive;
      constraints[id]->status = Inactive;
      activeConstraints->remove(id);
    }

    int getNConstraints() const{
      return constraints.getNActive();
    }

    void deactivateAllConstraints(){
      status = NoneActive;
    }

    void activateAllConstraints(){
      status = AllActive;
    }

    void individualActivation(){
      status = Individual;
    }

    void reset(){
      //SpMatrix<N, T>::clear();
      constraints.reset();
    }

    void listConstraints(){
#if 0
      PRINT_FUNCTION;
      for(int i=0;i<constraints.getNActive();i++){
        int idx = constraints.activeIndex(i);
        bool active = false;
        if(constraints[idx]->status == Active){
          active = true;
        }
        message("Constraint %d, active = %d", i, active);
        std::cout << constraints[idx];
      }
#endif
    }

    void extractConstraintMatrices(SpMatrix<N, T>& L, SpMatrix<N, T>& LT,
                                   Tree<int>& columns, Vector<T>* rhs = 0){

      int nConstraints = getNConstraints();

      for(int i=0;i<nConstraints;i++){
        AbstractMatrixConstraint<N, T>* c = getActiveConstraint(i);
        c->extractValues(&L, &LT, &columns, i, rhs);
      }
    }

#if 0
    void computeDelassusOperator();
    void computeDelassusOperatorLLT();
    void solveDelassusOperator(Vector<T>* b);
#endif
    /*Overloaded functions*/
    ConstraintMatrixStatus getStatus()const{
      return status;
    }

    template<int M, class TT>
      friend void spmvc(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
                        const VectorC2<TT>& v, const MatrixMulMode mode,
                        const VectorC2<TT>* mask);

    template<int M, class TT>
      friend void spmvc_t(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
                          const VectorC2<TT>& v, const MatrixMulMode mode,
                          const VectorC2<TT>* mask);

    template<int M, class TT>
      friend void spmv_parallelc(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
                                 const VectorC2<TT>& v,
                                 const MatrixMulMode mode,
                                 const VectorC2<TT>* mask);

    template<int M, class TT>
      friend void spmv_partialc(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
                                const VectorC2<TT>& v, const MatrixRange rg,
                                const MatrixMulMode mode,
                                const VectorC2<TT>* mask);

    /*Conversion routines*/
    template<int M, int MM, class TPA, class TPB>
      friend void convertMatrix(SpMatrixC2<M, TPA> & a,
                                const SpMatrix<MM, TPB>& b);

    template<int M, int MM, class TPA, class TPB>
      friend void convertMatrix(SpMatrix<M, TPA> & a,
                                const SpMatrixC2<MM, TPB>& b);

    template<int M, class TT>
      friend bool save_matrix_matlab(const char* filename,
                                     const SpMatrixC2<M, TT>* const mat);

    template<int M, class TT>
      friend void save_matrix_market_exchange(const char* filename,
                                              const SpMatrixC2<M, TT>* const mat);

    //protected:
    AbstractMatrixConstraint<N, T>* getConstraint(int index){
      //int index = constraints.activeIndex(i);
      return constraints[index];
    }

    const AbstractMatrixConstraint<N, T>* getConstraint(int index)const{
      //int index = constraints.activeIndex(i);
      return constraints[index];
    }

    AbstractMatrixConstraint<N, T>* getActiveConstraint(int index){
      index = constraints.activeIndex(index);
      return constraints[index];
    }

    const AbstractMatrixConstraint<N, T>* getActiveConstraint(int index)const{
      index = constraints.activeIndex(index);
      return constraints[index];
    }
    ConstraintMatrixStatus status;
    DynamicArray<AbstractMatrixConstraint<N, T>* > constraints;
    Tree<int>* activeConstraints;

    /*Used to store intermedite values from the constraints*/
  };

  template<int M, class TT>
  void spmvc(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
             const VectorC2<TT>& v, const MatrixMulMode mode = Full,
             const VectorC2<TT>* mask = 0);

  template<int M, class TT>
  void spmvc_t(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
               const VectorC2<TT>& v, const MatrixMulMode mode = Full,
               const VectorC2<TT>* mask = 0);

  template<int M, class TT>
  void spmv_parallelc(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
                      const VectorC2<TT>& v,
                      const MatrixMulMode mode = Full,
                      const VectorC2<TT>* mask = 0);

  template<int M, class TT>
  void spmv_partialc(VectorC2<TT>& r, const SpMatrixC2<M, TT>& m,
                     const VectorC2<TT>& v, const MatrixRange rg,
                     const MatrixMulMode mode = Full,
                     const VectorC2<TT>* mask = 0);


  extern BenchmarkTimer* spmv_timer;

  template<int N, class T>
  void conventionalSpmv(VectorC2<T>& r, const SpMatrixC2<N, T>& m,
                        const VectorC2<T>& v){
    if(m.n_blocks != 0){
      spmv_timer->start("spmv_conventional");

      spmv(r, m, v);

      spmv_timer->stop("spmv_conventional");
    }
  }

  template<int N, class T>
  void constraintSpmv(VectorC2<T>& r, const SpMatrixC2<N, T>& m,
                      const VectorC2<T>& v, const MatrixMulMode mode,
                      const VectorC2<T>* mask=0){
    int nConstraints = m.getNConstraints();
    ConstraintMatrixStatus status = m.getStatus();

    if(nConstraints == 0){
      return;
    }

    /*Call pre-multiply*/
    const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(0);

    spmv_timer->start("spmv_constr_pre");
    c->preMultiply(r, v, &m, false);
    spmv_timer->stop("spmv_constr_pre");

    spmv_timer->start("spmv_constr");

    for(int i=0; i<nConstraints; i++){
      const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(i);

      bool process = false;

      if(status == AllActive ||
         mode == FullActive ||
         mode == ConstraintsFullActive){
        process = true;
      }else if(status == Individual){
        if(c->status == Active){
          process = true;
        }else{
          process = false;
        }
      }

      if(process){
        if(c->getType() == rowConstraint){
          if(mode == ConstraintsCol        ||
             mode == ConstraintsFull       ||
             mode == ConstraintsFullActive ||
             mode == Full                  ||
             mode == FullActive){
            /*Multiply as column constraint*/
            c->multiplyTransposed(r, v);
          }

          if(mode == ConstraintsRow        ||
             mode == ConstraintsFull       ||
             mode == ConstraintsFullActive ||
             mode == Full ||
             mode == FullActive){
            /*Multiply as row constraint*/
            c->multiply(r, v);
          }
        }else if(c->getType() == blockConstraint){
          if(mode == Full || mode == FullActive){
            c->multiply(r, v);
          }
        }
      }
    }
    spmv_timer->stop("spmv_constr");
  }

  template<int N, class T>
  void spmvc(VectorC2<T>& r, const SpMatrixC2<N, T>& m, const VectorC2<T>& v,
             const MatrixMulMode mode,
             const VectorC2<T>* mask){
    cgfassert(m.finalized == true);

    r.clear();

    if(mode == Conventional || mode == Full || mode == FullActive){
      conventionalSpmv(r, m, v);
    }

    if(mode != Conventional || mode == ConstraintsFullActive ||
       mode == ConstraintsRow || mode == ConstraintsCol ||
       mode == ConstraintsFull){
      constraintSpmv(r, m, v, mode);
    }
  }

 template<int N, class T>
  void conventionalSpmvT(VectorC2<T>& r, const SpMatrixC2<N, T>& m,
                         const VectorC2<T>& v){
    if(m.n_blocks != 0){
      spmv_timer->start("spmv_conventional");

      spmv_t(r, m, v);

      spmv_timer->stop("spmv_conventional");
    }
  }

  template<int N, class T>
  void constraintSpmvT(VectorC2<T>& r, const SpMatrixC2<N, T>& m,
                       const VectorC2<T>& v, const MatrixMulMode mode){
    int nConstraints = m.getNConstraints();
    ConstraintMatrixStatus status = m.getStatus();

    if(nConstraints == 0){
      return;
    }

    /*Call pre-multiply*/
    const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(0);

    spmv_timer->start("spmv_constr_pre");
    c->preMultiply(r, v, &m, true); /**/
    spmv_timer->stop("spmv_constr_pre");

    spmv_timer->start("spmv_constr");

    for(int i=0;i<nConstraints; i++){
      const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(i);
      bool process = false;

      if(status == AllActive ||
         mode == FullActive ||
         mode == ConstraintsFullActive){
        process = true;
      }else if(status == Individual){
        if(c->status == Active){
          process = true;
        }else{
          process = false;
        }
      }
      if(process){
        if(c->getType() == rowConstraint){
          if(mode == ConstraintsCol  ||
             mode == ConstraintsFull ||
             mode == ConstraintsFullActive ||
             mode == Full ||
             mode == FullActive){
            /*Multiply as column constraint*/
            c->multiply(r, v);
          }

          if(mode == ConstraintsRow  ||
             mode == ConstraintsFull ||
             mode == ConstraintsFullActive ||
             mode == Full ||
             mode == FullActive){
            /*Multiply as row constraint*/
            c->multiplyTransposed(r, v);
          }
        }else if(c->getType() == blockConstraint){
          if(mode == Full ||
             mode == FullActive){
            c->multiplyTransposed(r, v);
          }
        }
      }
    }
    spmv_timer->stop("spmv_constr");
  }

  template<int N, class T>
  void spmvc_t(VectorC2<T>& r, const SpMatrixC2<N, T>& m, const VectorC2<T>& v,
               const MatrixMulMode mode,
               const VectorC2<T>* mask){
    cgfassert(m.finalized == true);

    r.clear();

    if(mode == Conventional || mode == Full || mode == FullActive){
      conventionalSpmvT(r, m, v);
    }


    if(mode != Conventional || mode == ConstraintsFullActive ||
       mode == ConstraintsRow || mode == ConstraintsCol ||
       mode == ConstraintsFull){
      constraintSpmvT(r, m, v, mode);
    }
  }
  //}



  //namespace CGF{

#if 0
  template<int N, class T>
  void SpMatrixC2<N, T>::solveDelassusOperator(Vector<T>* b){
    if(getNConstraints() == 0){
      return;
    }
    spmv_timer->start("delassus");

    DelassusOperator<N, T> op(this);
    int delassusDimension = op.getDimension();

    LinSolveCG<N, T> solver(delassusDimension);
    LinSolveCG<N, T> rhsSolver(this->getHeight());

    *rhsSolver.getb() = *b;
    rhsSolver.setMatrix((SpMatrix<N, T>*) this);
    rhsSolver.solve(100000, (T)1e-16);

    Vector<T>* solverB = solver.getb();

    spmv(*solverB, *op.getL(), *rhsSolver.getx());
    Vector<T>::sub(*solverB, *solverB, *op.getRHS());


    solver.setLinOperator(&op);


    //Vector<T>* solverX = solver.getx();

    /*    for(int i=0;i<delassusDimension;i++){
      if(i%3==0){
        (*solverB)[i] = (T)0.01;
      }
      }*/
    //(*solverB)[0] = (T)1.0;
    //*solverB = - *op.getRHS();

    solver.solve(100000, (T)1e-16);

    spmv_timer->stop("delassus");
  }

  template<int N, class T>
  void SpMatrixC2<N, T>::computeDelassusOperator(){
    if(getNConstraints() == 0){
      return;
    }
    spmv_timer->start("delassus");
    Tree<int> columns;
    message("%d constraints, %d rows", getNConstraints(), getNConstraints()*3);
    message("Matrix size = %d", this->getHeight());
    SpMatrix<N, T> LT(getNConstraints()*3, this->getHeight());
    SpMatrix<N, T> L (this->getHeight(), getNConstraints()*3);
    SpMatrix<N, T> W(getNConstraints()*3, getNConstraints()*3);

    SpMatrix<N, T> M(this->getHeight(), this->getHeight());

    extractConstraintMatrices(L, LT, columns);

    message("L  blocks = %d",  L.getNBlocks());
    message("LT blocks = %d", LT.getNBlocks());

    L.finalize();
    LT.finalize();
    this->finalize();

    int nColumns = columns.size();
    message("%d columns", nColumns);

    /*Compute inverted columns of M which correspond with the columns
      in the constraints.*/

    LinSolveCG<N, T> solver(this->getHeight());

    this->finalize();

    solver.setMatrix(this);
    solver.setSparse(true);

    Vector<T>* solverB = solver.getb();
    Vector<T>* solverX = solver.getx();

#if 0
    TreeIterator<int> it = columns.begin();
    int i = 0;
    while(it != columns.end()){
      int column = *it++;
      message("solve for column %d/%d/%d", column, nColumns, i++);
      solverB->clear();
      solverX->clear();

      (*solverB)[column] = (T)1.0;

      solver.solve();

      for(int j=0;j<this->getHeight();j++){
        T value = (*solverX)[j];
        if(Abs(value) > 1e-18){
          M[column][j] = value;
        }
      }
    }

    M.finalize();

    Timer multiplyTimer;
    multiplyTimer.start();
    W = L * (M * LT);
    multiplyTimer.stop();

    std::cout << multiplyTimer << std::endl;

    save_matrix_matlab("W2.mtx", &W);
    save_matrix_market_exchange("W2.fld", &W);

#else
    Vector<T> lcpB(getNConstraints()*3);
    lcpB.setSparse(true);

    message("constraints = %d", getNConstraints() * 3);

    for(int i=0;i<getNConstraints()*3;i++){
      message("column %d", i);
      solverX->clear();
      lcpB.clear();
      solver.clear();

      lcpB[i] = (T)1.0;

      *solverB = LT * lcpB;

      // std::cout << *solverB << std::endl;
      //message("solverB");
      //getchar();
      solver.solve(10000, (T)5e-5);

      //std::cout << *solverX << std::endl;
      //message("solverX");
      //getchar();

      lcpB = L * *solverX;

      //std::cout << lcpB << std::endl;
      //message("lcpB");
      //getchar();

      for(int j=0;j<getNConstraints()*3;j++){
        T val = lcpB[j];
        if(Abs(val) > 1e-16){
          W[i][j] = val;
          W[j][i] = val; /*Matrix should be symmetric*/
        }
      }
    }

    spmv_timer->stop("delassus");
    //save_matrix_matlab("W.mtx", &W);
    //save_matrix_market_exchange("W.fld", &W);
#endif
  }





  template<int N, class T>
  void SpMatrixC2<N, T>::computeDelassusOperatorLLT(){
    if(getNConstraints() == 0){
      return;
    }
    spmv_timer->start("delassus");
    Tree<int> columns;
    message("%d constraints, %d rows", getNConstraints(), getNConstraints()*3);
    message("Matrix size = %d", this->getHeight());

    SpMatrix<N, T> LT(getNConstraints()*3, this->getHeight());
    SpMatrix<N, T> L (this->getHeight(), getNConstraints()*3);
    SpMatrix<N, T> W(getNConstraints()*3, getNConstraints()*3);

    //SpMatrix<N, T> M(this->getHeight(), this->getHeight());

    Cholesky<N, T> chol;
    chol.compute(*this);

    //SpMatrix<N, T> DD = chol.getD();
    //SpMatrix<N, T>* MM = (SpMatrix<N, T>*)this;


    message("save matrices");
    //save_matrix_matlab("cholInput.mat", MM);
    //save_matrix_matlab("cholL.mat", &(chol.getL()));
    //save_matrix_matlab("cholD.mat", &DD);

    extractConstraintMatrices(L, LT, columns);

    Timer cholTimer;
    cholTimer.start();
    //L.finalize();
    //LT.finalize();
    //this->finalize();

    //save_matrix_matlab("constraint_matrix.mat", &L);

    int nColumns = columns.size();
    message("%d columns", nColumns);

    /*Compute inverted columns of M which correspond with the columns
      in the constraints.*/

    //LinSolveCG<N, T> solver(this->getHeight());

    //this->finalize();

    //solver.setMatrix(this);
    //solver.setSparse(true);

    Vector<T> solverB(this->getHeight());
    Vector<T> solverX(this->getHeight());

#if 0
    TreeIterator<int> it = columns.begin();
    int i = 0;
    while(it != columns.end()){
      int column = *it++;
      message("solve for column %d/%d/%d", column, nColumns, i++);
      solverB->clear();
      solverX->clear();

      (*solverB)[column] = (T)1.0;

      solver.solve();

      for(int j=0;j<this->getHeight();j++){
        T value = (*solverX)[j];
        if(Abs(value) > 1e-18){
          M[column][j] = value;
        }
      }
    }

    M.finalize();

    Timer multiplyTimer;
    multiplyTimer.start();
    W = L * (M * LT);
    multiplyTimer.stop();

    std::cout << multiplyTimer << std::endl;

    save_matrix_matlab("W2.mtx", &W);
    save_matrix_market_exchange("W2.fld", &W);

#else
    ////Vector<T> lcpB(getNConstraints()*3);
    //lcpB.setSparse(true);
    //solverB.setSparse(true);
    //solverX.setSparse(true);
    SpMatrix<N, T> lcpBsp(1, getNConstraints()*3);
    SpMatrix<N, T> solverBsp(1, this->getHeight());
    SpMatrix<N, T> solverXsp(1, this->getHeight());

    message("constraints = %d", getNConstraints() * 3);

    for(int i=0;i<getNConstraints()*3;i++){
      //message("column %d", i);
      //solverX.clear();
      lcpBsp.clearStructure();
      //solver.clear();
      ////lcpB.clear();

      lcpBsp[i][0] = (T)1.0;
      ////lcpB[i] = (T)1.0;

      ////solverB = LT * lcpB;
      ////solverBsp = LT * lcpBsp;
      SpMatrix<N, T>::multiply(&solverBsp, &LT, &lcpBsp);

      if(i%3 == 0){
        //getActiveConstraint(i/3)->print(std::cout);
      }

      // std::cout << *solverB << std::endl;
      //message("solverB");
      //getchar();
      //solver.solve(10000, (T)5e-5);
      //message("X = inv(L*D*LT) * X");
      /*Check size*/
      chol.inverseOperator(solverXsp, solverBsp);
      ////chol.inverseOperator(solverX, solverB);
      //message("solverX");
      //getchar();

      //message("compute W = L * X");
      ////lcpBsp = L * solverXsp;
      SpMatrix<N, T>::multiply(&lcpBsp, &L, &solverXsp);
      ////lcpB = L * solverX;
      //std::cout << lcpBsp << std::endl;

      //message("set W");
#if 0
      for(int j=0;j<getNConstraints()*3;j++){
        ///T val = lcpBsp[j][0];
        T val = lcpB[j];
        if(Abs(val) > 0.0){
          //W[i][j] = val;
          W[j][i] = val; /*Matrix should be symmetric*/
        }
      }

#else
      int nRows = getNConstraints() * 3;
      //message("construct W, %d, %d, %d, %d", W.getWidth(), W.getHeight(), W.getOrigWidth(), W.getOrigHeight());
      for(int bIndex = 0;bIndex<lcpBsp.n_blocks;bIndex++){
        int brow = lcpBsp.comp_row_indices[bIndex];
        for(int j=0;j<N;j++){
          int local_row = brow + j;
          //message("%d, %d", local_row, i);
          if(local_row < nRows && i < nRows){
            W[local_row][i] = lcpBsp.blocks[bIndex].m[j*N];
          }
        }
      }
#endif
    }
    spmv_timer->stop("delassus");
    cholTimer.stop();
    std::cout << cholTimer << std::endl;

    message("A blocks = %d", this->getNBlocks());
    message("L blocks = %d", chol.getL().getNBlocks());
    message("W blocks = %d", W.getNBlocks());
    //save_matrix_matlab("W.mtx", &W);
    //save_matrix_matlab("L.mtx", &chol.getL());
    //SpMatrix<N, T> DDD = chol.getD();
    //save_matrix_matlab("D.mtx", &DDD);
    //save_matrix_market_exchange("W.fld", &W);
    getchar();
    spmv_timer->start("inverse_operator");
    solverBsp.clearStructure();
    for(int i=0;i<this->getHeight();i++){
      solverBsp[i][0] = (T)1.0;
    }

    chol.inverseOperator(solverXsp, solverBsp);

    spmv_timer->stop("inverse_operator");
#endif
  }
#endif
}


#endif/*SPMATRIXC_HPP*/
