/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTSETEDGE_HPP
#define CONSTRAINTSETEDGE_HPP

#include "datastructures/DCEList.hpp"
#include "geo/Edge.hpp"
#include "datastructures/Tree.hpp"
#include "math/EvalType.hpp"

namespace CGF{

  template<class T>
  class CollisionContext;

  template<class T>
  class ConstraintCandidateEdge;

  template<class T>
  class ConstraintSetEdge{
  public:
    typedef ConstraintCandidateEdge<T> Candidate;
    typedef typename Map<int, Candidate*>::Iterator CandidatePtrMapIterator;
    typedef typename Tree<Candidate*>::Iterator CandidatePtrTreeIterator;

    ConstraintSetEdge();

    virtual ~ConstraintSetEdge();

    void setBackFace();

    void deactivateAll(CollisionContext<T>* n);

    bool evaluate(Edge<T>& p, Vector4<T>& com, Quaternion<T>& rot,
                  CollisionContext<T>* n,
                  const EvalType& etype,
                  EvalStats& stats);

    bool checkAndUpdate(Edge<T>& p, Vector4<T>& com, Quaternion<T>& rot,
                        CollisionContext<T>* n,
                        EvalStats& stats, bool friction,
                        bool force, T bnorm);

    bool updateInitial(Edge<T>& p, Vector4<T>& com, Quaternion<T>& rot,
                       CollisionContext<T>* n,
                       EvalStats& stats, bool friction,
                       bool force, T bnorm);

    void incrementalUpdate(CollisionContext<T>* n, bool onlyForced = false);
    bool checkForValidEdges(Edge<T>& p, Vector4<T>& com,
                            Quaternion<T>& rot,CollisionContext<T>* n);
    void update(CollisionContext<T>* n);

    void showEdgeCandidateState(int edge, CollisionContext<T>* ctx);

    void forceConstraint(CollisionContext<T>* n, int otherEdgeId);

    void updateStartPositions(const Edge<T>& edge,
                              const Vector4<T>& com,
                              const Quaternion<T>& rot,
                              Candidate* c);

    Map<int, Candidate*> candidates;
    Tree<int> candidatesTree;

    int edgeId;
    int vertexId[2];
    int faceId[2];
    Edge<T> startPosition;
    Vector4<T>      startCenterOfMass;
    Quaternion<T>   startOrientation;
    Edge<T> lastPosition;
    Vector4<T>      lastCenterOfMass;
    Quaternion<T>   lastOrientation;

    Tree<int> forcedPotentials;

    bool backFace;

    Mesh::GeometryType type;
  };
}

#endif/*CONSTRAINTSETEDGE_HPP*/
