/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef COLLISIONCONTEXT_HPP
#define COLLISIONCONTEXT_HPP

#include "geo/GridFile.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/List.hpp"

namespace CGF{
  template<class T>
  class ConstraintSet;

  template<class T>
  class ConstraintSetFaces;

  template<class T>
  class ConstraintSetEdge;

  template<class T>
  class ConstraintSetTriangleEdge;

  template<int N, class T>
  class SpMatrixC2;

  template<int N, class T>
  class IECLinSolveT2;

  template<class T>
  class CollisionContext{
  public:
    CollisionContext():stree(0), mesh(0), mat(0), solver(0),
                       sets(0), backSets(0), edgeSets(0),
                       faceFaceSets(0), v(0), bboxVertex(0),
                       bboxEdge(0), dt((T)0){
    }

    void extractPotentialVertexFace(int vertex, List<int>& faces, bool internal);
    void extractPotentialEdgeEdges(int edge, List<int>& edges, bool internal);
    void extractPotentialEdgeVertices(int edge, Tree<int>& vertices);

    void initializeBBoxes();
    void deleteBBoxes();
    void updateBBoxes();
    void checkBBoxesVertex(List<int>* invalidVertices);
    void checkBBoxesEdge(List<int>* invalidEdges);

    GridFile<T>*         gridFile;
    SurfaceTree<T>*      stree;
    DCTetraMesh<T>*      mesh;
    SpMatrixC2<2, T>*    mat;
    IECLinSolveT2<2, T>* solver;

    ConstraintSet<T>*             sets;
    ConstraintSet<T>*             backSets;
    ConstraintSetEdge<T>*         edgeSets;
    ConstraintSetEdge<T>*         backEdgeSets;
    ConstraintSetFaces<T>*        faceFaceSets;
    ConstraintSetTriangleEdge<T>* triangleEdgeSets;

    Vector<T>*  v;
    Vector4<T>* bboxVertex;
    Vector4<T>* bboxEdge;

    Vector<T>* vertexDegree;

    /*In order to check quickly if an edge is collapsed*/
    bool* collapsedEdges;
    bool* collapsedBackEdges;

    T dt;

    T getDT()const{
      return dt;
    }

    void setDT(T d){
      dt = d;
    }

    SpMatrixC2<2, T>* getMatrix()const{
      return mat;
    }

    IECLinSolveT2<2, T>* getSolver()const{
      return solver;
    }


    /*Debug interface*/

    void showStateVertexFace(int vertex, int face, bool backFace);
    void showStateEdgeEdge(int edgeA, int edgeB, bool backFace);
    void showStateEdgeVertex(int edge, int vertex);
  };
}

#endif/*COLLISIONCONTEXT_HPP*/
