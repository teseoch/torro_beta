/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTCANDIDATESTATUS_HPP
#define CONSTRAINTCANDIDATESTATUS_HPP

namespace CGF{
  enum CandidateStatus{Enabled, Disabled};
}

#endif/*CONSTRAINTCANDIDATESTATUS_HPP*/
