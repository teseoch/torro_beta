/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef MATH_HPP
#define MATH_HPP
#include <math.h>
#include "core/cgfdefs.hpp"
#include "core/Exception.hpp"
#include "core/types.hpp"

#ifndef PI
#define PI 3.14159265358979323846
#endif

#ifndef H_PI
#define H_PI (PI/2.0)
#endif

#ifndef D_PI
#define D_PI (PI*2.0)
#endif

#ifndef EULER
#define EULER 2.71828183
#endif

#ifndef PHI
#define PHI 1.6180339887498948482
#endif

#ifndef INV_PHI
#define INV_PHI 0.6180339887498948482
#endif

#include <limits>

#ifdef _WIN32
#define isnan(x) _isnan((x))
#endif

//#define FLOAT_MAX  1e+100
//#define FLOAT_MIN -1e+100
#define FLOAT_MAX std::numeric_limits<float>::max()
#define FLOAT_MIN std::numeric_limits<float>::min()

namespace CGF{
  template<class T>
  class Math{
  public:
    static const T Pi = (T)3.14159265358979323846;
    static const T DPi;
    static const T HPi;
    static const T E = (T)2.7182818284590452353602874713527;
    static const T Max;
    static const T Min;
    static const T Phi = (T)1.6180339887498948482;
    static const T InvPhi = (T)0.6180339887498948482;
  };

  template<class T>
  const T Math<T>::DPi = Math<T>::Pi * (T)2.0;

  template<class T>
  const T Math<T>::HPi = Math<T>::Pi * (T)0.5;

  template<class T>
  const T Math<T>::Max = std::numeric_limits<T>::max();

  template<class T>
  const T Math<T>::Min = std::numeric_limits<T>::min();

  template<class T>
  inline bool IsNan(const T& v){
    if(IsFloatType<T>()){
      const unsigned long u = *(unsigned long*)&v;
      return (u&0x7F800000) == 0x7F800000 && (u&0x7FFFFF);
    }else if(IsDoubleType<T>()){
      const unsigned long long u = *(unsigned long long*)&v;
      return (u&0x7FF0000000000000ULL) == 0x7FF0000000000000ULL && (u&0xFFFFFFFFFFFFFULL);
    }else{
      error("IsNan applied on non a non floating point value");
      return false;
    }
  }

  template<class T>
  inline T MultiplyIdentity(const T&){
    error("calling default identity function");
    return T();
  }

  template<>
  inline float MultiplyIdentity(const float&){
    return 1.0f;
  }

  template<>
  inline double MultiplyIdentity(const double&){
    return 1.0;
  }

  template<>
  inline long double MultiplyIdentity(const long double&){
    return (long double)1.0;
  }

  template<class T>
  inline T SumIdentity(const T&){
    error("calling default identity function");
    return T();
  }

  template<>
  inline float SumIdentity(const float&){
    return 0.0f;
  }

  template<>
  inline double SumIdentity(const double&){
    return 0.0;
  }

  template<>
  inline long double SumIdentity(const long double&){
    return (long double)0.0;
  }

  template<class T>
  inline const T& Min(const T& a, const T& b){
    return (a > b) ? b : a;
  }

  template<class T>
  inline const T& Max(const T& a, const T& b){
    return (a > b) ? a : b;
  }

  template<class T>
  inline const T& Min(const T& a, const T& b, const T& c, const T& d){
    return Min(Min(a, b), Min(c, d));
  }

  template<class T>
  inline const T& Max(const T& a, const T& b, const T& c, const T& d){
    return Max(Max(a, b), Max(c, d));
  }

  template<class T>
  inline T Pos(const T& a){
    return Max(a, (T)0.0);
  }

  template<class T>
  inline T Neg(const T& a){
    return Min(a, (T)0.0);
  }

  template<class T>
  inline const T& Clamp(const T& a, const T& min, const T& max){
    if(a < min){
      return min;
    }else if(a > max){
      return max;
    }
    return a;
  }

  template<class T>
  inline T Clamp01(const T& a){
    return Clamp(a, (T)0.0, (T)1.0);
  }

  template<class T>
  inline T Sqr(const T& f){
    if(IsRealType<T>()){
      return f * f;
    }else{
      error("Sqr applied on non a non floating point value");
      return (T)0;
    }
  }

  template<class T>
  inline T Sqrt(const T& v){
    error("Sqrt applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Sqrt(const float& v){
    return sqrtf(v);
  }

  template<>
  inline double Sqrt(const double& v){
    return sqrt(v);
  }

  template<>
  inline long double Sqrt(const long double& v){
    return sqrtl(v);
  }

  template<class T>
  inline T Abs(const T& v){
    error("Abs applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Abs(const float& v){
    return fabsf(v);
  }

  template<>
  inline double Abs(const double& v){
    return fabs(v);
  }

  template<>
  inline long double Abs(const long double& v){
    return fabsl(v);
  }

#if 0
  template<class T>
  inline bool Equal(const T& a, const T& b, const T& eps = (T)1e-6){
    if(Abs(a-b) < ((Abs(a) < Abs(b)) ? Abs(b) : Abs(a)) * eps){
      return true;
    }
    return false;
  }

  template<class T>
  inline bool NotEqual(const T& a, const T& b, const T& eps = (T)1e-6){
    return !Equal(a, b, eps);
  }
#endif

  template<class T>
  inline T Mod(const T& v, const T& b){
    error("Mod applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Mod(const float& v, const float& b){
    return fmodf(v, b);
  }

  template<>
  inline double Mod(const double& v, const double& b){
    return fmod(v, b);
  }

  template<>
  inline long double Mod(const long double& v, const long double& b){
    return fmodl(v, b);
  }

  template<class T>
  inline T Sign(const T& a, const T& b){
    return (b >= SumIdentity(T())) ? Abs(a) : -Abs(a);
  }

  template<class T>
  inline T Sign(const T& n){
    return Sign(MultiplyIdentity(T()), n);
  }

  template<class T>
  inline T Log(const T& v){
    error("Log applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Log(const float& v){
    return logf(v);
  }

  template<>
  inline double Log(const double& v){
    return log(v);
  }

  template<>
  inline long double Log(const long double& v){
    return logl(v);
  }

  template<class T>
  inline T Ceil(const T& v){
    error("Ceil applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Ceil(const float& v){
    return ceilf(v);
  }

  template<>
  inline double Ceil(const double& v){
    return ceil(v);
  }

  template<>
  inline long double Ceil(const long double& v){
    return ceill(v);
  }

  template<class T>
  inline T Floor(const T& v){
    error("Floor applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Floor(const float& v){
    return floorf(v);
  }

  template<>
  inline double Floor(const double& v){
    return floor(v);
  }

  template<>
  inline long double Floor(const long double& v){
    return floorl(v);
  }

  template<class T>
  inline T Exp(const T& v){
    error("Exp applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Exp(const float& v){
    return expf(v);
  }

  template<>
  inline double Exp(const double& v){
    return exp(v);
  }

  template<>
  inline long double Exp(const long double& v){
    return expl(v);
  }

  template<class T>
  inline T Round(const T& v){
    error("Round applied on a non floating point type");
    return (T)0.0;
  }

  template<>
  inline float Round(const float& v){
    return roundf(v);
  }

  template<>
  inline double Round(const double& v){
    return round(v);
  }

  template<>
  inline long double Round(const long double& v){
    return roundl(v);
  }

  template<class T>
  inline T Tan(const T& v){
    error("Tan applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Tan(const float& v){
    return tanf(v);
  }

  template<>
  inline double Tan(const double& v){
    return tan(v);
  }

  template<>
  inline long double Tan(const long double& v){
    return tanl(v);
  }

  template<class T>
  inline T CoTan(const T& v){
    return (T)1.0/Tan(v);
  }

  template<class T>
  inline T Atan(const T& v){
    error("ATan applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Atan(const float& v){
    return atanf(v);
  }

  template<>
  inline double Atan(const double& v){
    return atan(v);
  }

  template<>
  inline long double Atan(const long double& v){
    return atanl(v);
  }

  template<class T>
  inline T Atan2(const T& y, const T& x){
    error("Atan2 applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Atan2(const float& y, const float& x){
    return atan2f(y, x);
  }

  template<>
  inline double Atan2(const double& y, const double& x){
    return atan2(y, x);
  }

  template<>
  inline long double Atan2(const long double& y, const long double& x){
    return atan2l(y, x);
  }

  /*Custom Atan2 func*/
  template<class T>
  inline T Atan22(const T& y, const T& x){
    if(y != (T)0.0){
      return (T)2.0 * Atan( (Sqrt(Sqr(x) + Sqr(y)) - x) / y);
    }else if(x > (T)0.0){
      return (T)0.0;
    }else if(x < (T)0.0){
      return (T)PI;
    }else{
      error("Atan22 undefined for (0,0)");
      return (T)0.0;
    }
  }

  template<class T>
  inline T Sin(const T& v){
    error("Sin applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Sin(const float& v){
    return sinf(v);
  }

  template<>
  inline double Sin(const double& v){
    return sin(v);
  }

  template<>
  inline long double Sin(const long double& v){
    return sinl(v);
  }

  template<class T>
  inline T ArcSin(const T& v){
    error("ArcSin applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float ArcSin(const float& v){
    cgfassert(v >= -(float)1.0);
    cgfassert(v <=  (float)1.0);
    return asinf(v);
  }

  template<>
  inline double ArcSin(const double& v){
    cgfassert(v >= -(double)1.0);
    cgfassert(v <=  (double)1.0);
    return asin(v);
  }

  template<>
  inline long double ArcSin(const long double& v){
    cgfassert(v >= -(long double)1.0);
    cgfassert(v <=  (long double)1.0);
    return asinl(v);
  }

  template<class T>
  inline T Cos(const T& v){
    error("Cos applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Cos(const float& v){
    return cosf(v);
  }

  template<>
  inline double Cos(const double& v){
    return cos(v);
  }

  template<>
  inline long double Cos(const long double& v){
    return cosl(v);
  }

  template<class T>
  inline T ArcCos(const T& v){
    error("Acos applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float ArcCos(const float& v){
    cgfassert(v >= -(float)1.0);
    cgfassert(v <=  (float)1.0);
    return acosf(v);
  }

  template<>
  inline double ArcCos(const double& v){
    cgfassert(v >= -(double)1.0);
    cgfassert(v <=  (double)1.0);
    return acos(v);
  }

  template<>
  inline long double ArcCos(const long double& v){
    cgfassert(v >= -(long double)1.0);
    cgfassert(v <=  (long double)1.0);
    return acosl(v);
  }

  template<class T>
  inline T SafeArcCos(T v){
    return ArcCos(Clamp(v, (T)-1.0, (T)1.0));
  }

  template<class T>
  inline T RadiansToDegrees(T rad){
    return rad * (T)360.0 / (T)D_PI;
  }

  template<class T>
  inline T DegreesToRadians(T deg){
    return deg * (T)D_PI / (T)360.0;
  }

  template<class T>
  inline T Log2(const T& n){
    return Log(n) / Log((T)2.0);
  }


  template<class T>
  inline T Pow(const T& v, const T& p){
    error("Pow applied on non a non floating point value");
    return (T)0;
  }

  template<>
  inline float Pow(const float& v, const float& p){
    return powf(v, p);
  }

  template<>
  inline double Pow(const double& v, const double& p){
    return pow(v, p);
  }

  template<>
  inline long double Pow(const long double& v, const long double& p){
    return powl(v, p);
  }

  template<class T>
  inline T Coth(const T& v){
    T t = Pow<T>((T)EULER, v);
    return (t + (T)1.0/t) / (t - (T)1.0 / t);
  }
  /*
  template<class T>
  inline T grad2rad(const T& grad){
    return (T)D_PI*grad/(T)360;
  }

  template<class T>
  inline T rad2grad(const T& rad){
    return (T)360*rad/(T)D_PI;
  }
  */

  template<class T>
  inline T kroneckerDelta(int i, int j){
    return (i == j)?(T)1.0:(T)0.0;
  }

  template<class T>
  T pythagoras(T a, T b){
    T at = Abs(a);
    T bt = Abs(b);
    T ct, result;
    if(at > bt){
      ct = bt / at;
      result = at * Sqrt((T)1.0 + ct * ct);
    }else if(bt > (T)0.0){
      ct = at / bt;
      result = bt * Sqrt((T)1.0 + ct * ct);
    }else{
      return (T)0.0;
    }
    return result;
  }

  template<class T>
  inline void quicksort(T* data, int start, int end){
    if(start == end){
      return;
    }
    T midValue = data[(start + end)/2];

    int left = start;
    int right = end;

    do{
      while(data[left] < midValue){
        left++;
      }

      while(midValue < data[right]){
        right--;
      }

      if(left <= right){
        /*Swap*/
        T valueLeft = data[left];
        data[left] = data[right];
        data[right] = valueLeft;
        left++;
        right--;
      }
    }while(left <= right);
    if(start < right){
      /*Sort left*/
      quicksort(data, start, right);
    }

    if(left < end){
      /*Sort right*/
      quicksort(data, left, end);
    }
  }

  template<class T>
  inline T aitkenD2(T pn, T pn1, T pn2){
    T denom = pn2 - (T)2.0*pn1 + pn;
    if(Abs(denom) < 1e-16){
      /*second derivative is zero*/
      return pn;
    }

    return pn - Sqr(pn1 - pn)/denom;
  }

  template<class T, class Y>
  inline Y linterp(T iso, Y* d1, Y* d2, T val1, T val2,
                   T tol = (T)1E-40){
    Y *op1, *op2;
    T opv1, opv2;
    if(val1 > val2){
      op1 = d1;
      op2 = d2;
      opv1 = val1;
      opv2 = val2;
    }else{
      op1 = d2;
      op2 = d1;
      opv1 = val2;
      opv2 = val1;
    }

    T mu;

    //message("iso - opv1 = %10.10e", iso - opv1);
    if(Abs(iso - opv1) < tol)
      return *op1;
    //message("iso - opv2 = %10.10e", iso - opv2);
    if(Abs(iso - opv2) < tol)
      return *op2;
    //message("opv1 - opv2 = %10.10e", opv1 - opv2);
    if(Abs(opv1 - opv2) < tol)
      return *op1;

    mu = (iso - opv1)/(opv2 - opv1);
    //message("mu = %10.10e", mu);
    return *op1 + mu * (*op2 - *op1);
  }

#if 1
  template<class T, class X, class Y>
  class Interpolator{
  public:
    void init(){

    }

    T evaluate(T){
      return 0.0;
    }

    void getResult(T a, X* b, Y* c){

    }

    void setComputeDerivative(bool d, T dx){

    }
  };

#if 1
  template<class T, class X, class Y>
  T secant(Interpolator<T, X, Y>* func, T x1, T x2, T tol){
    func->init();

    T fx1 = func->evaluate(x1);
    T fx2 = func->evaluate(x2);

    for(int i=0;i<100;i++){
      //message("iter = %d, fx1, fx2 = %10.10e, %10.10e, x1, x2 = %10.10e, %10.10e",
      //i, fx1, fx2, x1, x2);

      T derivative = (fx2 - fx1)/(x2 - x1);

      T xt = x2 - (fx2)/(derivative);

      T fxt = func->evaluate(xt);

      //message("derivative = %10.10e, %10.10e, %10.10e",
      //      derivative, (fx2 - fx1), (x2 - x1));
      //message("xn = %10.10e, fxn = %10.10e", xt, fxt);

      if(Abs(derivative) < tol){
        return x2;
      }


      x1 = x2;
      fx1 = fx2;

      x2 = xt;
      fx2 = fxt;

      if(Abs(fxt) < tol){
        return xt;
      }
      if(Abs(x1 - x2) < tol){
        return xt;
      }
    }
    return x2;
  }


  template<class T, class X, class Y>
  T falsePosition(Interpolator<T, X, Y>* func, T x1, T x2, T tol){
    func->init();

    T fx1 = func->evaluate(x1);
    T fx2 = func->evaluate(x2);
#if 0
    if(fx1 > 0){
      /*Swap such that fx1, x1 are the negative part of the function*/
      T fxt = fx1;
      T xt = x1;
      fx1 = fx2;
      x1 = x2;
      fx2 = fxt;
      x2 = xt;
    }
#endif
    int side = 0;

    for(int i=0;i<100;i++){
      //message("fx1, fx2 = %10.10e, %10.10e", fx1, fx2);
      //T xn = x1 - (x2-x1)*fx1/(fx2-fx1);

      T xn = (fx1 * x2 - fx2 * x1)/(fx1 - fx2);

      if(Abs(x2 - xn) < tol || Abs(x1 - x2) < tol){
        return xn;
      }

      T fxn = func->evaluate(xn);

      //message("iter = %d, f(%10.10e) = %10.10e", i, xn, fxn);

      if(Abs(fxn) < tol){
        return xn;
      }

      //x2 = xn;
      //fx2 = fxn;

#if 0
      if(fx1*fxn > 0){
        x1 = xn;
        fx1 = fxn;
      }else{
        x2 = xn;
        fx2 = fxn;
      }
#else
      if(fxn * fx2 > 0.0){
        x2 = xn;
        fx2 = fxn;
        if(side == -1){
          fx1 /= (T)2.0;
        }
        side = -1;
      }else if(fx1 * fxn > 0.0){
        x1 = xn;
        fx1 = fxn;
        if(side == 1){
          fx2 /= (T)2.0;
        }
        side = 1;
      }else{
        return xn;
      }
#endif
    }
    return x2;
  }

  template<class T>
  class DoubleRootResult{
  public:
    DoubleRootResult(){
      roots[0] = roots[1] = 0.0;
      derivatives[0] = derivatives[1] = 0.0;
      defined[0] = defined[1] = false;
      functionValues[0] = functionValues[1] = functionValues[2] = (T)0.0;
      values[0] = values[1] = values[2] = (T)0.0;
    }
    T roots[2];
    T derivatives[2];
    bool defined[2];
    T functionValues[3];
    T values[3];
  };

  template<class T, class X, class Y>
  DoubleRootResult<T> doubleRootFinder(Interpolator<T, X, Y>* func,
                                       T x1, T x2, T tol){
    func->init();

    DoubleRootResult<T> result;

    /*Check if the root is bracketed*/
    T fx10 = func->evaluate(x1);
    T fx20 = func->evaluate(x2);

    DBG(message("fx10 = %10.10e", fx10));
    DBG(message("fx20 = %10.10e", fx20));

    if(Sign(fx10) != Sign(fx20)){
      result.roots[0] = brentDekker(func, x1, x2, tol);
      result.defined[0] = true;
      result.functionValues[0] = fx10;
      result.functionValues[1] = fx20;
      result.values[0] = x1;
      result.values[1] = x2;
      return result;
    }

    /*Same signs, approximate derivative at end and beginning*/
    T fx11 = func->evaluate(x1 + (T)1e-4);
    T fx21 = func->evaluate(x2 + (T)1e-4);

    DBG(message("fx11 = %10.10e", fx11));
    DBG(message("fx21 = %10.10e", fx21));

    T dfx1 = (fx11 - fx10)/(T)1e-4;
    T dfx2 = (fx21 - fx20)/(T)1e-4;

    DBG(message("Equal sign, derivatives = %10.10e, %10.10e", dfx1, dfx2));
    result.derivatives[0] = dfx1;
    result.derivatives[1] = dfx2;

    if(Sign(dfx1) != Sign(dfx2)){
      /*There is some maximum in between*/
      T mid = (x1 * dfx1 - x2 * dfx2)/(dfx1 - dfx2);
      DBG(message("  mid = %10.10e", mid));
      T fxmid = func->evaluate(mid);

      START_DEBUG;
      message("f 0   = %10.10e", fx10);
      message("f mid = %10.10e", fxmid);
      message("f 1   = %10.10e", fx20);

      message("x1 = %10.10e, mid = %10.10e, x2 = %10.10e\n\n", x1, mid, x2);
      END_DEBUG;

      if(Sign(fx10) != Sign(fxmid)){
        DBG(message("double root with different signed derivatives"));
        result.roots[0] = brentDekker(func, x1, mid, tol);
        result.defined[0] = true;

        result.roots[1] = brentDekker(func, mid, x2, tol);
        result.defined[1] = true;

        result.functionValues[0] = fx10;
        result.functionValues[1] = fxmid;
        result.functionValues[2] = fx20;
        result.values[0] = x1;
        result.values[1] = mid;
        result.values[2] = x2;

        DBG(message("root1 = %10.10e", result.roots[0]));
        DBG(message("root2 = %10.10e", result.roots[1]));
      }else{
        DBG(message("double root with equal signed derivatives"));

        func->setComputeDerivative(true, (T)1e-6);

        T extrema = brentDekker(func, x1, x2, tol);

        DBG(message("Extrema at %10.10e", extrema));

        func->setComputeDerivative(false, (T)0.0);

        fxmid = func->evaluate(extrema);
        DBG(message("fx1 = %10.10e", fx10));
        DBG(message("fxm = %10.10e", fxmid));
        DBG(message("fx2 = %10.10e", fx20));

        if(Sign(fxmid) == Sign(fx10)){
          result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
          result.defined[0] = true;

          result.functionValues[0] = fx10;
          result.functionValues[1] = fx20;
          result.values[0] = x1;
          result.values[1] = x2;

          if(result.roots[0] >= x1 && result.roots[0] <= x2){
            //ok
            (warning("After extrema test:: muller computed %10.10e", result.roots[0]));
          }else{
            result.defined[0] = false;
            (warning("After extrema test:: muller gave out of bounds result %10.10e", result.roots[0]));
            throw new MathException(__LINE__, __FILE__, "No root found, extrema has same sign as fx1 and fx2, muller failed");
            //throw new MathException(__LINE__, __FILE__, "No root found");
          }
        }

        result.roots[0] = brentDekker(func, x1, extrema, tol);
        result.defined[0] = true;
        DBG(message("Root 1 = %10.10e", result.roots[0]));

        result.roots[1] = brentDekker(func, extrema, x2, tol);
        result.defined[1] = true;
        DBG(message("Root 2 = %10.10e", result.roots[1]));

        result.functionValues[0] = fx10;
        result.functionValues[1] = fxmid;
        result.functionValues[2] = fx20;
        result.values[0] = x1;
        result.values[1] = extrema;
        result.values[2] = x2;

        DBG(message("root1 = %10.10e", result.roots[0]));
        DBG(message("root2 = %10.10e", result.roots[1]));
#if 0
        result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
        result.defined[0] = true;

        result.functionValues[0] = fx10;
        result.functionValues[1] = fx20;
        result.values[0] = x1;
        result.values[1] = x2;

        if(result.roots[0] >= x1 && result.roots[0] <= x2){
          //ok
          (warning("muller computed %10.10e", result.roots[0]));
        }else{
          result.defined[0] = false;
          (warning("muller gave out of bounds result %10.10e", result.roots[0]));
          throw new MathException(__LINE__, __FILE__, "No root found");
        }
#endif
      }
    }else{
      /*Derivatives have equal sign, assume monotonically
        (de/in)creasing function, no root, but check to be sure*/
#if 1
      result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
      result.defined[0] = true;

      result.functionValues[0] = fx10;
      result.functionValues[1] = fx20;
      result.values[0] = x1;
      result.values[1] = x2;

      if(result.roots[0] >= x1 && result.roots[0] <= x2){
        //ok
        warning("muller computed %10.10e", result.roots[0]);
      }else{
        result.defined[0] = false;
        warning("muller gave out of bounds result %10.10e", result.roots[0]);
        throw new MathException(__LINE__, __FILE__, "No root found");
      }
#endif
      //throw new MathException(__LINE__, __FILE__, "No root found, sd and derivatives have same sign");
    }

    return result;
  }

#if 0
  template<class T, class X, class Y>
  void multipleRootFinder(Interpolator<T, X, Y>* func,
                          T x1, T x2, T tol, List<T>& roots){
    func->init();

    /*Check if the root is bracketed*/
    T fx10 = func->evaluate(x1);
    T fx20 = func->evaluate(x2);

    T lastFoundRoot1 = 0.0;
    T lastFoundRoot2 = 0.0;

    bool doubleRoot = false;

    if(Sign(fx10) != Sign(fx20)){
      lastFoundRoot = brentDekker(func, x1, x2, tol);
      roots.append(lastFoundRoot);
    }else{
      /*Signs are equal, approximate derivative at end and beginning*/
      T fx11 = func->evaluate(x1 + (T)1e-4);
      T fx21 = func->evaluate(x2 + (T)1e-4);

      T dfx1 = (fx11 - fx10)/(T)1e-4;
      T dfx2 = (fx21 - fx20)/(T)1e-4;

      if(Sign(dfx1) != Sign(dfx2)){
        /*There is some maximum in between*/
        T mid = (x1 * dfx1 - x2 * dfx2)/(dfx1 - dfx2);
        message("  mid = %10.10e", mid);
        T fxmid = func->evaluate(mid);

        message("f 0   = %10.10e", fx10);
        message("f mid = %10.10e", fxmid);
        message("f 1   = %10.10e", fx20);

        message("x1 = %10.10e, mid = %10.10e, x2 = %10.10e\n\n", x1, mid, x2);

        if(Sign(fx10) != Sign(fxmid)){
          result.roots[0] = brentDekker(func, x1, mid, tol);
          result.defined[0] = true;

          result.roots[1] = brentDekker(func, mid, x2, tol);
          result.defined[1] = true;

          result.functionValues[0] = fx10;
          result.functionValues[1] = fxmid;
          result.functionValues[2] = fx20;
          result.values[0] = x1;
          result.values[1] = mid;
          result.values[2] = x2;

          message("root1 = %10.10e", result.roots[0]);
          message("root2 = %10.10e", result.roots[1]);
        }else{
          result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
          result.defined[0] = true;

          result.functionValues[0] = fx10;
          result.functionValues[1] = fx20;
          result.values[0] = x1;
          result.values[1] = x2;

          if(result.roots[0] >= x1 && result.roots[0] <= x2){
            //ok
            warning("muller computed %10.10e", result.roots[0]);
          }else{
            result.defined[0] = false;
            warning("muller gave out of bounds result %10.10e", result.roots[0]);
            throw new MathException(__LINE__, __FILE__, "No root found");
          }
        }
      }else{
        /*Derivatives have equal sign, assume monotonically (de/in)creasing function, no root*/
      }

    }



    message("Equal sign, derivatives = %10.10e, %10.10e", dfx1, dfx2);



    return result;
  }
#endif

  template<class T, class X, class Y>
  T bisection(Interpolator<T, X, Y>* func, T x1, T x2, T tol){
    T dx, f, fmid, xmid, rtb;

    func->init();

    f = func->evaluate(x1);
    fmid = func->evaluate(x2);

    if(f*fmid >= (T)0.0){
      error("Root must be bracketed");
    }

    rtb = f < (T)0.0 ? (dx = x2-x1, x1) : (dx=x1-x2, x2);

    for(int i = 0;i<100;i++){
      fmid = func->evaluate(xmid = rtb + (dx*=(T)0.5));
      if(fmid <= (T)0.0){
        rtb = xmid;
      }

      if(Abs(dx) < tol || fmid == (T)0.0){
        return rtb;
      }
    }
    error("Too many bisection iterations");
    return (T)0.0;
  }
#endif

  /*Also supports complex values of T*/
  template<class T, class X, class Y>
  T muller(Interpolator<T, X, Y>* func, T x0, T x1, T x2, T tol){
    int iter = 0;
    T h1 = x1 - x0;
    T h2 = x2 - x1;

    func->init();

    T fx0 = func->evaluate(x0);
    T fx1 = func->evaluate(x1);
    T fx2 = func->evaluate(x2);

    T delta1 = (fx1 - fx0)/h1;
    T delta2 = (fx2 - fx1)/h2;

    T d = (delta2 - delta1)/(h2 + h1);
    iter = 3;

    while(iter < 100){
      T b = delta2 + h2 * d;

      T arg = Sqr(b) - fx2 * d * (T)4.0;

      T D = Sqrt(arg);

      if(IsNan(D)){
        return D;
      }

      T E = (T)0.0;

      if(Abs(b - D) < Abs(b + D)){
        E = b + D;
      }else{
        E = b - D;
      }

      T h = fx2 * (T)(-2.0) / E;
      T x = x2 + h;

      if(Abs(h) < Abs(tol)){
        return x;
      }

      x0 = x1;
      fx0 = fx1;
      x1 = x2;
      fx1 = fx2;
      x2 = x;

      if(Abs(x2 - x1) < Abs(tol)){
        return x;
      }

      fx2 = func->evaluate(x2);

      h1 = x1 - x0;
      h2 = x2 - x1;
      delta1 = (fx1 - fx0)/h1;
      delta2 = (fx2 - fx1)/h2;

      d = (delta2 - delta1)/(h2 + h1);

      if(Abs(delta2 - delta1) < Abs(tol)){
        return x;
      }
      iter++;
    }
    return x2;
  }

  template<class T, class X, class Y>
  T brentDekker(Interpolator<T, X, Y>* func, T x1, T x2, T tol){
    int iter = 0;
    T a = x1;
    T b = x2;
    T c = x2;
    T d = 0, e = 0, min1, min2;

    T meps = (T)5e-8;

    func->init();

    T fa = func->evaluate(a);
    T fb = func->evaluate(b);
    T fc, p, q, r, s, tol1, xm;

#if 0
    if(Abs(fa) < meps){
      return a;
    }

    if(Abs(fb) < meps){
      return b;
    }
#endif

    if( (fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)){
      warning("Root is not bracketed, %10.10e, %10.10e, %10.10e, %10.10e",
              fa, fb, a, b);
      //error("Root is not bracketed, %10.10e, %10.10e", fa, fb);
      throw new MathException(__LINE__, __FILE__, "Root not bracketed");
    }

    fc = fb;

    for(iter=0;iter<100;iter++){
      if( (fb > 0.0 && fc > 0.0) ||
          (fb < 0.0 && fc < 0.0)){
        c  = a;
        fc = fa;
        e  = d = b - a;
      }

      if(Abs(fc) < Abs(fb)){
        a = b;
        b = c;
        c = a;
        fa = fb;
        fb = fc;
        fc = fa;
      }

      tol1 = (T)2.0 * meps * Abs(b) + (T)0.5*tol;
      xm   = (T)0.5 * (c-b);

      if(Abs(xm) <= tol1 || fb == (T)0.0){
        return b;
      }

      if(Abs(e) >= tol1 && Abs(fa) > Abs(fb)){
        /*Inverse quadratic interpolation*/
        s = fb/fa;

        if(a == c){
          p = (T)2.0*xm*s;
          q = (T)1.0 - s;
        }else{
          q = fa/fc;
          r = fb/fc;
          p = s*( (T)2.0*xm*q*(q-r) - (b-a)*(r-(T)1.0) );
          q = (q-(T)1.0)*(r-(T)1.0)*(s-(T)1.0);
        }

        if(p > 0.0){
          q = -q;
        }

        p = Abs(p);

        min1 = (T)3.0*xm*q - Abs(tol1*q);
        min2 = Abs(e*q);

        if((T)2.0*p < (min1 < min2 ? min1 : min2)){
          /*Accept interpolation*/
          e = d;
          d = p/q;
        }else{
          /*Interpolation failed, use bisection*/
          d = xm;
          e = d;
        }
      }else{
        /*Bounds decreasing too slowly, use bisection*/
        d = xm;
        e = d;
      }

      a = b;
      fa = fb;
      if( Abs(d) > tol1){
        b += d;
      }else{
        b += Sign(tol1, xm);
      }
      fb = func->evaluate(b);
    }

    error("Maximum numbers of iterations");
    return 0.0;

  }
#endif

  template<class T>
  void machine_epsilon(){
    T mach_eps = (T)1.0;
    do{
      mach_eps /= (T)2.0;
    }while((T)(1.0 + (mach_eps/(T)2.0)) != (T)1.0);
    message("Machine epsilon = %10.10e", mach_eps/(T)2.0);
  }


  struct _VectorRange{
    int startBlock;
    int endBlock;
    int range;
  };

  typedef struct _VectorRange VectorRange;

  struct _MatrixRange{
    int startRow;
    int endRow;
    int range;
  };

  typedef struct _MatrixRange MatrixRange;
}

#endif/*MATH_HPP*/
