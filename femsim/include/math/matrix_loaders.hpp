/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef MATRIX_LOADERS_HPP
#define MATRIX_LOADERS_HPP

#include "core/cgfdefs.hpp"

namespace CGF{
  template<int N, class T>
  class SpMatrix;

  template<int N, class T>
  class SpMatrixC2;

  template<class T>
  class Vector;

  template<int N, class T>
  SpMatrix<N, T>* load_matrix_market_exchange(const char* filename);

  template<int N, class T>
  void save_matrix_market_exchange(const char* filename,
                                   const SpMatrix<N, T>*const mat);

  template<int N, class T>
  Vector<T>* load_matrix_market_exchange_vector(const char* filename);

  template<int N, class T>
  bool save_matrix_matlab(const char* filename, const SpMatrix<N, T>* const mat);

  template<int N, class T>
  bool save_matrix_matlab(const char* filename, const SpMatrixC2<N, T>* const mat);

  template<int N, class T>
  SpMatrix<N, T>* load_matrix_matlab(const char* filename);

}

#endif/*MATRIX_LOADERS_HPP*/
