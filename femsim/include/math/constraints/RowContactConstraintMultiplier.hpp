/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef ROWCONTACTCONSTRAINTMULTIPLIER_HPP
#define ROWCONTACTCONSTRAINTMULTIPLIER_HPP

#include "math/constraints/RowConstraintMultiplier.hpp"
#include "math/Matrix44.hpp"

namespace CGF{
  template<int N, class T>
  class CGFAPI RowContactConstraintMultiplier :
    public RowConstraintMultiplier<N, T>{
  public:
    RowContactConstraintMultiplier(){

    }
    virtual ~RowContactConstraintMultiplier(){

    }
    /*a * D * b'*/
    void multiply(const AbstractRowConstraint<N, T>* a,
                  VectorC2<T>* D,
                  const AbstractRowConstraint<N, T>* b);

    void store(SpMatrix<N, T>* mat);


    void clear(){
      result.clear();
    }

  protected:
    Matrix44<T> tmp[5];
    Matrix44<T> result;
  };

}

#endif/*ROWCONTACTCONSTRAINTMULTIPLIER_HPP*/
