/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef GENERALROWCONSTRAINTMULTIPLIER_HPP
#define GENERALROWCONSTRAINTMULTIPLIER_HPP

#include "math/constraints/RowConstraintMultiplier.hpp"
#include "math/Matrix44.hpp"

namespace CGF{
  template<int N, class T>
  class CGFAPI GeneralRowConstraintMultiplier:
    public RowConstraintMultiplier<N, T>{
  public:
    GeneralRowConstraintMultiplier(){

    }

    virtual ~GeneralRowConstraintMultiplier(){

    }

    void multiply(const AbstractRowConstraint<N, T>* a,
                  VectorC2<T>* D,
                  const AbstractRowConstraint<N, T>* b);

    void store(SpMatrix<N, T>* mat);

    void clear();

  protected:
    Matrix44<T> localMatrix;
    int row;
    int col;
  };
}


#endif/*GENERALROWCONSTRAINTMULTIPLIER_HPP*/
