/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef ABSTRACTROWCONSTRAINT_HPP
#define ABSTRACTROWCONSTRAINT_HPP

#include "math/Vector4.hpp"
#include "math/constraints/AbstractMatrixConstraint.hpp"
#include "math/EvalType.hpp"

namespace CGF{

  template<int N, class T>
  class AbstractBlockConstraint;

  template<int N, class T>
  class RowConstraintMultiplier;

  class AdjacencyList;

  template<int N, class T>
  class CGFAPI AbstractRowConstraint : public AbstractMatrixConstraint<N, T>{
  public:
    AbstractRowConstraint():AbstractMatrixConstraint<N, T>(){
      this->type = rowConstraint;
    }

    virtual ~AbstractRowConstraint(){
    }

    static const int interval = 10;

    virtual int getInterval()const{
      return interval;
    }

    /*Used as a general interface for accessing the values. Used in ICA where
      constraints of different types are multiplied. Each type has a different
      storage scheme which are not compatible with each other. This function
      allows one to access the elements in a general way.
     */
    virtual ConstraintElement<T> getValue(int row, int col)const = 0;

    virtual T& getConstraintValue(int i) = 0;
    virtual const T& getConstraintValue(int i)const = 0;
    virtual void computePreconditioner(VectorC2<T>&C)const = 0;

    virtual void computePreconditionerMatrix(VectorC2<T>& DD,
                                             SpMatrixC2<N, T>& C,
                                             const SpMatrixC2<N, T>& B,
                                             AdjacencyList* l,
                                             VectorC2<T>* scaling = 0)const = 0;

    virtual AbstractBlockConstraint<N,T>* constructBlockConstraint()const = 0;
    virtual RowConstraintMultiplier<N,T>* constructConstraintMultiplier()const = 0;

    virtual void resetMultipliers(VectorC2<T>& x, Vector4<T>& c)const=0;
    virtual void cacheMultipliers(Vector4<T>& c, VectorC2<T>& x)const=0;

    /*Evaluates Hx > c and (de)activates the constraint accordingly,
      returns tru if a constraint has been changed*/
    virtual bool evaluate(VectorC2<T>& x, VectorC2<T>& b, T meps,
                          const EvalType& etype, EvalStats& stats,
                          bool* active = 0,
                          VectorC2<T>* kb = 0) = 0;

    virtual bool project(VectorC2<T>& x, VectorC2<T>& b, T meps,
                         const EvalType& etype,
                         bool* active = 0,
                         VectorC2<T>* kb = 0) = 0;

    virtual void computeFBFunction(VectorC2<T>& np,
                                   VectorC2<T>& sf,
                                   VectorC2<T>& kf,
                                   SpMatrixC2<N, T>& C,
                                   const VectorC2<T>& x,
                                   const VectorC2<T>& b,
                                   const VectorC2<T>& b2) = 0;

    //virtual void computeResidual(VectorC2<T>& r, const VectorC2<T>& x) = 0;
  };

  template<int N, class T>
  std::ostream& operator<<(std::ostream& os,
                           const AbstractMatrixConstraint<N, T>* const c){
    c->print(os);
    return os;
  }
}

#endif/*ABSTRACTROWCONSTRAINT_HPP*/
