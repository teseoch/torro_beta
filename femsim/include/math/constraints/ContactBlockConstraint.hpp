/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONTACTBLOCKCONSTRAINT_HPP
#define CONTACTBLOCKCONSTRAINT_HPP

#include "math/constraints/Constraint.hpp"
#include "math/constraints/AbstractBlockConstraint.hpp"
#include "math/Matrix44.hpp"

namespace CGF{
  template<int N, class T>
  class CGFAPI ContactBlockConstraint :
    public AbstractBlockConstraint<N, T>{
  public:
    template<class TT>
      friend class VectorC2;

    template<int M, class TT>
      friend class SpMatrixC2;

    template<int M, class TT>
      friend class IECLinSolveT2;

    template<int M, class TT>
      friend class IECLinSolveT2CR;

    template<int M, class TT>
      friend class ContactConstraint;

    ContactBlockConstraint();

    virtual ~ContactBlockConstraint(){

    }

    virtual void collectElements(List<ConstraintElement<T> >& elements)const{
    }

    virtual void multiplyAHSHA(VectorC2<T>& r, const VectorC2<T>& x,
                               const SpMatrixC2<N, T>* mat,
                               bool transposed,
                               const VectorC2<T>* mask)const;


    void generalMultiply(VectorC2<T>& r, const VectorC2<T>& x,
                         bool transposed = false)const;

    /*Performs a multiplication r = Q x*/
    virtual void multiply(VectorC2<T>& r, const VectorC2<T>& x)const;

    /*Performs a multiplication r = Q^T x*/
    virtual void multiplyTransposed(VectorC2<T>& r, const VectorC2<T>& x)const;
    void setIndex(int i, int idx){
      cgfassert(i>=0);
      cgfassert(i<5);
      index[i] = idx;
    }

    int getIndex(int i)const{
      cgfassert(i>=0);
      cgfassert(i<5);
      return index[i];
    }

    int getProjectionType(int l)const{
      return 0;
    }

    void print(std::ostream& os)const{
      os << "Block constraint" << std::endl;
    }

    ConstraintStatus getStatus()const{
      return this->status;
    }

    void destroy(VectorC2<T>&b, VectorC2<T>& x){
      //cgfassert(this->indexMap.size() == 0);
      //cgfassert(this->indices.size() == 0);
    }

    void extractValues(SpMatrix<N, T>* L,
                       SpMatrix<N, T>* LT,
                       Tree<int>* columns,
                       int row,
                       Vector<T>* rhs)const{

    }


  protected:
    int index[5];
    Matrix44<T> S;
    Matrix44<T> OS;
    Matrix44<T> SHA[5];
    Matrix44<T> AHS[5];
    Matrix44<T> HA[5];
    Matrix44<T> AHT[5];

    Matrix44<T> SHAT[5];
    Matrix44<T> AHST[5];
    Matrix44<T> HAT[5];
  };
}

#endif/*CONTACTBLOCKCONSTRAINT_HPP*/
