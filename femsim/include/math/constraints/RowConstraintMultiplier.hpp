/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef ROWCONSTRAINTMULTIPLIER_HPP
#define ROWCONSTRAINTMULTIPLIER_HPP

#include "math/constraints/AbstractRowConstraint.hpp"

namespace CGF{
  template<int N, class T>
  class SpMatrix;

  /*A multiplied constraint is the result of the multiplication of 2
    constraints. Usually the result of a multiplication HKH', in which
    K is a sparse matrix and H the matrix containing all constraints.*/
  template<int N, class T>
  class CGFAPI RowConstraintMultiplier{
  public:
    RowConstraintMultiplier(){

    }

    virtual ~RowConstraintMultiplier(){

    }

    virtual void multiply(const AbstractRowConstraint<N, T>* a,
                          VectorC2<T>* D,
                          const AbstractRowConstraint<N, T>* b) = 0;

    virtual void store(SpMatrix<N, T>* mat) = 0;

    virtual void clear() = 0;

  protected:
    int row;
    int col;
    int offset;
  };
}

#endif/*ROWCONSTRAINTMULTIPLIER_HPP*/
