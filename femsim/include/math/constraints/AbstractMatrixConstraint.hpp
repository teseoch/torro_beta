/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef ABSTRACTMATRIXCONSTRAINT_HPP
#define ABSTRACTMATRIXCONSTRAINT_HPP

#include "math/ConstraintOperations.hpp"
#include "math/constraints/Constraint.hpp"
#include "datastructures/List.hpp"

namespace CGF{
  /*For a matrix |A B'|
                 |B C |, row corresponds with sub-matrix B, block
    with C and A. In case of an spmv, type row multiplies B and B',
    while type block only multiplies C or A.*/
  enum AbstractConstraintType{rowConstraint, blockConstraint};

  /*Used to store values of the abstract constraint into some sparse
    representation, used for exporting the matrix.*/
  template<class T>
  class ConstraintElement{
  public:
    int row;
    int col;
    T value;
    T HA;
    T SHA;
    T D;
  };

  template<class T>
  class VectorC2;

  template<class T>
  class Vector;

  template<int N, class T>
  class SpMatrixC2;

  template<int N, class T>
  class SpMatrix;

  template<class T>
  class Tree;


  template<int N, class T>
  class CGFAPI AbstractMatrixConstraint{
  public:
    template<int M, class U>
      friend void spmvc(VectorC2<U>& r, const SpMatrixC2<M, U>& m,
                        const VectorC2<U>& v, const MatrixMulMode mode,
                        const VectorC2<U>* mask);

    AbstractMatrixConstraint(){
      row_id = Constraint::undefined;
      id = Constraint::undefined;
      row_id_given = Constraint::undefined;
      status = Inactive;
      offset = 0;
      cSize = 0;
      matrix = 0;
    }

    virtual ~AbstractMatrixConstraint(){

    }

    /*Performs a multiplication r = Hx*/
    virtual void multiply(VectorC2<T>& r, const VectorC2<T>& x)const = 0;

    /*Performs a multiplication r = (H^T)x*/
    virtual void multiplyTransposed(VectorC2<T>& r,
                                    const VectorC2<T>& x)const = 0;

    virtual void extractValues(SpMatrix<N, T>* L,
                               SpMatrix<N, T>* LT,
                               Tree<int>* columns, int row,
                               Vector<T>* rhs)const = 0;

    virtual void preMultiply(VectorC2<T>& r, const VectorC2<T>& x,
                             const SpMatrixC2<N, T>* mat,
                             bool transposed = false,
                             const VectorC2<T>* mask = 0)const = 0;

    virtual void setIndex(int i, int idx) = 0;
    virtual int getIndex(int i)const = 0;
    virtual int getProjectionType(int i)const = 0;

    virtual void print(std::ostream& os)const = 0;

    virtual ConstraintStatus getStatus()const = 0;

    virtual void init(SpMatrixC2<N, T>* owner){
      matrix = owner;
    }
    virtual void destroy(VectorC2<T>& b, VectorC2<T>& x) = 0;

    int getOffset()const{
      return offset;
    }

    int getSize()const{
      return cSize;
    }

    virtual int getIndividualSize()const = 0;

    SpMatrixC2<N, T>* getMatrix()const{
      return matrix;
    }

    template<int M, class TT>
      friend std::ostream& operator<<(std::ostream& os,
                                      const AbstractMatrixConstraint<M, TT>* const c);

    AbstractConstraintType getType()const{
      return type;
    }

    int getSubType()const{
      return subType;
    }

    virtual void collectElements(List<ConstraintElement<T> >& elements)const = 0;

    void setBB2Norm(T n){
      bb2norm = n;
    }

    void setBB2NormCheck(T n){
      bb2normCheck = n;
    }

    int row_id;
    int id;
    int row_id_given;
    ConstraintStatus status;
    AbstractConstraintType type;
    int subType;
    mutable VectorC2<T>* preconditioner;
    int sourceType;
    int sourceA;
    int sourceB;
  protected:
    int offset;
    int cSize;
    SpMatrixC2<N, T>* matrix;
    mutable T bb2norm;
    mutable T bb2normCheck;
  };
}

#endif/*ABSTRACTMATRIXCONSTRAINT_HPP*/
