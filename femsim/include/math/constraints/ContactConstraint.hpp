/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONTACTCONSTRAINT_HPP
#define CONTACTCONSTRAINT_HPP

#include "math/constraints/Constraint.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"
#include "math/VectorC.hpp"
#include "math/RunningAverage.hpp"

namespace CGF{

#define DIAGONAL_PRECONDITIONER 1
#define FULL_PRECONDITIONER 2

#define HISTORY_SIZE 5

#define CONTACT_CONSTRAINT_SUBTYPE 1234

  /*A contact constraint contains both non-penetration and friction
    constraints. Each individual constraint is represented by one
    vector. All the three constraints are stored in a matrix.*/

  template<int N, class T>
  class CGFAPI ContactConstraint : public AbstractRowConstraint<N, T>{
  public:
    template<class TT>
      friend class VectorC2;

    template<int M, class TT>
      friend class SpMatrixC2;

    template<int M, class TT>
      friend class IECLinSolveT2;

    template<int M, class TT>
      friend class IECLinSolveT2CR;

    static const int interval = 3;

    ContactConstraint();

    virtual void preMultiply(VectorC2<T>& r, const VectorC2<T>& x,
                             const SpMatrixC2<N, T>* mat,
                             bool transposed = false,
                             const VectorC2<T>* mask = 0)const;

    AbstractBlockConstraint<N, T>* constructBlockConstraint()const;

    RowConstraintMultiplier<N, T>* constructConstraintMultiplier()const;

    void setMu(T m);

    void setRow(const Vector4<T>& r, T fc, int i, int j, int idx);

    Vector4<T> getRow(int i, int j)const;

    T& getConstraintValue(int i);

    const T& getConstraintValue(int i)const;

    int getProjectionType(int l)const{
      if(l==0){
        return 1;
      }
      return 1000 + (int)((T)mu *(T) 1000.0);
    }

    ConstraintStatus getStatus()const;

    void loadLocalValues(const VectorC2<T>& x,
                         const VectorC2<T>& b,
                         Vector4<T>& lm, Vector4<T>& lr, Vector4<T>& lc)const;

    void forceFriction(Vector4<T>& lm,
                       Vector4<T>& lr,
                       VectorC2<T>& x,
                       VectorC2<T>& b,
                       bool* changed,
                       bool* frictionChanged,
                       EvalStats& stats);

    void evaluateAndUpdateFriction(Vector4<T>& lm,
                                   Vector4<T>& lr,
                                   VectorC2<T>& x,
                                   VectorC2<T>& b,
                                   bool* changed,
                                   bool* frictionChanged,
                                   EvalStats& stats);

    void update(VectorC2<T>& b, VectorC2<T>&x, VectorC2<T>& b2);

    bool valid(const VectorC2<T>& v)const;

    bool valid2(const VectorC2<T>& v)const;

    void destroy(VectorC2<T>& b, VectorC2<T>&x){
    }

    /*initialize constraint*/
    void init(SpMatrixC2<N, T>* owner);

    void evaluateKineticFriction(Vector4<T>& lm, Vector4<T>& lr,
                                 VectorC2<T>& x, VectorC2<T>& b,
                                 bool updateKinetic, bool updateVector,
                                 bool* changed, bool* frictionChanged,
                                 EvalStats& stats);

    void evaluateDepth(VectorC2<T>& x, VectorC2<T>& b,
                       bool evaluatePenetration,
                       Vector4<T>& lm, Vector4<T>& lr,
                       bool* penetrationViolated,
                       bool* changed,
                       bool* active, int height, int row,
                       bool removeDuplicates, EvalStats& stats);

    void forceEvaluateDepth(VectorC2<T>& x, VectorC2<T>& b,
                            bool evaluatePenetration,
                            Vector4<T>& lm, Vector4<T>& lr,
                            bool* penetrationViolated,
                            bool* changed,
                            bool* active, int height, int row,
                            bool removeDuplicates, EvalStats& stats);

    /*Evaluates Hx - b <=> lambda*/
    bool evaluate(VectorC2<T>& x, VectorC2<T>& b, T meps,
                  const EvalType& etype, EvalStats& stats, bool* active = 0,
                  VectorC2<T>* kb = 0);

    bool project(VectorC2<T>& x, VectorC2<T>& b, T meps,
                 const EvalType& etype, bool* active = 0,
                 VectorC2<T>* kb = 0);

    //void computeResidual(VectorC2<T>& r, const VectorC2<T>& x);

    void updateRHS(VectorC2<T>* kb)const;


    void multiply(VectorC2<T>& r, const VectorC2<T>& x)const;

    void multiplyTransposed(VectorC2<T>& r, const VectorC2<T>& x)const;

    void extractValues(SpMatrix<N, T>* L,
                       SpMatrix<N, T>* LT,
                       Tree<int>* columns, int row,
                       Vector<T>* rhs)const;


    void computePreconditioner(VectorC2<T>&C)const;

    void computePreconditionerMatrix(VectorC2<T>& DD,
                                     SpMatrixC2<N, T>& C,
                                     const SpMatrixC2<N, T>& B,
                                     AdjacencyList* l,
                                     VectorC2<T>* scaling = 0)const;

    void resetMultipliers(VectorC2<T>& x, Vector4<T>& oldValues)const;
    void cacheMultipliers(Vector4<T>& oldValues, VectorC2<T>& x)const;


    void print(std::ostream& os)const;
    void printIndices(std::ostream& os)const;

    void computeFBFunction(VectorC2<T>& np,
                           VectorC2<T>& sf,
                           VectorC2<T>& kf,
                           SpMatrixC2<N, T>& C,
                           const VectorC2<T>& x,
                           const VectorC2<T>& b, const VectorC2<T>& b2);

    int getIndividualSize()const{
      return this->getSize()*3;
    }

    ConstraintElement<T> getValue(int row, int col)const{
      cgfassert(row >=0 && row <= 3);

      int subMatrix = col/3;
      int matrixCol = col%3;

      ConstraintElement<T> ret;

      if(index[subMatrix] < 0){
        ret.col = -1;
        ret.value = (T)0.0;
      }else{
        ret.col = index[subMatrix]*3 + matrixCol;
        ret.value = normals[subMatrix][row][matrixCol];
        if(row != 0 && mu == (T)0.0){
          ret.value = 0.0;
        }
      }

      return ret;
    }

    virtual void collectElements(List<ConstraintElement<T> >& elements)const;

    int getIndex(int i)const;

    void setIndex(int i, int idx);

    /*Matrix containing the normals*/
    Matrix44<T> normals[5];

    /*Matrix containing the transposed normals. Note that the
      transposed and non transposed constraints are in principle their
      transpose, but when dealing with friction forces, the normal
      vector in H^T also contains entries \mu(J^T) if the friction
      constraints are inactive. This represents the friction force
      when the contact slides. The sign of these values are from the
      H^T before the constraint was deactivated. The signs only change
      when the friction constraint becomes active again and changes
      its signs. Therefore we do not have to check explicitly for the
      signs of these entries.*/
    Matrix44<T> normalsT[5];

    /*The constraint values for the associated constraints. Used for
      instance to evaluate a constraint. After initializing the
      solver, the values stored in c are copied to a particular vector
      constraint of the vector which should contain these values.*/
    Vector4<T>  c;

    T mu; /*Friction coefficient*/
    ConstraintFrictionStatus frictionStatus[2];
    T setMultipliers[3];
    T acceptedMultipliers[3];

#ifdef FRICTION_CONE
    T magnitudeHistory[HISTORY_SIZE];
#endif

    int index[5];

    mutable T zero;

    mutable T cache;
    mutable T cacheT1;
    mutable T cacheT2;

    int preconditionerType;
    mutable AbstractBlockConstraint<N, T>* blockConstraint;
#ifdef FRICTION_CONE
    T lastHV;
    T HVDiff;

    Vector4<T> kineticVector;
    Vector4<T> acceptedKineticVector;
    Vector4<T> cumulativeVector;
    Vector4<T> tangent1;
    Vector4<T> tangent2;
    Vector4<T> normal;
    RunningAverage<T, T, 25> averageNPMagnitude;
    RunningAverage<T, T, 25> averageFrictionMagnitude;
    RunningAverage<Vector4<T>, T, 25> averageFrictionDirection;
#endif
    mutable T usedScale;
  };

  template<int N, class T>
  class Compare<ContactConstraint<N, T>*>{
    typedef ContactConstraint<N, T>* CCP;
  public:
    static bool less(const CCP& a, const CCP& b){
      return a->row_id < b->row_id;
    }

    static bool equal(const CCP& a, const CCP& b){
      return a->row_id == b->row_id;
    }
  };
}

#endif/*CONTACTCONSTRAINT_HPP*/
