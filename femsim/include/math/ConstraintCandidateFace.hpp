/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTCANDIDATEFACE_HPP
#define CONSTRAINTCANDIDATEFACE_HPP

#include "math/constraints/Constraint.hpp"
#include "datastructures/DCEList.hpp"
#include "math/ConstraintCandidateStatus.hpp"
#include "math/Compare.hpp"
#include "geo/Triangle.hpp"
#include "math/ConstraintTol.hpp"
#include "math/EvalType.hpp"

#define DISTANCE_EPS2 SAFETY_DISTANCE

namespace CGF{
  template<class T>
  class ConstraintSet;

  template<class T>
  class CollisionContext;

  enum CollisionOrientation{FrontFaceCollision,
                            BackFaceCollision,
                            UndefinedCollisionOrientation};

  template<class T>
  class ConstraintCandidateFace{
  public:
    ConstraintCandidateFace(const CollisionContext<T>* n, int f, bool b);

    /*Recompute geometric properties of constraint*/
    void recompute(ConstraintSet<T>* c, CollisionContext<T>* n,
                   OrientedVertex<T>& p, Vector4<T>& com, Quaternion<T>& rot);

    /*Check if there is an intersection */
    bool checkCollision(const OrientedVertex<T>& p, const Vector4<T>& com,
                        const Quaternion<T>& rot,
                        T* dist=0, bool* plane=0);

    bool evaluate(ConstraintSet<T>* c,
                  CollisionContext<T>* n,
                  const OrientedVertex<T>& p,
                  const Vector4<T>& com,
                  const Quaternion<T>& rot);

    void updateGeometry(CollisionContext<T>* n, ConstraintSet<T>* s,
                        const OrientedVertex<T>& p, const OrientedVertex<T>& x);

    void updateConstraint(ConstraintSet<T>* c, CollisionContext<T>* n,
                          OrientedVertex<T>& x, OrientedVertex<T>& p,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          bool* skipped,
                          EvalStats& stats);

    void updateInitial(ConstraintSet<T>* c, CollisionContext<T>* n,
                       OrientedVertex<T>& x, OrientedVertex<T>& p,
                       const Vector4<T>& com,
                       const Quaternion<T>& rot,
                       bool* skipped,
                       EvalStats& stats);

    bool enableConstraint(ConstraintSet<T>* c, CollisionContext<T>* n,
                          OrientedVertex<T>& s, OrientedVertex<T>& p);

    bool disableConstraint(ConstraintSet<T>* c, CollisionContext<T>* n,
                           bool force = false, bool keepNeighbor = false);

    void forceConstraint();

    void showCandidateState(OrientedVertex<T>& xu, CollisionContext<T>* ctx);

    void updateInitialState(const OrientedVertex<T>& v,
                            const Vector4<T>& com,
                            const Quaternion<T>& rot);


    int faceId;
    int vertexId;

    bool forced;

    OrientedVertex<T> x0; /*Initial configuration of vertex x*/
    OrientedVertex<T> xi; /*Linear interpolated Vertex at ti*/
    Triangle<T>       t0; /*Initial configuration of face f*/
    Triangle<T>       tu; /*Triangle at current iteration*/
    Triangle<T>       ti; /*Linear interpolated triangle between t and tu*/

    OrientedVertex<T> enabledXU;
    Triangle<T>       enabledTU;
    OrientedVertex<T> enabledX0;
    Triangle<T>       enabledT0;

    Vector4<T>    comx0;
    Quaternion<T> rotx0;
    Vector4<T>    comt0;
    Quaternion<T> rott0;
    Vector4<T>    comtu;
    Quaternion<T> rottu;

    /*Orientation and centers of mass at moment of collision*/
    Vector4<T>    comxi;
    Quaternion<T> rotxi;
    Vector4<T>    comti;
    Quaternion<T> rotti;

    Vector4<T> baryi; /*Interpolated barycentric coordinates*/
    Vector4<T> bary2; /*Barycentric coordinates at current iteration*/

    T distance0;
    //T distance1;
    //T distance2;
    T distancec;
    T distancei; /*Distance from interpolated face to starting
                   point of vertex*/

    bool intersection0; /*Intersection state at begin of timestep*/
    bool intersectionc; /*Intersection state at current iteration*/

    int indices[3];

    /*Indices of edges creating this triangle*/
    int edgeIndices[3];

    CandidateStatus status; /*Enabled or disabled*/
    int enabledId; /*Id of constraint in solver*/

    //List<int> edgeConstraints;

    bool updated;

    bool tangentialDefined;
    Vector4<T> tangentReference[2];

    bool valid;

    Mesh::GeometryType faceType;
    Mesh::GeometryType vertexType;

#ifdef ACTIVE_CONSTRAINTS
    Vector4<T> cachedMultipliers;
#endif

    //CollisionOrientation orientation;

    bool backFace;

    bool vertexCollapsed;

    bool flag;
  };


  /*Required to make the simulation deterministic. It compares two
    candidates given its id instead of comparing their pointer values
    (default)*/
  typedef ConstraintCandidateFace<float>* CCPf;
  typedef ConstraintCandidateFace<double>* CCPd;

  template<>
  class Compare<CCPf>{
  public:
    static bool less(const CCPf& a,
                     const CCPf& b){
      //return a < b;
      return a->faceId < b->faceId;
    }

    static bool equal(const CCPf& a,
                      const CCPf& b){
      //return a == b;
      return a->faceId == b->faceId;
    }
  };

  template<>
  class Compare<CCPd>{
  public:
    static bool less(const CCPd& a,
                     const CCPd& b){
      //return a < b;
      return a->faceId < b->faceId;
    }

    static bool equal(const CCPd& a,
                      const CCPd& b){
      //return a == b;
      return a->faceId == b->faceId;
    }
  };

}

#endif/*CONSTRAINTCANDIDATEFACE_HPP*/
