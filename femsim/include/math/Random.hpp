/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef RANDOM_HPP
#define RANDOM_HPP

#include "core/cgfdefs.hpp"

namespace CGF{
  template<class T>
  class Random{
  public:
    Random();
    ~Random();
    void seed(ulong seed);
    T    genRand();
  protected:
    ulong* mt;
    int mti;
  };

  static Random<float> randomFloat;
  static Random<double> randomDouble;

  template<class T>
  T genRand();
}

#endif/*RANDOM_HPP*/
