/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef IECLINSOLVECRGEO_HPP
#define IECLINSOLVECRGEO_HPP

#include "math/constraints/ContactConstraint.hpp"
#include "math/IECLinSolveCR.hpp"
#include "math/ConstraintSet.hpp"
#include "math/ConstraintSetFace.hpp"
#include "math/IECGeo.hpp"
#include "math/constraints/GeneralConstraint.hpp"

namespace CGF{
  double frictionNorm;
  double forceNorm;

  template<int N, class T>
  class IECLinSolveT2CRGeo : public IECLinSolveT2CR<N, T>, public IECGeo<T>{
  public:
    typedef ContactConstraint<N, T>               CConstraint;
    typedef typename Tree<CConstraint*>::Iterator ConstraintTreeIterator;

    IECLinSolveT2CRGeo(int d, CollisionContext<T>* c):
      IECLinSolveT2CR<N, T>(d), IECGeo<T>(c){

      frictionScratch1 = new VectorC2<T>(d);
      frictionScratch2 = new VectorC2<T>(d);

      this->vectors.append(frictionScratch1);
      this->vectors.append(frictionScratch2);
    }

    virtual ~IECLinSolveT2CRGeo(){
      delete frictionScratch1;
      delete frictionScratch2;
    }

    /*Disables all constraints, i.e., calls the disableConstraint
      function of all candidates, which removes the constraints from
      the system.*/
    virtual void disableAllConstraints(){
      this->disableAllCandidates();
    }

    /*Checks the validity of the solution by:

      1: checking that all vertices and faces are inside the extended
      bounding volumes. If not, an additional collision check is
      required.

      2: Check if the distances of all constraints are correct, i.e.,
      that the problem is collision free.

      3: If the constraint distances are not correct, they are updated
      by slightly changing the distance.
    */
    virtual bool checkConstraints(bool friction, EvalStats& stats){
      bool changed = false;
      int nConstraintsPre = this->mat->getNConstraints();

      /*First check all constraints if they are properly resolved*/
      changed = this->checkGeometry(friction, this->bnorm, stats);

      if(changed){
        /*Update Constraint values in b*/
        IECLinSolveT2CR<N, T>::updateConstraints();
      }else{
        /*Check if all current bounding volumes are sufficient*/
        changed = this->checkBoundingVolumes();

        if(changed){
          /*Some bounding volumes were not sufficient, so new
            candidate pairs are added. These pairs must be checked
            further for possible collisions*/

          EvalType etype;
          etype.enableGeometryCheck();
          etype.enablePenetrationCheck();
          EvalStats s2;
          /*Evaluate possible new constraints*/
          this->evaluateConstraints(etype, s2);
        }
      }
      int nConstraintsApre = this->mat->getNConstraints();



      if(nConstraintsApre != nConstraintsPre){
        /*Constraints have been added, notify solver to update
          preconditioner scaling.*/
        this->constraintSetModified = true;
      }

      return changed;
    }

    /*Updates the candidate lists and updates the initial geometric
      information*/
    virtual void updateConstraints(){
      this->updateCandidateLists();
      IECLinSolveT2CR<N, T>::updateConstraints();
    }


    /*Perform an alternative update of the constraints by using the
      local geometry */
    virtual bool evaluateConstraints(const EvalType& etype, EvalStats& stats){
      bool changed = false;

      if(etype.geometryCheck()){
        /*Evaluates all candidate pairs for new collisions*/
        int nConstraintsPre = this->mat->getNConstraints();

        changed = this->evaluateCandidates(etype, stats);

        int nConstraintsApre = this->mat->getNConstraints();

        if(nConstraintsApre != nConstraintsPre){
          this->constraintSetModified = true;
        }
      }else{
        /*Evaluates existing constraints*/
        if(IECLinSolveT2CR<N, T>::evaluateConstraints(etype, stats)){
          changed = true;
        }
      }

      if(changed){
        /*Update Constraint values in b*/
        IECLinSolveT2CR<N, T>::updateConstraints();
        this->clean_iterations = 0;

        *frictionScratch1 = *this->b2;
      }
      return changed;
    }

    /*Adds a constraint to the problem and compute the preconditioner
      entries for the added constraint*/
    virtual int addConstraint(AbstractRowConstraint<N, T>* c){
      int r = IECLinSolveT2CR<N, T>::addConstraint(c);

      /*Initialize preconditioner*/
      c->computePreconditioner(*this->C);
      c->computePreconditionerMatrix(*this->C,
                                     *this->C2,
                                     *this->mat,
                                     this->constraintConnectivity,
                                     this->preconditionerScaling);
      return r;
    }

    /*Computes the scaling of the preconditioner entries of the
      constraints based on their connectivity and similarity.*/
    virtual void computePreconditionerScaling(){
      this->preconditionerScaling->clear();

      DBG(message("geo prec scaling, constraints = %d",
                  this->mat->getNConstraints()));

      for(int i=0;i<this->mat->getNConstraints();i++){
        AbstractRowConstraint<N, T>* c1 =
          (AbstractRowConstraint<N, T>*) this->mat->getActiveConstraint(i);

        T scale = (T)0.0;

        if(c1->status != Active){
          //continue;
        }

        /*Check for general constraint*/
        //if(c1->getValue(1, 0).col == -1){
        //c1->print(std::cout);
        if(c1->getSubType() == GENERAL_CONSTRAINT_SUBTYPE){
          /*This is not correct!!! Also other types of constraints can
            return a -1 column for the second row*/

          /*General constraint*/
          DBG(message("general constraint"));
          DBG(c1->print(std::cout));

          GeneralConstraint<N, T>* gc = (GeneralConstraint<N, T>*) c1;

          int maxDegree = 0;
          for(int j=0;j<gc->getSize();j++){
            int node = gc->getIndex(j);
            if(node == -1){
              warning("Is this is a constraint with an undefined index");
              c1->print(std::cout);
            }
            int localDegree = this->constraintConnectivity->getDegree(node);

            maxDegree = Max(maxDegree, localDegree);
            (*this->preconditionerScaling)[node*3] = (*this->preconditionerScaling)[node*3] + (T)1.0;

          }

          scale = (T)maxDegree;

          continue;
        }

        if(c1->getSubType() != CONTACT_CONSTRAINT_SUBTYPE){
          error("unknown subtype, %d", c1->getSubType());
        }

        int nDeformableVertices = 0;
        int nRigidObjects = 0;

#ifdef _DEBUG
        int type = -1;
#endif

        DBG(c1->print(std::cout));
        DBG(message("%d gridVertices", this->colContext->gridFile->getNGridVertices()));

        /*Check the amount of rigid and deformable vertices in the
          current constraint*/
        Tree<int> deformableVertices;
        Tree<int> rigidObjects;

        for(int j=0;j<c1->getSize();j++){
          int col = c1->getIndex(j);
          DBG(message("Column = %d, nVertices = %d", col,
                      this->colContext->gridFile->getNGridVertices()));
          if(col != Constraint::undefined){
            if(col < this->colContext->gridFile->getNGridVertices()){
              deformableVertices.uniqueInsert(col);
            }else{
              rigidObjects.uniqueInsert(col);
            }
          }
        }

        nRigidObjects = rigidObjects.size()/2;
        nDeformableVertices = deformableVertices.size();

        DBG(message("rig = %d, def = %d", nRigidObjects, nDeformableVertices));

        CConstraint* cc1 = (CConstraint*)c1;

        Tree<CConstraint*> connectedConstraints;

        connectedConstraints.uniqueInsert(cc1);

        for(int j=0;j<c1->getSize();j++){
          int col = c1->getIndex(j);
          DBG(message("col[%d] = %d", j, col));

          //T localScale = (T)0.0;


          if(col != Constraint::undefined){
            int nConnectedItems =
              this->constraintConnectivity->getDegree(col);

            DBG(message("col %d has %d connections", col, nConnectedItems));

            bool rigidObject = false;

            if(col >= this->colContext->gridFile->getNGridVertices()){
              /*Rigid object, skip tensor*/
              j++;
              rigidObject = true;
            }

            if(nRigidObjects != 0 && nDeformableVertices != 0){
              /*Rigid-deformable case */
              /*Instead of checking all the constraints that have the
                same rigid-body id (which can be a lot), search for
                the others deformable ids instead.*/

              if(rigidObject == true){
                continue;
              }
            }

            for(int k=0;k<nConnectedItems;k++){
              int connectedItem =
                this->constraintConnectivity->getConnectedItem(col, k);

              AbstractRowConstraint<N, T>* c2 =
                (AbstractRowConstraint<N, T>*) this->mat->getConstraint(connectedItem);

              if(c2->getValue(1, 0).col == -1){
                /*General constraint, skip*/
                continue;
              }

              if(nRigidObjects != 2 && nDeformableVertices == 0){
                /*Rigid-rigid case*/
                /*All ids must be equal*/
                int objectsCounted = 0;
                for(int l=0;l<c2->getSize();l++){
                  int idx = c2->getIndex(l);
                  if(rigidObjects.find(idx) != rigidObjects.end()){
                    objectsCounted++;
                  }
                }

                if(objectsCounted == 4){
                  /*Two similar object -> 4 identical ids*/
                }else{
                  continue;
                }
              }else if(nRigidObjects != 0 && nDeformableVertices != 0){
                /*Rigid-deformable*/
                /*Rigid id and at least one deformable id must be equal */
                int nFound = 0;
                for(int l=0;l<c2->getSize();l++){
                  if(deformableVertices.find(c2->getIndex(l)) !=
                     deformableVertices.end()){
                    nFound++;
                  }
                }

                if(nFound == 0){
                  continue;
                }
              }else{
                /*Deformable-deformable*/
                /*Depending on the configuration, only one identical
                  id is sufficient.*/
              }


              CConstraint* cc2 = (CConstraint*)c2;

              connectedConstraints.uniqueInsert(cc2);
            }
          }
        }

        ConstraintTreeIterator it = connectedConstraints.begin();

        DBG(message("%d Connected constraints", connectedConstraints.size()));

        int nMatches = 0;

        if(nRigidObjects != 0 && nDeformableVertices == 0){
          /*Pure rigid-rigid/static constraint*/
          //error("rigid rigid");
          DBG(type = 1);

          if(nRigidObjects == 1){
            /*Rigid object with static object, skip*/
            scale = (T)1.0;
          }else{
            /*Rigid - rigid case*/
            int objectA = c1->getIndex(0);
            int objectB = c1->getIndex(2);

            //Vector4<T> n1 = cc1->getRow(0, 0)/this->colContext->getDT();
            //Vector4<T> r1 = cc1->getRow(0, 1)/this->colContext->getDT();

            it = connectedConstraints.begin();

            while(it != connectedConstraints.end()){
              CConstraint* cc = *it++;

              if(cc->status != Active){
                //continue;
              }

              if(objectA == cc->getIndex(0) ||
                 objectA == cc->getIndex(2)){
                if(objectB == cc->getIndex(0) ||
                   objectB == cc->getIndex(2)){
                  DBG(message("found match"));
                  //TODO: measure similarity
                  //if(cc->status == Aactive){
                  /*Check similarity normals*/
#if 0
                  Vector4<T> n2, r2;
                  if(objectA == cc->getIndex(0)){
                    n2 =   cc->getRow(0, 2)/this->colContext->getDT();
                    r2 =   cc->getRow(0, 3)/this->colContext->getDT();
                  }else{
                    n2 =  cc1->getRow(0, 0)/this->colContext->getDT();
                    r2 =  cc1->getRow(0, 1)/this->colContext->getDT();
                  }

                  if( Abs(dot(n1, n2)) > 0.95){
                    //T factor = (T)1.0 - Abs((T)1.0 - Abs(dot(r1, r2))/(r1.length() * r2.length()));
                    //scale += Max(0.0, Min(factor, 1.0));
                  }else{

                  }

                  //Vector4<T> n1 =
                  //cc1->getRow(0, 0)/this->colContext->getDT();
                  //Vector4<T> n2 =
                  //cc->getRow(0, 2)/this->colContext->getDT();

                  //Vector4<T> r1 =
                  //cc1->getRow(0, 1)/this->colContext->getDT();
                  //Vector4<T> r2 =
                  //cc->getRow(0, 3)/this->colContext->getDT();
#endif
                  scale += (T)1.0;
                  //}
                  nMatches++;
                }
              }
            }
          }
        }else if(nRigidObjects != 0 && nDeformableVertices != 0){
          /*Rigid deformable case*/
          //error("rigid deformable");
          DBG(type = 2);

          /*A rigid / deformable constraint has only one rigid
            object*/
          Tree<int>::Iterator iit = rigidObjects.begin();
          int objectA = *iit++;

#ifdef _DEBUG
          int principleVertex = -1;
#endif

          DBG(c1->print(std::cout));

          DBG(message("Object = %d", objectA));

          iit = deformableVertices.begin();

          Vector4<T> reference;

          for(int j=0;j<cc1->getSize();j++){
            if(cc1->getIndex(j) == objectA){
              reference = cc1->getRow(0, j);
              reference.normalize();
              break;
            }
          }

          T beps = (T)0.1;

          int verticesA1[3] = {-1,-1,-1};

          T valuesA1[3] = {-1,-1,-1};

          int idxA1 = 0;

          T largest = (T)0.0;
          T localContrib = (T)0.0;

          //Vector4<T> currentWeights;
          //int currentVertexIndices[4];
          //int currentIndex = 0;

          for(int j=0;j<cc1->getSize();j++){
            int idx = cc1->getIndex(j);

            if(idx >= this->colContext->gridFile->getNGridVertices()){
              /*Rigid object*/
              continue;
            }

            if(idx < 0){
              continue;
            }

            DBG(message("index = %d", cc1->getIndex(j)));

            Vector4<T> normal = cc1->getRow(0, j) / this->colContext->getDT();

            localContrib = Abs(dot(normal, reference));

            DBG(message("local contrib = %10.10e", localContrib));

            //currentWeights[currentIndex] = localContrib;
            //currentVertexIndices[currentIndex] = cc1->getIndex(j);
            //currentIndex++;

            if(localContrib > largest){
              DBG(principleVertex = cc1->getIndex(j));
              largest = localContrib;
            }

            verticesA1[idxA1] = idx;
            valuesA1[idxA1] = localContrib;

            if(valuesA1[idxA1] < beps){
              verticesA1[idxA1] = -1;
            }

            idxA1++;
          }

          DBG(message("largest = %10.10e, %d", largest, principleVertex));

          /*Find combinations*/
          it = connectedConstraints.begin();

          while(it != connectedConstraints.end()){
            CConstraint* cc = *it++;

            //bool vertexFound = false;
            bool objectFound = false;

            //int found = 0;

            //T contrib = (T)0.0;

            DBG(message("%d - %d", c1->row_id, cc->row_id));

            if(cc->status != Active){
              //continue;
            }

            int verticesA2[3] = {-1,-1,-1};
            T valuesA2[3] = {-1,-1,-1};
            //int idxA2 = 0;

            for(int j=0;j<cc->getSize();j++){
              int col = cc->getIndex(j);

              if(col == objectA){
                objectFound = true;
              }

              for(int k=0;k<3;k++){
                if(verticesA1[k] == col){
                  verticesA2[k] = col;

                  Vector4<T> normal =
                    cc->getRow(0, j) / this->colContext->getDT();

                  valuesA2[k] = Abs(dot(normal, reference));

                  if(valuesA2[k] < beps){
                    verticesA2[k] = -1;
                  }
                }
              }
            }
            DBG(warning("verticesA1 = %d, %d, %d",
                        verticesA1[0], verticesA1[1], verticesA1[2]));
            DBG(warning("verticesA2 = %d, %d, %d",
                        verticesA2[0], verticesA2[1], verticesA2[2]));

            int count = 0;

            for(int i=0;i<3;i++){
              for(int j=0;j<3;j++){
                if(verticesA1[i] == verticesA2[j]){
                  count++;
                  break;
                }
              }
            }

            if(count == 3 && objectFound){
              START_DEBUG;
              warning("verticesA1 = %d, %d, %d",
                      verticesA1[0], verticesA1[1], verticesA1[2]);
              warning("verticesA2 = %d, %d, %d",
                      verticesA2[0], verticesA2[1], verticesA2[2]);

              warning("valuesA1 = %10.10e, %10.10e, %10.10e",
                      valuesA1[0], valuesA1[1], valuesA1[2]);
              warning("valuesA2 = %10.10e, %10.10e, %10.10e",
                      valuesA2[0], valuesA2[1], valuesA2[2]);
              END_DEBUG;

              T differenceA = (T)0.0;
              //int cnt = 0;
              for(int i = 0;i<3;i++){
                if(valuesA2[i] != (T)-1.0){
                  differenceA += Sqr(valuesA1[i] - valuesA2[i]);
                  //cnt++;
                }
              }

              //             differenceA /= (T)cnt;

              DBG(message("DifferenceA = %10.10e, %10.10e, %10.10e, %10.10e, %10.10e",
                       differenceA,
                       Sqrt(differenceA),
                       (T)1.0 - Sqrt(differenceA),
                       (T)1.0 - differenceA,
                       (T)1.0));

              //differenceA = Min((long double)1.0,
              //                Max(Sqrt(differenceA), (long double)0.0));

              differenceA = Clamp01((T)1.0 - Sqrt(differenceA));

              DBG(message("1 - DifferenceA^2 = %10.10e", differenceA));

              //scale += (((Max((T)0.0, Min((T)1.0 - differenceA, (T)1.0)))));
              //scale += Pow(((long double)1.0 - (differenceA)),
              //           (long double)1.0)*(long double)1.0;

              //scale += (T)1.75*Pow(((differenceA)*(T)1.0),
              //             (T)0.5) * (T)0.5;

              DBG(warning("Scale increment = %10.10e",
                          Pow(((differenceA)*(T)1.0),
                              (T)0.5) * (T)0.5));
              //scale += differenceA / (T)2.0;
              scale += differenceA;//(T)0.0;

              //getchar();
            }else{

            }
          }
        }else{// if(c1->status == Active){
          /*Deformable deformable*/
          /*Check for deformable / static case*/

          DBG(type = 3);

          Vector4<T> reference;

          for(int j=0;j<cc1->getSize();j++){
            if(cc1->getIndex(j) != Constraint::undefined){
              reference = cc1->getRow(0, j);
              reference.normalize();
              break;
            }
          }

          bool valid = false;

          for(int j=0;j<c1->getSize();j++){
            if(dot(reference, cc1->getRow(0, j)) < 0){
              /*There is an opposite part in the constraint->
                deformable - deformable case*/
              valid = true;
            }
          }

          if(valid){
            /*Figure out principle vertexA,B*/
            T beps = (T)0.1;

            int verticesA1[3] = {-1,-1,-1};
            int verticesB1[3] = {-1,-1,-1};

            T valuesA1[3] = {-1,-1,-1};
            T valuesB1[3] = {-1,-1,-1};

            int idxA1 = 0;
            int idxB1 = 0;


            DBG(int vertexA = -1);
            DBG(int vertexB = -1);

            T largestA = (T)0.0;
            T largestB = (T)0.0;

            /*Since reference is a normal vector, and normal is
              divided by dt, their dot product is the barycentric
              weight of the corresponding vertex.*/
            for(int i=0;i<c1->getSize();i++){
              if(cc1->getIndex(i) != -1){
                Vector4<T> normal =
                  cc1->getRow(0, i) / this->colContext->getDT();

                T val = dot(normal, reference);

                if(val > 0){
                  if(Abs(val) > largestA){
                    largestA = Abs(val);
                    DBG(vertexA = cc1->getIndex(i));
                  }
                  verticesA1[idxA1] = cc1->getIndex(i);
                  valuesA1[idxA1] = val;

                  if(valuesA1[idxA1] < beps){
                    verticesA1[idxA1] = -1; //check this
                  }
                  idxA1++;
                }else{
                  if(Abs(val) > largestB){
                    largestB = Abs(val);
                    DBG(vertexB = cc1->getIndex(i));
                  }
                  verticesB1[idxB1] = cc1->getIndex(i);
                  valuesB1[idxB1] = -val;

                  if(valuesB1[idxB1] < beps){
                    verticesB1[idxB1] = -1;
                  }
                  idxB1++;
                }
              }
            }

            DBG(message("vertex %d, %d, %10.10e, %10.10e",
                        vertexA, vertexB, largestA, largestB));

            ConstraintTreeIterator it = connectedConstraints.begin();

            while(it != connectedConstraints.end()){
              CConstraint* cc = *it++;

              int verticesA2[3] = {-1,-1,-1};
              int verticesB2[3] = {-1,-1,-1};

              T valuesA2[3] = {-1,-1,-1};
              T valuesB2[3] = {-1,-1,-1};

              // int idxA2 = 0;
              //int idxB2 = 0;

#if 0
              bool vertexAFound = false;
              bool vertexBFound = false;

              T valA = 0, valB = 0;

              int found = 0;
#endif
              if(cc->status != Active){
                //continue;
              }

              for(int i=0;i<cc->getSize();i++){
                int col = cc->getIndex(i);
                /*Check a*/
                for(int j=0;j<3;j++){
                  if(col == verticesA1[j]){
                    verticesA2[j] = col;
                    valuesA2[j] =
                      dot(reference, cc->getRow(0, i) /
                          this->colContext->getDT());

                    if(valuesA2[j] < beps){
                      verticesA2[j] = -1;
                    }
                  }

                  if(col == verticesB1[j]){
                    verticesB2[j] = col;
                    valuesB2[j] =
                      -dot(reference, cc->getRow(0, i) /
                           this->colContext->getDT());

                    if(valuesB2[j] < beps){
                      verticesB2[j] = -1;
                    }
                  }
                }
              }

              int count = 0;
              int countA = 0;
              int countB = 0;

              for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                  if(verticesA1[i] == verticesA2[j]){
                    count++;
                    break;
                  }
                }
              }

              for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                  if(verticesB1[i] == verticesB2[j]){
                    count++;
                    break;
                  }
                }
              }

#if 0

              message("count  = %d", count);
              message("countA = %d", countA);
              message("countB = %d", countB);

              warning("verticesA1 = %d, %d, %d",
                      verticesA1[0], verticesA1[1], verticesA1[2]);
              warning("verticesA2 = %d, %d, %d",
                      verticesA2[0], verticesA2[1], verticesA2[2]);

              warning("verticesB1 = %d, %d, %d",
                      verticesB1[0], verticesB1[1], verticesB1[2]);
              warning("verticesB2 = %d, %d, %d",
                      verticesB2[0], verticesB2[1], verticesB2[2]);

              warning("valuesA1 = %10.10e, %10.10e, %10.10e",
                      valuesA1[0], valuesA1[1], valuesA1[2]);
              warning("valuesA2 = %10.10e, %10.10e, %10.10e",
                      valuesA2[0], valuesA2[1], valuesA2[2]);

              warning("valuesB1 = %10.10e, %10.10e, %10.10e",
                      valuesB1[0], valuesB1[1], valuesB1[2]);
              warning("valuesB2 = %10.10e, %10.10e, %10.10e",
                      valuesB2[0], valuesB2[1], valuesB2[2]);
#endif
              //if(countA == 2 && countB == 2){
              if(count == 6){
                START_DEBUG;
                warning("verticesA1 = %d, %d, %d",
                        verticesA1[0], verticesA1[1], verticesA1[2]);
                warning("verticesA2 = %d, %d, %d",
                        verticesA2[0], verticesA2[1], verticesA2[2]);

                warning("verticesB1 = %d, %d, %d",
                        verticesB1[0], verticesB1[1], verticesB1[2]);
                warning("verticesB2 = %d, %d, %d",
                        verticesB2[0], verticesB2[1], verticesB2[2]);

                warning("valuesA1 = %10.10e, %10.10e, %10.10e",
                        valuesA1[0], valuesA1[1], valuesA1[2]);
                warning("valuesA2 = %10.10e, %10.10e, %10.10e",
                        valuesA2[0], valuesA2[1], valuesA2[2]);

                warning("valuesB1 = %10.10e, %10.10e, %10.10e",
                        valuesB1[0], valuesB1[1], valuesB1[2]);
                warning("valuesB2 = %10.10e, %10.10e, %10.10e",
                        valuesB2[0], valuesB2[1], valuesB2[2]);
                END_DEBUG;

                //scale += (T)1.0;
                T differenceA = (T)0.0;
                T differenceB = (T)0.0;

#if 1
                for(int i = 0;i<3;i++){
                  if(valuesA2[i] != -(T)1.0){
                    differenceA += Sqr(valuesA1[i] - valuesA2[i]);
                  }
                }

                for(int i = 0;i<3;i++){
                  if(valuesB2[i] != -(T)1.0){
                    differenceB += Sqr(valuesB1[i] - valuesB2[i]);
                  }
                }
#else
                for(int i=0;i<3;i++){
                  for(int j=0;j<3;j++){
                    if(verticesA1[i] == verticesA2[j] && verticesA1[i] != -1){
                      differenceA += Sqr(valuesA1[i] * valuesA2[j]);
                    }
                  }
                }

                for(int i=0;i<3;i++){
                  for(int j=0;j<3;j++){
                    if(verticesB1[i] == verticesB2[j] && verticesB1[i] != -1){
                      differenceB += Sqr(valuesB1[i] * valuesB2[j]);
                    }
                  }
                }
#endif
                DBG(warning("DifferenceA = %10.10le", differenceA));
                DBG(warning("DifferenceB = %10.10le", differenceB));

                //differenceA = Min((long double)1.0,
                //                Max((long double)1.0 - Sqrt(differenceA),
                //                    (long double)0.0));

                //                differenceB = Min((long double)1.0, Max((long double)1.0 - Sqrt(differenceB), (long double)0.0));

#if 1
                differenceA = Clamp01((T)1.0 - Sqrt(differenceA));
                differenceB = Clamp01((T)1.0 - Sqrt(differenceB));
#else
                differenceA = Clamp01((T)Sqrt(differenceA));
                differenceB = Clamp01((T)Sqrt(differenceB));
#endif

                //scale += (((T)1.0 - differenceA) +
                //        ((T)1.0 - differenceB))/(T)2.0;

                //long double difference = Min((long double)1.0, Max(((differenceA*differenceB)), (long double)0.0));

#if 1
                T difference = Clamp01(differenceA * differenceB);
#else
                T difference = Clamp01(differenceA + differenceB);
#endif
                //scale += Pow(((difference)*(T)0.25), (T)0.5) * (T)0.5;

                //scale += Pow(((difference)*(T)1.0),
                //             (T)0.5) * (T)0.5;
                //difference = 0.0;
                scale += difference;///(T)2.0;
                if(differenceA > (T)1.0){
                  error("difference too large");
                }
                if(differenceB > (T)1.0){
                  error("difference too large");
                }
                //scale = (T)1234.0;
                //scale += (T)1.0;
              }else if( (countA == 2 && countB == 0) ||
                        (countA == 0 && countB == 2) ){
                //scale = (T)100.25;
                //scale += (T)1234.0;
                //scale = 0.01;
                //scale += (T)1.25;
              }
            }
          }

          scale = Max(scale*(T)1.0, (T)1.0);
          scale = 1.0;
        }
        //scale *= (T)3.0;//scale;
        scale = (T)1.0;
        int cindex = c1->row_id;
        int offset = c1->getOffset();

        T* xdata = this->preconditionerScaling->getExtendedData();

        for(int k=0;k<offset;k++){
          xdata[cindex * offset + k] = (T)(Max(scale, (T)1.0));
        }

        DBG(warning("scaling = %10.10le, match = %d, type = %d",
                    scale, nMatches, type));
      }

      DBG(message("end prec scaling"));
      (std::cout << this->preconditionerScaling->getExtendedVector() << std::endl);
    }

    /*Evaluation functions at different stages*/


    virtual void evaluateStart(EvalStats& stats){
      /*Full evaluation of constraints*/
      EvalType etype;
      etype.enableGeometryCheck();
      etype.enablePenetrationCheck();
      etype.enableFrictionCheck();
      etype.enableEvaluateAllGeometry();
      etype.enableKineticFrictionUpdate();
      etype.enableKineticVectorUpdate();
      etype.enableVolumeCheck();

      etype.enableForceFriction();/*Explicitly set friction*/
      etype.enableForcePenetration();

      this->evaluateConstraints(etype, stats);
    }

    virtual bool evaluateAtConvergence(EvalStats& stats){
      /*Do not search for new collisions in geometry*/
      EvalType etype;

      etype.enableFrictionCheck();
      //etype.enableGeometryCheck();
      etype.enablePenetrationCheck();
      //etype.enableEvaluateAllGeometry();
      etype.enableKineticFrictionUpdate();
      etype.enableKineticVectorUpdate();

      etype.enableForceFriction();/*Explicitly set friction*/
      etype.enableForceVolume();

      etype.enableForcePenetration();

      return this->evaluateConstraints(etype, stats);
    }

    virtual bool evaluateAtConvergenceFinal(EvalStats& stats){
      EvalType etype;

      etype.enableFrictionCheck();
      //etype.enableGeometryCheck();
      etype.enablePenetrationCheck();
      //etype.enableEvaluateAllGeometry();
      etype.enableKineticFrictionUpdate();
      etype.enableKineticVectorUpdate();
      etype.enableForceFriction();/*Explicitly set friction*/
      etype.enableForceVolume();

      etype.enableForcePenetration();

      etype.enableGeometryCheck();
      etype.enableEvaluateAllGeometry();

      return this->evaluateConstraints(etype, stats);
    }

    virtual bool evaluateRegular(int iter, T r1, T r2, bool* lastFine,
                                 EvalStats& stats){
      EvalType etype;
      bool check = false;
      *lastFine = false;

      int interval = -1;
      T relative = (T)10.0*r1/((T)1.0*this->eps*this->bb2norm + this->eps);
      //T relative = r2/((T)(1e-6*1.0));

      interval = Max(1,
                     1*(int)Floor(Log(relative)/(3.0*Log(2.0))));

      message("evaluate with interval %d\n\n", interval);
#if 0
      if(r1 < 10.0*(this->eps*this->bb2norm + this->eps)){
        interval = 1;
      }else if(r1 < 100.0*(this->eps*this->bb2norm + this->eps)){
        interval = 3;
      }else if(r1 < 1000.0*(this->eps*this->bb2norm + this->eps)){
        interval = 6;
      }else if(r1 < 10000.0*(this->eps*this->bb2norm + this->eps)){
        interval = 12;
      }else if(r1 < 100000.0*(this->eps*this->bb2norm + this->eps)){
        interval = 24;
      }else if(r1 < 1000000.0*(this->eps*this->bb2norm + this->eps)){
        interval = 48;
      }else{
        return false;
      }
#endif
      if(iter % interval == 0){
        //if(interval <= 1){
        //if(relative < 10.0){

          etype.enableKineticVectorUpdate();
          check = true;
          *lastFine = true;
          //}
        etype.enableKineticFrictionUpdate();

        etype.enableFrictionCheck();
        //etype.enableKineticFrictionUpdate();
        check = true;
        *lastFine = true;

        etype.enablePenetrationCheck();
        etype.enableVolumeCheck();
        check = true;
        *lastFine = true;
      }

#if 0
      if(
         (r1 < 10.0*(this->eps*this->bb2norm + this->eps)
          /*||
            r2 < 10.0*(this->eps*this->bnorm + this->eps)*/)
         ){
        DBG(message("Finer check"));
#if 1
        //3
        //if(r1 < 5.0*(this->eps*this->bb2norm + this->eps)){
          if(iter%1==0 ){
            etype.enableKineticFrictionUpdate();
            etype.enableKineticVectorUpdate();
            check = true;
            *lastFine = true;
          }
          //}
#endif
        //3
        if(iter%1==0){
          etype.enableFrictionCheck();
          etype.enableKineticFrictionUpdate();
          check = true;
          *lastFine = true;
        }

        if(iter%1==0){
          etype.enablePenetrationCheck();
          etype.enableVolumeCheck();
          check = true;
          *lastFine = true;
        }
      }else{// if(r1 < (T)1e+0){
        DBG(message("Regular check"));
        //27
#if 1
#if 1//12 3 3
        //if(r1 < 100.0*(this->eps*this->bb2norm + this->eps)){
          if(iter%3==0 /*&& i > 3*/){//Larger works very well
            etype.enableKineticFrictionUpdate();
            //etype.enableKineticVectorUpdate();
            check = true;
          }
          //}
#endif
        /*It is very important to have all kinetic checks in the
          same iteration!!*/
        //9
        if(iter%3==0 /*&& i > 3*/){
          etype.enableFrictionCheck();
          check = true;
        }

        if(iter%3==0){
          etype.enablePenetrationCheck();
          etype.enableVolumeCheck();
          check = true;
        }
#endif
      }
#endif
      if(check){
        currentBNorm = computeRHSNorm();

        this->b2->clear();
        return evaluateConstraints(etype, stats);
      }
      return false;
    }

    /*When a state has changed, all kinetic friction constraints
      update the RHS stored in b2.*/
    virtual void evaluateAccept(){
      EvalType etype;
      EvalStats stats;
      etype.enableAcceptFriction();
      //etype.enableFrictionCheck();
      //etype.enablePenetrationCheck();
      this->evaluateConstraints(etype, stats);
      *this->ab2 = *this->b2;
    }

  protected:
    T computeRHSNorm(){
      return 1000000000.0;
      T sum = 0;
      int height = this->mat->getHeight();

      spmvc(*frictionScratch1, *this->mat, *this->x, ConstraintsCol);
      VectorC2<T>::sub(*frictionScratch1, *frictionScratch1, *this->b2);

      for(int i=0;i<height;i++){
        sum += Sqr((*frictionScratch1)[i]) / (*this->mat)[i][i];
        //forceSum += Sqr((*this->b)[i]) / (*this->mat)[i][i];
      }

      DBG(message("B2 norm = %10.10e", sum));

      frictionNorm = sum;
      //forceNorm = forceSum;

      return sum;
    }

    T computeFrictionDifference(){
      return 0;
    }

    T currentBNorm;

    VectorC2<T>* frictionScratch1;
    VectorC2<T>* frictionScratch2;
  };
}

#endif/*IECLINSOLVECRGEO_HPP*/
