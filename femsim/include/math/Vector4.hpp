/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef VECTOR4_H
#define VECTOR4_H

#if defined SSE2
#include "math/x86-sse/sse_vector_intrinsics.hpp"
using namespace CGF::x86_sse2;
#elif defined NEON
#include "math/arm-neon/neon_vector_intrinsics.hpp"
using namespace CGF::arm_neon;
#else
#include "math/default/default_vector_intrinsics.hpp"
using namespace CGF::default_proc;
#endif

#ifndef __ANDROID__
#include <iostream>
#endif
#include "core/cgfdefs.hpp"
#include "math/Math.hpp"
#include "math/Compare.hpp"

/*todo: use _mm_shuf_ps intrinsic for dotproduct /reduction*/

namespace CGF
{
  //static const float notANumber = sqrt(-1);
  //static const float notANumberVec4[4] = {notANumber, notANumber, notANumber, notANumber};

  static const float zeroVec4[4] = {0,0,0,0};

  template<class T>
  class CGFAPI Matrix44;

  template<class T>
  class CGFAPI Vector4
  {
  public:
#ifdef _WIN32
	__declspec(align(16)) T m[4];
#else
    T m[4] __attribute__ ((aligned (16)));
#endif

    //static const Vector4<T> Zero;

  public:
    Vector4(){
      vector4_load_zero(m);
#if defined SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }

    Vector4(const Vector4<T>& v){
      vector4_load(m, v.m);
#ifdef SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }

    template<class Y>
      Vector4(const Vector4<Y>& v){
      m[0] = (T)v.m[0];
      m[1] = (T)v.m[1];
      m[2] = (T)v.m[2];
      m[3] = (T)v.m[3];
#ifdef SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }

#if 0
    Vector4(const T v[]){
      vector4_load(m, v);
#ifdef SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }
#endif

    Vector4(const T xx, const T yy,
            const T zz, const T ww){
      vector4_load(m, xx, yy, zz, ww);
#ifdef SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }

    template<class U, class V, class W, class X>
      Vector4(const U xx, const V yy,
              const W zz, const X ww){
      vector4_load(m, (T)xx, (T)yy, (T)zz, (T)ww);
#ifdef SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }

#if 0
    Vector4(const T x){
      vector4_load(m, x);
#ifdef SSE2
      cgfassert(alignment(m, 16));   /*Check alignment*/
#endif
    }
#endif

    T& operator[](int i){
      ///return m[i];

      if(i>=0 && i<4){
        return m[i];
      }else{
        error("Vector4::operator[] out of bounds %d", i);
      }
      return m[0];
    }

    const T& operator[](int i)const{
      //return m[i];

      if(i>=0 && i<4){
        return m[i];
      }else{
        error("Vector4::operator[] out of bounds %d", i);
      }
      return m[0];
    }

    Vector4<T>& operator=(const Vector4<T>& v){
      if(&v != this){
        vector4_load(m, v.m);
      }
      return *this;
    }

    Vector4<T>& operator=(const T f){
      error("assigning scalar to vector");
      return *this;
    }

    Vector4<T>& set(const T f){
      vector4_load(m, f);
      return *this;
    }

    Vector4<T>& set(const Vector4<T>& v){
      if(&v != this){
        vector4_load(m, v.m);
      }
      return *this;
    }

    Vector4<T>& set(const T v[]){
      vector4_load(m, v);
      return *this;
    }

    Vector4<T>& set(T xx, T yy, T zz, T ww){
      vector4_load(m, xx, yy, zz, ww);
      return *this;
    }

    template<class U, class V, class W, class X>
      Vector4<T>& set(U xx, V yy, W zz, X ww){
      vector4_load(m, (T)xx, (T)yy, (T)zz, (T)ww);
      return *this;
    }

    Vector4<T>& operator*=(T n){
      vector4_muls(m, m, n);
      return *this;
    }

    Vector4<T>& operator/=(T n){
      vector4_divs(m, m, n);
      return *this;
    }

    Vector4<T>& operator+=(const Vector4<T>& v){
      vector4_add(m, m, v.m);
      return *this;
    }

    Vector4<T>& operator-=(const Vector4<T>& v){
      vector4_sub(m, m, v.m);
      return *this;
    }

    /*Conversion*/
    operator T*(){return m;}
    operator const T*() const {return m;}

    /*Unary*/
    Vector4<T> operator+() const {
      return *this;
    }
    Vector4<T> operator-() const {
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_muls(rr, m, -MultiplyIdentity(T()));
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    /*Vector vector*/
    Vector4<T> operator+(const Vector4<T>& v) const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_add(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> operator-(const Vector4<T>&v) const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_sub(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    /*Vector matrix*/
    Vector4<T> operator*(const Matrix44<T>& m) const;

    /*Scaling*/
    template<class Y>
      friend inline Vector4<Y> operator*(const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator*(Y n, const Vector4<Y>& a);
    template<class Y>
      friend inline Vector4<Y> operator/(const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator/(Y n, const Vector4<Y>& a);

    /*Element wise multiplication*/
    static Vector4<T> emul(const Vector4<T>& a, const Vector4<T>& b){
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_mul(rr, a.m, b.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    /*Element wise division*/
    static Vector4<T> ediv(const Vector4<T>& a, const Vector4<T>& b){
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_div(rr, a.m, b.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> emul(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_mul(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> ediv(const Vector4<T>& v){
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_div(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T>& emulassign(const Vector4<T>& v){
      vector4_mul(m, m, v.m);
      return *this;
    }

    Vector4<T> edivassign(const Vector4<T>& v){
      vector4_div(m, m, v.m);
      return *this;
    }

#if 0
    /*Zero test*/
    bool operator!() const{
      return (m[0] == (T)0.0 &&
              m[1] == (T)0.0 &&
              m[2] == (T)0.0 &&
              m[3] == (T)0.0);
    }
#else
    bool operator!() const{
      error("called?");
      return false;
    }
#endif

    Matrix44<T> outherProduct(const Vector4<T>& v)const{
      Matrix44<T> r(m[0] * v,
                    m[1] * v,
                    m[2] * v,
                    m[3] * v);

      return r;
    }

    Matrix44<T> directProduct(const Vector4<T>& v)const;
    /*{
      Matrix44f r(v, v, v, v);
      r.m[0]*=m[0];
      r.m[1]*=m[1];
      r.m[2]*=m[2];
      r.m[3]*=m[3];
      return r;
      }*/

    Vector4<T> cwEqual(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_eq(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> cwNotEqual(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_neq(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Matrix44<T> toSkewCross()const{
      Matrix44<T> ret;
      ret[0][1] = -m[2];
      ret[1][0] =  m[2];
      ret[0][2] =  m[1];
      ret[2][0] = -m[1];
      ret[1][2] = -m[0];
      ret[2][1] =  m[0];
      ret[3][3] = MultiplyIdentity(T());
      return ret;
    }

#if 0
    /*Equality tests*/
    Vector4<T> operator==(const Vector4<T>& v)const{
      Vector4<T> r;
      vector4_eq(r.m, m, v.m);
      return r;
    }

    Vector4<T> operator!=(const Vector4<T>& v)const{
      Vector4<T> r;
      vector4_neq(r.m, m, v.m);
      return r;
    }


    template<class Y>
      friend inline Vector4<Y> operator==(const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator!=(const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator==(Y n, const Vector4<Y>& a);
    template<class Y>
      friend inline Vector4<Y> operator!=(Y n, const Vector4<Y>&a);
#endif

    template<class Y>
      friend inline Vector4<Y> cross(const Vector4<Y>& a, const Vector4<Y>& b);

    template<class Y>
      friend inline Y dot(const Vector4<Y>& a, const Vector4<Y>& b);

    /*Inequality tests*/
    Vector4<T> operator<(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_lt(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> operator<=(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_le(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> operator>(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_gt(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    Vector4<T> operator>=(const Vector4<T>& v)const{
#ifdef _WIN32
      __declspec(align(16)) T rr[4];
#else
      T rr[4] __attribute__ ((aligned (16)));
#endif
      vector4_ge(rr, m, v.m);
      return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
    }

    template<class Y>
      friend inline Vector4<Y> operator < (const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator <=(const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator > (const Vector4<Y>& a, Y n);
    template<class Y>
      friend inline Vector4<Y> operator >=(const Vector4<Y>& a, Y n);

    template<class Y>
      friend inline Vector4<Y> operator < (Y n, const Vector4<Y>& a);
    template<class Y>
      friend inline Vector4<Y> operator <=(Y n, const Vector4<Y>& a);
    template<class Y>
      friend inline Vector4<Y> operator > (Y n, const Vector4<Y>& a);
    template<class Y>
      friend inline Vector4<Y> operator >=(Y n, const Vector4<Y>& a);

    bool isTrue() const{
      return(m[0] == MultiplyIdentity(T()) &&
             m[1] == MultiplyIdentity(T()) &&
             m[2] == MultiplyIdentity(T()) &&
             m[3] == MultiplyIdentity(T()));
    }

    bool isFalse() const{
      return(m[0] == SumIdentity(T()) &&
             m[1] == SumIdentity(T()) &&
             m[2] == SumIdentity(T()) &&
             m[3] == SumIdentity(T()) );
    }

    /*Sum*/
    T sum() const{
      return vector4_sum(m);
    }

    /*Length*/
    T length2() const{
      return vector4_dot(m, m);
    }

    T length() const{
      return Sqrt(vector4_dot(m, m));
    }

    /*Clamp*/
    Vector4<T>& clamp(T lo, T hi){
      m[0] = m[0] < lo ? lo : m[0] > hi ? hi : m[0];
      m[1] = m[1] < lo ? lo : m[1] > hi ? hi : m[1];
      m[2] = m[2] < lo ? lo : m[2] > hi ? hi : m[2];
      m[3] = m[3] < lo ? lo : m[3] > hi ? hi : m[3];
      return *this;
    }

    T reduceMax()const{
      return vector4_reduce_max(m);
    }

    T reduceMin()const{
      return vector4_reduce_min(m);
    }

    /*Max min*/
	template<class Y>
      friend inline void max(Vector4<Y>& r,
                             const Vector4<Y>& a, const Vector4<Y>& b);
    template<class Y>
      friend inline void min(Vector4<Y>& r,
                             const Vector4<Y>& a, const Vector4<Y>& b);

    /*Lo hi*/
    template<class Y>
      friend inline Vector4<Y> lo(const Vector4<Y>& a, const Vector4<Y>& b);
    template<class Y>
      friend inline Vector4<Y> hi(const Vector4<Y>& a, const Vector4<Y>& b);

    /*Normal from three points*/
    template<class Y>
      friend Vector4<T> normal(const Vector4<Y>& a, const Vector4<Y>& b,
                               const Vector4<Y>& c);

    /*Normalize vector*/
    Vector4<T>& normalize(){
      *this /= length();
      return *this;
    }

    bool operator == (const Vector4<T>& v)const{
      for(int i=0;i<4;i++){
        if(m[i] != v.m[i]) return false;
      }
      return true;
    }

    bool operator != (const Vector4<T>& v)const{
      return !((*this) == v);
    }

    /*
    operator bool (){
      error("Implicit conversion");
      return false;
      }*/


    template<class Y>
      friend Vector4<Y> norm(const Vector4<Y>& v);

#ifndef __ANDROID__
    template<class Y>
      friend inline std::ostream& operator<<(std::ostream& os,
                                             const Vector4<Y>& v);
#endif
  };


  template<class T>
  inline Vector4<T> operator*(const Vector4<T>& a, T n){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    vector4_muls(rr, a.m, n);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator*(T n, const Vector4<T>& a){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    vector4_muls(rr, a.m, n);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator/(const Vector4<T>& a, T n){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    vector4_divs(rr, a.m, n);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator/(T n, const Vector4<T>& a){
    Vector4<T> r;
    error("used?");
    return r;
  }

#if 0
  template<class T>
  inline Vector4<T> operator==(const Vector4<T>& a, T f){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    Vector4<T> b(f, f, f, f);
    vector4_eq(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator!=(const Vector4<T>& a, T f){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    Vector4<T> b(f, f, f, f);
    vector4_neq(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }
#endif

  template<class T>
  inline Vector4<T> operator<(const Vector4<T>& a, T f){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    Vector4<T> b(f, f, f, f);
    vector4_lt(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator<=(const Vector4<T>& a, T f){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    Vector4<T> b(f, f, f, f);
    vector4_le(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator>(const Vector4<T>& a, T f){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    Vector4<T> b(f, f, f, f);
    vector4_gt(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator>=(const Vector4<T>& a, T f){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    Vector4<T> b(f, f, f, f);
    vector4_ge(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline Vector4<T> operator <(T f, const Vector4<T>& a){
    return a > f;
  }

  template<class T>
  inline Vector4<T> operator <=(T f, const Vector4<T>& a){
    return a >= f;
  }

  template<class T>
  inline Vector4<T> operator >(T f, const Vector4<T>& a){
    return a < f;
  }

  template<class T>
  inline Vector4<T> operator >=(T f, const Vector4<T>& a){
    return a <= f;
  }

  template<class T>
  inline Vector4<T> lo(const Vector4<T>& a, const Vector4<T>& b){
    return Vector4<T>(MIN(a.m[0], b.m[0]), MIN(a.m[1], b.m[1]),
                      MIN(a.m[2], b.m[2]), MIN(a.m[3], b.m[3]));
  }

  template<class T>
  inline Vector4<T> hi(const Vector4<T>& a, const Vector4<T>& b){
    return Vector4<T>(MAX(a.m[0], b.m[0]), MAX(a.m[1], b.m[1]),
                      MAX(a.m[2], b.m[2]), MAX(a.m[3], b.m[3]));
  }

  template<class T>
  extern Vector4<T> normal(const Vector4<T>& a, const Vector4<T>& b,
                           const Vector4<T>& c);

  template<class T>
  extern Vector4<T> norm(const Vector4<T>& v);

  template<class T>
  inline Vector4<T> cross(const Vector4<T>& a, const Vector4<T>& b){
#ifdef _WIN32
	__declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    vector4_cross(rr, a.m, b.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  template<class T>
  inline T dot(const Vector4<T>& a, const Vector4<T>& b){
    return vector4_dot(a.m, b.m);
  }

#ifndef __ANDROID__
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const Vector4<T>& v){
    os << std::scientific;
    os.precision(10);
    os << v.m[0] << '\t' << v.m[1] << '\t' << v.m[2] << '\t' << v.m[3] << std::endl;
    return os;
  }
#endif

  /*Max min*/
  template<class T>
  inline void max(Vector4<T>& r, const Vector4<T>& a, const Vector4<T>& b){
    vector4_max(r.m, a.m, b.m);
  }

  template<class T>
  inline void min(Vector4<T>& r, const Vector4<T>& a, const Vector4<T>& b){
    vector4_min(r.m, a.m, b.m);
  }

  template<class T>
  inline Vector4<T> slexp(const Vector4<T>& va, const Vector4<T>& vc, T t){
    Vector4<T> a = va;
    Vector4<T> c = vc;

    if(Sqr(t) < (T)1E-6){
      error("t = %f", t);
    }

    a.normalize();
    c.normalize();

    T costheta = dot(a, c);

    if(Sqr(Abs(costheta) - (T)1.0 ) < (T)1E-6){
      return vc/t - ((T)1.0 - t) * va / t;
    }

    T theta = ArcCos(costheta)/t;

    return (T)(Sin(theta) / Sin(theta * t)) * vc -
      (T)(Sin(((T)1.0 - t)*theta) / Sin(t * theta)) * va;
  }

  template<class T>
  inline Vector4<T> slerp(const Vector4<T>& va, const Vector4<T>& vb, T t){
    cgfassert(t >= (T)0 && t <= (T)1);
    Vector4<T> a = va;
    Vector4<T> b = vb;

    //float la = a.length();
    //float lb = b.length();

    //a/=la;
    //b/=lb;

    a.normalize();
    b.normalize();

    T costheta = dot(a, b);

    if(Sqr(Abs(costheta) - (T)1.0) < (T)1E-6){
      /*va is a linear combination of vb. Angle between both vectors
        is zero, so cos(theta) is 1 -> theta = 0 -> sin(theta) = 0,
        which results in a division by zero, therefore, it is safe to
        do a simple linear interpolation of vector a and b.*/
      return ((T)1.0 - t) * va + t * vb;
    }

    T theta = ArcCos(costheta);
    T sintheta = Sin(theta);

    return ((T)(Sin(((T)1.0 - t) * theta) / sintheta) * va +
            (T)(Sin(t * theta) / Sin(theta)) * vb);//*((1.0-t)*la + (t)*lb);
  }

  template<class T>
  inline Vector4<T> normal(const Vector4<T>& a, const Vector4<T>& b,
                           const Vector4<T>& c){
    return norm((b-a)^(c-a));
  }

  template<class T>
  inline Vector4<T> norm(const Vector4<T>& vec){
    T t = vec.length();
    if(t>(T)0.0){
      return vec/t;
    }
    return Vector4<T>(0,0,0,0);
  }

  template<class T>
  inline Matrix44<T> Vector4<T>::directProduct(const Vector4<T>& v)const{
    Matrix44<T> r(v*m[0], v*m[1], v*m[2], v*m[3]);
    /*r.m[0]*=m[0];
    r.m[1]*=m[1];
    r.m[2]*=m[2];
    r.m[3]*=m[3];*/
    return r;
  }

  template<class T>
  inline Vector4<T> Vector4<T>::operator*(const Matrix44<T>& mm) const{
    error("?");
    return Vector4<T>(vector4_dot(m, mm[0].m),
                      vector4_dot(m, mm[1].m),
                      vector4_dot(m, mm[2].m),
                      vector4_dot(m, mm[3].m));
  }

  /*Creates an orthogonal frame in which v is one axis of the frame.
    The other axis are orthogonal to v and each other. The orientation
    will be arbitrary.
   */
  template<class T>
  inline void createArbitraryOrthogonalFrame(const Vector4<T>& v,
                                             Vector4<T>* n,
                                             Vector4<T>* t1,
                                             Vector4<T>* t2){
    *n = v;
    n->normalize();

    *t1 = *n;

    int largestComponent = -1;
    T largestValue = (T)0.0;
    for(int i=0;i<3;i++){
      if(Abs((*t1)[i]) > largestValue){
        largestValue = Abs((*t1)[i]);
        largestComponent = i;
      }
    }

    (*t1)[largestComponent] *= -(T)1.0;
    (*t1)[(largestComponent+1)%3] += (T)1.0;

    *t1 = cross(*t1, *n);
    t1->normalize();
    *t2 = cross(*t1, *n);
    t2->normalize();
  }

  /*Creates an orthogonal frame in which v is one of the axis and t1
    is aligned using w*/
  template<class T>
  inline void createAlignedOrthogonalFrame(const Vector4<T>& v,
                                           const Vector4<T>& w,
                                           Vector4<T>* n,
                                           Vector4<T>* t1,
                                           Vector4<T>* t2){
    *n = *v;
    n->normalize();

  }




  template<class T>
  class Compare<Vector4<T> >{
  public:
    static bool less(const Vector4<T>& a, const Vector4<T>& b){
      if(equal(a,b)){
        //return false;
      }

      if(Sqr(a.m[0] - b.m[0])/*Sqr(a.m[0])*/ < 1E-16){
        if(Sqr(a.m[1] - b.m[1])/*Sqr(a.m[1])*/ < 1E-16){
          if(Sqr(a.m[2] - b.m[2])/*Sqr(a.m[2])*/ < 1E-16){
            if(Sqr(a.m[3] - b.m[3])/*Sqr(a.m[3])*/ < 1E-16){
              return false;
            }else if(a.m[3] < b.m[3]){
              return true;
            }
          }else if(a.m[2] < b.m[2]){
            return true;
          }
        }else if(a.m[1] < b.m[1]){
          return true;
        }
      }else if(a.m[0] < b.m[0]){
        return true;
      }
      return false;

      //return less(a, b);
    }

    static bool equal(const Vector4<T>& a, const Vector4<T>& b){
#if 0
      if(Sqr(a.m[0] - b.m[0])/Sqr(a.m[0]) < 1E-16){
        if(Sqr(a.m[1] - b.m[1])/Sqr(a.m[1]) < 1E-16){
          if(Sqr(a.m[2] - b.m[2])/Sqr(a.m[2]) < 1E-16){
            if(Sqr(a.m[3] - b.m[3])/Sqr(a.m[3]) < 1E-16){
              return true;
            }
          }
        }
      }
#else
      if((a - b).length2() < 1E-16){
        return true;
      }
#endif
      return false;
      //return equal(a, b);
    }
  };

  template<class T>
  inline bool IsNan(const Vector4<T>& v){
    return IsNan(v[0]) || IsNan(v[1]) || IsNan(v[2]) || IsNan(v[3]);
  }

  typedef Vector4<float> Vector4f;
  typedef Vector4<double> Vector4d;

  //typedef typename Vector4<float> Vector4f;
  //typedef typename Vector4<double> Vector4d;
}

#endif/*VECTOR_4*/
