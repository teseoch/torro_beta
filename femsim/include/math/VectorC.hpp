/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef VECTORC_HPP
#define VECTORC_HPP

#include "math/Vector.hpp"
#include "math/SpMatrixC.hpp"
#include "datastructures/Tree.hpp"
#include "math/constraints/Constraint.hpp"

namespace CGF{

  template<class T=float>
  class CGFAPI VectorC2 : public Vector<T>{
  public:
    VectorC2(int size=0):Vector<T>(size){
      constrVec = new Vector<T>(1); /*Default size*/
    }

    VectorC2(const VectorC2<T>& v):Vector<T>(v){
      constrVec = new Vector<T>(*v.constrVec);
    }

    virtual ~VectorC2(){
      if(constrVec){
        delete constrVec;
      }
    }

    /*Assignment operators*/
    VectorC2<T>& operator=(const VectorC2<T>& v){
      if(this == &v){
        /*Self assignment*/
        return *this;
      }else{
        Vector<T>::operator=(v);
        (*constrVec) = (*v.constrVec);
      }
      return *this;
    }

    T getAbsMax()const{
      return Max(Vector<T>::getAbsMax(), constrVec->getAbsMax());
    }

    T getOAbsMax()const{
      return Vector<T>::getAbsMax();
    }

    T getCAbsMax()const{
      return constrVec->getAbsMax();
    }

    T oSum() const{
      return Vector<T>::sum();
    }

    T cSum() const{
      return constrVec->sum();
    }

    T sum() const{
      return oSum() + cSum();
    }

    T sump(const VectorRange r)const{
      error("Not implemented for enriched vectors");
      return Vector<T>::sump(r);
    }

    T& operator[](int i){
      if(i < this->getSize()){
        return Vector<T>::operator[](i);
      }

      int index = i - this->getSize();

      return (*constrVec)[index];
    }

    const T operator[](int i)const{
      if(i < this->getSize()){
        return Vector<T>::operator[](i);
      }

      int index = i - this->getSize();

      return (*constrVec)[index];
    }

    void clear(VectorOpMode mode = Normal){
      if(mode == Normal || mode == NoConstraints){
        Vector<T>::clear();
      }

      if(mode == Normal || mode == Constraints){
        constrVec->clear();
      }
    }

    void truncate(){
      for(int i=0;i<constrVec->getSize();i++){
        if((*constrVec)[i] < 0){
          (*constrVec)[i]  = 0;
        }
      }
    }

    void reset(){
      constrVec->clear();
    }

    T* getExtendedData(){
      return constrVec->getData();
    }

    const T* getExtendedData() const{
      return constrVec->getData();
    }

    const Vector<T>& getExtendedVector()const{
      return *constrVec;
    }

    Vector<T>& getExtendedVector(){
      return *constrVec;
    }

    VectorC2<T>& operator=(const T f){
      Vector<T>::operator=(f);
      (*constrVec) = f;

      return *this;
    }

    VectorC2<T>& operator*=(T n){
      VectorC2<T>::mulf(*this, *this, n);
      return *this;
    }

    VectorC2<T>& operator/=(T n){
      VectorC2<T>::mulf(*this, *this, (T)1.0/n);
      return *this;
    }

    VectorC2<T>& operator+=(const VectorC2<T>& v){
      VectorC2<T>::add(*this, *this, v);
      return *this;
    }

    VectorC2<T>& operator-=(const VectorC2<T>& v){
      VectorC2<T>::sub(*this, *this, v);
      return *this;
    }

    VectorC2<T>& operator*=(const VectorC2<T>& v){
      VectorC2<T>::mul(*this, *this, v);
      return *this;
    }

    VectorC2<T>& operator/=(const VectorC2<T>& v){
      VectorC2<T>::div(*this, *this, v);
      return *this;
    }

   /*Unary*/
    VectorC2<T> operator+() const{
      VectorC2<T> r(*this);
      return r;
    }

    VectorC2<T> operator-() const{
      VectorC2<T> r(*this);
      r *= -1.0f;
      return r;
    }

    void printConstraints(std::ostream& os)const;

    /*Vector vector*/
    VectorC2<T> operator+(const VectorC2<T>& v) const{
      cgfassert(this->getSize() == v.getSize());
      VectorC2<T> r(*this);
      r += v;
      return r;
    }

    VectorC2<T> operator-(const VectorC2<T>& v) const{
      cgfassert(this->getSize() == v.getSize());
      VectorC2<T> r(*this);
      r -= v;
      return r;
    }


    template<class U>
      friend VectorC2<U> operator*(const VectorC2<U>& a, U n);
    template<class U>
      friend VectorC2<U> operator*(U n, const VectorC2<U>& a);
    template<class U>
      friend VectorC2<U> operator/(const VectorC2<U>& a, U n);
    template<class U>
      friend VectorC2<U> operator/(U n, const VectorC2<U>& a);

    /*Dot product*/
    T operator*(const VectorC2<T>& v) const{
      T a = Vector<T>::operator*(v);
      T b = (*constrVec) * (*v.constrVec);
      return a+b;
    }

    /*Cross product*/
    VectorC2<T> operator^(const VectorC2<T>& v) const{
      VectorC2<T> r(*this);
      error("Not implemented yet");
      return r;
    }

    /*Vector matrix multiplication*/
    template<int M, class U>
      friend VectorC2<U> operator*(const SpMatrixC2<M, U>& m,
                                   const VectorC2<U>& v);
    template<int M, class U>
      friend void spmvc(VectorC2<U>& r, const SpMatrixC2<M, U>& m,
                        const VectorC2<U>& v, const MatrixMulMode mode,
                        const VectorC2<U>* mask);

    template<int M, class U>
      friend void spmv_partialc(VectorC2<U>& r, const SpMatrixC2<M, U>& m,
                                const VectorC2<U>& v, const MatrixRange mr,
                                const MatrixMulMode mode,
                                const VectorC2<U>* mask);

    template<class U>
      friend std::ostream& operator<<(std::ostream& os,
                                      const VectorC2<U>& v);

    static void sub  (VectorC2<T>& r, const VectorC2<T>& a,
                      const VectorC2<T>& b,
                      const VectorOpMode mode = Normal);

    static void add  (VectorC2<T>& r, const VectorC2<T>& a,
                      const VectorC2<T>& b, const VectorOpMode mode = Normal);

    static void madd (VectorC2<T>& r, const VectorC2<T>& a,
                      const VectorC2<T>& b, const VectorC2<T>& c,
                      const VectorOpMode mode = Normal);

    static void mfadd(VectorC2<T>& r, T f, const VectorC2<T>& b,
                      const VectorC2<T>& c, const VectorOpMode mode = Normal);

    static void mfadd2(VectorC2<T>& r, T f, const VectorC2<T>& b,
                       const VectorC2<T>& c, const VectorC2<T>& d,
                       const VectorOpMode mode = Normal);

    static void mul  (VectorC2<T>& r, const VectorC2<T>& a,
                      const VectorC2<T>& b, const VectorOpMode mode = Normal);

    static void mulf (VectorC2<T>& r, const VectorC2<T>& a, T f,
                      const VectorOpMode mode = Normal);

    static void div  (VectorC2<T>& r, const VectorC2<T>& a,
                      const VectorC2<T>& b, const VectorOpMode mode = Normal);

    /*Partial operations*/
    static void subp  (VectorC2<T>& r, const VectorC2<T>& a,
                       const VectorC2<T>& b, const VectorRange rg,
                       const VectorOpMode mode = Normal);

    static void addp  (VectorC2<T>& r, const VectorC2<T>& a,
                       const VectorC2<T>& b, const VectorRange rg,
                       const VectorOpMode mode = Normal);

    static void maddp (VectorC2<T>& r, const VectorC2<T>& a,
                       const VectorC2<T>& b, const VectorC2<T>& c,
                       const VectorRange rg, const VectorOpMode mode = Normal);

    static void mfaddp(VectorC2<T>& r, T f, const VectorC2<T>& b,
                       const VectorC2<T>& c, const VectorRange rg,
                       const VectorOpMode mode = Normal);

    static void mfadd2p(VectorC2<T>& r, T f, const VectorC2<T>& b,
                        const VectorC2<T>& c, const VectorC2<T>& d,
                        const VectorRange rg, const VectorOpMode mode = Normal);

    static void mulp  (VectorC2<T>& r, const VectorC2<T>& a,
                       const VectorC2<T>& b, const VectorRange rg,
                       const VectorOpMode mode = Normal);

    static void mulfp (VectorC2<T>& r, const VectorC2<T>& a, T f,
                       const VectorRange rg, const VectorOpMode mode = Normal);

    void extendConstraints(int csize){
      if(csize <= constrVec->getSize()){
        return;
      }

      Vector<T>* tmp = new Vector<T>(csize);
      tmp->clear();

      memcpy(tmp->getData(), constrVec->getData(),
             sizeof(T)*(unsigned long int)constrVec->getSize());

      delete constrVec;

      constrVec = tmp;
    }

    Vector<T>* constrVec;
  };

#define VECTOR_C_CV

  template<class T>
  VectorC2<T> operator*(const VectorC2<T>& a, T n){
    VectorC2<T> r = a;
    r *= n;
    return r;
  }

  template<class T>
  VectorC2<T> operator*(T n, const VectorC2<T>& a){
    VectorC2<T> r = a;
    r *= n;
    return r;
  }

  template<class T>
  VectorC2<T> operator/(const VectorC2<T>& a, T n){
    VectorC2<T> r = a;
    r *= n;
    return r;
  }

  template<class T>
  VectorC2<T> operator/(T n, const VectorC2<T>& a){
    VectorC2<T> r = a;
    r *= n;
    return r;
  }

  template<class T>
  bool operator==(const VectorC2<T>& a, T n){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator!=(const VectorC2<T>& a, T n){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator==(T n, const VectorC2<T>& a){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator!=(T n, const VectorC2<T>& a){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator<(const VectorC2<T>& a, T n){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator<=(const VectorC2<T>& a, T n){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator>(const VectorC2<T>& a, T n){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator>=(const VectorC2<T>& a, T n){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator<(T n, const VectorC2<T>& a){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator<=(T n, const VectorC2<T>& a){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator>(T n, const VectorC2<T>& a){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  bool operator>=(T n, const VectorC2<T>& a){
    error("Not implemented yet");
    return false;
  }

  template<class T>
  VectorC2<T> lo(const VectorC2<T>& a, VectorC2<T>& b){
    error("Not implemented yet");
    return a;
  }

  template<class T>
  VectorC2<T> hi(const VectorC2<T>& a, VectorC2<T>& b){
    error("Not implemented yet");
    return a;
  }

#if 1
  template<int N, class T>
  VectorC2<T> operator*(const SpMatrixC2<N, T>& m, const VectorC2<T>& v){
    if(!m.isFinalized()){
      error("Matrix is not finalized");
    }

    VectorC2<T> r(v);
    r *= 0;

    spmvc(r, m, v);

    return r;
  }
#endif

  template<class T>
  std::ostream& operator<<(std::ostream& stream, const VectorC2<T>& v){
    stream << "vector size = " << v.getPaddedSize() << " " << v.getSize() << "," << v.constrVec->getSize() << std::endl;
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);

    for(int i=0;i<v.getSize();i++){
      stream << i << "\t" << v.data[i] << "\n";
    }

    stream << "------------ Constraint part ------------" << std::endl;

    for(int i=0;i<v.constrVec->getSize();i++){
      stream << i+v.getSize() << "\t" << (*v.constrVec)[i] << "\n";
    }

    stream << std::endl;
    stream.flags(origflags);
    return stream;
  }

  template<class T>
  void VectorC2<T>::printConstraints(std::ostream& stream)const{
    stream << "vector size = " << this->getPaddedSize() << " " << this->getSize() << "," << this->constrVec->getSize() << std::endl;
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);

    //for(int i=0;i<v.getSize();i++){
    //stream << i << "\t" << v.data[i] << "\n";
    //}

    stream << "------------ Constraint part ------------" << std::endl;

    for(int i=0;i<constrVec->getSize();i++){
      stream << i+this->getSize() << "\t" << (*constrVec)[i] << "\n";
    }

    stream << std::endl;
    stream.flags(origflags);
  }

  template<class T>
  void VectorC2<T>::sub(VectorC2<T>& r, const VectorC2<T>& a,
                        const VectorC2<T>& b, const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::sub(r, a, b);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::sub(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  template<class T>
  void VectorC2<T>::add(VectorC2<T>& r, const VectorC2<T>& a,
                        const VectorC2<T>& b, const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::add(r, a, b);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::add(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  template<class T>
  void VectorC2<T>::madd (VectorC2<T>& r, const VectorC2<T>& a,
                          const VectorC2<T>& b, const VectorC2<T>& c,
                          const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::madd(r, a, b, c);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::madd(*r.constrVec, *a.constrVec, *b.constrVec, *c.constrVec);
    }
  }

  template<class T>
  void VectorC2<T>::mfadd(VectorC2<T>& r, T f, const VectorC2<T>& b,
                          const VectorC2<T>& c, const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mfadd(r, f, b, c);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::mfadd(*r.constrVec, f, *b.constrVec, *c.constrVec);
    }
  }

  template<class T>
  void VectorC2<T>::mfadd2(VectorC2<T>& r, T f, const VectorC2<T>& b,
                           const VectorC2<T>& c, const VectorC2<T>& d,
                           const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mfadd2(r, f, b, c, d);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::mfadd2(*r.constrVec, f, *b.constrVec,
                        *c.constrVec, *d.constrVec);
    }
  }

  template<class T>
  void VectorC2<T>::mul(VectorC2<T>& r, const VectorC2<T>& a,
                        const VectorC2<T>& b, const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mul(r, a, b);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::mul(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  template<class T>
  void VectorC2<T>::mulf(VectorC2<T>& r, const VectorC2<T>& a, T f,
                         const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mulf(r, a, f);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::mulf(*r.constrVec, *a.constrVec, f);
    }
  }

  template<class T>
  void VectorC2<T>::div(VectorC2<T>& r, const VectorC2<T>& a,
                        const VectorC2<T>& b, const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::div(r, a, b);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::div(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  /*Partial operations*/
  template<class T>
  void VectorC2<T>::subp(VectorC2<T>& r, const VectorC2<T>& a,
                         const VectorC2<T>& b, const VectorRange rg,
                         const VectorOpMode mode){
    Vector<T>::subp(r, a, b, rg);
    error("No parallel support for enriched vectors");
  }

  template<class T>
  void VectorC2<T>::addp(VectorC2<T>& r, const VectorC2<T>& a,
                         const VectorC2<T>& b, const VectorRange rg,
                         const VectorOpMode mode){
    Vector<T>::addp(r, a, b, rg);
    error("No parallel support for enriched vectors");
  }

  template<class T>
  void VectorC2<T>::maddp(VectorC2<T>& r, const VectorC2<T>& a,
                          const VectorC2<T>& b, const VectorC2<T>& c,
                          const VectorRange rg, const VectorOpMode mode){
    Vector<T>::maddp(r, a, b, c, rg);
    error("No parallel support for enriched vectors");
  }

  template<class T>
  void VectorC2<T>::mfaddp(VectorC2<T>& r, T f, const VectorC2<T>& b,
                           const VectorC2<T>& c, const VectorRange rg,
                           const VectorOpMode mode){
    Vector<T>::mfaddp(r, f, b, c, rg);
    error("No parallel support for enriched vectors");
  }

  template<class T>
  void VectorC2<T>::mfadd2p(VectorC2<T>& r, T f, const VectorC2<T>& b,
                            const VectorC2<T>& c, const VectorC2<T>& d,
                            const VectorRange rg,
                            const VectorOpMode mode){
    Vector<T>::mfadd2p(r, f, b, c, d, rg);
    error("No parallel support for enriched vectors");
  }

  template<class T>
  void VectorC2<T>::mulp(VectorC2<T>& r, const VectorC2<T>& a,
                         const VectorC2<T>& b, const VectorRange rg,
                         const VectorOpMode mode){
    Vector<T>::mulp(r, a, b, rg);
    error("No parallel support for enriched vectors");
  }

  template<class T>
  void VectorC2<T>::mulfp(VectorC2<T>& r, const VectorC2<T>& a, T f,
                          const VectorRange rg, const VectorOpMode mode){
    Vector<T>::mulfp(r, a, f, rg);
    error("No parallel support for enriched vectors");
  }
}

#endif/*VECTORC_HPP*/
