/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef IECGEO_HPP
#define IECGEO_HPP

#include "math/CollisionContext.hpp"
#include <unistd.h>

namespace CGF{

  /*Provides functions for updating the collision context*/
  template<class T>
  class IECGeo{
  public:
    IECGeo(CollisionContext<T>* c):colContext(c){
    }

    virtual void disableAllCandidates(){
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        if(colContext->edgeSets[i].candidates.size() > 0){
          colContext->edgeSets[i].deactivateAll(colContext);
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->backEdgeSets[i].candidates.size() > 0){
          colContext->backEdgeSets[i].deactivateAll(colContext);
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->triangleEdgeSets[i].candidates.size() > 0){
          colContext->triangleEdgeSets[i].deactivateAll(colContext);
        }
      }

      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        if(colContext->sets[i].candidates.size() > 0){
          colContext->sets[i].deactivateAll(colContext);
        }
      }

      for(int i=0;i<n_vertices;i++){
        if(colContext->backSets[i].candidates.size() > 0){
          colContext->backSets[i].deactivateAll(colContext);
        }
      }

#if 0
      int n_volumes = colContext->mesh->getNTetrahedra();
      for(int i=0;i<n_volumes;i++){
        colContext->volumeSets[i].deactivateAll(colContext);
      }
#endif
    }

    virtual bool checkBoundingVolumes(){
      bool changed = false;
      colContext->mesh->computeDisplacement(colContext->solver->getx(),
                                            colContext->getDT());

      bool geometryInvalid = false;
      List<int> invalidFaces;
      List<int> invalidVertices;
      List<int> invalidEdges;

      if(!colContext->stree->volumesValid(&invalidFaces)){
        /*Update the bounding volumes*/
        warning("bounding volumes of triangles not bounding");
        geometryInvalid = true;
        colContext->stree->update(colContext->solver->getx(),
                                  colContext->getDT());
      }

      /*Check individual vertex-face / edge-edge combination*/
      /*Update collision detection structures*/

      colContext->checkBBoxesVertex(&invalidVertices);
      colContext->checkBBoxesEdge(&invalidEdges);

      if(invalidVertices.size() != 0 || invalidEdges.size() != 0){
        /*Some vertices and/or edges moved outside their bounding
          volume, update them.*/
        geometryInvalid = true;
      }

      bool hotlistedPotentials = false;

      {
        int n_vertices = colContext->mesh->getNVertices();
        for(int i=0;i<n_vertices;i++){
          if(colContext->sets[i].forcedPotentials.size() > 0){
            hotlistedPotentials = true;
            break;
          }
        }

        for(int i=0;i<n_vertices;i++){
          if(colContext->backSets[i].forcedPotentials.size() > 0){
            hotlistedPotentials = true;
            break;
          }
        }
      }

      if(geometryInvalid || hotlistedPotentials){
        warning("Invalid bboxes detected");
        if(hotlistedPotentials){
          warning("hotlisted potentials detected");
        }
        List<int>::Iterator it = invalidFaces.begin();
        while(it != invalidFaces.end()){
          int face = *it++;
          warning("Face %d out of box", face);

          Triangle<T> triangle;
          bool degenerate = false;
          try{
            colContext->mesh->getTriangle(&triangle, face, false);
          }catch(Exception* e){
            degenerate = true;
            delete e;
          }
          std::cout << triangle << std::endl;
          if(degenerate){
            warning("Degenerate face");
          }
        }

        it = invalidEdges.begin();
        while(it != invalidEdges.end()){
          warning("Edge %d out of box", *it++);
        }

        it = invalidVertices.begin();
        while(it != invalidVertices.end()){
          warning("Vertex %d out of box", *it++);
        }

        /*Update per edge and vertex bounding boxes.  The bounding
          boxes in the context and the boxes in the surface tree are
          getting synced */
        colContext->updateBBoxes();

        int n_faces = colContext->mesh->getNHalfFaces();
        for(int i=0;i<n_faces;i++){
          colContext->faceFaceSets[i].update(colContext);
        }

        int n_edges = colContext->mesh->getNHalfEdges();
        for(int i=0;i<n_edges;i++){
          int twin = colContext->mesh->halfEdges[i].twin;
          if(i < twin){
            colContext->edgeSets[i].incrementalUpdate(colContext);
          }
        }

        for(int i=0;i<n_edges;i++){
          int twin = colContext->mesh->halfEdges[i].twin;
          if(i < twin){
            colContext->backEdgeSets[i].incrementalUpdate(colContext);
          }
        }

        int n_vertices = colContext->mesh->getNVertices();
        for(int i=0;i<n_vertices;i++){
          colContext->sets[i].incrementalUpdate(colContext);
        }

        for(int i=0;i<n_vertices;i++){
          colContext->backSets[i].incrementalUpdate(colContext);
        }

        changed = true;
      }

      /*Check for invalid edges that became valid*/
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        int twin = colContext->mesh->halfEdges[i].twin;
        if(i < twin){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular thing %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          bool validated =
            colContext->edgeSets[i].checkForValidEdges(ne, com, rot,
                                                       colContext);

          if(validated){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        int twin = colContext->mesh->halfEdges[i].twin;
        if(i < twin){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular thing %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          bool validated =
            colContext->backEdgeSets[i].checkForValidEdges(ne, com, rot,
                                                           colContext);

          if(validated){
            changed = true;
          }
        }
      }

      return changed;
    }

    virtual bool checkGeometry(bool friction, T bnorm, EvalStats& stats){
      bool changed = false;
#if 1
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        if(colContext->edgeSets[i].candidates.size() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular thing %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->edgeSets[i].checkAndUpdate(ne, com, rot,
                                                    colContext, stats,
                                                    friction, false,
                                                    bnorm)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->backEdgeSets[i].candidates.size() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular thing %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backEdgeSets[i].checkAndUpdate(ne, com, rot,
                                                        colContext,
                                                        stats,
                                                        friction, false,
                                                        bnorm)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->triangleEdgeSets[i].candidates.size() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular thing %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->triangleEdgeSets[i].checkAndUpdate(ne, com, rot,
                                                            colContext,
                                                            stats,
                                                            friction, false,
                                                            bnorm)){
            changed = true;
          }
        }
      }
#endif
#if 1
      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        if(colContext->sets[i].candidates.size() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, false);

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->sets[i].checkAndUpdate(p, com, rot,
                                                colContext,
                                                stats,
                                                friction, false,
                                                bnorm)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_vertices;i++){
        if(colContext->backSets[i].candidates.size() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, true);

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backSets[i].checkAndUpdate(p, com, rot,
                                                    colContext,
                                                    stats,
                                                    friction, false,
                                                    bnorm)){
            changed = true;
          }
        }
      }
#endif

#if 0
      int n_volumes = colContext->mesh->getNTetrahedra();
      for(int i=0;i<n_volumes;i++){
        if(colContext->volumeSets[i].checkAndUpdate(colContext)){
          changed = true;
        }
      }
#endif

      if(!changed){
        message("Update initial states");

        /*Update initial states*/
        int n_edges = colContext->mesh->getNHalfEdges();
        for(int i=0;i<n_edges;i++){
          if(colContext->edgeSets[i].candidates.size() > 0){
            Edge<T> ne;
            try{
              colContext->mesh->getDisplacedEdge(&ne, i, false);
            }catch(Exception* e){
              warning("Singular thing %s", e->getError().c_str());
              delete e;
              continue;
            }

            Vector4<T> com(0,0,0,0);
            Quaternion<T> rot(0,0,0,1);

            colContext->mesh->setCurrentEdge(i);
            int vertex = colContext->mesh->getOriginVertex();

            if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[vertex];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->edgeSets[i].updateInitial(ne, com, rot,
                                                     colContext, stats,
                                                     friction, false,
                                                     bnorm)){
              changed = true;
            }
          }
        }

        for(int i=0;i<n_edges;i++){
          if(colContext->backEdgeSets[i].candidates.size() > 0){
            Edge<T> ne;
            try{
              colContext->mesh->getDisplacedEdge(&ne, i, true);
            }catch(Exception* e){
              warning("Singular thing %s", e->getError().c_str());
              delete e;
              continue;
            }

            Vector4<T> com(0,0,0,0);
            Quaternion<T> rot(0,0,0,1);

            colContext->mesh->setCurrentEdge(i);
            int vertex = colContext->mesh->getOriginVertex();

            if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[vertex];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->backEdgeSets[i].updateInitial(ne, com, rot,
                                                         colContext,
                                                         stats,
                                                         friction, false,
                                                         bnorm)){
              changed = true;
            }
          }
        }

        for(int i=0;i<n_edges;i++){
          if(colContext->triangleEdgeSets[i].candidates.size() > 0){
            Edge<T> ne;
            try{
              colContext->mesh->getDisplacedEdge(&ne, i, false);
            }catch(Exception* e){
              warning("Singular thing %s", e->getError().c_str());
              delete e;
              continue;
            }

            Vector4<T> com(0,0,0,0);
            Quaternion<T> rot(0,0,0,1);

            colContext->mesh->setCurrentEdge(i);
            int vertex = colContext->mesh->getOriginVertex();

            if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[vertex];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->triangleEdgeSets[i].updateInitial(ne, com, rot,
                                                             colContext,
                                                             stats,
                                                             friction, false,
                                                             bnorm)){
              changed = true;
            }
          }
        }

        int n_vertices = colContext->mesh->getNVertices();
        for(int i=0;i<n_vertices;i++){
          if(colContext->sets[i].candidates.size() > 0){
            OrientedVertex<T> p;
            colContext->mesh->getDisplacedOrientedVertex(&p, i, false);

            Vector4<T> com(0,0,0,0);
            Quaternion<T> rot(0,0,0,1);

            if(colContext->mesh->vertices[i].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[i];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->sets[i].updateInitial(p, com, rot,
                                                 colContext,
                                                 stats,
                                                 friction, false,
                                                 bnorm)){
              changed = true;
            }
          }
        }

        for(int i=0;i<n_vertices;i++){
          if(colContext->backSets[i].candidates.size() > 0){
            OrientedVertex<T> p;
            colContext->mesh->getDisplacedOrientedVertex(&p, i, true);

            Vector4<T> com(0,0,0,0);
            Quaternion<T> rot(0,0,0,1);

            if(colContext->mesh->vertices[i].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[i];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->backSets[i].updateInitial(p, com, rot,
                                                     colContext,
                                                     stats,
                                                     friction, false,
                                                     bnorm)){
              changed = true;
            }
          }
        }

        if(changed){
          message("Evaluate geometry for updated initial states");
          //EvalType etype;
          //etype.enableGeometryCheck();
          //etype.enablePenetrationCheck();
          //etype.enableFrictionCheck();
          //evaluateCandidates(etype, stats);
        }
      }

      return changed;
    }

    virtual void updateCandidateLists(){
      /*Update all face normals*/
      this->colContext->mesh->computeFaceNormals();

      /*Update BBoxes (with extension due to velocity) of each vertex
        and edge. Face boxes are stored in tree.*/
      this->colContext->updateBBoxes();

      /*Performs raw collision detection for each triangle and finds a
        set of potentially colliding / nearby triangles.*/
      for(int i=0;i<this->colContext->mesh->getNHalfFaces();i++){
        this->colContext->faceFaceSets[i].update(this->colContext);
      }

#if 1
      int n_edges = this->colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        int twin = this->colContext->mesh->halfEdges[i].twin;
        if(i<twin){
          this->colContext->edgeSets[i].update(this->colContext);
        }
      }

      for(int i=0;i<n_edges;i++){
        int twin = this->colContext->mesh->halfEdges[i].twin;
        if(i<twin){
          this->colContext->backEdgeSets[i].update(this->colContext);
        }
      }

      for(int i=0;i<n_edges;i++){
        int twin = this->colContext->mesh->halfEdges[i].twin;
        if(i<twin){
          this->colContext->triangleEdgeSets[i].update(this->colContext);
        }
      }
#endif

#if 1
      int n_vertices = this->colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        this->colContext->sets[i].update(this->colContext);
      }

      for(int i=0;i<n_vertices;i++){
        this->colContext->backSets[i].update(this->colContext);
      }
#endif

#if 0
      int n_volumes = this->colContext->mesh->getNTetrahedra();
      for(int i=0;i<n_volumes;i++){
        this->colContext->volumeSets[i].update(this->colContext);
      }
#endif
    }

    void saveIntermediate(){
      while(true){
        bool error = false;
        char buffer[256];

        /*Save single precision file*/
        sprintf(buffer, "out_intermediate.tet2");
        FILE* file = fopen(buffer, "w");

        for(int i=0;i<colContext->mesh->getNVertices();i++){
          if(colContext->mesh->vertices[i].type == Mesh::Static){
            //Skip static vertices
            continue;
          }

          float xx, yy, zz;
          zz = (float)(3.0*(colContext->mesh->vertices[i].displCoord[0]));
          xx = (float)(3.0*(colContext->mesh->vertices[i].displCoord[1]));
          yy = (float)(3.0*(colContext->mesh->vertices[i].displCoord[2]));

          char* pp;
          pp = (char*)&xx;

          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }

          pp = (char*)&yy;
          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }

          pp = (char*)&zz;
          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }
        }

        fclose(file);

        if(!error){
          return;
        }else{
          warning("Could not properly write states to a file, disk fulll?");
          warning("Hit enter to retry, after making some space");
          getchar();
          sleep(10000000);
        }
      }
    }



    virtual bool evaluateCandidates(const EvalType& etype, EvalStats& stats){
      bool changed = false;

      /*Update displacement information*/

      colContext->mesh->computeDisplacement(colContext->solver->getx(),
                                            colContext->getDT());

      saveIntermediate();

      /*Check for collisions on the candidate pairs*/

      /*Here it is important that the edges are checked first since
        they can be flagged due to a collapse. If an edge is flagged,
        the faces using the collapsed edge are not evaluated, except
        when they are forced to resolve a collapse*/
#if 1
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        //if(colContext->edgeSets[i].candidates.size() > 0){
        if(true){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular thing, %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->edgeSets[i].evaluate(ne, com, rot,
                                              colContext, etype, stats)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        //if(colContext->backEdgeSets[i].candidates.size() > 0){
        if(true){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular thing, %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backEdgeSets[i].evaluate(ne, com, rot,
                                                  colContext, etype, stats)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->triangleEdgeSets[i].candidates.size() > 0){

          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular thing, %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->triangleEdgeSets[i].evaluate(ne, com, rot,
                                                      colContext, etype,
                                                      stats)){
            changed = true;
          }
        }
      }
#endif

#if 1
      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        if(colContext->sets[i].candidates.size() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, false);

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->sets[i].evaluate(p, com, rot,
                                          colContext, etype, stats)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_vertices;i++){
        if(colContext->backSets[i].candidates.size() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, true);

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backSets[i].evaluate(p, com, rot,
                                              colContext, etype, stats)){
            changed = true;
          }
        }
      }
#endif

#if 0
      /*Here new volume springs can be added to the system matrix. If
        so, the complete preconditioner needs to be recomputed, also
        the part corresponding to the system matrix.*/
      int n_volumes = colContext->mesh->getNTetrahedra();
      for(int i=0;i<n_volumes;i++){
        if(colContext->volumeSets[i].evaluate(colContext, etype, stats)){
          changed = true;
        }
      }
#endif
      if(changed){
        stats.n_geometry_check++;
      }
      return changed;
    }

    virtual void checkNonLinearTerms(){
      /*Checks the model stored in matrix M. If M contains a
        linearization of a non-linear problem at t0, check its
        difference according to its linearization at t1. If there is a
        significant difference, an updated linearization of M must be
        computed, the RHS must be updated, the preconditioner needs to
        be updated and the solver can continue.*/
    }

    virtual ~IECGeo(){
    }
  protected:
    CollisionContext<T>* colContext;
  };
}

#endif/*IECGEO_HPP*/
