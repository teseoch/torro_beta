/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "math/Math.hpp"
#include "math/Matrix44.hpp"
#include "math/Vector.hpp"
#include "math/SpMatrixBlock.hpp"

namespace CGF{

  /*Templatized matrix implementation: can only be used iff the
    dimensions of the matrix are known at compile time. Due to this,
    the code is better optimized by the compiler, resulting in fast
    execution.*/

  template<int N, int M, class T=float, int B=2>
  class CGFAPI MatrixT{
  public:
    MatrixT(){
      clear();
    }

    MatrixT(bool b){
    }

    /*Copy constructor*/
    MatrixT(const MatrixT<N, M, T, B>& m){
      memcpy(data, m.data, sizeof(T)*N*M);
    }

    /*Assignment operator*/
    MatrixT<N, M, T, B>& operator=(const MatrixT<N, M, T, B>& m){
      if(this != &m){
        memcpy(data, m.data, sizeof(T)*N*M);
      }
      return *this;
    }

    void clear(){
      memset(data, 0, sizeof(T)*N*M);
    }

    MatrixT<N, M, T, B>& operator=(T f){
      for(int i=0;i<N*M;i++){
        data[i] = f;
      }
      return *this;
    }

    T* operator[](int i){
      return &(data[i*M]);
    }

    const T* operator[](int i)const{
      return &(data[i*M]);
    }

    T& element(int x, int y){
      return data[x*M+y];
    }

    const T& element(int x, int y)const{
      return data[x*M+y];
    }

    T& elementv(int x){
      return data[x*M];
    }

    const T& elementv(int x)const{
      return data[x*M];
    }

#if 0
    RowProxy operator[](int i){
      cgfassert(i>=0 && i < N);
      RowProxy r;
      r.mat = this;
      r.row = i;
      return r;
    }

    const RowProxy operator[](int i)const{
      cgfassert(i>=0 && i < N);
      RowProxy r;
      r.mat = (MatrixT*)this;
      r.row = i;
      return r;
    }
#endif

    /*Needed since multiplication operator technically operates on
      different classes*/
    template<int NN, int MM, class TT, int BB>
      friend class MatrixT;

    template<int P>
      MatrixT<N, P, T, B> operator*(const MatrixT<M, P, T, B>& m){
      MatrixT<N, P, T, B> mat(false);

      for(int i=0;i<P;i++){
        for(int j=0;j<N;j++){
          T r = (T)0.0;

          for(int k=0;k<M;k++){
            r += data[j*M + k] * m.data[k*P+i];
          }

          mat.data[j*P+i] = r;
        }
      }

      return mat;
    }

    MatrixT<N, M, T, B> mulBlockR(Matrix44<T>& bl, int bs){
      MatrixT<N, M, T, B> mat(false);

      for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
          T r = (T)0.0;

          for(int k=i/bs;k<(i+1/bs);k++){
            //r += data[j*M + k] * m.data[k*P+i];
            r += data[j*M + k] * bl[k%bs][i%bs];
          }

          mat.data[j*M+i] = r;
        }
      }

      return mat;
    }

    MatrixT<N, M, T, B> mulBlockL(Matrix44<T>& bl, int bs){
      MatrixT<N, M, T, B> mat(false);

      for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
          T r = (T)0.0;

          for(int k=j/bs;k<(j+1/bs);k++){
            r += bl[j%bs][k%bs] * data[k*M+i];
          }

          mat.data[j*M+i] = r;
        }
      }

      return mat;
    }

    /*Matrix vector multiplication*/
    Vector<T> operator*(const Vector<T>& v)const{
      Vector<T> r(M);

      for(int j=0;j<N;j++){
        T val = (T)0.0;

        for(int i=0;i<M;i++){
          val += data[j*N+i] * v[i];
        }

        r[j] = val;
      }
      return r;
    }

    T powerMethod(T tol = (T)1e-6, T shift = (T)0.0){
      MatrixT<M, 1, T, B> x;
      MatrixT<M, 1, T, B> y;
      MatrixT<M, 1, T, B> xx;

      T mu = 0.0;
      T mu1 = 0.0;
      T mu2 = 0.0;

      T lastAitken = (T)0.0;

      for(int i=0;i<M;i++){
        x[i][0] = (T)1.0;///(T)(i+1);
      }
      x[0][0] = (T)1.0;

      int p = 0;//Since x[0] contains the first largest absolute value of x

      for(int j=0;j<M*1000;j++){
        //message("iter  = %d", j);

        //std::cout << x << std::endl;

        y = *this * x;

        //std::cout << y << std::endl;

        for(int k=0;k<M;k++){
          y[k][0] -= shift * x[k][0];
        }

        mu = y[p][0];

        T muAitken = aitkenD2(mu2, mu1, mu);

        //message("mu = %10.10e, %10.10e", mu, muAitken);

        int maxIndex = -1;
        T maxVal = -1.0;
        for(int k=0;k<M;k++){
          //message("y[k] = %10.10e, max = %10.10e", y[k][0], maxVal);
          if(Abs(y[k][0]) > maxVal){
            maxVal = Abs(y[k][0]);
            maxIndex = k;
            //message("idx = %d", maxIndex);
          }
        }

        p = Max(maxIndex, 0);

        //message("p = %d", p);

        T nrm = maxVal;

        //message("norm = %10.10e", nrm);

        if(Abs(nrm) < tol && j > 4){
          return muAitken + shift;
        }


        xx = x - y / y[p][0];

        maxIndex = -1;
        maxVal = -1.0;
        for(int k=0;k<M;k++){
          if(Abs(xx[k][0]) > maxVal){
            maxVal = Abs(xx[k][0]);
            maxIndex = k;
          }
        }

        T err = maxVal;

        //message("l = %10.10e, err = %10.10e, nrm = %10.10e",
        //        mu + shift, err, nrm);

        x = y / y[p][0];

        if(Abs(err) < tol && j > 4){
          return muAitken + shift;
        }

        mu2 = mu1;
        mu1 = mu;

        if(Abs(Abs(lastAitken) - Abs(muAitken)) < tol && j > 4){
          return muAitken + shift;
        }
        lastAitken = muAitken;
      }
      return mu + shift;
    }

    T symmetricPowerMethod(T tol = (T)1e-6, T shift = (T)0.0){
      MatrixT<M, 1, T, B> x;
      MatrixT<M, 1, T, B> y;
      MatrixT<M, 1, T, B> xx;

      T mu  = 0.0;
      T mu1 = 0.0;
      T mu2 = 0.0;

      T sum = (T)0.0;

      T lastAitken = (T)0.0;

      for(int i=0;i<M;i++){
        x[i][0] = (T)1.0;
        sum += Sqr(x[i][0]);
      }

      x /= Sqrt(sum);

      for(int j=0;j<M*10;j++){
        //message("iter  = %d", j);

        //std::cout << x << std::endl;

        y = *this * x;

        //std::cout << y << std::endl;

        for(int k=0;k<M;k++){
          y[k][0] -= shift * x[k][0];
        }

        mu = (x.transpose() * y)[0][0];

        T muAitken = aitkenD2(mu2, mu1, mu);

        //message("mu = %10.10e, %10.10e", mu, muAitken);

        sum = (T)0.0;

        for(int k=0;k<M;k++){
          sum += Sqr(y[k][0]);
        }

        T nrm = Sqrt(sum);

        //message("norm = %10.10e", nrm);

        if(Abs(nrm) < tol && j > 4){
          return muAitken + shift;
        }


        xx = x - y / nrm;

        //std::cout << xx << std::endl;

        sum = (T)0.0;
        for(int k=0;k<M;k++){
          sum += Sqr(xx[k][0]);
        }

        T err = Sqrt(sum);

        //message("l = %10.10e, err = %10.10e, nrm = %10.10e",
        //      mu + shift, err, nrm);

        x = y / nrm;

        sum = (T)0.0;
        for(int k=0;k<M;k++){
          sum += Sqr(x[k][0]);
        }

        if(Abs(err) < tol && j > 4){
          return muAitken + shift;
        }

        mu2 = mu1;
        mu1 = mu;

        if(Abs(Abs(lastAitken) - Abs(muAitken)) < tol && j > 4){
          return muAitken + shift;
        }

        lastAitken = muAitken;
      }
      return mu + shift;
    }

    T conditionNumber(bool symmetric, T tol = (T)1e-6, T* lmn = 0, T* lmx = 0){
      T lMax = (T)0.0;
      T lMin = (T)0.0;

      if(symmetric){
        lMax = symmetricPowerMethod(tol);
        lMin = symmetricPowerMethod(tol, lMax);
      }else{
        lMax = powerMethod(tol);
        lMin = powerMethod(tol, lMax);
      }

      if(lmn){
        *lmn = lMin;
      }

      if(lmx){
        *lmx = lMax;
      }
      //return Abs(lMax)/Abs(lMin);
      return Max(Abs(lMax), Abs(lMin))/Min(Abs(lMax), Abs(lMin));
    }

    MatrixT<M, N, T, B> transpose() const{
      MatrixT<M, N, T, B> tr;

      for(int j=0;j<N;j++){
        for(int i=0;i<M;i++){
          tr.data[i*N+j] = data[j*M+i];
        }
      }
      return tr;
    }

    MatrixT<N, M, T, B>& operator*=(const T n){
      for(int i=0;i<N*M;i++){
        data[i] *= n;
      }
      return *this;
    }

    MatrixT<N, M, T, B>& operator/=(const T n){
      for(int i=0;i<N*M;i++){
        data[i] /= n;
      }
      return *this;
    }

    MatrixT<N, M, T, B>& operator-=(const MatrixT<N, M, T, B>& m){
      for(int i=0;i<N*M;i++){
        data[i] -= m.data[i];
      }
      return *this;
    }

    MatrixT<N, M, T, B>& operator+=(const MatrixT<N, M, T, B>& m){
      for(int i=0;i<N*M;i++){
        data[i] += m.data[i];
      }

      return *this;
    }

    MatrixT<N, M, T, B> operator-(const MatrixT<N, M, T, B>& m)const{
      MatrixT<N, M, T, B> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] - m.data[i];
      }
      return r;
    }

    MatrixT<N, M, T, B> operator+(const MatrixT<N, M, T, B>& m)const{
      MatrixT<N, M, T, B> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] + m.data[i];
      }
      return r;
    }

    MatrixT<N, M, T, B> operator+() const{
      return *this;
    }

    MatrixT<N, M, T, B> operator-() const{
      MatrixT<N, M, T, B> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = -data[i];
      }
      return r;
    }

    MatrixT<N, M, T, B> operator/(const T n)const{
      MatrixT<N, M, T, B> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] / n;
      }
      return r;
    }

    MatrixT<N, M, T, B> operator*(const T n)const{
      MatrixT<N, M, T, B> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] * n;
      }
      return r;
    }

    template<int NN, int MM, class TT, int BB>
      friend std::ostream& operator<<(std::ostream& os,
                                      const MatrixT<NN, MM, TT, BB>& v);

    protected:
    /*Storage*/
#ifdef _WIN32
	__declspec(align(16)) T data[(uint)(N*M)];
#else
    T data[(uint)(N*M)] __attribute__ ((aligned (16)));
#endif
  };

  template<int N, int M, class T, int B>
  std::ostream& operator<<(std::ostream& os,
                           const MatrixT<N, M, T, B>& v){
    os << "MatrixT(" << N << ", " << M << ")" << std::endl;
    for(int i=0;i<N;i++){
      for(int j=0;j<M;j++){
        std::cout << v[i][j] << "\t";
      }
      std::cout << std::endl;
    }
    return os;
  }

  template<int N, int M, class T, int B>
  inline bool IsNan(const MatrixT<N, M, T, B>& mat){
    for(int i=0;i<N;i++){
      for(int j=0;j<M;j++){
        if(IsNan(mat[i][j])){
          return true;
        }
      }
    }
    return false;
  }

  template<int N, int M, class T, int B>
  void eig(const MatrixT<N, M, T, B>& A, MatrixT<N, M, T, B>& vectors,
           MatrixT<N, 1, T>& eigenvalues){
    MatrixT<N, M, T, B> AA = A;
    MatrixT<N, M, T, B> vecr;

    MatrixT<N, 1, T, B> val;

    for(int i=0;i<N;i++){
      val[i][0] = rayleighQuotientIteration(AA, vecr[i], i);
      vecr[i] /= vecr[i].length();

      /*Wielandt deflation*/
    }


  }


  /*Non templatized matrix implementation: can be used if the
    dimensions of the matrix are unknown at compile time. The code is
    almost identical to the templatized version, but is optimized less
    by the compiler*/

  template<class T=float, int B=2>
  class CGFAPI Matrix{
  public:
    class RowProxy{
    public:
      T& operator[](int col){
        cgfassert(col >= 0 && col < mat->height);

        return mat->data[(row/B)*mat->blocks_w + col/B].m[(row%B) * B + col%B];
      }

      const T& operator[](int col)const{
        cgfassert(col >= 0 && col < mat->height);

        return mat->data[(row/B)*mat->blocks_w + col/B].m[(row%B) * B + col%B];
      }

      Matrix<T, B>* mat;

      int row;
    };


    Matrix(int h, int w){
      blocks_w = (w/B) + ((w%B)>0?1:0);
      blocks_h = (h/B) + ((h%B)>0?1:0);

      vsize = ((w/B)+((w%B)>0?1:0))*B;

      width  = w;
      height = h;

      data = new SpMatrixBlock<B, T>[blocks_w * blocks_h];

      alignedMalloc((void**)&vr, 16, (ulong)blocks_w*B*sizeof(T));

      cgfassert(alignment(vr, 16));

      clear();
    }

    /*Copy constructor*/
    Matrix(const Matrix<T, B>& m){
      blocks_w = m.blocks_w;
      blocks_h = m.blocks_h;

      width = m.width;
      height = m.height;

      vsize = m.vsize;

      data = new SpMatrixBlock<B, T>[blocks_w * blocks_h];

      alignedMalloc((void**)&vr, 16, (ulong)blocks_w*B*sizeof(T));

      cgfassert(alignment(vr, 16));

      memcpy(data, m.data, sizeof(SpMatrixBlock<B, T>) *
             (uint)(blocks_w * blocks_h));
    }

    ~Matrix(){
      delete[] data;
      alignedFree(vr);
    }

    /*Assignment operator*/
    Matrix<T, B>& operator=(const Matrix<T, B>& m){
      message("assignment operator");
      if(this != &m){

        delete[] data;

        alignedFree(vr);

        blocks_w = m.blocks_w;
        blocks_h = m.blocks_h;

        width = m.width;
        height = m.height;

        vsize = m.vsize;

        data = new SpMatrixBlock<B, T>[blocks_w * blocks_h];

        alignedMalloc((void**)&vr, 16, blocks_w*B*sizeof(T));

        cgfassert(alignment(vr, 16));

        memcpy(data, m.data, sizeof(SpMatrixBlock<B, T>) * blocks_w * blocks_h);
      }
      return *this;
    }

    void clear(){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].clear();
        }
      }
    }

    Matrix<T, B>& operator=(T f){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].set(f);
        }
      }
      return *this;
    }

    RowProxy operator[](int i){
      RowProxy r;
      r.mat = this;
      r.row = i;
      return r;
    }

    const RowProxy operator[](int i)const{
      RowProxy r;
      r.mat = (Matrix<T, B>*)this;
      r.row = i;
      return r;
    }

    Matrix<T, B> operator*(const Matrix<T, B>& m){
      Matrix<T, B> mat(height, m.width);

      for(int i=0;i<m.width;i++){

        for(int j=0;j<width;j++){
          vr[j] = m.data[(j/B)*m.blocks_w + (i/B)].m[(j%B)*B+i%B];
        }

        /*Vector loaded*/

        /*Multiply blocks with vector*/

        /*Block-rows*/
        for(int j=0;j<blocks_h;j++){
          SpMatrixBlock<B, T> block;
          block.clear();

          for(int k=0;k<blocks_w;k++){
            block.vectorMulAdd(&(data[j*blocks_w + k]), vr+k*B);
          }

          block.rowSumReduce();

          /*Store data*/

          for(int k=0;k<B;k++){
            mat.data[j*m.blocks_w + i/B].m[k*B+i%B] =
              block.m[k*B];
          }
        }
      }
      return mat;
    }

    /*Matrix vector multiplication*/
    Vector<T> operator*(const Vector<T>& v)const{
      cgfassert(v.getSize() == width);
      Vector<T> r(height);

      const T* vdata = v.getData();

      for(int i=0;i<blocks_h;i++){
        SpMatrixBlock<B, T> block;
        block.clear();

        for(int j=0;j<blocks_w;j++){
          block.vectorMulAdd(&(data[i*blocks_w + j]), vdata+j*B);
        }

        block.rowSumReduce();

        /*Store data*/

        for(int k=0;k<B;k++){
          r[i*B+k] = block.m[k*B];
        }
      }
      return r;
    }

    Matrix<T, B> transpose() const{
      Matrix<T, B> tr(width, height);

      for(int i=0;i<height;i++){
        for(int j=0;j<width;j++){
          tr[j][i] = (*this)[i][j];
        }
      }
      return tr;
    }

    Matrix<T, B>& operator*=(const T n){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].mul(n);
        }
      }
      return *this;
    }

    Matrix<T, B>& operator/=(const T n){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].div(n);
        }
      }
      return *this;
    }

    Matrix<T, B>& operator-=(const Matrix<T, B>& m){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].sub(&(m.data[i*blocks_w+j]));
        }
      }
      return *this;
    }

    Matrix<T, B>& operator+=(const Matrix<T, B>& m){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].add(&(m.data[i*blocks_w+j]));
        }
      }
      return *this;
    }

    Matrix<T, B> operator-(const Matrix<T, B>& m)const{
      Matrix<T, B> r(*this);
      r -= m;
      return r;
    }

    Matrix<T, B> operator+(const Matrix<T, B>& m)const{
      Matrix<T, B> r(*this);
      r += m;
      return r;
    }

    Matrix<T, B> operator+() const{
      return *this;
    }

    Matrix<T, B> operator-() const{
      Matrix<T, B> r(*this);
      r *= (T)-1.0;
      return r;
    }

    Matrix<T, B> operator/(const T n)const{
      Matrix<T, B> r(*this);
      r /= n;
      return r;
    }

    Matrix<T, B> operator*(const T n)const{
      Matrix<T, B> r(*this);
      r *= n;
      return r;
    }

    template<class TT, int BB>
      friend std::ostream& operator<<(std::ostream& os,
                                      const Matrix<TT, BB>& v);

    protected:
    int blocks_w;
    int blocks_h;

    int width;
    int height;

    int vsize;

    /*Storage*/
    SpMatrixBlock<B, T>* data;
    T* vr;
  };

  template<class T, int B>
  std::ostream& operator<<(std::ostream& os,
                           const Matrix<T, B>& v){
    os << "Matrix(" << v.height << ", " << v.width << ")" << std::endl;
    for(int i=0;i<v.height;i++){
      for(int j=0;j<v.width;j++){
        std::cout << v[i][j] << "\t";
      }
      std::cout << std::endl;
    }
    return os;
  }
}

#endif/*MATRIX_HPP*/
