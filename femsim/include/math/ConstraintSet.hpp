/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTSET_HPP
#define CONSTRAINTSET_HPP

#include "datastructures/DCEList.hpp"
#include "geo/Triangle.hpp"
#include "datastructures/Tree.hpp"
#include "math/EvalType.hpp"

namespace CGF{
  template<class T>
  class CollisionContext;

  template<class T>
  class ConstraintCandidateFace;

  template<class T>
  class ConstraintSet{
  public:
    typedef ConstraintCandidateFace<T> Candidate;
    typedef typename Tree<Candidate*>::Iterator CandidateTreeIterator;
    typedef typename Map<int, Candidate*>::Iterator CandidateMapIterator;
    ConstraintSet();

    virtual ~ConstraintSet();

    void setBackFace();

    void deactivateAll(CollisionContext<T>* n);

    bool evaluate(OrientedVertex<T>& p, Vector4<T>& com, Quaternion<T>& rot,
                  CollisionContext<T>* n, /*uint operation*/
                  const EvalType& etype,
                  EvalStats& stats);

    /*Check the validity of the solution, i.e., constrained vertices
      can not penetrate the constraining faces.

      However, if the current solution does not project on the current
      triangle, there must be an other triangle that does so. In such
      a case, don't check the validity since it is not valid by
      definition.*/
    bool checkAndUpdate(OrientedVertex<T>& p,
                        Vector4<T>& com, Quaternion<T>& rot,
                        CollisionContext<T>* n,
                        EvalStats& stats,
                        bool friction,
                        bool force, T bnorm);

    bool updateInitial(OrientedVertex<T>& p,
                       Vector4<T>& com, Quaternion<T>& rot,
                       CollisionContext<T>* n,
                       EvalStats& stats,
                       bool friction,
                       bool force, T bnorm);

    void incrementalUpdate(CollisionContext<T>* n, bool onlyForced = false);
    void update(CollisionContext<T>* n);

    void forceConstraint(CollisionContext<T>* n, int faceId);

    void showFaceCandidateState(int face, CollisionContext<T>* ctx);

    void updateStartPositions(const OrientedVertex<T>& vertex,
                              const Vector4<T>& com,
                              const Quaternion<T>& rot,
                              Candidate* c);

    OrientedVertex<T> startPosition;
    Vector4<T>        startCenterOfMass;
    Quaternion<T>     startOrientation;
    OrientedVertex<T> lastPosition;
    Vector4<T>        lastCenterOfMass;
    Quaternion<T>     lastOrientation;

    Map<int, Candidate*> candidates; /*List of faces close by vertex
                                       lastPosition*/
    Tree<int> candidatesTree;     /*Stores face ids of candidates*/

    Tree<int> forcedPotentials;

    int vertexId;
    Mesh::GeometryType type;
    bool backFace;
  };
}

#include "math/ConstraintSetEdge.hpp"

#endif/*CONSTRAINTSET_HPP*/
