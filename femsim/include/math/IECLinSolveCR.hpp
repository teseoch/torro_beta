/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef IECLINSOLVE_CR_HPP
#define IECLINSOLVE_CR_HPP

#include <limits.h>
#include "math/IECPrecLinSolve.hpp"
#include "math/constraints/ContactBlockConstraint.hpp"
#include "core/Exception.hpp"

#include "util/CSVExporter.hpp"

#include "math/ConstraintTol.hpp"

#define CONVERGENCE_EXPORT
#undef  CONVERGENCE_EXPORT

namespace CGF{

  double scf = 1.0;

  static int tstep = 0;
  extern BenchmarkTimer* spmv_timer;

  /*In order to instantiate this solver, one must create a subclass
    which implements the virtual functions profided by the
    IECEvaluator interface. This interface gives the 'rules' for
    constraint evaluations.*/

  template<int N, class T>
  class IECLinSolveT2CR : public IECPrecLinSolveT2<N, T>,
                          public IECEvaluator<T>{
  public:
    IECLinSolveT2CR(int d):IECPrecLinSolveT2<N,T>(d){
      ru   = new VectorC2<T>(*(this->x));
      rc   = new VectorC2<T>(*(this->x));

      p    = new VectorC2<T>(*(this->x));
      Ap   = new VectorC2<T>(*(this->x));
      Aup  = new VectorC2<T>(*(this->x));
      Acp  = new VectorC2<T>(*(this->x));

      Cr   = new VectorC2<T>(*(this->x));
      ACr  = new VectorC2<T>(*(this->x));
      AuCr = new VectorC2<T>(*(this->x));
      AcCr = new VectorC2<T>(*(this->x));

      CAp1 = new VectorC2<T>(*(this->x));

      scratch1 = new VectorC2<T>(*(this->x));
      scratch2 = new VectorC2<T>(*(this->x));
      scratchP = new VectorC2<T>(*(this->x));

      pACAp  = (T)0.0;
      beta   = (T)0.0;
      alpha  = (T)0.0;
      alpha1 = (T)0.0;

      spmv_timer = &this->timer;

      this->vectors.append(ru);
      this->vectors.append(rc);

      this->vectors.append(p);
      this->vectors.append(Ap);
      this->vectors.append(Aup);
      this->vectors.append(Acp);

      this->vectors.append(Cr);
      this->vectors.append(ACr);
      this->vectors.append(AuCr);
      this->vectors.append(AcCr);
      this->vectors.append(CAp1);

      this->vectors.append(scratch1);
      this->vectors.append(scratch2);
      this->vectors.append(scratchP);
    }

    virtual ~IECLinSolveT2CR(){
      delete ru;
      delete rc;
      delete p;
      delete Ap;
      delete Aup;
      delete Acp;
      delete Cr;
      delete ACr;
      delete AuCr;
      delete AcCr;
      delete CAp1;

      delete scratch1;
      delete scratch2;
      delete scratchP;
    }

    virtual void disableAllConstraints(){
    }

    virtual bool checkConstraints(bool friction, EvalStats& stats){
      return false;
    }


    virtual void recomputeResidual(VectorC2<T>* s1, VectorC2<T>* s2,
                                   bool useAccepted = false,
                                   bool recomputeru = false){
      /*rc = Ac * x*/
      spmvc(*s1, *this->mat, *this->x, ConstraintsFull);

      /*Reset inactive constraint values in b*/
      {
        this->timer.start("vector");
        VectorC2<T>::mulf(*s2, *this->b, 1, Constraints);
        this->resetInactive(*s2);

        /*rc = b - Ac*x*/
        VectorC2<T>::sub(*rc, *s2, *s1);

        if(useAccepted){
          /*Adding additional RHS for friction*/
          VectorC2<T>::add(*rc, *rc, *this->ab2);
        }else{
          /*Adding additional RHS for friction*/
          VectorC2<T>::add(*rc, *rc, *this->b2);
        }
        this->timer.stop("vector");
      }

      if(recomputeru){
        /*For accuracy it is better to also recompute ru, especially
          in tough situations where it takes a long time before
          converge. RU might get very small while rc is larger. At
          some point ru just contains some irrelevant noise. If ru is
          not reset, this noise 'can' be amplified due to the
          multiplications in the search process. In some cases, this
          can result in a non-converging sequence. By resetting also
          ru, it always resembles the real unconstrained error.*/

        /*Compute r, ru, rc (r = ru+rc), r = b - Ax*/
        spmvc(*s1, *this->mat, *this->x, Conventional);
        /*Compute ru = bu - Axu*/
        {
          this->timer.start("vector");
          VectorC2<T>::sub(*ru, *this->b, *s1, NoConstraints);
          /*r = ru + rc*/
          VectorC2<T>::add(*this->r, *ru, *rc);
          this->timer.stop("vector");
        }
      }else{
        this->timer.start("vector");
        /*r = ru + rc*/
        VectorC2<T>::add(*this->r, *ru, *rc);
        this->timer.stop("vector");
      }
    }

    /*Evaluates (Ax - b) and (de)activates the constraints when
      needed. Returns true in case of a change.*/
    virtual bool evaluateConstraints(const EvalType& etype, EvalStats& stats){
      bool change = false;
      bool active = false;

      /*Consider all constraints*/

      PRINT(this->getb());

      for(int i=0;i<this->mat->getNConstraints();i++){
        AbstractRowConstraint<N, T>* c =
          (AbstractRowConstraint<N, T>*)this->mat->getActiveConstraint(i);

        //c->setBB2Norm(this->bb2norm);
        c->setBB2Norm((T)1.0);
        c->setBB2NormCheck(this->bb2norm);

        bool changed = c->evaluate(*this->getx(),
                                   *this->getb(),
                                   0, etype, stats, &active,
                                   this->getb2());
        if(changed){
          change = true;
        }
      }

      if(change){
        syncConstraints();
        clean_iterations = 0;
      }
      return change;
    }

    /*Reset search, residual and all associated vectors*/
    virtual void resetSearchVector(bool updatePreconditioner,
                                   bool useGradient = false,
                                   bool recomputeru = false){
      changed_iterations++;

      /*Recompute preconditioner for changed constraints*/
      if(updatePreconditioner){
        this->timer.start("prec_update");
        this->recomputePreconditioner();
        this->timer.stop("prec_update");
      }

      /*Re-evaluate rc first*/
      recomputeResidual(scratch1, scratch2, true, recomputeru);

      /*Cr = C * r*/
      this->applyPreconditioner(Cr,  this->r, RightPreconditioning, false);

      if(useGradient){
        /*ACr*/
        spmvc(*scratch1, *this->mat, *Cr);
        /*CACr*/
        this->applyPreconditioner(p,  scratch1, RightPreconditioning, false);
      }else{
        /*p  = Cr*/
        this->timer.start("vector");
        *p = *Cr;
        this->timer.stop("vector");
      }

      /*Compute Apu, Apc*/
      spmvc(*Aup, *this->mat, *p, Conventional);
      spmvc(*Acp, *this->mat, *p, ConstraintsFull);

      {
        this->timer.start("vector");
        VectorC2<T>::add(*Ap, *Aup, *Acp);

        /*ACr = Ap*/
        *ACr  = *Ap;
        *AuCr = *Aup;
        *AcCr = *Acp;
        this->timer.stop("vector");
      }
    }

    bool isConverged(int i, T rr, T rr2, T rr3, T rr4){
      /*Compute both the norm of the residual r and Ar*/

      message("| r|  = %10.10e", rr);
      message("|Ar|  = %10.10e", rr2);
      message("|rc|  = %10.10e", rr3);
      message("|Arc| = %10.10e", rr4);

      if((rr3 < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0) ||
         (rr4 < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0)){
        //return false;
      }else{
        return false;
      }
      //was 100
      if(rr  < (this->eps*this->bb2norm + this->eps) ||
         rr2 < (this->eps*this->bb2norm + this->eps)/(T)1.0){
        /*Method converged, store statistics*/

        this->timer.stop("solve");

        warning("%d iterations", i);
        warning("| r|  = %10.10e", rr);
        warning("|Ar|  = %10.10e", rr2);
        warning("|rc|  = %10.10e", rr3);
        warning("|Arc| = %10.10e", rr4);

        recomputeResidual(scratch1, scratch2, true);

        T res = this->getResidualNorm(scratch1);
        warning("|Ax-ab| = %10.10e", res);
        message("|Ax-ab| = %10.10e", res);

        recomputeResidual(scratch1, scratch2, false);

        res = this->getResidualNorm(scratch1);
        warning("|Ax-ab| = %10.10e", res);
        message("|Ax-ab| = %10.10e", res);

        if(this->exporter){
          this->exporter->setValue("n_iterations",
                                   i);

          this->exporter->setValue("n_clean_iterations",
                                   clean_iterations);

          this->exporter->setValue("n_changed_iterations",
                                   changed_iterations);

          this->exporter->setValue("n_constraints",
                                   this->mat->getNConstraints());

          this->exporter->setValue("n_update_iterations",
                                   n_updated);

          this->exporter->setValue("n_changed_eval_iterations",
                                   n_changed_iter);

          this->exporter->setValue("delassus_operator",
                                   this->timer.getTotalTimeUSec("delassus"));

          this->exporter->setValue("n_delassus_operator",
                                   this->timer.getTotalCalls("delassus"));

          this->exporter->setValue("solverTime",
                                   this->timer.getTotalTimeUSec("solve"));

          this->exporter->setValue("preconditionerTime",
                                   this->timer.getTotalTimeUSec("applyPreconditioner"));

          this->exporter->setValue("preSolverTime",
                                   this->timer.getTotalTimeUSec("solve_pre"));

          this->exporter->setValue("n_preconditionerCalls",
                                   this->timer.getTotalCalls("applyPreconditioner"));

          this->exporter->setValue("evaluationTime",
                                   this->timer.getTotalTimeUSec("evalConstraints"));

          this->exporter->setValue("n_evalCalls",
                                   this->timer.getTotalCalls("evalConstraints"));

          this->exporter->setValue("spmvTime",
                                   this->timer.getTotalTimeUSec("spmv"));

          this->exporter->setValue("spmvCalls",
                                   this->timer.getTotalCalls("spmv"));

          this->exporter->setValue("evaluationTime2",
                                   this->timer.getTotalTimeUSec("evalConstraints2"));

          this->exporter->setValue("n_evalCalls2",
                                   this->timer.getTotalCalls("evalConstraints2"));

          this->exporter->setValue("evaluationTime3",
                                   this->timer.getTotalTimeUSec("evalConstraints3"));

          this->exporter->setValue("n_evalCalls3",
                                   this->timer.getTotalCalls("evalConstraints3"));

          this->exporter->setValue("updateTime",
                                   this->timer.getTotalTimeUSec("updateConstraints"));

          this->exporter->setValue("n_updateCalls",
                                   this->timer.getTotalCalls("updateConstraints"));

          this->exporter->setValue("bNorm",
                                   this->bb2norm);

          this->exporter->setValue("spmv_conventional",
                                   this->timer.getTotalTimeUSec("spmv_conventional"));

          this->exporter->setValue("n_spmv_conventional",
                                   this->timer.getTotalCalls("spmv_conventional"));

          this->exporter->setValue("spmv_pre",
                                   this->timer.getTotalTimeUSec("spmv_constr_pre"));

          this->exporter->setValue("n_spmv_pre",
                                   this->timer.getTotalCalls("spmv_constr_pre"));

          this->exporter->setValue("spmv_constraints",
                                   this->timer.getTotalTimeUSec("spmv_constr"));

          this->exporter->setValue("n_spmv_constraints",
                                   this->timer.getTotalCalls("spmv_constr"));

          this->exporter->setValue("precond_vec",
                                   this->timer.getTotalTimeUSec("precond_vec"));

          this->exporter->setValue("n_precond_vec",
                                   this->timer.getTotalCalls("precond_vec"));

          this->exporter->setValue("check_time",
                                   this->timer.getTotalTimeUSec("checkConstraints"));

          this->exporter->setValue("n_check",
                                   this->timer.getTotalCalls("checkConstraints"));

          this->exporter->setValue("vector", this->timer.getTotalTimeUSec("vector"));
          this->exporter->setValue("n_vector", this->timer.getTotalCalls("vector"));

          this->exporter->setValue("vector_red",
                                   this->timer.getTotalTimeUSec("vector_red"));
          this->exporter->setValue("n_vector_red",
                                   this->timer.getTotalCalls("vector_red"));

          this->exporter->setValue("prec_update",
                                   this->timer.getTotalTimeUSec("prec_update"));
          this->exporter->setValue("n_prec_update",
                                   this->timer.getTotalCalls("prec_update"));
          this->exporter->setValue("prec_scaling_update",
                                   this->timer.getTotalTimeUSec("prec_scaling_update"));
          this->exporter->setValue("n_prec_scaling_update",
                                   this->timer.getTotalCalls("prec_scaling_update"));

          /*Compute FBNorm*/
          scratch1->clear();
          scratch2->clear();
          scratchP->clear();

          for(int i=0;i<this->mat->getNConstraints();i++){
            AbstractRowConstraint<N, T>* c =
              (AbstractRowConstraint<N, T>*)this->mat->getActiveConstraint(i);

            c->computeFBFunction(*scratch1, *scratch2,
                                 *scratchP, *this->C2, *this->x, *this->b, *this->b2);

          }

          //VectorC2<T>::mul(*scratch1, *scratch1, *scratch1);
          //T fbnormnp = Sqrt(scratch1->sum());
          this->exporter->setValue("FBValueNP", scratch1->getAbsMax());

          //VectorC2<T>::mul(*scratch2, *scratch2, *scratch2);
          //T fbnormsf = Sqrt(scratch2->sum());
          this->exporter->setValue("FBValueSF", scratch2->getAbsMax());

          //VectorC2<T>::mul(*scratchP, *scratchP, *scratchP);
          //T fbnormkf = Sqrt(scratchP->sum());
          this->exporter->setValue("FBValueKF", scratchP->getAbsMax());

        }

        tstep++;

        return true;
      }
      return false;
    }

    virtual T getPreconditionedResidualNorm(VectorC2<T>* tmp){
      T correctionFactor = Max((T)this->mat->getNConstraints(), (T)1.0);
      VectorC2<T>::mul(*tmp, *this->r, *Cr);
      return Sqrt(Abs(tmp->oSum() + tmp->cSum()/correctionFactor));
    }

    /*Computing the residual nor using a preconditioner*/
    virtual T getResidualNorm(VectorC2<T>* tmp){
      return IECLinSolveT2<N, T>::getResidualNorm(tmp);
    }

    virtual T getResidualNormC(VectorC2<T>* tmp){
      return IECLinSolveT2<N, T>::getResidualNormC(tmp);
      T correctionFactor = Max((T)this->mat->getNConstraints(), (T)1.0);
      VectorC2<T>::mul(*tmp, *this->r, *Cr);
      return Sqrt(Abs(tmp->cSum()/correctionFactor));
    }

#define PENALTY 3

    virtual void solve(int steps = 10000, T _eps = (T)1e-6){
      cgfassert(this->mat->getWidth() == this->b->getSize());
      cgfassert(this->mat->getWidth() == this->mat->getHeight());
      cgfassert(this->mat->getWidth() == this->x->getSize());

      steps = 100000;

#ifdef CONVERGENCE_EXPORT
      char fileName[255];
      sprintf(fileName, "residual_%d.csv", tstep);

      FILE* file = fopen(fileName, "w");

      convergenceExporter = new CSVExporter(file);
      convergenceExporter->addColumn("residual");
      convergenceExporter->addColumn("residual_c1");
      convergenceExporter->addColumn("residual_ru");
      convergenceExporter->addColumn("residual_rc");
      convergenceExporter->addColumn("residual_ar");
      convergenceExporter->addColumn("residual_ccar");
      convergenceExporter->addColumn("residual_g");
      convergenceExporter->addColumn("alpha");
      convergenceExporter->addColumn("alpha1");
      convergenceExporter->addColumn("alpha2");

      convergenceExporter->addColumn("beta");
      convergenceExporter->addColumn("beta1");
      convergenceExporter->addColumn("beta2");

      convergenceExporter->addColumn("n_kinetic_friction_update");
      convergenceExporter->addColumn("n_kinetic_friction_vector_update");
      convergenceExporter->addColumn("n_friction_check");
      convergenceExporter->addColumn("n_penetration_check");
      convergenceExporter->addColumn("n_volume_check");
      convergenceExporter->addColumn("n_penetration_force");
      convergenceExporter->addColumn("n_volume_force");
      convergenceExporter->addColumn("n_friction_force");
      convergenceExporter->addColumn("n_geometry_check");
      convergenceExporter->addColumn("n_evaluate_all_geometry");

#endif

      iter = 0;
      n_updated = 0;
      n_changed_iter = 0;

      constraintSetModified = false;

      spmv_timer = &this->timer;

      EvalStats firstStats;

      message("matrix height = %d", this->mat->getHeight());
      message("matrix n_elements = %d", this->mat->getNElements());
      message("matrix average row fill = %10.10e", this->mat->getAverageRow());
      message("%d constraints", this->mat->getNConstraints());
      this->eps = _eps;

      this->timer.resetTimers();

      this->timer.start("solve");

      /*-----------------------------------------------------------*/

      /*Performs a raw collision check which populates all candidate
        lists, updates existing constraints and updates vector b*/
      {
        this->timer.start("updateConstraints");
        this->updateConstraints();
        this->timer.stop("updateConstraints");
      }

#ifdef DELASSUS_TEST
      warning("Added for delassus test");
      this->disableAllConstraints();
#endif

      /*Compute norm of b, used for relative error.*/
      /*-----------------------------------------------------------*/
      //this->timer.start("solve_pre");
      this->computeBNorm(scratch1);

      /*Used in stats -> number of iterations that triggered a reset*/
      changed_iterations = 0;

      /*Used in stats -> number of iterations without a reset prior to
        convergence*/
      clean_iterations = 0;

      /*Finalize matrices*/
      this->mat->finalize();

      /*Evaluate and (de)activate constraints*/
      this->b2->clear();

      //this->x->clear(NoConstraints);

      /*Update diagonal preconditioner*/
      this->computePreconditioner();
      this->C2->finalize();

      /*-----------------------------------------------------------*/
      //this->timer.stop("solve_pre");

      /*-----------------------------------------------------------*/
      {
        this->timer.start("evalConstraints2");
        EvalStats stats;
        this->evaluateStart(stats);
        //warning("Enabled for delassus test in order to populate the constraints");
        //return;
        this->timer.stop("evalConstraints2");
      }
      /*-----------------------------------------------------------*/

      /*-----------------------------------------------------------*/
      /*Update kinetic friction values b2 -> ab2*/
      {
        this->timer.start("evalConstraints3");
        this->evaluateAccept();
        this->timer.stop("evalConstraints3");
      }
      /*-----------------------------------------------------------*/

      /*-----------------------------------------------------------*/

      /*Compute r, ru, rc (r = ru+rc), r = b - Ax*/
      spmvc(*scratch1, *this->mat, *this->x, Conventional);

      {
        this->timer.start("vector");
        /*Compute ru = bu - Axu*/
        VectorC2<T>::sub(*ru, *this->b, *scratch1, NoConstraints);
        this->timer.stop("vector");
      }

      {
        this->timer.start("prec_scaling_update");
        this->computePreconditionerScaling();
        this->timer.stop("prec_scaling_update");
      }

      /*Compute r, p and related vectors, and update preconditioner*/
      resetSearchVector(true);
      changed_iterations = 0;

      bool converged   = false; /*If converged, check validity afterwards*/
      bool lastChanged = false;

      int liter = -1;
      int reset_iter = 0;
      int penalty = 0;

      T residual   = this->getResidualNorm(scratch1);
      T residualC  = this->getResidualNormC(scratch1);
      T residual2  = (T)0.0;
      T residualC2 = (T)0.0;

      T residualNorm = this->getResidualNorm(scratch1);
      T lastResidual = (T)1e-3;

      /*-----------------------------------------------------------*/

      bool lastFreeze = false;

      int stepIter = 0;

      //this->mat->computeDelassusOperator();

      bool lastFine = false;

      int nReset = 0;

      //return;

      for(int i=0;i<steps*10;i++){
        EvalStats stats;/*Constraint evaluation statistics for this iteration*/
        if(this->iterCallBack){
          /*Can pause the process!*/
          this->iterCallBack();
        }
        clean_iterations++;
        reset_iter++;
        liter++;
        stepIter++;

        T residualU = this->getResidualNormUnconstrained(scratch1);

        /*Compute |r| and |ACr|*/
        residual  = this->getResidualNorm/*Unconstrained*/(scratch1);
        residualC = this->getResidualNormC(scratch1);

        this->computeBB2Norm(scratch1);

#ifdef CONVERGENCE_EXPORT
        convergenceExporter->setValue("residual", residual);
        convergenceExporter->setValue("residual_c1", residualC);
#endif

        //this->applyPreconditioner(scratch1, ACr, RightPreconditioning, false);

        {
          this->timer.start("vector");
          //VectorC2<T>::mul(*scratch1, *scratch1, *ACr);
          VectorC2<T>::mul(*scratch1, *ACr, *ACr);
          this->timer.stop("vector");
        }

        {
          this->timer.start("vector_red");
          //T correctionFactor = Max((T)this->mat->getNConstraints(), (T)1.0);
          //correctionFactor = 1.0;
          residual2 = (T)1.0*(Sqrt(Abs(scratch1->oSum())) +
                               ACr->getCAbsMax());
          //residual2 = Sqrt(scratch1->sum());
          residualC2 = ACr->getCAbsMax()*(T)1.0;
          this->timer.stop("vector_red");
        }

#ifdef CONVERGENCE_EXPORT
        convergenceExporter->setValue("residual_ar",
                                      Sqrt(Abs(scratch1->sum())));
#endif


#if 1
        message("%d relative error = %10.10e, res = %10.10e, bnorm = %10.10e, res^2 = %10.10e, eps = %10.10e",i ,
                residual/this->bb2norm, residual, this->bb2norm,
                residual*residual, this->eps);
        message("%d relative error = %10.10e, res = %10.10e, bnorm = %10.10e, res^2 = %10.10e, eps = %10.10e",i,
                residual2/this->bb2norm, residual2, this->bb2norm,
                residual2*residual2, this->eps);
        message("residual factor = %10.10e", residual/residual2);
        message("residualU = %10.10e", residualU);

        VectorC2<T>::mul(*scratch1, *this->x, *this->x);
        T vnorm = Sqrt(scratch1->oSum());
        T lnorm = Sqrt(scratch1->cSum());
        VectorC2<T>::add(*scratch1, *this->b, *this->b2);
        VectorC2<T>::mul(*scratch1, *scratch1, *scratch1);
        T bnorm = Sqrt(scratch1->sum());

        VectorC2<T>::mul(*scratch1, *this->r, *this->r);

        message("residual C = %10.10e, norm v = %10.10e, norm l = %10.10e, bnorm = %10.10e",
                residualC, vnorm, lnorm, bnorm);
        message("residual v = %10.10e, l = %10.10e, t = %10.10e",
                Sqrt(scratch1->oSum()), Sqrt(scratch1->getCAbsMax()),
                Sqrt(scratch1->sum()));

        message("residual c2 = %10.10e", residualC2);
#endif

        bool converged2 = converged;

        message("lastFreeze = %d", lastFreeze);
        message("penalty    = %d", penalty);


        if(!lastChanged && penalty <= 0){
#if 1
          if((residual  < (this->eps*this->bb2norm + this->eps) ||
              residual2 < (this->eps*this->bb2norm + this->eps) * (T)1.0) &&
             (residualC < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0
              ||
              residualC2 < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0
              ) &&
             residualU < (this->eps*this->bb2norm + this->eps)){
            converged = true;
          }
#else
          if((residual  < (this->eps*this->bb2norm + this->eps) &&
              residualC < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0)

             ||

             (residual2 < (this->eps*this->bb2norm + this->eps) * (T)1.0 &&
              residualC2 < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0)

             ){
            converged = true;
          }
#endif
          if(converged2){
            converged = true;
          }

          /*Check constraints if converged*/
          bool changed = false;
          bool changed1 = false;

          message("converged  = %d", converged);

          if(converged && !lastFreeze && penalty <= 0){
            /*Perform a full evaluation*/

            lastResidual *= (T)10.0;

            //lastResidual = (T)1e+8;

            warning("Converged -> final check @ %d - %d - %d",
                    i, liter, iter);
            warning("%10.10e, %10.10e", residual, residual2);
            warning("%10.10e, %10.10e", residualC, residualC2);

            bool changed2 = false;
            {
              this->timer.start("evalConstraints2");
              changed2 = false;

              /*evaluateAtConvergence does not clear b2*/
              this->b2->clear();
              changed1 = this->evaluateAtConvergence(stats);
              if(changed1){
                n_changed_iter++;
                penalty = 3;
                message("constraints changed at convergence");
              }else{
                /*Not changed*/
                message("no constraints changed at convergence");
                /*evaluateAtConvergenceFinal clears b2*/
                this->b2->clear();
                changed1 = this->evaluateAtConvergenceFinal(stats);

                if(changed1){
                  message("constraints changed at convergence final");
                  penalty = 3;
                }else{
                  message("no constraints changed at convergence final");
                }
#ifdef DELASSUS_TEST
                /*In case of Delassus test, stop here. At this point
                  we are interested in the set of constraints in order
                  to compute the Delassus operator.*/
                this->timer.stop("evalConstraints2");
                return;
#endif
              }
              this->timer.stop("evalConstraints2");
            }

            /*-----------------------------------------------------------*/

            if(this->firstIteration){
              /*Force the method to continuously update the constraints*/
              //changed1 = false;
            }

            if(!changed1){
            //if(true){
              message("converged & not changed -> check constraints");

              if(false){
                this->b2->clear(); // checkConstraints affects b2
              }

              {
                this->timer.start("checkConstraints");
                /*Verifies constraints and corrects them if needed*/
                changed2 = checkConstraints(false, stats) || changed1;

                if(changed1){
                  this->evaluateAccept();
                }

                /*Check constraints might affect b2 or not. To be sure
                  we reset b2 by running an empty evaluate
                  constraints*/
                this->b2->clear();
                EvalType dummy;
                this->evaluateConstraints(dummy, stats);
                this->timer.stop("checkConstraints");
              }

              if(changed2){
                message("Constraints checked and UPDATED!!\n\n");
                changed     = true;
                lastChanged = true;
                n_updated++;
                penalty = 3;
                /*Recompute ru*/
              }
            }else{
              changed = true;
              lastChanged = true;
              penalty = 3;

              message("Something has changed");
              /*-----------------------------------------------------------*/
              { //friction in b2 -> ab2
                this->timer.start("evalConstraints3");
                this->evaluateAccept();
                this->timer.stop("evalConstraints3");
              }
              /*-----------------------------------------------------------*/
            }

            if(!changed){
              if(isConverged(i, residual, residual2, residualC, residualC2)){

#ifdef CONVERGENCE_EXPORT
                delete convergenceExporter;
                fclose(file);
#endif
                return;
              }
            }
          }

          if(changed){
            /*Some constraints are changed, added or removed,
              re-evaluate residual*/
            {
              //ScopeBenchmarkTimer(this->timer, "vector");
              //*this->b2 = *this->ab2;
            }

            /*Do this only if constraints are added, otherwise it is a
              waste of time*/
            if(constraintSetModified){
              constraintSetModified = false;
              message("constraint set modified, update scaling");
              {
                this->timer.start("prec_scaling_update");
                this->computePreconditionerScaling();
                this->timer.stop("prec_scaling_update");
              }
            }
            if(changed1){
              /*Only constraints are forced/updated, no update of
                preconditioner needed*/
              resetSearchVector(true, false, true); /*Also recomputes ru*/
            }else{
              resetSearchVector(true, false, true); /*Also recomputes ru*/
            }
            residual  = this->getResidualNorm(scratch1);
            residualC = this->getResidualNormC(scratch1);
            //lastResidual = this->getResidualNorm(scratch1);

            {
              this->timer.start("vector");
              VectorC2<T>::mul(*scratch1, *ACr, *ACr);
              residual2 = (Sqrt(scratch1->oSum()) +
                         ACr->getCAbsMax())*(T)1.0;
              //residual2 = Sqrt(scratch1->sum());
              this->timer.stop("vector");
            }

            message("Residual after reset %10.10e, %10.10e", residual, residualC);
            liter = 1;
            if(penalty == 0){
              penalty = 1;
            }
            lastChanged = true;
          }
        }

        converged = false;

        /*Compute CAp*/
        this->applyPreconditioner(CAp1, Ap, RightPreconditioning, false);

        /*Compute rCACr*/
        {
          this->timer.start("vector");
          VectorC2<T>::mul(*scratch2, *Cr, *ACr);
          this->timer.stop("vector");
        }

        {
          this->timer.start("vector_red");
          alpha1 = scratch2->sum();
          this->timer.stop("vector_red");
        }

#ifdef CONVERGENCE_EXPORT
        convergenceExporter->setValue("alpha1", alpha1);
#endif/*CONVERGENCE_EXPORT*/

        {
          this->timer.start("vector");
          /*Compute pACAp*/
          VectorC2<T>::mul(*scratch2, *Ap, *CAp1);
          this->timer.stop("vector");
        }
        //PRINT(*scratch2);

        {
          this->timer.start("vector_red");
          pACAp = scratch2->sum();
          this->timer.stop("vector_red");
        }

#if 1
        if(residual2 <= 1e-13){
          /*Hard rest*/
          *this->x *= (T)0.9999;
#ifdef FRICTION_CONE
          resetSearchVector(false);
#else/*FRICTION_CONE*/
          resetSearchVector(true);
#endif/*FRICTION_CONE*/
          PRINT("hard reset");
          warning("hard reset");
          getchar();
          continue;
        }
#endif
        alpha = alpha1/pACAp;


        /*Compute
          x  = x + alpha * p
          ru = ru - alpha * Apu
          rc = rc - alpha * Apc
          r  = ru + rc
          Cr  = Cr - alpha * CAp*/
        {
          this->timer.start("vector");
          VectorC2<T>::mfadd(*this->x, alpha, *p, *this->x);
          VectorC2<T>::mfadd(*ru, -alpha, *Aup, *ru);
          VectorC2<T>::mfadd(*rc, -alpha, *Acp, *rc);
          VectorC2<T>::add(*this->r, *ru, *rc);
          VectorC2<T>::mfadd(*Cr, -alpha, *CAp1, *Cr);
          this->timer.stop("vector");
        }

        residualNorm = this->getResidualNorm(scratch1);

#ifdef CONVERGENCE_EXPORT
        convergenceExporter->setValue("alpha2", pACAp);

        convergenceExporter->setValue("alpha", alpha);
#endif/*CONVERGENCE_EXPORT*/

        (message("Alpha = %10.10e, %10.10e/%10.10e",
                    alpha, alpha1, pACAp));

        /*If alpha becomes larger than 5, skip the evaluation of the
          constraints*/
        bool freeze = false;
        message("last = %10.10e, current = %10.10e",
                lastResidual, residualNorm);
#if 1
        if( Abs(alpha) > (T)5.0 || (/*residualNorm > 1e-4 &&*/ alpha < 0.0
                                       //&& abs(alpha)>5.0
                                       )// || residualNorm > 1e-1

           ){
          freeze = true;
          PRINT("freeze");
          //abort();
          if(penalty < 1){
            //penalty = 1;
          }
          warning("FREEZE, alpha = %10.10e", alpha);
          //getchar();
          //penalty = 0;
        }
#endif
#if 1
        if(residualNorm < 1e-3){
          lastChanged = false;
        }
#endif
        /*Evaluate constraints given updated x*/
        bool changed   = false;

#ifdef PREC_DIAG
        //penalty = 0;
#endif

        message("penalty = %d, lastChanged = %d", penalty, lastChanged);
        if(/*penalty <= 0 &&*/ !lastChanged && !freeze){
          {
            message("eval");
            this->timer.start("evalConstraints");
            changed = this->evaluateRegular(liter, residual,
                                            residualC, &lastFine, stats);
            this->timer.stop("evalConstraints");
          }

          if(changed){
            /*Friction b2 -> ab2*/
            this->timer.start("evalConstraints3");
            this->evaluateAccept();
            this->timer.stop("evalConstraints3");
          }
          lastChanged = changed;
        }else{
          lastChanged = false;
        }
        //lastChanged = false;
        freeze = freeze;

        penalty--;

        (message("changed @ %d, %d", i, changed));

        /*End constraint evaluation*/

        converged = false;

        if(changed){// || (i % 50 == 0)){
          /*Constraints changed, recompute residual and update preconditioner*/
          lastResidual = Min(residualNorm, lastResidual);
          //lastResidual /= (T)2.0;

          bool fullReset = false;
          if(nReset % 1 == 0){
            fullReset = true;
          }
          fullReset = false;
          nReset++;

#ifdef FRICTION_CONE
          resetSearchVector(false, false, fullReset);
#else/*FRICTION_CONE*/
          resetSearchVector(true, false, fullReset);
#endif/*FRICTION_CONE*/

#ifdef CONVERGENCE_EXPORT
          convergenceExporter->setValue("beta",  (T)0.0);
          convergenceExporter->setValue("beta1", (T)0.0);
          convergenceExporter->setValue("beta2", (T)0.0);
#endif/*CONVERGENCE_EXPORT*/
        }else{
          if(Abs(alpha) < 1e-9){
            warning("alpha = %10.10e", alpha);
            getchar();
            resetSearchVector(false, true);
            continue;
          }

          /*Compute ACr*/
          spmvc(*AuCr, *this->mat, *Cr, Conventional);
          spmvc(*AcCr, *this->mat, *Cr, ConstraintsFull);

          {
            this->timer.start("vector");
            VectorC2<T>::add(*ACr, *AuCr, *AcCr);

            /*Compute beta = -r_{i+1}CACr_{i+1}/r_{i}CACr_{i}*/

            /*Compute rCACr*/
            VectorC2<T>::mul(*scratch2, *ACr, *Cr);
            this->timer.stop("vector");

            this->timer.start("vector_red");
            beta = -scratch2->sum()/alpha1;
            this->timer.stop("vector_red");
          }

          message("Beta = %10.10e/%10.10e = %10.10e", beta*alpha1, alpha1, beta);

#ifdef CONVERGENCE_EXPORT
          convergenceExporter->setValue("beta1", scratch2->sum());
          convergenceExporter->setValue("beta2", alpha1);
          convergenceExporter->setValue("beta" , scratch2->sum()/alpha1);
#endif/*CONVERGENCE_EXPORT*/

          DBG(message("capcap[%d] = %10.10e", 0, alpha1));
          DBG(message("beta[%d]   = %10.10e", 0, beta));

          {
            this->timer.start("vector");
            /*p   = Cr - beta * p */
            VectorC2<T>::mfadd(*p, -beta, *p, *Cr);

            /*Aup = AuCr - beta * Aup */
            VectorC2<T>::mfadd(*Aup, -beta, *Aup, *AuCr);

            /*Acp = AcCr - beta * Acp */
            VectorC2<T>::mfadd(*Acp, -beta, *Acp, *AcCr);

            /*Ap = Aup + Acp*/
            VectorC2<T>::add(*Ap,  *Aup,  *Acp);
            this->timer.stop("vector");
          }
        }
        iter++;
#ifdef CONVERGENCE_EXPORT

        convergenceExporter->setValue("n_kinetic_friction_update",
                                      stats.n_kinetic_friction_update);
        convergenceExporter->setValue("n_kinetic_friction_vector_update",
                                      stats.n_kinetic_friction_vector_update);
        convergenceExporter->setValue("n_friction_check",
                                      stats.n_friction_check);
        convergenceExporter->setValue("n_penetration_check",
                                      stats.n_penetration_check);
        convergenceExporter->setValue("n_volume_check",
                                      stats.n_volume_check);
        convergenceExporter->setValue("n_penetration_force",
                                      stats.n_penetration_force);
        convergenceExporter->setValue("n_volume_force",
                                      stats.n_volume_force);
        convergenceExporter->setValue("n_friction_force",
                                      stats.n_friction_force);
        convergenceExporter->setValue("n_geometry_check",
                                      stats.n_geometry_check);

        convergenceExporter->saveRow();
#endif/*CONVERGENCE_EXPORT*/
      }
      throw new SolutionNotFoundException(__LINE__, __FILE__,
                                          "Number of iterations exceeded.");
    }

  protected:
    VectorC2<T>* ru;   /*Unconstrained residual*/
    VectorC2<T>* rc;   /*Constraint residual*/
    VectorC2<T>* p;    /*Search vector*/
    VectorC2<T>* Ap;   /*A * p = (Au + Ac) * p*/
    VectorC2<T>* Aup;  /*Au * p*/
    VectorC2<T>* Acp;  /*Ac * p*/
    VectorC2<T>* Cr;   /*Preconditioned residual*/
    VectorC2<T>* ACr;  /*A * C * r = (Au + Ac) * C * r*/
    VectorC2<T>* AuCr; /*Au * C * r*/
    VectorC2<T>* AcCr; /*Ac * C * r*/
    VectorC2<T>* CAp1; /*C * A * p_i*/

    VectorC2<T>* scratch1;
    VectorC2<T>* scratch2;
    VectorC2<T>* scratchP;

    T pACAp;
    T alpha;
    T alpha1;
    T beta;

    int clean_iterations;
    int changed_iterations;

    int iter;
    int n_updated;
    int n_changed_iter;
  protected:
    void syncConstraints(){
    }

    bool constraintSetModified;

#ifdef CONVERGENCE_EXPORT
    CSVExporter* convergenceExporter;
#endif
  };
}

#endif/*IECLINSOLVE_CR_HPP*/
