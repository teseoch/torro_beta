/* Copyright (C) 2012-2019 by Mickeal Verschoor */

namespace CGF{
  template<class T, class U, int N>
  class RunningAverage{
  public:
    RunningAverage(){
      lastSample = -1;
      nSamples = 0;
      nTotalSamples = 0;
    }

    void addSample(const T& val){

      if(IsNan(val)){
        error("input is NAN");
      }

      lastSample++;
      lastSample = lastSample % N;
      samples[lastSample] = val;

      if(nSamples < N){
        nSamples++;
      }

      cumulative = (val + cumulative * (U)nTotalSamples)/(U)(nTotalSamples + 1);
      nTotalSamples ++;
    }

    void clear(){
      nSamples = 0;
      nTotalSamples = 0;
      cumulative = T();
      lastSample = -1;
    }

    T getAverage()const{
      T sum = T();
      if(nSamples == 0){
        return sum;
      }
      //message("Running average samples::");
      for(int i=0;i<nSamples;i++){
        sum += samples[i];
        //std::cout << samples[i] << std::endl;
      }
      //message("Average::");
      //std::cout << (U)(1.0 / nSamples) * sum << std::endl;

      return (U)(1.0 / nSamples) * sum;
    }

    T getCumulativeAverage()const{
      return cumulative;
    }

    T getWeightedAverage()const{
      T sum = T();
      if(nSamples == 0){
        return sum;
      }
      //message("Weighted running average samples::");

      int sumWeights = 0;

      for(int i=0, j=lastSample;i<nSamples;i++, j--){
        sum += (U)(j+5) * samples[i];
        sumWeights += (j+5);
        //std::cout << (U)(j+5) * samples[j] << " " << samples[j] << " " << j+5 << std::endl;
        if(j <= 0){
          j += nSamples;
        }
      }
      //message("Weighted average::");
      //std::cout << (U)(1.0 / sumWeights) * sum << std::endl;

      return (U)(1.0 / sumWeights) * sum;
    }

    T getReverseWeightedAverage()const{
      T sum = T();
      if(nSamples == 0){
        return sum;
      }
      //message("RWeighted running average samples::");

      int sumWeights = 0;

      for(int i=0, j=lastSample;i<nSamples;i++, j--){
        //message("last = %d", lastSample);
        //message("j    = %d", j);
        sum += (U)(nSamples - j + 5) * samples[i];
        sumWeights += (nSamples - j + 5);
        //std::cout << (U)(nSamples - j + 5) * samples[j] << " " << samples[j] << " " << nSamples - j + 5 << std::endl;
        if(j <= 0){
          j += nSamples;
        }
      }
      //message("RWeighted average::");
      //std::cout << (U)(1.0 / sumWeights) * sum << std::endl;

      return (U)(1.0 / sumWeights) * sum;
    }

  protected:
    T samples[(uint)(N)];
    T cumulative;
    int lastSample;
    int nSamples;
    int nTotalSamples;
  };
}
