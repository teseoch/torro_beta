/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef IECLINSOLVE_HPP
#define IECLINSOLVE_HPP

#include "math/SpMatrixC.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"
#include "datastructures/AdjacencyList.hpp"
#include "core/BenchmarkTimer.hpp"

namespace CGF{
  class CSVExporter;

  enum PreconditionerApplication{LeftPreconditioning,
                                 RightPreconditioning};

  template<class T>
  class IECEvaluator{
  public:
    virtual void evaluateStart(EvalStats&) = 0;
    virtual bool evaluateAtConvergence(EvalStats&) = 0;
    virtual bool evaluateAtConvergenceFinal(EvalStats&) = 0;
    virtual bool evaluateRegular(int iter, T r1, T r2, bool* fine, EvalStats&) = 0;
    virtual void evaluateAccept() = 0;
  };

  template<int N, class T>
  class IECLinSolveT2{
  public:
    typedef VectorC2<T>      VectorC;
    typedef SpMatrixC2<N, T> SpMatrixC;
    typedef typename List<VectorC*>::Iterator VectorCIterator;

    IECLinSolveT2(int d):dim(d){
      mat = new SpMatrixC(dim, dim);
      b   = new VectorC(dim);
      x   = new VectorC(dim);
      x1  = new VectorC(dim);
      b2  = new VectorC(dim);
      ab2 = new VectorC(dim);
      r   = new VectorC(dim);

      vectors.append(b);
      vectors.append(x);
      vectors.append(x1);
      vectors.append(b2);
      vectors.append(ab2);
      vectors.append(r);

      /*Initialize a dummy, will be removed when the constraints are
        distributed.*/
      constraintConnectivity = new AdjacencyList(d,1);
      activeConstraints      = new Tree<int>();

      mat->activeConstraints = activeConstraints;

      b  ->clear();
      b2 ->clear();
      ab2->clear();
      x  ->clear();
      x1 ->clear();
      r  ->clear();

      exporter = 0;
      eps      = (T)1E-6;
      mnorm    = (T)1E-6;

      externalAllocatedMat = false;
      externalAllocatedb   = false;
      externalAllocatedb2  = false;
      externalAllocatedx   = false;

      iterCallBack = 0;

      firstIteration = false;
    }

    virtual ~IECLinSolveT2(){
      if(!externalAllocatedMat){
        delete mat;
      }
      if(!externalAllocatedb){
        delete b;
      }
      if(!externalAllocatedb2){
        delete b2;
      }

      delete ab2;

      if(!externalAllocatedx){
        delete x;
      }

      delete x1;
      delete r;
      delete constraintConnectivity;
      delete activeConstraints;
    }

    SpMatrixC* getMatrix(){
      return mat;
    }

    VectorC* getb(){
      return b;
    }

    VectorC* getb2(){
      return b2;
    }

    VectorC* getx(){
      return x;
    }

    VectorC* getx1(){
      return x1;
    }

    void setIterationCallBack(void(*cb)(void)){
      iterCallBack = cb;
    }

    virtual void setb(VectorC* vec){
      cgfassert(vec->getSize() == b->getSize());
      if(!externalAllocatedb){
        vectors.remove(b);
        delete b;
      }
      b = vec;
      externalAllocatedb = true;
    }

    virtual void setb2(VectorC* vec){
      cgfassert(vec->getSize() == b2->getSize());
      if(!externalAllocatedb2){
        vectors.remove(b2);
        delete b2;
      }
      b2 = vec;
      externalAllocatedb2 = true;
    }

    virtual void setx(VectorC* vec){
      cgfassert(vec->getSize() == x->getSize());
      if(!externalAllocatedb){
        vectors.remove(x);
        delete x;
      }
      x = vec;
      externalAllocatedx = true;
    }

    virtual void setMatrix(SpMatrixC* m){
      cgfassert(m->getWidth()  == mat->getWidth());
      cgfassert(m->getHeight() == mat->getHeight());
      if(!externalAllocatedMat){
        delete mat;
      }

      mat = m;
      externalAllocatedMat = true;
    }

    int getDim()const{
      return dim;
    }

    T getEpsilon()const{
      return eps;
    }

    T getRelativeEpsilon()const{
      return eps * bnorm + eps;
    }

    /*Computes the norm of all multipliers involved*/
    void computeMultiplierNorm(VectorC* tmp){
      this->timer.start("vector");
      VectorC::mul(*tmp, *x, *x, Constraints);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      mnorm = Sqrt(tmp->cSum());
      this->timer.stop("vector_red");
    }

    /*Computes norm of b*/
    void computeBNorm(VectorC* tmp){
      this->timer.start("vector");
      VectorC::mul(*tmp, *this->b, *this->b);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      bnorm = Sqrt(tmp->sum());
      this->timer.stop("vector_red");
    }

    void computeBB2Norm(VectorC* tmp){
      this->timer.start("vector");
      VectorC::add(*tmp, *this->b, *this->b2);
      VectorC::mul(*tmp, *tmp, *tmp);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      bb2norm = Sqrt(tmp->oSum());
      this->timer.stop("vector_red");
      //bb2norm = 10.0*10.0;
    }

    void computeB2Norm(VectorC* tmp){
      this->timer.start("vector");
      VectorC::mul(*tmp, *this->b2, *this->b2);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      b2norm = Sqrt(tmp->sum());
      this->timer.stop("vector_red");
    }

    void computeAB2Norm(VectorC* tmp){
      this->timer.start("vector");
      VectorC::mul(*tmp, *this->ab2, *this->ab2);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      ab2norm = Sqrt(tmp->sum());
      this->timer.stop("vector_red");
    }

    virtual T getResidualNorm(VectorC* tmp){
      //T correctionFactor = (T)1.0;//Max((T)this->mat->getNConstraints(), (T)1.0);
      this->timer.start("vector");
      VectorC::mul(*tmp, *this->r, *this->r);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      //T ret =  Sqrt(tmp->oSum() + tmp->cSum()/correctionFactor);
      T ret = Sqrt(tmp->oSum()) + this->r->getCAbsMax();
      this->timer.stop("vector_red");

      return ret;
    }

    virtual T getResidualNormUnconstrained(VectorC* tmp){
      this->timer.start("vector");
      VectorC::mul(*tmp, *this->r, *this->r);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      T ret =  Sqrt(tmp->oSum());
      this->timer.stop("vector_red");

      return ret;
    }

    virtual T getResidualNormC(VectorC* tmp){
      //T correctionFactor = (T)1.0;//Max((T)this->mat->getNConstraints(), (T)1.0);
      this->timer.start("vector");
      //VectorC::mul(*tmp, *this->r, *this->r);
      T res = this->r->getCAbsMax();
      this->timer.stop("vector");

      //this->timer.start("vector_red");
      //T ret = Sqrt(tmp->cSum()/correctionFactor);

      //this->timer.stop("vector_red");

      return res;
    }

    T getMultiplierNorm()const{
      return mnorm;
    }

    void setExporter(CSVExporter* e){
      exporter = e;
    }

    virtual bool checkConstraints(bool friction, EvalStats& stats) = 0;

    virtual bool evaluateConstraints(const EvalType& etype,
                                     EvalStats& stats) = 0;

    virtual void disableAllConstraints() = 0;

    virtual void solve(int steps = 10000, T eps = (T)1e-6)=0;

    /*Clears all values in vec associated with an inactive
      constraint*/
    virtual void resetInactive(VectorC& vec){
      int size = vec.getSize();

      for(int i=0;i<this->mat->constraints.getNActive();i++){
        int idx = this->mat->constraints.activeIndex(i);

        if(this->mat->constraints[idx]->getStatus() != Active){
          int offset = this->mat->constraints[idx]->getOffset();
          int row_id = this->mat->constraints[idx]->row_id;

          for(int j=0;j<offset;j++){
            vec[size + row_id * offset + j] = 0;
          }
        }
      }
    }


    /*Is called when constraints are changed. It copies the constraint
      values from the constraints into vector b. In overriden
      functions, updateConstraints can also perform a domain specific
      update of the constraints by adding or removing
      constraints. After that is done, this function should be
      called.*/

    virtual void updateConstraints(){
      DBG(message("update constraints"));
      int size = this->b->getSize();
      PRINT_FUNCTION;

      for(int i=0;i<this->mat->getNConstraints();i++){
        int idx = this->mat->constraints.activeIndex(i);

        AbstractRowConstraint<N, T>* c =
          (AbstractRowConstraint<N, T>*)this->mat->constraints[idx];

        for(int j=0;j<c->getOffset();j++){
          T val = c->getConstraintValue(j);
          (*this->b)[size + c->row_id * c->getOffset() + j] = val;
        }
      }
    }

    /*Add constraint to matrix and vectors*/
    virtual int addConstraint(AbstractRowConstraint<N, T>* c){
      int idx = this->mat->addConstraint(c);

      if(c->getSize() == 0){
        error("Empty constraint added, so we can't maintain a connectivity graph of all constraints.");
      }


      this->mat->activeConstraints = activeConstraints;

      int updatedSize = this->mat->constraints.getSize();

      c->init(this->mat);

      int offset = c->getOffset();
      int row_id = c->row_id;
      int size   = this->b->getSize();

      VectorCIterator it = this->vectors.begin();

      while(it != this->vectors.end()){
        VectorC* vv = *it++;

        /*Update size of vectors if needed*/
        vv->extendConstraints(updatedSize * c->getOffset());

        for(int j=0;j<offset;j++){
          if(vv == this->b){
            (*vv)[size + row_id * offset + j] = c->getConstraintValue(j);
          }else{
            (*vv)[size + row_id * offset + j] = 0;
          }
        }
      }

      /*Store connectivity information. Store only unique indices*/
      Tree<int> collection;
      for(int i=0;i<c->getSize();i++){
        if(c->getIndex(i) != Constraint::undefined){
          int index = c->getIndex(i);
          collection.uniqueInsert(index);
        }
      }

      Tree<int>::Iterator cit = collection.begin();
      while(cit != collection.end()){
        constraintConnectivity->connect(*cit++, idx);
      }

      return idx;
    }

    virtual void removeConstraint(int id){
      AbstractRowConstraint<N, T>* c =
        (AbstractRowConstraint<N, T>*)this->mat->getConstraint(id);

      /*Make unique*/
      Tree<int> collection;
      for(int i=0;i<c->getSize();i++){
        if(c->getIndex(i) != Constraint::undefined){
          int index = c->getIndex(i);
          collection.uniqueInsert(index);
        }
      }

      Tree<int>::Iterator cit = collection.begin();
      while(cit != collection.end()){
        constraintConnectivity->disconnect(*cit++, id);
      }

      int row_id = c->row_id;
      int offset = c->getOffset();
      int size   = this->b->getSize();

      c->destroy(*this->b, *this->x);

      this->mat->removeConstraint(id);

      VectorCIterator it = this->vectors.begin();

      while(it != this->vectors.end()){
        VectorC* curVec = *it++;

        for(int j=0;j<offset;j++){
          (*curVec)[size + row_id * offset + j] = 0;
        }
      }
    }

    List<VectorC*> getVectorList() const{
      return vectors;
    }

    AdjacencyList* getConnectivityList() const{
      return constraintConnectivity;
    }

    void setFirstIteration(bool first){
      firstIteration = first;
    }

    BenchmarkTimer* getTimer(){
      return &timer;
    }

  protected:
    int dim;
    SpMatrixC* mat;
    VectorC* b;
    VectorC* b2;
    VectorC* ab2;
    VectorC* x;
    VectorC* x1;
    VectorC* r;

    List<VectorC*> vectors;            /*Contains all vectors present
                                         in the numerical method. When
                                         the addition of a constraint
                                         requires additional storage,
                                         these vectors are extended.*/

    AdjacencyList* constraintConnectivity; /*Stores information of
                                             constraints applied to
                                             the same node.*/

    Tree<int>* activeConstraints;  /*A tree containing indices of all
                                     active constraints, used by all
                                     associated vectors and matrices
                                     in order to find active
                                     constraints fast.*/

    CSVExporter* exporter;
    BenchmarkTimer timer;

    T eps;
    T mnorm;
    T bnorm;
    T b2norm;
    T bb2norm;
    T ab2norm;

    bool externalAllocatedMat;
    bool externalAllocatedb;
    bool externalAllocatedb2;
    bool externalAllocatedx;

    bool firstIteration;

    /*Called at the beginning of each iteration in order to interrupt
      the process in order to check things out*/
    void (*iterCallBack)(void);
  };
}

#endif/*IECLINSOLVE_HPP*/
