/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CONSTRAINTSETFACE_HPP
#define CONSTRAINTSETFACE_HPP

#include "math/BBox.hpp"
#include "datastructures/List.hpp"

namespace CGF{

  template<class T>
  class CollisionContext;

  template<class T>
  class ConstraintSetFaces{
  public:
    ConstraintSetFaces();
    virtual ~ConstraintSetFaces();

    void update(CollisionContext<T>* n);

    int faceId;
    List<int> candidates;
  };
}

#endif/*CONSTRAINTSETFACE_HPP*/
