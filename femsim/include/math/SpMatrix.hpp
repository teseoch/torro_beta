/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef SPMATRIX_HPP
#define SPMATRIX_HPP

#include "core/cgfdefs.hpp"
#include "math/Math.hpp"
#include "math/SpMatrixBlock.hpp"
#include "datastructures/Tree.hpp"
#include "datastructures/List.hpp"
#include "math/SpMatrixReorder.hpp"

#include <ostream>
#include <map>
#include <stdio.h>
#ifdef CUDA
/*Number of blocks processed simultaneously*/
#define SIM_BLOCKS  2

/*A block row is divided in a number of segment of size SEG_LENGTH*/
#define SEG_LENGTH  8

#define THREADS 16
#endif

namespace CGF{

  template<class T>
  class CGFAPI Vector;

  /*typedef struct _block_index block_index_t;
    struct _block_index{
    int col;
    int index;
    };*/

  typedef struct _reverse_index reverse_index_t;
  struct _reverse_index{
    int blockRow;
    int blockCol;
  };

  template<int N, class T>
  class CGFAPI SpMatrix;

  template<int N, class T>
  class CGFAPI Cholesky;

#ifdef USE_THREADS
  class CGFAPI ThreadPool;

  template<int N, class T>
  class CGFAPI ParallelSPMVTask;
#endif

  template<int N, class T>
  class CGFAPI SpMatrixTuple{
  public:
    T val;
    int row;
    int col;
  };

  template<int N, class T>
  class Compare<SpMatrixTuple<N, T> >{
  public:
    static bool less(const SpMatrixTuple<N, T>& a,
                     const SpMatrixTuple<N, T>& b){
      if(a.row/N < b.row/N){
        return true;
      }else if(a.row/N == b.row/N){
        if(a.col/N < b.col/N){
          return true;
        }
      }
      return false;
    }

    static bool equal(const SpMatrixTuple<N, T>& a,
                      const SpMatrixTuple<N, T>& b){
      if(a.row/N == b.row/N && a.row/N == b.row/N){
        return true;
      }
      return false;
    }
  };

  template<int N, class T>
  class CGFAPI SpRowProxy{
  public:
    SpRowProxy(int r, const SpMatrix<N, T>* const cm);
    SpRowProxy(int r, SpMatrix<N, T>* m);

    T& operator[](int c);
    T operator[](int c) const;

  protected:
    template <int, class>
      friend class SpMatrix;

    SpRowProxy(){
    }

    SpMatrix<N, T>* matrix;
    const SpMatrix<N, T>* cmatrix;
    int row;
  };

  template<int N, class T>
  class CGFAPI SpMatrix{
  public:
    friend SpMatrix<N, T>* reorderRCM<N, T>(const SpMatrix<N, T>*);
    friend SpMatrix<N, T>* reorderKING<N, T>(const SpMatrix<N, T>*,
                                             int* map, int*rmap);
    friend SpMatrix<N, T>* reorderMMD<N, T>(const SpMatrix<N, T>*);
    friend SpMatrix<N, T>* reorderMy<N, T>(const SpMatrix<N, T>*);
    friend class Cholesky<N, T>;

    /*Constructors*/
    SpMatrix();
    SpMatrix(int w, int h);

    /*Copy constructor*/
    SpMatrix(const SpMatrix<N, T>& m);

    /*Destructor*/
    virtual ~SpMatrix();

    /*Assignment operator*/
    SpMatrix<N, T>& operator=(const SpMatrix<N, T>& m);

    /*Enables direct manipulation of the data items. If the element
      does not exists then this function will create the element for
      you!*/
    SpRowProxy<N, T> operator[](int i){
      cgfassert(i<origHeight);
      return SpRowProxy<N, T>(i, this);
    }

    /*If an element does not exist, this function will not create the
      memory block but returns the zero element*/
    const SpRowProxy<N, T> operator[](int i) const{
      cgfassert(i<origHeight);
      const SpMatrix<N, T>* cmatrix = (const SpMatrix<N, T>*)this;
      return SpRowProxy<N, T>(i, cmatrix);
    }

    T getValue(int row, int col)const;

    void setElements(List<SpMatrixTuple<N, T> >& elements);
    void setElements(Tree<SpMatrixTuple<N, T> >& elements);

    /*Sparse matrix sparse matrix multiplication*/
#if 1
	/*Not available yet*/
    SpMatrix<N, T>  operator* (const SpMatrix<N, T>& m)const;
    SpMatrix<N, T>& operator*=(const SpMatrix<N, T>& m);

    static void multiply(SpMatrix<N, T>* r,
                         const SpMatrix<N, T>* A, const SpMatrix<N, T>* B);
#endif

    /*Add similar sparse matrices together*/
    SpMatrix<N, T>  operator+ (const SpMatrix<N, T>& m) const;
    SpMatrix<N, T>& operator+=(const SpMatrix<N, T>& m);
    SpMatrix<N, T>  operator- (const SpMatrix<N, T>& m) const;
    SpMatrix<N, T>& operator-=(const SpMatrix<N, T>& m);

#if 0
    /*Turns the sparse matrix into a full matrix! Perhaps not a good
      idea*/
    SpMatrix<N, T>  operator+ (T f) const;
    SpMatrix<N, T>& operator+=(T f);
    SpMatrix<N, T>  operator- (T f) const;
    SpMatrix<N, T>& operator-=(T f);
#endif

    /*Multiply/divide each non zero value*/
    SpMatrix<N, T>  operator* (T f) const;
    SpMatrix<N, T>& operator*=(T f);
    SpMatrix<N, T>  operator/ (T f) const;
    SpMatrix<N, T>& operator/=(T f);

    /*Helper function for full multiplication*/
    template<int M, class TT>
      friend TT multiplyRows(SpMatrix<M, TT>& A, int rowA,
                             SpMatrix<M, TT>& B, int rowB);

    /*Test binary relations. The result of an (in)equality test is a
      binary matrix.*/

    /*Binary equality tests*/
    template<int M, class TT>
      friend SpMatrix<M, TT> operator==(const SpMatrix<M, TT>& m, TT n);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator!=(const SpMatrix<M, TT>& m, TT n);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator==(TT n, const SpMatrix<M, TT>& m);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator!=(TT n, const SpMatrix<M, TT>& m);

    /*Binary inequality tests*/
    template<int M, class TT>
      friend SpMatrix<M, TT> operator< (const SpMatrix<M, TT>& m, TT n);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator<=(const SpMatrix<M, TT>& m, TT n);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator> (const SpMatrix<M, TT>& m, TT n);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator>=(const SpMatrix<M, TT>& m, TT n);

    template<int M, class TT>
      friend SpMatrix<M, TT> operator< (TT n, const SpMatrix<M, TT>& m);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator<=(TT n, const SpMatrix<M, TT>& m);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator> (TT n, const SpMatrix<M, TT>& m);
    template<int M, class TT>
      friend SpMatrix<M, TT> operator>=(TT n, const SpMatrix<M, TT>& m);

    /*Set one singe value and increment the number of stored
      elements*/
    void set(int row, int col, T value){
      (*this)[row][col] = value;
      n_elements++;
    }

    //SpMatrix<N, T>* reorderRCM()const;
    //SpMatrix<N, T>* reorderKing()const;
    //SpMatrix<N, T>* reorderMMD()const;
    //SpMatrix<N, T>* reorderMy()const;

    T powerMethod(T tol = (T)1e-6, T shift = (T)0.0,
                  int firstRow = -1, int lastRow = -1);

    T symmetricPowerMethod(T tol = (T)1e-6, T shift = (T)0.0,
                           int firstRow = -1, int lastRow = -1);
    T infiniteNorm();
    T conditionNumber(bool symmetric,
                      T& evMin, T& evMax,
                      T tol = (T)1e-6,
                      int firstRow = -1, int lastRow = -1);


    void columnwiseInverseCG(SpMatrix<N, T>& M);


    void clear();
    void clearStructure();

    void analyse()const;
    int getNElements()const;
    int getNBlocks()const;
    int getShortestRow()const;
    int getLongestRow()const;
    float getAverageRow()const;
    float getAverageBlockFill()const;

    SpMatrix<N, T> getTranspose()const;
    void getTranspose(SpMatrix<N, T>& out)const;

    /*Candidate for removal, use LinSolve classes instead*/
#if 0
    Vector<T>* solveSystemCG(const Vector<T>* b);
    void       solveSystemCG(const Vector<T>* b, Vector<T>* x);
    Vector<T>* solveSystemBICGSTAB(const Vector<T>* b);
    void       solveSystemBICGSTAB(const Vector<T>* b, Vector<T>* x);
    Vector<T>* solveSystemBICGStabl(const Vector<T>* b, int l);
    void       solveSystemBICGStabl(const Vector<T>* b, int l, Vector<T>* x,
                                    int iter=1000);
    Vector<T>* solveSystemCGParallel(const Vector<T>* b, ThreadPool* pool);
    Vector<T>* solveSystemBICGSTABParallel(const Vector<T>* b,
                                           ThreadPool* pool);
#endif
#ifndef NO_CUDA
    Vector<T>* solveSystemCGCuda(const Vector<T>* b, ThreadPool* pool);
#endif

    /*Multiplies matrix M = (D+U+L) with v as D^{-1}(U+L)v*/
    template<int M, class TT>
      friend void multiplyJacobi(Vector<TT>& r, const SpMatrix<M, TT>& m,
                                 const Vector<TT>& v);

    template<int M, class TT>
      friend void multiplyGaussSeidel(Vector<TT>& r, const SpMatrix<M, TT>& m,
                                      const Vector<TT>& v,
                                      const Vector<TT>* b);

    template<int M, class TT>
      friend void spmv(Vector<TT>& r, const SpMatrix<M, TT>& m,
                       const Vector<TT>& v);

    template<int M, class TT>
      friend void spmv_t(Vector<TT>& r, const SpMatrix<M, TT>& m,
                         const Vector<TT>& v);

    template<int M, class TT>
      friend void spmv_parallel(Vector<TT>& r, const SpMatrix<M, TT>& m,
                                const Vector<TT>& v);

    template<int M, class TT>
      friend void spmv_partial(Vector<TT>& r, const SpMatrix<M, TT>& m,
                               const Vector<TT>& v, const MatrixRange rg);

    template<int M, class TT>
      friend Vector<TT> operator*(const SpMatrix<M, TT>&m, const Vector<TT>&v);

    template<int M, class TT>
      friend SpMatrix<M, TT> operator*(const SpMatrix<M, TT>&m,
                                       const SpMatrix<M, TT>&v);

    template<int M, int MM, class TPA, class TPB>
      friend void convertMatrix(SpMatrix<M, TPA> & a,
                                const SpMatrix<MM, TPB>& b);

    int getWidth()const {return origWidth;}
    int getHeight()const {return origHeight;}

    int getOrigWidth()const {return origWidth;}
    int getOrigHeight()const {return origHeight;}



    void printRow(int row)const;

    template<int M, class TT>
      friend std::ostream& operator<<(std::ostream& stream, const SpMatrix<M, TT>& m);

    void finalize(){
      if(!finalized){
        reorderBlocks();

        /*Allocate new temp buffer for spmv_t*/
        //if(tBlocks){
        //  delete[]tBlocks;
        //}

        //tBlocks = new SpMatrixBlock<N, T>[width/N];

        finalized = true;
      }
    }

    bool isFinalized()const{
      return finalized;
    }

    void unFinalize(){
      finalized = false;
    }

    //void finalizeParallel(ThreadPool* pool);

    void computeBlockDistribution(MatrixRange* mRange,
                                  VectorRange* vRange,
                                  int* n_blocks,
                                  int n_segments) const;

#if 0
    void setBandwidth(int bw){
      bandwidth = bw;
    }

    int getBandwidth(){
      return bandwidth;
    }
#endif

    SpMatrix<N, T> getSubMatrix(int width, int height)const{
      SpMatrix<N, T> ret(width, height);

      for(int i=0;i<height/N;i++){
        for(int j=0;j<row_lengths[i];j++){
          int block_index = block_indices[i][j];
          int col_index   = col_indices[i][j] * N;
          int idx = 0;
          for(int k=0;k<N;k++){
            for(int l=0;l<N;l++){
              T bval = blocks[block_index].m[idx++];
              if(bval != (T)0.0){
                if(i*N + k < height && col_index + l < width){
                  ret[i*N + k][col_index + l] = (T)bval;
                }
              }
            }
          }
        }
      }
      return ret;
    }

  protected:
    template<int M, class TT>
      friend class LinSolveGS;

    template<int M, class TT>
      friend class LinSolveBGS;

    template<int M, class TT>
      friend class IECLinSolveGS;

    template<int M, class TT>
      friend class IECLinSolveBGS;

    template<int M, class TT>
      friend class SpRowProxy;

    template<class TT>
      friend class Vector;

    template<int M, class TT>
      friend class ParallelCGCudaTask;

    template<int M, class TT>
      friend class ParallelSPMVCudaTask;

    template<int M, class TT>
      friend class ParallelCGTask;

    template<int M, class TT>
      friend class ParallelSPMVTask;

    template<int M, class TT>
      friend class CSpMatrix;

    template<int M, class TT>
      friend bool save_matrix_matlab(const char* filename,
                                     const SpMatrix<M, TT>* const mat);

    template<int M, class TT>
      friend void save_matrix_market_exchange(const char* filename,
                                              const SpMatrix<M, TT>* const mat);

    int width;
    int height;
    int origWidth;
    int origHeight;

  public:
    //std::map<int, int>* block_map; /*Block lookup map*/
    Tree<int>* block_map;
    int** col_indices;      /*Indices to blocks for each row*/
    int** row_indices;
    int** block_indices;    /*Indices to blocks for each row*/
    int** block_indices_col;
    int* comp_col_indices;  /*Compressed column indices multiplied with N*/
    int* comp_row_indices;  /*Compressed row indices multiplied with N*/
    int* row_lengths;       /*Stores for each row the length*/
    int* col_lengths;
    int* allocated_length_row;  /*Stores for each row the allocated length */
    int* allocated_length_col;  /*Stores for each col the allocated length */
    SpMatrixBlock<N, T>* blocks; /*Blocks*/
    int n_allocated_blocks; /*Stores the number of allocated blocks*/
    int n_blocks;           /*Number of stored blocks*/
    int n_elements;         /*Number of elements*/
  protected:
    void reorderBlocks();    /*Reorder blocks such that all blocks are
                               stored in a cache coherent order*/
    void grow_row(int row); /*Extends the storage of a row*/
    void grow_col(int col); /*Extends the storage of a col*/
    void grow_blocks();      /*Extends the storafe of for the blocks*/
    SpRowProxy<N, T> proxy;  /*Proxy object for [][] operator*/
    //ParallelSPMVTask<N, T>*task;/*Task descriptor*/
    //ThreadPool* pool;        /*Pool associated with task descriptor*/
    bool finalized;          /*True if the matrix is finalized. If the
                               matrix is altered after finalization,
                               this value becomes false*/

    SpMatrixBlock<N, T>* tBlocks; /*Temporary buffer for storing the
                                    intermediate results for a
                                    transposed multiplication*/
#if 0
    CSVExporter* exporter;   /*Exporter*/
    int bandwidth;
#endif
  };

#ifdef CUDA
  void cuda_sum();
#endif

  template<int N, class T>
  SpRowProxy<N, T>::SpRowProxy(int r, const SpMatrix<N, T>* const cm){
    row = r;
    cmatrix = cm;
    matrix = 0;
  }

  template<int N, class T>
  SpRowProxy<N, T>::SpRowProxy(int r, SpMatrix<N, T>* m){
    row = r;
    matrix = m;
    cmatrix = 0;
  }

  template<int N, class T>
  T& SpRowProxy<N, T>::operator[](int c){
    int col = c;
    bool block_exists = false;
    int block_index;
    int block_row_col;
    int block_row_index = row/N;
    int block_col_index = col/N;

    if(!(block_col_index < matrix->width  / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    if(!(block_row_index < matrix->height / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    cgfassert(col < matrix->origWidth);

    cgfassert(block_col_index < matrix->width  / N);
    cgfassert(block_row_index < matrix->height / N);

    /*Find target block*/
#if 0
    for(int i=0;i<matrix->row_lengths[block_row_index];i++){
      if(matrix->col_indices[block_row_index][i] == block_col_index){
        block_exists = true;
        block_row_col = i;
        break;
      }
    }
#else
    if(matrix->getWidth() == 1){
      if(matrix->row_lengths[block_row_index] == 0){
        block_exists = false;
      }else{
        block_exists = true;
        block_row_col = 0;
      }
    }else{
      int idx = matrix->block_map[block_row_index].findIndex(block_col_index);
      if(idx == Tree<int>::undefinedIndex){
        /*Block not found*/
        block_exists = false;
      }else{
        /*Block found*/
        block_exists = true;
        block_row_col = idx;//it->second;
        //block_row_col = it->second;
      }
    }
#endif

    if(!block_exists){
      /*Create a new block*/
      if(matrix->n_blocks == matrix->n_allocated_blocks){
        matrix->grow_blocks();
      }
#if 0
      if(!matrix->row_lengths[block_row_index] <=
         matrix->allocated_length_row[block_row_index]){
        printf("Block row index = %d, row_lengths = %d, allocated = %d\n",
               block_row_index, matrix->row_lengths[block_row_index],
               matrix->allocated_length_row[block_row_index]);
      }
#endif
      cgfassert(matrix->row_lengths[block_row_index] <=
                matrix->allocated_length_row[block_row_index]);

      if(matrix->row_lengths[block_row_index] ==
         matrix->allocated_length_row[block_row_index]){
        matrix->grow_row(block_row_index);
      }

      if(matrix->col_lengths[block_col_index] ==
         matrix->allocated_length_col[block_col_index]){
        matrix->grow_col(block_col_index);
      }

      matrix->block_indices[block_row_index][matrix->row_lengths[block_row_index]] = matrix->n_blocks;
      matrix->block_indices_col[block_col_index][matrix->col_lengths[block_col_index]] = matrix->n_blocks;
      matrix->col_indices[block_row_index][matrix->row_lengths[block_row_index]] = block_col_index;
      matrix->row_indices[block_col_index][matrix->col_lengths[block_col_index]] = block_row_index;
      block_index = matrix->n_blocks;

      matrix->block_map[block_row_index].insert(block_col_index, matrix->row_lengths[block_row_index]);

      matrix->row_lengths[block_row_index]++;
      matrix->col_lengths[block_col_index]++;
      matrix->comp_col_indices[block_index] = block_col_index * N;
      matrix->comp_row_indices[block_index] = block_row_index * N;

      /*Reset block*/
      matrix->blocks[matrix->n_blocks].clear();
      matrix->n_blocks++;
      //printf("%d, ", block_index);


      /*Structure of matrix has been altered, finalization is needed.*/
      matrix->finalized = false;
    }else{
      block_index = matrix->block_indices[block_row_index][block_row_col];

      /*Structure has not been altered, no finalization is needed yet.*/
    }

    int block_row = row%N;
    int block_col = col%N;

    return matrix->blocks[block_index].m[block_row*N + block_col];
  }

  template<int N, class T>
  T SpRowProxy<N, T>::operator[](int c) const{
    int col = c;
    bool block_exists = false;
    int block_index;
    int block_row_col;
    int block_row_index = row/N;
    int block_col_index = col/N;

    cgfassert(col < cmatrix->getWidth());

    cgfassert(block_col_index < cmatrix->width  / N);
    cgfassert(block_row_index < cmatrix->height / N);

    if(!(block_col_index < cmatrix->width  / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    if(!(block_row_index < cmatrix->height / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

#if 0
    /*Find target block*/
    for(int i=0;i<cmatrix->row_lengths[block_row_index];i++){
      if(cmatrix->col_indices[block_row_index][i] == block_col_index){
        block_exists = true;
        block_row_col = i;
        break;
      }
    }
#else
    if(cmatrix->getWidth() == 1){
      if(cmatrix->row_lengths[block_row_index] == 0){
        return SumIdentity(T());
      }

      block_exists = true;
      block_row_col = 0;
    }else{
      //std::map<int, int>::iterator it =
      //cmatrix->block_map[block_row_index].find(block_col_index);
      int idx = cmatrix->block_map[block_row_index].findIndex(block_col_index);
      //if(it == cmatrix->block_map[block_row_index].end()){
      if(idx == Tree<int>::undefinedIndex){
        /*Block not found*/
        block_exists = false;
      }else{
        /*Block found*/
        block_exists = true;
        block_row_col = idx;//it->second;
        //block_row_col = it->second;
      }
    }
#endif

    if(!block_exists){
      /*Block does not exist and since this is a const object we can
        not create new blocks*/
      return SumIdentity(T());
    }else{
      block_index = cmatrix->block_indices[block_row_index][block_row_col];
    }

    int block_row = row%N;
    int block_col = col%N;

    return cmatrix->blocks[block_index].m[block_row*N + block_col];
  }

  template<int N, class T>
  T SpMatrix<N, T>::getValue(int row, int col)const{
    bool block_exists = false;
    int block_index;
    int block_row_col;
    int block_row_index = row/N;
    int block_col_index = col/N;

    cgfassert(col < getWidth());

    cgfassert(block_col_index < width  / N);
    cgfassert(block_row_index < height / N);

    if(!(block_col_index < width / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    if(!(block_row_index < height / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

#if 0
    /*Find target block*/
    for(int i=0;i<cmatrix->row_lengths[block_row_index];i++){
      if(cmatrix->col_indices[block_row_index][i] == block_col_index){
        block_exists = true;
        block_row_col = i;
        break;
      }
    }
#else
    if(getWidth() == 1){
      if(row_lengths[block_row_index] == 0){
        return SumIdentity(T());
      }

      block_exists = true;
      block_row_col = 0;
    }else{
      //std::map<int, int>::iterator it =
      //cmatrix->block_map[block_row_index].find(block_col_index);
      int idx = block_map[block_row_index].findIndex(block_col_index);
      //if(it == cmatrix->block_map[block_row_index].end()){
      if(idx == Tree<int>::undefinedIndex){
        /*Block not found*/
        block_exists = false;
      }else{
        /*Block found*/
        block_exists = true;
        block_row_col = idx;//it->second;
        //block_row_col = it->second;
      }
    }
#endif

    if(!block_exists){
      /*Block does not exist and since this is a const object we can
        not create new blocks*/
      return SumIdentity(T());
    }else{
      block_index = block_indices[block_row_index][block_row_col];
    }

    int block_row = row%N;
    int block_col = col%N;

    return blocks[block_index].m[block_row*N + block_col];
  }

  template<int N, class T>
  void SpMatrix<N, T>::setElements(List<SpMatrixTuple<N, T> >& elements){
    clearStructure();
    elements.sort();

    int lastBlockRow = -1;
    int lastBlockCol = -1;

    typename List<SpMatrixTuple<N, T> >::Iterator it = elements.begin();

    while(it != elements.end()){
      int blockRow = it->row/N;
      int blockCol = it->col/N;

      if(blockRow == lastBlockRow && blockCol == lastBlockCol){
        /*Add to last blocks*/
      }else{
        /*Use new block*/
        if(n_blocks == n_allocated_blocks){
          grow_blocks();
        }

        if(row_lengths[blockRow] == allocated_length_row[blockRow]){
          grow_row(blockRow);
        }

        if(col_lengths[blockCol] == allocated_length_col[blockCol]){
          grow_col(blockCol);
        }

        lastBlockRow = blockRow;
        lastBlockCol = blockCol;

        row_lengths[blockRow]++;
        col_lengths[blockCol]++;

        n_blocks++;

        comp_col_indices[n_blocks-1] = blockCol * N;
        comp_row_indices[n_blocks-1] = blockRow * N;

        col_indices[blockRow][row_lengths[blockRow]-1] = blockCol;
        row_indices[blockCol][col_lengths[blockCol]-1] = blockRow;

        block_indices    [blockRow][row_lengths[blockRow]-1] = n_blocks-1;
        block_indices_col[blockCol][col_lengths[blockCol]-1] = n_blocks-1;


        block_map[blockRow].insert(blockCol, row_lengths[blockRow]-1);
      }

      int localRow = it->row % N;
      int localCol = it->col % N;

      blocks[n_blocks-1].m[localRow*N+localCol] += it->val;

      it++;
    }
  }

  template<int N, class T>
  void SpMatrix<N, T>::setElements(Tree<SpMatrixTuple<N, T> >& elements){
    clearStructure();
    //elements.sort();

    int lastBlockRow = -1;
    int lastBlockCol = -1;

    typename Tree<SpMatrixTuple<N, T> >::Iterator it = elements.begin();

    while(it != elements.end()){
      int blockRow = it->row/N;
      int blockCol = it->col/N;

      if(blockRow == lastBlockRow && blockCol == lastBlockCol){
        /*Add to last blocks*/
      }else{
        /*Use new block*/
        if(n_blocks == n_allocated_blocks){
          grow_blocks();
        }

        if(row_lengths[blockRow] == allocated_length_row[blockRow]){
          grow_row(blockRow);
        }

        if(col_lengths[blockCol] == allocated_length_col[blockCol]){
          grow_col(blockCol);
        }

        lastBlockRow = blockRow;
        lastBlockCol = blockCol;

        row_lengths[blockRow]++;
        col_lengths[blockCol]++;

        n_blocks++;

        comp_col_indices[n_blocks-1] = blockCol * N;
        comp_row_indices[n_blocks-1] = blockRow * N;

        col_indices[blockRow][row_lengths[blockRow]-1] = blockCol;
        row_indices[blockCol][col_lengths[blockCol]-1] = blockRow;

        block_indices    [blockRow][row_lengths[blockRow]-1] = n_blocks-1;
        block_indices_col[blockCol][col_lengths[blockCol]-1] = n_blocks-1;


        block_map[blockRow].insert(blockCol, row_lengths[blockRow]-1);
      }

      int localRow = it->row % N;
      int localCol = it->col % N;

      blocks[n_blocks-1].m[localRow*N+localCol] += it->val;

      it++;
    }
  }


  template<int N, class T>
  void spmv(Vector<T>& r, const SpMatrix<N, T>& m, const Vector<T>& v);

  template<int N, class T>
  void spmv_t(Vector<T>& r, const SpMatrix<N, T>& m, const Vector<T>& v);

  template<int N, class T>
  void spmv_partial(Vector<T>& r, const SpMatrix<N, T>& m,
                    const Vector<T>& v, const MatrixRange rg);

  template<int N, class T>
  void spmv_parallel(Vector<T>& r, const SpMatrix<N, T>& m,
                     const Vector<T>& v);

  /*Performs a multiplication x^{k+1}' = D^{-1}(L+U)x^{k} and is used
    in Jacobi's method as x^{k+1} = D^{-1}b - x^{k+1}'
  */
  template<int N, class T>
  void multiplyJacobi(Vector<T>& r, const SpMatrix<N, T>& m,
                      const Vector<T>& v);

  /*Performs one Gauss-Seidel step*/
  template<int N, class T>
  void multiplyGaussSeidel(Vector<T>& r, const SpMatrix<N, T>& m,
                           const Vector<T>& v, const Vector<T>* b = 0);

  template<int N, int M, class TPA, class TPB>
  void convertMatrix(SpMatrix<N, TPA> & a, const SpMatrix<M, TPB>& b){
    /*Create new empty matrix*/
    SpMatrix<N, TPA> na(b.getWidth(), b.getHeight());

    /*Assign empty matrix*/
    a = na;
    for(int i=0;i<b.height/M;i++){
      for(int j=0;j<b.row_lengths[i];j++){
        int block_index = b.block_indices[i][j];
        int col_index   = b.col_indices[i][j] * M;
        int idx = 0;
        for(int k=0;k<M;k++){
          for(int l=0;l<M;l++){
            TPB bval = b.blocks[block_index].m[idx++];
            if(bval != (TPB)0.0){
              a[i*M + k][col_index + l] = (TPA)bval;
            }
          }
        }
      }
    }
  }


  /********************************************************************/

  template<int N, class T>
  SpMatrix<N, T>::SpMatrix():width(0), height(0), origWidth(0),
                             origHeight(0), block_map(0), col_indices(0),
                             row_indices(0),
                             block_indices(0), block_indices_col(0),
                             comp_col_indices(0),
                             comp_row_indices(0),
                             row_lengths(0), col_lengths(0),
                             allocated_length_row(0),
                             allocated_length_col(0),
                             blocks(0),
                             n_allocated_blocks(0), n_blocks(0),
                             n_elements(0), /*task(0), pool(0),*/
                             finalized(false),
                             tBlocks(0){
  }

  template<int N, class T>
  SpMatrix<N, T>::SpMatrix(int w, int h):block_map(0), col_indices(0),
                                         row_indices(0),
                                         block_indices(0),
                                         block_indices_col(0),
                                         comp_col_indices(0),
                                         comp_row_indices(0),
                                         row_lengths(0), col_lengths(0),
                                         allocated_length_row(0),
                                         allocated_length_col(0),
                                         blocks(0), n_allocated_blocks(0),
                                         n_blocks(0), n_elements(0),
                                         /*task(0), pool(0),*/
                                         finalized(false),
                                         tBlocks(0){
    //size = w;
    origWidth = w;
    origHeight = h;

    if(h%16 == 0){
      height = h;
    }else{
      height = ((h/16)+1)*16;
    }

    if(w%16 == 0){
      width = w;
    }else{
      width = ((w/16)+1)*16;
    }

    /*Allocate data for rows*/
    if((height/N)%32 == 0){
      row_lengths      = new int[height/N];
      allocated_length_row = new int[height/N];
      col_indices      = new int*[height/N];
      block_indices    = new int*[height/N];
      //block_map        = new std::map<int, int>[height/N];
      block_map        = new Tree<int>[height/N];
    }else{
      int extendedHeight = (height/N)/32;
      extendedHeight++;
      extendedHeight*=32;
      row_lengths      = new int[extendedHeight];
      allocated_length_row = new int[extendedHeight];
      col_indices      = new int*[extendedHeight];
      block_indices    = new int*[extendedHeight];
      //block_map        = new std::map<int, int>[extendedHeight];
      block_map        = new Tree<int>[extendedHeight];
    }

    /*Allocate data for cols*/
    if((width/N)%32 == 0){
      col_lengths      = new int[width/N];
      allocated_length_col = new int[width/N];
      row_indices      = new int*[width/N];
      block_indices_col    = new int*[width/N];
      //block_map        = new std::map<int, int>[height/N];
      //block_map_col        = new Tree<int>[width/N];
    }else{
      int extendedWidth = (width/N)/32;
      extendedWidth++;
      extendedWidth*=32;
      col_lengths      = new int[extendedWidth];
      allocated_length_col = new int[extendedWidth];
      row_indices      = new int*[extendedWidth];
      block_indices_col    = new int*[extendedWidth];
      //block_map        = new std::map<int, int>[extendedHeight];
      //block_map_col        = new Tree<int>[extendedWidth];
    }



    for(int i=0;i<height/N;i++){
      allocated_length_row[i] = 1;
      row_lengths[i]      = 0;
      col_indices[i]      = new int[1];
      block_indices[i]    = new int[1];
    }

    for(int i=0;i<width/N;i++){
      allocated_length_col[i] = 1;
      col_lengths[i]      = 0;
      row_indices[i]      = new int[1];
      block_indices_col[i]    = new int[1];
    }

    /*Allocate blocks*/
    n_allocated_blocks = 16;
    n_blocks = 0;
    n_elements = 0;

    alignedMalloc((void**)&blocks, 16,
                  (uint)n_allocated_blocks * sizeof(SpMatrixBlock<N, T>));

    proxy.matrix = this;

    comp_col_indices = new int[n_allocated_blocks];
    comp_row_indices = new int[n_allocated_blocks];
    //task = 0;
    //pool = 0;
    //exporter = 0;
  }

  template<int N, class T>
  SpMatrix<N, T>::SpMatrix(const SpMatrix<N, T>& m):width(0), height(0),
                                                    origWidth(0),
                                                    origHeight(0),
                                                    block_map(0),
                                                    col_indices(0),
                                                    row_indices(0),
                                                    block_indices(0),
                                                    block_indices_col(0),
                                                    comp_col_indices(0),
                                                    comp_row_indices(0),
                                                    row_lengths(0),
                                                    col_lengths(0),
                                                    allocated_length_row(0),
                                                    allocated_length_col(0),
                                                    blocks(0),
                                                    n_allocated_blocks(0),
                                                    n_blocks(0),
                                                    n_elements(0)/*, task(0),
                                                                   pool(0)*/{
    *this = m;
  }


  /*Destructor*/
  template<int N, class T>
  SpMatrix<N, T>::~SpMatrix(){
    alignedFree(blocks);

    for(int i=0;i<height/N;i++){
      delete [] col_indices[i];
      delete [] block_indices[i];
    }

    for(int i=0;i<width/N;i++){
      delete [] row_indices[i];
      delete [] block_indices_col[i];
    }

    delete [] block_map;

    delete [] col_indices;
    delete [] row_indices;
    delete [] block_indices;
    delete [] block_indices_col;
    delete [] allocated_length_row;
    delete [] allocated_length_col;
    delete [] row_lengths;
    delete [] col_lengths;

    delete[] comp_col_indices;
    delete[] comp_row_indices;


#if 0
    if(task)
      delete task;
#endif

    //if(tBlocks){
    //  delete[]tBlocks;
    //}
  }

  template<int N, class T>
  void SpMatrix<N, T>::clear(){
    for(int i=0;i<n_blocks;i++){
      blocks[i].clear();
    }
  }

  template<int N, class T>
  void SpMatrix<N, T>::clearStructure(){
    //clear();

    if(n_blocks < (width + height)/N){
      /*Instead of looping over large arrays, just loop over the
      blocks and reset the individual components*/

      for(int blockIndex = 0;blockIndex<n_blocks;blockIndex++){
        int row = comp_row_indices[blockIndex]/N;
        int col = comp_col_indices[blockIndex]/N;

        cgfassert(row != -1);
        cgfassert(col != -1);

        row_lengths[row] = 0;
        col_lengths[col] = 0;

        comp_row_indices[blockIndex] = -1;
        comp_col_indices[blockIndex] = -1;

        block_map[row].clear();
      }
    }else{
      /*Reset pointers*/
      for(int i=0;i<height/N;i++){
        row_lengths[i] = 0;
        block_map[i].clear();
      }

      for(int i=0;i<width/N;i++){
        col_lengths[i] = 0;
        //block_map_col[i].clear();
      }
    }

    n_blocks = 0;
  }

  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator=(const SpMatrix<N, T>& m){
    if(this == &m){
      /*Self assignment*/
      return *this;
    }
    //warning("Please check this for small matrices, block_map copy seems not correct");

    if(height == m.height &&
       width  == m.width &&
       origWidth  == m.origWidth &&
       origHeight == m.origHeight){
      /*Matrices have same shape*/

      message("same shape, %d / %d blocks", n_blocks, m.n_blocks);

      clearStructure();

      if(n_blocks < (m.width + m.height) * 4){
        if(m.n_blocks >= n_allocated_blocks){
          grow_blocks();
        }

        //List<SpMatrixTuple<N, T> > elements;

        for(int blockIndex = 0;blockIndex < m.n_blocks;blockIndex++){
          int row = m.comp_row_indices[blockIndex];
          int col = m.comp_col_indices[blockIndex];

          cgfassert(row != -1);
          cgfassert(col != -1);

          comp_row_indices[blockIndex] = row;
          comp_col_indices[blockIndex] = col;

          for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
              T value = m.blocks[blockIndex].m[i*N+j];
              if(Abs(value) != (T)0.0){
                //SpMatrixTuple<N, T> tuple;
                //tuple.row = row + i;
                //tuple.col = col + j;
                //tuple.val = m.blocks[blockIndex].m[i*N+j];
                //elements.append(tuple);
                (*this)[row + i][col + j] = m.blocks[blockIndex].m[i*N+j];
              }
            }
          }
        }

        //setElements(elements);

        proxy.matrix       = this;
        proxy.cmatrix      = this;
        finalized = m.finalized;

        return *this;
      }

      //Rows
      for(int i=0;i<height/N;i++){
        if(allocated_length_row[i] >= m.row_lengths[i]){
          /*Does fit*/
          row_lengths[i] = m.row_lengths[i];

          memcpy(col_indices[i], m.col_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
          memcpy(block_indices[i], m.block_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
        }else{
          /*Does not fit*/
          row_lengths[i] = m.row_lengths[i];
          delete[] col_indices[i];
          delete[] block_indices[i];

          allocated_length_row[i] = m.allocated_length_row[i];
          col_indices[i] = new int[allocated_length_row[i]];
          block_indices[i] = new int[allocated_length_row[i]];

          memcpy(col_indices[i], m.col_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
          memcpy(block_indices[i], m.block_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
        }
        block_map[i] = m.block_map[i];
      }

      //Cols
      for(int i=0;i<width/N;i++){
        if(allocated_length_col[i] >= m.col_lengths[i]){
          /*Does fit*/
          col_lengths[i] = m.col_lengths[i];

          memcpy(row_indices[i], m.row_indices[i],
                 sizeof(int)*(uint)col_lengths[i]);
          memcpy(block_indices_col[i], m.block_indices_col[i],
                 sizeof(int)*(uint)col_lengths[i]);
        }else{
          /*Does not fit*/
          col_lengths[i] = m.col_lengths[i];
          delete[] row_indices[i];
          delete[] block_indices_col[i];

          allocated_length_col[i] = m.allocated_length_col[i];
          row_indices[i] = new int[allocated_length_col[i]];
          block_indices_col[i] = new int[allocated_length_col[i]];

          memcpy(row_indices[i], m.row_indices[i],
                 sizeof(int)*(uint)col_lengths[i]);
          memcpy(block_indices_col[i], m.block_indices_col[i],
                 sizeof(int)*(uint)col_lengths[i]);
        }
        block_map[i] = m.block_map[i];
      }

      if(n_allocated_blocks >= m.n_blocks){
        /*Does fit*/
        n_blocks = m.n_blocks;
        /*Copy blocks*/
        for(int k=0;k<n_blocks;k++){
          blocks[k] = m.blocks[k];
        }
        //memcpy(blocks, m.blocks,
        //       sizeof(SpMatrixBlock<N, T>)*(uint)n_blocks);

        memcpy(comp_col_indices, m.comp_col_indices,
               sizeof(int)*(long unsigned int)n_blocks);

        memcpy(comp_row_indices, m.comp_row_indices,
               sizeof(int)*(long unsigned int)n_blocks);
      }else{
        /*Does not fit*/
        alignedFree(blocks);
        delete[] comp_col_indices;
        delete[] comp_row_indices;

        n_allocated_blocks = m.n_allocated_blocks;
        n_blocks = m.n_blocks;

        /*Allocate blocks*/
        alignedMalloc((void**)&blocks, 16,
                      (uint)n_allocated_blocks * sizeof(SpMatrixBlock<N, T>));
        comp_col_indices = new int[n_allocated_blocks];
        comp_row_indices = new int[n_allocated_blocks];

        /*Copy blocks*/
        for(int k=0;k<n_blocks;k++){
          blocks[k] = m.blocks[k];
        }
        //memcpy(blocks, m.blocks,
        //       sizeof(SpMatrixBlock<N, T>)*(uint)n_blocks);

        memcpy(comp_col_indices, m.comp_col_indices,
               sizeof(int)*(long unsigned int)n_blocks);

        memcpy(comp_row_indices, m.comp_row_indices,
               sizeof(int)*(long unsigned int)n_blocks);
      }

      proxy.matrix       = this;
      proxy.cmatrix      = this;

      return *this;
    }

    /*Not equal, reshape*/

    message("matrices not equally shaped");
    message("%d, %d", height, m.height);
    message("%d, %d", width,  m.width);
    message("%d, %d", origWidth, m.origWidth);
    message("%d, %d", origHeight, m.origHeight);
    getchar();

    for(int i=0;i<height/N;i++){
      delete [] col_indices[i];
      delete [] block_indices[i];
    }

    for(int i=0;i<width/N;i++){
      delete [] row_indices[i];
      delete [] block_indices_col[i];
    }

    delete [] block_map;
    delete [] col_indices;
    delete [] row_indices;
    delete [] block_indices;
    delete [] block_indices_col;
    delete [] allocated_length_row;
    delete [] allocated_length_col;
    delete [] row_lengths;
    delete [] col_lengths;

    //message("deleted data");

    height             = m.height;
    width              = m.width;
    origHeight         = m.origHeight;
    origWidth          = m.origWidth;
    n_blocks           = m.n_blocks;
    n_elements         = m.n_elements;

    proxy.matrix       = this;
    proxy.cmatrix      = this;

    /*Allocate new arrays*/
    if((height/N)%32 == 0){
      //message("normal height = %d", height/N);
      row_lengths          = new int[height/N];
      allocated_length_row = new int[height/N];
      col_indices          = new int*[height/N];
      block_indices        = new int*[height/N];
      //block_map        = new std::map<int, int>[height/N];
      block_map            = new Tree<int>[height/N];
    }else{
      //message("height = %d", height);
      int extendedHeight = (height/N)/32;
      //message("extended height = %d", (height/N)/32);
      extendedHeight++;
      extendedHeight*=32;
      row_lengths          = new int[extendedHeight];
      allocated_length_row = new int[extendedHeight];
      col_indices          = new int*[extendedHeight];
      block_indices        = new int*[extendedHeight];
      //block_map        = new std::map<int, int>[extendedHeight];
      block_map            = new Tree<int>[extendedHeight];
    }

    /*Allocate new arrays*/
    if((width/N)%32 == 0){
      //message("normal height = %d", height/N);
      col_lengths          = new int[width/N];
      allocated_length_col = new int[width/N];
      row_indices          = new int*[width/N];
      block_indices_col    = new int*[width/N];
      //block_map        = new std::map<int, int>[height/N];
      //block_map        = new Tree<int>[height/N];
    }else{
      //message("height = %d", height);
      int extendedWidth = (width/N)/32;
      //message("extended height = %d", (height/N)/32);
      extendedWidth++;
      extendedWidth*=32;
      col_lengths          = new int[extendedWidth];
      allocated_length_col = new int[extendedWidth];
      row_indices          = new int*[extendedWidth];
      block_indices_col    = new int*[extendedWidth];
      //block_map        = new std::map<int, int>[extendedHeight];
      //block_map        = new Tree<int>[extendedHeight];
    }

    //message("new allocated");

    /*Copy data*/
    memcpy(row_lengths, m.row_lengths, sizeof(int)*(uint)height/N);
    memcpy(col_lengths, m.col_lengths, sizeof(int)*(uint)width/N);

    memcpy(allocated_length_row, m.allocated_length_row,
           sizeof(int)*(uint)height/N);
    memcpy(allocated_length_col, m.allocated_length_col,
           sizeof(int)*(uint)width/N);

    //message("copy data1");

    //message("height = %d", height/N);

    for(int i=0;i<height/N;i++){
      //message("alloc_length[%d] = %d", i, allocated_length_row[i]);
      col_indices[i]   = new int[allocated_length_row[i]];
      block_indices[i] = new int[allocated_length_row[i]];

      memcpy(col_indices[i], m.col_indices[i],
             sizeof(int)*(uint)row_lengths[i]);
      memcpy(block_indices[i], m.block_indices[i],
             sizeof(int)*(uint)row_lengths[i]);

      block_map[i] = m.block_map[i];
    }

    for(int i=0;i<width/N;i++){
      //message("alloc_length[%d] = %d", i, allocated_length_row[i]);
      row_indices[i]       = new int[allocated_length_col[i]];
      block_indices_col[i] = new int[allocated_length_col[i]];

      memcpy(row_indices[i], m.row_indices[i],
             sizeof(int)*(uint)col_lengths[i]);
      memcpy(block_indices_col[i], m.block_indices_col[i],
             sizeof(int)*(uint)col_lengths[i]);

      //block_map[i] = m.block_map[i];
    }

    //message("copy data2");

    if(m.n_blocks >= n_allocated_blocks){
      /*Free blocks*/
      alignedFree(blocks);
      delete [] comp_col_indices;
      delete [] comp_row_indices;

      /*Allocate blocks*/
      alignedMalloc((void**)&blocks, 16,
                    (uint)m.n_allocated_blocks * sizeof(SpMatrixBlock<N, T>));

      comp_col_indices = new int[m.n_allocated_blocks];
      comp_row_indices = new int[m.n_allocated_blocks];

      n_allocated_blocks = m.n_allocated_blocks;
    }

    /*Copy blocks*/
    for(int k=0;k<n_blocks;k++){
      blocks[k] = m.blocks[k];
    }
    //memcpy(blocks, m.blocks,
    //       sizeof(SpMatrixBlock<N, T>)*(uint)n_blocks);

    memcpy(comp_col_indices, m.comp_col_indices,
           sizeof(int)*(uint)n_blocks);

    memcpy(comp_row_indices, m.comp_row_indices,
           sizeof(int)*(uint)n_blocks);

    //message("copy data3");

    //task = 0;
    //pool = 0;

    finalized = m.finalized;

    return *this;
  }

  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator+(const SpMatrix<N, T>& m) const{
    /*@Todo, add complete blocks and improve speed. Now O(N^2)*/
    SpMatrix<N, T> m2 = *this;

    m2 += m;
    return m2;
  }

  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator+=(const SpMatrix<N, T>& m){
    cgfassert(origWidth == m.origWidth);
    cgfassert(origHeight == m.origHeight);

    for(int i=0;i<m.height/N;i++){
      for(int j=0;j<m.row_lengths[i];j++){
        int block_index = m.block_indices[i][j];
        int block_col   = m.col_indices[i][j] * N;

        for(int k=0;k<N*N;k++){
          int l_row = k/N;
          int l_col = k%N;
          int row = i*N + l_row;
          int col = block_col + l_col;

          T val = m.blocks[block_index].m[k];

          if(val != (T)0.0){
            (*this)[row][col] += val;
          }
        }
      }
    }
    return *this;
  }

  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator-(const SpMatrix<N, T>& m) const{
    cgfassert(origWidth == m.origWidth);
    cgfassert(origHeight == m.origHeight);
    SpMatrix<N, T> m2 = *this;

    m2 -= m;
    return m2;
  }

  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator-=(const SpMatrix<N, T>& m){
    cgfassert(origWidth == m.origWidth);
    cgfassert(origHeight == m.origHeight);

    for(int i=0;i<m.height/N;i++){
      for(int j=0;j<m.row_lengths[i];j++){
        int block_index = m.block_indices[i][j];
        int block_col   = m.col_indices[i][j] * N;

        for(int k=0;k<N*N;k++){
          int l_row = k/N;
          int l_col = k%N;
          int row = i*N + l_row;
          int col = block_col + l_col;

          T val = m.blocks[block_index].m[k];

          if(val != (T)0.0){
            (*this)[row][col] -= val;
          }
        }
      }
    }
    return *this;
  }

  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator*(const SpMatrix<N, T>& B)const{
    SpMatrix<N, T> r;
    multiply(&r, this, &B);
    return r;
  }

  template<int N, class T>
  void SpMatrix<N, T>::multiply(SpMatrix<N, T>* r,
                                const SpMatrix<N, T>* A,
                                const SpMatrix<N, T>* B){
    int rWidth = B->getWidth();
    int rHeight = A->getHeight();
    if(r->getWidth() == rWidth &&
       r->getHeight() == rHeight){
      /*Same shape*/
      //message("same structure");
      r->clearStructure();
    }else{
      message("r->width  = %d, rWidth  = %d", r->getWidth(), rWidth);
      message("r->height = %d, rHeight = %d", r->getHeight(), rHeight);
      *r = SpMatrix<N, T>(rWidth, rHeight);
    }

    if(!A->finalized || !B->finalized){
      //error("matrices not finalized");
    }

    if(A->getWidth() != B->getHeight()){
      error("matrix dimensions do not agree");
    }

#if 0
    /*Since the blocks are ordered, we only need to maintain per row a
      pointer to the current block in the row.*/

    int* row_pointers = new int[B->getHeight()];

    /*Initialize pointers*/

    for(int i=0;i<B->getHeight();i++){
      row_pointers[i] = 0;
    }

    for(int i=0;i<B->width/N;i++){
      for(int j=0;j<A->height/N;j++){
        //message("compute block %d, %d", i, j);
        /*Compute block (i, j)*/
        /*Loop over all blocks in row j in this matrix*/
        SpMatrixBlock<N, T> result;
        result.clear();

        int length = A->row_lengths[j];
        //message("length row %d = %d", j, length);

        for(int k=0;k<length;k++){
          int columnA     = A->col_indices[j][k];
          int rowPointerB = row_pointers[columnA];
          int rowLengthB  = B->row_lengths[columnA];
          //message("columnA = %d", columnA);
          //message("lengthB = %d", rowLengthB);
          //message("rowPointerB = %d, %d", rowPointerB, columnA);
          if(rowLengthB == 0){
            /*Row in B is empty*/
            continue;
          }

          while(rowPointerB < rowLengthB){
            //message("%d, %d, %d, %d", columnA, B.col_indices[columnA][rowPointerB], i, j);
            if(B->col_indices[columnA][rowPointerB] < i){
              rowPointerB++;
            }else{
              break;
            }
          }

          row_pointers[columnA] = rowPointerB;

          if(B->col_indices[columnA][rowPointerB] == i){
            /*Perform multiplication and add to resulting block*/
            int blockIdxA = A->block_indices[j][k];
            int blockIdxB = B->block_indices[columnA][rowPointerB];

            result.matrixMulAdd(&(A->blocks[blockIdxA]),
                                &(B->blocks[blockIdxB]));
          }
        }

        /*Store block*/
        for(int k=0;k<N;k++){
          for(int l=0;l<N;l++){
            if(j*N+k < rHeight && i*N+l < rWidth){
              (*r)[j*N+k][i*N+l] = result.m[k*N+l];
            }
          }
        }
      }
    }

    delete[] row_pointers;
#else

    if(A->n_blocks < B->n_blocks){
    //if(B->width > A->height){
      //SpMatrixBlock<N, T>* rowBlocks = new SpMatrixBlock<N, T>[r.width/N];

      Tree<int> touchedBlocks;
      std::map<int, SpMatrixBlock<N, T> > blockMap;

      for(int blockIndex = 0;blockIndex < A->n_blocks;blockIndex++){
        int row = A->comp_row_indices[blockIndex]/N;
        int col = A->comp_col_indices[blockIndex]/N;

        /*Loop over row in B*/
        for(int i=0;i<B->row_lengths[col];i++){
          int colB = B->col_indices[col][i];
          int bBlockIndex = B->block_indices[col][i];

          int index = touchedBlocks.findIndex(colB);

          SpMatrixBlock<N, T>* tmp = 0;
          if(index == Tree<int>::undefinedIndex){
            touchedBlocks.insert(colB, colB);
            tmp = &(blockMap[colB]);
            tmp->clear();
          }else{
            tmp = &(blockMap[colB]);
          }

          tmp->matrixMulAdd(&(A->blocks[blockIndex]),
                            &(B->blocks[bBlockIndex]));

        }

        if(col == A->col_indices[row][A->row_lengths[row]-1]){
          /*End of row, write row in result matrix*/
          Tree<int>::Iterator it = touchedBlocks.begin();
          while(it != touchedBlocks.end()){

            int rCol = *it++;
            SpMatrixBlock<N, T>* tmp = &(blockMap[rCol]);
            /*Store block*/
            for(int k=0;k<N;k++){
              for(int l=0;l<N;l++){
                if(row*N+k < rHeight && rCol*N+l < rWidth){
                  (*r)[row*N+k][rCol*N+l] = tmp->m[k*N+l];//result.m[k*N+l];
                }
              }
            }
            tmp->clear();

          }
          touchedBlocks.clear();
        }
      }
      //delete [] rowBlocks;
    }else{
      //SpMatrixBlock<N, T>* colBlocks = new SpMatrixBlock<N, T>[r.height/N];

      Tree<int> touchedBlocks;
      std::map<int, SpMatrixBlock<N, T> > blockMap;

      for(int blockIndex = 0;blockIndex < B->n_blocks;blockIndex++){
        int row = B->comp_row_indices[blockIndex]/N;
        int col = B->comp_col_indices[blockIndex]/N;

        /*Loop over col in A*/
        //message("row = %d, col = %d", row, col);
        for(int i=0;i<A->col_lengths[row];i++){
          int rowA = A->row_indices[row][i];
          int aBlockIndex = A->block_indices_col[row][i];

          int index = touchedBlocks.findIndex(rowA);

          SpMatrixBlock<N, T>* tmp = 0;
          if(index == Tree<int>::undefinedIndex){
            touchedBlocks.insert(rowA, rowA);
            tmp = &(blockMap[rowA]);
            tmp->clear();
          }else{
            tmp = &(blockMap[rowA]);
          }

          tmp->matrixMulAdd(&(A->blocks[aBlockIndex]),
                            &(B->blocks[blockIndex]));

        }

        if(row == B->row_indices[col][B->col_lengths[col]-1]){
          /*End of row, write row in result matrix*/
          Tree<int>::Iterator it = touchedBlocks.begin();
          while(it != touchedBlocks.end()){

            int rRow = *it++;
            SpMatrixBlock<N, T>* tmp = &(blockMap[rRow]);

            /*Store block*/
            for(int k=0;k<N;k++){
              for(int l=0;l<N;l++){
                if(rRow*N+k < rHeight && col*N+l < rWidth){
                  (*r)[rRow*N+k][col*N+l] = tmp->m[k*N+l];//result.m[k*N+l];
                }
              }
            }
            tmp->clear();

          }
          touchedBlocks.clear();
        }
      }
    }

#endif

    //r->finalize();
    //return r;
  }


  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator*(T f) const{
    SpMatrix<N, T> m2 = *this;

    for(int i=0;i<m2.n_blocks;i++){
      m2.blocks[i].mul(f);
    }
    return m2;
  }

  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator*=(T f){
    for(int i=0;i<n_blocks;i++){
      blocks[i].mul(f);
    }
    return *this;
  }

  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator/(T f) const{
    SpMatrix<N, T> m2 = *this;

    for(int i=0;i<m2.n_blocks;i++){
      m2.blocks[i].div(f);
    }
    return m2;
  }

  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator/=(T f){
    for(int i=0;i<n_blocks;i++){
      blocks[i].div(f);
    }
    return *this;
  }

  template<int N, class T>
  T multiplyRows(SpMatrix<N, T>& A, int rowA, SpMatrix<N, T>& B, int rowB){
    T sum = (T)0.0;

    A.finalize();
    B.finalize();

    if(A.getWidth() != B.getWidth()){
      error("dimensions not equal");
    }

    int blockRowA = rowA/N;
    int blockRowB = rowB/N;
    int subRowA   = rowA%N;
    int subRowB   = rowB%N;

    int rowLengthA = A.row_lengths[blockRowA];
    int rowLengthB = B.row_lengths[blockRowB];

    int blockPtrA = 0;
    int blockPtrB = 0;

    while(true){
      if(blockPtrA >= rowLengthA ||
         blockPtrB >= rowLengthB){
        /*One row has reached its end*/
        break;
      }

      int colA = A.col_indices[blockRowA][blockPtrA];
      int colB = B.col_indices[blockRowB][blockPtrB];

      if(colA < colB){
        blockPtrA++;
      }else if(colA == colB){
        /*Compute*/
        int blockIdxA = A.block_indices[blockRowA][blockPtrA];
        int blockIdxB = B.block_indices[blockRowB][blockPtrB];

        for(int i=0;i<N;i++){
          sum += (A.blocks[blockIdxA].m[subRowA*N+i] *
                  B.blocks[blockIdxB].m[subRowB*N+i]);

        }

        blockPtrA++;
        blockPtrB++;
      }else{
        blockPtrB++;
      }
    }
    return sum;
  }

  template<int N, class T>
  SpMatrix<N, T> operator==(const SpMatrix<N, T>& m, T f){
    SpMatrix<N, T> r(m);

    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpeq(f);
    }
    return r;
  }

  template<int N, class T>
  SpMatrix<N, T> operator!=(const SpMatrix<N, T>& m, T f){
    SpMatrix<N, T> r(m);
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpneq(f);
    }
    return r;
  }

  template<int N, class T>
  SpMatrix<N, T> operator==(T f, const SpMatrix<N, T>& m){
    return m==f;
  }

  template<int N, class T>
  SpMatrix<N, T> operator!=(T f, const SpMatrix<N, T>& m){
    return m!=f;
  }

  template<int N, class T>
  SpMatrix<N, T> operator<(const SpMatrix<N, T>& m, T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmplt(f);
    }
    return r;
  }

  template<int N, class T>
  SpMatrix<N, T> operator<=(const SpMatrix<N, T>& m, T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmple(f);
    }
    return r;
  }

  template<int N, class T>
  SpMatrix<N, T> operator>(const SpMatrix<N, T>& m, T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpgt(f);
    }
    return r;
  }

  template<int N, class T>
  SpMatrix<N, T> operator>=(const SpMatrix<N, T>& m, T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpge(f);
    }
    return r;
  }

  template<int N, class T>
  SpMatrix<N, T> operator<(T f, const SpMatrix<N, T>& m){
    return m>f;
  }

  template<int N, class T>
  SpMatrix<N, T> operator<=(T f, const SpMatrix<N, T>& m){
    return m>=f;
  }

  template<int N, class T>
  SpMatrix<N, T> operator>(T f, const SpMatrix<N, T>& m){
    return m<f;
  }

  template<int N, class T>
  SpMatrix<N, T> operator>=(T f, const SpMatrix<N, T>& m){
    return m<=f;
  }

  template<int N, class T>
  void SpMatrix<N, T>::printRow(int row)const{
    T mul = 200.0;
    T sum = 0;
    T sum2 = 0;
    int block_row = row/N;
    int local_row = row%N;

    for(int i=0;i<row_lengths[block_row];i++){
      int block_index = block_indices[block_row][i];
      int block_col   = col_indices[block_row][i] * N;

      for(int j=0;j<N;j++){
        message("(%d, %d) = %20.20e, %20.20e", row, block_col + j, blocks[block_index].m[local_row*N + j], blocks[block_index].m[local_row*N + j]*mul);
        sum += blocks[block_index].m[local_row*N + j] * mul;
        sum2 += blocks[block_index].m[local_row*N + j];
      }
    }

    message("sum = %10.10e", sum);
    message("sum2 = %10.10e, %10.20e", sum2, sum2*200);
  }

  template<int N, class T>
  void SpMatrix<N, T>::analyse()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      if(row_lengths[i] != 0){
        minrow = MIN(minrow, row_lengths[i]);
        maxrow = MAX(maxrow, row_lengths[i]);
        totalrow += row_lengths[i];
      }
    }

    message("%d stored elements", n_elements);
    message("%d minimal number of blocks", n_elements/(N*N));
    message("%d allocated blocks", n_blocks);
    message("Average block fill = %f", (float)n_elements/(float)n_blocks);

    message("Shortest row = %d", minrow);
    message("Longest  row = %d", maxrow);
    message("Average  row = %f", (float)totalrow/(float)(height/N));
  }

  template<int N, class T>
  int SpMatrix<N, T>::getNElements()const{
    return n_elements;
  }

  template<int N, class T>
  int SpMatrix<N, T>::getNBlocks()const{
    return n_blocks;
  }

  template<int N, class T>
  T SpMatrix<N, T>::powerMethod(T tol, T shift,
                                int firstRow, int lastRow){
    finalize();
    int dim = getHeight();

    Vector<T> x(dim);
    Vector<T> y(dim);
    Vector<T> xx(dim);

    int p = 0;

    T mu  = (T)0.0;
    T mu1 = (T)0.0;
    T mu2 = (T)0.0;

    T lastAitken = (T)0.0;

    if(firstRow == -1 || lastRow == -1){
      for(int i=0;i<dim;i++){
        x[i] = (T)1.0;
      }
    }else{
      for(int i=0;i<dim;i++){
        if(i >= firstRow && i < lastRow){
          x[i] = (T)1.0;
        }
      }
      p = firstRow;
    }

    for(int j=0;j<1000*dim;j++){

      spmv(y, *this, x);

      /*Apply spectral shift*/
      for(int k=0;k<dim;k++){
        y[k] -= shift * x[k];
      }

      mu = y[p];

      T muAitken = aitkenD2(mu2, mu1, mu);

      T nrm = y.infiniteNorm(&p);

      if(Abs(nrm) < tol && j > 4){
        message("Zero eigenvalue for vector");
        return muAitken + shift;
      }

      xx = x - y / y[p];

      T err = xx.infiniteNorm();

      message("[%d], err = %10.10e, l = %10.10e, a = %10.10e",
              j, err, mu + shift, muAitken + shift);

      x = y / y[p];

      if(Abs(err) < tol && j > 4){
        return muAitken + shift;
      }

      mu2 = mu1;
      mu1 = mu;

      if(Abs(Abs(lastAitken) - Abs(muAitken)) < tol && j > 4){
        return muAitken + shift;
      }
      lastAitken = muAitken;
    }

    message("Error, max iterations exceeded");
    return mu + shift;
  }

  template<int N, class T>
  T SpMatrix<N, T>::symmetricPowerMethod(T tol, T shift,
                                         int firstRow, int lastRow){
    finalize();
    int dim = getHeight();

    Vector<T> x(dim);
    Vector<T> y(dim);
    Vector<T> xx(dim);

    T mu  = (T)0.0;
    T mu1 = (T)0.0;
    T mu2 = (T)0.0;

    T lastAitken = (T)0.0;

    if(firstRow == -1 || lastRow == -1){
      for(int i=0;i<dim;i++){
        x[i] = (T)1.0;
      }
    }else{
      for(int i=0;i<dim;i++){
        if(i >= firstRow && i < lastRow){
          x[i] = (T)1.0;
        }
      }
    }

    x /= x.norm();

    for(int j=0;j<1000*dim;j++){

      spmv(y, *this, x);

      /*Apply spectral shift*/
      for(int k=0;k<dim;k++){
        y[k] -= shift * x[k];
      }

      mu = x * y;

      T muAitken = aitkenD2(mu2, mu1, mu);

      T nrm = y.length();

      if(Abs(nrm) < tol && j > 4){
        message("Zero eigenvalue for vector");
        return muAitken + shift;
      }

      xx = x - y / y.length();

      T err = xx.length();

      message("[%d], err = %10.10e, %10.10e, l = %10.10e, a = %10.10e",
              j, err, Abs(err - 2.0), mu + shift, muAitken + shift);

      x = y / y.length();

      if((Abs(err) < tol || Abs(err - (T)2.0) < tol ) && j > 4){
        return muAitken + shift;
      }

      mu2 = mu1;
      mu1 = mu;

      if(Abs(Abs(lastAitken) - Abs(muAitken)) < tol && j > 4){
        //message("lastAitken = %10.10e", lastAitken);
        //message("muAitken = %10.10e", muAitken);
        //return muAitken + shift;
      }
      lastAitken = muAitken;
    }

    message("Error, max iterations exceeded");
    return mu + shift;
  }

  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::getTranspose()const{
    //message("width = %d, height = %d", origWidth, origHeight);
    SpMatrix<N, T> ret(origHeight, origWidth);

    getTranspose(ret);

    return ret;
  }

  template<int N, class T>
  void SpMatrix<N, T>::getTranspose(SpMatrix<N, T>& out)const{
    out.clearStructure();

    for(int block_index=0;block_index<n_blocks;block_index++){
      int origRow = comp_row_indices[block_index];
      int origCol = comp_col_indices[block_index];

      for(int k=0;k<N;k++){
        for(int l=0;l<N;l++){
          T val = blocks[block_index].m[k*N+l];
          if(Abs(val) != 0.0){
            out[origCol + l][origRow + k] = val;
          }
        }
      }
    }
  }

  template<int N, class T>
  T SpMatrix<N, T>::infiniteNorm(){
    int n_rows = height/N;

    int idx = 0;

    T norm = 0.0;

    for(int i=0;i<n_rows;i++){
      T res[(uint)(N)];
      for(int j=0;j<N;j++){
        res[j] = 0.0;
      }

      int row_length = row_lengths[i];
      for(int j=0;j<row_length;j++, idx++){
        for(int k=0;k<N;k++){
          for(int l=0;l<N;l++){
            res[k] += Abs(blocks[idx].m[k*N+l]);
          }
        }
      }

      for(int j=0;j<N;j++){
        norm = Max(norm, res[j]);
      }
    }
    return norm;
  }

  template<int N, class T>
  T SpMatrix<N, T>::conditionNumber(bool symmetric,
                                    T& evMin, T& evMax,
                                    T tol,
                                    int firstRow, int lastRow){
    T lMin = (T)0.0;
    T lMax = (T)0.0;

    if(symmetric){
      lMax = symmetricPowerMethod(tol, (T)0.0, firstRow, lastRow);

      lMin = symmetricPowerMethod(tol, lMax, firstRow, lastRow);
    }else{
      lMax = powerMethod(tol, (T)0.0, firstRow, lastRow);

      lMin = powerMethod(tol, lMax, firstRow, lastRow);
    }

    message("max = %10.10e, min = %10.10e, K = %10.10e",
            lMax, lMin, Abs(lMax)/Abs(lMin));

    evMin = lMin;
    evMax = lMax;


    return Abs(lMax)/Abs(lMin);
  }

  template<int N, class T>
  int SpMatrix<N, T>::getShortestRow()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = MIN(minrow, row_lengths[i]);
      maxrow = MAX(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return minrow;
  }

  template<int N, class T>
  int SpMatrix<N, T>::getLongestRow()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = MIN(minrow, row_lengths[i]);
      maxrow = MAX(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return maxrow;
  }

  template<int N, class T>
  float SpMatrix<N, T>::getAverageRow()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = MIN(minrow, row_lengths[i]);
      maxrow = MAX(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return (float)totalrow/(float)(height/N);
  }

  template<int N, class T>
  float SpMatrix<N, T>::getAverageBlockFill()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = MIN(minrow, row_lengths[i]);
      maxrow = MAX(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return (float)n_elements/(float)n_blocks;
  }

  /*****Protected functions*******************/
  template<int N, class T>
  void SpMatrix<N, T>::grow_row(int row){
    int* tmp_blk_index = block_indices[row];
    int* tmp_col_index = col_indices[row];

    col_indices[row]   = new int[allocated_length_row[row]*2];
    block_indices[row] = new int[allocated_length_row[row]*2];

    memcpy(col_indices[row], tmp_col_index,
           sizeof(int)*(uint)allocated_length_row[row]);

    memcpy(block_indices[row], tmp_blk_index,
           sizeof(int)*(uint)allocated_length_row[row]);

    allocated_length_row[row] *=2;

    delete [] tmp_blk_index;
    delete [] tmp_col_index;
  }

  template<int N, class T>
  void SpMatrix<N, T>::grow_col(int col){
    int* tmp_blk_index = block_indices_col[col];
    int* tmp_row_index = row_indices[col];

    row_indices[col]   = new int[allocated_length_col[col]*2];
    block_indices_col[col] = new int[allocated_length_col[col]*2];

    memcpy(row_indices[col], tmp_row_index,
           sizeof(int)*(uint)allocated_length_col[col]);

    memcpy(block_indices_col[col], tmp_blk_index,
           sizeof(int)*(uint)allocated_length_col[col]);

    allocated_length_col[col] *=2;

    delete [] tmp_blk_index;
    delete [] tmp_row_index;
  }



  template<int N, class T>
  void SpMatrix<N, T>::grow_blocks(){
    SpMatrixBlock<N, T>* tmp = blocks;
    int* tmp_comp_col = comp_col_indices;
    int* tmp_comp_row = comp_row_indices;

    alignedMalloc((void**)&blocks, 16,
                  (uint)n_allocated_blocks*2*sizeof(SpMatrixBlock<N, T>));

    comp_col_indices = new int[n_allocated_blocks * 2];
    comp_row_indices = new int[n_allocated_blocks * 2];

    //memcpy(blocks, tmp, sizeof(SpMatrixBlock<N, T>)*(uint)n_allocated_blocks);
    for(int k=0;k<n_allocated_blocks;k++){
      blocks[k] = tmp[k];
    }
    memcpy(comp_col_indices, tmp_comp_col,
           sizeof(int)*(uint)n_allocated_blocks);
    memcpy(comp_row_indices, tmp_comp_row,
           sizeof(int)*(uint)n_allocated_blocks);

    n_allocated_blocks*=2;

    alignedFree(tmp);
    delete[] tmp_comp_col;
    delete[] tmp_comp_row;
  }

  template<int N, class T>
  std::ostream& operator<<(std::ostream& stream , const SpMatrix<N, T>& m){
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);
    //stream << std::setprecision(10);
    for(int row=0;row<m.origHeight;row++){
      for(int col=0;col<m.origWidth;col++){
        int block_index;
        int block_row;
        int block_col;
        bool found = false;

#if 1
        //std::map<int, int>::iterator it = m.block_map[row/N].find(col/N);

        //if(it != m.block_map[row/N].end()){
        int idx = m.block_map[row/N].findIndex(col/N);
        if(idx != Tree<int>::undefinedIndex){
          found = true;
          block_index = m.block_indices[row/N][idx];//[it->second];
          //block_index = m.block_indices[row/N][it->second];
          block_row = row%N;
          block_col = col%N;
        }
#else
        for(int k=0;k<m.row_lengths[row/N];k++){
          if(m.col_indices[row/N][k] == col/N){
            block_index = m.block_indices[row/N][k];
            block_row = row%N;
            block_col = col%N;
            found = true;
            break;
          }
        }
#endif

        if(found){
          stream << m.blocks[block_index].m[block_row*N+block_col] << "\t";
        }else{
          stream << T() << "\t";
        }
      }
      stream<<std::endl;
    }
    stream.flags(origflags);
    return stream;
  }

  template<int N, class T>
  void SpMatrix<N, T>::reorderBlocks(){
    /*Order the blocks such that they are ordered in memory in the way
      the are accessed. This should improve the cache coherency*/

    /*First build a reverse index such that we know where each block
      is located. This information is needed to swap the blocks
      properly*/
    reverse_index_t* reverseIndices = new reverse_index_t[n_blocks];

    int blockIndex = 0;

    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++){
        int block_index = block_indices[i][j];

        reverseIndices[block_index].blockRow = i;
        reverseIndices[block_index].blockCol = j;
      }
    }

    blockIndex = 0;

    /*Swap blocks*/

    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++){
        int tmpBlockIndex = block_indices[i][j];

        if(blockIndex != tmpBlockIndex){
          /*Swap blocks and adapt indices*/

          reverse_index_t rev = reverseIndices[blockIndex];

          SpMatrixBlock<N, T> tmpBlock1 = blocks[tmpBlockIndex];
          SpMatrixBlock<N, T> tmpBlock2 = blocks[blockIndex];
          cgfassert(rev.blockRow != -1);
          cgfassert(rev.blockCol != -1);

          block_indices[i][j]    = blockIndex;
          blocks[blockIndex]     = tmpBlock1;
          blocks[tmpBlockIndex]  = tmpBlock2;
          block_indices[rev.blockRow][rev.blockCol] = tmpBlockIndex;

          /*Update reverse pointers*/
          reverseIndices[blockIndex].blockRow = -1;
          reverseIndices[blockIndex].blockCol = -1;
          reverseIndices[tmpBlockIndex].blockRow = rev.blockRow;
          reverseIndices[tmpBlockIndex].blockCol = rev.blockCol;
        }
        blockIndex++;
      }
    }

    /*Each block is now moved to its row, but the row itself is
      unsorted*/

    for(int i=0;i<height/N;i++){
      //message("sort row %d", i);

      Tree<int>::Iterator it = block_map[i].begin();
#if 0
      while(it != block_map[i].end()){
        int col = *it++;

        int idx = block_map[i].findIndex(col);

        message("block_map col = %d, idx = %d", col, idx);

      }
#endif
      Tree<int> colMap;

      for(int j=0;j<row_lengths[i];j++){
        int col = col_indices[i][j];
        int block_index = block_indices[i][j];

        colMap.insert(col, block_index);
        //message("col = %d, block = %d", col, block_index);
      }

      SpMatrixBlock<N, T>* tmpBlocks =
        new SpMatrixBlock<N, T>[row_lengths[i]];

      //int* tmpColIndices = new int[row_lengths[i]];

      it = colMap.begin();

      //message("sort");
      for(int j=0;j<row_lengths[i];j++){
        int newColumn = *it++;
        int index = colMap.findIndex(newColumn);

        //message("new column = %d, block = %d", newColumn, index);

        tmpBlocks[j] = blocks[index];

        col_indices[i][j] = newColumn;
      }

      block_map[i].clear();

      for(int j=0;j<row_lengths[i];j++){
        blocks[block_indices[i][j]] = tmpBlocks[j];

        block_map[i].insert(col_indices[i][j], j);
      }

#if 0
      it = block_map[i].begin();
      while(it != block_map[i].end()){
        int col = *it++;
        int idx = block_map[i].findIndex(col);

        message("block_map col = %d, idx = %d", col, idx);
      }
#endif
      delete[] tmpBlocks;
    }

    delete [] reverseIndices;

    /*Align indices*/
#if 0
    int** tmp_col_indices = new int*[height/N];
    int** tmp_blk_indices = new int*[height/N];
    int n_indices = 0;
    for(int i=0;i<height/N;i++){
      tmp_col_indices[i] = new int[row_lengths[i]];
      memcpy(tmp_col_indices[i], col_indices[i], sizeof(int)*row_lengths[i]);
      n_indices+=row_lengths[i];
    }

    for(int i=0;i<height/N;i++){
      tmp_blk_indices[i] = new int[row_lengths[i]];
      memcpy(tmp_blk_indices[i], block_indices[i], sizeof(int)*row_lengths[i]);
    }

    for(int i=0;i<height/N;i++){
      delete[] col_indices[i];
      delete[] block_indices[i];
    }
    delete[] col_indices;
    delete[] block_indices;

    col_indices = tmp_col_indices;
    block_indices = tmp_blk_indices;
#endif

    int idx = 0;
    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++, idx++){
        comp_col_indices[idx] = col_indices[i][j] * N;
        comp_row_indices[idx] = i * N;
        //col_lengths[col_indices[i][j]]++;
      }
    }

    for(int i=0;i<width/N;i++){
      col_lengths[i] = 0;
    }

    idx = 0;
    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++, idx++){
        int row = comp_row_indices[idx]/N;
        int col = comp_col_indices[idx]/N;
        row_indices[col][col_lengths[col]] = row;
        block_indices_col[col][col_lengths[col]] = idx;
        col_lengths[col]++;
      }
    }
  }

  template<int N, class T>
  void spmv(Vector<T>& r, const SpMatrix<N, T>& m, const Vector<T>& v){
    MatrixRange range;

    if(v.getSize() != m.getWidth()){
      error("Dimensions of vector (%d) does not match width of matrix (%d)",
            v.getSize(), m.getWidth());
    }

    range.startRow = 0;
    range.endRow   = m.height/N;
    range.range    = m.height/N;

    spmv_partial(r, m, v, range);
  }

  /*Transposed multiplication*/
  template<int N, class T>
  void spmv_t(Vector<T>& r, const SpMatrix<N, T>& m, const Vector<T>& v){
    cgfassert(m.getHeight() == v.getSize());
    cgfassert(r.getSize()  == m.getWidth());
    cgfassert(m.finalized  == true);

    warning("spmv_t does not take sign into account");

    error("tBlocks are about to be removed");

    /*Clear tmp_data*/
    for(int i=0;i<m.width/N;i++){
      m.tBlocks[i].clear();
    }

    int idx = 0;
    /*Number of block rows*/
    int n_rows = m.height/N;

    const T* data = v.data;

    for(int i=0;i<n_rows;i++){
      int row_length = m.row_lengths[i];
      for(int j=0;j<row_length;j++, idx++){
        int blockIndex = m.comp_col_indices[idx]/N;
        m.tBlocks[blockIndex].vectorMulAddTranspose(&(m.blocks[idx]),
                                                    &(data[i*N]));
      }
    }

    /*Column reduce*/
    for(int i=0;i<m.width/N;i++){
      m.tBlocks[i].columnSumReduce();
    }

    for(int i=0;i<m.width/N;i++){
      for(int j=0;j<N;j++){
        r.data[i*N+j] = m.tBlocks[i].m[N*(N-1) + j];
      }
    }
  }

  int computeSteps(int size);
#if 0
  {
    /*Find a value, smaller than size, which is a power of 2*/
    int steps = size/2;
    int pwr = 1;
    while(steps >1){
      steps/=2;
      pwr++;
    }
    return 1<<pwr;
  }
#endif

#define SPLIT_MATRIX
#undef SPLIT_MATRIX

  /*Performs a partial sparse matrix vector
    multiplication. I.e. performs the multiplication such that the
    result vector of this operation is a subvector of the actual
    solution. This function enables the parallel multiplication of a
    sparse vector with a vector. At the end the resulting subvectors
    are combined to the actual result*/

  /*The result vector is divided in n separate sub vectors and this
    function computes the result denoted with index*/
  template<int N, class T>
  void spmv_partial(Vector<T>& r, const SpMatrix<N, T>& m,
                    const Vector<T>& v, const MatrixRange mr){
    cgfassert(m.getWidth() == v.getSize());
    cgfassert(r.getSize() == m.getHeight());
    cgfassert(m.finalized == true);

    if(mr.range == 0)
      return;
    if(mr.startRow == mr.endRow){
      return;
    }

    const T* data = v.data;


#ifdef SPLIT_MATRIX
    SpMatrixBlock<N, T> tmpBlockPos;
    SpMatrixBlock<N, T> tmpBlockNeg;
#else
    SpMatrixBlock<N, T> tmpBlock;
#endif

    for(int i=mr.startRow;i<mr.endRow;i++){
      int row_length = m.row_lengths[i];

      if(row_length > 0){
        bool processed = false;

        int idx = m.block_indices[i][0];
#ifdef SPLIT_MATRIX
        tmpBlockPos.clear();
        tmpBlockNeg.clear();
#else
        tmpBlock.clear();
#endif

        for(int j=0;j<row_length;j++,idx++){
          int columnIndex = m.comp_col_indices[idx];
          bool skip = true;
          for(int k=0;k<N;k++){
            if(data[columnIndex+k] != T()){
              skip = false;
              break;
            }
          }

          if(!skip){
#ifdef SPLIT_MATRIX
            tmpBlockPos.vectorMulAddP(&(m.blocks[idx]),
                                      &(data[columnIndex]));
            tmpBlockNeg.vectorMulAddN(&(m.blocks[idx]),
                                      &(data[columnIndex]));
#else
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(data[columnIndex]));
#endif
            processed = true;
          }
        }
#ifdef SPLIT_MATRIX
        if(processed){
          tmpBlockPos.rowSumReduce();
          tmpBlockNeg.rowSumReduce();
          for(int j=0;j<N;j++){
            r.data[i*N + j] = tmpBlockPos.m[j*N] + tmpBlockNeg.m[j*N];
          }
          for(int j=0;j<N;j++){
            r.data[i*N + j] = tmpBlockPos.m[j*N] + tmpBlockNeg.m[j*N];
          }
        }
#else
        if(processed){
          tmpBlock.rowSumReduce();

          for(int j=0;j<N;j++){
            r.data[i*N + j] = tmpBlock.m[j*N];
          }
        }
#endif
      }
    }
  }

#if 0
  template<int N, class T>
  void spmv_parallel(Vector<T>& r, const SpMatrix<N, T>& m, const Vector<T>& v){
    cgfassert(m.finalized == true);
    m.task->setVectors(&r, &v);
    m.pool->assignTask(m.task);
  }

  template<int N, class T>
  void SpMatrix<N, T>::finalizeParallel(ThreadPool* _pool){
    pool = _pool;
    if(task)
      delete task;

    finalize();

    task = new ParallelSPMVTask<N, T>(pool, this);
  }
#endif


  template<int N, class T>
  void SpMatrix<N, T>::computeBlockDistribution(MatrixRange* mRange,
                                                VectorRange* vRange,
                                                int* n_blks,
                                                int n_segments)const{
    cgfassert(blocks != 0);
    /*Compute distribution of the blocks over the segments. The
      corresponding vector dimension per segment is a multiple of 16,
      because the partial vector and matrix functions operate with
      chunks of 16 floats*/

	int n_combined_rows = 16/N;
    int blocks_per_segment = (int)ceil((float)n_blocks/(float)n_segments);

    int currentBlockRow = 0;
    int currentRow = 0;

    int totalBlocks = 0;
    for(int i=0;i<n_segments;i++){
      mRange[i].startRow = currentRow;
      currentBlockRow = 0;
      while(currentBlockRow < blocks_per_segment){
        int stop = 0;
        int k = 0;
        for(;k<n_combined_rows;k++){
          if(currentRow + k < height/N){
            currentBlockRow += row_lengths[currentRow + k];
          }else{
            stop = 1;
            break;
          }
        }
        currentRow += k;
        if(stop){
          break;
        }
      }

      mRange[i].endRow = currentRow;
      n_blks[i] = currentBlockRow;
      totalBlocks += currentBlockRow;
    }
    cgfassert(n_blocks == totalBlocks);

    for(int i=0;i<n_segments;i++){
      vRange[i].startBlock = mRange[i].startRow * N;
      vRange[i].endBlock   = mRange[i].endRow * N;
      vRange[i].range = vRange[i].endBlock - vRange[i].startBlock;
      mRange[i].range = mRange[i].endRow - mRange[i].startRow;
    }
  }

  template<int N, class T>
  void multiplyJacobi(Vector<T>& r, const SpMatrix<N, T>& m,
                      const Vector<T>& v){
    cgfassert(m.getWidth() == v.getSize());
    cgfassert(r.getSize()  == m.getHeight());
    cgfassert(m.finalized);

    int idx = 0;
    int n_rows = m.height/N;

    const T* data = v.data;

    SpMatrixBlock<N, T> tmpBlock;

    for(int i=0;i<n_rows;i++){
      int row_length = m.row_lengths[i];

      if(row_length > 0){
        tmpBlock.clear();

        int diagBlockIndex = -1;

        for(int j=0;j<row_length;j++, idx++){
          int col_index = m.comp_col_indices[idx]/N;
          if(col_index == i){
            //Diagonal block
            tmpBlock.vectorMulAddJacobi(&(m.blocks[idx]),
                                        &(data[col_index*N]));
            diagBlockIndex = idx;
          }else{
            //Off-diagonal block
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(data[col_index*N]));
          }
        }

        cgfassert(diagBlockIndex != -1);

        tmpBlock.rowSumReduce();

        /*Store and divide by diagonal*/
        for(int j=0;j<N;j++){
          r.data[i*N + j] = tmpBlock.m[j*N] / m.blocks[diagBlockIndex].m[j*N+j];
        }
      }
    }
  }

  template<int N, class T>
  void multiplyGaussSeidel(Vector<T>& r, const SpMatrix<N, T>& m,
                           const Vector<T>& v, const Vector<T>* b){
    cgfassert(m.getWidth() == v.getSize());
    cgfassert(r.getSize()  == m.getHeight());
    cgfassert(m.finalized);

    int idx = 0;
    int n_rows = m.height/N;

    int height = m.getHeight();

    const T* data = v.data;
    T* rdata = r.data;

    SpMatrixBlock<N, T> tmpBlock;

    for(int i=0;i<n_rows;i++){
      int row_length = m.row_lengths[i];

      if(row_length > 0){
        tmpBlock.clear();

        int diagBlockIndex = -1;

        for(int j=0;j<row_length;j++, idx++){
          int col_index = m.comp_col_indices[idx] / N;
          if(col_index == i){
            //Diagonal block
            /*Skip diagonal block for now, and is performed as last*/
            diagBlockIndex = idx;
          }else if(col_index < i){
            //Off-diagonal block, use vector r containing updated
            //values
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(rdata[col_index*N]));
          }else{
            //Off-diagonal block, use vector v
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(data[col_index*N]));

          }
        }

        cgfassert(diagBlockIndex != -1);

        tmpBlock.rowSumReduce();

        /*Store*/
        for(int j=0;j<N;j++){
          r.data[i*N + j] = -tmpBlock.m[j*N];

          if(b){
            r.data[i*N + j] += b->data[i*N + j];
          }
        }

        /*Perform multiplication of diagonal block*/
        int col_index = m.comp_col_indices[diagBlockIndex] / N;
        for(int j=0;j<N;j++){
          T diagElement = 0;

          for(int k=0;k<N;k++){
            if(j==k){
              /*Diagonal element*/
              diagElement = m.blocks[diagBlockIndex].m[j*N+k];
            }else if(j<k){
              /*Use vector v*/
              r.data[i*N+j] -= m.blocks[diagBlockIndex].m[j*N+k] *
                data[col_index*N + k];
            }else{
              /*Use updated k*/
              r.data[i*N+j] -= m.blocks[diagBlockIndex].m[j*N+k] *
                rdata[col_index*N + k];
            }
          }

          if(i*N + j < height){
            r.data[i*N + j] /= diagElement;
          }else{
            r.data[i*N + j] = (T)0.0;
          }
        }
      }
    }
  }

}

#endif/*SPMATRIX_HPP*/
