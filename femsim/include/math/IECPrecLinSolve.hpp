#ifndef IECPRECLINSOLVE_HPP
#define IECPRECLINSOLVE_HPP

#include "math/IECLinSolve.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"
#include "math/constraints/AbstractBlockConstraint.hpp"

namespace CGF{
  template<int N, class T>
  class IECPrecLinSolveT2 : public IECLinSolveT2<N, T>{
  public:
    typedef VectorC2<T>      VectorC;
    typedef SpMatrixC2<N, T> SpMatrixC;

    IECPrecLinSolveT2(int d):IECLinSolveT2<N, T>(d){
      C  = new VectorC(*(this->x));

      C2 = new SpMatrixC(d, d);

      C2->activeConstraints = this->activeConstraints;

      preconditionerScaling = new VectorC(*(this->x));

      this->vectors.append(C);
      this->vectors.append(preconditionerScaling);

      precScratch = new VectorC(d);

      this->vectors.append(precScratch);

      externalHandledVector = false;
      externalHandledMatrix = false;

      precMask = 0;
    }

    virtual ~IECPrecLinSolveT2(){
      if(externalHandledVector == false){
        delete C;
      }
      if(externalHandledMatrix == false){
        delete C2;
      }
      delete preconditionerScaling;

      delete precScratch;
    }

    virtual int addConstraint(AbstractRowConstraint<N, T>* c){
      if(externalHandledMatrix == false){
        int idx = IECLinSolveT2<N, T>::addConstraint(c);

        /*Add block constraint in preconditioner matrix*/

        C2->activeConstraints = this->activeConstraints;

        cgfassert(c->row_id_given != Constraint::undefined);

        AbstractBlockConstraint<N, T>* bc = c->constructBlockConstraint();

        C2->addConstraint(bc);
        bc->setRowConstraint(c);

        return idx;
      }
      return -1;
    }

    virtual void removeConstraint(int id){
      /*Remove block constraint from preconditioner matrix*/

      if(externalHandledMatrix == false){
        AbstractBlockConstraint<N, T>* bc =
          (AbstractBlockConstraint<N, T>*)C2->getConstraint(id);

        bc->destroy(*this->b, *this->x);

        C2->removeConstraint(id);

        delete bc;

        IECLinSolveT2<N, T>::removeConstraint(id);
      }
    }

    void computePreconditioner(){
      int height = this->mat->getHeight();

      if(externalHandledMatrix == false){
        C2->clear();
      }

      if(externalHandledVector == false){
        /*Diag preconditioner*/
        for(int i=0;i<height;i++){
          (*C)[i] = (T)1.0/Abs((*this->mat)[i][i]);
          if(IsNan((*C)[i])){
            std::cout << *C << std::endl;
            error("Singular value in diag preconditioner :: %10.10e, %10.1e", (*C)[i], (*this->mat)[i][i]);
            (*C)[i] = (T)1000.0;
          }

          if((*C)[i] > (T)1000.0){
            //(*C)[i] = (T)1000.0;
          }
        }
      }

      if(externalHandledMatrix == false){
        C2->finalize();
      }
    }

    void resetPreconditioner(){
      return;//Why did we want a reset?
      int height = this->mat->getHeight();

      if(externalHandledVector == false){
        /*Diag preconditioner*/
        for(int i=0;i<height;i++){
          (*C)[i] = (T)1.0;
        }
      }
    }

    void recomputePreconditioner(){
      if(externalHandledVector == false){
        message("compute preconditioner");


        /*Compute diagonal preconditioner*/
        for(int i=0;i < this->mat->getNConstraints();i++){
          AbstractRowConstraint<N, T>* c =
            (AbstractRowConstraint<N, T>*) this->mat->getActiveConstraint(i);

          c->computePreconditioner(*C);
        }
      }

      if(externalHandledMatrix == false){
        /*Compute block matrix preconditioner*/
        for(int i=0;i < this->mat->getNConstraints();i++){
          AbstractRowConstraint<N, T>* c =
            (AbstractRowConstraint<N, T>*) this->mat->getActiveConstraint(i);

          c->computePreconditionerMatrix(*C, *C2, *this->mat,
                                         this->constraintConnectivity,
                                         preconditionerScaling);
        }
        C2->finalize();
      }
    }

    virtual void computePreconditionerScaling(){
#ifdef PREC_FULL
      if(externalHandledMatrix == false){
        PRINT_FUNCTION;
        preconditionerScaling->clear();
        message("prec scaling, constraints = %d", this->mat->getNConstraints());

        T mx = 0.0;

        for(int i=0;i < this->mat->getNConstraints();i++){
          int nDuplicates = 0;

          AbstractRowConstraint<N, T>* c1 =
            (AbstractRowConstraint<N, T>*) this->mat->getActiveConstraint(i);

          c1->computePreconditioner(*C);

          //int nSubConstraints = 0;

          //get unique columns


          int testColumn = -1;

          Tree<int> uniqueColumns;

          bool generalConstraint = false;

          c1->print(std::cout);

          message("size = %d", c1->getSize());
          for(int j=0;j<c1->getSize();j++){
            int col = c1->getIndex(j);
            PRINT(col);

            message("currentColumn[%d] = %d", j, col);

            if(col != Constraint::undefined /*&& c1->status == Active*/){
              testColumn = col;
              uniqueColumns.uniqueInsert(col);

              if(c1->getValue(1,j).col == -1){
                generalConstraint = true;
                message("generalConstraint");
              }
              //break;
            }
          }

          Tree<int>::Iterator iter = uniqueColumns.begin();
          while(iter != uniqueColumns.end()){
            int col = *iter++;
            for(int k=0;k<3;k++){
              if(generalConstraint){
                (*preconditionerScaling)[col*3+k] += (T)1.0;///(T)3.0;
                mx = Max(mx,
                         (*preconditionerScaling)[testColumn*3+k] );
              }else{
                (*preconditionerScaling)[col*3+k] += (T)1.0;
                mx = Max(mx,
                         (*preconditionerScaling)[testColumn*3+k] );
              }
            }
          }
#if 0
          if(testColumn == -1){
            continue;
          }

          PRINT(testColumn);

          int nConnectedItems = this->constraintConnectivity->getDegree(testColumn);
          PRINT(nConnectedItems);

          message("%d connected items", nConnectedItems);

          for(int j=0;j<nConnectedItems;j++){
            int connectedItem =
              this->constraintConnectivity->getConnectedItem(testColumn, j);

            message("connected item %d", connectedItem);
            PRINT(connectedItem);

            AbstractRowConstraint<N, T>* c2 =
              (AbstractRowConstraint<N, T>*) this->mat->getConstraint(connectedItem);

            int n1 = 0;
            int n2 = 0;

            Tree<int> indicesA;
            Tree<int> indicesB;

            for(int k=0;k<c1->getSize();k++){
              int indexA = c1->getIndex(k);
              PRINT(indexA);
              if(indexA != Constraint::undefined){
                n1++;
                indicesA.uniqueInsert(indexA);
              }
            }

            for(int k=0;k<c2->getSize();k++){
              int indexB = c2->getIndex(k);
              PRINT(indexB);
              if(indexB != Constraint::undefined){
                n2++;
                indicesB.uniqueInsert(indexB);
              }
            }

            n1 = indicesA.size();
            n2 = indicesB.size();


            PRINT(n1);
            PRINT(n2);

            message("n1, n2: %d, %d", n1, n2);
#if 1
            int dup = 0;

            if(n1 == n2){
              /*Constraints have the same shape, check if the are identical*/
#if 0
              for(int k=0;k<c1->getSize();k++){
                int col = c1->getIndex(k);
                if(col == Constraint::undefined){
                  continue;
                }
                for(int l=0;l<c2->getSize();l++){
                  int row = c2->getIndex(l);

                  if(row == Constraint::undefined){
                    continue;
                  }

                  message("row = %d, col = %d", col, row);

                  if(col == row){
                    dup++;
                  }
                }
              }
#else
              Tree<int>::Iterator ita = indicesA.begin();
              while(ita != indicesA.end()){
                int indexA = *ita++;

                Tree<int>::Iterator itb = indicesB.begin();
                while(itb != indicesB.end()){
                  int indexB = *itb++;

                  message("index a, b = %d, %d", indexA, indexB);

                  if(indexA == indexB){
                    dup++;
                  }
                }
              }
#endif
              if(dup == n1 && c2->status == Active){
                nDuplicates++;
              }
            }
#endif
          }
#endif
          int cindex = c1->row_id;
          int offset = c1->getOffset();

          PRINT(nDuplicates);
          if(nDuplicates == 0){
            nDuplicates = 1;
          }

          T* xdata = preconditionerScaling->getExtendedData();
          for(int k=0;k<offset;k++){
            xdata[cindex * offset + k] = (T)1.0*((T)nDuplicates);
          }
        }
        PRINT(*preconditionerScaling);
        //std::cout << *preconditionerScaling << std::endl;
        message("end prec scaling");
        message("max = %10.10e", mx);
      }
#endif
    }


    virtual void applyPreconditioner(VectorC* ww, const VectorC* rr,
                                     PreconditionerApplication application,
                                     bool transposed = false){
      if(ww == rr){
        error("Result and input vector are the same");
      }
      //*ww = *rr;
      //return;
      if(application == LeftPreconditioning){
        this->timer.start("precond_vec");

        *ww = *rr;
        abort();
        this->timer.stop("precond_vec");
      }else{
        this->timer.start("applyPreconditioner");

        if(precMask){
          VectorC::mul(*precScratch, *precMask, *rr);

          if(transposed){
            spmvc_t(*ww, *C2, *precScratch, Full, precMask);
          }else{
            spmvc(*ww, *C2, *precScratch, Full, precMask);
          }

          VectorC::mul(*ww, *precMask, *ww);
        }else{
          if(transposed){
            spmvc_t(*ww, *C2, *rr, Full, precMask);
          }else{
            spmvc(*ww, *C2, *rr, Full, precMask);
          }
        }

        /*ww = C * rr + C2 * rr*/
        if(precMask){
          VectorC::madd(*ww, *C, *precScratch, *ww);
        }else{
          VectorC::madd(*ww, *C, *rr, *ww);
        }

        this->timer.stop("applyPreconditioner");
      }
    }

    void setPreconditionerMatrix(SpMatrixC* m){
      if(externalHandledMatrix == false){
        delete C2;
      }
      externalHandledMatrix = true;

      C2 = m;
    }

    void setPreconditionerVector(VectorC* v){
      if(externalHandledVector == false){
        this->vectors.remove(C);
        delete C;
      }
      externalHandledVector = true;
      C = v;
    }

    void setMaskVector(VectorC* mask){
      precMask = mask;
    }

    VectorC* C;    /*Diagonal preconditioner*/
    SpMatrixC* C2;
    VectorC* preconditionerScaling;
    VectorC* precMask;
    VectorC* precScratch;

    /*If C and/or C2 are managed outside this solver, only
      applyPreconditioner should do something.*/
    bool externalHandledVector;
    bool externalHandledMatrix;
  };
}

#endif/*IECPRECLINSOLVE_HPP*/
