#ifndef FORCE_HPP
#define FORCE_HPP

#include "math/SpMatrixC.hpp"
#include "math/Vector.hpp"
#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"

namespace CGF{

  template<class T>
  class Force{
  public:
    Force(){
      stiffness = (T)3600.0;
    }
    virtual void computeForcevector(Vector<T>* v,
                                    SpMatrixC2<2, T>* mat,
                                    T dt, bool full, bool* remove) = 0;

    void setStiffness(T s){
      stiffness = s;
    }
  protected:
    T stiffness;
  };


  /*Creates a springforce between two points in a mesh*/
  template<class T>
  class InterpolatedForce : public Force<T>{
  public:
    InterpolatedForce(const Matrix44<T>& v, const Vector4<T>& w,
                      const Vector4<T>& n,  const T _length, const int idx[4]){
      for(int i=0;i<4;i++){
        vertices[i] = v[i];
        weights[i]  = w[i];
        indices[i]  = idx[i];

        vertices[i][3] = (T)0.0;
      }
      normal = n;
      length = _length;
    }

    void computeForcevector(Vector<T>* v, SpMatrixC2<2, T>* gm,
                            T dt, bool full, bool* remove){
      Vector4<T> pA;
      Vector4<T> pB;

      /*Compute interpolated positions*/
      for(int i=0;i<4;i++){
        if(indices[i] != -1){
          if(weights[i] < (T)0.0){
            pA += vertices[i] * Abs(weights[i]);
          }else if(weights[i] > (T)0.0){
            pB += vertices[i] * Abs(weights[i]);
          }
        }
      }

      Vector4<T> distanceVector = pA - pB;
      Vector4<T> frame[3];

      createArbitraryOrthogonalFrame(normal,
                                     &(frame[0]),
                                     &(frame[1]),
                                     &(frame[2]));

      for(int i=0;i<3;i++){
        if(frame[i].isNaN()){
          std::cout << normal << std::endl;
          std::cout << frame[0] << std::endl;
          std::cout << frame[1] << std::endl;
          std::cout << frame[2] << std::endl;
          error("Frame[%d] is NaN", i);
        }


        T distancec = dot(frame[i], distanceVector);

        T forceDepth = distancec + length;

        if(i!=0){
          /*In tangential directions the spring length is zero*/
          forceDepth = distancec;
        }

        for(int j=0;j<4;j++){
          if(indices[j] != -1){
            Vector4<T> force =
              weights[j] * this->stiffness * frame[i] * forceDepth * dt;

            for(int k=0;k<3;k++){
              (*v)[indices[j]*3+k] += force[k];
            }
          }
        }

        Matrix44<T> normalTensor = frame[i].outherProduct(frame[i]);
        T damping = (T)10.0 * 1.0;
        /*Set Jacobian*/
        for(int j=0;j<4;j++){
          for(int k=0;k<4;k++){
            if(indices[j] != -1 && indices[k] != -1){
              Matrix44<T> nt =
                dt*dt*weights[j]*weights[k]*this->stiffness*normalTensor +
                dt*Matrix44<T>::Identity*damping*weights[j]*weights[k];

              for(int l=0;l<3;l++){
                for(int m=0;m<3;m++){
                  (*gm)[indices[j]*3+l][indices[k]*3+m] += nt[l][m];
                }
              }
            }
          }
        }
        if(!full){
          i = 10;
          break;
        }
      }
    }
  protected:
    Matrix44<T> vertices;
    int indices[4];
    T length;
    Vector4<T> weights;
    Vector4<T> normal;
  };

  template<class T>
  class VertexFaceForce : public Force<T>{
  public:
    VertexFaceForce(Matrix44<T>& vert,
                    Vector4<T>& n,
                    Vector4<T>& b,
                    T _length, int ind[4]){
      for(int i=0;i<4;i++){
        vertices[i]    = vert[i];
        indices[i]     = ind[i];
        vertices[i][3] = (T)0.0;
      }
      normal = n;
      bary   = b;
      length = _length;
    }

    void computeForcevector(Vector<T>* v, SpMatrixC2<2, T>* mat, T dt,
                            bool full, bool* remove){
      Triangle<T> tri(vertices[0], vertices[1], vertices[2]);
      Vector4<T> weights(-bary[0], -bary[1], -bary[2], (T)1.0);

      message("VF weights sum = %10.10e, should be zero", weights.sum());
      std::cout << weights << std::endl;

      InterpolatedForce<T> interpForce(vertices, weights,
                                       tri.n, length, indices);
      interpForce.computeForcevector(v, mat, dt, full, remove);
    }

    Vector4<T> getNormal()const{
      return normal;
    }

  protected:
    Matrix44<T> vertices;
    Vector4<T> normal;
    Vector4<T> bary;
    T length;
    int indices[4];
  };

  template<class T>
  class EdgeEdgeForce : public Force<T>{
  public:
    EdgeEdgeForce(Matrix44<T>& vert,
                  Vector4<T>& n,
                  Vector4<T>& b, T dist, int ind[4]){
      for(int i=0;i<4;i++){
        vertices[i]    = vert[i];
        indices[i]     = ind[i];
        vertices[i][3] = (T)0.0;
      }
      normal = n;
      bary = b;
      distance = dist;
    }

    void computeForcevector(Vector<T>* v, SpMatrixC2<2, T>* mat,
                            T dt, bool full, bool* remove){

      TriangleEdge<T> edgeA(vertices[0], vertices[1]);
      TriangleEdge<T> edgeB(vertices[2], vertices[3]);

      Vector4<T> p1, p2;
      Vector4<T> b1, b2;

      try{
        edgeA.getOutwardNormal(edgeB, &normal, &p1, &p2, &b1, &b2);

        Vector4<T> weights(-b1[0], -b1[1], b2[0], b2[1]);

        message("EE weights sum = %10.10e", weights.sum());

        InterpolatedForce<T> interpForce(vertices, weights,
                                         normal, distance,
                                         indices);
        interpForce.computeForcevector(v, mat, dt, full, remove);
      }catch(Exception* e){
        std::cout << e->getError().c_str() << std::endl;
        message("vertices");
        std::cout << vertices << std::endl;
        std::cout << edgeA << std::endl;
        std::cout << edgeB << std::endl;
        Vector4<T> va = (vertices[0] - vertices[1]).normalize();
        Vector4<T> vb = (vertices[2] - vertices[3]).normalize();

        std::cout << va << std::endl;
        std::cout << vb << std::endl;
        std::cout << cross(va, vb) << std::endl;

        message("Vol = %10.10e", vertices.det()/(T)6.0);

        getchar();
        delete e;
      }
    }

    Vector4<T> getNormal()const{
      return normal;
    }

  protected:
    Matrix44<T> vertices;
    Vector4<T> normal;
    Vector4<T> bary;
    T distance;
    int indices[4];
  };


  template<class T>
  class EdgeForce : public Force<T>{
  public:
    EdgeForce(Vector4<T>& v1, Vector4<T>& v2,
              Vector4<T>& n, T _length,
              int idx1, int idx2){
      vertices[0] = v1;
      vertices[1] = v2;

      vertices[0][3] = vertices[1][3] = (T)0.0;

      normal = n;

      /*Length of spring in rest*/
      length = _length;

      indices[0] = idx1;
      indices[1] = idx2;
    }

    void computeForcevector(Vector<T>* v, SpMatrixC2<2, T>* gm,
                            T dt, bool full, bool* remove){
      Matrix44<T> coords;
      coords[0] = vertices[0];
      coords[1] = vertices[1];

      int lindices[4];
      lindices[0] = indices[0];
      lindices[1] = indices[1];
      lindices[2] = -1;
      lindices[3] = -1;

      Vector4<T> weights((T)-1.0, (T)1.0, (T)0.0, (T)0.0);

      InterpolatedForce<T> interpForce(coords, weights,
                                       normal, length, lindices);

      interpForce.computeForcevector(v, gm, dt, full, remove);
    }

    Vector4<T> getNormal()const{
      return normal;
    }
  protected:
    Vector4<T> vertices[2];
    Vector4<T> normal;
    T length;
    int indices[2];//EdgeIndices
  };


}

#endif/*FORCE_HPP*/
