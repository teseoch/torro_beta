/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef CSVEXPORTER_HPP
#define CSVEXPORTER_HPP

#include "core/cgfdefs.hpp"
#include "stdio.h"
#include <map>
#include <string>

namespace CGF{
  class CSVExporter{
  public:
    CSVExporter(FILE* f);
    ~CSVExporter();

    void addColumn(const char* columnName);
    void setValue(const char* column, float value);
    void setValue(const char* column, double value);
    void setValue(const char* column, uint value);
    void setValue(const char* column, int value);
    void setValue(const char* column, long value);
    void setValue(const char* column, ulong value);
    void setValue(const char* column, const char* value);

    void saveHeader();
    void saveRow();
    void clear(){
      n_columns = 0;
      valueMap.clear();
      indexMap.clear();
    }
  protected:
    FILE* file;
    bool headerWritten;
    std::map<std::string, std::string> valueMap;
    std::map<int, std::string> indexMap;
    int n_columns;

  };
}

#endif/*CSVEXPORTER_HPP*/
