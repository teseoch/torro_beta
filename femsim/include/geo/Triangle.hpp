/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "math/Vector3.hpp"
#include "math/Matrix22.hpp"
#include "math/Matrix33.hpp"

#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "math/Quaternion.hpp"

#include "math/Ray.hpp"
#include "core/Exception.hpp"
#include <stdio.h>
namespace CGF{
#if 0
  /*LUT for inside test*/
  static int validBits[] = {true,  //0
                            false, //1
                            true,  //2
                            false, //3
                            false, //4
                            false, //5
                            true,  //6
                            false, //7
                            true,  //8
                            true,  //9
                            true,  //10
                            false, //11
                            false, //12
                            false, //13
                            false, //14
                            false  //15
  };

#endif

  static int edgesInside[] = {1,   //0
                              5,   //1
                              5,   //2
                              4,   //3
                              3,   //4
                              14,  //5
                              14,  //6
                              12,  //7
                              3,   //8
                              14,  //9
                              14,  //10
                              12,  //11
                              2,   //12
                              10,  //13
                              10,  //14
                              8};  //15


#define DET_EPS 1e-12*0


  /*v0 = p2-p1, v1 = p-p1*/
  template<class T>
  inline Vector4<T> getBarycentricCoordinatesEdge(Vector4<T>& v0,
                                                  Vector4<T>& v1){
    T dot00 = dot(v0, v0);
    T dot01 = dot(v0, v1);
    T u = (dot01)/(dot00);
    return Vector4<T>((T)1.0-u, u, 0, 0);
  }

  template<class T>
  inline bool barycentricFaceMatch(const Vector4<T>& a, const Vector4<T>& b){
    char v1[3];
    char v2[3];
    int count = 0;
    for(int i=0;i<3;i++){
      if(a[i] <= (T)0.0){
        v1[i] = 1;
      }else if(a[i] <= (T)1.0){
        v1[i] = 2;
      }else{
        v1[i] = 3;
      }

      if(b[i] <= (T)0.0){
        v2[i] = 1;
      }else if(b[i] <= (T)1.0){
        v2[i] = 2;
      }else{
        v2[i] = 3;
      }
      if(v1[i] == v2[i]){
        count++;
      }
    }
    if(count == 3){
      return true;
    }
    return false;
  }


  template<class T>
  class TriangleEdge2;

  template<class T>
  class TriangleEdge3;

  template<class T>
  class TriangleEdge{
  public:
    TriangleEdge(){
      //v[0] = v[1] = n[0] = n[1] = t[0] = t[1] = Vector4f(false);
    }

    TriangleEdge(const Vector4<T>& a, const Vector4<T>& b){
      v[0] = a;
      v[1] = b;
      /*n not defined yet*/
    }

    TriangleEdge(const TriangleEdge<T>& e){
      v[0] = e.v[0];
      v[1] = e.v[1];

      n[0] = e.n[0];
      n[1] = e.n[1];

      t[0] = e.t[0];
      t[1] = e.t[1];

      fv[0] = e.fv[0];
      fv[1] = e.fv[1];
    }

    TriangleEdge(const TriangleEdge2<T>& e);

    TriangleEdge<T>& operator=(const TriangleEdge<T>& e){
      if(&e != this){
        v[0]  = e.v[0];
        v[1]  = e.v[1];

        n[0]  = e.n[0];
        n[1]  = e.n[1];

        t[0]  = e.t[0];
        t[1]  = e.t[1];

        fv[0] = e.fv[0];
        fv[1] = e.fv[1];
      }
      return *this;
    }

    void set(const Vector4<T>& a, const Vector4<T>& b){
      v[0] = a;
      v[1] = b;
      n[0].set(0,0,0,0);
      n[1].set(0,0,0,0);
      t[0].set(0,0,0,0);
      t[1].set(0,0,0,0);
      fv[0].set(0,0,0,0);
      fv[1].set(0,0,0,0);
      error("called?");
    }

    void set(const Vector4<T>& a, const Vector4<T>& b,
             const Vector4<T>& c, const Vector4<T>& d){
      v[0]  = a;
      v[1]  = b;
      fv[0] = c;
      fv[1] = d;
    }

    void computeNormalAndTangent(){
      Vector4<T> v1v0 = v[1]  - v[0];
      Vector4<T> v2v0 = fv[0] - v[0];

      n[0] = cross(v1v0, v2v0);

      v1v0 = v[0] - v[1];
      v2v0 = fv[1] - v[1];

      n[1] = cross(v1v0, v2v0);

      n[0].normalize();
      n[1].normalize();

#if 1
      t[0] = fv[0] - v[0];
      t[1] = fv[1] - v[0];

      t[0] = fv[0] - v[0];
      t[1] = fv[1] - v[0];

      Vector4<T> ee = v[1] - v[0];

      if(ee.length() < 1e-9){
        std::cout << *this << std::endl;
        error("Too small edge!!");
      }

      t[0] = cross(ee, t[0]);
      t[0] = cross(t[0], ee);

      t[1] = cross(ee, t[1]);
      t[1] = cross(t[1], ee);

      t[0].normalize();
      t[1].normalize();
#else
      Vector4<T> ee = v[1] - v[0];
      ee.normalize();
      Vector4<T> t1 = fv[0] - v[0];
      Vector4<T> t2 = fv[1] - v[0];

      T dt1 = dot(ee, t1);
      T dt2 = dot(ee, t2);

      t[0] = t1 - dt1 * ee;
      t[1] = t2 - dt2 * ee;
      message("t1.length = %10.10e", t[0].length());
      message("t2.length = %10.10e", t[1].length());
      t[0].normalize();
      t[1].normalize();
#endif
    }

    bool isConvex(){
      computeNormalAndTangent();

      T dtn1 = dot(t[0], n[1]);
      T dtn2 = dot(t[1], n[0]);

      /*If an edge is convex, then t[0].n[1] < 0 && t[1].n[0] < 0*/

      if(dtn1 <= (T)0.0 && dtn2 <= (T)0.0){
        return true;
      }else{
        if(Sign(dtn1) != Sign(dtn2)){
          if(Abs(dtn1 + dtn2) > 1e-10){
            std::cout << *this << std::endl;
            error("Indeterminate flat");
          }
        }
        return true;
      }
      return false;
    }

    bool isConcave(){
      return !isConvex();
    }

    T getFaceArea(int f)const{
      Vector4<T> v1 = v[1] - v[0];
      Vector4<T> v2 = fv[f] - v[0];

      v1[3] = v2[3] = 0;

      Vector4<T> normal = cross(v1, v2);
      return (T)0.5 * normal.length();
    }

    bool isIndeterminate()const{
      T area1 = getFaceArea(0);
      T area2 = getFaceArea(1);

      if(area1 < 1e-8 && area2 < 1e-8){
        return true;
      }
      return false;
    }

    bool faceEdgeIntersection(const TriangleEdge3<T>& edge, int faceId,
                              Vector4<T>& intersection,
                              bool* degenerate)const{
      Vector4<T> dir = edge.v[1] - edge.v[0];
      //Vector4<T> dir(0,0,1,0);
      //Vector4<T> s(-10,-10,-10,0);
      Vector4<T> s = edge.v[0];

      *degenerate = false;

      Matrix44<T> faceVertices;
      if(faceId == 0){
        faceVertices[0] = v[0];
        faceVertices[1] = v[1];
        faceVertices[2] = fv[0];
      }else{
        faceVertices[0] = v[1];
        faceVertices[1] = v[0];
        faceVertices[2] = fv[1];
      }

      Matrix33<T> A;
      A.m[0] = faceVertices[1][0] - faceVertices[0][0];
      A.m[1] = faceVertices[2][0] - faceVertices[0][0];
      A.m[2] = -dir[0];

      A.m[3] = faceVertices[1][1] - faceVertices[0][1];
      A.m[4] = faceVertices[2][1] - faceVertices[0][1];
      A.m[5] = -dir[1];

      A.m[6] = faceVertices[1][2] - faceVertices[0][2];
      A.m[7] = faceVertices[2][2] - faceVertices[0][2];
      A.m[8] = -dir[2];

      Vector3<T> b(s[0]- faceVertices[0][0],
                   s[1]- faceVertices[0][1],
                   s[2]- faceVertices[0][2]);

      T det = 0;
      Vector3<T> x = A.inverse(&det) * b;

      message("det = %10.10e", det);

      if(Abs(det) <= DET_EPS){
        *degenerate = true;
        /*Edge is parallel to face*/
        return false;
      }else{
        Vector4<T> bb((T)1.0 - x.m[0] - x.m[1], x.m[0], x.m[1], 0);

        message("bary");
        std::cout << x << std::endl;

        message("intersection edge");
        std::cout << s + x.m[2] * dir;

        message("intersection face");
        intersection =
          faceVertices[0] +
          x.m[0] * (faceVertices[1] - faceVertices[0]) +
          x.m[1] * (faceVertices[2] - faceVertices[0]);

        std::cout << intersection << std::endl;

        if(x.m[1] >= 0.0){
          return true;
        }else{
          /*Intersection on other side of edge.*/
          return false;
        }

        return false;
      }
      return false;
    }

    /*Used for detecting between a non- intersecting state -
      intersecting state*/
    bool edgeIntersects(const TriangleEdge<T>& edge)const{
      /*If both edges are convex, then an intersection is possible*/
      if(isConvex() && edge.isConvex()){
        Vector4<T> ca, cb;
        bool dega = false;
        bool degb = false;

        bool intersectionA = faceEdgeIntersection(edge, 0, ca, &dega);
        bool intersectionB = faceEdgeIntersection(edge, 1, cb, &degb);

        if(dega && !degb){
          /*edge is parallel to face A*/
          if(intersectionB){
            return true;
          }else{
            return false;
          }
        }else if(!dega && degb){
          /*edge is parallel to face B*/
          if(intersectionA){
            return true;
          }else{
            return false;
          }
        }else if(dega && degb){
          /*edge is parallel to both faces*/
        }else{
          /*edge is not parallel to both faces*/
          if(intersectionA && intersectionB){
            return true;
          }else{
            return false;
          }
        }
      }
      /*A concave edge can not have an intersection with another
        edge*/
      return false;
    }

    /*Projects point p on this edge*/
    Vector4<T> getProjection(const Vector4<T>& p)const{
      return getCoordinates(getBarycentricCoordinates(p));
    }

    Vector4<T> getBarycentricCoordinates(const Vector4<T>& p)const{
      Vector4<T> v0 = v[1] - v[0];
      Vector4<T> v1 = p    - v[0];
      return getBarycentricCoordinatesEdge(v0, v1);
    }

    T getLength()const{
      return (v[0] - v[1]).length();
    }

    Vector4<T> getCoordinates(const Vector4<T>& bary)const{
      return v[0] * bary[0] + v[1] * bary[1];
    }

    Vector4<T> clampWeights(const Vector4<T>& bary, T mn, T mx)const{
      T b0 = Clamp(bary[1], mn, mx);
      T r  = Clamp((T)1.0 - b0, mn, mx);
      return Vector4<T>(r, b0, 0, 0);
    }

    bool barycentricOnEdge(const Vector4<T>& bary, T eps)const{
      if((bary[0] >= -eps) && (bary[1] >= -eps) &&
         (bary[0] <= 1+eps) && (bary[1] <= 1+eps)){
        return true;
      }
      return false;
    }

    bool barycentricOnEdgeDist(const Vector4<T>& bary, T dist)const{
      if(barycentricOnEdge(bary, (T)0.0)){
        /*On edge*/
        return true;
      }else{
        Vector4<T> p = getCoordinates(bary);

        T d1 = (p - v[0]).length();
        T d2 = (p - v[1]).length();

        T d = Min(d1, d2);

        if(d < dist){
          return true;
        }
      }
      return false;
    }

    bool pointProjectsOnEdge(const Vector4<T>& p, T eps = 0)const{
      Vector4<T> bary = getBarycentricCoordinates(p);
      return barycentricOnEdge(bary, eps);
    }

    T getPlaneDistance(const Vector4<T>& p, const Vector4<T>& n)const{
      Vector4<T> A, B;
      if((p-v[0]).length() < (p-v[1]).length()){
        A = v[0];
        B = v[1];
      }else{
        A = v[1];
        B = v[0];
      }

      /*Least-Square approximation for p = A + (B-A)d*/

      Vector4<T> BA = B - A;
      Vector4<T> PA = p - A;

      double BABA  = (double)dot(BA, BA);
      double BAPA  = (double)dot(BA, PA);

      double d = BAPA / BABA;

      Vector4<T> C = A + BA*(T)d;

      Vector4<T> dir = p - C;

      return dot(dir, n);
    }

    void sampleContactNormal(const TriangleEdge<T>& edge,
                             Vector4<T>& normal,
                             Vector4<T>& p)const;

    /*Valid configuration is called after one solve, so the
      configuration should be collision free. If points are 'inside'
      an edge, the configuration is marked as invalid and not used
      during the solve process*/
    bool validConfiguration(const TriangleEdge<T>& e,
                            const Vector4<T>* reference,
                            Vector4<T>* dvp = 0)const{
      Vector4<T> dv;
      return true;
      try{
        dv = getOutwardNormal(e, reference);
      }catch(Exception* e){
        delete e;
        return false;
      }

      if(dvp){
        *dvp = dv;
      }

      Vector4<T> edge = v[0] - v[1];
      edge.normalize();

      T dvt0 = dot(dv, t[0]);
      T dvt1 = dot(dv, t[1]);

      T l1 = n[0].length();
      T l2 = n[1].length();

      if(l1 < 0.5 || l2 < 0.5){
        std::cout << n[0] << std::endl;
        std::cout << n[1] << std::endl;
        error("normals not correct");
      }

      T dtn1 = dot(t[0], n[1]);
      T dtn2 = dot(t[1], n[0]);

      if(Sign(dtn1) != Sign(dtn2)){
        if(Abs(dtn1 + dtn2) > 1e-10){
          std::cout << *this << std::endl;
          message("dtn1 = %10.10e", dtn1);
          message("dtn2 = %10.10e", dtn2);
          message("err = %10.10e", Abs(dtn1 - dtn2));
          message("%10.10e", dot(t[0], n[0]));
          message("%10.10e", dot(t[1], n[1]));
          //error("t1.n2 != t2.n1");
        }
      }

      T area1 = getFaceArea(0);
      T area2 = getFaceArea(1);

      Vector4<T> halfVector = n[0] * area1 + n[1] * area2;

      if(halfVector.length() < 1e-8){
        //return false;
        message("areas = %10.10e, %10.10e", area1, area2);
        std::cout << *this << std::endl;
        std::cout << e << std::endl;
        error("flat configuration");
      }else{
        halfVector.normalize();
      }

      T dvhv = dot(dv, halfVector);

      T dve = Abs(dot(dv, edge));

      if(dvt0 < (T)0.5){
        if(dvt1 < (T)0.5){
          if(dvhv > (T)0.0){
            if(dve < (T)0.5){
              //return true;
              /*Distance vector has a proper angle wrt tangent vectors,
                check if the distance vector points outwards*/
              //Vector4<T> pp = v[0] + dv;
              bool pi = pointsInside(dv);

              return !pi;
            }
          }
        }
      }
      return false;
    }


    /*Test if a point is 'inside' the edge, given the two tangent and
      normal vectors of the connecting faces.*/
    bool pointsInside(const Vector4<T>& pp)const{
      Vector4<T> bits(1,2,4,8);
      //Vector4<T> pp = (p-v[0]).normalize();
#if 1
      Vector4<T> re(dot(pp, n[0]),  /*bit 1*/
                    dot(pp, t[0]),  /*bit 2*/
                    dot(pp, n[1]),  /*bit 3*/
                    dot(pp, t[1])); /*bit 4*/

      if(re.length() < 1e-8){
        /*This is an exceptional case which can not happen. Since both
          the normal and tangent vectors are perpendicular, pp can not
          be perpendicual to both n and t. The only case in which this
          can happen is when pp is aligned with the edge, which is not
          possible by design. Vector pp is always perpendicular to the
          edge.*/
        error("Exception pointsInside case");
        return false;
      }

      int result = (int)dot(re > (T)0.0, bits);
#else
      T pn0 = dot(pp, n[0]); /*bit 1*/
      T pt0 = dot(pp, t[0]); /*bit 2*/

      T pn1 = dot(pp, n[1]); /*bit 3*/
      T pt1 = dot(pp, t[1]); /*bit 4*/
      int result = 0;

      if(pn0 > 0){
        result |= 1;
      }

      if(pt0 > 0){
        result |= 2;
      }

      if(pn1 > 0){
        result |= 4;
      }

      if(pt1 > 0){
        result |= 8;
      }

#endif
      //message("case = %d", result);
      //message("%10.10e, %10.10e, %10.10e, %10.10e", pn0, pt0, pn1, pt1);

      /*
        value | in / out
        --------------------------
        0     | in
        1     | out
        2     | in
        3     | out
        4     | out
        5     | out
        6     | in
        7     | out
        8     | in
        9     | in
        10    | in
        11    | out
        12    | out
        13    | out
        14    | out
        15    | out

      */

#if 1
      switch(result){
      case 0:
      case 2:
      case 6:
      case 8:
      case 9:
      case 10:
        return true;
      default:
        return false;
      }
#else
      return validBits[result];
#endif
    }

    /*Returns by definition a distance vector pointing outwards from
      the geometry, i.e., n1*dv > 0 && n2*dv > 0, assuming that the
      reference vector point from this edge to e if the signed
      distance is positive.*/
    Vector4<T> getCorrectedDistanceVector(const TriangleEdge<T>&e,
                                          const Vector4<T>& reference,
                                          Vector4<T>* pp1 = 0,
                                          Vector4<T>* pp2 = 0,
                                          Vector4<T>* bb1 = 0,
                                          Vector4<T>* bb2 = 0,
                                          bool* degenerate = 0)const{
      Vector4<T> p1, p2, b1, b2;
      /*p1 = projection of e on this
        p2 = projection of this on e*/
      error("deprecated function");
      Vector4<T> dv = getDistanceVector(e, &p1, &p2, &b1, &b2, degenerate);

      if(pp1)*pp1 = p1;
      if(pp2)*pp2 = p2;
      if(bb1)*bb1 = b1;
      if(bb2)*bb2 = b2;

      if(dot(reference, dv) < 0){
        /*Flip*/
        return -dv;
      }
      return dv;
    }

    /*Obtain correct distance vector without a reference vector, but
      inspecting all normal and tangent vectors instead,*/
    Vector4<T> getOutwardNormal(const TriangleEdge<T>& e,
                                const Vector4<T>* reference,
                                Vector4<T>* pp1 = 0,
                                Vector4<T>* pp2 = 0,
                                Vector4<T>* bb1 = 0,
                                Vector4<T>* bb2 = 0)const;

    /*Vector dv should always point out of this edge, but function
      getDistanceVector returns a vector which can point inside or
      outside. It first tries to compute the projections of both edges
      on each other and then computes the normale vector through both
      points. If their distance is too small, the cross product is
      used instead. Since the signed distance between both edges can
      be negative and the direction of the crossproduct has no
      meaning, we must correct the computed vector. One strategy is by
      using a reference vector. This vector is computed at the moment
      the edge-edge pair is discovered first and updated every
      step. However, if this vector is set incorrectly, all following
      computations are performed not correct. In this function we
      compute the direction of this vector using all tangent and
      normal vectors. */

    T getSignedDistanceAndOutwardNormal(const TriangleEdge<T>& e,
                                        Vector4<T>* normal,
                                        Vector4<T>* pp1 = 0,
                                        Vector4<T>* pp2 = 0,
                                        Vector4<T>* bb1 = 0,
                                        Vector4<T>* bb2 = 0,
                                        const Vector4<T>* reference = 0)const;


    /*By definition it returns the vector between this and e. If the
      distance approaches zero, */
    Vector4<T> getDistanceVector(const TriangleEdge<T>& e,
                                 Vector4<T>* p1 = 0,
                                 Vector4<T>* p2 = 0,
                                 Vector4<T>* b1 = 0,
                                 Vector4<T>* b2 = 0,
                                 bool* degenerate = 0)const{
      /*Compute vector from this edge to e*/

      Vector4<T> n1, n2, bb1, bb2;
      bool parallel = false;

      infiniteProjections(e, &n1, &n2, &bb1, &bb2, &parallel);

      if(parallel){
        if(degenerate){
          *degenerate = true;
        }
        throw new DegenerateCaseException(__LINE__, __FILE__, "unhandled parallel edges");
      }

      if(p1) *p1 = n1;
      if(p2) *p2 = n2;
      if(b1) *b1 = bb1;
      if(b2) *b2 = bb2;

      Vector4<T> normal = n2 - n1;

      if(normal.length() < 1E-1){
        /*In this case the distance is close to zero*/
        Vector4<T> a =   v[1] -   v[0];
        Vector4<T> b = e.v[1] - e.v[0];

        normal = cross(a, b);
      }

      normal.normalize();

      if(IsNan(normal)){
        throw new DegenerateCaseException(__LINE__, __FILE__, "NaN");
      }

      return normal;
    }

    T getClosestDistance(const TriangleEdge<T>& e, const Vector4<T>& p)const{
      Vector4<T> dv = getDistanceVector(e);
      Vector4<T> dir = p-v[0];
      error("used?");
      return dir*dv;
    }

  public:

    /*Shortest distance from edge to point*/
    T getDistance(const Vector4<T>& p, const T eps,
                  Vector4<T>* ep = 0)const{
      Vector4<T> ba = v[1] - v[0];
      Vector4<T> pa = p    - v[0];

      T t = dot(ba, pa) / dot(ba, ba);

      Vector4<T> pp = ba * t + v[0];

      if(ep){
        *ep = pp;
      }

      return (p-pp).length() - eps;
    }

    /*Signed distance between two edges*/
    T getSignedDistance(const TriangleEdge<T>& e,
                        Vector4<T>* p1=0,
                        Vector4<T>* p2=0,
                        Vector4<T>* b1=0,
                        Vector4<T>* b2=0,
                        const Vector4<T>* reference=0)const;

    /*Computes the two projections (p1, p2) of two edges (this, e) on
      each other, with b1, b2 the computed Barycentric coordinates of
      p1, p2*/
    void infiniteProjections(const TriangleEdge<T>& e,
                             Vector4<T>* p1,   Vector4<T>* p2,
                             Vector4<T>* b1=0, Vector4<T>* b2 = 0,
                             bool* parallel = 0)const{
      if(p1 == 0 && p2 == 0){
        return;
      }

      /*
        equations for intersecting lines:

        p1 = a + (b - a) * t1
        p2 = c + (d - c) * t2
        p1 = p2

        =>

        a + (b - a) * t1 = c + (d - c) * t2

        =>

        (b - a) * t1 - (d - c) * t2 = c - a

        //a, b, c, d are points in 3d, so we have an overdetermined system
        //furthermore, p1 != p2 in most cases
        //Use least square approximation min_x(Ax-b) which finds t1 and t2
        //which minimizes the distance between both line segments
        //(is just wat we want).
        //Solved using the normal equation x = (A'A)^{-1}A'b
        */

      Vector4<T> v1mv0   =     v[1] -   v[0];
      Vector4<T> ev1mev0 =   e.v[1] - e.v[0];

      Vector4<T> bb = e.v[0] - v[0];

      T dd[4];
      dd[0] = dot(v1mv0, v1mv0);
      dd[1] = dot(v1mv0, ev1mev0);
      dd[2] = dd[1];
      dd[3] = dot(ev1mev0, ev1mev0);

      Matrix22<T> AtA2(dd);

      T determinant = 0;

      Matrix22<T> AtAi = AtA2.inverse(&determinant);

      if(Abs(determinant) <= DET_EPS){
        if(parallel){
          *parallel = true;
        }
      }

      Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                              dot(ev1mev0, bb));

      if(p1){
        *p1 =   v[0] + (v1mv0)*solution[0];
        if(b1){
          b1->set((T)1.0 - solution[0], solution[0], 0, 0);
        }
      }
      if(p2){
        *p2 = e.v[0] - (ev1mev0)*solution[1];
        if(b2){
          solution[1] *= (T)-1;
          b2->set((T)1.0 - solution[1], solution[1], 0, 0);
        }
      }
    }

    /*Projection of two infinite edges -> projected intersection*/
    /*Result is on this (infinite) edge*/
    Vector4<T> infiniteProjection(const TriangleEdge<T>& e,
                                  bool degenerate = false)const{
      Vector4<T> p1;
      infiniteProjections(e, &p1, 0);
      return p1;
    }

    bool projection(const TriangleEdge<T>& e)const{
      return pointProjectsOnEdge(infiniteProjection(e));
    }


    Vector4<T> v[2]; /*Vertices*/
    Vector4<T> n[2]; /*Normals*/
    Vector4<T> t[2]; /*Tangent vectors*/
    Vector4<T> fv[2];/*Face vertices*/

    template<class Y>
    friend std::ostream& operator<<(std::ostream& os, const TriangleEdge<Y>& e);

    template<class Y>
    friend int intersection(const TriangleEdge<Y>& p, const TriangleEdge<Y>& x,
                            const TriangleEdge<Y>& eu, const TriangleEdge<Y>& e,
                            Vector4<Y>& res, const Y eps,
                            Vector4<Y>* b1, Vector4<Y>* b2,
                            TriangleEdge<Y>* ppp, TriangleEdge<Y>* eee,
                            Y beps, int method, const Vector4<Y>* ref, Y* s);

    template<class Y>
    friend int rigidIntersection(const TriangleEdge<Y>& p,
                                 const Vector4<Y>& comp,
                                 const Quaternion<Y>& rotp,
                                 const TriangleEdge<Y>& x,
                                 const Vector4<Y>& comx,
                                 const Quaternion<Y>& rotx,
                                 const TriangleEdge<Y>& eu,
                                 const Vector4<Y>& comeu,
                                 const Quaternion<Y>& roteu,
                                 const TriangleEdge<Y>& e,
                                 const Vector4<Y>& come,
                                 const Quaternion<Y>& rote,
                                 Vector4<Y>& res, const Y eps,
                                 Vector4<Y>* b1, Vector4<Y>* b2,
                                 TriangleEdge<Y>* ppp, TriangleEdge<Y>* eee,
                                 Y beps, int method, bool r1, bool r2,
                                 Y* s,
                                 const Vector4<Y>* reference);
  };

  template<class T>
  inline std::ostream& operator<<(std::ostream& os, const TriangleEdge<T>& e){
    os << "Edge vertices      (\n" << e.v[0]  << e.v[1]  << ")" << std::endl;
    os << "Edge normals       (\n" << e.n[0]  << e.n[1]  << ")" << std::endl;
    os << "Edge tangents      (\n" << e.t[0]  << e.t[1]  << ")" << std::endl;
    os << "Edge face vertices (\n" << e.fv[0] << e.fv[1] << ")" << std::endl;
    return os;
  }

  template<class T>
  inline TriangleEdge<T> interpolateEdges(const TriangleEdge<T>& e1,
                                          const TriangleEdge<T>& e2,
                                          T d1, T d2, T iso=(T)0.0){
    TriangleEdge<T> r;
    r.v[0] = linterp(iso, &e1.v[0], &e2.v[0], d1, d2);
    r.v[1] = linterp(iso, &e1.v[1], &e2.v[1], d1, d2);

    r.fv[0] = linterp(iso, &e1.fv[0], &e2.fv[0], d1, d2);
    r.fv[1] = linterp(iso, &e1.fv[1], &e2.fv[1], d1, d2);

    r.computeNormalAndTangent();

    return r;
  }

  /*Given two sets of edges, where set x, e is the initial
    configuration and p, eu is the current configuration, find the
    intersection point and edges for::

    x*(1-c)+p*(c) = e*(1-c)+eu*(c)

    for some value of c.

    If the initial distance (x-e) and the current distance (p-eu)
    differs in sign, there must be a root for c between 0 and 1. This
    root can be found using a root-finder. In this case we use the
    Secant method.
  */
#define EDGE_TOL 1E-7

  template<class T>
  class Interpolator<T, TriangleEdge<T>, TriangleEdge<T> >{
  public:
    Interpolator(){
      rigidEdge1 = false;
      rigidEdge2 = false;
      initialized = false;
    }

    void init(){
      if(!initialized){
        A = e11.v[0];
        B = e11.v[1];
        C = e12.v[0];
        D = e12.v[1];

        FA = e11.fv[0];
        FB = e11.fv[1];
        FC = e12.fv[0];
        FD = e12.fv[1];

        E = e21.v[0];
        F = e21.v[1];
        G = e22.v[0];
        H = e22.v[1];

        FE = e21.fv[0];
        FF = e21.fv[1];
        FG = e22.fv[0];
        FH = e22.fv[1];
        initialized = true;
      }
    }

    T evaluate(T s){
      if(rigidEdge1){
        Vector4<T> com = com11*((T)1.0-s) + com12*s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s);

        Vector4<T> v11 = com + rot1.rotate(A  - com11);
        Vector4<T> v12 = com + rot1.rotate(B  - com11);
        Vector4<T> v13 = com + rot1.rotate(FA - com11);
        Vector4<T> v14 = com + rot1.rotate(FB - com11);

        e11.set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C - A)*s;
        Vector4<T> v12 = B  + (D - B)*s;
        Vector4<T> v13 = FA + (FC-FA)*s;
        Vector4<T> v14 = FB + (FD-FB)*s;

        e11.set(v11, v12, v13, v14);
      }

      if(rigidEdge2){
        Vector4<T> com = com21*((T)1.0-s) + com22*s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s);

        Vector4<T> v21 = com + rot2.rotate(E  - com21);
        Vector4<T> v22 = com + rot2.rotate(F  - com21);
        Vector4<T> v23 = com + rot2.rotate(FE - com21);
        Vector4<T> v24 = com + rot2.rotate(FF - com21);

        e22.set(v21, v22, v23, v24);
      }else{
        Vector4<T> v21 = E  + (G - E)*s;
        Vector4<T> v22 = F  + (H - F)*s;
        Vector4<T> v23 = FE + (FG-FE)*s;
        Vector4<T> v24 = FF + (FH-FF)*s;

        e22.set(v21, v22, v23, v24);
      }

      Vector4<T> p1, p2, b1, b2;

      T d = e11.getSignedDistance(e22, &p1, &p2, &b1, &b2, &reference);
      T d2 = Abs(d);
      d -= eps;
      d2 = d2 * Sign(dot(p2-p1, reference)) - eps;
      return d2;
    }

    void getResult(T s, TriangleEdge<T>* p, TriangleEdge<T>* e){
      if(rigidEdge1){
        Vector4<T> com = com11*((T)1.0-s) + com12*s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s);

        Vector4<T> v11 = com + rot1.rotate(A  - com11);
        Vector4<T> v12 = com + rot1.rotate(B  - com11);
        Vector4<T> v13 = com + rot1.rotate(FA - com11);
        Vector4<T> v14 = com + rot1.rotate(FB - com11);

        p->set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C - A)*s;
        Vector4<T> v12 = B  + (D - B)*s;
        Vector4<T> v13 = FA + (FC-FA)*s;
        Vector4<T> v14 = FB + (FD-FB)*s;

        p->set(v11, v12, v13, v14);
      }

      if(rigidEdge2){
        Vector4<T> com = com21*((T)1.0-s) + com22*s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s);

        Vector4<T> v21 = com + rot2.rotate(E  - com21);
        Vector4<T> v22 = com + rot2.rotate(F  - com21);
        Vector4<T> v23 = com + rot2.rotate(FE - com21);
        Vector4<T> v24 = com + rot2.rotate(FF - com21);

        e->set(v21, v22, v23, v24);
      }else{
        Vector4<T> v21 = E  + (G - E)*s;
        Vector4<T> v22 = F  + (H - F)*s;
        Vector4<T> v23 = FE + (FG-FE)*s;
        Vector4<T> v24 = FF + (FH-FF)*s;

        e->set(v21, v22, v23, v24);
      }
    }

    TriangleEdge<T> e11;
    TriangleEdge<T> e12;

    TriangleEdge<T> e21;
    TriangleEdge<T> e22;

    Vector4<T> com11;
    Vector4<T> com12;
    Vector4<T> com21;
    Vector4<T> com22;

    Quaternion<T> rot11;
    Quaternion<T> rot12;
    Quaternion<T> rot21;
    Quaternion<T> rot22;

    T eps;
    bool rigidEdge1;
    bool rigidEdge2;

    Vector4<T> reference;

  protected:
    Vector4<T> A;
    Vector4<T> B;
    Vector4<T> C;
    Vector4<T> D;

    Vector4<T> FA;
    Vector4<T> FB;
    Vector4<T> FC;
    Vector4<T> FD;

    Vector4<T> E;
    Vector4<T> F;
    Vector4<T> G;
    Vector4<T> H;

    Vector4<T> FE;
    Vector4<T> FF;
    Vector4<T> FG;
    Vector4<T> FH;
    bool initialized;
  };


  /*Finds the configuration of two initial edges (e, x) and two
    current edges (eu, p) ((e->eu, x->p)) at the moment they collide
    (eee, ppp), with res the computed collision point and (bary1,
    bary2) the Barycentric coordinates of res on both edges.*/
  template<class T>
  inline int intersection(const TriangleEdge<T>& p,  const TriangleEdge<T>& x,
                          const TriangleEdge<T>& eu, const TriangleEdge<T>& e,
                          Vector4<T>& res,
                          const T eps,
                          Vector4<T>* bary1 = 0, Vector4<T>* bary2 = 0,
                          TriangleEdge<T>* ppp = 0, TriangleEdge<T>* eee = 0,
                          T deps = 0, int method = 0, const Vector4<T>* reference = 0, T* s = 0){

    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;
    TriangleEdge<T> pp;
    TriangleEdge<T> ee;

    Interpolator<T, TriangleEdge<T>, TriangleEdge<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.e21 = e;
    func.e22 = eu;
    func.eps = eps;

    func.reference = *reference;

    /*Perform a root finding on the distance function of the given
      edges*/
    T root = brentDekker<T, TriangleEdge<T>, TriangleEdge<T> >(&func, 0, 1,
                                                               (T)EDGE_TOL);
    func.getResult(root, &pp, &ee);

    pp.computeNormalAndTangent();
    ee.computeNormalAndTangent();

    Vector4<T> p1, p2, b1, b2;

    pp.infiniteProjections(ee, &p1, &p2, &b1, &b2);

    res = p1;

    if(s)     *s = root;
    if(bary1) *bary1 = b1;
    if(bary2) *bary2 = b2;
    if(eee)   *eee = ee;
    if(ppp)   *ppp = pp;

    if(ee.barycentricOnEdgeDist(b1, deps) &&
       ee.barycentricOnEdgeDist(b2, deps)){

      return 1;
    }
    return 0;
  }

  template<class T>
  inline int rigidIntersection(const TriangleEdge<T>& p,
                               const Vector4<T>&      comp,
                               const Quaternion<T>&   rotp,
                               const TriangleEdge<T>& x,
                               const Vector4<T>&      comx,
                               const Quaternion<T>&   rotx,
                               const TriangleEdge<T>& eu,
                               const Vector4<T>&      comeu,
                               const Quaternion<T>&   roteu,
                               const TriangleEdge<T>& e,
                               const Vector4<T>&      come,
                               const Quaternion<T>&   rote,
                               Vector4<T>&      res,
                               const T          eps,
                               Vector4<T>*      bary1,
                               Vector4<T>*      bary2,
                               TriangleEdge<T>* ppp,
                               TriangleEdge<T>* eee,
                               T    deps,
                               int  method,
                               bool r1,
                               bool r2,
                               T*   s,
                               const Vector4<T>* reference){
    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;
    TriangleEdge<T> pp;
    TriangleEdge<T> ee;

    Interpolator<T, TriangleEdge<T>, TriangleEdge<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.e21 = e;
    func.e22 = eu;
    func.eps = eps;
    func.rigidEdge1 = r1;
    func.rigidEdge2 = r2;
    func.com11 = comx;
    func.com12 = comp;
    func.com21 = come;
    func.com22 = comeu;

    func.rot11 = rotx;
    func.rot12 = rotp;
    func.rot21 = rote;
    func.rot22 = roteu;

    func.reference = *reference;

    /*Perform a root finding on the distance function of the given
      edges*/
    T root = brentDekker<T, TriangleEdge<T>, TriangleEdge<T> >(&func, 0, 1,
                                                               (T)EDGE_TOL);
    func.getResult(root, &pp, &ee);

    *s = root;

    pp.computeNormalAndTangent();
    ee.computeNormalAndTangent();

    Vector4<T> p1, p2, b1, b2;

    pp.infiniteProjections(ee, &p1, &p2, &b1, &b2);

    res = p1;

    if(bary1) *bary1 = b1;
    if(bary2) *bary2 = b2;
    if(eee)   *eee = ee;
    if(ppp)   *ppp = pp;

    if(ee.barycentricOnEdgeDist(b1, deps) &&
       ee.barycentricOnEdgeDist(b2, deps)){
      return 1;
    }
    return 0;
  }

  template<class T>
  class Triangle{
  public:
    Triangle();

    Triangle(const Vector4<T>& a, const Vector4<T>& b, const Vector4<T>& c);

    Triangle(const Triangle<T>& t);

    void computeNormal();

    Triangle<T>& operator=(const Triangle<T>& t);

    void set(const Vector4<T>& a, const Vector4<T>& b, const Vector4<T>& c);

    Vector4<T> v[3];
    Vector4<T> n;

    /*Returns projection of p on the triangle*/
    Vector4<T> getProjection(const Vector4<T>& p)const;

    /*Projects a vector on the plane of the triangle*/
    Vector4<T> getProjectedVector(const Vector4<T>& vec)const;

    /*Projects a vector on the normal of the plane*/
    Vector4<T> getNormalProjectedVector(const Vector4<T>& vec)const;

    T getPlaneDistance(const Vector4<T>& p)const;

    Vector4<T> getBarycentricCoordinates(const Vector4<T>& p)const;

    void getEdgeProjections(const Vector4<T>& p, Vector4<T>* ep)const;
    /*Computes the clamped projections of p on all three edges of this
      triangle. If there is no projection for a particular edge, the
      closest vertex is is returned (clamping) for that edge*/
    void getClampedEdgeProjections(const Vector4<T>& p, Vector4<T>* ep)const;


    T getArea()const;

    Vector4<T> getCoordinates(const Vector4<T>& bary)const;

    Vector4<T> clampWeights(const Vector4<T>& bary, T mn, T mx)const;

    Vector4<T> edgeClampedBarycentricCoordinates(const Vector4<T>& p)const{
      Vector4<T> bary = getBarycentricCoordinates(p);
      return bary;
      if(barycentricInTriangle(bary, (T)0.0)){
        return bary;
      }else{
        Vector4<T> ep[3];
        getClampedEdgeProjections(p, ep);

        Vector4<T> projection;

        T closestDistance = (T)10000.0;
        for(int i=0;i<3;i++){
          T dist = (ep[i] - p).length();
          if(dist < closestDistance){
            closestDistance = dist;
            projection = ep[i];
          }
        }
        return getBarycentricCoordinates(projection);
      }
    }


    /*Creates an edge from a vertex of the triangle to vertex p, if p
      is outside the triangle, then there is one edge-edge crossing
      for which this function returns the barycentric coordinates.*/
    Vector4<T> edgeCrossedBarycentricCoordinates(const Vector4<T>& p)const{
      Vector4<T> bary = getBarycentricCoordinates(p);
      //return bary;
      if(barycentricInTriangle(bary, (T)0.0)){
        return bary;
      }

      int edgeList[3][3] = {{0, 1, 2},
                            {1, 2, 0},
                            {2, 0, 1}};

      /*Check barycentric coordinates of edge*/

      Vector4<T> bestBary1, bestBary2;
      Vector4<T> p1, p2;

      int bestCandidate = -1;

      for(int i = 0;i<3;i++){
        Vector4<T> v1mv0   = v[edgeList[i][0]] - p;
        Vector4<T> ev1mev0 = v[edgeList[i][1]] - v[edgeList[i][2]];

        Vector4<T> bb = v[edgeList[i][2]] - p;

        T dd[4];
        dd[0] = dot(v1mv0, v1mv0);
        dd[1] = dot(v1mv0, ev1mev0);
        dd[2] = dd[1];
        dd[3] = dot(ev1mev0, ev1mev0);

        Matrix22<T> AtA2(dd);

        T determinant = 0;

        Matrix22<T> AtAi = AtA2.inverse(&determinant);

        if(Abs(determinant) <= DET_EPS){
          continue;
        }

        Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                                dot(ev1mev0, bb));

        solution[1] *= (T)-1;

        Vector4<T> bary1((T)1.0 - solution[0], solution[0], 0, 0);
        Vector4<T> bary2((T)1.0 - solution[1], solution[1], 0, 0);

        if(bary1[0] >= 0.0 && bary1[0] <= 1.0 &&
           bary1[1] >= 0.0 && bary1[1] <= 1.0 &&
           bary2[0] >= 0.0 && bary2[0] <= 1.0 &&
           bary2[1] >= 0.0 && bary2[1] <= 1.0 ){
          bestBary1 = bary1;
          bestBary2 = bary2;

          bestCandidate = i;

          p1 =                 p +   (v1mv0)*solution[0];
          p2 = v[edgeList[i][2]] + (ev1mev0)*solution[1];
        }
      }

      if(bestCandidate == -1){
        return edgeClampedBarycentricCoordinates(p);
      }

      return getBarycentricCoordinates(p2);
    }

    bool barycentricInTriangle(const Vector4<T>& bary, T eps)const;

    bool barycentricInTriangleBand(const Vector4<T>& bary, T eps)const{
      if(barycentricInTriangle(bary, (T)0.0)){
        /*Barycentric coordinates inside triangle, so it is also in
          the EPS band around the triangle.*/
        return true;
      }else{
        /*Barycentric outside triangle, compute edge clamped
          barycentric coordinates*/

        /*Check if there is a projection on one of the three edges*/

        Vector4<T> p = getCoordinates(bary);

        Vector4<T> ep[3];
        getClampedEdgeProjections(p, ep);

        Vector4<T> projection;

        T closestDistance = (T)10000.0;
        for(int i=0;i<3;i++){
          T dist = (ep[i] - p).length();
          if(dist < closestDistance){
            closestDistance = dist;
            projection = ep[i];
          }
        }


        if(closestDistance < eps){
          return true;
        }
        return false;
      }
    }

    bool barycentricInTriangleDist(const Vector4<T>& bary, T dist)const;

    bool pointProjectsOnTriangle(const Vector4<T>& p, T eps=0,
                                 Vector4<T>* b=0)const;

    T getSignedDistance2(const Vector4<T>& p, T* sgn)const;

    T getSignedDistance(const Vector4<T>& p,
                        Vector4<T>*       bary = 0,
                        Vector4<T>*       projection = 0,
                        T*                ddd = 0)const;

    bool intersects(const Ray<T>& ray, Vector4<T>& colPoint,
                    T* t = 0, Vector4<T>* bary = 0)const;

    /*Checks for an intersection of a ray, defined by a point p and
      vector dir. If there is an intersection, the function returns
      true and res contains the intersection point and t such that res
      = p + dir * t*/
    bool rayIntersection(const Vector4<T>& p,
                         const Vector4<T>& dir,
                         Vector4<T>& res,
                         T* t = 0,
                         Vector4<T>* bary = 0,
                         T deps = 0,
                         T* detp = 0)const;

    bool lineIntersection(const Vector4<T>& p,
                          const Vector4<T>& dir,
                          Vector4<T>& res,
                          T* t             = 0,
                          Vector4<T>* bary = 0,
                          T deps           = 0,
                          T* detp          = 0)const;

#if 0
    /*Check for intersections with a plane defined by triangle tri*/
    bool planeIntersection(const Triangle<T>& tri,
                           Vector4<T>& res1,
                           Vector4<T>& res2,
                           T* t1             = 0,
                           T* t2             = 0,
                           Vector4<T>* bary1 = 0,
                           Vector4<T>* bary2 = 0,
                           Vector4<T>* bary3 = 0,
                           Vector4<T>* bary4 = 0,
                           T beps            = 0,
                           T* detp1          = 0,
                           T* detp2          = 0)const;

    //No intersection
    //This triangle is completely intersected by the other
    //This triangle is partly intersected by the other
    //The intersection of this triangle and the other is completely embedded by
    //this triangle
    enum TriangleIntersection{None, Full, Partly, Embedded};

    TriangleIntersection triangleIntersection(const Triangle<T>& tri,
                                              Vector4<T>& res1,
                                              Vector4<T>& res2,
                                              T* t1             = 0,
                                              T* t2             = 0,
                                              Vector4<T>* bary1 = 0,
                                              Vector4<T>* bary2 = 0,
                                              Vector4<T>* bary3 = 0,
                                              Vector4<T>* bary4 = 0,
                                              T beps            = 0,
                                              T* detp1          = 0,
                                              T* detp2          = 0)const{
      if(planeIntersection(tri, res1, res2, t1, t2, bary1, bary2, bary3, bary4,
                           beps, detp1, detp2)){
        /*Some intersection has ocured, figure out what kind*/

        bool p1f1 = barycentricInTriangle(*bary1, beps);
        bool p2f1 = barycentricInTriangle(*bary2, beps);
        bool p1f2 = barycentricInTriangle(*bary3, beps);
        bool p2f2 = barycentricInTriangle(*bary4, beps);

        if(p1f1 && p2f1){
          /*Intersection is completely embedded in this triangle, only a
            small fraction is intersected.*/
          message("Embedded");
          return Embedded;
        }else if(p1f1 || p2f1){
          /*Intersection is partly inside this triangle, the
            intersection cuts through an edge.*/
          message("Partly");
          return Partly;
        }else{
          /*Intersection starts and ends outside this triangle, i.e., a
            part is completely cut off.*/
          /*Check barycentric coordinates of other triangle*/
          message("Full");
          if(p1f2 && p2f2){
            message("Full and other embedded");
          }else if(p1f2 || p2f2){
            message("Full and other partly, not possible");
            return None;
          }else{
            message("Full and other full, not valid");
            return None;
          }
          return Full;
        }
      }else{
        return None;
      }
    }
#endif

    template<class Y>
    friend std::ostream& operator<<(std::ostream& os, const Triangle<Y>& t);

    template<class Y>
    friend int intersection(const Triangle<Y>& p, const Triangle<Y>& x,
                            const Vector4<Y>& vu, const Vector4<Y>& v,
                            Vector4<Y>& res, const Y eps,
                            Vector4<Y>* bary1, Triangle<Y>* ppp, Y beps,
                            Y normalFactor, int method, Y* s, bool forced);

    template<class Y>
    friend int rigidIntersection(const Triangle<Y>& p, const Vector4<Y>& cp,
                                 const Quaternion<Y>& rp, const Triangle<Y>& t,
                                 const Vector4<Y>& ct,
                                 const Quaternion<Y>& rt,
                                 const Vector4<Y>& vu, const Vector4<Y>& cvu,
                                 const Quaternion<Y>& rvu,
                                 const Vector4<Y>& v, const Vector4<Y>& cv,
                                 const Quaternion<Y>& rv,
                                 Vector4<Y>& res, const Y eps,
                                 Vector4<Y>* bary1, Triangle<Y>* ppp,
                                 Y beps, Y normalFactor,
                                 int method, bool rigidFace, bool rigidVertex,
                                 Y* s, bool forced);
  };




  template<class T>
  class TriangleEdge2{
  public:
    TriangleEdge2(){

    };

    TriangleEdge2(const Vector4<T>& a, const Vector4<T>& b,
                  const Vector4<T>& c, const Vector4<T>& d):ta(a,b,c),
                                                            tb(b,a,d){
      v[0] = a;
      v[1] = b;
      v[2] = c;
      v[3] = d;

      //ta(v[0], v[1], v[2]);
      //tb(v[1], v[0], v[3]);
    }

    bool barycentricOnEdge(const Vector4<T>& bary, T eps)const{
      if((bary[0] >=  -eps) && (bary[1] >=  -eps) &&
         (bary[0] <= 1+eps) && (bary[1] <= 1+eps)){
        return true;
      }
      return false;
    }

#define CONCAVE_EDGE 1
#define CONVEX_EDGE  2
#define FLAT_EDGE    3

    int computeType()const{
      T sda = ta.getSignedDistance(v[3]);
      T sdb = tb.getSignedDistance(v[2]);

      T tol = (T)1e-6;

      //message("sda = %10.10e, sdb = %10.10e", sda, sdb);

      if(sda > tol && sdb > tol){
        //Concave
        return CONCAVE_EDGE;
      }else if(sda < -tol && sdb < -tol){
        return CONVEX_EDGE;
      }else{// if(sda <= tol && sdb <= tol &&
        // sda >= -tol && sdb >= -tol){
        return FLAT_EDGE;
      }

      error("Unknown type");
      return -1;
    }

    bool pointInside(const Vector4<T>& p)const{
      int type = computeType();

      if(type == FLAT_EDGE){
        /*If p below both faces, p is inside this edge*/
        if((ta.getSignedDistance(p) <= 0) &&
           (tb.getSignedDistance(p) <= 0) ){
          return true;
        }
        return false;
      }else if(type == CONVEX_EDGE){
        /*If p below both faces, p is inside this edge*/
        if((ta.getSignedDistance(p) <= 0) &&
           (tb.getSignedDistance(p) <= 0) ){
          return true;
        }
        return false;
      }else if(type == CONCAVE_EDGE){
        /*If p below one of both faces, p is inside this edge*/
        if((ta.getSignedDistance(p) <= 0) ||
           (tb.getSignedDistance(p) <= 0) ){
          return true;
        }
        return false;
      }
      error("Unknown type");
      return false;
    }

    void getProjection(const TriangleEdge2<T>& edge,
                       Vector4<T>* p1,
                       Vector4<T>* p2,
                       Vector4<T>* b1,
                       Vector4<T>* b2,
                       bool* degenerate)const{
      /*
        equations for intersecting lines:

        p1 = a + (b - a) * t1
        p2 = c + (d - c) * t2
        p1 = p2

        =>

        a + (b - a) * t1 = c + (d - c) * t2

        =>

        (b - a) * t1 - (d - c) * t2 = c - a

        //a, b, c, d are points in 3d, so we have an overdetermined system
        //furthermore, p1 != p2 in most cases
        //Use least square approximation min_x(Ax-b) which finds t1 and t2
        //which minimizes the distance between both line segments
        //(is just wat we want).
        //Solved using the normal equation x = (A'A)^{-1}A'b
        */
      Vector4<T> v1mv0   =        v[1] -      v[0];
      Vector4<T> ev1mev0 =   edge.v[1] - edge.v[0];

      Vector4<T> bb = edge.v[0] - v[0];

      T dd[4];

      dd[0] = dot(v1mv0, v1mv0);
      dd[1] = dot(v1mv0, ev1mev0);
      dd[2] = dd[1];
      dd[3] = dot(ev1mev0, ev1mev0);

      Matrix22<T> AtA2(dd);

      T determinant = 0.0;
      Matrix22<T> AtAi = AtA2.inverse(&determinant);

      if(Abs(determinant) <= DET_EPS){
        *degenerate = true;
        return;
        Vector4<T> v1 = v1mv0;
        Vector4<T> v2 = ev1mev0;
        std::cout << v1 << std::endl;
        std::cout << v2 << std::endl;
        std::cout << cross(v1, v2) << std::endl;

        v1.normalize();
        v2.normalize();

        std::cout << v1 << std::endl;
        std::cout << v2 << std::endl;

        Vector4<T> nn = cross(v1, v2);

        message("normal = ");
        std::cout << nn << std::endl;
        std::cout << nn/nn.length() << std::endl;

        Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                                dot(ev1mev0, bb));

        if(p1){
          *p1 =   v[0] + (v1mv0)*solution[0];
          if(b1){
            b1->set((T)1.0 - solution[0], solution[0], 0, 0);
          }
        }
        if(p2){
          *p2 = edge.v[0] - (ev1mev0)*solution[1];
          if(b2){
            solution[1] *= -1;
            b2->set((T)1.0 - solution[1], solution[1], 0, 0);
          }
        }

        if(IsNan(*p1) ||
           IsNan(*p2) ){
          if(degenerate){
            *degenerate = true;
          }
        }

        if(!degenerate){

          Vector4<T> n2 = *p1 - *p2;

          message("solution = %10.10e, %10.10e", solution[0], solution[1]);


          message("determinant = %10.10e", determinant);

          message("p1 p2 b1 b2");

          std::cout << *p1 << std::endl;
          std::cout << *p2 << std::endl;

          std::cout << *b1 << std::endl;
          std::cout << *b2 << std::endl;

          message("normal2 = ");
          std::cout << n2 << std::endl;
          std::cout << n2/n2.length() << std::endl;

          error("Small EPS");

          if(nn.length() < 1e-3){
            //{
            if(degenerate){
            *degenerate = true;
            }
          }
        }
      }

      Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                              dot(ev1mev0, bb));

      if(p1){
        *p1 =   v[0] + (v1mv0)*solution[0];
        if(b1){
          b1->set((T)1.0 - solution[0], solution[0], 0, 0);
        }
      }
      if(p2){
        *p2 = edge.v[0] - (ev1mev0)*solution[1];
        if(b2){
          solution[1] *= -1;
          b2->set((T)1.0 - solution[1], solution[1], 0, 0);
        }
      }

      if(IsNan(*p1) ||
         IsNan(*p2) ){
        if(degenerate){
          *degenerate = true;
        }
      }
    }

    T getSignedDistance(const TriangleEdge2<T>& edge,
                        Vector4<T>* pp1 = 0, Vector4<T>* pp2 = 0,
                        Vector4<T>* bb1 = 0, Vector4<T>* bb2 = 0,
                        bool* deg = 0)const{
      PRINT_FUNCTION;
      int s = 0;
      Vector4<T> p1, p2;
      T d = getDistance(edge, &p1, &p2, bb1, bb2, deg);
      if(pp1){
        *pp1 = p1;
      }
      if(pp2){
        *pp2 = p2;
      }
      error("called?");
      PRINT(pointInside(p2));
      PRINT(edge.pointInside(p1));

#if 1
      if(pointInside(p2) || edge.pointInside(p1)){
        s = -1;
      }else{
        s = 1;
#endif
        //return s;
        //{

#if 0
      try{
        s = getSign(edge);
      }catch(DegenerateCaseException* e){
        warning("Exception");
        if(Abs(d) < 1e-10){
          /*Distance is close to zero, in this case the computation of
            the sign makes no sense*/
          delete e;
          return 0;
        }else{
          throw e;
        }
      }
#endif
      }
      return d*(T)s;
    }

    T getDistance(const TriangleEdge2<T>& edge,
                  Vector4<T>* pp1 = 0,
                  Vector4<T>* pp2 = 0,
                  Vector4<T>* bb1 = 0,
                  Vector4<T>* bb2 = 0,
                  bool* deg = 0)const{
      Vector4<T> p1, p2;
      bool degenerate = false;

      /*Computes the projection of each edge on the other edge*/
      getProjection(edge, &p1, &p2, bb1, bb2, &degenerate);

      /*If degenerate, the edges are (nearly) parallel*/
      if(!degenerate){
        /*Try to compute normal using crossproduct*/
        Vector4<T> ee1 =      v[1] -      v[0];
        Vector4<T> ee2 = edge.v[1] - edge.v[0];
        Vector4<T> nn = cross(ee1, ee2);

        nn.normalize();

        T dist = Abs(dot(p1-p2, nn));

        if(pp1) *pp1 = p1;
        if(pp2) *pp2 = p2;

        return dist;
      }else{
        if(deg) *deg = true;

        /*Try to find a contact normal between the edges and
          approximate a distance between them.*/
        Vector4<T> v0 = edge.v[0] - v[0];
        Vector4<T> v1 = edge.v[0] - v[1];
        Vector4<T> ee = v[1] - v[0];

        //error("called??");

        T d0 = v0.length();
        T d1 = v1.length();

        Vector4<T> tangent;

        if(d0 < d1){
          if(Abs(d0) < 1e-6){
            //return 0;
          }
          tangent = cross(v1, ee);
        }else{
          if(Abs(d1) < 1e-6){
            //return 0;
          }
          tangent = cross(v0, ee);
        }

        if(pp1) *pp1 = p1;
        if(pp2) *pp2 = p2;

        Vector4<T> n = cross(tangent, ee);
        n.normalize();

        if(d0 < d1){
          return Abs(dot(n, v0));
        }else{
          return Abs(dot(n, v1));
        }
        return 0;
      }
    }

    bool faceIntersectionWithEdge(int type, const Triangle<T>& f,
                                  const TriangleEdge<T>& edge,
                                  Vector4<T>& col, Vector4<T>& bary,
                                  T& det, T& t, bool& deg)const{

      f.lineIntersection(edge.v[0], edge.v[1] - edge.v[0],
                         col, &t, &bary, 0, &det);

      bool c = false;

      if(Abs(det) > DET_EPS){
        if(type == CONVEX_EDGE){
          if(bary[2] > 0){
            c = true;
          }
        }else if(type == CONCAVE_EDGE){
          if(bary[2] < 0){
            c = true;
          }
        }
      }else{
        deg = true;
      }
      return c;
    }


    int getSign2(const TriangleEdge2<T>& edge)const{
      PRINT_FUNCTION;
      bool ca = false;
      bool cb = false;
      bool cc = false;
      bool cd = false;

      Vector4<T> res1, res2, res3, res4;
      Vector4<T> bary1, bary2, bary3, bary4;
      T det1, det2, det3, det4;
      T t1, t2, t3, t4;
      bool deg1, deg2, deg3, deg4;
      deg1 = deg2 = deg3 =deg4 = false;

      int type1 = computeType();
      int type2 = edge.computeType();

      /*Special case, which can be easily determined due to the flat
        edges*/

      PRINT(type1);
      PRINT(type2);

      if(type1 == FLAT_EDGE && type2 == FLAT_EDGE){
        return 0;
      }else if(type1 == FLAT_EDGE){
        return 0;
      }else if(type2 == FLAT_EDGE){
        return 0;
      }


      ca = faceIntersectionWithEdge(type1, ta, edge, res1, bary1,
                                    det1, t1, deg1);
      cb = faceIntersectionWithEdge(type1, tb, edge, res2, bary2,
                                    det2, t2, deg2);
      cc = faceIntersectionWithEdge(type2, edge.ta, *this, res3, bary3,
                                    det3, t3, deg3);
      cd = faceIntersectionWithEdge(type2, edge.tb, *this, res4, bary4,
                                    det4, t4, deg4);

      message("face intersections %d, %d, %d, %d", ca, cb, cc, cd);


      /*Get lookup index*/
      int index = 0;
      if(ca) index |= 8;
      if(cb) index |= 4;
      if(cc) index |= 2;
      if(cd) index |= 1;

      int bitOffset = 0;
      int bitMask = 0;
      if(type1 == CONVEX_EDGE && type2 == CONVEX_EDGE){
        bitOffset = 3;
      }else if(type1 == CONCAVE_EDGE && type2 == CONVEX_EDGE){
        bitOffset = 2;
      }else if(type1 == CONVEX_EDGE && type2 == CONCAVE_EDGE){
        bitOffset = 1;
      }else if(type1 == CONCAVE_EDGE && type2 == CONCAVE_EDGE){
        bitOffset = 0;
      }

      bitMask = 1 << bitOffset;

      message("index = %d, offset = %d, mask = %d", index, bitOffset, bitMask);


      int inside = (edgesInside[index] & bitMask) == bitMask;

      message("inside = %d", inside);

      if(inside){
        return -1;
      }
      return 1;
    }


    int getSign(const TriangleEdge2<T>& edge)const{
      return getSign2(edge);
#if 1
      int s1 = 0;
      int s2 = 0;
      bool deg1 = false;
      bool deg2 = false;

      s1 = getSignWith(edge, &deg1);
      s2 = edge.getSignWith(*this, &deg2);

      message("s1 = %d, s2 = %d", s1, s2);

#if 1
      if(!deg1){
        if(s1 == -1){
          return -1;
        }else{
          if(!deg2){
            if(s2 == -1){
              return -1;
            }
            return 1;
          }
        }
        return 1;
      }else{
        /*s2 = edge.getSignWith(*this, &deg2);

        */
        if(!deg2){
          if(s2 == -1){
            return -1;
          }
          return 1;
        }else{
          throw new DegenerateCaseException(__LINE__, __FILE__, "deg case");
        }
      }
      error();
#endif
#else
      s1 = getSignWith(edge, &deg1);
      s2 = edge.getSignWith(*this, &deg2);

      if(!deg1 || !deg2){
        if(s1 == -1 || s2 == -1){
          /*If one of both tests give a negative sign, then there is a
            collision. Which results in a negative sign.*/
          return -1;
        }
        return 1;
      }else{
        /*Double degenerate case*/
        throw new DegenerateCaseException(__LINE__, __FILE__, "deg case");
        /*Check individual distances, we can safely assume that the
          edges are parallel to the geometry, hence the signed
          distance can be used.*/
        Triangle<T> ta(v[1], v[0], v[2]);

        return (int)Sign(ta.getSignedDistance(edge.v[0]));
      }
#endif
    }



    /*TODO:: needs some optimization*/
    int getSignWith(const TriangleEdge2<T>& edge, bool* degenerate)const{
      /*Determine if the current edge, given its neighbouring faces ta
        and tb is convex or concave. */

      /*Since v[0] and v[1] is the considered edge, measure the signed
        distance between ta and v[3] and for tb with v[2]*/

      int type = computeType();

      message("type = %d", type);

      if(type == FLAT_EDGE){
        /*If projection of edge is above surface, sign is positive*/
        Vector4<T> p1, p2, b1, b2;
        bool deg = false;

        getProjection(edge, &p1, &p2, &b1, &b2, &deg);

        if(deg){
          /*Both edges are parallel, check distances of vertices*/
          T da = ta.getSignedDistance(edge.v[0]);
          T db = ta.getSignedDistance(edge.v[1]);
          T dc = tb.getSignedDistance(edge.v[0]);
          T dd = tb.getSignedDistance(edge.v[1]);

          if(da > 0 && db > 0 &&
             dc > 0 && dd > 0){
            /*Both vertices above face*/
            return 1;
          }else if(da < 0 && db < 0 &&
                   dc < 0 && dd < 0){
            /*Both vertices below face*/
            return -1;
          }else{
            if(degenerate){
              *degenerate = true;
            }
            throw new DegenerateCaseException(__LINE__, __FILE__, "edges too close for sign check");
            return 0;
            //error("Can not determine sign correctly");
          }
        }

        return (int)Sign(ta.getSignedDistance(p2));
      }

      message("Face normals, dot = %10.10e", dot(ta.n, tb.n));
      std::cout << ta.n << std::endl;
      std::cout << tb.n << std::endl;

      Vector4<T> res1;
      T t1 = 0;
      Vector4<T> bary1;
      T det1 = 0;
      bool ca = ta.lineIntersection(edge.v[0], edge.v[1] - edge.v[0],
                                    res1, &t1, &bary1, 0, &det1);


      Vector4<T> res2;
      T t2 = 0;
      Vector4<T> bary2;
      T det2 = 0;
      bool cb = tb.lineIntersection(edge.v[0], edge.v[1] - edge.v[0],
                                    res2, &t2, &bary2, 0, &det2);

      bool degenerate1 = false;
      bool degenerate2 = false;

      START_DEBUG;
      message("t1 = %10.10e, t2 = %10.10e", t1, t2);
      message("ca = %d, cb = %d => barys", ca, cb);
      std::cout << bary1 << std::endl;
      std::cout << bary2 << std::endl;
      PRINT(det1);
      PRINT(det2);
      END_DEBUG;

      ca = false;
      if(Abs(det1) > DET_EPS){
        if(type == CONVEX_EDGE){
          if(bary1[2] > 0){
            ca = true;
          }
        }else if(type == CONCAVE_EDGE){
          if(bary1[2] < 0){
            ca = true;
          }
        }
      }else{
        degenerate1 = true;
      }

      if(bary1[2] == (T)0.0){
        /*Edge-edge instersection*/
        //degenerate1 = true;
      }

      cb = false;
      if(Abs(det2) > DET_EPS){
        if(type == CONVEX_EDGE){
          if(bary2[2] > 0){
            cb = true;
          }
        }else if(type == CONCAVE_EDGE){
          if(bary2[2] < 0){
            cb = true;
          }
        }
      }else{
        degenerate2 = true;
      }

      if(bary2[2] == (T)0.0){
        /*Edge edge intersection*/
        //degenerate2 = true;
      }

      START_DEBUG;
      message("ca = %d, cb = %d", ca, cb);
      message("deg1 = %d, deg2 = %d", degenerate1, degenerate2);
      END_DEBUG;

      /*ca or cb are true if edge2 intersects the corresponding
        adjacent faces of this edge*/

      if(!degenerate1 && degenerate2){
        /*Edge parallel to face b and not parallel to face a. Intersection
          with face a determines collision.*/
        return ca?-1:1;
      }else if(degenerate1 && !degenerate2){
        /*Edge parallel to face a and not parallel to face b. Intersection
          with face b determines collision.*/
        return cb?-1:1;
      }else if(degenerate1 && degenerate2){
        /*Edge is parallel to both faces, figure out if the edge is
          outside or inside*/

        /*This is only possible if this edge is a flat edge, which is
          handled earlier. Therefore this is an exceptional case*/
        throw new DegenerateCaseException(__LINE__, __FILE__, "edge parallel to both adjacent faces");
      }else if(!degenerate1 && !degenerate2){
        /*Edge intersects with both faces. Sign is negative if edge
          intersects both adjacent faces (depending on edge type)*/
        return (ca && cb)?-1:1;
      }

      error("c");
      *degenerate = true;
      return 0;
    }

    Vector4<T> v[4];

    Triangle<T> ta;
    Triangle<T> tb;

    //Edge -> v1, v2
    //Face a -> v1, v2, v3
    //Face b -> v2, v1, v4

  };

  template<class T>
  inline TriangleEdge<T>::TriangleEdge(const TriangleEdge2<T>& e){
    v[0]  = e.v[0];
    v[1]  = e.v[1];
    n[0]  = e.ta.n;
    n[1]  = e.tb.n;
    fv[0] = e.v[2];
    fv[1] = e.v[3];

    t[0] = fv[0] - v[0];
    t[1] = fv[1] - v[0];

    Vector4<T> ee = v[1] - v[0];

    t[0] = cross(ee, t[0]);
    t[0] = cross(t[0], ee);

    t[1] = cross(ee, t[1]);
    t[1] = cross(t[1], ee);

    t[0].normalize();
    t[1].normalize();
  }

#if 1
  template<class T>
  inline T TriangleEdge<T>::getSignedDistance(const TriangleEdge<T>& e,
                                              Vector4<T>* p1,
                                              Vector4<T>* p2,
                                              Vector4<T>* b1,
                                              Vector4<T>* b2,
                                              const Vector4<T>* reference)const{
    TriangleEdge2<T> edge1(  v[0],   v[1],   fv[0],   fv[1]);
    TriangleEdge2<T> edge2(e.v[0], e.v[1], e.fv[0], e.fv[1]);

    //TriangleEdge3<T> edge31(  v[0],   v[1],   fv[0],   fv[1]);
    //TriangleEdge3<T> edge32(e.v[0], e.v[1], e.fv[0], e.fv[1]);

    //Vector4<T> edge3Normal;
    //bool edge3Deg;

    //T edge3Dist = edge31.getSignedDistance(edge32, edge3Normal, &edge3Deg);

    //message("edge3Dist = %10.10e, degenerate = %d", edge3Dist, edge3Deg);

    if(reference == 0){
      error("reference vector not provided");
    }

    bool degenerate = false;
    Vector4<T> pp1, pp2;
    PRINT_FUNCTION;
    T d = edge1.getDistance(edge2, &pp1, &pp2, b1, b2, &degenerate);

    if(p1) *p1 = pp1;
    if(p2) *p2 = pp2;

    if(degenerate){
      throw new DegenerateCaseException(__LINE__, __FILE__, "parallel edges");
    }

    Vector4<T> normal = (pp2 - pp1);

    T sd = d * Sign(dot(normal, *reference));

    //message("sd = %10.10e", sd);

    return sd;
  }

  template<class T>
  inline Vector4<T>
  TriangleEdge<T>::getOutwardNormal(const TriangleEdge<T>& e,
                                    const Vector4<T>* reference,
                                    Vector4<T>* pp1,
                                    Vector4<T>* pp2,
                                    Vector4<T>* bb1,
                                    Vector4<T>* bb2)const{
    Vector4<T> p1, p2;

    TriangleEdge2<T> edge1(  v[0],   v[1],   fv[0],   fv[1]);
    TriangleEdge2<T> edge2(e.v[0], e.v[1], e.fv[0], e.fv[1]);

    bool degenerate = false;

    T d = edge1.getDistance(edge2, &p1, &p2, bb1, bb2, &degenerate);

    if(degenerate){
      Vector4<T> ea =   v[1] -   v[0];
      Vector4<T> eb = e.v[1] - e.v[0];
      message("ea.length = %10.10e", ea.length());
      message("eb.length = %10.10e", ea.length());
      std::cout << cross(ea, eb) << std::endl;
      ea.normalize();
      eb.normalize();
      message("ea.length = %10.10e", ea.length());
      message("eb.length = %10.10e", ea.length());
      std::cout << cross(ea, eb) << std::endl;
      throw new DegenerateCaseException(__LINE__, __FILE__, "parallel edges");
    }

    if(pp1) *pp1 = p1;
    if(pp2) *pp2 = p2;

    Vector4<T> normal = p2 - p1;

    if(normal.length() < 1e-10){
      /*Edges are intersecting almost, compute normal using crossproduct*/
      Vector4<T> ea =   v[1] -   v[0];
      Vector4<T> eb = e.v[1] - e.v[0];

      warning("Close to zero contact normal");
      normal = cross(ea, eb).normalize();

      /*Since getSignedDistance did not throw an exception, the edges
        are not parallel. So the computed normal does have a length.*/

      message("reference = %p", reference);

      if(reference){
        if(dot(normal, *reference) < (T)0.0){
          normal *= -1;
        }

        return normal;
      }else{
        throw new DegenerateCaseException(__LINE__, __FILE__, "no distance to determine sign and no reference for correction provided");
      }
    }

    normal.normalize();

    if(d * Sign(dot(normal, *reference)) < 0){
      normal *= -1;
    }

    return normal;
  }

  template<class T>
  T TriangleEdge<T>::getSignedDistanceAndOutwardNormal(const TriangleEdge<T>& e,
                                                       Vector4<T>* normal,
                                                       Vector4<T>* pp1,
                                                       Vector4<T>* pp2,
                                                       Vector4<T>* bb1,
                                                       Vector4<T>* bb2,
                                                       const Vector4<T>* reference)const{
    Vector4<T> p1, p2;
    error("called?");
    TriangleEdge2<T> edge1(v[0], v[1], fv[0], fv[1]);
    TriangleEdge2<T> edge2(e.v[0], e.v[1], e.fv[0], e.fv[1]);

    bool degenerate = false;

    T sd = edge1.getSignedDistance(edge2, &p1, &p2, bb1, bb2, &degenerate);

    if(degenerate){
      throw new DegenerateCaseException(__LINE__, __FILE__, "parallel edges");
    }

    if(pp1) *pp1 = p1;
    if(pp2) *pp2 = p2;

    *normal = p2 - p1;

    if(normal->length() < 1e-10){
      /*Edges are intersecting almost, compute normal using crossproduct*/
      Vector4<T> ea =   v[1] -   v[0];
      Vector4<T> eb = e.v[1] - e.v[0];

      *normal = cross(ea, eb).normalize();

      /*Since getSignedDistance did not throw an exception, the edges
        are not parallel. So the computed normal does have a length.*/

      if(reference){
        if(dot(*normal, *reference) < (T)0.0){
          *normal *= -1;
        }

        return sd;
      }else{
        throw new DegenerateCaseException(__LINE__, __FILE__, "no distance to determine sign and no reference for correction provided");
        return sd;
      }
    }

    normal->normalize();

    if(sd < 0){
      *normal *= -1;
    }

    return sd;
  }

#endif

  template<class T>
  class Interpolator<T, Matrix44<T>, Matrix44<T> >{
  public:
    Interpolator(){
      initialized = false;
    }

    void init(){
      if(!initialized){
        for(int i=0;i<4;i++){
          A[i][3] = (T)1.0;
          B[i][3] = (T)1.0;
        }

        initialized = true;
      }
    }

    T evaluate(T s){
      Matrix44<T> C = A*((T)1.0 - s) + B * s;
      return C.det()/(T)6.0 - eps;
    }

    void getResult(T s, Matrix44<T>* p, Matrix44<T>* e){
      *p = A*((T)1.0 - s) + B * s;
      *e = *p;
    }

    T eps;
    Matrix44<T> A;
    Matrix44<T> B;
    bool initialized;
  };


  template<class T>
  inline int intersection(const Matrix44<T>& tu, const Matrix44<T>& t,
                          Matrix44<T>& col, T eps){
    Interpolator<T, Matrix44<T>, Matrix44<T> > func;
    func.A = t;
    func.B = tu;
    func.eps = eps;

    T root = brentDekker<T, Matrix44<T>, Matrix44<T> >(&func, 0.0, 1.0,
                                                       (T)0.0);
    func.getResult(root, &col, &col);

    return 1;
  }

#if 1
  template<class T>
  class TriangleEdge3{
  public:
    TriangleEdge3(){
    }

    TriangleEdge3(const Vector4<T>& v1, const Vector4<T>& v2,
                  const Vector4<T>& fv1, const Vector4<T>& fv2){
      v[0] = v1;
      v[1] = v2;
      fv[0] = fv1;
      fv[1] = fv2;

      computeNormalAndTangent();
    }

    TriangleEdge3(const TriangleEdge3<T>& edge){
      v[0] = edge.v[0];
      v[1] = edge.v[1];
      fv[0] = edge.fv[0];
      fv[1] = edge.fv[1];

      computeNormalAndTangent();
    }

    TriangleEdge3<T>& operator=(const TriangleEdge3<T>& edge){
      if(&edge != this){
        v[0] = edge.v[0];
        v[1] = edge.v[1];
        fv[0] = edge.fv[0];
        fv[1] = edge.fv[1];

        computeNormalAndTangent();
      }
      return *this;
    }

    void set(const Vector4<T>& v1, const Vector4<T>& v2,
             const Vector4<T>& fv1, const Vector4<T>& fv2){
      v[0] = v1;
      v[1] = v2;
      fv[0] = fv1;
      fv[1] = fv2;

      computeNormalAndTangent();
    }

    bool barycentricOnEdge(const Vector4<T>& bary, T eps)const{
      if((bary[0] >= -eps) && (bary[1] >= -eps) &&
         (bary[0] <= 1+eps) && (bary[1] <= 1+eps)){
        return true;
      }
      return false;
    }

#if 0
    bool barycentricOnEdgeDist(const Vector4<T>& bary, T dist)const{
      if(barycentricOnEdge(bary, (T)0.0)){
        /*On edge*/
        return true;
      }else{
        Vector4<T> p = getCoordinates(bary);

        T d1 = (p - v[0]).length();
        T d2 = (p - v[1]).length();

        T d = Min(d1, d2);

        if(d < dist){
          return true;
        }
      }
      return false;
    }
#endif

    Vector4<T> getBarycentricCoordinates(const Vector4<T>& p)const{
      Vector4<T> v0 = v[1] - v[0];
      Vector4<T> v1 = p    - v[0];
      return getBarycentricCoordinatesEdge(v0, v1);
    }

    Vector4<T> getCoordinates(const Vector4<T>& bary)const{
      return v[0] * bary[0] + v[1] * bary[1];
    }

    T getPlaneDistance(const Vector4<T>& p, const Vector4<T>& n)const{
      Vector4<T> A, B;
      if((p-v[0]).length() < (p-v[1]).length()){
        A = v[0];
        B = v[1];
      }else{
        A = v[1];
        B = v[0];
      }

      /*Least-Square approximation for p = A + (B-A)d*/

      Vector4<T> BA = B - A;
      Vector4<T> PA = p - A;

      double BABA  = (double)dot(BA, BA);
      double BAPA  = (double)dot(BA, PA);

      double d = BAPA / BABA;

      Vector4<T> C = A + BA*(T)d;

      Vector4<T> dir = p - C;

      return dot(dir, n);
    }

    void computeNormalAndTangent(){
      Vector4<T> v1v0 = v[1] - v[0];
      Vector4<T> v2v0 = fv[0] - v[0];

      n[0] = cross(v1v0, v2v0);

      v1v0 = v[0] - v[1];
      v2v0 = fv[1] - v[1];

      n[1] = cross(v1v0, v2v0);

      n[0].normalize();
      n[1].normalize();

#if 1
      t[0] = fv[0] - v[0];
      t[1] = fv[1] - v[0];

      t[0] = fv[0] - v[0];
      t[1] = fv[1] - v[0];

      Vector4<T> ee = v[1] - v[0];

      if(ee.length() < 1e-9){
        std::cout << this << std::endl;
        error("Too small edge!!");
      }

      t[0] = cross(ee, t[0]);
      t[0] = cross(t[0], ee);

      t[1] = cross(ee, t[1]);
      t[1] = cross(t[1], ee);

      t[0].normalize();
      t[1].normalize();
#else
      Vector4<T> ee = v[1] - v[0];
      ee.normalize();
      Vector4<T> t1 = fv[0] - v[0];
      Vector4<T> t2 = fv[1] - v[0];

      T dt1 = dot(ee, t1);
      T dt2 = dot(ee, t2);

      t[0] = t1 - dt1 * ee;
      t[1] = t2 - dt2 * ee;
      message("t1.length = %10.10e", t[0].length());
      message("t2.length = %10.10e", t[1].length());
      t[0].normalize();
      t[1].normalize();
#endif

      T dtn1 = dot(t[0], n[1]);
      T dtn2 = dot(t[1], n[0]);

      /*If an edge is convex, then t[0].n[1] < 0 && t[1].n[0] < 0*/

      message("dtn1 = %10.10e", dtn1);
      message("dtn2 = %10.10e", dtn2);

      if(dtn1 <= (T)0.0 && dtn2 <= (T)0.0){
        convex = true;
      }else{
        if(Sign(dtn1) != Sign(dtn2)){
          if(Abs(dtn1 + dtn2) > 1e-10){
            std::cout << *this << std::endl;
            error("Indeterminate flat");
          }
        }
        convex = true;
      }
      convex = false;
    }

    T getSign(const Vector4<T>& p)const{
      Vector4<T> e = p - v[0];
      message("getSign, convex = %d", convex);
      std::cout << p << std::endl;
      std::cout << v[0] << std::endl;
      std::cout << e << std::endl;

      message("dot1 = %10.10e", dot(e, n[0]));
      message("dot2 = %10.10e", dot(e, n[1]));

      if(convex){
        if(dot(e, n[0]) < 0.0 && dot(e, n[1]) < 0.0){
          return -(T)1.0;
        }else{
          return (T)1.0;
        }
      }else{
        if(dot(e, n[0]) > 0.0 && dot(e, n[1]) > 0.0){
          return (T)1.0;
        }else{
          return -(T)1.0;
        }
      }
    }

    T getSignedDistance(const TriangleEdge3<T>& edge, Vector4<T>& normal,
                        bool* degenerate)const{
      /*Compute intersections of the given edge with the adjacent
        faces of this edge. Each edge intersects 2 faces. Per edge,
        the point in between the two intersection points is
        computed. Next the sign of both these half points are
        computed. If two negative signs are obtained, the signed
        distance is negative.*/
      *degenerate = false;
      if(&edge == this){
        error("Getting signed distance of edge wrt itself!!");
      }
      message("Get signed distance\n\n\n\n");
      T signA = getIntersectedEdgeSign(edge);
      T signB = edge.getIntersectedEdgeSign(*this);

      message("signA = %10.10e", signA);
      message("signB = %10.10e", signB);

      T sign = (T)1.0;

      if(signA < (T)0.0 || signB < (T)0.0){
        sign = -(T)1.0;
      }

      /*Compute distance, projections and normal.*/
      bool degenerateNormal = false;
      T dist = 0.0;
      normal = getEdgeEdgeOutwardNormalAndDistance(edge, sign,
                                                   &degenerateNormal, &dist);
      std::cout << normal << std::endl;
      message("distance = %10.10e", dist);

      message("degenerate = %d", degenerateNormal);

      if(degenerateNormal){
        *degenerate = true;
      }
      return dist;
    }

    bool isConvex()const{
      return convex;
    }

    bool isConcave()const{
      return !convex;
    }

    /*Computes the two projections (p1, p2) of two edges (this, e) on
      each other, with b1, b2 the computed Barycentric coordinates of
      p1, p2*/
    void infiniteProjections(const TriangleEdge3<T>& e,
                             Vector4<T>* p1,   Vector4<T>* p2,
                             Vector4<T>* b1=0, Vector4<T>* b2 = 0,
                             bool* parallel = 0)const{
      if(p1 == 0 && p2 == 0){
        return;
      }

      /*
        equations for intersecting lines:

        p1 = a + (b - a) * t1
        p2 = c + (d - c) * t2
        p1 = p2

        =>

        a + (b - a) * t1 = c + (d - c) * t2

        =>

        (b - a) * t1 - (d - c) * t2 = c - a

        //a, b, c, d are points in 3d, so we have an overdetermined system
        //furthermore, p1 != p2 in most cases
        //Use least square approximation min_x(Ax-b) which finds t1 and t2
        //which minimizes the distance between both line segments
        //(is just wat we want).
        //Solved using the normal equation x = (A'A)^{-1}A'b
        */

      Vector4<T> v1mv0   =     v[1] -   v[0];
      Vector4<T> ev1mev0 =   e.v[1] - e.v[0];

      Vector4<T> bb = e.v[0] - v[0];

      T dd[4];
      dd[0] = dot(v1mv0, v1mv0);
      dd[1] = dot(v1mv0, ev1mev0);
      dd[2] = dd[1];
      dd[3] = dot(ev1mev0, ev1mev0);

      Matrix22<T> AtA2(dd);

      T determinant = 0;

      Matrix22<T> AtAi = AtA2.inverse(&determinant);

      if(Abs(determinant) <= DET_EPS){
        if(parallel){
          *parallel = true;
        }
      }

      Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                              dot(ev1mev0, bb));

      if(p1){
        *p1 =   v[0] + (v1mv0)*solution[0];
        if(b1){
          b1->set((T)1.0 - solution[0], solution[0], 0, 0);
        }
      }
      if(p2){
        *p2 = e.v[0] - (ev1mev0)*solution[1];
        if(b2){
          solution[1] *= (T)-1;
          b2->set((T)1.0 - solution[1], solution[1], 0, 0);
        }
      }
    }

    template<class Y>
    friend int intersection(const TriangleEdge3<Y>& p,
                            const TriangleEdge3<Y>& x,
                            const TriangleEdge3<Y>& eu,
                            const TriangleEdge3<Y>& e,
                            Vector4<Y>& res,
                            const Y eps,
                            Vector4<Y>* bary1,
                            Vector4<Y>* bary2,
                            TriangleEdge3<Y>* ppp,
                            TriangleEdge3<Y>* eee,
                            Y deps, int method,
                            Y* s);

    template<class Y>
    friend int rigidIntersection(const TriangleEdge3<Y>& p,
                                 const Vector4<Y>&       comp,
                                 const Quaternion<Y>&    rotp,
                                 const TriangleEdge3<Y>& x,
                                 const Vector4<Y>&       comx,
                                 const Quaternion<Y>&    rotx,
                                 const TriangleEdge3<Y>& eu,
                                 const Vector4<Y>&       comeu,
                                 const Quaternion<Y>&    roteu,
                                 const TriangleEdge3<Y>& e,
                                 const Vector4<Y>&       come,
                                 const Quaternion<Y>&    rote,
                                 Vector4<Y>&       res,
                                 const Y           eps,
                                 Vector4<Y>*       bary1,
                                 Vector4<Y>*       bary2,
                                 TriangleEdge3<Y>* ppp,
                                 TriangleEdge3<Y>* eee,
                                 Y    deps,
                                 int  method,
                                 bool r1,
                                 bool r2,
                                 Y*   s);



    template<class Y>
    friend std::ostream& operator<<(std::ostream& os, const TriangleEdge3<Y>& e);

    Vector4<T> v[2];
    Vector4<T> fv[2];
    Vector4<T> n[2];
    Vector4<T> t[2];

    bool convex;

  protected:
    Vector4<T> getEdgeEdgeOutwardNormalAndDistance(const TriangleEdge3<T>& edge,
                                                   T sign, bool* degenerate,
                                                   T* dist)const{
      Vector4<T> e1 = v[1] - v[0];
      Vector4<T> e2 = edge.v[1] - edge.v[0];

      Vector4<T> normal = cross(e1, e2);

      message("edge edge normal");
      std::cout << normal << std::endl;


      Vector4<T> p1, p2, b1, b2;

      bool degenerate2 = false;

      /*p1 and b1 correspond to this edge, p2 and b2 to the provided
        edge*/
      getProjection(edge, &p1, &p2, &b1, &b2, &degenerate2);

      if(degenerate2){
        message("degenerate projections");

        /*Compute distance alternatively*/

        Vector4<T> n = (edge.v[0] - v[0]) * sign;

        *dist = n.length() * sign;

        normal = n;

        if(n.length() > 1e-9){
          degenerate2 = false;
        }

      }else{
        message("p1, p2, b1, b2");
        std::cout << p1 << std::endl;
        std::cout << p2 << std::endl;
        std::cout << b1 << std::endl;
        std::cout << b2 << std::endl;

        Vector4<T> n = (p2 - p1) * sign;

        message("n = ");

        std::cout << n << std::endl;
        std::cout << normal << std::endl;

        *dist = n.length() * sign;

        if(n.length() < 1e-9){
          degenerate2 = true;
        }

        if(dot(n, normal) < (T)0.0){
          normal *= -(T)1.0;
        }
      }

      if(normal.length() < 1e-9 || degenerate2){
        *degenerate = true;
      }

      return normal.normalize();
    }

    T getIntersectedEdgeSign(const TriangleEdge3<T>& edge)const{
      bool degA = false;
      bool degB = false;

      Vector4<T> va, vb, vc;

      computeFaceIntersection(edge, 0, va, &degA);
      computeFaceIntersection(edge, 1, vb, &degB);

      if(!degA && !degB){
        /*Compute intermediate point between two intersectionpoints*/
        vc = (va + vb)*(T)0.5;
        return getSign(vc);
      }else if(degA && !degB){
        return getSign(vb);
      }else if(degB && !degA){
        return getSign(va);
      }else{
        /*Edge parallel to both faces. In theory, any point will work
          here*/
        message("colinear edge and face");
        message("edge = ");
        std::cout << edge.v[0] << std::endl;
        std::cout << edge.v[1] << std::endl;
        return getSign((edge.v[0] + edge.v[1])*(T)0.5);
      }
    }

    void computeFaceIntersection(const TriangleEdge3<T>& edge, int faceId,
                                 Vector4<T>& intersection,
                                 bool* degenerate)const{
      Vector4<T> dir = edge.v[1] - edge.v[0];
      //Vector4<T> dir(0,0,1,0);
      //Vector4<T> s(-10,-10,-10,0);
      Vector4<T> s = edge.v[0];

      Matrix44<T> faceVertices;
      if(faceId == 0){
        faceVertices[0] = v[0];
        faceVertices[1] = v[1];
        faceVertices[2] = fv[0];
      }else{
        faceVertices[0] = v[1];
        faceVertices[1] = v[0];
        faceVertices[2] = fv[1];
      }

      Matrix33<T> A;
      A.m[0] = faceVertices[1][0] - faceVertices[0][0];
      A.m[1] = faceVertices[2][0] - faceVertices[0][0];
      A.m[2] = -dir[0];

      A.m[3] = faceVertices[1][1] - faceVertices[0][1];
      A.m[4] = faceVertices[2][1] - faceVertices[0][1];
      A.m[5] = -dir[1];

      A.m[6] = faceVertices[1][2] - faceVertices[0][2];
      A.m[7] = faceVertices[2][2] - faceVertices[0][2];
      A.m[8] = -dir[2];

      Vector3<T> b(s[0]- faceVertices[0][0],
                   s[1]- faceVertices[0][1],
                   s[2]- faceVertices[0][2]);

      T det = 0;
      Vector3<T> x = A.inverse(&det) * b;
      Vector4<T> bb((T)1.0 - x.m[0] - x.m[1], x.m[0], x.m[1], 0);

      message("det = %10.10e", det);

      message("bary");
      std::cout << x << std::endl;

      message("intersection edge");
      std::cout << s + x.m[2] * dir;

      message("intersection face");
      std::cout << faceVertices[0] + x.m[0] * (faceVertices[1] - faceVertices[0]) + x.m[1] * (faceVertices[2] - faceVertices[0]) << std::endl;

      if(Abs(det) <= DET_EPS){
        *degenerate = true;
      }else{

      }

#if 0
      //if(detp) *detp = det;

      //if(t)   *t = -(T)1000;

      if(Abs(det) <= DET_EPS){
        /*Point does not move-> singular system, i.e., dir = 0*/

        Vector4<T> bb;
        T sd = getSignedDistance(p, &bb);

        if(Abs(sd) < 1E-6){
          /*Point does not move, but lies on the plane, check
            barycentric coordinates.*/
          if(bary) *bary = bb;

          res = p;
          return barycentricInTriangle(bb, deps);
        }

        res = p;

        if(t) *t = 0;

        res *= 0;
        if(bary) *bary = res;

        return false;
      }

      if(t)    *t = x.m[2];
      if(bary) *bary = bb;

      res = p + x.m[2] * dir;


#endif
    }

    void getProjection(const TriangleEdge3<T>& edge,
                       Vector4<T>* p1,
                       Vector4<T>* p2,
                       Vector4<T>* b1,
                       Vector4<T>* b2,
                       bool* degenerate)const{
      /*
        equations for intersecting lines:

        p1 = a + (b - a) * t1
        p2 = c + (d - c) * t2
        p1 = p2

        =>

        a + (b - a) * t1 = c + (d - c) * t2

        =>

        (b - a) * t1 - (d - c) * t2 = c - a

        //a, b, c, d are points in 3d, so we have an overdetermined system
        //furthermore, p1 != p2 in most cases
        //Use least square approximation min_x(Ax-b) which finds t1 and t2
        //which minimizes the distance between both line segments
        //(is just wat we want).
        //Solved using the normal equation x = (A'A)^{-1}A'b
        */
      Vector4<T> v1mv0   =        v[1] -      v[0];
      Vector4<T> ev1mev0 =   edge.v[1] - edge.v[0];

      Vector4<T> bb = edge.v[0] - v[0];

      T dd[4];

      dd[0] = dot(v1mv0, v1mv0);
      dd[1] = dot(v1mv0, ev1mev0);
      dd[2] = dd[1];
      dd[3] = dot(ev1mev0, ev1mev0);

      Matrix22<T> AtA2(dd);

      T determinant = 0.0;
      Matrix22<T> AtAi = AtA2.inverse(&determinant);

      if(Abs(determinant) <= DET_EPS){
        *degenerate = true;
        return;
        Vector4<T> v1 = v1mv0;
        Vector4<T> v2 = ev1mev0;
        std::cout << v1 << std::endl;
        std::cout << v2 << std::endl;
        std::cout << cross(v1, v2) << std::endl;

        v1.normalize();
        v2.normalize();

        std::cout << v1 << std::endl;
        std::cout << v2 << std::endl;

        Vector4<T> nn = cross(v1, v2);

        message("normal = ");
        std::cout << nn << std::endl;
        std::cout << nn/nn.length() << std::endl;

        Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                                dot(ev1mev0, bb));

        if(p1){
          *p1 =   v[0] + (v1mv0)*solution[0];
          if(b1){
            b1->set((T)1.0 - solution[0], solution[0], 0, 0);
          }
        }
        if(p2){
          *p2 = edge.v[0] - (ev1mev0)*solution[1];
          if(b2){
            solution[1] *= -1;
            b2->set((T)1.0 - solution[1], solution[1], 0, 0);
          }
        }

        if(IsNan(*p1) ||
           IsNan(*p2) ){
          if(degenerate){
            *degenerate = true;
          }
        }

        if(!degenerate){

          Vector4<T> n2 = *p1 - *p2;

          message("solution = %10.10e, %10.10e", solution[0], solution[1]);


          message("determinant = %10.10e", determinant);

          message("p1 p2 b1 b2");

          std::cout << *p1 << std::endl;
          std::cout << *p2 << std::endl;

          std::cout << *b1 << std::endl;
          std::cout << *b2 << std::endl;

          message("normal2 = ");
          std::cout << n2 << std::endl;
          std::cout << n2/n2.length() << std::endl;

          error("Small EPS");

          if(nn.length() < 1e-3){
            //{
            if(degenerate){
            *degenerate = true;
            }
          }
        }
      }

      Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                              dot(ev1mev0, bb));

      if(p1){
        *p1 =   v[0] + (v1mv0)*solution[0];
        if(b1){
          b1->set((T)1.0 - solution[0], solution[0], 0, 0);
        }
      }
      if(p2){
        *p2 = edge.v[0] - (ev1mev0)*solution[1];
        if(b2){
          solution[1] *= -1;
          b2->set((T)1.0 - solution[1], solution[1], 0, 0);
        }
      }

      if(IsNan(*p1) ||
         IsNan(*p2) ){
        if(degenerate){
          *degenerate = true;
        }
      }
    }

  };

  template<class T>
  inline std::ostream& operator<<(std::ostream& os, const TriangleEdge3<T>& e){
    if(e.convex){
      os << "Convex edge" << std::endl;
    }else{
      os << "Concave edge" << std::endl;
    }
    os << "Edge vertices      (\n" << e.v[0]  << e.v[1]  << ")" << std::endl;
    os << "Edge normals       (\n" << e.n[0]  << e.n[1]  << ")" << std::endl;
    os << "Edge tangents      (\n" << e.t[0]  << e.t[1]  << ")" << std::endl;
    os << "Edge face vertices (\n" << e.fv[0] << e.fv[1] << ")" << std::endl;

    return os;
  }

  template<class T>
  class Interpolator<T, TriangleEdge3<T>, TriangleEdge3<T> >{
  public:
    Interpolator(){
      rigidEdge1 = false;
      rigidEdge2 = false;
      initialized = false;
    }

    void init(){
      if(!initialized){
        A = e11.v[0];
        B = e11.v[1];
        C = e12.v[0];
        D = e12.v[1];

        FA = e11.fv[0];
        FB = e11.fv[1];
        FC = e12.fv[0];
        FD = e12.fv[1];

        E = e21.v[0];
        F = e21.v[1];
        G = e22.v[0];
        H = e22.v[1];

        FE = e21.fv[0];
        FF = e21.fv[1];
        FG = e22.fv[0];
        FH = e22.fv[1];

        initialized = true;
      }
    }

    T evaluate(T s){
      if(rigidEdge1){
        Vector4<T> com = com11*((T)1.0-s) + com12*s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s);

        Vector4<T> v11 = com + rot1.rotate(A  - com11);
        Vector4<T> v12 = com + rot1.rotate(B  - com11);
        Vector4<T> v13 = com + rot1.rotate(FA - com11);
        Vector4<T> v14 = com + rot1.rotate(FB - com11);

        e11.set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C - A)*s;
        Vector4<T> v12 = B  + (D - B)*s;
        Vector4<T> v13 = FA + (FC-FA)*s;
        Vector4<T> v14 = FB + (FD-FB)*s;

        e11.set(v11, v12, v13, v14);
      }

      if(rigidEdge2){
        Vector4<T> com = com21*((T)1.0-s) + com22*s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s);

        Vector4<T> v21 = com + rot2.rotate(E  - com21);
        Vector4<T> v22 = com + rot2.rotate(F  - com21);
        Vector4<T> v23 = com + rot2.rotate(FE - com21);
        Vector4<T> v24 = com + rot2.rotate(FF - com21);

        e22.set(v21, v22, v23, v24);
      }else{
        Vector4<T> v21 = E  + (G - E)*s;
        Vector4<T> v22 = F  + (H - F)*s;
        Vector4<T> v23 = FE + (FG-FE)*s;
        Vector4<T> v24 = FF + (FH-FF)*s;

        e22.set(v21, v22, v23, v24);
      }

      Vector4<T> normal;
      bool degenerate = false;
      T d = e11.getSignedDistance(e22, normal, &degenerate);

      return d - eps;
    }

    void getResult(T s, TriangleEdge3<T>* p, TriangleEdge3<T>* e){
      if(rigidEdge1){
        Vector4<T> com = com11*((T)1.0-s) + com12*s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s);

        Vector4<T> v11 = com + rot1.rotate(A  - com11);
        Vector4<T> v12 = com + rot1.rotate(B  - com11);
        Vector4<T> v13 = com + rot1.rotate(FA - com11);
        Vector4<T> v14 = com + rot1.rotate(FB - com11);

        p->set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C - A)*s;
        Vector4<T> v12 = B  + (D - B)*s;
        Vector4<T> v13 = FA + (FC-FA)*s;
        Vector4<T> v14 = FB + (FD-FB)*s;

        p->set(v11, v12, v13, v14);
      }

      if(rigidEdge2){
        Vector4<T> com = com21*((T)1.0-s) + com22*s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s);

        Vector4<T> v21 = com + rot2.rotate(E  - com21);
        Vector4<T> v22 = com + rot2.rotate(F  - com21);
        Vector4<T> v23 = com + rot2.rotate(FE - com21);
        Vector4<T> v24 = com + rot2.rotate(FF - com21);

        e->set(v21, v22, v23, v24);
      }else{
        Vector4<T> v21 = E  + (G - E)*s;
        Vector4<T> v22 = F  + (H - F)*s;
        Vector4<T> v23 = FE + (FG-FE)*s;
        Vector4<T> v24 = FF + (FH-FF)*s;

        e->set(v21, v22, v23, v24);
      }
    }

    TriangleEdge3<T> e11;
    TriangleEdge3<T> e12;

    TriangleEdge3<T> e21;
    TriangleEdge3<T> e22;

    Vector4<T> com11;
    Vector4<T> com12;
    Vector4<T> com21;
    Vector4<T> com22;

    Quaternion<T> rot11;
    Quaternion<T> rot12;
    Quaternion<T> rot21;
    Quaternion<T> rot22;

    T eps;
    bool rigidEdge1;
    bool rigidEdge2;

    Vector4<T> reference;

  protected:
    Vector4<T> A;
    Vector4<T> B;
    Vector4<T> C;
    Vector4<T> D;

    Vector4<T> FA;
    Vector4<T> FB;
    Vector4<T> FC;
    Vector4<T> FD;

    Vector4<T> E;
    Vector4<T> F;
    Vector4<T> G;
    Vector4<T> H;

    Vector4<T> FE;
    Vector4<T> FF;
    Vector4<T> FG;
    Vector4<T> FH;
    bool initialized;
  };


  /*Finds the configuration of two initial edges (e, x) and two
    current edges (eu, p) ((e->eu, x->p)) at the moment they collide
    (eee, ppp), with res the computed collision point and (bary1,
    bary2) the Barycentric coordinates of res on both edges.*/
  template<class T>
  inline int intersection(const TriangleEdge3<T>& p,  const TriangleEdge3<T>& x,
                          const TriangleEdge3<T>& eu, const TriangleEdge3<T>& e,
                          Vector4<T>& res,
                          const T eps,
                          Vector4<T>* bary1 = 0, Vector4<T>* bary2 = 0,
                          TriangleEdge3<T>* ppp = 0, TriangleEdge3<T>* eee = 0,
                          T deps = 0, int method = 0,
                          T* s = 0){

    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;
    TriangleEdge3<T> pp;
    TriangleEdge3<T> ee;

    Interpolator<T, TriangleEdge3<T>, TriangleEdge3<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.e21 = e;
    func.e22 = eu;
    func.eps = eps;

    //func.reference = *reference;

    /*Perform a root finding on the distance function of the given
      edges*/
    T root = brentDekker<T, TriangleEdge3<T>, TriangleEdge3<T> >(&func, 0, 1,
                                                                 (T)EDGE_TOL);
    func.getResult(root, &pp, &ee);

    pp.computeNormalAndTangent();
    ee.computeNormalAndTangent();

    Vector4<T> p1, p2, b1, b2;

    pp.infiniteProjections(ee, &p1, &p2, &b1, &b2);

    res = p1;

    if(s)     *s = root;
    if(bary1) *bary1 = b1;
    if(bary2) *bary2 = b2;
    if(eee)   *eee = ee;
    if(ppp)   *ppp = pp;

    T distance = (p1-p2).length();

    if(distance > 2.0*EDGE_TOL){
      return 0;
    }

    if(ee.barycentricOnEdge(b1, deps) &&
       ee.barycentricOnEdge(b2, deps)){

      return 1;
    }
    return 0;
  }

  template<class T>
  inline int rigidIntersection(const TriangleEdge3<T>& p,
                               const Vector4<T>&       comp,
                               const Quaternion<T>&    rotp,
                               const TriangleEdge3<T>& x,
                               const Vector4<T>&       comx,
                               const Quaternion<T>&    rotx,
                               const TriangleEdge3<T>& eu,
                               const Vector4<T>&       comeu,
                               const Quaternion<T>&    roteu,
                               const TriangleEdge3<T>& e,
                               const Vector4<T>&       come,
                               const Quaternion<T>&    rote,
                               Vector4<T>&       res,
                               const T           eps,
                               Vector4<T>*       bary1,
                               Vector4<T>*       bary2,
                               TriangleEdge3<T>* ppp,
                               TriangleEdge3<T>* eee,
                               T    deps,
                               int  method,
                               bool r1,
                               bool r2,
                               T*   s){
    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;
    TriangleEdge3<T> pp;
    TriangleEdge3<T> ee;

    Interpolator<T, TriangleEdge3<T>, TriangleEdge3<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.e21 = e;
    func.e22 = eu;
    func.eps = eps;
    func.rigidEdge1 = r1;
    func.rigidEdge2 = r2;
    func.com11 = comx;
    func.com12 = comp;
    func.com21 = come;
    func.com22 = comeu;

    func.rot11 = rotx;
    func.rot12 = rotp;
    func.rot21 = rote;
    func.rot22 = roteu;

    //func.reference = *reference;

    /*Perform a root finding on the distance function of the given
      edges*/
    T root = brentDekker<T, TriangleEdge3<T>, TriangleEdge3<T> >(&func, 0, 1,
                                                                 (T)EDGE_TOL);
    func.getResult(root, &pp, &ee);

    *s = root;

    pp.computeNormalAndTangent();
    ee.computeNormalAndTangent();

    Vector4<T> p1, p2, b1, b2;

    pp.infiniteProjections(ee, &p1, &p2, &b1, &b2);

    res = p1;

    if(bary1) *bary1 = b1;
    if(bary2) *bary2 = b2;
    if(eee)   *eee = ee;
    if(ppp)   *ppp = pp;

    T distance = (p1-p2).length();

    if(distance > 2.0*EDGE_TOL){
      return 0;
    }

    if(ee.barycentricOnEdge(b1, deps) &&
       ee.barycentricOnEdge(b2, deps)){
      return 1;
    }
    return 0;
  }

  template<class T>
  inline TriangleEdge3<T> interpolateEdges(const TriangleEdge3<T>& e1,
                                           const TriangleEdge3<T>& e2,
                                           T d1, T d2, T iso=(T)0.0){
    TriangleEdge3<T> r;
    r.v[0] = linterp(iso, &e1.v[0], &e2.v[0], d1, d2);
    r.v[1] = linterp(iso, &e1.v[1], &e2.v[1], d1, d2);

    r.fv[0] = linterp(iso, &e1.fv[0], &e2.fv[0], d1, d2);
    r.fv[1] = linterp(iso, &e1.fv[1], &e2.fv[1], d1, d2);

    r.computeNormalAndTangent();

    return r;
  }


#endif

  template<class T>
  void TriangleEdge<T>::sampleContactNormal(const TriangleEdge<T>& edge,
                                            Vector4<T>& normal,
                                            Vector4<T>& p)const{
    Vector4<T> bary = edge.getBarycentricCoordinates(p);

    Triangle<T> fa(v[0], v[1], fv[0]);
    Triangle<T> fb(v[1], v[0], fv[1]);

    message("Normals");
    std::cout << n[0] << std::endl;
    std::cout << n[1] << std::endl;

    std::cout << fa.n << std::endl;
    std::cout << fb.n << std::endl;

    message("bary edge");
    std::cout << bary << std::endl;

    Vector4<T> baryf1 = fa.getBarycentricCoordinates(p);
    Vector4<T> baryf2 = fb.getBarycentricCoordinates(p);

    message("bary faces");
    std::cout << baryf1 << std::endl;
    std::cout << baryf2 << std::endl;

    Vector4<T> baryf12 = baryf1;
    Vector4<T> baryf22 = baryf2;

    baryf12[2] += (T)1e-4;
    baryf22[2] += (T)1e-4;

    baryf12[0] = (T)1.0 - baryf12[1] - baryf12[2];
    baryf22[0] = (T)1.0 - baryf22[1] - baryf22[2];

    std::cout << baryf12 << std::endl;
    std::cout << baryf22 << std::endl;

    Vector4<T> pf1 = fa.getCoordinates(baryf12);
    Vector4<T> pf2 = fb.getCoordinates(baryf22);

    message("pf1,2");

    std::cout << pf1 << std::endl;
    std::cout << pf2 << std::endl;

    Vector4<T> pe1 = edge.getCoordinates(edge.getBarycentricCoordinates(pf1));
    Vector4<T> pe2 = edge.getCoordinates(edge.getBarycentricCoordinates(pf2));

    message("pe1,2");

    std::cout << pe1 << std::endl;
    std::cout << pe2 << std::endl;

    T d1 = (pf1-pe1).length();
    T d2 = (pf2-pe2).length();

    message("d1 = %10.10e", d1);
    message("d2 = %10.10e", d2);

    normal = (n[0] / d1) + (n[1] / d2);
    normal.normalize();

    message("sampled normal = ");
    std::cout << normal << std::endl;
  }
}


#endif/*TRIANGLE_HPP*/
