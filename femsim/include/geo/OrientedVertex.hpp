/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef ORIENTEDVERTEX_HPP
#define ORIENTEDVERTEX_HPP

#include "math/Vector4.hpp"
#include "datastructures/List.hpp"
#include "datastructures/Pair.hpp"

namespace CGF{
  template<class T>
  class OrientedVertex{
  public:
    /*
    class SubTriangle{
    public:
      Vector4<T> va;
      Vector4<T> vb;
      Vector4<T> normal;
      int edgeVertexId;
      int edgeId;
      };*/

    class SubEdge{
    public:
      Vector4<T> v;
      Vector4<T> normal;
      int edgeId;
      int vertexId;
    };

    OrientedVertex();

    OrientedVertex(const OrientedVertex<T>& ov);

    OrientedVertex<T>& operator=(const OrientedVertex<T>& ov);

    const Vector4<T>& getVertex()const;

    void setVertex(const Vector4<T>& _p);

    void set(const Vector4<T>& _p,
             List<Vector4<T> >& edgeVertices,
             List<Vector4<T> >& edgeNormals,
             List<int>& edgeIds,
             List<int>& vertexIds);

    bool planeIntersection(const Vector4<T>& planeVertex,
                           const Vector4<T>& planeNormal,
                           const int* indices = 0,
                           bool forced = false,
                           bool debug = false)const;
    T getSmallestEdge()const{
      return smallestEdge;
    }

    int getNEdges()const{
      return subEdges.size();
    }

    void getEdgeIndices(List<int>& l)const;

    template<class Y>
    friend bool orientedVertexCollapsed(const OrientedVertex<Y>& v1,
                                        const OrientedVertex<Y>& v2);

    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const OrientedVertex<Y>& ov);

  protected:
    /*Position of vertex*/
    Vector4<T> p;
    List<SubEdge> subEdges;
    T smallestEdge;
  };

  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const OrientedVertex<T>& ov){
    os << "OrientedVertex" << std::endl;
    os << ov.p << std::endl;
    os << "normals = " << std::endl;

    typename List<typename OrientedVertex<T>::SubEdge>::Iterator it =
      ov.subEdges.begin();
    while(it != ov.subEdges.end() ){
      os << it->edgeId << " | V " << it->v << "N " << it->normal;
      it++;
    }
    return os;
  }
}

#endif/*ORIENTEDVERTEX_HPP*/
