/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef TETRAHEDRON_DATA_HPP
#define TETRAHEDRON_DATA_HPP

#include "math/Matrix44.hpp"
#include "math/Matrix.hpp"
#include "math/Vector4.hpp"

namespace CGF{
  template<class T>
  class TetrahedronData{
  public:
    TetrahedronData(){
      //stiffnessFactor = (T)1.0;
      //volumeForcesActive = false;
    }
    Matrix44<T> initialPos;
    Matrix44<T> c;
    Matrix44<T> lastPos;
    Matrix44<T> lastRotation;
    MatrixT<12, 12, T> initialK;
    int indices[4];

    T volume;
    T initialVolume;
    T mass;
    T* pointers[144];
    //T corVolume;
    ////T stiffnessFactor;
    ////bool volumeInverted[4];
    ////Matrix44<T> faceNormals;
    ////T penaltyStiffness;

    ////bool volumeForcesActive;
    ////int constrainedA;
    ////int constrainedB;
    ////enum VolumeType{ VertexFace, EdgeEdge, Invalid};
    ////VolumeType type;
    ////Vector4<T> springNormals[7];
    ////T distances[7];
    ////Vector4<T> bary;
    ////T initialEdgeDistances[6];
    ////T initialAltitudeDistances[7];
    ////T initialSpringDistances[7];

    ////int invertedDim;
  };

}

#endif/*TETRAHEDRON_DATA_HPP*/
