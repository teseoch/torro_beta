/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef DAEMON_HPP
#define DAEMON_HPP

namespace CGF{

  int continue_as_daemon_process();

  int is_daemon();

  void redirect_std_file_descriptors();

  void redirect_std_file_descriptors_null();

}

#endif/*DAEMON_HPP*/
