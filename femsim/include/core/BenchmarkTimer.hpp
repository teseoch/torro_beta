/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#ifndef BENCHMARKTIMER_HPP
#define BENCHMARKTIMER_HPP

#include "core/cgfdefs.hpp"
#include <map>
#include <string>

namespace CGF{
  typedef struct _timer timer;

  struct _timer{
    struct timeval total_time;
    struct timeval last_time;
    int n;
    int state;
  };


  class CGFAPI BenchmarkTimer{
  public:
    BenchmarkTimer(){
      timeMap.clear();
    }

    void start(const char* timerName);
    void stop(const char* timerName);
    long getAverageTimeUSec(const char* timerName);
    long getTotalTimeUSec(const char* timerName);
    long getTotalCalls(const char* timerName);

    long getAccumulativeUSec();

    void printAverageUSec(const char* timerName);
    void printTotalUSec(const char* timerName);

    void printAllAverageUSec();
    void printAllTotalUSec();

    void printAccumulativeUSec();

    void resetTimers(){
      timeMap.clear();
    }

  protected:
    std::map<std::string, timer>  timeMap;

  private:
    BenchmarkTimer(const BenchmarkTimer&);
    BenchmarkTimer& operator=(const BenchmarkTimer&);
  };

#if 0
  /*This construction is most of the times optimized away*/
  class ScopeBenchmarkTimer{
  public:
    ScopeBenchmarkTimer(BenchmarkTimer& t, const char* n){
      timer = &t;
      name = n;
      timer->start(name);
    }

    ~ScopeBenchmarkTimer(){
      timer->stop(name);
    }

    BenchmarkTimer* timer;
    const char* name;
  };
#endif

#ifdef BENCHMARK
#define TIME(timer, name, call)     \
  timer.start(name);                \
  call;                             \
  timer.stop(name);
#else
#define TIME(timer, name, call)                 \
  call;
#endif
}

#endif/*BENCHMARKTIMER_HPP*/
