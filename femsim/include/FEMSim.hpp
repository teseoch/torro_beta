/* Copyright (C) 2012-2019 by Mickeal Verschoor */

#include "math/Matrix.hpp"
#include "math/Matrix44.hpp"
#include "geo/GridFile.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "math/CollisionContext.hpp"
#include "math/IECLinSolveCR.hpp"
#include "math/IECLinSolveCRGeo.hpp"
#include "math/ConstraintSet.hpp"
#include "math/ConstraintSetFace.hpp"
#include "math/ConstraintSetTriangleEdge.hpp"
#include "math/constraints/GeneralConstraint.hpp"
#include "math/SVD.hpp"

#include "TetrahedronData.hpp"
#include "CorotationalFEM.hpp"
#include "HyperElasticityFEM.hpp"

#include "core/Timer.hpp"
#include "core/BenchmarkTimer.hpp"

#include <unistd.h>

#define T double

namespace CGF{
  extern bool simPaused;
  extern BenchmarkTimer* spmv_timer;

  static void pauseCallBack(){
    if(simPaused){
      message("Simulation paused");

      while(simPaused){
        CGF::usleep(10000);
      }
    }else{
      message("Not paused");
    }
  }

  int vertexFace[4][3]   = {{1,2,3},{0,3,2},{0,1,3},{0,2,1}};
  int edgeEdge[6][2]     = {{0,1}, {2,0}, {0,3}, {1,2}, {1,3}, {2,3}};
  int edgeFaces[6][2]    = {{2,3}, {1,3}, {1,2}, {0,3}, {0,2}, {0,1}};
  int edgeEdgePair[3][2] = {{0,5}, {1,4}, {2,3}};

  extern bool spmv_debug;

  extern bool inverted;

  class VertexData{
    Vector4<T> pos;
  };

  class FaceIndex{
  public:
    FaceIndex(){
      volumes[0] = volumes[1] = -1;
      faceId = -1;
    }

    void addVolume(int v){
      if(volumes[0] == -1){
        volumes[0] = v;
      }else{
        volumes[1] = v;
      }
    }

    void sort(){
      int smallest = Min(v[0], Min(v[1], v[2]));

      int largest  = Max(v[0], Max(v[1], v[2]));

      int middle = -1;

      for(int i=0;i<3;i++){
        if(v[i] != smallest && v[i] != largest){
          middle = v[i];
        }
      }

      v[0] = smallest;
      v[1] = middle;
      v[2] = largest;
    }

    bool operator==(const FaceIndex& f)const{
      for(int i=0;i<3;i++){
        if(v[i] != f.v[i]){
          return false;
        }
      }
      return true;
    }

    bool operator<(const FaceIndex& f)const{
      if(v[0] < f.v[0]){
        return true;
      }else if(v[0] == f.v[0]){
        if(v[1] < f.v[1]){
          return true;
        }else if(v[1] == f.v[1]){
          if(v[2] < f.v[2]){
            return true;
          }else{
            return false;
          }
        }else{
          return false;
        }
      }else{
        return false;
      }
    }

    int v[3];
    int volumes[2];
    int faceId;
  };


  /*FixedEqualityConstraint used to fixate a vertex on a particular position*/
  class FixedEqualityConstraint{
  public:
    Vector4<T> pos; /*Initial position*/
    int vertexId;

    GeneralConstraint<2, T>* constraint1;
    GeneralConstraint<2, T>* constraint2;
    GeneralConstraint<2, T>* constraint3;

    int cid[3];

    ConstraintDescriptor<T> descriptor;
  };

  enum ConstraintGeometryType{
    Undefined = 0,
    CVertex,
    CEdge,
    CFace
  };

  /*LinkedEqualityConstraint used to fixate a vertex on a particular position*/
  class LinkedEqualityConstraint{
  public:
    Vector4<T> pos[2]; /*Initial position*/
    Vector4<T> bary[2]; /*Barycentric coordinates of 0 on 1 and 1 on 0*/

    int vertexId[2][3];
    T initialDistance; /*Initial distance between p1 and p2, which
                         will be maintained.*/

    ConstraintGeometryType type[2]; /*Types of linked entities, e.g.,
                                      vertex-face.*/

    GeneralConstraint<2, T>* constraint1;
    GeneralConstraint<2, T>* constraint2;
    GeneralConstraint<2, T>* constraint3;

    int cid[3];

    ConstraintDescriptor<T> descriptor;
  };

  class FEMSim{
  public:
    typedef typename List<FixedEqualityConstraint*>::Iterator  FECIterator;
    typedef typename List<LinkedEqualityConstraint*>::Iterator LECIterator;

    /*Resume simulation*/
    FEMSim(GridFile<T>& g, int continueFrame):mesh(0), gridFile(&g),
                                              exporter(0), conditionExporter(0),
                                              solver(0), ctx(0){
      construct(g);

      message("continueFrame = %d", continueFrame);
      /*continueFrame is the previous frame*/
      continueFrame++;
      tstep = continueFrame;
      resumeStep = continueFrame;
    }

    /*Start simulation*/
    FEMSim(GridFile<T>& g):mesh(0), gridFile(&g), exporter(0),
                           conditionExporter(0), solver(0), ctx(0){
      construct(g);
      resumeStep = 0;
    }
    void construct(GridFile<T>& g){
      setParameters();

      nDeformableVertices = g.getNGridVertices();
      nDeformableElements = g.getNGridElements();
      nStaticVertices     = g.getNStaticVertices();
      nRigidVertices      = g.getNRigidVertices();
      nRigidElements      = g.getNRigidElements();
      nClothVertices      = g.getNClothVertices();

      vectorSize          = (nDeformableVertices * 3 +
                             nRigidElements * 6 +
                             nClothVertices * 3);

      mesh = g.getMesh();

      //FEM = new CorotationalFEM<T>(mesh, nDeformableElements);
      FEM = new HyperElasticityFEM<T>(mesh, nDeformableElements);
      //FEM->setMu(mu);
      FEM->setE(E);

      rigidVerticesMap = new int[nRigidVertices];

      for(int i=0;i<nRigidVertices;i++){
        rigidVerticesMap[i] = g.getRigidObjectFromVertex(i);
      }

      setInitialPositions(); /*Transform coordinates*/

      globalInitialPositions       = new Vector<T>(vectorSize);
      globalCurrentPositions       = new Vector<T>(vectorSize);
      globalTotalDisplacement      = new Vector<T>(vectorSize);

      gm = 0;

      globalLastVelocity = 0;
      globalForceVector  = 0;

      tetdata                = new TetrahedronData<T>[nDeformableElements];
      vertdata               = new VertexData[mesh->getNVertices()];

      inertiaTensors         = new Matrix44<T>[nRigidElements];
      objectMass             = new T[nRigidElements];
      objectAngVelocity      = new Vector4<T>[nRigidElements];
      objectLinVelocity      = new Vector4<T>[nRigidElements];
      objectExternalVelocity = new int[nRigidElements];
      objectAngForce         = new Vector4<T>[nRigidElements];
      objectLinForce         = new Vector4<T>[nRigidElements];
      objectExternalForce    = new int[nRigidElements];

      for(int i=0;i<nRigidElements;i++){
        objectExternalVelocity[i] = 0;
        objectExternalForce[i] = 0;
      }
      /**/

      message("%d tet", nDeformableElements);
      message("%d vert", mesh->getNVertices());



#if 0
      volumeConstraints = new GeneralConstraint<2, T>*[nDeformableElements];

      for(int i=0;i<nDeformableElements;i++){
        volumeConstraints[i] = new GeneralConstraint<2, T>();
      }
#endif
      tstep = 0;
    }

    ~FEMSim(){
      delete globalInitialPositions;
      delete globalCurrentPositions;
      delete globalTotalDisplacement;

      delete [] tetdata;
      delete [] vertdata;

      delete [] rigidVerticesMap;

      delete [] ctx->sets;
      delete [] ctx->backSets;
      delete [] ctx->edgeSets;
      delete [] ctx->backEdgeSets;
      delete [] ctx->faceFaceSets;
      delete [] ctx->triangleEdgeSets;
      delete [] ctx->collapsedEdges;
      delete [] ctx->collapsedBackEdges;

#if 0
      delete [] ctx->volumeFaceSets;
      delete [] ctx->volumeSets;
#endif

      delete [] inertiaTensors;
      delete [] objectMass;
      delete [] objectAngVelocity;
      delete [] objectLinVelocity;
      delete [] objectExternalVelocity;

      delete [] objectAngForce;
      delete [] objectLinForce;
      delete [] objectExternalForce;

      delete mesh;

      delete ctx->stree;
      delete ctx->vertexDegree;
      delete solver;
      delete FEM;
      ctx->deleteBBoxes();
      delete ctx;
#if 0
      for(int i=0;i<nDeformableElements;i++){
        delete volumeConstraints[i];
      }

      delete[] volumeConstraints;
#endif

    }

    void computeInertiaTensors(){
      T factor = (T)1.0;
      Matrix44<T> I = Matrix44<T>::identity();

      I[3][3] = 0.0;

      for(int i=0;i<nRigidElements;i++){
        Matrix44<T> iTensor;

        T totalVolume = 0.0;
        T totalMass   = 0.0;

        for(int j=0;j<mesh->getNHalfFaces();j++){
          mesh->setCurrentEdge(mesh->halfFaces[j].half_edge);
          int v = mesh->getOriginVertex();

          if(mesh->vertexObjectMap[v] == i){
            /*Vertex of face j belongs to object i*/

            Vector4<T> va = mesh->vertices[v].coord;
            mesh->nextEdge();
            Vector4<T> vb = mesh->vertices[mesh->getOriginVertex()].coord;
            mesh->nextEdge();
            Vector4<T> vc = mesh->vertices[mesh->getOriginVertex()].coord;

            Vector4<T> vd = mesh->centerOfMass[i];

            cgfassert(va[3] == 0.0);
            cgfassert(vb[3] == 0.0);
            cgfassert(vc[3] == 0.0);
            cgfassert(vd[3] == 0.0);

            mesh->nextEdge();

            if(v != mesh->getOriginVertex()){
              error("vertex not equal to begin vertex");
            }

            Matrix44<T> tet;
            tet[0] = va;
            tet[1] = vb;
            tet[2] = vc;
            tet[3] = vd;


            tet[0][3] = 1;
            tet[1][3] = 1;
            tet[2][3] = 1;
            tet[3][3] = 1;

            T volume = tet.det()/6.0;

            totalVolume += volume;
            totalMass   += volume * density/factor;

            Vector4<T> comTet = (va + vb + vc + vd)*(T)0.25;

            Matrix44<T> coords;
            coords[0] = va;
            coords[1] = vb;
            coords[2] = vc;
            coords[3] = vd;

            Matrix44<T> localInertiaTensor;

            for(int l=0;l<4;l++){
              /*Compute r*/
              Vector4<T> r1 = coords[l] - comTet;

              Matrix44<T> skewSymmetric;
              skewSymmetric[0][1] = -r1[2];
              skewSymmetric[0][2] =  r1[1];
              skewSymmetric[1][0] =  r1[2];
              skewSymmetric[1][2] = -r1[0];
              skewSymmetric[2][0] = -r1[1];
              skewSymmetric[2][1] =  r1[0];
              skewSymmetric[3][3] =  0;

              localInertiaTensor +=
                -skewSymmetric*skewSymmetric * volume * density * 0.25/factor;

            }

            Vector4<T> r2 = comTet - mesh->centerOfMass[i];

            iTensor += localInertiaTensor + (dot(r2, r2) * I - r2.outherProduct(r2))*volume*density/factor;
          }
        }
        //iTensor /= totalMass;
        message("tensor");
        std::cout << iTensor;
        message("mass    = %10.10e", totalMass);
        message("volume  = %10.10e", totalVolume);
        inertiaTensors[i] = iTensor;
        objectMass[i] = totalMass;
        //getchar();
      }
    }

    void setInitialPositions(){
      for(int i=0;i<mesh->getNTetrahedra();i++){
        int v3 = mesh->tetrahedra[i].v3;
        int v4 = mesh->tetrahedra[i].v4;

        mesh->tetrahedra[i].v3 = v4;
        mesh->tetrahedra[i].v4 = v3;
      }

      for(int i=0;i<mesh->getNVertices();i++){
        T x = mesh->vertices[i].coord[2]/(T)3.0;
        T y = mesh->vertices[i].coord[0]/(T)3.0;
        T z = mesh->vertices[i].coord[1]/(T)3.0;

        mesh->vertices[i].coord[0] = x;
        mesh->vertices[i].coord[1] = y;
        mesh->vertices[i].coord[2] = z;
      }
    }

    void setParameters(){
      density = (T)1000.0;    /*Kg/M^3*/
      mu      = (T)0.2;       /*Elasticity index*/
      E       = (T)50000.0; /*Young's modulus GPa or GN/m^2*/ /*5 MPa = 0.005 GPa*/
      E       = (T)5000000.0;///(T)16.0;
      //mu = 0.4;
      damping = (T)1000.0 * 1.0;       /*N/ms^{-1}*/
      //dt      = (T)0.000125*(T)1.0;    /*s*/
      //dt = (T)0.0005;
      dt = (T)0.0005 * (T)50.0;
      dt      = (T)0.001;
      gravity = (T)9.81;      /*m/s^2*/
      //density = (T)1;

      gravityVector.set(0,-1,1,0);
      gravityVector.set(0,0,1,0);
      gravityVector.normalize();
      gravityVector *= - gravity;
    }

    void initialize(){
      intNN.clear();
      intN.clear();

      for(int i=0;i<12;i++){
        for(int j=0;j<12;j++){
          if(i==j){
            //intNN[i][j] = (T)0.1;
            intNN[i][j] = 0.25;
          }else{
            if((j-i%3)%3==0){
              //intNN[i][j] = (T)0.05;
            }
          }
        }
      }

      std::cout << intNN;

      for(int i=0;i<12;i++){
        for(int j=0;j<3;j++){
          if((j-i)%3==0){
            intN[i][j] = (T)0.25;
          }
        }
      }
      std::cout << intN;
      std::cout << intN.transpose();
      //MatrixT<12, 12, T>  intNT = intN*intN.transpose();

      //std::cout << intN*intN.transpose();
      //std::cout << intNT;

      /**/

      computeVolumes(true);

      computeInitialMass();

      /*Compute gravitational force*/
      for(int i=0;i<12;i++){
        volumeForceVector[i][0] = gravityVector[i%3];
      }


      /*Compute centers of mass*/

      T coordsx[] = {5.0, 10.0, 10.0, 5.0};
      T coordsy[] = {0.0,  0.0,  1.0, 1.0};

      Vector4<T> center;
      //Vector4<T> center2;

      T totalArea = 0;

      for(int i=0;i<4;i++){
        int j = i+1;
        if(j>=4){
          j=0;
        }

        message("i, j = %d, %d", i, j);

        Vector4<T> a(coordsx[i], coordsy[i], 0, 0);
        Vector4<T> b(coordsx[j], coordsy[j], 0, 0);
        Vector4<T> c(0,0,0,0);

        message("a, b");
        std::cout << a;
        std::cout << b;



        Vector4<T> ac = a-c;
        Vector4<T> bc = b-c;

        Vector4<T> nn = cross(ac, bc);

        T area = nn.length()/2.0;


        message("area = %f", area);

        Vector4<T> zz(0,0,1,0);

        message("sign = %f", Sign(dot(nn, zz)));

        message("l center");
        std::cout << (a + b + c)/3.0;

        center += area*Sign(dot(nn, zz))*(a + b + c)/3.0;

        totalArea += Sign(dot(nn, zz))*area;

      }

      std::cout << center;
      std::cout << center/totalArea;
    }

    void findTetrahedronFaces(Tree<FaceIndex>& faceIndices){
      /*Mesh does not contain internal faces.. move to DCEList?*/

      int faceIndex = 0;
      for(int i=0;i<mesh->getNTetrahedra();i++){
        int vertices[4];
        mesh->tetrahedra[i].getIndices(vertices);

        FaceIndex f1, f2, f3, f4;
        f1.v[0] = vertices[0];
        f1.v[1] = vertices[1];
        f1.v[2] = vertices[2];
        f1.sort();

        f2.v[0] = vertices[3];
        f2.v[1] = vertices[1];
        f2.v[2] = vertices[2];
        f2.sort();

        f3.v[0] = vertices[0];
        f3.v[1] = vertices[3];
        f3.v[2] = vertices[2];
        f3.sort();

        f4.v[0] = vertices[0];
        f4.v[1] = vertices[1];
        f4.v[2] = vertices[3];
        f4.sort();

        int idx = faceIndices.findIndex(f1);
        if(idx == -1){
          f1.addVolume(i);
          f1.faceId = faceIndex++;
          faceIndices.insert(f1, i);
        }else{
          (*faceIndices.find(f1)).addVolume(i);
        }

        idx = faceIndices.findIndex(f2);
        if(idx == -1){
          f2.addVolume(i);
          f2.faceId = faceIndex++;
          faceIndices.insert(f2, i);
        }else{
          (*faceIndices.find(f2)).addVolume(i);
        }

        idx = faceIndices.findIndex(f3);
        if(idx == -1){
          f3.addVolume(i);
          f3.faceId = faceIndex++;
          faceIndices.insert(f3, i);
        }else{
          (*faceIndices.find(f3)).addVolume(i);
        }

        idx = faceIndices.findIndex(f4);
        if(idx == -1){
          f4.addVolume(i);
          f4.faceId = faceIndex++;
          faceIndices.insert(f4, i);
        }else{
          (*faceIndices.find(f4)).addVolume(i);
        }
      }

      nDeformableElementFaces = faceIndices.size();

      /*
      message("found %d faces associated with tetrahedra",
      nDeformableElementFaces);

      TreeIterator<FaceIndex> it = faceIndices.begin();

      while(it != faceIndices.end()){
        FaceIndex f = *it++;
        message("face[%d] %d, %d | %d, %d, %d",
                f.faceId, f.volumes[0], f.volumes[1],
                f.v[0], f.v[1], f.v[2]);
      }

      getchar();*/
    }

    void initializeSolver(){
      /*Set up solver*/
      ctx = new CollisionContext<T>();

      solver = new IECLinSolveT2CRGeo<2, T>(vectorSize, ctx);

      solver->setIterationCallBack(&pauseCallBack);
      solver->setExporter(exporter);

      gm = solver->getMatrix();
      globalLastVelocity = solver->getx();
      globalForceVector  = solver->getb();

      ctx->gridFile = gridFile;
      ctx->mesh     = mesh;
      ctx->solver   = solver;
      ctx->mat      = solver->getMatrix();
      ctx->v        = solver->getx();

      ctx->setDT(dt);

      /*Compute initial center of mass*/
      ctx->mesh->computeCentersOfMass();

      computeInertiaTensors();

      ctx->mesh->computeDisplacement(ctx->v, dt);
      ctx->initializeBBoxes();
      ctx->updateBBoxes();

      ctx->stree  = new SurfaceTree<T>(mesh);
      ctx->stree->constructTree();

      ctx->vertexDegree = new Vector<T>(*ctx->v);
      *(ctx->vertexDegree) *= (T)0.0;

      initialize();


      /*Create constraintsets*/

      int nVertices = mesh->getNVertices();

      ctx->sets = new ConstraintSet<T>[nVertices];
      for(int i=0;i<nVertices;i++){
        ctx->sets[i].vertexId = i;
        ctx->mesh->getOrientedVertex(&ctx->sets[i].startPosition, i, false);
        ctx->sets[i].type = mesh->vertices[i].type;
      }

      ctx->backSets = new ConstraintSet<T>[nVertices];
      for(int i=0;i<nVertices;i++){
        ctx->backSets[i].vertexId = i;
        ctx->mesh->getOrientedVertex(&ctx->backSets[i].startPosition, i, true);
        ctx->backSets[i].type = mesh->vertices[i].type;
        ctx->backSets[i].setBackFace();
      }

      ctx->edgeSets = new ConstraintSetEdge<T>[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        mesh->setCurrentEdge(i);

        ctx->edgeSets[i].edgeId = i;
        ctx->edgeSets[i].vertexId[0]   = mesh->getOriginVertex();
        ctx->edgeSets[i].vertexId[1]   = mesh->getTwinOriginVertex();
        ctx->edgeSets[i].faceId[0]     = mesh->getFace();
        ctx->edgeSets[i].faceId[1]     = mesh->getTwinFace();
        ctx->edgeSets[i].type          = mesh->halfEdges[i].type;
        mesh->getEdge(&ctx->edgeSets[i].startPosition, i, false);
      }

      ctx->backEdgeSets = new ConstraintSetEdge<T>[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        mesh->setCurrentEdge(i);

        ctx->backEdgeSets[i].edgeId = i;
        ctx->backEdgeSets[i].vertexId[0]   = mesh->getOriginVertex();
        ctx->backEdgeSets[i].vertexId[1]   = mesh->getTwinOriginVertex();
        ctx->backEdgeSets[i].faceId[0]     = mesh->getFace();
        ctx->backEdgeSets[i].faceId[1]     = mesh->getTwinFace();
        ctx->backEdgeSets[i].setBackFace();
        ctx->backEdgeSets[i].type          = mesh->halfEdges[i].type;
        mesh->getEdge(&ctx->backEdgeSets[i].startPosition, i, true);
      }

      ctx->faceFaceSets = new ConstraintSetFaces<T>[mesh->getNHalfFaces()];
      for(int i=0;i<mesh->getNHalfFaces();i++){
        ctx->faceFaceSets[i].faceId = i;
      }

      ctx->triangleEdgeSets =
        new ConstraintSetTriangleEdge<T>[mesh->getNHalfEdges()];
      for(int i=0;i<mesh->getNHalfEdges();i++){
        ctx->triangleEdgeSets[i].edgeId = i;
        ctx->triangleEdgeSets[i].type = mesh->halfEdges[i].type;
      }

      ctx->collapsedEdges     = new bool[mesh->getNHalfEdges()];
      ctx->collapsedBackEdges = new bool[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        ctx->collapsedEdges[i]     = false;
        ctx->collapsedBackEdges[i] = false;
      }

#if 0
      ctx->volumeSets = new ConstraintSetVolume3<T>[mesh->getNTetrahedra()];
      for(int i=0;i<mesh->getNTetrahedra();i++){

        ctx->volumeSets[i].volumeId = i;

        mesh->tetrahedra[i].getIndices(ctx->volumeSets[i].vertices);
        mesh->getTetrahedronCoords(i, ctx->volumeSets[i].startVertices);

        ctx->volumeSets[i].initialVertices = ctx->volumeSets[i].startVertices;

        ctx->volumeSets[i].initialVolume = tetdata[i].volume;

        ctx->volumeSets[i].computeInitialDistances();
      }
#endif
      Tree<FaceIndex> faceIndices;
      findTetrahedronFaces(faceIndices);

#if 0
      ctx->volumeFaceSets =
        new ConstraintSetVolumeFace<T>[nDeformableElementFaces];

      TreeIterator<FaceIndex> it = faceIndices.begin();
      int idx = 0;
      while(it != faceIndices.end()){
        FaceIndex face = *it++;
        ctx->volumeFaceSets[idx].volumes[0] = face.volumes[0];
        ctx->volumeFaceSets[idx].volumes[1] = face.volumes[1];
        ctx->volumeFaceSets[idx].faceId = face.faceId;

        int indices[8];
        //int indices2[8];
        for(int i=0;i<4;i++){
          if(face.volumes[0] != -1){
            indices[i+0] = ctx->volumeSets[face.volumes[0]].vertices[i];
          }else{
            indices[i+0] = -1;
          }
          if(face.volumes[1] != -1){
            indices[i+4] = ctx->volumeSets[face.volumes[1]].vertices[i];
          }else{
            indices[i+4] = -1;
          }
          //indices2[i+0] = indices[i+0];
          //indices2[i+4] = indices[i+4];
        }

        /*Find non face vertices*/
        for(int i=0;i<8;i++){
          for(int j=0;j<3;j++){
            if(indices[i] == face.v[j]){
              indices[i] = -1;
            }
          }
        }

        /*Move to begin*/
        int curIdx = 0;
        for(int i=0;i<8;i++){
          if(indices[i] != -1){
            indices[curIdx] = indices[i];
            if(i != curIdx){
              indices[i] = -1;
            }
            curIdx++;
          }
        }

        /*
        message("face[%d] %d, %d | (%d, %d, %d, %d) | (%d, %d, %d, %d) | (%d, %d, %d)",
                idx, indices[0], indices[1],
                indices2[0], indices2[1], indices2[2], indices2[3],
                indices2[4], indices2[5], indices2[6], indices2[7],
                face.v[0], face.v[1], face.v[2]);
        */

        /*Face indices*/
        ctx->volumeFaceSets[idx].indices[0] = face.v[0];
        ctx->volumeFaceSets[idx].indices[1] = face.v[1];
        ctx->volumeFaceSets[idx].indices[2] = face.v[2];

        /*Vertex indices*/
        ctx->volumeFaceSets[idx].indices[3] = indices[0];
        ctx->volumeFaceSets[idx].indices[4] = indices[1];

        for(int i=0;i<2;i++){
          if(face.volumes[i] != -1){
            for(int j=0;j<4;j++){
              if(ctx->volumeSets[face.volumes[i]].faceConstraints[j] == 0){
                ctx->volumeSets[face.volumes[i]].faceConstraints[j] =
                  &(ctx->volumeFaceSets[idx]);
                j = 10;
              }
            }
          }
        }

        idx++;
      }
      //getchar();
#endif

      /*Add equality constraints*/
      for(int i=0;i<gridFile->getNConstraintDescriptors();i++){
        ConstraintDescriptor<T> d = gridFile->getConstraintDescriptor(i);
        message("Constraint %d, %d, %d", i, d.objectId[0], d.objectId[1]);
        std::cout << d << std::endl;

        if(d.type == ConstraintDescriptor<T>::Fixed){
          /*Fixed constraint*/
          message("Fixed constraint");
          if(d.objectId[0] != -1){
            /*Fixed rigid object*/
            message("Fixed object");
            error("Not implemented yet");
          }else{
            if(d.vertexId[0][0] != -1 &&
               d.vertexId[0][1] != -1 &&
               d.vertexId[0][2] != -1){
              /*Fixed face*/
              message("Fixed face");
            }else if(d.vertexId[0][0] != -1 &&
                     d.vertexId[0][1] != -1){
              /*Fixed edge*/
              message("Fixed edge");
            }else if(d.vertexId[0][0] != -1){
              /*Fixed vertex*/
              message("Fixed vertex");
            }

            for(int j=0;j<3;j++){
              if(d.vertexId[0][j] == -1){
                break;
              }

              FixedEqualityConstraint* eqConstraint =
                new FixedEqualityConstraint();

              fEqConstraints.append(eqConstraint);

              eqConstraint->vertexId = d.vertexId[0][j];
              eqConstraint->pos = mesh->vertices[d.vertexId[0][j]].coord;

              eqConstraint->constraint1 = new GeneralConstraint<2, T>();
              eqConstraint->constraint2 = new GeneralConstraint<2, T>();
              eqConstraint->constraint3 = new GeneralConstraint<2, T>();

              eqConstraint->descriptor = d;

              /*Is vertex a rigid vertex or deformable vertex?*/
              if(mesh->vertices[d.vertexId[0][j]].type == Mesh::Rigid){
                /*Rigid vertex*/
                message("Rigid vertex");
              }else{
                /*Deformable vertex*/
                message("Deformable vertex");
              }

              if(d.interpolated[0] == true &&
                 mesh->vertices[d.vertexId[0][j]].type != Mesh::Deformable){
                eqConstraint->pos = d.p[0];
                break;
              }
            }
          }
        }else if(d.type == ConstraintDescriptor<T>::Linked){
          message("Linked constraint");
          ConstraintGeometryType types[2];
          types[0] = types[1] = Undefined;

          if(d.objectId[0] != -1){
            /*Fixed rigid object*/
            message("Fixed object");
            error("Not implemented yet");
          }else{
            if(d.vertexId[0][0] != -1 &&
               d.vertexId[0][1] != -1 &&
               d.vertexId[0][2] != -1){
              /*Linked face*/
              message("Linked face");
              types[0] = CFace;

            }else if(d.vertexId[0][0] != -1 &&
                     d.vertexId[0][1] != -1){
              /*Linked edge*/
              message("Linked edge");
              types[0] = CEdge;

            }else if(d.vertexId[0][0] != -1){
              /*Linked vertex*/
              message("Linked vertex");

              types[0] = CVertex;
            }

            message("Linked with");

            if(d.vertexId[1][0] != -1 &&
               d.vertexId[1][1] != -1 &&
               d.vertexId[1][2] != -1){
              /*Linked face*/
              message("Linked face");

              types[1] = CFace;

            }else if(d.vertexId[1][0] != -1 &&
                     d.vertexId[1][1] != -1){
              /*Linked edge*/
              message("Linked edge");

              types[1] = CEdge;

            }else if(d.vertexId[1][0] != -1){
              /*Linked vertex*/
              message("Linked vertex");

              types[1] = CVertex;
            }

            if( (types[0] == CVertex && types[1] == CFace) ||
                (types[0] == CEdge   && types[1] == CEdge) ||
                (types[0] == CFace   && types[1] == CVertex) ){
              message("Valid combination, create linked constraint");

              LinkedEqualityConstraint* eq = new LinkedEqualityConstraint();

              eq->type[0] = types[0];
              eq->type[1] = types[1];
              eq->descriptor = d;

              for(int j=0;j<2;j++){
                for(int k=0;k<3;k++){
                  eq->vertexId[j][k] = d.vertexId[j][k];
                }
              }

              bool degenerate = false;

              /*Compute barycentric weights*/
              if(eq->type[0] == CVertex){

                eq->pos[0] = mesh->vertices[eq->vertexId[0][0]].coord;

                eq->bary[0].set(1,0,0,0);
                if(eq->type[1] == CFace){
                  /*Project vertex 0 on face 1*/
                  Triangle<T> tri(mesh->vertices[eq->vertexId[1][0]].coord,
                                  mesh->vertices[eq->vertexId[1][1]].coord,
                                  mesh->vertices[eq->vertexId[1][2]].coord);
                  eq->bary[1] = tri.getBarycentricCoordinates(eq->pos[0]);
                  eq->pos[1]  = tri.getCoordinates(eq->bary[1]);
                }
              }else if(eq->type[0] == CEdge){
                TriangleEdge<T> edge1(mesh->vertices[eq->vertexId[0][0]].coord,
                                      mesh->vertices[eq->vertexId[0][1]].coord);

                if(eq->type[1] == CEdge){
                  TriangleEdge<T>
                    edge2(mesh->vertices[eq->vertexId[1][0]].coord,
                          mesh->vertices[eq->vertexId[1][1]].coord);

                  try{
                    edge1.getSignedDistance(edge2, &(eq->pos[0]), &(eq->pos[1]),
                                            &(eq->bary[0]), &(eq->bary[1]));
                  }catch(DegenerateCaseException* e){
                    PRINT(e->getError());
                    degenerate = true;
                  }

                }
              }else if(eq->type[0] == CFace){
                Triangle<T> tri(mesh->vertices[eq->vertexId[0][0]].coord,
                                mesh->vertices[eq->vertexId[0][1]].coord,
                                mesh->vertices[eq->vertexId[0][2]].coord);
                if(eq->type[1] == CVertex){
                  eq->pos[1]  = mesh->vertices[eq->vertexId[1][0]].coord;
                  eq->bary[1].set(1,0,0,0);

                  eq->bary[0] = tri.getBarycentricCoordinates(eq->pos[1]);
                  eq->pos[0]  = tri.getCoordinates(eq->bary[0]);
                }
              }else{
                error("Unsupported combination");
              }

              if(!degenerate){
                eq->initialDistance = (eq->pos[0] - eq->pos[1]).length();

                eq->constraint1 = new GeneralConstraint<2, T>();
                eq->constraint2 = new GeneralConstraint<2, T>();
                eq->constraint3 = new GeneralConstraint<2, T>();

                lEqConstraints.append(eq);
              }else{
                delete eq;
              }
            }
          }
        }
      }

      for(int i=0;i<gridFile->getNVelocityDescriptors();i++){
        ExternalVelocityDescriptor<T> d = gridFile->getVelocityDescriptor(i);
        std::cout << d << std::endl;

        objectAngVelocity[d.rigidObject]      = d.angVelocity;
        objectLinVelocity[d.rigidObject]      = d.linVelocity;
        objectExternalVelocity[d.rigidObject] = 1;
      }

      for(int i=0;i<gridFile->getNForceDescriptors();i++){
        ExternalForceDescriptor<T> d = gridFile->getForceDescriptor(i);
        std::cout << d << std::endl;

        objectAngForce[d.rigidObject]      = d.angForce;
        objectLinForce[d.rigidObject]      = d.linForce;
        objectExternalForce[d.rigidObject] = 1;
      }

#if 0
      ThreadPool pool(1);

      QTTask task(&pool);
      task.ctx = ctx;

      message("ctx pointer = %p", ctx);

      pool.assignTask(&task, 0);

      pool.start();
#endif
    }




    void gatherLocalVector(int v[4], MatrixT<12, 1, T>& local,
                           Vector<T>& global){
      for(int i=0;i<4;i++){
        int idx1 = v[i]*3;
        int idx2 = i*3;
        for(int j=0;j<3;j++){
          local[idx2+j][0] = global[idx1+j];
        }
      }
    }

    void FEMUpdate(){
      Vector<T>* velocity = (Vector<T>*)globalLastVelocity;

      (*globalCurrentPositions)  += (*velocity) * dt;
      (*globalTotalDisplacement) += (*velocity) * dt;

      for(int i=0;i<velocity->getSize();i++){
        if(IsNan((*velocity)[i])){
          std::cout << *velocity << std::endl;
          error("nan detected in velocity");
        }
      }

      //std::cout << *velocity;

      /*Update mesh using current velocity*/
      mesh->computeDisplacement(velocity, dt);
      mesh->acceptDisplacement();

      /*Update (extended) bounding volumes*/
      ctx->stree->update(globalLastVelocity, dt);

      save_ply("last.ply", mesh);
    }

#define CONDITION_CHECK

    void FEMSolve(){
      /*Reset solver*/
      //getchar();
      if(tstep == 0){
        /*Set geometry in solver*/

        //Add vertices

        //Add face

        /*Copy solid geometry*/

        /*Precompute stuff*/
      }

#ifdef CONDITION_CHECK
      T maxCond = 0.0;
      T minCond = 1e+10;

      T maxEv   = -1e+10;
      T minEv   = 1e+10;
#endif

      Timer timer;
      timer.start();

      int nTetrahedra = mesh->getNTetrahedra();
      T* rhsData = globalForceVector->getData();

      message("tstep = %d, resumeStep = %d", tstep, resumeStep);

      for(int i=0;i<nTetrahedra;i++){
        MatrixT<12, 12, T> k(false);
        MatrixT<12, 1,  T> elastic(false);
        MatrixT<12, 1,  T> forceVector_1(false);
        MatrixT<12, 1,  T> localVelocity(false);
        MatrixT<12, 1,  T> localInitialPositions(false);
        MatrixT<12, 1,  T> localCurrentPositions(false);

        int* vert = tetdata[i].indices;

        gatherLocalVector(vert, localVelocity, *globalLastVelocity);
        //gatherLocalVector(vert, localInitialPositions, *globalInitialPositions);
        //gatherLocalVector(vert, localCurrentPositions, *globalCurrentPositions);


        /*Stiffness Matrix*/
#if 1
        FEM->computeElementStiffnessMatrixAndForce(k, elastic,
                                               localInitialPositions,
                                               localCurrentPositions,
                                               tetdata[i], i, dt,
                                               false, false);
#else
        k.clear();
        elastic.clear();
#endif
        /*Mass & Damping*/
        T massFactor = (T)1.0 * tetdata[i].mass;
        T dampingFactor = (T)1.0 * tetdata[i].initialVolume * damping * dt;

        for(int l=0;l<12;l++){
          T massForcel   = massFactor * localVelocity[l][0];
          T volumeForcel = massFactor * (volumeForceVector[l][0] * dt);

          k[l][l] += (massFactor + dampingFactor);

          /*Volue forces*/
          forceVector_1[l][0] =
            elastic[l][0] + massForcel + volumeForcel;
        }


        if(tstep <= 2 || tstep <= resumeStep+11){
        //if(true){
          /*Set pointers in the second simulation step.
            In the first step we work with a non-finalized matrix.
           */
          for(int jj=0;jj<12;jj++){
            int row = vert[jj/3]*3 + jj%3;
            for(int kk=0;kk<12;kk++){
              int col = vert[kk/3]*3 + kk%3;
#if 1
              (*gm)[row][col] += k[jj][kk];
#else
              if(row == col){
                (*gm)[row][col] = (T)1.0;
              }else{
                (*gm)[row][col] = (T)0.0;
              }
#endif
              tetdata[i].pointers[jj*12+kk] = &((*gm)[row][col]);
            }
          }
#if 1
        }else{
          for(int jj=0;jj<12;jj++){
            int col = jj*12;
            for(int kk=0;kk<12;kk++){
#if 1
              *(tetdata[i].pointers[col+kk]) += k[jj][kk];
#else
              if(jj == kk){
                *(tetdata[i].pointers[col+kk]) = (T)1.0;
              }else{
                *(tetdata[i].pointers[col+kk]) = (T)0.0;
              }
#endif
            }
          }
        }
#endif

        /*Save RHS*/
        for(int j=0;j<4;j++){
          int idx1 = vert[j]*3;
          int idx2 = j*3;
          for(int k=0;k<3;k++){
            rhsData[idx1+k] += forceVector_1[idx2+k][0];
          }
        }

        /*Compute Eigenvalues and Condition number of k*/
        T evkmin, evkmax;
        T cond = k.conditionNumber(true, 1e-9, &evkmin, &evkmax);
        if(evkmin < minEv){
          minEv = evkmin;
        }

        if(evkmax > maxEv){
          maxEv = evkmax;
        }

        if(cond < minCond){
          minCond = cond;
        }

        if(cond > maxCond){
          maxCond = cond;
        }
      }

      timer.stop();

      std::cout << timer << std::endl;

#ifdef CONDITION_CHECK
      message("max condition number = %10.10e", maxCond);
      message("min condition number = %10.10e", minCond);
      message("max eigenvalue       = %10.10e", maxEv);
      message("min eigenvalue       = %10.10e", minEv);

      T evMin = 0.0;
      T evMax = 0.0;

      T globalCondition = 0.0;
      //gm->conditionNumber(true, evMin, evMax, (T)2e-10,
      //                  0, nDeformableVertices*3);

      message("condition number = %10.10e", globalCondition);

      conditionExporter->setValue("conditionNumber", globalCondition);
      conditionExporter->setValue("minEigenValue", evMin);
      conditionExporter->setValue("maxEigenValue", evMax);
      conditionExporter->setValue("smallestEigenValue", minEv);
      conditionExporter->setValue("largestEigenValue",  maxEv);
      conditionExporter->setValue("smallestConditionNumber", minCond);
      conditionExporter->setValue("largestConditionNumber", maxCond);

      conditionExporter->saveRow();
#endif

    }

    void RBSolve(){
      PRINT_FUNCTION;
      int offset = nDeformableVertices * 3;

      computeInertiaTensors();

      PRINT(*globalLastVelocity);

      PRINT(nRigidElements);
      PRINT(gm->getWidth());
      PRINT(gm->getHeight());

      for(int i=0;i<nRigidElements;i++){
        //offset += i*6;
        int localOffset = offset + i*6;

        PRINT(offset);
        PRINT(localOffset);

        /*Set mass matrix*/
        T rbd = 0.001*1;
        for(int j=0;j<3;j++){
          PRINT(localOffset + j);
          (*gm)[localOffset + j][localOffset + j] = objectMass[i] + dt * rbd;//kg
        }

        PRINT(objectMass[i]);

        /*Set external forces*/
        for(int j=0;j<3;j++){
          (*globalForceVector)[localOffset + j] =
            objectMass[i] * (*globalLastVelocity)[localOffset + j];
          //if(j%3==2){
          (*globalForceVector)[localOffset + j] +=
            dt * (objectMass[i] * gravityVector[j%3] + objectLinForce[i][j]);
            //}
        }

        Vector4<T> lastVelocity;

        if(objectExternalVelocity[i] == 1){
          /*Object has an external fixed velocity*/
          lastVelocity = objectLinVelocity[i];

          for(int j=0;j<3;j++){
            (*globalForceVector)[localOffset + j] =
              objectMass[i] * lastVelocity[j];
          }
        }

        /*Set external torque for rb 1*/
        if(i==0){
          //(*globalForceVector)[3] -= dt * 100.0;
          //(*globalForceVector)[4] -= dt * 100.0;
          //(*globalForceVector)[5] -= dt * 100.0;
        }

        /*Set inetria tensor*/
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            (*gm)[localOffset + 3 + j][localOffset + 3 + k] =
              inertiaTensors[i][j][k];
          }
          (*gm)[localOffset + 3 + j][localOffset + 3 + j] += dt * rbd;
        }

        /*Set external rotational force*/
        for(int j=0;j<3;j++){
          //(*globalForceVector)[offset + 3 + j] =
          //1.0 * (*globalLastVelocity)[offset + 3 + j];
          lastVelocity[j] = (*globalLastVelocity)[localOffset + 3 + j];
        }

        if(objectExternalVelocity[i] == 1){
          /*Object has en external fixed velocity*/
          lastVelocity = objectAngVelocity[i];
        }

#if 0
#if 0
        if(objectAngVelocity[i].length() > 1e-6){
          lastVelocity = objectAngVelocity[i];
        }
#else
        if(i==0){
          lastVelocity[0] = 5;
          //lastVelocity[1] = 0;
          //lastVelocity[2] = 0;

          //(*globalForceVector)[offset + 3] += 0.01;
        }
        if(i==1){
          lastVelocity[0] = 1;
          //lastVelocity[1] = 0;
          //lastVelocity[2] = 0;
          //(*globalForceVector)[offset + 3] += 0.001;
        }
#endif
#endif
        Vector4<T> localRotationalForce = inertiaTensors[i] * lastVelocity;

        for(int j=0;j<3;j++){
          //(*globalForceVector)[offset + 3 + j] =
          //1.0 * (*globalLastVelocity)[offset + 3 + j];
          /*Should be multiplied with dt*/
          (*globalForceVector)[localOffset + 3 + j] =
            localRotationalForce[j] + dt * objectAngForce[i][j];
        }
      }

      PRINT(*globalForceVector);
    }

    void resume(){
      /*Load double precision state*/
      message("resuming from step %d", tstep-1);
      loadDPState(tstep-1);
      message("State loaded");
#if 1
      /*Restore last velocity*/

      for(int i=0;i<nDeformableVertices;i++){
        (*globalCurrentPositions)[i*3+0] = mesh->vertices[i].coord[0];
        (*globalCurrentPositions)[i*3+1] = mesh->vertices[i].coord[1];
        (*globalCurrentPositions)[i*3+2] = mesh->vertices[i].coord[2];

        (*globalTotalDisplacement)[i*3+0] =
          mesh->vertices[i].coord[0] - (*globalInitialPositions)[i*3+0];

        (*globalTotalDisplacement)[i*3+1] =
          mesh->vertices[i].coord[1] - (*globalInitialPositions)[i*3+1];

        (*globalTotalDisplacement)[i*3+2] =
          mesh->vertices[i].coord[2] - (*globalInitialPositions)[i*3+2];
      }
      mesh->computeCentersOfMass();

      computeInertiaTensors();

      ctx->mesh->computeDisplacement(ctx->v, dt);
      //ctx->initializeBBoxes();
      ctx->updateBBoxes();

      //(*globalCurrentPositions)  += (*velocity) * dt;
      //(*globalTotalDisplacement) += (*velocity) * dt;

      //std::cout << *velocity;

      /*Update mesh using current velocity*/
      //mesh->computeDisplacement(velocity, dt);
      //mesh->acceptDisplacement();

      /*Update (extended) bounding volumes*/
      //ctx->stree->update(globalLastVelocity, dt);

      //mesh->computeDisplacement(velocity, dt);
      //mesh->acceptDisplacement();

      /*Update (extended) bounding volumes*/
      ctx->stree->update(globalLastVelocity, dt);

      /*Unfortunately we have to loop over all frames*/
      for(int i=0;i<tstep;i++){
        updateConstraints(i);
      }
#endif
    }

    void resume2(){
      /*Load double precision state*/
      message("resuming from step %d", tstep-1);
      loadDPState(tstep-1+1800*0);
      message("State loaded");
#if 1
      /*Restore last velocity*/

      for(int i=0;i<nDeformableVertices;i++){
        (*globalCurrentPositions)[i*3+0] = mesh->vertices[i].coord[0];
        (*globalCurrentPositions)[i*3+1] = mesh->vertices[i].coord[1];
        (*globalCurrentPositions)[i*3+2] = mesh->vertices[i].coord[2];

        (*globalTotalDisplacement)[i*3+0] =
          mesh->vertices[i].coord[0] - (*globalInitialPositions)[i*3+0];

        (*globalTotalDisplacement)[i*3+1] =
          mesh->vertices[i].coord[1] - (*globalInitialPositions)[i*3+1];

        (*globalTotalDisplacement)[i*3+2] =
          mesh->vertices[i].coord[2] - (*globalInitialPositions)[i*3+2];
      }
      mesh->computeCentersOfMass();

      computeInertiaTensors();

      ctx->mesh->computeDisplacement(ctx->v, dt);
      //ctx->initializeBBoxes();
      ctx->updateBBoxes();

      //(*globalCurrentPositions)  += (*velocity) * dt;
      //(*globalTotalDisplacement) += (*velocity) * dt;

      //std::cout << *velocity;

      /*Update mesh using current velocity*/
      //mesh->computeDisplacement(velocity, dt);
      //mesh->acceptDisplacement();

      /*Update (extended) bounding volumes*/
      //ctx->stree->update(globalLastVelocity, dt);

      //mesh->computeDisplacement(velocity, dt);
      //mesh->acceptDisplacement();

      /*Update (extended) bounding volumes*/
      ctx->stree->update(globalLastVelocity, dt);

      /*Unfortunately we have to loop over all frames*/
      for(int i=1;i<tstep;i++){
        updateConstraints(i);
      }

#endif
    }

    void loadDPState(int t){
#if 1
      char buffer[256];
      sprintf(buffer, "dpstate_%d.tet2d", t);
      message("opening %s", buffer);
      FILE* file = fopen(buffer, "r");

      Vector<T>* velocity = (Vector<T>*)globalLastVelocity;

      message("%d vertices", mesh->getNVertices());
      for(int i=0;i<mesh->getNVertices();i++){
        if(mesh->vertices[i].type == Mesh::Static){
          //Skip static vertices
          continue;
        }

        double xx, yy, zz, vx, vy, vz, vrx, vry, vrz;

        char* pp;
        pp = (char*)&xx;

        for(uint j=0;j<sizeof(double);j++){
          pp[j] = (char)fgetc(file);
        }

        pp = (char*)&yy;
        for(uint j=0;j<sizeof(double);j++){
          pp[j] = (char)fgetc(file);
        }

        pp = (char*)&zz;
        for(uint j=0;j<sizeof(double);j++){
          pp[j] = (char)fgetc(file);
        }

        pp = (char*)&vx;
        for(uint j=0;j<sizeof(double);j++){
          pp[j] = (char)fgetc(file);
        }

        pp = (char*)&vy;
        for(uint j=0;j<sizeof(double);j++){
          pp[j] = (char)fgetc(file);
        }

        pp = (char*)&vz;
        for(uint j=0;j<sizeof(double);j++){
          pp[j] = (char)fgetc(file);
        }

        if(mesh->vertices[i].type == Mesh::Rigid){
          pp = (char*)&vrx;
          for(uint j=0;j<sizeof(double);j++){
            pp[j] = (char)fgetc(file);
          }

          pp = (char*)&vry;
          for(uint j=0;j<sizeof(double);j++){
            pp[j] = (char)fgetc(file);
          }

          pp = (char*)&vrz;
          for(uint j=0;j<sizeof(double);j++){
            pp[j] = (char)fgetc(file);
          }
        }

        mesh->vertices[i].coord[0] = zz/3.0;
        mesh->vertices[i].coord[1] = xx/3.0;
        mesh->vertices[i].coord[2] = yy/3.0;

        mesh->vertices[i].displCoord[0] = mesh->vertices[i].coord[0];
        mesh->vertices[i].displCoord[1] = mesh->vertices[i].coord[1];
        mesh->vertices[i].displCoord[2] = mesh->vertices[i].coord[2];

        mesh->vertices[i].displCoordExt[0] = mesh->vertices[i].coord[0];
        mesh->vertices[i].displCoordExt[1] = mesh->vertices[i].coord[1];
        mesh->vertices[i].displCoordExt[2] = mesh->vertices[i].coord[2];


        int velIndex = mesh->vertices[i].vel_index;

        message("index = %d, size = %d, vertices = %d", velIndex, vectorSize,
                mesh->getNVertices());

        (*velocity)[velIndex+0] = vz;
        (*velocity)[velIndex+1] = vx;
        (*velocity)[velIndex+2] = vy;

        if(mesh->vertices[i].type == Mesh::Rigid){
          (*velocity)[velIndex+3] = vrz;
          (*velocity)[velIndex+4] = vrx;
          (*velocity)[velIndex+5] = vry;
        }
      }
      fclose(file);
#endif
    }

    void saveState(int t){
      char buffer[256];
      sprintf(buffer, "out_%d.ply", t);
      FILE *file = fopen(buffer, "w");
      save_ply(buffer, mesh);

      while(true){
        bool error = false;
        char buffer[256];

        /*Save single precision file*/
        sprintf(buffer, "out_%d.tet2", t);
        FILE* file = fopen(buffer, "w");

        for(int i=0;i<mesh->getNVertices();i++){
          if(mesh->vertices[i].type == Mesh::Static){
            //Skip static vertices
            continue;
          }

          float xx, yy, zz;
          zz = (float)(3.0*(mesh->vertices[i].coord[0]));
          xx = (float)(3.0*(mesh->vertices[i].coord[1]));
          yy = (float)(3.0*(mesh->vertices[i].coord[2]));

          char* pp;
          pp = (char*)&xx;

          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }

          pp = (char*)&yy;
          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }

          pp = (char*)&zz;
          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }
        }

        fclose(file);

        /*Save double precision state, used for recovery*/
        if(t % 10 == 0){
          sprintf(buffer, "dpstate_%d.tet2d", t);
          file = fopen(buffer, "w");

          Vector<T>* velocity = (Vector<T>*)globalLastVelocity;

          for(int i=0;i<mesh->getNVertices();i++){
            if(mesh->vertices[i].type == Mesh::Static){
              //Skip static vertices
              continue;
            }

            double xx, yy, zz, vx, vy, vz, vrx, vry, vrz;
            zz = (double)(3.0*(mesh->vertices[i].coord[0]));
            xx = (double)(3.0*(mesh->vertices[i].coord[1]));
            yy = (double)(3.0*(mesh->vertices[i].coord[2]));

            int velIndex = mesh->vertices[i].vel_index;

            if(mesh->vertices[i].type == Mesh::Rigid){
              vz = (double)(*velocity)[velIndex+0];
              vx = (double)(*velocity)[velIndex+1];
              vy = (double)(*velocity)[velIndex+2];
              vrz = (double)(*velocity)[velIndex+3];
              vrx = (double)(*velocity)[velIndex+4];
              vry = (double)(*velocity)[velIndex+5];
            }else{
              vz = (double)(*velocity)[velIndex+0];
              vx = (double)(*velocity)[velIndex+1];
              vy = (double)(*velocity)[velIndex+2];
            }

            char* pp;
            pp = (char*)&xx;

            for(uint j=0;j<sizeof(double);j++){
              fputc(pp[j], file);
              if(ferror(file)){
                error = true;
              }
            }

            pp = (char*)&yy;
            for(uint j=0;j<sizeof(double);j++){
              fputc(pp[j], file);
              if(ferror(file)){
                error = true;
              }
            }

            pp = (char*)&zz;
            for(uint j=0;j<sizeof(double);j++){
              fputc(pp[j], file);
              if(ferror(file)){
                error = true;
              }
            }

            pp = (char*)&vx;
            for(uint j=0;j<sizeof(double);j++){
              fputc(pp[j], file);
              if(ferror(file)){
                error = true;
              }
            }

            pp = (char*)&vy;
            for(uint j=0;j<sizeof(double);j++){
              fputc(pp[j], file);
              if(ferror(file)){
                error = true;
              }
            }

            pp = (char*)&vz;
            for(uint j=0;j<sizeof(double);j++){
              fputc(pp[j], file);
              if(ferror(file)){
                error = true;
              }
            }

            if(mesh->vertices[i].type == Mesh::Rigid){
              pp = (char*)&vrx;
              for(uint j=0;j<sizeof(double);j++){
                fputc(pp[j], file);
                if(ferror(file)){
                  error = true;
                }
              }

              pp = (char*)&vry;
              for(uint j=0;j<sizeof(double);j++){
                fputc(pp[j], file);
                if(ferror(file)){
                  error = true;
                }
              }

              pp = (char*)&vrz;
              for(uint j=0;j<sizeof(double);j++){
                fputc(pp[j], file);
                if(ferror(file)){
                  error = true;
                }
              }
            }
          }

          fclose(file);
        }

        if(!error){
          return;
        }else{
          warning("Could not properly write states to a file, disk fulll?");
          warning("Hit enter to retry, after making some space");
          getchar();
          CGF::usleep(10000000);
        }
      }
    }

#if 0
    bool VolumePenaltySolve(){
      bool changed = false;
      for(int i=0;i<mesh->getNTetrahedra();i++){
        int vertices[4];
        mesh->tetrahedra[i].getIndices(vertices);

        //Matrix44<T> A = tetdata[i].lastPos.transpose();
        Matrix44<T> B = tetdata[i].c.transpose();
        //Matrix44<T> C;

        /*Check edge deformation*/
        for(int j=0;j<6;j++){
          Vector4<T> normal = (B[edgeEdge[j][0]] - B[edgeEdge[j][1]]).normalize();
          Vector4<T> edgeA  = (B[edgeEdge[j][0]] - B[edgeEdge[j][1]]);

          T dist = dot(normal, edgeA);
          if(dist < tetdata[i].initialEdgeDistances[j] * (T)0.2){
            message("edge collapseB");
            message("current dist = %10.10e, initial = %10.10e",
                    dist, tetdata[i].initialEdgeDistances[j]);

            EdgeForce<T> ef(B[edgeEdge[j][0]],
                            B[edgeEdge[j][1]],
                            normal,
                            tetdata[i].initialEdgeDistances[j] * (T)0.2001,
                            vertices[edgeEdge[j][0]],
                            vertices[edgeEdge[j][1]]);

            bool remove = false;
            ef.computeForcevector(globalForceVector, gm, dt, true, &remove);
            changed = true;
          }
        }

        T largestCross = 0;
        int largestIndex = -1;
        T dist = (B.det()/(T)6.0)/largestCross;

        /*Check altitude edge edge*/
        for(int j=0;j<3;j++){
          int edgeA = edgeEdgePair[j][0];
          int edgeB = edgeEdgePair[j][1];

          Vector4<T> eA = B[edgeEdge[edgeA][0]] - B[edgeEdge[edgeA][1]];
          Vector4<T> eB = B[edgeEdge[edgeB][0]] - B[edgeEdge[edgeB][1]];

          T denom = cross(eA, eB).length();

          if(denom > largestCross){
            largestCross = denom;
            largestIndex = j;

            TriangleEdge<T> edge1(B[edgeEdge[edgeA][0]], B[edgeEdge[edgeA][1]]);
            TriangleEdge<T> edge2(B[edgeEdge[edgeB][0]], B[edgeEdge[edgeB][1]]);

            Vector4<T> normal =
              cross(B[edgeEdge[edgeA][0]] - B[edgeEdge[edgeA][1]],
                    B[edgeEdge[edgeB][0]] - B[edgeEdge[edgeB][1]]).normalize();

            dist = edge1.getSignedDistance(edge2, 0, 0, 0, 0, &normal);
          }
        }
        /*Check altitude vertexFace*/
        for(int j=0;j<4;j++){
          Vector4<T> v0 = B[vertexFace[j][1]] - B[vertexFace[j][0]];
          Vector4<T> v1 = B[vertexFace[j][2]] - B[vertexFace[j][0]];

          T denom = cross(v0, v1).length();

          if(denom > largestCross){
            largestCross = denom;
            largestIndex = j+3;

            Triangle<T> tri(B[vertexFace[j][0]],
                            B[vertexFace[j][1]],
                            B[vertexFace[j][2]]);

            dist = tri.getSignedDistance(B[j]);
          }
        }

        if(dist < tetdata[i].initialSpringDistances[largestIndex]*(T)0.2){
#if 0
          if(A.det() > 0.0 && B.det() < 0.0){
            int inters = intersection(B, A, C, 0.0);
            if(inters){
              C = C;
            }else{

              C = B;
            }

          }else{
            C = (B+A)*(T)0.5;
          }

          B = C;
#endif

          message(" dist = %10.10e", dist);
          message("idist = %10.10e",
                  tetdata[i].initialSpringDistances[largestIndex]);
          message(" cross = %10.10e", largestCross);
          message("near collapse %d", largestIndex);

          message("volume = %10.10e", B.det()/(T)6.0);

          if(largestIndex  < 3){
            //edgeEdge case
            int ee = largestIndex;
            int edgeA = edgeEdgePair[ee][0];
            int edgeB = edgeEdgePair[ee][1];

            //Vector4<T> eA = B[edgeEdge[edgeA][0]] - B[edgeEdge[edgeA][1]];
            //Vector4<T> eB = B[edgeEdge[edgeB][0]] - B[edgeEdge[edgeB][1]];

            //int eA = tetdata[i].constrainedA;
            //int eB = tetdata[i].constrainedB;

            int indices[4];

            indices[0] = vertices[edgeEdge[edgeA][0]];
            indices[1] = vertices[edgeEdge[edgeA][1]];
            indices[2] = vertices[edgeEdge[edgeB][0]];
            indices[3] = vertices[edgeEdge[edgeB][1]];

            Matrix44<T> edgeVertices;
            edgeVertices[0] = B[edgeEdge[edgeA][0]];
            edgeVertices[1] = B[edgeEdge[edgeA][1]];
            edgeVertices[2] = B[edgeEdge[edgeB][0]];
            edgeVertices[3] = B[edgeEdge[edgeB][1]];

            TriangleEdge<T> eA(edgeVertices[0], edgeVertices[1]);
            TriangleEdge<T> eB(edgeVertices[2], edgeVertices[3]);

            Vector4<T> p1, p2;
            Vector4<T> b1, b2;

            Vector4<T> normal = cross(edgeVertices[0] - edgeVertices[1],
                                      edgeVertices[2] - edgeVertices[3]);
            normal.normalize();

            message("dump %d, %d, %d", ee, edgeA, edgeB);
            for(int j=0;j<4;j++){
              message("index = %d", indices[j]);
              std::cout << edgeVertices[j] << std::endl;
            }

            std::cout << normal << std::endl;

            EdgeEdgeForce<T> ef(edgeVertices,
                                normal,
                                tetdata[i].bary,
                                tetdata[i].initialSpringDistances[largestIndex]*(T)0.2001,
                                indices);
            bool remove = false;
            ef.computeForcevector(globalForceVector, gm, dt, true, &remove);
            //getchar();
            changed = true;
          }else{
            //vertex face case
            int vf = largestIndex - 3;
            Triangle<T> tri(B[vertexFace[vf][0]],
                            B[vertexFace[vf][1]],
                            B[vertexFace[vf][2]]);
            Vector4<T> vertex = B[vf];

            Vector4<T> bary = tri.getBarycentricCoordinates(vertex);

            int indices[4];
            indices[0] = vertices[vertexFace[vf][0]];
            indices[1] = vertices[vertexFace[vf][1]];
            indices[2] = vertices[vertexFace[vf][2]];
            indices[3] = vertices[vf];

            Matrix44<T> vertexFaceVertices;
            vertexFaceVertices[0] = B[vertexFace[vf][0]];
            vertexFaceVertices[1] = B[vertexFace[vf][1]];
            vertexFaceVertices[2] = B[vertexFace[vf][2]];
            vertexFaceVertices[3] = B[vf];

            VertexFaceForce<T> vff(vertexFaceVertices,
                                   tri.n,
                                   bary,
                                   tetdata[i].initialSpringDistances[largestIndex]*(T)0.2001,
                                   indices);

            bool remove = false;
            vff.computeForcevector(globalForceVector, gm, dt, true, &remove);
            //getchar();
            changed = true;
          }


          //getchar();
        }
      }
      return changed;
    }
#endif

    void simulate(){
      int localStep = 0;
      for(tstep=tstep;true;tstep++){
        if(tstep == (16750 + 200)){
          //return;
        }
        computeVolumes(false);

        gm->clear();

        globalForceVector->clear();

        FEMSolve();  /*Setup matrices for FEM sim*/

        RBSolve();   /*Setup matrices for Rigid Body sim*/

        updateConstraints(tstep);

        gm->finalize();

        if(localStep == 0){
          /*If this is the first iteration due to a recovery, notify
            the solver.*/
          solver->setFirstIteration(true);
        }else{
          solver->setFirstIteration(false);
        }

        solver->solve(1000000, (T)5e-6);

        FEMUpdate(); /*Apply velocity*/

        /*Save updated configuration*/
        saveState(tstep);

        exporter->saveRow();

        //tstep++;
        localStep++;
        if(localStep > 0){
          //break;
        }

        if(tstep == 7177){
          //break;
        }
      }
    }

#if 1



    void computeInitialMass(){
      MatrixT<12, 1, T> dummy;

      for(int i=0;i<mesh->getNTetrahedra();i++){
        tetdata[i].mass = tetdata[i].initialVolume * density;
        tetdata[i].lastRotation = Matrix44<T>::identity();
        mesh->tetrahedra[i].getIndices(tetdata[i].indices);

        FEM->computeElementStiffnessMatrixAndForce(tetdata[i].initialK,
                                                   dummy,
                                                   dummy,
                                                   dummy,
                                                   tetdata[i],
                                                   i, dt, true, false);

        //createLocalStiffnessMatrix(tetdata[i].initialK, i,
        //                           Matrix44<T>::identity());
      }

      for(int i=0;i<nDeformableVertices;i++){
        Vector4<T> pos = mesh->vertices[i].coord;
        Vector4<T> pos2 = pos;
        //pos2[0] = (2.0*genRand<T>() - 1.0)*2.75;
        //pos2[1] = (2.0*genRand<T>() - 1.0)*2.75;
        //pos2[2] = (2.0*genRand<T>() - 1.0)*2.75;

        //pos2[0] += (2.0*genRand<T>() - 1.0)*0.075;
        //pos2[1] += (2.0*genRand<T>() - 1.0)*0.075;
        //pos2[2] += (2.0*genRand<T>() - 1.0)*0.075;

        //pos2[0] = pos2[1] = pos2[2] = (T)0.0;

        mesh->vertices[i].displCoord = mesh->vertices[i].displCoordExt =
          mesh->vertices[i].coord = pos2;

        for(int j=0;j<3;j++){
          (*globalInitialPositions)[i*3+j] = pos[j];
          (*globalCurrentPositions)[i*3+j] = pos2[j];
        }
      }
    }
#endif

    void computeVolumes(bool updateInitial){
      T totalDifference = 0.0;

      T largestVolume = (T)-1e+10;
      T smallestVolume = (T)1e+10;
      T averageVolume = (T)0.0;
      T volumeVar = (T)0.0;

      for(int i=0;i<mesh->getNTetrahedra();i++){
        tetdata[i].lastPos = tetdata[i].c;
        Matrix44<T>* c = &(tetdata[i].c);

        mesh->getTetrahedronCoords(i, *c);

        *c = c->transpose();

        tetdata[i].volume = c->det()/(T)6.0;

        if(updateInitial){
          tetdata[i].initialPos    = *c;
          tetdata[i].lastPos       = *c;
          tetdata[i].initialVolume = tetdata[i].volume;

          if(tetdata[i].volume > largestVolume){
            largestVolume = tetdata[i].volume;
          }
          if(tetdata[i].volume < smallestVolume){
            smallestVolume = tetdata[i].volume;
          }
          averageVolume += tetdata[i].volume;

#if 0
          tetdata[i].volumeInverted[0] =
            tetdata[i].volumeInverted[1] =
            tetdata[i].volumeInverted[2] =
            tetdata[i].volumeInverted[3] = false;


          tetdata[i].penaltyStiffness = (T)10.0;

          Matrix44<T> A = tetdata[i].lastPos.transpose();

          /*Compute initial edge distances*/
          for(int j=0;j<6;j++){
            tetdata[i].initialEdgeDistances[j] =
              (A[edgeEdge[j][0]] - A[edgeEdge[j][1]]).length();
          }

          /*Compute initial altitude distances*/
          for(int j=0;j<3;j++){
            int edgeA = edgeEdgePair[j][0];
            int edgeB = edgeEdgePair[j][1];
            Vector4<T> eA = A[edgeEdge[edgeA][0]] - A[edgeEdge[edgeA][1]];
            Vector4<T> eB = A[edgeEdge[edgeB][0]] - A[edgeEdge[edgeB][1]];

            tetdata[i].initialAltitudeDistances[j] =
              (c->det()/(T)6.0) / cross(eA, eB).length();

            TriangleEdge<T> edge1(A[edgeEdge[edgeA][0]],
                                  A[edgeEdge[edgeA][1]]);
            TriangleEdge<T> edge2(A[edgeEdge[edgeB][0]],
                                  A[edgeEdge[edgeB][1]]);

            Vector4<T> normal = cross(eA, eB);
            normal.normalize();

            tetdata[i].initialSpringDistances[j] =
              edge1.getSignedDistance(edge2, 0, 0, 0, 0, &normal);

            if(tetdata[i].initialSpringDistances[j] < 0.0){
              error("negative initializer");
            }
          }

          for(int j=0;j<4;j++){
            Vector4<T> v0 = A[vertexFace[j][1]] - A[vertexFace[j][0]];
            Vector4<T> v1 = A[vertexFace[j][2]] - A[vertexFace[j][0]];

            tetdata[i].initialAltitudeDistances[j+3] =
              (c->det()/(T)6.0) / cross(v0, v1).length();

            Triangle<T> tri(A[vertexFace[j][0]],
                            A[vertexFace[j][1]],
                            A[vertexFace[j][2]]);

            tetdata[i].initialSpringDistances[j+3] =
              tri.getSignedDistance(A[j]);
            if(tetdata[i].initialSpringDistances[j] < 0.0){
              error("negative initializer");
            }
          }
#endif
        }

        totalDifference += tetdata[i].volume - tetdata[i].initialVolume;

        if(tetdata[i].volume <= (T)0.0){
          warning("Negative volume found %10.10e", tetdata[i].volume);
          message("Negative volume found %10.10e", tetdata[i].volume);
        }
      }

      if(updateInitial){
        averageVolume /= (T)mesh->getNTetrahedra();
      }

      for(int i=0;i<mesh->getNTetrahedra();i++){
        volumeVar += Sqr(tetdata[i].volume - averageVolume);
      }

      message("Total difference = %10.10e", totalDifference);

      if(updateInitial){
        volumeVar /= (T)mesh->getNTetrahedra();
        message("SmallestVolume = %10.10e", smallestVolume);
        message("LargestVolume  = %10.10e", largestVolume);
        message("AverageVolume  = %10.10e", averageVolume);
        message("VolumeSDev     = %10.10e", Sqrt(volumeVar));
        getchar();
      }
    }

    void setExporter(CSVExporter* exp){
      exporter = exp;
    }

    void setConditionExporter(CSVExporter* exp){
      conditionExporter = exp;
    }

    void updateConstraints(int step){
      PRINT_FUNCTION;
      FECIterator it = fEqConstraints.begin();

      /*Each equality constraint acts on one vertex*/

      while(it != fEqConstraints.end()){
        FixedEqualityConstraint* eq = *it++;

        if(eq->descriptor.startFrame == step){
          /*Constraint should fix from now on, set current position*/
          if(mesh->vertices[eq->vertexId].type == Mesh::Rigid ||
             mesh->vertices[eq->vertexId].type == Mesh::Deformable){
            Vector4<T> cpos = mesh->vertices[eq->vertexId].coord;
            eq->pos = cpos;
            eq->constraint1->status = Active;
            eq->constraint2->status = Active;
            eq->constraint3->status = Active;
          }
        }

        if(eq->descriptor.type == ConstraintDescriptor<T>::Fixed){
          /*Fixed constraint*/
          message("Fixed constraint");
          if(eq->descriptor.objectId[0] != -1){
            /*Fixed rigid object*/
            message("Fixed object");
            error("Not implemented yet");
          }else{
            message("Fixed vertex");

            /*Is vertex a rigid vertex or deformable vertex?*/
            if(mesh->vertices[eq->vertexId].type == Mesh::Rigid){
              /*Rigid vertex*/
              message("Rigid vertex");
              int objectId    = mesh->vertexObjectMap[eq->vertexId];
              Vector4<T> com  = mesh->centerOfMass[objectId];
              Vector4<T> cpos = mesh->vertices[eq->vertexId].coord;

              if(eq->descriptor.interpolated[0]){
                message("Interpolated vertex");
                cpos = eq->descriptor.p[0];
              }

              int velIndex    = mesh->vertices[eq->vertexId].vel_index;

              Vector4<T> r    = cpos - com;
              Vector4<T> rhs  = eq->pos - cpos;//(com + r);

              Vector4<T> normal(1,0,0,0);
              Vector4<T> arm  = cross(r, normal);

              eq->constraint1->setValue(velIndex+0, dt);
              eq->constraint1->setValue(velIndex+3, arm[0]*dt);
              eq->constraint1->setValue(velIndex+4, arm[1]*dt);
              eq->constraint1->setValue(velIndex+5, arm[2]*dt);
              eq->constraint1->getConstraintValue(0) = rhs[0];

              normal.set(0,1,0,0);
              arm  = cross(r, normal);

              eq->constraint2->setValue(velIndex+1, dt);
              eq->constraint2->setValue(velIndex+3, arm[0]*dt);
              eq->constraint2->setValue(velIndex+4, arm[1]*dt);
              eq->constraint2->setValue(velIndex+5, arm[2]*dt);
              eq->constraint2->getConstraintValue(0) = rhs[1];

              normal.set(0,0,1,0);
              arm  = cross(r, normal);

              eq->constraint3->setValue(velIndex+2, dt);
              eq->constraint3->setValue(velIndex+3, arm[0]*dt);
              eq->constraint3->setValue(velIndex+4, arm[1]*dt);
              eq->constraint3->setValue(velIndex+5, arm[2]*dt);
              eq->constraint3->getConstraintValue(0) = rhs[2];

              if(step == 0){
                eq->cid[0] = solver->addConstraint(eq->constraint1);
                eq->cid[1] = solver->addConstraint(eq->constraint2);
                eq->cid[2] = solver->addConstraint(eq->constraint3);
              }
            }else{
              /*Deformable vertex*/
              message("Deformable vertex");
              int velIndex    = mesh->vertices[eq->vertexId].vel_index;

              Vector4<T> cpos = mesh->vertices[eq->vertexId].coord;
              Vector4<T> rhs  = eq->pos - cpos;

              Vector4<T> normal(1,0,0,0);

              eq->constraint1->setValue(velIndex+0, normal[0]*dt);
              eq->constraint1->setValue(velIndex+1, normal[1]*dt);
              eq->constraint1->setValue(velIndex+2, normal[2]*dt);
              eq->constraint1->getConstraintValue(0) = rhs[0];

              normal.set(0,1,0,0);

              eq->constraint2->setValue(velIndex+0, normal[0]*dt);
              eq->constraint2->setValue(velIndex+1, normal[1]*dt);
              eq->constraint2->setValue(velIndex+2, normal[2]*dt);
              eq->constraint2->getConstraintValue(0) = rhs[1];

              normal.set(0,0,1,0);

              eq->constraint3->setValue(velIndex+0, normal[0]*dt);
              eq->constraint3->setValue(velIndex+1, normal[1]*dt);
              eq->constraint3->setValue(velIndex+2, normal[2]*dt);
              eq->constraint3->getConstraintValue(0) = rhs[2];

              if(step == 0){
                eq->cid[0] = solver->addConstraint(eq->constraint1);
                eq->cid[1] = solver->addConstraint(eq->constraint2);
                eq->cid[2] = solver->addConstraint(eq->constraint3);
              }
            }
          }
        }

        if((eq->descriptor.endFrame <= step && eq->descriptor.endFrame != -1)||
           (eq->descriptor.startFrame > step && eq->descriptor.startFrame != -1)){
          /*Delete constraints*/
          warning("disabling %d", eq->cid[0]);
          warning("disabling %d", eq->cid[1]);
          warning("disabling %d", eq->cid[2]);
          eq->constraint1->status = Inactive;
          eq->constraint2->status = Inactive;
          eq->constraint3->status = Inactive;
          continue;
        }
      }

      LECIterator lit = lEqConstraints.begin();

      /*Each linked equality constraint acts on two vertices*/

      while(lit != lEqConstraints.end()){
        LinkedEqualityConstraint* eq = *lit++;

        if(eq->descriptor.type == ConstraintDescriptor<T>::Linked){
          /*Linked constraint*/
          message("Linked constraint");

          if(eq->descriptor.objectId[0] != -1){
            /*Linked rigid object*/
            message("Linked object");
            error("Not implemented yet");
          }else{
            message("Linked vertex-face / edge-edge");

            /*Update positions based on their barycentric coordinates*/
            if(eq->type[0] == CVertex){
              eq->pos[0] = mesh->vertices[eq->vertexId[0][0]].coord;
            }else if(eq->type[0] == CEdge){
              TriangleEdge<T> edge(mesh->vertices[eq->vertexId[0][0]].coord,
                                   mesh->vertices[eq->vertexId[0][1]].coord);

              eq->pos[0] = edge.getCoordinates(eq->bary[0]);
            }else if(eq->type[0] == CFace){
              Triangle<T> tri(mesh->vertices[eq->vertexId[0][0]].coord,
                              mesh->vertices[eq->vertexId[0][1]].coord,
                              mesh->vertices[eq->vertexId[0][2]].coord);

              eq->pos[0] = tri.getCoordinates(eq->bary[0]);
            }

            if(eq->type[1] == CVertex){
              eq->pos[1] = mesh->vertices[eq->vertexId[1][0]].coord;
            }else if(eq->type[1] == CEdge){
              TriangleEdge<T> edge(mesh->vertices[eq->vertexId[1][0]].coord,
                                   mesh->vertices[eq->vertexId[1][1]].coord);

              eq->pos[1] = edge.getCoordinates(eq->bary[1]);
            }else if(eq->type[1] == CFace){
              Triangle<T> tri(mesh->vertices[eq->vertexId[1][0]].coord,
                              mesh->vertices[eq->vertexId[1][1]].coord,
                              mesh->vertices[eq->vertexId[1][2]].coord);

              eq->pos[1] = tri.getCoordinates(eq->bary[1]);
            }


            if(mesh->vertices[eq->vertexId[0][0]].type == Mesh::Rigid &&
               mesh->vertices[eq->vertexId[1][0]].type == Mesh::Rigid){
              message("Rigid vertices");

              int objectId1    = mesh->vertexObjectMap[eq->vertexId[0][0]];
              Vector4<T> com1  = mesh->centerOfMass[objectId1];
              Vector4<T> cpos1 = eq->pos[0];

              int objectId2    = mesh->vertexObjectMap[eq->vertexId[1][0]];
              Vector4<T> com2  = mesh->centerOfMass[objectId2];
              Vector4<T> cpos2 = eq->pos[1];

              /*Link vertices to a virtual vertex vp on the center of
                the virtual edge between p1 and p2.*/

              Vector4<T> vp = (cpos1 + cpos2)/(T)2.0;
              Vector4<T> edge = cpos2 - cpos1;

              Vector4<T> nEdge = edge;
              nEdge.normalize();
              nEdge *= eq->initialDistance;
              PRINT(nEdge.length());
              nEdge -= edge;
              PRINT(nEdge.length());
              nEdge *= -1;

              if(eq->descriptor.interpolated[0]){
                message("Interpolated vertex0");
                cpos1 = eq->descriptor.p[0];
                error("Interpolated vertex not supported yet");
              }

              if(eq->descriptor.interpolated[1]){
                message("Interpolated vertex1");
                cpos2 = eq->descriptor.p[1];
                error("Interpolated vertex not supported yet");
              }

              /*Velocity indices for vertices belonging to the same
                rigid object are the same.*/
              int velIndex1    = mesh->vertices[eq->vertexId[0][0]].vel_index;
              int velIndex2    = mesh->vertices[eq->vertexId[1][0]].vel_index;

              PRINT(velIndex1);
              PRINT(velIndex2);

              Vector4<T> r1    = vp - com1;
              Vector4<T> r2    = vp - com2;

              PRINT(r1);
              PRINT(r2);

              //Vector4<T> rhs1  = eq->pos[0] - cpos1;//(com + r);
              //Vector4<T> rhs2  = eq->pos[1] - cpos2;//(com + r);
              Vector4<T> rhs = nEdge;

              Vector4<T> n1(1,0,0,0);
              Vector4<T> n2(0,1,0,0);
              Vector4<T> n3(0,0,1,0);

              Vector4<T> arm1  = cross(r1, n1);
              Vector4<T> arm2  = cross(r2, n1);

              Vector4<T> arm11 = arm1;
              Vector4<T> arm12 = arm2;

              eq->constraint1->setValue(velIndex1+0, dt);
              eq->constraint1->setValue(velIndex1+3, arm11[0]*dt);
              eq->constraint1->setValue(velIndex1+4, arm11[1]*dt);
              eq->constraint1->setValue(velIndex1+5, arm11[2]*dt);
              eq->constraint1->setValue(velIndex2+0, -dt);
              eq->constraint1->setValue(velIndex2+3, -arm12[0]*dt);
              eq->constraint1->setValue(velIndex2+4, -arm12[1]*dt);
              eq->constraint1->setValue(velIndex2+5, -arm12[2]*dt);
              eq->constraint1->getConstraintValue(0) = rhs[0];

              arm1  = cross(r1, n2);
              arm2  = cross(r2, n2);

              Vector4<T> arm21 = arm1;
              Vector4<T> arm22 = arm2;

              eq->constraint2->setValue(velIndex1+1, dt);
              eq->constraint2->setValue(velIndex1+3, arm21[0]*dt);
              eq->constraint2->setValue(velIndex1+4, arm21[1]*dt);
              eq->constraint2->setValue(velIndex1+5, arm21[2]*dt);
              eq->constraint2->setValue(velIndex2+1, -dt);
              eq->constraint2->setValue(velIndex2+3, -arm22[0]*dt);
              eq->constraint2->setValue(velIndex2+4, -arm22[1]*dt);
              eq->constraint2->setValue(velIndex2+5, -arm22[2]*dt);
              eq->constraint2->getConstraintValue(0) = rhs[1];

              arm1  = cross(r1, n3);
              arm2  = cross(r2, n3);

              Vector4<T> arm31 = arm1;
              Vector4<T> arm32 = arm2;

              eq->constraint3->setValue(velIndex1+2, dt);
              eq->constraint3->setValue(velIndex1+3, arm31[0]*dt);
              eq->constraint3->setValue(velIndex1+4, arm31[1]*dt);
              eq->constraint3->setValue(velIndex1+5, arm31[2]*dt);
              eq->constraint3->setValue(velIndex2+2, -dt);
              eq->constraint3->setValue(velIndex2+3, -arm32[0]*dt);
              eq->constraint3->setValue(velIndex2+4, -arm32[1]*dt);
              eq->constraint3->setValue(velIndex2+5, -arm32[2]*dt);
              eq->constraint3->getConstraintValue(0) = rhs[2];

              PRINT(dot(arm11, arm21));
              PRINT(dot(arm21,  arm31));
              PRINT(dot(arm31,  arm11));

              PRINT(dot(arm12, arm22));
              PRINT(dot(arm22, arm32));
              PRINT(dot(arm32, arm12));

              PRINT(dot(n1, n2));
              PRINT(dot(n2, n3));
              PRINT(dot(n3, n1));

              if(step == 0){
                eq->cid[0] = solver->addConstraint(eq->constraint1);
                eq->cid[1] = solver->addConstraint(eq->constraint2);
                eq->cid[2] = solver->addConstraint(eq->constraint3);
              }
            }else if(mesh->vertices[eq->vertexId[0][0]].type != Mesh::Rigid &&
                     mesh->vertices[eq->vertexId[1][0]].type == Mesh::Rigid){
              message("Deformable / rigid vertices");

              //int objectId1    = mesh->vertexObjectMap[eq->vertexId[0][0]];
              //Vector4<T> com1  = mesh->centerOfMass[objectId1];
              Vector4<T> cpos1 = eq->pos[0];

              int objectId2    = mesh->vertexObjectMap[eq->vertexId[1][0]];
              Vector4<T> com2  = mesh->centerOfMass[objectId2];
              Vector4<T> cpos2 = eq->pos[1];

              /*Link vertices to a virtual vertex vp on the center of
                the virtual edge between p1 and p2.*/

              Vector4<T> vp = cpos1;
              Vector4<T> edge = cpos2 - cpos1;

              PRINT(cpos1);
              PRINT(cpos2);
              PRINT(edge);

              Vector4<T> nEdge = edge;
              nEdge.normalize();
              nEdge *= eq->initialDistance;
              PRINT(nEdge.length());
              nEdge -= edge;
              PRINT(nEdge.length());
              nEdge *= -1;

              if(eq->descriptor.interpolated[0]){
                message("Interpolated vertex0");
                cpos1 = eq->descriptor.p[0];
                error("Interpolated vertex not supported yet");
              }

              if(eq->descriptor.interpolated[1]){
                message("Interpolated vertex1");
                cpos2 = eq->descriptor.p[1];
                error("Interpolated vertex not supported yet");
              }

              /*Velocity indices for vertices belonging to the same
                rigid object are the same.*/
              int velIndex1[3];
              int velIndex2[3];

              for(int j=0;j<3;j++){
                if(eq->vertexId[0][j] != -1){
                  velIndex1[j] = mesh->vertices[eq->vertexId[0][j]].vel_index;
                }else{
                  velIndex1[j] = -1;
                }

                if(eq->vertexId[1][j] != -1){
                  velIndex2[j] = mesh->vertices[eq->vertexId[1][j]].vel_index;
                }else{
                  velIndex2[j] = -1;
                }
                PRINT(velIndex1[j]);
                PRINT(velIndex2[j]);
              }

              //Vector4<T> r1    = (cpos2 - cpos1).normalize();
              Vector4<T> r2    = vp - com2;

              //PRINT(r1);
              PRINT(r2);

              //Vector4<T> rhs1  = eq->pos[0] - cpos1;//(com + r);
              //Vector4<T> rhs2  = eq->pos[1] - cpos2;//(com + r);
              //std::cerr << nEdge;
              // getchar();

              Vector4<T> rhs = nEdge;

              Vector4<T> n1(1,0,0,0);
              Vector4<T> n2(0,1,0,0);
              Vector4<T> n3(0,0,1,0);


              Vector4<T> arm1  = n1;
              Vector4<T> arm2  = cross(r2, n1);

              Vector4<T> arm11 = arm1;
              Vector4<T> arm12 = arm2;

              if(eq->type[0] != CVertex){
                eq->constraint1->setValue(velIndex1[0]+0,
                                          arm11[0]*dt*eq->bary[0][0]);
                eq->constraint1->setValue(velIndex1[0]+1,
                                          arm11[1]*dt*eq->bary[0][0]);
                eq->constraint1->setValue(velIndex1[0]+2,
                                          arm11[2]*dt*eq->bary[0][0]);

                eq->constraint1->setValue(velIndex1[1]+0,
                                          arm11[0]*dt*eq->bary[0][1]);
                eq->constraint1->setValue(velIndex1[1]+1,
                                          arm11[1]*dt*eq->bary[0][1]);
                eq->constraint1->setValue(velIndex1[1]+2,
                                          arm11[2]*dt*eq->bary[0][1]);

                eq->constraint1->setValue(velIndex1[2]+0,
                                          arm11[0]*dt*eq->bary[0][2]);
                eq->constraint1->setValue(velIndex1[2]+1,
                                          arm11[1]*dt*eq->bary[0][2]);
                eq->constraint1->setValue(velIndex1[2]+2,
                                          arm11[2]*dt*eq->bary[0][2]);
              }else{
                eq->constraint1->setValue(velIndex1[0]+0, arm11[0]*dt);
                eq->constraint1->setValue(velIndex1[0]+1, arm11[1]*dt);
                eq->constraint1->setValue(velIndex1[0]+2, arm11[2]*dt);
              }
              eq->constraint1->setValue(velIndex2[0]+0, -dt);
              eq->constraint1->setValue(velIndex2[0]+3, -arm12[0]*dt);
              eq->constraint1->setValue(velIndex2[0]+4, -arm12[1]*dt);
              eq->constraint1->setValue(velIndex2[0]+5, -arm12[2]*dt);
              eq->constraint1->getConstraintValue(0) = rhs[0];

              arm1  = n2;
              arm2  = cross(r2, n2);

              Vector4<T> arm21 = arm1;
              Vector4<T> arm22 = arm2;

              if(eq->type[0] != CVertex){
                eq->constraint2->setValue(velIndex1[0]+0,
                                          arm21[0]*dt*eq->bary[0][0]);
                eq->constraint2->setValue(velIndex1[0]+1,
                                          arm21[1]*dt*eq->bary[0][1]);
                eq->constraint2->setValue(velIndex1[0]+2,
                                          arm21[2]*dt*eq->bary[0][2]);

                eq->constraint2->setValue(velIndex1[1]+0,
                                          arm21[0]*dt*eq->bary[0][0]);
                eq->constraint2->setValue(velIndex1[1]+1,
                                          arm21[1]*dt*eq->bary[0][1]);
                eq->constraint2->setValue(velIndex1[1]+2,
                                          arm21[2]*dt*eq->bary[0][2]);

                eq->constraint2->setValue(velIndex1[2]+0,
                                          arm21[0]*dt*eq->bary[0][0]);
                eq->constraint2->setValue(velIndex1[2]+1,
                                          arm21[1]*dt*eq->bary[0][1]);
                eq->constraint2->setValue(velIndex1[2]+2,
                                          arm21[2]*dt*eq->bary[0][2]);
              }else{
                eq->constraint2->setValue(velIndex1[0]+0, arm21[0]*dt);
                eq->constraint2->setValue(velIndex1[0]+1, arm21[1]*dt);
                eq->constraint2->setValue(velIndex1[0]+2, arm21[2]*dt);
              }

              eq->constraint2->setValue(velIndex2[0]+1, -dt);
              eq->constraint2->setValue(velIndex2[0]+3, -arm22[0]*dt);
              eq->constraint2->setValue(velIndex2[0]+4, -arm22[1]*dt);
              eq->constraint2->setValue(velIndex2[0]+5, -arm22[2]*dt);
              eq->constraint2->getConstraintValue(0) = rhs[1];

              arm1  = n3;
              arm2  = cross(r2, n3);

              Vector4<T> arm31 = arm1;
              Vector4<T> arm32 = arm2;

              if(eq->type[0] != CVertex){
                eq->constraint3->setValue(velIndex1[0]+0,
                                          arm31[0]*dt*eq->bary[0][0]);
                eq->constraint3->setValue(velIndex1[0]+1,
                                          arm31[1]*dt*eq->bary[0][1]);
                eq->constraint3->setValue(velIndex1[0]+2,
                                          arm31[2]*dt*eq->bary[0][2]);

                eq->constraint3->setValue(velIndex1[1]+0,
                                          arm31[0]*dt*eq->bary[0][0]);
                eq->constraint3->setValue(velIndex1[1]+1,
                                          arm31[1]*dt*eq->bary[0][1]);
                eq->constraint3->setValue(velIndex1[1]+2,
                                          arm31[2]*dt*eq->bary[0][2]);

                eq->constraint3->setValue(velIndex1[2]+0,
                                          arm31[0]*dt*eq->bary[0][0]);
                eq->constraint3->setValue(velIndex1[2]+1,
                                          arm31[1]*dt*eq->bary[0][1]);
                eq->constraint3->setValue(velIndex1[2]+2,
                                          arm31[2]*dt*eq->bary[0][2]);
              }else{
                eq->constraint3->setValue(velIndex1[0]+0, arm31[0]*dt);
                eq->constraint3->setValue(velIndex1[0]+1, arm31[1]*dt);
                eq->constraint3->setValue(velIndex1[0]+2, arm31[2]*dt);
              }
              eq->constraint3->setValue(velIndex2[0]+2, -dt);
              eq->constraint3->setValue(velIndex2[0]+3, -arm32[0]*dt);
              eq->constraint3->setValue(velIndex2[0]+4, -arm32[1]*dt);
              eq->constraint3->setValue(velIndex2[0]+5, -arm32[2]*dt);
              eq->constraint3->getConstraintValue(0) = rhs[2];

              PRINT(dot(arm11, arm21));
              PRINT(dot(arm21,  arm31));
              PRINT(dot(arm31,  arm11));

              PRINT(dot(arm12, arm22));
              PRINT(dot(arm22, arm32));
              PRINT(dot(arm32, arm12));

              PRINT(dot(n1, n2));
              PRINT(dot(n2, n3));
              PRINT(dot(n3, n1));

              //getchar();
              if(step == 0){
                eq->cid[0] = solver->addConstraint(eq->constraint1);
                eq->cid[1] = solver->addConstraint(eq->constraint2);
                eq->cid[2] = solver->addConstraint(eq->constraint3);
              }
            }else{
              /*Deformable vertex*/
              message("Deformable vertex");
              error("Not implemented yet");
            }
          }
        }

        if(eq->descriptor.endFrame <= step && eq->descriptor.endFrame != -1){
          /*Delete constraints*/
          warning("disabling %d", eq->cid[0]);
          warning("disabling %d", eq->cid[1]);
          warning("disabling %d", eq->cid[2]);
          eq->constraint1->status = Inactive;
          eq->constraint2->status = Inactive;
          eq->constraint3->status = Inactive;
          continue;
        }
      }

      warning("step = %d", step);

      for(int i=0;i<nRigidElements;i++){
        objectExternalVelocity[i] = 0;
        objectExternalForce[i] = 0;
      }

      for(int i=0;i<gridFile->getNVelocityDescriptors();i++){
        ExternalVelocityDescriptor<T> d = gridFile->getVelocityDescriptor(i);
        std::cerr << d << std::endl;

        bool enabled = false;

        if(d.startFrame == -1){
          /*No start time specified*/
          if(d.endFrame == -1){
            /*No end time specified*/
            enabled = true;
          }else if(step <= d.endFrame){
            enabled = true;
          }
        }else{
          /*Start time specified*/
          if(d.endFrame == -1){
            if(step >= d.startFrame){
              enabled = true;
            }
          }
          if(step >= d.startFrame){
            if(step <= d.endFrame){
              enabled = true;
            }
          }
        }

        message("enabled = %d", enabled);

        if(enabled){
          objectAngVelocity[d.rigidObject]      = d.angVelocity;
          objectLinVelocity[d.rigidObject]      = d.linVelocity;
          objectExternalVelocity[d.rigidObject] = 1;
        }else{
          if(objectExternalVelocity[d.rigidObject] != 1){
            warning("Disabling external velocity");
            objectAngVelocity[d.rigidObject]      = d.angVelocity*0.0;
            objectLinVelocity[d.rigidObject]      = d.linVelocity*0.0;
            objectExternalVelocity[d.rigidObject] = 0;
          }
        }
      }

      for(int i=0;i<gridFile->getNForceDescriptors();i++){
        ExternalForceDescriptor<T> d = gridFile->getForceDescriptor(i);
        std::cerr << d << std::endl;

        bool enabled = false;

        if(d.startFrame == -1){
          /*No start time specified*/
          if(d.endFrame == -1){
            /*No end time specified*/
            enabled = true;
          }else if(step <= d.endFrame){
            enabled = true;
          }
        }else{
          /*Start time specified*/
          if(d.endFrame == -1){
            if(step >= d.startFrame){
              enabled = true;
            }
          }
          if(step >= d.startFrame){
            if(step <= d.endFrame){
              enabled = true;
            }
          }
        }

        message("enabled = %d", enabled);

        if(enabled){
          objectAngForce[d.rigidObject]      = d.angForce;
          objectLinForce[d.rigidObject]      = d.linForce;
          objectExternalForce[d.rigidObject] = 1;
        }else{
          if(objectExternalForce[d.rigidObject] != 1){
            warning("Disabling external velocity");
            objectAngForce[d.rigidObject]      = d.angForce*0.0;
            objectLinForce[d.rigidObject]      = d.linForce*0.0;
            objectExternalForce[d.rigidObject] = 0;
          }
        }
      }
    }

  protected:
    DCTetraMesh<T>* mesh;

    GridFile<T>* gridFile;

    TetrahedronData<T>* tetdata;
    VertexData* vertdata;

    FEMModel<T>* FEM;

    T density;
    T mu;
    T E;
    T damping;
    T dt;
    T gravity;
    Vector4<T> gravityVector;
    //MatrixT<6, 6, T> dm; /*Material matrix*/

    MatrixT<12, 12, T> intNN;
    MatrixT<12, 3, T>  intN;

    /*Local velocity vectors*/
    MatrixT<1, 12, T> lastVelocity;
    MatrixT<1, 12, T> localInitialPosition;
    MatrixT<1, 12, T> localCurrentPosition;
    MatrixT<1, 12, T> localCorrectedPositions;

    MatrixT<12, 1, T> volumeForceVector;

    Vector<T>* globalInitialPositions;
    Vector<T>* globalCurrentPositions;
    VectorC2<T>* globalLastVelocity; /*Points to computed solution*/
    Vector<T>* globalTotalDisplacement;

    VectorC2<T>* globalForceVector;  /*Points to RHS of solver*/

    SpMatrixC2<2, T>* gm; /*Points to matrix of solver*/

    Matrix44<T>* inertiaTensors;
    T*           objectMass;

    Vector4<T>*  objectAngVelocity;
    Vector4<T>*  objectLinVelocity;
    int*         objectExternalVelocity;

    Vector4<T>*  objectAngForce;
    Vector4<T>*  objectLinForce;
    int*         objectExternalForce;

    int tstep;
    int resumeStep;

    CSVExporter* exporter;
    CSVExporter* conditionExporter;
    IECLinSolveT2<2, T>* solver;
    CollisionContext<T>* ctx;

    int* rigidVerticesMap;

    int nDeformableVertices;
    int nDeformableElements;
    int nStaticVertices;
    int nRigidVertices;
    int nRigidElements;
    int nClothVertices;
    int nDeformableElementFaces;

    int vectorSize;

    List<FixedEqualityConstraint*>  fEqConstraints;
    List<LinkedEqualityConstraint*> lEqConstraints;
    GeneralConstraint<2, T>** volumeConstraints;
  };
}
